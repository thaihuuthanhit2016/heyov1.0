<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\HomeController;
use App\Http\Controllers\AdminController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/admin', [AdminController::class, 'index']);
Route::post('/admin', [AdminController::class, 'postDashboard']);
Route::get('/logout', [AdminController::class, 'getLogout']);

Route::get('/autocomplect_ajax_search_location', [AdminController::class, 'autocomplect_ajax_search_location']);
Route::get('/autocomplect_ajax_search_hotel', [AdminController::class, 'autocomplect_ajax_search_hotel']);

Route::group(['prefix' => '/admin'], function () {
    Route::get('/loadlocation', [AdminController::class, 'loadlocation']);
    Route::get('/load_city_by_location_to', [AdminController::class, 'load_city_by_location_to']);
    Route::get('/load_city_by_location', [AdminController::class, 'load_city_by_location']);
    Route::get('/load_airport_by_city', [AdminController::class, 'load_airport_by_city']);
    Route::get('/load_airport_by_city_to', [AdminController::class, 'load_airport_by_city_to']);
    Route::get('/loadcity', [AdminController::class, 'loadcityadmin']);
    
    Route::group(['prefix' => '/website'], function () {
        Route::get('/list/quocgia', [AdminController::class, 'listquocgia']);
        Route::post('/list/quocgia', [AdminController::class, 'postlistquocgia']);
        Route::get('/list/quocgia/{id}', [AdminController::class, 'listquocgiapaginate']);
        Route::post('/list/quocgia/{id}', [AdminController::class, 'postlistquocgia']);
        Route::get('/edit/quocgia/{id}', [AdminController::class, 'editquocgia']);
        Route::post('/edit/quocgia/{id}', [AdminController::class, 'posteditquocgia']);
        Route::get('/add/quocgia', [AdminController::class, 'addquocgia']);
        Route::post('/add/quocgia', [AdminController::class, 'postaddquocgia']);
        Route::get('/list/tinhthanhpho', [AdminController::class, 'listtinhthanhpho']);
        Route::post('/list/tinhthanhpho', [AdminController::class, 'postlisttinhthanhpho']); 
        Route::get('/list/tinhthanhpho/{id}', [AdminController::class, 'listtinhthanhphopaginate']);
        Route::post('/list/tinhthanhpho/{id}', [AdminController::class, 'postlisttinhthanhpho']);
        Route::get('/edit/tinhthanhpho/{id}', [AdminController::class, 'edittinhthanhpho']);
        Route::post('/edit/tinhthanhpho/{id}', [AdminController::class, 'postedittinhthanhpho']);
        Route::get('/add/tinhthanhpho', [AdminController::class, 'addtinhthanhpho']);
        Route::post('/add/tinhthanhpho', [AdminController::class, 'postaddtinhthanhpho']);
        Route::get('/list/quanhuyen', [AdminController::class, 'listquanhuyen']);
        Route::post('/list/quanhuyen', [AdminController::class, 'postlistquanhuyen']);
        Route::get('/list/quanhuyen/{id}', [AdminController::class, 'listquanhuyenpaginate']);
        Route::post('/list/quanhuyen/{id}', [AdminController::class, 'postlistquanhuyen']);
        Route::get('/edit/quanhuyen/{id}', [AdminController::class, 'editquanhuyen']);
        Route::post('/edit/quanhuyen/{id}', [AdminController::class, 'posteditquanhuyen']);
        Route::get('/add/quanhuyen', [AdminController::class, 'addquanhuyen']);
        Route::post('/add/quanhuyen', [AdminController::class, 'postaddquanhuyen']);
        Route::get('/onoffstatus', [AdminController::class, 'onoffstatuswebsite']);



    });
    Route::group(['prefix' => '/flight'], function () {
      
        Route::get('/orders/detail/{id}', [AdminController::class, 'orderDetailFlight']);
        Route::get('/list', [AdminController::class, 'listFlight']);
        Route::post('/order/{id}/{id1}', [AdminController::class, 'postorderFlight']);
        Route::get('/order/{id}/{id1}', [AdminController::class, 'orderFlight']);
        Route::get('/add', [AdminController::class, 'addFlight']);
        Route::post('/add', [AdminController::class, 'postaddFlight']);
        Route::get('/onoffstatus', [AdminController::class, 'onoffstatusFlight']);
        Route::post('/list', [AdminController::class, 'postlistFlight']);
        Route::get('/view/{id}', [AdminController::class, 'viewFlight']);
        Route::get('/del/{id}', [AdminController::class, 'delFlight']);
        Route::get('/edit/{id}', [AdminController::class, 'editFlight']);
        Route::post('/edit/{id}', [AdminController::class, 'posteditFlight']);
        Route::get('/duplicate/{id}', [AdminController::class, 'duplicateFlight']);
        Route::post('/duplicate/{id}', [AdminController::class, 'postduplicateFlight']);

        Route::get('/list/{id}', [AdminController::class, 'listpaginateFlight']);
        Route::post('/list/{id}', [AdminController::class, 'postlistFlight']);

 
    });
    Route::group(['prefix' => '/hotel'], function () {
        
        
        Route::get('/searchHotel', [AdminController::class, 'searchHotel']);
        Route::get('/searchHotelDetail/{id}', [AdminController::class, 'searchHotelDetail']);
        Route::get('/searchHotelBooking/{id}', [AdminController::class, 'searchHotelBooking']);
        Route::get('/list', [AdminController::class, 'list']);
        Route::get('/onoffstatus', [AdminController::class, 'onoffstatus']);
        Route::post('/list', [AdminController::class, 'postlist']);
        Route::get('/list/{id}', [AdminController::class, 'listpaginate']);
        Route::post('/list/{id}', [AdminController::class, 'postlist']);
        Route::get('/order', [AdminController::class, 'order']);
        Route::post('/order', [AdminController::class, 'postorder']);
        Route::get('/order/{id}', [AdminController::class, 'orderpaginate']);
        Route::post('/order/{id}', [AdminController::class, 'postorder']);
        Route::get('/order/detail/{id}', [AdminController::class, 'getDetailHotel']);

        Route::get('/checktitle', [AdminController::class, 'checktitle']);
        Route::get('/checkcode_pro', [AdminController::class, 'checkcode_pro']);
        Route::get('/checkdateend', [AdminController::class, 'checkdateend']);
        Route::get('/checkinFrom', [AdminController::class, 'checkinFrom']);
        Route::get('/checkinTo', [AdminController::class, 'checkinTo']);

        Route::get('/listerror', [AdminController::class, 'listerror']);
        Route::post('/listerror', [AdminController::class, 'listerror']);
        Route::get('/search', [AdminController::class, 'postlist']);
        Route::get('/add', [AdminController::class, 'add']);
        Route::get('/view/{id}', [AdminController::class, 'view']);
        Route::get('/del/{id}', [AdminController::class, 'del']);
        Route::get('/edit/{id}', [AdminController::class, 'edit']);
        Route::get('/duplicate/{id}', [AdminController::class, 'duplicate']);
        Route::post('/duplicate/{id}', [AdminController::class, 'postduplicate']);
        Route::post('/add', [AdminController::class, 'postadd']);
        Route::post('/edit/{id}', [AdminController::class, 'postedit']);
        Route::get('/customlink', [AdminController::class, 'customlink']);
        Route::post('/loadhotelcity', [AdminController::class, 'loadhotelcity']);
        Route::get('/loaddata', [AdminController::class, 'loaddata']);
        Route::get('/loadcity', [AdminController::class, 'loadcity']);
        Route::get('/delservice', [AdminController::class, 'delservice']);

        Route::get('/order/{id}/{id1}', [AdminController::class, 'orderpaginate']);
        Route::post('/order/{id}/{id1}', [AdminController::class, 'postorder']);

    });
});
Route::post('/api/promo', [AdminController::class, 'apipromo']);
Route::post('/api/checkpromo', [AdminController::class, 'apicheckpromo']);
Route::post('/api/addpromo', [AdminController::class, 'apiaddpromoCustomer']);
Route::post('/api/listpromocustomer', [AdminController::class, 'apilistpromocustomer']);
Route::post('/api/usepromocustomer', [AdminController::class, 'apiusepromocustomer']);
Route::post('/api/tenpromolast', [AdminController::class, 'tenpromolast']);
Route::post('/api/promotionsbytype', [AdminController::class, 'apipromotionsbytype']);

