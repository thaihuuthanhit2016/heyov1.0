$(document).ready(function () {
    $("#btn_admin").click(function (e) {
        e.stopPropagation();
        var display = $("#profile").css(["display"]);
        if (display.display === "none") {
            document.getElementById("profile").style = "display: flex";
        } else {
            document.getElementById("profile").style = "display: none";
        }
    }); 
});

$(document).ready(function () {

    $("#button_tab_all").click(function () {
        $("#all_order").removeClass("hide");
        $("#all_order").addClass("show");
        $("#waiting_order").removeClass("show");
        $("#waiting_order").addClass("hide");
        $("#accepted_order").removeClass("show");
        $("#accepted_order").addClass("hide");
        $("#canceled_order").removeClass("show");
        $("#canceled_order").addClass("hide");
        $("#done_order").removeClass("show");
        $("#done_order").addClass("hide");
        $("#button_tab_all").addClass("tab_active");
        $("#button_tab_waiting").removeClass("tab_active");
        $("#button_tab_accepted").removeClass("tab_active");
        $("#button_tab_canceled").removeClass("tab_active");
        $("#button_tab_done").removeClass("tab_active");
    });
    $("#button_tab_waiting").click(function () {
        $("#all_order").removeClass("show");
        $("#all_order").addClass("hide");
        $("#waiting_order").removeClass("hide");
        $("#waiting_order").addClass("show");
        $("#accepted_order").removeClass("show");
        $("#accepted_order").addClass("hide");
        $("#canceled_order").removeClass("show");
        $("#canceled_order").addClass("hide");
        $("#done_order").removeClass("show");
        $("#done_order").addClass("hide");
        $("#button_tab_all").removeClass("tab_active");
        $("#button_tab_waiting").addClass("tab_active");
        $("#button_tab_accepted").removeClass("tab_active");
        $("#button_tab_canceled").removeClass("tab_active");
        $("#button_tab_done").removeClass("tab_active");
    });
    $("#button_tab_accepted").click(function () {
        $("#all_order").removeClass("show");
        $("#all_order").addClass("hide");
        $("#waiting_order").removeClass("show");
        $("#waiting_order").addClass("hide");
        $("#accepted_order").removeClass("hide");
        $("#accepted_order").addClass("show");
        $("#canceled_order").removeClass("show");
        $("#canceled_order").addClass("hide");
        $("#done_order").removeClass("show");
        $("#done_order").addClass("hide");
        $("#button_tab_all").removeClass("tab_active");
        $("#button_tab_waiting").removeClass("tab_active");
        $("#button_tab_accepted").addClass("tab_active");
        $("#button_tab_canceled").removeClass("tab_active");
        $("#button_tab_done").removeClass("tab_active");
    });
    $("#button_tab_canceled").click(function () {
        $("#all_order").removeClass("show");
        $("#all_order").addClass("hide");
        $("#waiting_order").removeClass("show");
        $("#waiting_order").addClass("hide");
        $("#accepted_order").removeClass("show");
        $("#accepted_order").addClass("hide");
        $("#canceled_order").addClass("show");
        $("#canceled_order").removeClass("hide");
        $("#done_order").removeClass("show");
        $("#done_order").addClass("hide");
        $("#button_tab_all").removeClass("tab_active");
        $("#button_tab_waiting").removeClass("tab_active");
        $("#button_tab_accepted").removeClass("tab_active");
        $("#button_tab_canceled").addClass("tab_active");
        $("#button_tab_done").removeClass("tab_active");
    });
    $("#button_tab_done").click(function () {
        $("#all_order").removeClass("show");
        $("#all_order").addClass("hide");
        $("#waiting_order").removeClass("show");
        $("#waiting_order").addClass("hide");
        $("#accepted_order").removeClass("show");
        $("#accepted_order").addClass("hide");
        $("#canceled_order").removeClass("show");
        $("#canceled_order").addClass("hide");
        $("#done_order").removeClass("hide");
        $("#done_order").addClass("show");
        $("#button_tab_all").removeClass("tab_active");
        $("#button_tab_waiting").removeClass("tab_active");
        $("#button_tab_accepted").removeClass("tab_active");
        $("#button_tab_canceled").removeClass("tab_active");
        $("#button_tab_done").addClass("tab_active");
    });
    $("body").click(function () {
        document.getElementById("profile").style = "display: none";
        if (document.getElementById("box_popover_id1"))
            document.getElementById("box_popover_id1").style = "display: none";
        if (document.getElementById("box_popover_id2"))
            document.getElementById("box_popover_id2").style = "display: none";
        if (document.getElementById("box_popover_id3"))
            document.getElementById("box_popover_id3").style = "display: none";
        if (document.getElementById("box_popover_id4"))
            document.getElementById("box_popover_id4").style = "display: none";
        if (document.getElementById("box_popover_id5"))
            document.getElementById("box_popover_id5").style = "display: none";
        if (document.getElementById("open_popover_done1"))
            document.getElementById("box_popover_id_done1").style = "display: none";
        if (document.getElementById("open_popover_done2"))
            document.getElementById("box_popover_id_done2").style = "display: none";
        if (document.getElementById("open_popover_done3"))
            document.getElementById("box_popover_id_done3").style = "display: none";
        if (document.getElementById("open_popover_done4"))
            document.getElementById("box_popover_id_done4").style = "display: none";
        if (document.getElementById("open_popover_done5"))
            document.getElementById("box_popover_id_done5").style = "display: none";
        if (document.getElementById("open_popover_cancle1"))
            document.getElementById("box_popover_id_cancle1").style = "display: none";
        if (document.getElementById("open_popover_cancle2"))
            document.getElementById("box_popover_id_cancle2").style = "display: none";
        if (document.getElementById("open_popover_cancle3"))
            document.getElementById("box_popover_id_cancle3").style = "display: none";
        if (document.getElementById("open_popover_cancle4"))
            document.getElementById("box_popover_id_cancle4").style = "display: none";
        if (document.getElementById("open_popover_cancle5"))
            document.getElementById("box_popover_id_cancle5").style = "display: none";
        if (document.getElementById("open_popover_waiting1"))
            document.getElementById("box_popover_id_waiting1").style = "display: none";
        if (document.getElementById("open_popover_waiting2"))
            document.getElementById("box_popover_id_waiting2").style = "display: none";
        if (document.getElementById("open_popover_waiting3"))
            document.getElementById("box_popover_id_waiting3").style = "display: none";
        if (document.getElementById("open_popover_waiting4"))
            document.getElementById("box_popover_id_waiting4").style = "display: none";
        if (document.getElementById("open_popover_waiting5"))
            document.getElementById("box_popover_id_waiting5").style = "display: none";
        if (document.getElementById("open_popover_waiting6"))
            document.getElementById("box_popover_id_waiting6").style = "display: none";
        if (document.getElementById("open_popover_waiting7"))
            document.getElementById("box_popover_id_waiting7").style = "display: none";
        if (document.getElementById("open_popover_waiting8"))
            document.getElementById("box_popover_id_waiting8").style = "display: none";
        if (document.getElementById("open_popover_waiting9"))
            document.getElementById("box_popover_id_waiting9").style = "display: none";
        if (document.getElementById("open_popover_waiting10"))
            document.getElementById("box_popover_id_waiting10").style = "display: none";
        if (document.getElementById("open_popover_accepted1"))
            document.getElementById("box_popover_id_accepted1").style = "display: none";
        if (document.getElementById("open_popover_accepted2"))
            document.getElementById("box_popover_id_accepted2").style = "display: none";
        if (document.getElementById("open_popover_accepted3"))
            document.getElementById("box_popover_id_accepted3").style = "display: none";
        if (document.getElementById("open_popover_accepted4"))
            document.getElementById("box_popover_id_accepted4").style = "display: none";
        if (document.getElementById("open_popover_accepted5"))
            document.getElementById("box_popover_id_accepted5").style = "display: none";
        if (document.getElementById("open_popover_accepted6"))
            document.getElementById("box_popover_id_accepted6").style = "display: none";
        if (document.getElementById("open_popover_accepted7"))
            document.getElementById("box_popover_id_accepted7").style = "display: none";
        if (document.getElementById("open_popover_accepted8"))
            document.getElementById("box_popover_id_accepted8").style = "display: none";
        if (document.getElementById("open_popover_accepted9"))
            document.getElementById("box_popover_id_accepted9").style = "display: none";
        if (document.getElementById("open_popover_accepted10"))
            document.getElementById("box_popover_id_accepted10").style = "display: none";
        if (document.getElementById("open_popover_doned1"))
            document.getElementById("box_popover_id_doned1").style = "display: none";
        if (document.getElementById("open_popover_doned2"))
            document.getElementById("box_popover_id_doned2").style = "display: none";
        if (document.getElementById("open_popover_doned3"))
            document.getElementById("box_popover_id_doned3").style = "display: none";
        if (document.getElementById("open_popover_doned4"))
            document.getElementById("box_popover_id_doned4").style = "display: none";
        if (document.getElementById("open_popover_doned5"))
            document.getElementById("box_popover_id_doned5").style = "display: none";
        if (document.getElementById("open_popover_doned6"))
            document.getElementById("box_popover_id_doned6").style = "display: none";
        if (document.getElementById("open_popover_doned7"))
            document.getElementById("box_popover_id_doned7").style = "display: none";
        if (document.getElementById("open_popover_doned8"))
            document.getElementById("box_popover_id_doned8").style = "display: none";
        if (document.getElementById("open_popover_doned9"))
            document.getElementById("box_popover_id_doned9").style = "display: none";
        if (document.getElementById("open_popover_doned10"))
            document.getElementById("box_popover_id_doned10").style = "display: none";
        if (document.getElementById("open_popover_cancled1"))
            document.getElementById("box_popover_id_cancled1").style = "display: none";
        if (document.getElementById("open_popover_cancled2"))
            document.getElementById("box_popover_id_cancled2").style = "display: none";
        if (document.getElementById("open_popover_cancled3"))
            document.getElementById("box_popover_id_cancled3").style = "display: none";
        if (document.getElementById("open_popover_cancled4"))
            document.getElementById("box_popover_id_cancled4").style = "display: none";
        if (document.getElementById("open_popover_cancled5"))
            document.getElementById("box_popover_id_cancled5").style = "display: none";
        if (document.getElementById("open_popover_cancled6"))
            document.getElementById("box_popover_id_cancled6").style = "display: none";
        if (document.getElementById("open_popover_cancled7"))
            document.getElementById("box_popover_id_cancled7").style = "display: none";
        if (document.getElementById("open_popover_cancled8"))
            document.getElementById("box_popover_id_cancled8").style = "display: none";
        if (document.getElementById("open_popover_cancled9"))
            document.getElementById("box_popover_id_cancled9").style = "display: none";
        if (document.getElementById("open_popover_cancled10"))
            document.getElementById("box_popover_id_cancled10").style = "display: none";

        if (document.getElementById("open_popover_flight_waiting1"))
            document.getElementById("box_popover_id_flight_waiting1").style = "display: none";

        if (document.getElementById("open_popover_flight_waiting2"))
            document.getElementById("box_popover_id_flight_waiting2").style = "display: none";

        if (document.getElementById("open_popover_flight_waiting3"))
            document.getElementById("box_popover_id_flight_waiting3").style = "display: none";

        if (document.getElementById("open_popover_flight_waiting4"))
            document.getElementById("box_popover_id_flight_waiting4").style = "display: none";
        if (document.getElementById("open_popover_flight_waiting5"))
            document.getElementById("box_popover_id_flight_waiting5").style = "display: none";

        if (document.getElementById("open_popover_flight_accepte1"))
            document.getElementById("box_popover_id_flight_accepte1").style = "display: none";

  
        if (document.getElementById("open_popover_flight_accepte2"))
            document.getElementById("box_popover_id_flight_accepte2").style = "display: none";

        if (document.getElementById("open_popover_flight_accepte3"))
            document.getElementById("box_popover_id_flight_accepte3").style = "display: none";

        if (document.getElementById("open_popover_flight_accepte4"))
            document.getElementById("box_popover_id_flight_accepte4").style = "display: none";

        if (document.getElementById("open_popover_flight_accepte5"))
            document.getElementById("box_popover_id_flight_accepte5").style = "display: none";
        if (document.getElementById("open_popover_flight_cancle1"))
            document.getElementById("box_popover_id_flight_cancle1").style = "display: none";
        if (document.getElementById("open_popover_flight_cancle2"))
            document.getElementById("box_popover_id_flight_cancle2").style = "display: none";
        if (document.getElementById("open_popover_flight_cancle3"))
            document.getElementById("box_popover_id_flight_cancle3").style = "display: none";
        if (document.getElementById("open_popover_flight_cancle4"))
            document.getElementById("box_popover_id_flight_cancle4").style = "display: none";
        if (document.getElementById("open_popover_flight_cancle5"))
            document.getElementById("box_popover_id_flight_cancle5").style = "display: none";
        if (document.getElementById("open_popover_flight_done1"))
            document.getElementById("box_popover_id_flight_done1").style = "display: none";
        if (document.getElementById("open_popover_flight_done2"))
            document.getElementById("box_popover_id_flight_done2").style = "display: none";
        if (document.getElementById("open_popover_flight_done3"))
            document.getElementById("box_popover_id_flight_done3").style = "display: none";
        if (document.getElementById("open_popover_flight_done4"))
            document.getElementById("box_popover_id_flight_done4").style = "display: none";
        if (document.getElementById("open_popover_flight_done5"))
            document.getElementById("box_popover_id_flight_done5").style = "display: none";
        if (document.getElementById("open_popover_flight_waited1"))
            document.getElementById("box_popover_id_flight_waited1").style = "display: none";
  
        if (document.getElementById("open_popover_flight_waited2"))
            document.getElementById("box_popover_id_flight_waited2").style = "display: none";
        if (document.getElementById("open_popover_flight_waited3"))
            document.getElementById("box_popover_id_flight_waited3").style = "display: none";
        if (document.getElementById("open_popover_flight_waited4"))
            document.getElementById("box_popover_id_flight_waited4").style = "display: none";
        if (document.getElementById("open_popover_flight_waited5"))
            document.getElementById("box_popover_id_flight_waited5").style = "display: none";
        if (document.getElementById("open_popover_flight_waited6"))
            document.getElementById("box_popover_id_flight_waited6").style = "display: none";
        if (document.getElementById("open_popover_flight_waited7"))
            document.getElementById("box_popover_id_flight_waited7").style = "display: none";
        if (document.getElementById("open_popover_flight_waited8"))
            document.getElementById("box_popover_id_flight_waited8").style = "display: none";
        if (document.getElementById("open_popover_flight_waited9"))
            document.getElementById("box_popover_id_flight_waited9").style = "display: none";
        if (document.getElementById("open_popover_flight_waited10"))
            document.getElementById("box_popover_id_flight_waited10").style = "display: none";

        if (document.getElementById("open_popover_flight_accepted1"))
            document.getElementById("box_popover_id_flight_accepted1").style = "display: none";
        if (document.getElementById("open_popover_flight_accepted2"))
            document.getElementById("box_popover_id_flight_accepted2").style = "display: none";
        if (document.getElementById("open_popover_flight_accepted3"))
            document.getElementById("box_popover_id_flight_accepted3").style = "display: none";
        if (document.getElementById("open_popover_flight_accepted4"))
            document.getElementById("box_popover_id_flight_accepted4").style = "display: none";
        if (document.getElementById("open_popover_flight_accepted5"))
            document.getElementById("box_popover_id_flight_accepted5").style = "display: none";
        if (document.getElementById("open_popover_flight_accepted6"))
            document.getElementById("box_popover_id_flight_accepted6").style = "display: none";
        if (document.getElementById("open_popover_flight_accepted7"))
            document.getElementById("box_popover_id_flight_accepted7").style = "display: none";
        if (document.getElementById("open_popover_flight_accepted8"))
            document.getElementById("box_popover_id_flight_accepted8").style = "display: none";
        if (document.getElementById("open_popover_flight_accepted9"))
            document.getElementById("box_popover_id_flight_accepted9").style = "display: none";
        if (document.getElementById("open_popover_flight_accepted10"))
            document.getElementById("box_popover_id_flight_accepted10").style = "display: none";
        if (document.getElementById("open_popover_flight_cancled1"))
            document.getElementById("box_popover_id_flight_cancled1").style = "display: none";
        if (document.getElementById("open_popover_flight_cancled2"))
            document.getElementById("box_popover_id_flight_cancled2").style = "display: none";
        if (document.getElementById("open_popover_flight_cancled3"))
            document.getElementById("box_popover_id_flight_cancled3").style = "display: none";
        if (document.getElementById("open_popover_flight_cancled4"))
            document.getElementById("box_popover_id_flight_cancled4").style = "display: none";
        if (document.getElementById("open_popover_flight_cancled5"))
            document.getElementById("box_popover_id_flight_cancled5").style = "display: none";
        if (document.getElementById("open_popover_flight_cancled6"))
            document.getElementById("box_popover_id_flight_cancled6").style = "display: none";
        if (document.getElementById("open_popover_flight_cancled7"))
            document.getElementById("box_popover_id_flight_cancled7").style = "display: none";
        if (document.getElementById("open_popover_flight_cancled8"))
            document.getElementById("box_popover_id_flight_cancled8").style = "display: none";
        if (document.getElementById("open_popover_flight_cancled9"))
            document.getElementById("box_popover_id_flight_cancled9").style = "display: none";
        if (document.getElementById("open_popover_flight_cancled10"))
            document.getElementById("box_popover_id_flight_cancled10").style = "display: none";
        if (document.getElementById("open_popover_flight_doned1"))
            document.getElementById("box_popover_id_flight_doned1").style = "display: none";
        if (document.getElementById("open_popover_flight_doned2"))
            document.getElementById("box_popover_id_flight_doned2").style = "display: none";
        if (document.getElementById("open_popover_flight_doned3"))
            document.getElementById("box_popover_id_flight_doned3").style = "display: none";
        if (document.getElementById("open_popover_flight_doned4"))
            document.getElementById("box_popover_id_flight_doned4").style = "display: none";
        if (document.getElementById("open_popover_flight_doned5"))
            document.getElementById("box_popover_id_flight_doned5").style = "display: none";
        if (document.getElementById("open_popover_flight_doned6"))
            document.getElementById("box_popover_id_flight_doned6").style = "display: none";
        if (document.getElementById("open_popover_flight_doned7"))
            document.getElementById("box_popover_id_flight_doned7").style = "display: none";
        if (document.getElementById("open_popover_flight_doned8"))
            document.getElementById("box_popover_id_flight_doned8").style = "display: none";
        if (document.getElementById("open_popover_flight_doned9"))
            document.getElementById("box_popover_id_flight_doned9").style = "display: none";
        if (document.getElementById("open_popover_flight_doned10"))
            document.getElementById("box_popover_id_flight_doned10").style = "display: none";


    });
    $("#dots1").click(function (e) {
        e.stopPropagation();
        var position = $("#profile").css(["top", "right"]);
        console.log($("#dots1").position());
        console.log('1');
        document.getElementById("box_option_discount1").style = "display: block";
        document.getElementById("box_option_discount2").style = "display: none";
        document.getElementById("box_option_discount3").style = "display: none";
        document.getElementById("box_option_discount4").style = "display: none";
        document.getElementById("box_option_discount5").style = "display: none";
        document.getElementById("box_option_discount6").style = "display: none";
        document.getElementById("box_option_discount7").style = "display: none";
        document.getElementById("box_option_discount8").style = "display: none";
        document.getElementById("box_option_discount9").style = "display: none";
        document.getElementById("box_option_discount10").style = "display: none";

        document.getElementById("box_option_discount12").style = "display: none";
        document.getElementById("box_option_discount13").style = "display: none";
        document.getElementById("box_option_discount14").style = "display: none";
        document.getElementById("box_option_discount15").style = "display: none";
        document.getElementById("box_option_discount16").style = "display: none";
        document.getElementById("box_option_discount17").style = "display: none";
        document.getElementById("box_option_discount18").style = "display: none";
        document.getElementById("box_option_discount19").style = "display: none";
        document.getElementById("box_option_discount20").style = "display: none";

        
        document.getElementById("box_option_discount21").style = "display: none";
        document.getElementById("box_option_discount22").style = "display: none";
        document.getElementById("box_option_discount23").style = "display: none";
        document.getElementById("box_option_discount24").style = "display: none";
        document.getElementById("box_option_discount25").style = "display: none";
        document.getElementById("box_option_discount26").style = "display: none";
        document.getElementById("box_option_discount27").style = "display: none";
        document.getElementById("box_option_discount28").style = "display: none";
        document.getElementById("box_option_discount29").style = "display: none";
        document.getElementById("box_option_discount30").style = "display: none";
    });
    $("#dots2").click(function (e) {
        e.stopPropagation();
        var position = $("#profile").css(["top", "right"]);
        console.log($("#dots").position());
        console.log('2');
        document.getElementById("box_option_discount2").style = "display: block";

        document.getElementById("box_option_discount1").style = "display: none";
        document.getElementById("box_option_discount3").style = "display: none";
        document.getElementById("box_option_discount4").style = "display: none";
        document.getElementById("box_option_discount5").style = "display: none";
        document.getElementById("box_option_discount6").style = "display: none";
        document.getElementById("box_option_discount7").style = "display: none";
        document.getElementById("box_option_discount8").style = "display: none";
        document.getElementById("box_option_discount9").style = "display: none";
        document.getElementById("box_option_discount10").style = "display: none";

        document.getElementById("box_option_discount12").style = "display: none";
        document.getElementById("box_option_discount13").style = "display: none";
        document.getElementById("box_option_discount14").style = "display: none";
        document.getElementById("box_option_discount15").style = "display: none";
        document.getElementById("box_option_discount16").style = "display: none";
        document.getElementById("box_option_discount17").style = "display: none";
        document.getElementById("box_option_discount18").style = "display: none";
        document.getElementById("box_option_discount19").style = "display: none";
        document.getElementById("box_option_discount20").style = "display: none";

        
        document.getElementById("box_option_discount21").style = "display: none";
        document.getElementById("box_option_discount22").style = "display: none";
        document.getElementById("box_option_discount23").style = "display: none";
        document.getElementById("box_option_discount24").style = "display: none";
        document.getElementById("box_option_discount25").style = "display: none";
        document.getElementById("box_option_discount26").style = "display: none";
        document.getElementById("box_option_discount27").style = "display: none";
        document.getElementById("box_option_discount28").style = "display: none";
        document.getElementById("box_option_discount29").style = "display: none";
        document.getElementById("box_option_discount30").style = "display: none";
    });
    $("#dots3").click(function (e) {
        e.stopPropagation();console.log('3');
        var position = $("#profile").css(["top", "right"]);
        console.log($("#dots").position());
        document.getElementById("box_option_discount3").style = "display: block";

        document.getElementById("box_option_discount2").style = "display: none";
        document.getElementById("box_option_discount1").style = "display: none";
        document.getElementById("box_option_discount4").style = "display: none";
        document.getElementById("box_option_discount5").style = "display: none";
        document.getElementById("box_option_discount6").style = "display: none";
        document.getElementById("box_option_discount7").style = "display: none";
        document.getElementById("box_option_discount8").style = "display: none";
        document.getElementById("box_option_discount9").style = "display: none";
        document.getElementById("box_option_discount10").style = "display: none";

        document.getElementById("box_option_discount12").style = "display: none";
        document.getElementById("box_option_discount13").style = "display: none";
        document.getElementById("box_option_discount14").style = "display: none";
        document.getElementById("box_option_discount15").style = "display: none";
        document.getElementById("box_option_discount16").style = "display: none";
        document.getElementById("box_option_discount17").style = "display: none";
        document.getElementById("box_option_discount18").style = "display: none";
        document.getElementById("box_option_discount19").style = "display: none";
        document.getElementById("box_option_discount20").style = "display: none";

        
        document.getElementById("box_option_discount21").style = "display: none";
        document.getElementById("box_option_discount22").style = "display: none";
        document.getElementById("box_option_discount23").style = "display: none";
        document.getElementById("box_option_discount24").style = "display: none";
        document.getElementById("box_option_discount25").style = "display: none";
        document.getElementById("box_option_discount26").style = "display: none";
        document.getElementById("box_option_discount27").style = "display: none";
        document.getElementById("box_option_discount28").style = "display: none";
        document.getElementById("box_option_discount29").style = "display: none";
        document.getElementById("box_option_discount30").style = "display: none";
    });
    $("#dots4").click(function (e) {
        e.stopPropagation();
        console.log('4');
        var position = $("#profile").css(["top", "right"]);
        console.log($("#dots").position());
        document.getElementById("box_option_discount4").style = "display: block";

        document.getElementById("box_option_discount2").style = "display: none";
        document.getElementById("box_option_discount3").style = "display: none";
        document.getElementById("box_option_discount1").style = "display: none";
        document.getElementById("box_option_discount5").style = "display: none";
        document.getElementById("box_option_discount6").style = "display: none";
        document.getElementById("box_option_discount7").style = "display: none";
        document.getElementById("box_option_discount8").style = "display: none";
        document.getElementById("box_option_discount9").style = "display: none";
        document.getElementById("box_option_discount10").style = "display: none";

        document.getElementById("box_option_discount12").style = "display: none";
        document.getElementById("box_option_discount13").style = "display: none";
        document.getElementById("box_option_discount14").style = "display: none";
        document.getElementById("box_option_discount15").style = "display: none";
        document.getElementById("box_option_discount16").style = "display: none";
        document.getElementById("box_option_discount17").style = "display: none";
        document.getElementById("box_option_discount18").style = "display: none";
        document.getElementById("box_option_discount19").style = "display: none";
        document.getElementById("box_option_discount20").style = "display: none";

        
        document.getElementById("box_option_discount21").style = "display: none";
        document.getElementById("box_option_discount22").style = "display: none";
        document.getElementById("box_option_discount23").style = "display: none";
        document.getElementById("box_option_discount24").style = "display: none";
        document.getElementById("box_option_discount25").style = "display: none";
        document.getElementById("box_option_discount26").style = "display: none";
        document.getElementById("box_option_discount27").style = "display: none";
        document.getElementById("box_option_discount28").style = "display: none";
        document.getElementById("box_option_discount29").style = "display: none";
        document.getElementById("box_option_discount30").style = "display: none";
    });
    $("#dots5").click(function (e) {
        e.stopPropagation();
        console.log('5');
        var position = $("#profile").css(["top", "right"]);
        console.log($("#dots").position());
        document.getElementById("box_option_discount5").style = "display: block";

        document.getElementById("box_option_discount2").style = "display: none";
        document.getElementById("box_option_discount3").style = "display: none";
        document.getElementById("box_option_discount4").style = "display: none";
        document.getElementById("box_option_discount1").style = "display: none";
        document.getElementById("box_option_discount6").style = "display: none";
        document.getElementById("box_option_discount7").style = "display: none";
        document.getElementById("box_option_discount8").style = "display: none";
        document.getElementById("box_option_discount9").style = "display: none";
        document.getElementById("box_option_discount10").style = "display: none";

        document.getElementById("box_option_discount12").style = "display: none";
        document.getElementById("box_option_discount13").style = "display: none";
        document.getElementById("box_option_discount14").style = "display: none";
        document.getElementById("box_option_discount15").style = "display: none";
        document.getElementById("box_option_discount16").style = "display: none";
        document.getElementById("box_option_discount17").style = "display: none";
        document.getElementById("box_option_discount18").style = "display: none";
        document.getElementById("box_option_discount19").style = "display: none";
        document.getElementById("box_option_discount20").style = "display: none";

        
        document.getElementById("box_option_discount21").style = "display: none";
        document.getElementById("box_option_discount22").style = "display: none";
        document.getElementById("box_option_discount23").style = "display: none";
        document.getElementById("box_option_discount24").style = "display: none";
        document.getElementById("box_option_discount25").style = "display: none";
        document.getElementById("box_option_discount26").style = "display: none";
        document.getElementById("box_option_discount27").style = "display: none";
        document.getElementById("box_option_discount28").style = "display: none";
        document.getElementById("box_option_discount29").style = "display: none";
        document.getElementById("box_option_discount30").style = "display: none";
    });
    $("#dots6").click(function (e) {
        e.stopPropagation();console.log('6');
        var position = $("#profile").css(["top", "right"]);
        console.log($("#dots").position());
        document.getElementById("box_option_discount6").style = "display: block";

        document.getElementById("box_option_discount2").style = "display: none";
        document.getElementById("box_option_discount3").style = "display: none";
        document.getElementById("box_option_discount4").style = "display: none";
        document.getElementById("box_option_discount5").style = "display: none";
        document.getElementById("box_option_discount1").style = "display: none";
        document.getElementById("box_option_discount7").style = "display: none";
        document.getElementById("box_option_discount8").style = "display: none";
        document.getElementById("box_option_discount9").style = "display: none";
        document.getElementById("box_option_discount10").style = "display: none";

        document.getElementById("box_option_discount12").style = "display: none";
        document.getElementById("box_option_discount13").style = "display: none";
        document.getElementById("box_option_discount14").style = "display: none";
        document.getElementById("box_option_discount15").style = "display: none";
        document.getElementById("box_option_discount16").style = "display: none";
        document.getElementById("box_option_discount17").style = "display: none";
        document.getElementById("box_option_discount18").style = "display: none";
        document.getElementById("box_option_discount19").style = "display: none";
        document.getElementById("box_option_discount20").style = "display: none";

        
        document.getElementById("box_option_discount21").style = "display: none";
        document.getElementById("box_option_discount22").style = "display: none";
        document.getElementById("box_option_discount23").style = "display: none";
        document.getElementById("box_option_discount24").style = "display: none";
        document.getElementById("box_option_discount25").style = "display: none";
        document.getElementById("box_option_discount26").style = "display: none";
        document.getElementById("box_option_discount27").style = "display: none";
        document.getElementById("box_option_discount28").style = "display: none";
        document.getElementById("box_option_discount29").style = "display: none";
        document.getElementById("box_option_discount30").style = "display: none";
    });
    $("#dots7").click(function (e) {
        e.stopPropagation();console.log('7');
        var position = $("#profile").css(["top", "right"]);
        console.log($("#dots").position());
        document.getElementById("box_option_discount7").style = "display: block";

        document.getElementById("box_option_discount2").style = "display: none";
        document.getElementById("box_option_discount3").style = "display: none";
        document.getElementById("box_option_discount4").style = "display: none";
        document.getElementById("box_option_discount5").style = "display: none";
        document.getElementById("box_option_discount6").style = "display: none";
        document.getElementById("box_option_discount1").style = "display: none";
        document.getElementById("box_option_discount8").style = "display: none";
        document.getElementById("box_option_discount9").style = "display: none";
        document.getElementById("box_option_discount10").style = "display: none";

        document.getElementById("box_option_discount12").style = "display: none";
        document.getElementById("box_option_discount13").style = "display: none";
        document.getElementById("box_option_discount14").style = "display: none";
        document.getElementById("box_option_discount15").style = "display: none";
        document.getElementById("box_option_discount16").style = "display: none";
        document.getElementById("box_option_discount17").style = "display: none";
        document.getElementById("box_option_discount18").style = "display: none";
        document.getElementById("box_option_discount19").style = "display: none";
        document.getElementById("box_option_discount20").style = "display: none";

        
        document.getElementById("box_option_discount21").style = "display: none";
        document.getElementById("box_option_discount22").style = "display: none";
        document.getElementById("box_option_discount23").style = "display: none";
        document.getElementById("box_option_discount24").style = "display: none";
        document.getElementById("box_option_discount25").style = "display: none";
        document.getElementById("box_option_discount26").style = "display: none";
        document.getElementById("box_option_discount27").style = "display: none";
        document.getElementById("box_option_discount28").style = "display: none";
        document.getElementById("box_option_discount29").style = "display: none";
        document.getElementById("box_option_discount30").style = "display: none";
    });
    $("#dots8").click(function (e) {
        e.stopPropagation();console.log('8');
        var position = $("#profile").css(["top", "right"]);
        console.log($("#dots").position());
        document.getElementById("box_option_discount8").style = "display: block";

        document.getElementById("box_option_discount2").style = "display: none";
        document.getElementById("box_option_discount3").style = "display: none";
        document.getElementById("box_option_discount4").style = "display: none";
        document.getElementById("box_option_discount5").style = "display: none";
        document.getElementById("box_option_discount6").style = "display: none";
        document.getElementById("box_option_discount1").style = "display: none";
        document.getElementById("box_option_discount7").style = "display: none";
        document.getElementById("box_option_discount9").style = "display: none";
        document.getElementById("box_option_discount10").style = "display: none";

        document.getElementById("box_option_discount12").style = "display: none";
        document.getElementById("box_option_discount13").style = "display: none";
        document.getElementById("box_option_discount14").style = "display: none";
        document.getElementById("box_option_discount15").style = "display: none";
        document.getElementById("box_option_discount16").style = "display: none";
        document.getElementById("box_option_discount17").style = "display: none";
        document.getElementById("box_option_discount18").style = "display: none";
        document.getElementById("box_option_discount19").style = "display: none";
        document.getElementById("box_option_discount20").style = "display: none";

        
        document.getElementById("box_option_discount21").style = "display: none";
        document.getElementById("box_option_discount22").style = "display: none";
        document.getElementById("box_option_discount23").style = "display: none";
        document.getElementById("box_option_discount24").style = "display: none";
        document.getElementById("box_option_discount25").style = "display: none";
        document.getElementById("box_option_discount26").style = "display: none";
        document.getElementById("box_option_discount27").style = "display: none";
        document.getElementById("box_option_discount28").style = "display: none";
        document.getElementById("box_option_discount29").style = "display: none";
        document.getElementById("box_option_discount30").style = "display: none";

    });
    $("#dots9").click(function (e) {
        e.stopPropagation();console.log('9');
        var position = $("#profile").css(["top", "right"]);
        console.log($("#dots").position());
        document.getElementById("box_option_discount9").style = "display: block";

        document.getElementById("box_option_discount2").style = "display: none";
        document.getElementById("box_option_discount3").style = "display: none";
        document.getElementById("box_option_discount4").style = "display: none";
        document.getElementById("box_option_discount5").style = "display: none";
        document.getElementById("box_option_discount6").style = "display: none";
        document.getElementById("box_option_discount1").style = "display: none";
        document.getElementById("box_option_discount8").style = "display: none";
        document.getElementById("box_option_discount7").style = "display: none";
        document.getElementById("box_option_discount10").style = "display: none";

        document.getElementById("box_option_discount12").style = "display: none";
        document.getElementById("box_option_discount13").style = "display: none";
        document.getElementById("box_option_discount14").style = "display: none";
        document.getElementById("box_option_discount15").style = "display: none";
        document.getElementById("box_option_discount16").style = "display: none";
        document.getElementById("box_option_discount17").style = "display: none";
        document.getElementById("box_option_discount18").style = "display: none";
        document.getElementById("box_option_discount19").style = "display: none";
        document.getElementById("box_option_discount20").style = "display: none";

        
        document.getElementById("box_option_discount21").style = "display: none";
        document.getElementById("box_option_discount22").style = "display: none";
        document.getElementById("box_option_discount23").style = "display: none";
        document.getElementById("box_option_discount24").style = "display: none";
        document.getElementById("box_option_discount25").style = "display: none";
        document.getElementById("box_option_discount26").style = "display: none";
        document.getElementById("box_option_discount27").style = "display: none";
        document.getElementById("box_option_discount28").style = "display: none";
        document.getElementById("box_option_discount29").style = "display: none";
        document.getElementById("box_option_discount30").style = "display: none";
    });
    $("#dots10").click(function (e) {
        e.stopPropagation();console.log('10');
        var position = $("#profile").css(["top", "right"]);
        console.log($("#dots").position());
        document.getElementById("box_option_discount10").style = "display: block";

        document.getElementById("box_option_discount2").style = "display: none";
        document.getElementById("box_option_discount3").style = "display: none";
        document.getElementById("box_option_discount4").style = "display: none";
        document.getElementById("box_option_discount5").style = "display: none";
        document.getElementById("box_option_discount6").style = "display: none";
        document.getElementById("box_option_discount1").style = "display: none";
        document.getElementById("box_option_discount8").style = "display: none";
        document.getElementById("box_option_discount9").style = "display: none";
        document.getElementById("box_option_discount7").style = "display: none";

        document.getElementById("box_option_discount12").style = "display: none";
        document.getElementById("box_option_discount13").style = "display: none";
        document.getElementById("box_option_discount14").style = "display: none";
        document.getElementById("box_option_discount15").style = "display: none";
        document.getElementById("box_option_discount16").style = "display: none";
        document.getElementById("box_option_discount17").style = "display: none";
        document.getElementById("box_option_discount18").style = "display: none";
        document.getElementById("box_option_discount19").style = "display: none";
        document.getElementById("box_option_discount20").style = "display: none";

        
        document.getElementById("box_option_discount21").style = "display: none";
        document.getElementById("box_option_discount22").style = "display: none";
        document.getElementById("box_option_discount23").style = "display: none";
        document.getElementById("box_option_discount24").style = "display: none";
        document.getElementById("box_option_discount25").style = "display: none";
        document.getElementById("box_option_discount26").style = "display: none";
        document.getElementById("box_option_discount27").style = "display: none";
        document.getElementById("box_option_discount28").style = "display: none";
        document.getElementById("box_option_discount29").style = "display: none";
        document.getElementById("box_option_discount30").style = "display: none";
    });
    $("#box_option_discount").click(function (e) {
        e.stopPropagation();
    });
    $("#btn_choose_file_cover").click(function (e) {
        e.stopPropagation();
        $("#coverFile").click();
    });
    $("#btn_choose_file_logo").click(function (e) {
        e.stopPropagation();
        $("#logoFile").click();
    });


    $("#radioDiscount1").click(function (e) {
        document.getElementById("box_discount1").style = "display: block!important";
        document.getElementById("box_discount2").style = "display: none!important";
    });

    $("#radioDiscount2").click(function (e) {
        document.getElementById("box_discount1").style = "display: none!important";
        document.getElementById("box_discount2").style = "display: flex!important";
    });
    $("#del_payment_partner").click(function () {
        $("#payment_partner").val(null).trigger("change");
    });
    $("#del_hotel").click(function () {
        $("#hotel_apply").val(null).trigger("change");
    });
    $("#del_service").click(function () {
        $("#service").val(null).trigger("change");
    });

    $("#btn_term").click(function () {
        var rotate = parseInt(
            $("#icon_term").attr("style").split("rotate(")[1].split("deg)")[0]
        );
        if (rotate === 0) {
            document.getElementById("icon_term").style =
                "transform: rotate(180deg); width: 25px; height: 25px";
        } else {
            document.getElementById("icon_term").style =
                "transform: rotate(0deg); width: 25px; height: 25px";
        }
    });
    $("#btn_condition").click(function () {
        var rotate = parseInt(
            $("#icon_condition").attr("style").split("rotate(")[1].split("deg)")[0]
        );
        if (rotate === 0) {
            document.getElementById("icon_condition").style =
                "transform: rotate(180deg); width: 25px; height: 25px";
        } else {
            document.getElementById("icon_condition").style =
                "transform: rotate(0deg); width: 25px; height: 25px";
        }
    });
    $("#btn_choose_criteria").click(function () {
        var display = $("#box_criteria").css(["display"]);
        if (display.display === "none") {
            document.getElementById("box_criteria").style = "display: block";
        } else {
            document.getElementById("box_criteria").style = "display: none";
        }
    });
    $("#btn_done").click(function (e) {
        e.stopPropagation();
        var display = $("#box_criteria").css(["display"]);
        if (display.display === "none") {
            document.getElementById("box_criteria").style = "display: block";
        } else {
            document.getElementById("box_criteria").style = "display: none";
        }
    });
    $("#open_popover1").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id1").style = "display: block";
        document.getElementById("box_popover_id2").style = "display: none";
        document.getElementById("box_popover_id3").style = "display: none";
        document.getElementById("box_popover_id4").style = "display: none";
        document.getElementById("box_popover_id5").style = "display: none";

    });
    $("#open_popover2").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id2").style = "display: block";

        document.getElementById("box_popover_id1").style = "display: none";
        document.getElementById("box_popover_id3").style = "display: none";
        document.getElementById("box_popover_id4").style = "display: none";
        document.getElementById("box_popover_id5").style = "display: none";
    });
    $("#open_popover3").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id3").style = "display: block";

        document.getElementById("box_popover_id2").style = "display: none";
        document.getElementById("box_popover_id1").style = "display: none";
        document.getElementById("box_popover_id4").style = "display: none";
        document.getElementById("box_popover_id5").style = "display: none";
    });
    $("#open_popover4").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id4").style = "display: block";
        document.getElementById("box_popover_id2").style = "display: none";
        document.getElementById("box_popover_id3").style = "display: none";
        document.getElementById("box_popover_id1").style = "display: none";
        document.getElementById("box_popover_id5").style = "display: none";
    });
    $("#open_popover5").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id5").style = "display: block";

        document.getElementById("box_popover_id2").style = "display: none";
        document.getElementById("box_popover_id3").style = "display: none";
        document.getElementById("box_popover_id4").style = "display: none";
        document.getElementById("box_popover_id1").style = "display: none";
    });
    $("#open_popover_accepted1").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_accepted1").style = "display: block";

        document.getElementById("box_popover_id_accepted2").style = "display: none";
        document.getElementById("box_popover_id_accepted3").style = "display: none";
        document.getElementById("box_popover_id_accepted4").style = "display: none";
        document.getElementById("box_popover_id_accepted5").style = "display: none";
    });
    $("#open_popover_accepted2").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_accepted2").style = "display: block";

        document.getElementById("box_popover_id_accepted1").style = "display: none";
        document.getElementById("box_popover_id_accepted3").style = "display: none";
        document.getElementById("box_popover_id_accepted4").style = "display: none";
        document.getElementById("box_popover_id_accepted5").style = "display: none";
    });
    $("#open_popover_accepted3").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_accepted3").style = "display: block";

        document.getElementById("box_popover_id_accepted2").style = "display: none";
        document.getElementById("box_popover_id_accepted1").style = "display: none";
        document.getElementById("box_popover_id_accepted4").style = "display: none";
        document.getElementById("box_popover_id_accepted5").style = "display: none";
    });
    $("#open_popover_accepted4").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_accepted4").style = "display: block";

        document.getElementById("box_popover_id_accepted2").style = "display: none";
        document.getElementById("box_popover_id_accepted3").style = "display: none";
        document.getElementById("box_popover_id_accepted1").style = "display: none";
        document.getElementById("box_popover_id_accepted5").style = "display: none";
    });
    $("#open_popover_accepted5").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_accepted5").style = "display: block";

        document.getElementById("box_popover_id_accepted2").style = "display: none";
        document.getElementById("box_popover_id_accepted3").style = "display: none";
        document.getElementById("box_popover_id_accepted1").style = "display: none";
        document.getElementById("box_popover_id_accepted4").style = "display: none";
    });
    $("#open_popover_cancle1").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_cancle1").style = "display: block";

        document.getElementById("box_popover_id_cancle2").style = "display: none";
        document.getElementById("box_popover_id_cancle3").style = "display: none";
        document.getElementById("box_popover_id_cancle4").style = "display: none";
        document.getElementById("box_popover_id_cancle5").style = "display: none";
    });
    $("#open_popover_cancle2").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_cancle2").style = "display: block";

        document.getElementById("box_popover_id_cancle1").style = "display: none";
        document.getElementById("box_popover_id_cancle3").style = "display: none";
        document.getElementById("box_popover_id_cancle4").style = "display: none";
        document.getElementById("box_popover_id_cancle5").style = "display: none";
    });
    $("#open_popover_cancle3").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_cancle3").style = "display: block";

        document.getElementById("box_popover_id_cancle2").style = "display: none";
        document.getElementById("box_popover_id_cancle1").style = "display: none";
        document.getElementById("box_popover_id_cancle4").style = "display: none";
        document.getElementById("box_popover_id_cancle5").style = "display: none";
    });
    $("#open_popover_cancle4").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_cancle4").style = "display: block";

        document.getElementById("box_popover_id_cancle2").style = "display: none";
        document.getElementById("box_popover_id_cancle3").style = "display: none";
        document.getElementById("box_popover_id_cancle1").style = "display: none";
        document.getElementById("box_popover_id_cancle5").style = "display: none";
    });
    $("#open_popover_cancle5").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_cancle5").style = "display: block";

        document.getElementById("box_popover_id_cancle2").style = "display: none";
        document.getElementById("box_popover_id_cancle3").style = "display: none";
        document.getElementById("box_popover_id_cancle4").style = "display: none";
        document.getElementById("box_popover_id_cancle1").style = "display: none";
    });
    $("#open_popover_done1").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_done1").style = "display: block";

        document.getElementById("box_popover_id_done2").style = "display: none";
        document.getElementById("box_popover_id_done3").style = "display: none";
        document.getElementById("box_popover_id_done4").style = "display: none";
        document.getElementById("box_popover_id_done5").style = "display: none";
    });
    $("#open_popover_done2").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_done2").style = "display: block";

        document.getElementById("box_popover_id_done1").style = "display: none";
        document.getElementById("box_popover_id_done3").style = "display: none";
        document.getElementById("box_popover_id_done4").style = "display: none";
        document.getElementById("box_popover_id_done5").style = "display: none";
    });
    $("#open_popover_done3").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_done3").style = "display: block";

        document.getElementById("box_popover_id_done2").style = "display: none";
        document.getElementById("box_popover_id_done1").style = "display: none";
        document.getElementById("box_popover_id_done4").style = "display: none";
        document.getElementById("box_popover_id_done5").style = "display: none";
    });
    $("#open_popover_done4").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_done4").style = "display: block";

        document.getElementById("box_popover_id_done2").style = "display: none";
        document.getElementById("box_popover_id_done3").style = "display: none";
        document.getElementById("box_popover_id_done1").style = "display: none";
        document.getElementById("box_popover_id_done5").style = "display: none";
    });
    $("#open_popover_done5").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_done5").style = "display: block";

        document.getElementById("box_popover_id_done2").style = "display: none";
        document.getElementById("box_popover_id_done3").style = "display: none";
        document.getElementById("box_popover_id_done4").style = "display: none";
        document.getElementById("box_popover_id_done1").style = "display: none";
    });
    $("#open_popover_waiting1").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_waiting1").style = "display: block";
        document.getElementById("box_popover_id_waiting2").style = "display: none";
        document.getElementById("box_popover_id_waiting3").style = "display: none";
        document.getElementById("box_popover_id_waiting4").style = "display: none";
        document.getElementById("box_popover_id_waiting5").style = "display: none";
        document.getElementById("box_popover_id_waiting6").style = "display: none";
        document.getElementById("box_popover_id_waiting7").style = "display: none";
        document.getElementById("box_popover_id_waiting8").style = "display: none";
        document.getElementById("box_popover_id_waiting9").style = "display: none";
        document.getElementById("box_popover_id_waiting10").style = "display: none";
    });
    $("#open_popover_waiting2").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_waiting2").style = "display: block";
        document.getElementById("box_popover_id_waiting1").style = "display: none";
        document.getElementById("box_popover_id_waiting3").style = "display: none";
        document.getElementById("box_popover_id_waiting4").style = "display: none";
        document.getElementById("box_popover_id_waiting5").style = "display: none";
        document.getElementById("box_popover_id_waiting6").style = "display: none";
        document.getElementById("box_popover_id_waiting7").style = "display: none";
        document.getElementById("box_popover_id_waiting8").style = "display: none";
        document.getElementById("box_popover_id_waiting9").style = "display: none";
        document.getElementById("box_popover_id_waiting10").style = "display: none";
    });
    $("#open_popover_waiting3").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_waiting3").style = "display: block";
        document.getElementById("box_popover_id_waiting1").style = "display: none";
        document.getElementById("box_popover_id_waiting2").style = "display: none";
        document.getElementById("box_popover_id_waiting4").style = "display: none";
        document.getElementById("box_popover_id_waiting5").style = "display: none";
        document.getElementById("box_popover_id_waiting6").style = "display: none";
        document.getElementById("box_popover_id_waiting7").style = "display: none";
        document.getElementById("box_popover_id_waiting8").style = "display: none";
        document.getElementById("box_popover_id_waiting9").style = "display: none";
        document.getElementById("box_popover_id_waiting10").style = "display: none";
    });
    $("#open_popover_waiting4").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_waiting4").style = "display: block";
        document.getElementById("box_popover_id_waiting1").style = "display: none";
        document.getElementById("box_popover_id_waiting3").style = "display: none";
        document.getElementById("box_popover_id_waiting2").style = "display: none";
        document.getElementById("box_popover_id_waiting5").style = "display: none";
        document.getElementById("box_popover_id_waiting6").style = "display: none";
        document.getElementById("box_popover_id_waiting7").style = "display: none";
        document.getElementById("box_popover_id_waiting8").style = "display: none";
        document.getElementById("box_popover_id_waiting9").style = "display: none";
        document.getElementById("box_popover_id_waiting10").style = "display: none";
    });
    $("#open_popover_waiting5").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_waiting5").style = "display: block";
        document.getElementById("box_popover_id_waiting1").style = "display: none";
        document.getElementById("box_popover_id_waiting3").style = "display: none";
        document.getElementById("box_popover_id_waiting4").style = "display: none";
        document.getElementById("box_popover_id_waiting2").style = "display: none";
        document.getElementById("box_popover_id_waiting6").style = "display: none";
        document.getElementById("box_popover_id_waiting7").style = "display: none";
        document.getElementById("box_popover_id_waiting8").style = "display: none";
        document.getElementById("box_popover_id_waiting9").style = "display: none";
        document.getElementById("box_popover_id_waiting10").style = "display: none";
    });
    $("#open_popover_waiting6").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_waiting6").style = "display: block";
        document.getElementById("box_popover_id_waiting2").style = "display: none";
        document.getElementById("box_popover_id_waiting3").style = "display: none";
        document.getElementById("box_popover_id_waiting4").style = "display: none";
        document.getElementById("box_popover_id_waiting5").style = "display: none";
        document.getElementById("box_popover_id_waiting1").style = "display: none";
        document.getElementById("box_popover_id_waiting7").style = "display: none";
        document.getElementById("box_popover_id_waiting8").style = "display: none";
        document.getElementById("box_popover_id_waiting9").style = "display: none";
        document.getElementById("box_popover_id_waiting10").style = "display: none";
    });
    $("#open_popover_waiting7").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_waiting7").style = "display: block";
        document.getElementById("box_popover_id_waiting1").style = "display: none";
        document.getElementById("box_popover_id_waiting3").style = "display: none";
        document.getElementById("box_popover_id_waiting4").style = "display: none";
        document.getElementById("box_popover_id_waiting5").style = "display: none";
        document.getElementById("box_popover_id_waiting6").style = "display: none";
        document.getElementById("box_popover_id_waiting2").style = "display: none";
        document.getElementById("box_popover_id_waiting8").style = "display: none";
        document.getElementById("box_popover_id_waiting9").style = "display: none";
        document.getElementById("box_popover_id_waiting10").style = "display: none";
    });
    $("#open_popover_waiting8").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_waiting8").style = "display: block";
        document.getElementById("box_popover_id_waiting1").style = "display: none";
        document.getElementById("box_popover_id_waiting2").style = "display: none";
        document.getElementById("box_popover_id_waiting4").style = "display: none";
        document.getElementById("box_popover_id_waiting5").style = "display: none";
        document.getElementById("box_popover_id_waiting6").style = "display: none";
        document.getElementById("box_popover_id_waiting7").style = "display: none";
        document.getElementById("box_popover_id_waiting3").style = "display: none";
        document.getElementById("box_popover_id_waiting9").style = "display: none";
        document.getElementById("box_popover_id_waiting10").style = "display: none";
    });
    $("#open_popover_waiting9").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_waiting9").style = "display: block";
        document.getElementById("box_popover_id_waiting1").style = "display: none";
        document.getElementById("box_popover_id_waiting3").style = "display: none";
        document.getElementById("box_popover_id_waiting2").style = "display: none";
        document.getElementById("box_popover_id_waiting5").style = "display: none";
        document.getElementById("box_popover_id_waiting6").style = "display: none";
        document.getElementById("box_popover_id_waiting7").style = "display: none";
        document.getElementById("box_popover_id_waiting8").style = "display: none";
        document.getElementById("box_popover_id_waiting4").style = "display: none";
        document.getElementById("box_popover_id_waiting10").style = "display: none";
    });
    $("#open_popover_waiting10").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_waiting10").style = "display: block";
        document.getElementById("box_popover_id_waiting1").style = "display: none";
        document.getElementById("box_popover_id_waiting3").style = "display: none";
        document.getElementById("box_popover_id_waiting4").style = "display: none";
        document.getElementById("box_popover_id_waiting2").style = "display: none";
        document.getElementById("box_popover_id_waiting6").style = "display: none";
        document.getElementById("box_popover_id_waiting7").style = "display: none";
        document.getElementById("box_popover_id_waiting8").style = "display: none";
        document.getElementById("box_popover_id_waiting9").style = "display: none";
        document.getElementById("box_popover_id_waiting5").style = "display: none";
    });
    $("#open_popover_accepted1").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_accepted1").style = "display: block";
        document.getElementById("box_popover_id_accepted2").style = "display: none";
        document.getElementById("box_popover_id_accepted3").style = "display: none";
        document.getElementById("box_popover_id_accepted4").style = "display: none";
        document.getElementById("box_popover_id_accepted5").style = "display: none";
        document.getElementById("box_popover_id_accepted6").style = "display: none";
        document.getElementById("box_popover_id_accepted7").style = "display: none";
        document.getElementById("box_popover_id_accepted8").style = "display: none";
        document.getElementById("box_popover_id_accepted9").style = "display: none";
        document.getElementById("box_popover_id_accepted10").style = "display: none";
    });
    $("#open_popover_accepted2").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_accepted2").style = "display: block";
        document.getElementById("box_popover_id_accepted1").style = "display: none";
        document.getElementById("box_popover_id_accepted3").style = "display: none";
        document.getElementById("box_popover_id_accepted4").style = "display: none";
        document.getElementById("box_popover_id_accepted5").style = "display: none";
        document.getElementById("box_popover_id_accepted6").style = "display: none";
        document.getElementById("box_popover_id_accepted7").style = "display: none";
        document.getElementById("box_popover_id_accepted8").style = "display: none";
        document.getElementById("box_popover_id_accepted9").style = "display: none";
        document.getElementById("box_popover_id_accepted10").style = "display: none";
    });
    $("#open_popover_accepted3").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_accepted3").style = "display: block";
        document.getElementById("box_popover_id_accepted2").style = "display: none";
        document.getElementById("box_popover_id_accepted1").style = "display: none";
        document.getElementById("box_popover_id_accepted4").style = "display: none";
        document.getElementById("box_popover_id_accepted5").style = "display: none";
        document.getElementById("box_popover_id_accepted6").style = "display: none";
        document.getElementById("box_popover_id_accepted7").style = "display: none";
        document.getElementById("box_popover_id_accepted8").style = "display: none";
        document.getElementById("box_popover_id_accepted9").style = "display: none";
        document.getElementById("box_popover_id_accepted10").style = "display: none";
    });
    $("#open_popover_accepted4").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_accepted4").style = "display: block";
        document.getElementById("box_popover_id_accepted2").style = "display: none";
        document.getElementById("box_popover_id_accepted3").style = "display: none";
        document.getElementById("box_popover_id_accepted1").style = "display: none";
        document.getElementById("box_popover_id_accepted5").style = "display: none";
        document.getElementById("box_popover_id_accepted6").style = "display: none";
        document.getElementById("box_popover_id_accepted7").style = "display: none";
        document.getElementById("box_popover_id_accepted8").style = "display: none";
        document.getElementById("box_popover_id_accepted9").style = "display: none";
        document.getElementById("box_popover_id_accepted10").style = "display: none";
    });
    $("#open_popover_accepted5").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_accepted5").style = "display: block";
        document.getElementById("box_popover_id_accepted2").style = "display: none";
        document.getElementById("box_popover_id_accepted3").style = "display: none";
        document.getElementById("box_popover_id_accepted4").style = "display: none";
        document.getElementById("box_popover_id_accepted1").style = "display: none";
        document.getElementById("box_popover_id_accepted6").style = "display: none";
        document.getElementById("box_popover_id_accepted7").style = "display: none";
        document.getElementById("box_popover_id_accepted8").style = "display: none";
        document.getElementById("box_popover_id_accepted9").style = "display: none";
        document.getElementById("box_popover_id_accepted10").style = "display: none";
    });
    $("#open_popover_accepted6").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_accepted6").style = "display: block";
        document.getElementById("box_popover_id_accepted2").style = "display: none";
        document.getElementById("box_popover_id_accepted3").style = "display: none";
        document.getElementById("box_popover_id_accepted4").style = "display: none";
        document.getElementById("box_popover_id_accepted5").style = "display: none";
        document.getElementById("box_popover_id_accepted1").style = "display: none";
        document.getElementById("box_popover_id_accepted7").style = "display: none";
        document.getElementById("box_popover_id_accepted8").style = "display: none";
        document.getElementById("box_popover_id_accepted9").style = "display: none";
        document.getElementById("box_popover_id_accepted10").style = "display: none";
    });
    $("#open_popover_accepted7").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_accepted7").style = "display: block";
        document.getElementById("box_popover_id_accepted2").style = "display: none";
        document.getElementById("box_popover_id_accepted3").style = "display: none";
        document.getElementById("box_popover_id_accepted4").style = "display: none";
        document.getElementById("box_popover_id_accepted5").style = "display: none";
        document.getElementById("box_popover_id_accepted6").style = "display: none";
        document.getElementById("box_popover_id_accepted1").style = "display: none";
        document.getElementById("box_popover_id_accepted8").style = "display: none";
        document.getElementById("box_popover_id_accepted9").style = "display: none";
        document.getElementById("box_popover_id_accepted10").style = "display: none";
    });
    $("#open_popover_accepted8").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_accepted8").style = "display: block";
        document.getElementById("box_popover_id_accepted2").style = "display: none";
        document.getElementById("box_popover_id_accepted3").style = "display: none";
        document.getElementById("box_popover_id_accepted4").style = "display: none";
        document.getElementById("box_popover_id_accepted5").style = "display: none";
        document.getElementById("box_popover_id_accepted6").style = "display: none";
        document.getElementById("box_popover_id_accepted7").style = "display: none";
        document.getElementById("box_popover_id_accepted1").style = "display: none";
        document.getElementById("box_popover_id_accepted9").style = "display: none";
        document.getElementById("box_popover_id_accepted10").style = "display: none";
    });
    $("#open_popover_accepted9").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_accepted9").style = "display: block";
        document.getElementById("box_popover_id_accepted2").style = "display: none";
        document.getElementById("box_popover_id_accepted3").style = "display: none";
        document.getElementById("box_popover_id_accepted4").style = "display: none";
        document.getElementById("box_popover_id_accepted5").style = "display: none";
        document.getElementById("box_popover_id_accepted6").style = "display: none";
        document.getElementById("box_popover_id_accepted7").style = "display: none";
        document.getElementById("box_popover_id_accepted8").style = "display: none";
        document.getElementById("box_popover_id_accepted1").style = "display: none";
        document.getElementById("box_popover_id_accepted10").style = "display: none";
    });
    $("#open_popover_accepted10").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_accepted10").style = "display: block";
        document.getElementById("box_popover_id_accepted2").style = "display: none";
        document.getElementById("box_popover_id_accepted3").style = "display: none";
        document.getElementById("box_popover_id_accepted4").style = "display: none";
        document.getElementById("box_popover_id_accepted5").style = "display: none";
        document.getElementById("box_popover_id_accepted6").style = "display: none";
        document.getElementById("box_popover_id_accepted7").style = "display: none";
        document.getElementById("box_popover_id_accepted8").style = "display: none";
        document.getElementById("box_popover_id_accepted9").style = "display: none";
        document.getElementById("box_popover_id_accepted1").style = "display: none";
    });

    $("#open_popover_cancled1").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_cancled1").style = "display: block";
        document.getElementById("box_popover_id_cancled2").style = "display: none";
        document.getElementById("box_popover_id_cancled3").style = "display: none";
        document.getElementById("box_popover_id_cancled4").style = "display: none";
        document.getElementById("box_popover_id_cancled5").style = "display: none";
        document.getElementById("box_popover_id_cancled6").style = "display: none";
        document.getElementById("box_popover_id_cancled7").style = "display: none";
        document.getElementById("box_popover_id_cancled8").style = "display: none";
        document.getElementById("box_popover_id_cancled9").style = "display: none";
        document.getElementById("box_popover_id_cancled10").style = "display: none";
    });

    $("#open_popover_cancled2").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_cancled2").style = "display: block";
        document.getElementById("box_popover_id_cancled1").style = "display: none";
        document.getElementById("box_popover_id_cancled3").style = "display: none";
        document.getElementById("box_popover_id_cancled4").style = "display: none";
        document.getElementById("box_popover_id_cancled5").style = "display: none";
        document.getElementById("box_popover_id_cancled6").style = "display: none";
        document.getElementById("box_popover_id_cancled7").style = "display: none";
        document.getElementById("box_popover_id_cancled8").style = "display: none";
        document.getElementById("box_popover_id_cancled9").style = "display: none";
        document.getElementById("box_popover_id_cancled10").style = "display: none";
    });

    $("#open_popover_cancled3").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_cancled3").style = "display: block";
        document.getElementById("box_popover_id_cancled2").style = "display: none";
        document.getElementById("box_popover_id_cancled1").style = "display: none";
        document.getElementById("box_popover_id_cancled4").style = "display: none";
        document.getElementById("box_popover_id_cancled5").style = "display: none";
        document.getElementById("box_popover_id_cancled6").style = "display: none";
        document.getElementById("box_popover_id_cancled7").style = "display: none";
        document.getElementById("box_popover_id_cancled8").style = "display: none";
        document.getElementById("box_popover_id_cancled9").style = "display: none";
        document.getElementById("box_popover_id_cancled10").style = "display: none";
    });

    $("#open_popover_cancled4").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_cancled4").style = "display: block";
        document.getElementById("box_popover_id_cancled2").style = "display: none";
        document.getElementById("box_popover_id_cancled3").style = "display: none";
        document.getElementById("box_popover_id_cancled1").style = "display: none";
        document.getElementById("box_popover_id_cancled5").style = "display: none";
        document.getElementById("box_popover_id_cancled6").style = "display: none";
        document.getElementById("box_popover_id_cancled7").style = "display: none";
        document.getElementById("box_popover_id_cancled8").style = "display: none";
        document.getElementById("box_popover_id_cancled9").style = "display: none";
        document.getElementById("box_popover_id_cancled10").style = "display: none";
    });

    $("#open_popover_cancled5").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_cancled5").style = "display: block";
        document.getElementById("box_popover_id_cancled2").style = "display: none";
        document.getElementById("box_popover_id_cancled3").style = "display: none";
        document.getElementById("box_popover_id_cancled4").style = "display: none";
        document.getElementById("box_popover_id_cancled1").style = "display: none";
        document.getElementById("box_popover_id_cancled6").style = "display: none";
        document.getElementById("box_popover_id_cancled7").style = "display: none";
        document.getElementById("box_popover_id_cancled8").style = "display: none";
        document.getElementById("box_popover_id_cancled9").style = "display: none";
        document.getElementById("box_popover_id_cancled10").style = "display: none";
    });
    $("#open_popover_cancled6").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_cancled6").style = "display: block";
        document.getElementById("box_popover_id_cancled2").style = "display: none";
        document.getElementById("box_popover_id_cancled3").style = "display: none";
        document.getElementById("box_popover_id_cancled4").style = "display: none";
        document.getElementById("box_popover_id_cancled5").style = "display: none";
        document.getElementById("box_popover_id_cancled1").style = "display: none";
        document.getElementById("box_popover_id_cancled7").style = "display: none";
        document.getElementById("box_popover_id_cancled8").style = "display: none";
        document.getElementById("box_popover_id_cancled9").style = "display: none";
        document.getElementById("box_popover_id_cancled10").style = "display: none";
    });

    $("#open_popover_cancled7").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_cancled7").style = "display: block";
        document.getElementById("box_popover_id_cancled2").style = "display: none";
        document.getElementById("box_popover_id_cancled3").style = "display: none";
        document.getElementById("box_popover_id_cancled4").style = "display: none";
        document.getElementById("box_popover_id_cancled5").style = "display: none";
        document.getElementById("box_popover_id_cancled6").style = "display: none";
        document.getElementById("box_popover_id_cancled1").style = "display: none";
        document.getElementById("box_popover_id_cancled8").style = "display: none";
        document.getElementById("box_popover_id_cancled9").style = "display: none";
        document.getElementById("box_popover_id_cancled10").style = "display: none";
    });

    $("#open_popover_cancled8").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_cancled8").style = "display: block";
        document.getElementById("box_popover_id_cancled2").style = "display: none";
        document.getElementById("box_popover_id_cancled3").style = "display: none";
        document.getElementById("box_popover_id_cancled4").style = "display: none";
        document.getElementById("box_popover_id_cancled5").style = "display: none";
        document.getElementById("box_popover_id_cancled6").style = "display: none";
        document.getElementById("box_popover_id_cancled7").style = "display: none";
        document.getElementById("box_popover_id_cancled1").style = "display: none";
        document.getElementById("box_popover_id_cancled9").style = "display: none";
        document.getElementById("box_popover_id_cancled10").style = "display: none";
    });

    $("#open_popover_cancled9").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_cancled9").style = "display: block";
        document.getElementById("box_popover_id_cancled2").style = "display: none";
        document.getElementById("box_popover_id_cancled3").style = "display: none";
        document.getElementById("box_popover_id_cancled4").style = "display: none";
        document.getElementById("box_popover_id_cancled5").style = "display: none";
        document.getElementById("box_popover_id_cancled6").style = "display: none";
        document.getElementById("box_popover_id_cancled7").style = "display: none";
        document.getElementById("box_popover_id_cancled8").style = "display: none";
        document.getElementById("box_popover_id_cancled1").style = "display: none";
        document.getElementById("box_popover_id_cancled10").style = "display: none";
    });

    $("#open_popover_cancled10").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_cancled10").style = "display: block";
        document.getElementById("box_popover_id_cancled2").style = "display: none";
        document.getElementById("box_popover_id_cancled3").style = "display: none";
        document.getElementById("box_popover_id_cancled4").style = "display: none";
        document.getElementById("box_popover_id_cancled5").style = "display: none";
        document.getElementById("box_popover_id_cancled6").style = "display: none";
        document.getElementById("box_popover_id_cancled7").style = "display: none";
        document.getElementById("box_popover_id_cancled8").style = "display: none";
        document.getElementById("box_popover_id_cancled9").style = "display: none";
        document.getElementById("box_popover_id_cancled1").style = "display: none";
    });
    $("#open_popover_doned1").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_doned1").style = "display: block";
        document.getElementById("box_popover_id_doned2").style = "display: none";
        document.getElementById("box_popover_id_doned3").style = "display: none";
        document.getElementById("box_popover_id_doned4").style = "display: none";
        document.getElementById("box_popover_id_doned5").style = "display: none";
        document.getElementById("box_popover_id_doned6").style = "display: none";
        document.getElementById("box_popover_id_doned7").style = "display: none";
        document.getElementById("box_popover_id_doned8").style = "display: none";
        document.getElementById("box_popover_id_doned9").style = "display: none";
        document.getElementById("box_popover_id_doned10").style = "display: none";
    });
    $("#open_popover_doned2").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_doned2").style = "display: block";
        document.getElementById("box_popover_id_doned1").style = "display: none";
        document.getElementById("box_popover_id_doned3").style = "display: none";
        document.getElementById("box_popover_id_doned4").style = "display: none";
        document.getElementById("box_popover_id_doned5").style = "display: none";
        document.getElementById("box_popover_id_doned6").style = "display: none";
        document.getElementById("box_popover_id_doned7").style = "display: none";
        document.getElementById("box_popover_id_doned8").style = "display: none";
        document.getElementById("box_popover_id_doned9").style = "display: none";
        document.getElementById("box_popover_id_doned10").style = "display: none";
    });
    $("#open_popover_doned3").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_doned3").style = "display: block";
        document.getElementById("box_popover_id_doned2").style = "display: none";
        document.getElementById("box_popover_id_doned1").style = "display: none";
        document.getElementById("box_popover_id_doned4").style = "display: none";
        document.getElementById("box_popover_id_doned5").style = "display: none";
        document.getElementById("box_popover_id_doned6").style = "display: none";
        document.getElementById("box_popover_id_doned7").style = "display: none";
        document.getElementById("box_popover_id_doned8").style = "display: none";
        document.getElementById("box_popover_id_doned9").style = "display: none";
        document.getElementById("box_popover_id_doned10").style = "display: none";
    });
    $("#open_popover_doned4").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_doned4").style = "display: block";
        document.getElementById("box_popover_id_doned2").style = "display: none";
        document.getElementById("box_popover_id_doned3").style = "display: none";
        document.getElementById("box_popover_id_doned1").style = "display: none";
        document.getElementById("box_popover_id_doned5").style = "display: none";
        document.getElementById("box_popover_id_doned6").style = "display: none";
        document.getElementById("box_popover_id_doned7").style = "display: none";
        document.getElementById("box_popover_id_doned8").style = "display: none";
        document.getElementById("box_popover_id_doned9").style = "display: none";
        document.getElementById("box_popover_id_doned10").style = "display: none";
    });
    $("#open_popover_doned5").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_doned5").style = "display: block";
        document.getElementById("box_popover_id_doned2").style = "display: none";
        document.getElementById("box_popover_id_doned3").style = "display: none";
        document.getElementById("box_popover_id_doned4").style = "display: none";
        document.getElementById("box_popover_id_doned1").style = "display: none";
        document.getElementById("box_popover_id_doned6").style = "display: none";
        document.getElementById("box_popover_id_doned7").style = "display: none";
        document.getElementById("box_popover_id_doned8").style = "display: none";
        document.getElementById("box_popover_id_doned9").style = "display: none";
        document.getElementById("box_popover_id_doned10").style = "display: none";
    });

    $("#open_popover_doned6").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_doned6").style = "display: block";
        document.getElementById("box_popover_id_doned2").style = "display: none";
        document.getElementById("box_popover_id_doned3").style = "display: none";
        document.getElementById("box_popover_id_doned4").style = "display: none";
        document.getElementById("box_popover_id_doned5").style = "display: none";
        document.getElementById("box_popover_id_doned1").style = "display: none";
        document.getElementById("box_popover_id_doned7").style = "display: none";
        document.getElementById("box_popover_id_doned8").style = "display: none";
        document.getElementById("box_popover_id_doned9").style = "display: none";
        document.getElementById("box_popover_id_doned10").style = "display: none";
    });
    $("#open_popover_doned7").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_doned7").style = "display: block";
        document.getElementById("box_popover_id_doned2").style = "display: none";
        document.getElementById("box_popover_id_doned3").style = "display: none";
        document.getElementById("box_popover_id_doned4").style = "display: none";
        document.getElementById("box_popover_id_doned5").style = "display: none";
        document.getElementById("box_popover_id_doned6").style = "display: none";
        document.getElementById("box_popover_id_doned1").style = "display: none";
        document.getElementById("box_popover_id_doned8").style = "display: none";
        document.getElementById("box_popover_id_doned9").style = "display: none";
        document.getElementById("box_popover_id_doned10").style = "display: none";
    });
    $("#open_popover_doned8").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_doned8").style = "display: block";
        document.getElementById("box_popover_id_doned2").style = "display: none";
        document.getElementById("box_popover_id_doned3").style = "display: none";
        document.getElementById("box_popover_id_doned4").style = "display: none";
        document.getElementById("box_popover_id_doned5").style = "display: none";
        document.getElementById("box_popover_id_doned6").style = "display: none";
        document.getElementById("box_popover_id_doned7").style = "display: none";
        document.getElementById("box_popover_id_doned1").style = "display: none";
        document.getElementById("box_popover_id_doned9").style = "display: none";
        document.getElementById("box_popover_id_doned10").style = "display: none";
    });
    $("#open_popover_doned9").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_doned9").style = "display: block";
        document.getElementById("box_popover_id_doned2").style = "display: none";
        document.getElementById("box_popover_id_doned3").style = "display: none";
        document.getElementById("box_popover_id_doned4").style = "display: none";
        document.getElementById("box_popover_id_doned5").style = "display: none";
        document.getElementById("box_popover_id_doned6").style = "display: none";
        document.getElementById("box_popover_id_doned7").style = "display: none";
        document.getElementById("box_popover_id_doned8").style = "display: none";
        document.getElementById("box_popover_id_doned1").style = "display: none";
        document.getElementById("box_popover_id_doned10").style = "display: none";
    });
    $("#open_popover_doned10").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_doned10").style = "display: block";
        document.getElementById("box_popover_id_doned2").style = "display: none";
        document.getElementById("box_popover_id_doned3").style = "display: none";
        document.getElementById("box_popover_id_doned4").style = "display: none";
        document.getElementById("box_popover_id_doned5").style = "display: none";
        document.getElementById("box_popover_id_doned6").style = "display: none";
        document.getElementById("box_popover_id_doned7").style = "display: none";
        document.getElementById("box_popover_id_doned8").style = "display: none";
        document.getElementById("box_popover_id_doned9").style = "display: none";
        document.getElementById("box_popover_id_doned1").style = "display: none";
    });
    $("#open_popover_flight_waiting1").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_waiting1").style = "display: block";
        document.getElementById("box_popover_id_flight_waiting2").style = "display: none";
        document.getElementById("box_popover_id_flight_waiting3").style = "display: none";
        document.getElementById("box_popover_id_flight_waiting4").style = "display: none";
        document.getElementById("box_popover_id_flight_waiting5").style = "display: none";
    });

    $("#open_popover_flight_waiting2").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_waiting2").style = "display: block";
        document.getElementById("box_popover_id_flight_waiting1").style = "display: none";
        document.getElementById("box_popover_id_flight_waiting3").style = "display: none";
        document.getElementById("box_popover_id_flight_waiting4").style = "display: none";
        document.getElementById("box_popover_id_flight_waiting5").style = "display: none";
    });

    $("#open_popover_flight_waiting5").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_waiting5").style = "display: block";
        document.getElementById("box_popover_id_flight_waiting1").style = "display: none";
        document.getElementById("box_popover_id_flight_waiting3").style = "display: none";
        document.getElementById("box_popover_id_flight_waiting4").style = "display: none";
        document.getElementById("box_popover_id_flight_waiting2").style = "display: none";
    });

    $("#open_popover_flight_waiting3").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_waiting3").style = "display: block";
        document.getElementById("box_popover_id_flight_waiting1").style = "display: none";
        document.getElementById("box_popover_id_flight_waiting2").style = "display: none";
        document.getElementById("box_popover_id_flight_waiting4").style = "display: none";
        document.getElementById("box_popover_id_flight_waiting5").style = "display: none";
    });

    $("#open_popover_flight_waiting4").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_waiting4").style = "display: block";
        document.getElementById("box_popover_id_flight_waiting1").style = "display: none";
        document.getElementById("box_popover_id_flight_waiting3").style = "display: none";
        document.getElementById("box_popover_id_flight_waiting2").style = "display: none";
        document.getElementById("box_popover_id_flight_waiting5").style = "display: none";
    });

    $("#open_popover_flight_accepte1").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_accepte1").style = "display: block";
        document.getElementById("box_popover_id_flight_accepte2").style = "display: none";
        document.getElementById("box_popover_id_flight_accepte3").style = "display: none";
        document.getElementById("box_popover_id_flight_accepte4").style = "display: none";
        document.getElementById("box_popover_id_flight_accepte5").style = "display: none";
    });


    $("#open_popover_flight_accepte2").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_accepte2").style = "display: block";
        document.getElementById("box_popover_id_flight_accepte1").style = "display: none";
        document.getElementById("box_popover_id_flight_accepte3").style = "display: none";
        document.getElementById("box_popover_id_flight_accepte4").style = "display: none";
        document.getElementById("box_popover_id_flight_accepte5").style = "display: none";
    });


    $("#open_popover_flight_accepte3").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_accepte3").style = "display: block";
        document.getElementById("box_popover_id_flight_accepte2").style = "display: none";
        document.getElementById("box_popover_id_flight_accepte1").style = "display: none";
        document.getElementById("box_popover_id_flight_accepte4").style = "display: none";
        document.getElementById("box_popover_id_flight_accepte5").style = "display: none";
    });


    $("#open_popover_flight_accepte4").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_accepte4").style = "display: block";
        document.getElementById("box_popover_id_flight_accepte2").style = "display: none";
        document.getElementById("box_popover_id_flight_accepte3").style = "display: none";
        document.getElementById("box_popover_id_flight_accepte1").style = "display: none";
        document.getElementById("box_popover_id_flight_accepte5").style = "display: none";
    });


    $("#open_popover_flight_accepte5").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_accepte5").style = "display: block";
        document.getElementById("box_popover_id_flight_accepte2").style = "display: none";
        document.getElementById("box_popover_id_flight_accepte3").style = "display: none";
        document.getElementById("box_popover_id_flight_accepte4").style = "display: none";
        document.getElementById("box_popover_id_flight_accepte1").style = "display: none";
    });
    $("#open_popover_flight_cancle1").click(function (e) {
        e.stopPropagation();
        document.getElementById("open_popover_flight_cancle1").style = "display: block";
        document.getElementById("box_popover_id_flight_cancle2").style = "display: none";
        document.getElementById("box_popover_id_flight_cancle3").style = "display: none";
        document.getElementById("box_popover_id_flight_cancle4").style = "display: none";
        document.getElementById("box_popover_id_flight_cancle5").style = "display: none";
    });

    $("#open_popover_flight_cancle2").click(function (e) {
        e.stopPropagation();
        document.getElementById("open_popover_flight_cancle2").style = "display: block";
        document.getElementById("box_popover_id_flight_cancle1").style = "display: none";
        document.getElementById("box_popover_id_flight_cancle3").style = "display: none";
        document.getElementById("box_popover_id_flight_cancle4").style = "display: none";
        document.getElementById("box_popover_id_flight_cancle5").style = "display: none";
    });

    $("#open_popover_flight_cancle3").click(function (e) {
        e.stopPropagation();
        document.getElementById("open_popover_flight_cancle3").style = "display: block";
        document.getElementById("box_popover_id_flight_cancle2").style = "display: none";
        document.getElementById("box_popover_id_flight_cancle1").style = "display: none";
        document.getElementById("box_popover_id_flight_cancle4").style = "display: none";
        document.getElementById("box_popover_id_flight_cancle5").style = "display: none";
    });

    $("#open_popover_flight_cancle4").click(function (e) {
        e.stopPropagation();
        document.getElementById("open_popover_flight_cancle4").style = "display: block";
        document.getElementById("box_popover_id_flight_cancle2").style = "display: none";
        document.getElementById("box_popover_id_flight_cancle3").style = "display: none";
        document.getElementById("box_popover_id_flight_cancle1").style = "display: none";
        document.getElementById("box_popover_id_flight_cancle5").style = "display: none";
    });

    $("#open_popover_flight_cancle5").click(function (e) {
        e.stopPropagation();
        document.getElementById("open_popover_flight_cancle5").style = "display: block";
        document.getElementById("box_popover_id_flight_cancle2").style = "display: none";
        document.getElementById("box_popover_id_flight_cancle3").style = "display: none";
        document.getElementById("box_popover_id_flight_cancle4").style = "display: none";
        document.getElementById("box_popover_id_flight_cancle1").style = "display: none";
    });

    $("#open_popover_flight_done1").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_done1").style = "display: block";
        document.getElementById("box_popover_id_flight_done2").style = "display: none";
        document.getElementById("box_popover_id_flight_done3").style = "display: none";
        document.getElementById("box_popover_id_flight_done4").style = "display: none";
        document.getElementById("box_popover_id_flight_done5").style = "display: none";
    });
    $("#open_popover_flight_done5").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_done5").style = "display: block";
        document.getElementById("box_popover_id_flight_done2").style = "display: none";
        document.getElementById("box_popover_id_flight_done3").style = "display: none";
        document.getElementById("box_popover_id_flight_done4").style = "display: none";
        document.getElementById("box_popover_id_flight_done1").style = "display: none";
    });
    $("#open_popover_flight_done2").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_done2").style = "display: block";
        document.getElementById("box_popover_id_flight_done1").style = "display: none";
        document.getElementById("box_popover_id_flight_done3").style = "display: none";
        document.getElementById("box_popover_id_flight_done4").style = "display: none";
        document.getElementById("box_popover_id_flight_done5").style = "display: none";
    });
    $("#open_popover_flight_done3").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_done3").style = "display: block";
        document.getElementById("box_popover_id_flight_done2").style = "display: none";
        document.getElementById("box_popover_id_flight_done1").style = "display: none";
        document.getElementById("box_popover_id_flight_done4").style = "display: none";
        document.getElementById("box_popover_id_flight_done5").style = "display: none";
    });
    $("#open_popover_flight_done4").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_done4").style = "display: block";
        document.getElementById("box_popover_id_flight_done2").style = "display: none";
        document.getElementById("box_popover_id_flight_done3").style = "display: none";
        document.getElementById("box_popover_id_flight_done1").style = "display: none";
        document.getElementById("box_popover_id_flight_done5").style = "display: none";
    });
    $("#open_popover_flight_waited1").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_waited1").style = "display: block";
        document.getElementById("box_popover_id_flight_waited2").style = "display: none";
        document.getElementById("box_popover_id_flight_waited3").style = "display: none";
        document.getElementById("box_popover_id_flight_waited4").style = "display: none";
        document.getElementById("box_popover_id_flight_waited5").style = "display: none";
        document.getElementById("box_popover_id_flight_waited6").style = "display: none";
        document.getElementById("box_popover_id_flight_waited7").style = "display: none";
        document.getElementById("box_popover_id_flight_waited8").style = "display: none";
        document.getElementById("box_popover_id_flight_waited9").style = "display: none";
        document.getElementById("box_popover_id_flight_waited10").style = "display: none";
    });
    $("#open_popover_flight_waited2").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_waited2").style = "display: block";
        document.getElementById("box_popover_id_flight_waited1").style = "display: none";
        document.getElementById("box_popover_id_flight_waited3").style = "display: none";
        document.getElementById("box_popover_id_flight_waited4").style = "display: none";
        document.getElementById("box_popover_id_flight_waited5").style = "display: none";
        document.getElementById("box_popover_id_flight_waited6").style = "display: none";
        document.getElementById("box_popover_id_flight_waited7").style = "display: none";
        document.getElementById("box_popover_id_flight_waited8").style = "display: none";
        document.getElementById("box_popover_id_flight_waited9").style = "display: none";
        document.getElementById("box_popover_id_flight_waited10").style = "display: none";
    });
    $("#open_popover_flight_waited3").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_waited3").style = "display: block";
        document.getElementById("box_popover_id_flight_waited2").style = "display: none";
        document.getElementById("box_popover_id_flight_waited1").style = "display: none";
        document.getElementById("box_popover_id_flight_waited4").style = "display: none";
        document.getElementById("box_popover_id_flight_waited5").style = "display: none";
        document.getElementById("box_popover_id_flight_waited6").style = "display: none";
        document.getElementById("box_popover_id_flight_waited7").style = "display: none";
        document.getElementById("box_popover_id_flight_waited8").style = "display: none";
        document.getElementById("box_popover_id_flight_waited9").style = "display: none";
        document.getElementById("box_popover_id_flight_waited10").style = "display: none";
    });
    $("#open_popover_flight_waited4").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_waited4").style = "display: block";
        document.getElementById("box_popover_id_flight_waited2").style = "display: none";
        document.getElementById("box_popover_id_flight_waited3").style = "display: none";
        document.getElementById("box_popover_id_flight_waited1").style = "display: none";
        document.getElementById("box_popover_id_flight_waited5").style = "display: none";
        document.getElementById("box_popover_id_flight_waited6").style = "display: none";
        document.getElementById("box_popover_id_flight_waited7").style = "display: none";
        document.getElementById("box_popover_id_flight_waited8").style = "display: none";
        document.getElementById("box_popover_id_flight_waited9").style = "display: none";
        document.getElementById("box_popover_id_flight_waited10").style = "display: none";
    });
    $("#open_popover_flight_waited5").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_waited5").style = "display: block";
        document.getElementById("box_popover_id_flight_waited2").style = "display: none";
        document.getElementById("box_popover_id_flight_waited3").style = "display: none";
        document.getElementById("box_popover_id_flight_waited4").style = "display: none";
        document.getElementById("box_popover_id_flight_waited1").style = "display: none";
        document.getElementById("box_popover_id_flight_waited6").style = "display: none";
        document.getElementById("box_popover_id_flight_waited7").style = "display: none";
        document.getElementById("box_popover_id_flight_waited8").style = "display: none";
        document.getElementById("box_popover_id_flight_waited9").style = "display: none";
        document.getElementById("box_popover_id_flight_waited10").style = "display: none";
    });
    $("#open_popover_flight_waited6").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_waited6").style = "display: block";
        document.getElementById("box_popover_id_flight_waited2").style = "display: none";
        document.getElementById("box_popover_id_flight_waited3").style = "display: none";
        document.getElementById("box_popover_id_flight_waited4").style = "display: none";
        document.getElementById("box_popover_id_flight_waited5").style = "display: none";
        document.getElementById("box_popover_id_flight_waited1").style = "display: none";
        document.getElementById("box_popover_id_flight_waited7").style = "display: none";
        document.getElementById("box_popover_id_flight_waited8").style = "display: none";
        document.getElementById("box_popover_id_flight_waited9").style = "display: none";
        document.getElementById("box_popover_id_flight_waited10").style = "display: none";
    });
    $("#open_popover_flight_waited7").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_waited7").style = "display: block";
        document.getElementById("box_popover_id_flight_waited2").style = "display: none";
        document.getElementById("box_popover_id_flight_waited3").style = "display: none";
        document.getElementById("box_popover_id_flight_waited4").style = "display: none";
        document.getElementById("box_popover_id_flight_waited5").style = "display: none";
        document.getElementById("box_popover_id_flight_waited6").style = "display: none";
        document.getElementById("box_popover_id_flight_waited1").style = "display: none";
        document.getElementById("box_popover_id_flight_waited8").style = "display: none";
        document.getElementById("box_popover_id_flight_waited9").style = "display: none";
        document.getElementById("box_popover_id_flight_waited10").style = "display: none";
    });
    $("#open_popover_flight_waited8").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_waited8").style = "display: block";
        document.getElementById("box_popover_id_flight_waited2").style = "display: none";
        document.getElementById("box_popover_id_flight_waited3").style = "display: none";
        document.getElementById("box_popover_id_flight_waited4").style = "display: none";
        document.getElementById("box_popover_id_flight_waited5").style = "display: none";
        document.getElementById("box_popover_id_flight_waited6").style = "display: none";
        document.getElementById("box_popover_id_flight_waited7").style = "display: none";
        document.getElementById("box_popover_id_flight_waited1").style = "display: none";
        document.getElementById("box_popover_id_flight_waited9").style = "display: none";
        document.getElementById("box_popover_id_flight_waited10").style = "display: none";
    });
    $("#open_popover_flight_waited9").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_waited9").style = "display: block";
        document.getElementById("box_popover_id_flight_waited2").style = "display: none";
        document.getElementById("box_popover_id_flight_waited3").style = "display: none";
        document.getElementById("box_popover_id_flight_waited4").style = "display: none";
        document.getElementById("box_popover_id_flight_waited5").style = "display: none";
        document.getElementById("box_popover_id_flight_waited6").style = "display: none";
        document.getElementById("box_popover_id_flight_waited7").style = "display: none";
        document.getElementById("box_popover_id_flight_waited8").style = "display: none";
        document.getElementById("box_popover_id_flight_waited1").style = "display: none";
        document.getElementById("box_popover_id_flight_waited10").style = "display: none";
    });
    $("#open_popover_flight_waited10").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_waited10").style = "display: block";
        document.getElementById("box_popover_id_flight_waited2").style = "display: none";
        document.getElementById("box_popover_id_flight_waited3").style = "display: none";
        document.getElementById("box_popover_id_flight_waited4").style = "display: none";
        document.getElementById("box_popover_id_flight_waited5").style = "display: none";
        document.getElementById("box_popover_id_flight_waited6").style = "display: none";
        document.getElementById("box_popover_id_flight_waited7").style = "display: none";
        document.getElementById("box_popover_id_flight_waited8").style = "display: none";
        document.getElementById("box_popover_id_flight_waited1").style = "display: none";
        document.getElementById("box_popover_id_flight_waited9").style = "display: none";
    });
    $("#open_popover_flight_accepted1").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_accepted1").style = "display: block";
        document.getElementById("box_popover_id_flight_accepted2").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted3").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted4").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted5").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted6").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted7").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted8").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted10").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted9").style = "display: none";
    });
    $("#open_popover_flight_accepted2").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_accepted2").style = "display: block";
        document.getElementById("box_popover_id_flight_accepted1").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted3").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted4").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted5").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted6").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted7").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted8").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted10").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted9").style = "display: none";
    });
    $("#open_popover_flight_accepted3").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_accepted3").style = "display: block";
        document.getElementById("box_popover_id_flight_accepted2").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted1").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted4").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted5").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted6").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted7").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted8").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted10").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted9").style = "display: none";
    });
    $("#open_popover_flight_accepted4").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_accepted4").style = "display: block";
        document.getElementById("box_popover_id_flight_accepted2").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted3").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted1").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted5").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted6").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted7").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted8").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted10").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted9").style = "display: none";
    });
    $("#open_popover_flight_accepted5").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_accepted5").style = "display: block";
        document.getElementById("box_popover_id_flight_accepted2").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted3").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted4").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted1").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted6").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted7").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted8").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted10").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted9").style = "display: none";
    });
    $("#open_popover_flight_accepted6").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_accepted6").style = "display: block";
        document.getElementById("box_popover_id_flight_accepted2").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted3").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted4").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted5").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted1").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted7").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted8").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted10").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted9").style = "display: none";
    });
    $("#open_popover_flight_accepted7").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_accepted7").style = "display: block";
        document.getElementById("box_popover_id_flight_accepted2").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted3").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted4").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted5").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted6").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted1").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted8").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted10").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted9").style = "display: none";
    });
    $("#open_popover_flight_accepted8").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_accepted8").style = "display: block";
        document.getElementById("box_popover_id_flight_accepted2").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted3").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted4").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted5").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted6").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted7").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted1").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted10").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted9").style = "display: none";
    });
    $("#open_popover_flight_accepted9").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_accepted9").style = "display: block";
        document.getElementById("box_popover_id_flight_accepted2").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted3").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted4").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted5").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted6").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted7").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted8").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted10").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted1").style = "display: none";
    });
    $("#open_popover_flight_accepted10").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_accepted10").style = "display: block";
        document.getElementById("box_popover_id_flight_accepted2").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted3").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted4").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted5").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted6").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted7").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted8").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted1").style = "display: none";
        document.getElementById("box_popover_id_flight_accepted9").style = "display: none";
    });
    $("#open_popover_flight_cancled1").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_cancled1").style = "display: block";
        document.getElementById("box_popover_id_flight_cancled2").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled3").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled4").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled5").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled6").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled7").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled8").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled9").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled10").style = "display: none";

    });
    $("#open_popover_flight_cancled2").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_cancled2").style = "display: block";
        document.getElementById("box_popover_id_flight_cancled1").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled3").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled4").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled5").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled6").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled7").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled8").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled9").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled10").style = "display: none";

    });
    $("#open_popover_flight_cancled3").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_cancled3").style = "display: block";
        document.getElementById("box_popover_id_flight_cancled2").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled1").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled4").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled5").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled6").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled7").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled8").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled9").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled10").style = "display: none";

    });
    $("#open_popover_flight_cancled4").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_cancled4").style = "display: block";
        document.getElementById("box_popover_id_flight_cancled2").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled3").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled1").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled5").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled6").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled7").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled8").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled9").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled10").style = "display: none";

    });
    $("#open_popover_flight_cancled5").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_cancled5").style = "display: block";
        document.getElementById("box_popover_id_flight_cancled2").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled3").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled4").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled1").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled6").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled7").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled8").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled9").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled10").style = "display: none";

    });
    $("#open_popover_flight_cancled6").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_cancled6").style = "display: block";
        document.getElementById("box_popover_id_flight_cancled2").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled3").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled4").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled5").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled1").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled7").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled8").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled9").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled10").style = "display: none";

    });
    $("#open_popover_flight_cancled7").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_cancled7").style = "display: block";
        document.getElementById("box_popover_id_flight_cancled2").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled3").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled4").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled5").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled6").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled1").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled8").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled9").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled10").style = "display: none";

    });
    $("#open_popover_flight_cancled8").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_cancled8").style = "display: block";
        document.getElementById("box_popover_id_flight_cancled2").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled3").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled4").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled5").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled6").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled7").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled1").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled9").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled10").style = "display: none";

    });
    $("#open_popover_flight_cancled9").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_cancled9").style = "display: block";
        document.getElementById("box_popover_id_flight_cancled2").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled3").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled4").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled5").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled6").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled7").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled8").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled1").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled10").style = "display: none";

    });
    $("#open_popover_flight_cancled10").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_cancled10").style = "display: block";
        document.getElementById("box_popover_id_flight_cancled2").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled3").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled4").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled5").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled6").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled7").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled8").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled9").style = "display: none";
        document.getElementById("box_popover_id_flight_cancled1").style = "display: none";

    });
    $("open_popover_flight_doned1").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_doned1").style = "display: block";
        document.getElementById("box_popover_id_flight_doned2").style = "display: none";
        document.getElementById("box_popover_id_flight_doned3").style = "display: none";
        document.getElementById("box_popover_id_flight_doned4").style = "display: none";
        document.getElementById("box_popover_id_flight_doned5").style = "display: none";
        document.getElementById("box_popover_id_flight_doned6").style = "display: none";
        document.getElementById("box_popover_id_flight_doned7").style = "display: none";
        document.getElementById("box_popover_id_flight_doned8").style = "display: none";
        document.getElementById("box_popover_id_flight_doned9").style = "display: none";
        document.getElementById("box_popover_id_flight_doned10").style = "display: none";

    });

    $("open_popover_flight_doned2").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_doned2").style = "display: block";
        document.getElementById("box_popover_id_flight_doned1").style = "display: none";
        document.getElementById("box_popover_id_flight_doned3").style = "display: none";
        document.getElementById("box_popover_id_flight_doned4").style = "display: none";
        document.getElementById("box_popover_id_flight_doned5").style = "display: none";
        document.getElementById("box_popover_id_flight_doned6").style = "display: none";
        document.getElementById("box_popover_id_flight_doned7").style = "display: none";
        document.getElementById("box_popover_id_flight_doned8").style = "display: none";
        document.getElementById("box_popover_id_flight_doned9").style = "display: none";
        document.getElementById("box_popover_id_flight_doned10").style = "display: none";

    });
    $("open_popover_flight_doned3").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_doned3").style = "display: block";
        document.getElementById("box_popover_id_flight_doned2").style = "display: none";
        document.getElementById("box_popover_id_flight_doned1").style = "display: none";
        document.getElementById("box_popover_id_flight_doned4").style = "display: none";
        document.getElementById("box_popover_id_flight_doned5").style = "display: none";
        document.getElementById("box_popover_id_flight_doned6").style = "display: none";
        document.getElementById("box_popover_id_flight_doned7").style = "display: none";
        document.getElementById("box_popover_id_flight_doned8").style = "display: none";
        document.getElementById("box_popover_id_flight_doned9").style = "display: none";
        document.getElementById("box_popover_id_flight_doned10").style = "display: none";

    });
    $("open_popover_flight_doned4").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_doned4").style = "display: block";
        document.getElementById("box_popover_id_flight_doned2").style = "display: none";
        document.getElementById("box_popover_id_flight_doned3").style = "display: none";
        document.getElementById("box_popover_id_flight_doned1").style = "display: none";
        document.getElementById("box_popover_id_flight_doned5").style = "display: none";
        document.getElementById("box_popover_id_flight_doned6").style = "display: none";
        document.getElementById("box_popover_id_flight_doned7").style = "display: none";
        document.getElementById("box_popover_id_flight_doned8").style = "display: none";
        document.getElementById("box_popover_id_flight_doned9").style = "display: none";
        document.getElementById("box_popover_id_flight_doned10").style = "display: none";

    });
    $("open_popover_flight_doned5").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_doned5").style = "display: block";
        document.getElementById("box_popover_id_flight_doned2").style = "display: none";
        document.getElementById("box_popover_id_flight_doned3").style = "display: none";
        document.getElementById("box_popover_id_flight_doned4").style = "display: none";
        document.getElementById("box_popover_id_flight_doned1").style = "display: none";
        document.getElementById("box_popover_id_flight_doned6").style = "display: none";
        document.getElementById("box_popover_id_flight_doned7").style = "display: none";
        document.getElementById("box_popover_id_flight_doned8").style = "display: none";
        document.getElementById("box_popover_id_flight_doned9").style = "display: none";
        document.getElementById("box_popover_id_flight_doned10").style = "display: none";

    });
    $("open_popover_flight_doned6").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_doned6").style = "display: block";
        document.getElementById("box_popover_id_flight_doned2").style = "display: none";
        document.getElementById("box_popover_id_flight_doned3").style = "display: none";
        document.getElementById("box_popover_id_flight_doned4").style = "display: none";
        document.getElementById("box_popover_id_flight_doned5").style = "display: none";
        document.getElementById("box_popover_id_flight_doned1").style = "display: none";
        document.getElementById("box_popover_id_flight_doned7").style = "display: none";
        document.getElementById("box_popover_id_flight_doned8").style = "display: none";
        document.getElementById("box_popover_id_flight_doned9").style = "display: none";
        document.getElementById("box_popover_id_flight_doned10").style = "display: none";

    });
    $("open_popover_flight_doned7").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_doned7").style = "display: block";
        document.getElementById("box_popover_id_flight_doned2").style = "display: none";
        document.getElementById("box_popover_id_flight_doned3").style = "display: none";
        document.getElementById("box_popover_id_flight_doned4").style = "display: none";
        document.getElementById("box_popover_id_flight_doned5").style = "display: none";
        document.getElementById("box_popover_id_flight_doned6").style = "display: none";
        document.getElementById("box_popover_id_flight_doned1").style = "display: none";
        document.getElementById("box_popover_id_flight_doned8").style = "display: none";
        document.getElementById("box_popover_id_flight_doned9").style = "display: none";
        document.getElementById("box_popover_id_flight_doned10").style = "display: none";

    });
    $("open_popover_flight_doned8").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_doned8").style = "display: block";
        document.getElementById("box_popover_id_flight_doned2").style = "display: none";
        document.getElementById("box_popover_id_flight_doned3").style = "display: none";
        document.getElementById("box_popover_id_flight_doned4").style = "display: none";
        document.getElementById("box_popover_id_flight_doned5").style = "display: none";
        document.getElementById("box_popover_id_flight_doned6").style = "display: none";
        document.getElementById("box_popover_id_flight_doned7").style = "display: none";
        document.getElementById("box_popover_id_flight_doned1").style = "display: none";
        document.getElementById("box_popover_id_flight_doned9").style = "display: none";
        document.getElementById("box_popover_id_flight_doned10").style = "display: none";

    });
    $("open_popover_flight_doned9").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_doned9").style = "display: block";
        document.getElementById("box_popover_id_flight_doned2").style = "display: none";
        document.getElementById("box_popover_id_flight_doned3").style = "display: none";
        document.getElementById("box_popover_id_flight_doned4").style = "display: none";
        document.getElementById("box_popover_id_flight_doned5").style = "display: none";
        document.getElementById("box_popover_id_flight_doned6").style = "display: none";
        document.getElementById("box_popover_id_flight_doned7").style = "display: none";
        document.getElementById("box_popover_id_flight_doned8").style = "display: none";
        document.getElementById("box_popover_id_flight_doned1").style = "display: none";
        document.getElementById("box_popover_id_flight_doned10").style = "display: none";

    });
    $("open_popover_flight_doned10").click(function (e) {
        e.stopPropagation();
        document.getElementById("box_popover_id_flight_doned10").style = "display: block";
        document.getElementById("box_popover_id_flight_doned2").style = "display: none";
        document.getElementById("box_popover_id_flight_doned3").style = "display: none";
        document.getElementById("box_popover_id_flight_doned4").style = "display: none";
        document.getElementById("box_popover_id_flight_doned5").style = "display: none";
        document.getElementById("box_popover_id_flight_doned6").style = "display: none";
        document.getElementById("box_popover_id_flight_doned7").style = "display: none";
        document.getElementById("box_popover_id_flight_doned8").style = "display: none";
        document.getElementById("box_popover_id_flight_doned9").style = "display: none";
        document.getElementById("box_popover_id_flight_doned1").style = "display: none";

    }); 

    $("#box_popover_id_done1").click(function (e) {
        e.stopPropagation();
    });
    $("#box_popover_id_done2").click(function (e) {
        e.stopPropagation();
    });
    $("#box_popover_id_done3").click(function (e) {
        e.stopPropagation();
    });
    $("#box_popover_id_done4").click(function (e) {
        e.stopPropagation();
    });
    $("#box_popover_id_done5").click(function (e) {
        e.stopPropagation();
    });
    $("#box_popover_id_cancle1").click(function (e) {
        e.stopPropagation();
    });
    $("#box_popover_id_cancle2").click(function (e) {
        e.stopPropagation();
    });
    $("#box_popover_id_cancle3").click(function (e) {
        e.stopPropagation();
    });
    $("#box_popover_id_cancle4").click(function (e) {
        e.stopPropagation();
    });
    $("#box_popover_id_cancle5").click(function (e) {
        e.stopPropagation();
    });
    $("#box_popover_id_accepted5").click(function (e) {
        e.stopPropagation();
    });
    $("#box_popover_id_accepted4").click(function (e) {
        e.stopPropagation();
    });
    $("#box_popover_id_accepted3").click(function (e) {
        e.stopPropagation();
    });
    $("#box_popover_id_accepted2").click(function (e) {
        e.stopPropagation();
    });
    $("#box_popover_id_accepted1").click(function (e) {
        e.stopPropagation();
    });
    $("#box_popover_id1").click(function (e) {
        e.stopPropagation();
    });
    $("#box_popover_id2").click(function (e) {
        e.stopPropagation();
    });
    $("#box_popover_id3").click(function (e) {
        e.stopPropagation();
    });
    $("#box_popover_id4").click(function (e) {
        e.stopPropagation();
    });
    $("#box_popover_id5").click(function (e) {
        e.stopPropagation();
    });
    $("#buttonWaiting").click(function () {
        var rotate = parseInt(
            $("#iconWaiting").attr("style").split("rotate(")[1].split("deg)")[0]
        );
        if (rotate === 0) {
            $("#collapseWaiting").removeClass("show");
            document.getElementById("iconWaiting").style =
                "color: #888888; cursor: pointer; transform: rotate(180deg);";
        } else {
            document.getElementById("iconWaiting").style =
                "color: #888888; cursor: pointer; transform: rotate(0deg);";
        }
    });





    $("#buttonAccepted").click(function () {
        var rotate = parseInt(
            $("#iconAccepted").attr("style").split("rotate(")[1].split("deg)")[0]
        );
        if (rotate === 0) {
            $("#collapseAccepted").removeClass("show");
            document.getElementById("iconAccepted").style =
                "color: #888888; cursor: pointer; transform: rotate(180deg);";
        } else {
            document.getElementById("iconAccepted").style =
                "color: #888888; cursor: pointer; transform: rotate(0deg);";
        }
    });
    $("#buttonCancelled").click(function () {
        var rotate = parseInt(
            $("#iconCancelled").attr("style").split("rotate(")[1].split("deg)")[0]
        );
        if (rotate === 0) {
            $("#collapseCancelled").removeClass("show");
            document.getElementById("iconCancelled").style =
                "color: #888888; cursor: pointer; transform: rotate(180deg);";
        } else {
            document.getElementById("iconCancelled").style =
                "color: #888888; cursor: pointer; transform: rotate(0deg);";
        }
    });
    $("#buttonDone").click(function () {
        var rotate = parseInt(
            $("#iconDone").attr("style").split("rotate(")[1].split("deg)")[0]
        );
        if (rotate === 0) {
            $("#collapseDone").removeClass("show");
            document.getElementById("iconDone").style =
                "color: #888888; cursor: pointer; transform: rotate(180deg);";
        } else {
            document.getElementById("iconDone").style =
                "color: #888888; cursor: pointer; transform: rotate(0deg);";
        }
    });
});


$(document).ready(function () {

    $("#hotel_serach").keyup(function (){
        var query =$(this).val();
        if(query!=''){
            $.ajax({
                url:"https://test.heyo.group/autocomplect_ajax_search_hotel",
                method:"get",
                data:{query:query},
                success:function (data){
                    $("#hotel_serach_ajax").fadeIn();
                    $("#hotel_serach_ajax").html(data);

                }
            });
        }else {
            $("#hotel_serach_ajax").fadeOut();

        }

    });
    $(document).on('click','li',function (){
        $("#hotel_serach").val($(this).text());
        $("#hotel_serach_ajax").fadeOut();

    });
    $("#locations_serach").keyup(function (){
        var query =$(this).val();
        if(query!=''){

            $.ajax({
                url:"https://test.heyo.group/autocomplect_ajax_search_location",
                method:"get",
                data:{query:query},
                success:function (data){
                    console.log(data);
                    $("#location_serach_ajax").fadeIn();
                    $("#location_serach_ajax").html(data);

                }
            });
        }else {
            $("#location_serach_ajax").fadeOut();

        }

    });
    $(document).on('click','p',function (){
        $("#locations_serach").val($(this).text());

        $("#location_serach_ajax").fadeOut();

    });
    $(".box_choose_criteria").click(function (e) {
        e.stopPropagation();
    });
    // $("#button_done_criteria").click(function (e) {
    //     var atLeastOneIsChecked = $('input[name="chk[]"]:checked').length > 0;
    //     console.log($('input[name="chk[]"]:checked'))
    // });
    $("#button_done_criteria").click(function (e) {
        $(".dropdown.open").removeClass("open");
    });

    $(".js-select2-multi").select2();
    $("#toggle-state-switch").change(function () {
        console.log("first");
    });



});




$(document).ready(function () {


    $('#create_fromto').addClass('none');
    $('#phone_customer').addClass('none');
    $('#name_hotel').addClass('none');
    $('#location').addClass('none');
    $('#nhanvienphutrach').addClass('none');
    $('#partner').addClass('none');
    $('#revice_fromto').addClass('none');


    $("#datetimepicker1_search").datetimepicker({
        format: "YYYY-MM-DD HH:mm:ss ",});
    $("#datetimepicker2_search").datetimepicker({
        format: "YYYY-MM-DD HH:mm:ss",});

   $("#datetimepicker5").datetimepicker({
        format: "DD-MM-YYYY LT",
        minDate: new Date(),
    });  $("#datetimepicker6").datetimepicker({
        format: "DD-MM-YYYY LT",
        minDate: new Date(),
    });
})





$(document).ready(function () {
    $("#search_button_collapse_policy").click(function () {
        var rotate = parseInt(
            $("#search_button_collapse_policy_icon")
                .attr("style")
                .split("rotate(")[1]
                .split("deg)")[0]
        );
        if (rotate === 0) {
            $("#search_box_collapse_policy").removeClass("show_collapse");
            $("#search_box_collapse_policy").addClass("hide_collapse");
            document.getElementById("search_button_collapse_policy_icon").style =
                "transform: rotate(180deg);";
        } else {
            $("#search_box_collapse_policy").removeClass("hide_collapse");
            $("#search_box_collapse_policy").addClass("show_collapse");
            document.getElementById("search_button_collapse_policy_icon").style =
                "transform: rotate(0deg);";
        }
    });
    $("#search_button_collapse_price_per_night").click(function () {
        var rotate = parseInt(
            $("#search_button_collapse_price_per_night_icon")
                .attr("style")
                .split("rotate(")[1]
                .split("deg)")[0]
        );
        if (rotate === 0) {
            $("#search_box_collapse_price_per_night").removeClass("show_collapse");
            $("#search_box_collapse_price_per_night").addClass("hide_collapse");
            document.getElementById(
                "search_button_collapse_price_per_night_icon"
            ).style = "transform: rotate(180deg);";
        } else {
            $("#search_box_collapse_price_per_night").removeClass("hide_collapse");
            $("#search_box_collapse_price_per_night").addClass("show_collapse");
            document.getElementById(
                "search_button_collapse_price_per_night_icon"
            ).style = "transform: rotate(0deg);";
        }
    });
    $("#search_button_collapse_free_breakfast").click(function () {
        var rotate = parseInt(
            $("#search_button_collapse_free_breakfast_icon")
                .attr("style")
                .split("rotate(")[1]
                .split("deg)")[0]
        );
        if (rotate === 0) {
            $("#search_box_collapse_free_breakfast").removeClass("show_collapse");
            $("#search_box_collapse_free_breakfast").addClass("hide_collapse");
            document.getElementById(
                "search_button_collapse_free_breakfast_icon"
            ).style = "transform: rotate(180deg);";
        } else {
            $("#search_box_collapse_free_breakfast").removeClass("hide_collapse");
            $("#search_box_collapse_free_breakfast").addClass("show_collapse");
            document.getElementById(
                "search_button_collapse_free_breakfast_icon"
            ).style = "transform: rotate(0deg);";
        }
    });
    $("#search_button_collapse_quantity_star").click(function () {
        var rotate = parseInt(
            $("#search_button_collapse_quantity_star_icon")
                .attr("style")
                .split("rotate(")[1]
                .split("deg)")[0]
        );
        if (rotate === 0) {
            $("#search_box_collapse_quantity_star").removeClass("show_collapse");
            $("#search_box_collapse_quantity_star").addClass("hide_collapse");
            document.getElementById(
                "search_button_collapse_quantity_star_icon"
            ).style = "transform: rotate(180deg);";
        } else {
            $("#search_box_collapse_quantity_star").removeClass("hide_collapse");
            $("#search_box_collapse_quantity_star").addClass("show_collapse");
            document.getElementById(
                "search_button_collapse_quantity_star_icon"
            ).style = "transform: rotate(0deg);";
        }
    });
    $("#search_button_collapse_convenient").click(function () {
        var rotate = parseInt(
            $("#search_button_collapse_convenient_icon")
                .attr("style")
                .split("rotate(")[1]
                .split("deg)")[0]
        );
        if (rotate === 0) {
            $("#search_box_collapse_convenient").removeClass("show_collapse");
            $("#search_box_collapse_convenient").addClass("hide_collapse");
            document.getElementById("search_button_collapse_convenient_icon").style =
                "transform: rotate(180deg);";
        } else {
            $("#search_box_collapse_convenient").removeClass("hide_collapse");
            $("#search_box_collapse_convenient").addClass("show_collapse");
            document.getElementById("search_button_collapse_convenient_icon").style =
                "transform: rotate(0deg);";
        }
    });

    $("#box_dropdown_sort_result_search_hotel").click(function (e) {
        e.stopPropagation();
    });
});


