<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use DB;

class Location extends Model
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table="locations";
    protected $primaryKey  = "id";

    const UPDATED_AT = null;
    const CREATED_AT = null;
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function scopeLocation($query, $request,$request2,$request3,$request4,$request5,$request6)
    {
      
        if($request!=null){
            
          $query->where('name_vi',$request);
        }
        if($request2!=null){
          $query->where('code_location',$request2);

        }
        
        if($request3!=null){
            $query->where('phone_code',$request3);
  
          }
          
        if($request4!=null){
            $query->where('status',$request4);
  
          }          
          
          if($request5!=null||$request5==0){
            
              $query->where('type_id',$request5);
    
            }
            if($request6!=null){
                $query->where('parent_id',$request6);
      
              }


            
            
        return $query;

    }
    public function scopeLocation2($query, $request,$request2,$request3,$request4,$request5)
    {
        if($request!=null){
            
          $query->where('name_vi',$request);
        }
        if($request2!=null){
          $query->where('code_location',$request2);

        }
        
        if($request3!=null){
            $query->where('phone_code',$request3);
  
          }
          
        if($request4!=null){
          $query->where('status',$request4);

        }    
        if($request5!=null||$request5==0){
          
            $query->where('parent_id',$request5);
  
          }    
        
        
          


            
            
        return $query;

    }public function scopeLocation3($query, $request,$request2,$request3,$request4)
    {
        if($request!=null){
            
          $query->where('name_vi',$request);
        }
        if($request2!=null){
          $query->where('code_location',$request2);

        }
        
        if($request3!=null){
            $query->where('parent_id',$request3);
  
          }
          
        if($request4!=null){
          $query->where('status',$request4);

        }    
        $query->where('type_id',1);

        
          


            
            
        return $query;

    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
