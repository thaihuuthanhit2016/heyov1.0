<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use DB;

class Promotion extends Model
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table="tbl_promotion";
    protected $primaryKey  = "id_promo";

    const UPDATED_AT = null;
    const CREATED_AT = null;
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function scopeFilterprogram_code_promo($query, $request,$request2,$request3,$request4,$request5,$request6,$request7,$request8,$request9)
    {
        if($request!=null){
          $query->where('program_code_promo',$request);
        }
        if($request2!=null){
          $query->where('code_promotion',$request2);

        }
        
        if($request3!=null){
            $query->where('type_promo',$request3);
  
          }
          
        if($request4!=null){
            $query->where('status',$request4);
  
          }
          if($request5!=null){
              $query->where('nation',$request5);
    
            }

            if($request6!=null && $request7!=null){
                $data=DB::table('tbl_promotion')->get();
                    foreach($data as $dt){
                        $data_start=strtotime($dt->date_start);
                        if($data_start>=strtotime($request6) && $data_start<=strtotime($request7) ){
                            $query->where('date_start', '>=', $request6)->where('date_start', '<=', $request7);

                        }

                     
                }

            }

          if($request8!=null && $request9!=null){
            $data=DB::table('tbl_promotion')->get();
            foreach($data as $dt){
                $data_end=strtotime($dt->date_end);
                if($data_end>=strtotime($request8) && $data_end<=strtotime($request9) ){
                    $query->where('date_end', '>=', $request8)->where('date_end', '<=', $request9);

                }

             
        }
            }
            

            if($request6!=null && $request7!=null && $request8!=null && $request9!=null){
               
                 
                        $query->where('date_start', '>=', $request6);
                        $query->where('date_end', '<=', $request9);


                     
            }


            
            
        return $query;

    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
