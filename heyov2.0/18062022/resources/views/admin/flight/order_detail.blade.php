<!DOCTYPE html>
<html lang="en">

<head>
    <title>HF Admin</title>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="{{asset('public/admin/css/flight-detail.css')}}" />
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="https://code.iconify.design/2/2.2.1/iconify.min.js"></script>
    <script src="https://unpkg.com/@popperjs/core@2"></script>
    <link rel='stylesheet'
        href='https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css'>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js'></script>
    <script
        src='https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js'></script>
    <script></script>
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css"
        rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
    <script src="{{asset('public/admin/js/main.js')}}"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <style>
        .toggle.ios,
        .toggle-on.ios,
        .toggle-off.ios {
            border-radius: 20rem;
        }

        .toggle.ios .toggle-handle {
            border-radius: 20rem;
        }
    </style>
    <script>
        $(document).ready(function () {
            $("#buttonCheckin").click();
        })
    </script>
</head>

<body>
    <div class="menu_header">
        <div class="left">
            <a href="#" class="logo">HEYO TRIP</a>
        </div>
        <div class="right">
            <span class="name_menu"> Chi tiết đơn hàng # {{$detail_flight->order_code}}</span>
            <div class="box_admin">
                <span class="iconify" style="width: 25px; height: 25px; color: #032044"
                    data-icon="ic:baseline-notifications-none"></span>
                <div style="color: white;background-color: red" class="badge badge-danger counter">9</div>
                <div id="btn_admin" class="wrapper_admin">
                    <span style="width: 25px; height: 25px; color: #032044" class="iconify"
                        data-icon="lucide:user"></span>
                    <span class="admin">Admin</span>
                    <span style="
                width: 15px;
                height: 15px;
                color: #032044;
                margin-left: 10px;
              " class="iconify" data-icon="ant-design:caret-down-filled"></span>
                </div>
            </div>
        </div>
        <div id="profile" style="box-shadow: 10;" class="box_profile">
            <span style="width: 30px; height: 30px;color: #032044;" class="iconify"
                data-icon="simple-line-icons:logout"></span>
            <span class="logout">Đăng xuất</span>
        </div>
    </div>
    <div style="display: flex">
        <div class="menu_left">
            <div class="top">
                <a href="{{asset('admin/hotel/order/all/1')}}" style="text-decoration: none" class="item">
                    <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                        data-icon="icon-park-outline:hotel"></span>
                    <span style="color: #526a87" class="name_menu">Khách sạn</span>
                </a>
                <a href="{{asset('admin/flight/order/all/1')}}" style="background-color: #f2f9ff; text-decoration: none" class="item">
                    <span class="iconify" style="color: #526a87; width: 30px; height: 30px" data-icon="ps:plane"></span>
                    <span class="name_menu">Vé máy bay</span>
                </a>
                <a href="#" style="text-decoration: none" class="item">
                    <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                        data-icon="clarity:car-line"></span>
                    <span style="color: #526a87" class="name_menu">Thuê xe</span>
                </a>
                <a href="#" style="text-decoration: none" class="item">
                    <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                        data-icon="clarity:data-cluster-line"></span>
                    <span style="color: #526a87" class="name_menu">Crypto</span>
                </a>
            </div>
            <div class="top">
                <a href="#" style="text-decoration: none" class="item">
                    <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                        data-icon="ep:user-filled"></span>
                    <span style="color: #526a87" class="name_menu">Tài khoản</span>
                </a>
                <a href="#" style="text-decoration: none" class="item">
                    <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                        data-icon="dashicons:welcome-widgets-menus"></span>
                    <span style="color: #526a87" class="name_menu">Danh mục</span>
                </a>
                <a href="#" style="text-decoration: none" class="item">
                    <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                        data-icon="gg:website"></span>
                    <span style="color: #526a87" class="name_menu">Website</span>
                </a>
                <a href="{{asset('admin/website/list/quocgia')}}" style="text-decoration: none" class="item">
                    <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                        data-icon="material-symbols:settings-suggest-outline-sharp"></span>
                    <span style="color: #526a87" class="name_menu">Cấu hình</span>
                </a>
            </div>
        </div>
        <div class="menu_right">
            <a href="#" class="item">
                <span style="width: 25px; height: 25px; color: #032044" class="iconify"
                    data-icon="majesticons:analytics"></span>
                <span class="name_menu">Dashboard</span>
            </a>
            <a href="#" class="item">
                <span style="width: 25px; height: 25px; color: #032044" class="iconify" data-icon="mdi:bed"></span>
                <span class="name_menu">Tìm - Đặt vé máy bay</span>
            </a>
            <a href="{{asset('admin/flight/order/all/1')}}" class="item">
                <span style="width: 25px; height: 25px; color: #329223" class="iconify"
                    data-icon="fa-solid:calendar-day"></span>
                <span class="name_menu" style="color: #329223; font-weight: 800">Quản lý đơn đặt vé</span>
            </a>
            <a href="#" href="#" class="item">
                <span style="width: 25px; height: 25px; color: #032044" class="iconify"
                    data-icon="fa-solid:money-check-alt"></span>
                <span class="name_menu">Tuỳ chỉnh giá vé</span>
            </a>
            <a href="{{asset('admin/hotel/list')}}" class="item">
                <span style="width: 25px; height: 25px; color: #032044" class="iconify"
                    data-icon="icomoon-free:price-tags"></span>
                <span class="name_menu">Chương trình khuyến mãi</span>
            </a>
            <a href="#" class="item">
                <span style="width: 25px; height: 25px; color: #032044" class="iconify"
                    data-icon="eva:percent-fill"></span>
                <span class="name_menu">Quản lý hoa hồng</span>
            </a>
            <a href="#" class="item">
                <span style="width: 25px; height: 25px; color: #032044" class="iconify"
                    data-icon="fa6-solid:hand-holding-dollar"></span>
                <span class="name_menu">Huỷ - Hoàn tiền</span>
            </a>
            <a href="#" class="item">
                <span style="width: 25px; height: 25px; color: #032044" class="iconify"
                    data-icon="fluent:news-20-filled"></span>
                <span class="name_menu">Bài viết Marketing</span>
            </a>
            <div data-toggle="collapse" data-target="#collapseInsurance" aria-expanded="true"
                aria-controls="collapseInsurance" class="item" style="justify-content: space-between;cursor: pointer;">
                <div id="menuInsurance" style="display: flex; align-items: center;">
                    <span style="width: 25px; height: 25px; color: #032044" class="iconify"
                        data-icon="bi:shield-shaded"></span>
                    <span class="name_menu">Quản lý bảo hiểm</span>
                </div>
                <span id="iconInsurance" style="width: 15px; height: 15px; color: #032044; transform: rotate(0deg);"
                    class="iconify" data-icon="ant-design:caret-up-filled"></span>
            </div>
            <div id="collapseInsurance" class="collapse">
                <div class="item_child">
                    <span style="width: 15px; height: 15px; color: #032044" class="iconify"
                        data-icon="ant-design:caret-right-filled"></span>
                    <span class="name_item_child">Danh sách hợp đồng BH</span>
                </div>
                <div class="item_child">
                    <span style="width: 15px; height: 15px; color: #032044" class="iconify"
                        data-icon="ant-design:caret-right-filled"></span>
                    <span class="name_item_child">Đối soát hợp đồng BH</span>
                </div>
                <div class="item_child">
                    <span style="width: 15px; height: 15px; color: #032044" class="iconify"
                        data-icon="ant-design:caret-right-filled"></span>
                    <span class="name_item_child">Thanh toán đối tác BH</span>
                </div>
            </div>
        </div>
        <div class="box_content">
            <div class="box_left">
                <div class="wrapper">
                    <div class="box_title">
                        <div class="title">Thông tin đơn hàng</div>
                        <div class="box_option">
                            <button class="button_confirm">XN thanh toán</button>
                            <button class="button_cancel">Hủy đơn</button>
                        </div>
                    </div>
                    <div class="divider"></div>
                    <div class="row_info">
                        <div class="box_info">
                            <div class="label">Mã đơn hàng:</div>
                            <div class="info_black">{{$detail_flight->order_code}}</div>
                        </div>
                        <div class="box_info">
                            <div class="label">Trạng thái đơn hàng:</div>
                            <div class=">@if($detail_flight->order_status_id==1||$detail_flight->order_status_id==4||$detail_flight->order_status_id==6) info_red  @elseif($detail_flight->order_status_id==3||$detail_flight->order_status_id==5) info_green @endif">@if($detail_flight->order_status_id==1)Chờ xử lý @elseif($detail_flight->order_status_id==3) Đã xuất vé  @elseif($detail_flight->order_status_id==4) Đã hủy  @elseif($detail_flight->order_status_id==5) Đã hoàn thành  @elseif($detail_flight->order_status_id==6) Thất bại @endif  </div>
                        </div>
                    </div>
                    <div class="row_info">
                        <div class="box_info">
                            <div class="label">Ngày tạo:</div>
                            <div class="info_black">{{$detail_flight->created_at}}</div>
                        </div>
                        <div class="box_info">
                            <div class="label">Lý do cần xử lý:</div>
                            <div class="@if($detail_flight->order_status_id==1) info_red @else info_green @endif">@if($detail_flight->order_status_id==1)Xuất vé thất bại @else Xuất vé thành công @endif </div>
                        </div>
                    </div>
                    <div class="row_info">
                        <div class="box_info">
                            <div class="label">Người tạo:</div>
                            <div class="info_black">{{$detail_flight->first_name}} {{$detail_flight->last_name}}</div>
                        </div>
                        <div class="box_info">
                            <div class="label">Trạng thái thanh toán:</div>
                            <div class="@if($detail_flight->payment_status_id==0||$detail_flight->payment_status_id==2||$detail_flight->payment_status_id==3||$detail_flight->payment_status_id==4) info_red @else info_green @endif">@if($detail_flight->payment_status_id==0)Chưa thanh toán @elseif($detail_flight->payment_status_id==1) Đã thanh toán  @elseif($detail_flight->payment_status_id==2) Thanh toán thất bại  @elseif($detail_flight->payment_status_id==3) Hết hạn thanh toán  @elseif($detail_flight->payment_status_id==4) Đang hoàn hủy  @elseif($detail_flight->payment_status_id==5) Đã hoàn tiền @endif  </div>
                        </div>
                    </div>
                    <div class="row_info">
                        <div class="box_info">
                            <div class="label">Ngày cập nhật:</div>
                            <div class="info_black">{{$detail_flight->updated_at}}</div>
                        </div>
                        <div class="box_info">
                            <div class="label">Người phụ trách:</div>
                            <div class="info_black">CS01 - Hương -- update version next</div>
                        </div>
                    </div>
                    <div style="justify-content: space-between;" class="row_info">
                        <div class="info_black">Ghi chú</div>
                        <button class="button_edit">
                            <span style="color: #0770CD; width: 20px; height: 20px;" class="iconify"
                                data-icon="ci:edit"></span>
                            <span>Chỉnh sửa</span>
                        </button>
                    </div>
                    <div class="box_note">đơn hàng chuyển giao từ CS03 - Minh sang cho CS01 Hương -- update version next</div>
                </div>
                <div style="margin-top: 10px;" class="wrapper">
                    <div style="padding: 5px 0px" class="box_title">
                        <div class="title">Thông tin liên hệ</div>
                    </div>
                    <div class="divider"></div>
                    <div class="row_info">
                        <div class="box_info">
                            <div class="label">Tên - Họ:</div>
                            <div class="info_black">{{$detail_flight->first_name}} {{$detail_flight->last_name}}</div>
                        </div>
                        <div class="box_info">
                            <div class="label">Email:</div>
                            <div class="info_black">{{$detail_flight->email}}</div>
                        </div>
                    </div>
                    <div class="row_info">
                        <div class="box_info">
                            <div class="label">Giới tính:</div>
                            <div class="info_black">@if($detail_flight->gender_id==1)Nam @else Nữ @endif</div>
                        </div>
                        <div class="box_info">
                            <div class="label">Số điện thoại:</div>
                            <div class="info_black">{{$detail_flight->phone_number}}</div>
                        </div>
                    </div>
                    <div class="row_info">
                        <div style="width: 20%;" class="label">Địa chỉ</div>
                        <div style="width: 80%;" class="info_black">{{$detail_flight->address}}</div>
                    </div>
                </div>
                <?php
                    $detail_flight_info=DB::table('flight_order_details')->where('order_id',$detail_flight->id)->get();
                    $i=0;
                ?>
                <div style="margin-top: 10px;" class="wrapper">
                    <div style="padding: 5px 0px" class="box_title">
                        <div class="title">Thông tin chuyến bay</div>
                    </div>
                    <div class="divider"></div>
                    @foreach($detail_flight_info as $dfi)
                    <?php
                    $i++;
                    ?>
                    <div class="box_hotel">
                        <div style="width: 100%; display: flex; justify-content: space-between;">
                            <div>
                                <div class="label_upper">
                                    Chuyến bay {{$i}}
                                </div>
                                <div class="flight_name">{{$dfi->start_point}} - {{$dfi->end_point}}</div>
                                <div>
                                    <span class="label">Trạng thái đặt chỗ:</span>
                                    <span  class="info_green">Đã xuất vé  -- update version next</span>

                                </div>
                            </div>
                            <div style="text-align: right;">
                                <div class="dropdown">
                                    <button id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                        aria-expanded="false" type="button" class="btn button_option">
                                        Tùy chọn
                                        <span data-icon="ant-design:caret-down-filled"
                                            style="color: #888888; width: 15px; height: 15px;margin-left: 10px;"
                                            class="iconify"></span>
                                    </button>
                                    <div id="box_option_flight_detail" class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <div>
                                            <button type="button" class="btn">Xuất vé</button>
                                        </div>
                                        <div>
                                            <button type="button" class="btn">Huỷ chuyến bay</button>
                                        </div>
                                        <div>
                                            <button type="button" class="btn">Đổi chuyến bay</button>
                                        </div>
                                        <div>
                                            <button type="button" class="btn">Huỷ thông tin hành khách</button>
                                        </div>
                                        <div>
                                            <button type="button" class="btn">Đổi thông tin hành khách</button>
                                        </div>
                                        <div>
                                            <button type="button" class="btn">Mua thêm hành lý</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="name_gray">Tổng giá vé</div>
                                <div class="flight_name">đ {{number_format($dfi->total_price,0,',','.')}}</div>
                            </div>
                        </div>
                        <div class="box_checkin">
                            <div>
                                <div style="font-weight: bold; text-align: right;" class="name_green">Mã đặt chỗ:
                                    {{$dfi->booking_code}}</div>
                                <div
                                    style="display: flex; align-items: center; justify-content: space-around; margin-top: 20px;">
                                    <div style="text-align: center;">
                                        <img class="image"
                                            src="https://image.thanhnien.vn/w1024/Uploaded/2022/sgozna/2022_03_02/bang-gia-ve-may-bay-tet-2022-vietjet-air-cap-nhat-moi-nhat-1-150531-3629.jpeg" />
                                        <div class="name_gray">Vietjet ---- update version next</div>
                                    </div>
                                    <div class="wrapper_checkin">
                                        <?php $tach=explode(' ',$dfi->start_date);
                                                $tach2=explode('-',$tach[0]);
                                                if($tach2[1]=='01'){
                                                    $thang='JAN';
                                                }if($tach2[1]=='02'){
                                                    $thang='FEB';
                                                }if($tach2[1]=='03'){
                                                    $thang='MAR';
                                                }if($tach2[1]=='04'){
                                                    $thang='APR';
                                                }if($tach2[1]=='05'){
                                                    $thang='MAY';
                                                }if($tach2[1]=='06'){
                                                    $thang='JUN';
                                                }if($tach2[1]=='07'){
                                                    $thang='JUL';
                                                }if($tach2[1]=='08'){
                                                    $thang='AUG';
                                                }if($tach2[1]=='09'){
                                                    $thang='SEP';
                                                }if($tach2[1]=='10'){
                                                    $thang='OCT';
                                                }if($tach2[1]=='11'){
                                                    $thang='NOV';
                                                }if($tach2[1]=='12'){
                                                    $thang='DEC';
                                                }

                                                $giophut=explode(':',$tach[1]);

                                        ?>
                                        <div class="hour_checkin">{{$tach[1]}}</div>
                                        <div class="date_checkin">{{$tach2[2]}} {{$thang}} {{$tach2[0]}}</div>
                                    </div><?php $tach_end=explode(' ',$dfi->end_date);
                                                $tach2_end=explode('-',$tach_end[0]);
                                                if($tach2_end[1]=='01'){
                                                    $thang_end='JAN';
                                                }if($tach2_end[1]=='02'){
                                                    $thang_end='FEB';
                                                }if($tach2_end[1]=='03'){
                                                    $thang_end='MAR';
                                                }if($tach2_end[1]=='04'){
                                                    $thang_end='APR';
                                                }if($tach2_end[1]=='05'){
                                                    $thang_end='MAY';
                                                }if($tach2_end[1]=='06'){
                                                    $thang_end='JUN';
                                                }if($tach2_end[1]=='07'){
                                                    $thang_end='JUL';
                                                }if($tach2_end[1]=='08'){
                                                    $thang_end='AUG';
                                                }if($tach2_end[1]=='09'){
                                                    $thang_end='SEP';
                                                }if($tach2_end[1]=='10'){
                                                    $thang_end='OCT';
                                                }if($tach2_end[1]=='11'){
                                                    $thang_end='NOV';
                                                }if($tach2_end[1]=='12'){
                                                    $thang_end='DEC';
                                                }
                                                $giophut_end=explode(':',$tach_end[1]);

                                                $gio=$giophut_end[0]-$giophut[0];
                                                $phut=$giophut_end[1]-$giophut[1];



                                        ?>
                                    <div
                                        style="display: flex; flex-direction: column; justify-content: center; align-items: center;">
                                        <div style="margin-bottom: 10px;" class="name_gray">{{$gio}}h {{$phut}}m</div>
                                        <span
                                            style="width: 30px; height: 30px; transform: rotate(30deg); color: #888888;"
                                            class="iconify" data-icon="clarity:plane-solid"></span>
                                        <div style="display: flex; align-items: center;">
                                            <img class="dot" src="./images/dot-circle.png" />
                                            <hr style="border-top: 1px dashed #bbb;width: 100px; margin: 0px;" />
                                            <img class="dot" src="./images/dot-circle.png" />
                                        </div>
                                        <div style="margin-top: 10px;" class="checkin">Bay thẳng</div>
                                    </div>
                                    <div class="wrapper_checkin">
                                        <div class="hour_checkin">{{$tach_end[1]}}</div>
                                        <div class="date_checkin">{{$tach2_end[2]}} {{$thang_end}} {{$tach2_end[0]}}</div>
                                    </div>
                                </div>
                                <div
                                    style="display: flex; align-items: center; justify-content: space-between; margin-top: 20px;">
                                    <div class="note_checkin">Lỗi!! Xử lý đặt phòng lại trước 18:25:00 25/08/2021  --- update version next</div>
                                    <button id="buttonCheckin" data-toggle="collapse" data-target="#collapseCheckin"
                                        aria-expanded="true" aria-controls="collapseCheckin" type="button"
                                        class="btn button_show_checkin">
                                        <span>
                                            Hiển
                                            thị chi tiết
                                        </span>
                                        <span id="iconCheckin" class="iconify" style="transform: rotate(0deg);"
                                            data-icon="akar-icons:chevron-down"></span>
                                    </button>
                                </div>
                                <div id="collapseCheckin" class="collapse">
                                    <div class="divider"></div>
                                    <div class="box_info_checkin">
                                        <div class="row_info_checkin">
                                            <div class="label_checkin">Số chuyến bay</div>
                                            <div class="info_black_checkin">{{$dfi->flight_number}}</div>
                                            <div class="label_checkin">Loại máy bay</div>
                                            <div class="info_black_checkin">Airbus 321  --- update version next</div>
                                        </div>
                                        <div class="info_checkin_blue">Hành khách (2 người lớn, 1 trẻ em, 0 em bé) -- update version next</div>
                                        <div class="row_info_checkin">
                                            <div class="label_checkin">Ông/Anh:</div>
                                            <div class="info_black_checkin">{{$detail_flight->first_name}} {{$detail_flight->last_name}}</div>
                                            <div class="wrapper_row_info_child">
                                                <div class="row_info_child">
                                                    <div class="label_checkin">Ngày sinh:</div>
                                                    <div class="info_black_checkin">-  -- update version next</div>
                                                </div>
                                                <div class="row_info_child">
                                                    <div class="label_checkin">Hành lý:</div>
                                                    <div class="info_black_checkin">{{$dfi->hand_baggage}} - đ 80.000  -- update version next</div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="divider"></div>
                                    <div class="box_info_checkin">
                                        <div class="info_checkin_blue">Điều kiện giá vé -- update version next</div>
                                        <div class="policy">
                                            - Date change/flight change: Must change before 24h prior schedule departure
                                            time, change fee 374.000 VND + fare difference (if any)
                                            <br />
                                            - Route change: Must change before 24h prior schedule departure time, change
                                            fee 374.000 VND + fare difference (if any)
                                            <br />
                                            - Name change: Not permitted.
                                            <br />
                                            - Refund/Cancel: Not permitted.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div style="margin-top: 10px;" class="wrapper">
                    <div style="padding: 10px" class="box_title">
                        <div data-toggle="collapse" data-target="#collapseInfoInsurance" aria-expanded="true"
                            aria-controls="collapseInfoInsurance" id="buttonInfoInsurance" style="cursor: pointer;"
                            class="title">Thông tin bảo hiểm -- update version next</div>
                        <span id="iconInfoInsurance" data-icon="ant-design:caret-down-filled"
                            style="transform: rotate(0deg);" class="iconify"></span>
                    </div>
                    <div id="collapseInfoInsurance" class="collapse">
                        <div class="divider"></div>
                        <div>
                            <div class="row_info_insurance">
                                <div class="label_insurance">
                                    Gói bảo hiểm:
                                </div>
                                <div class="info_insurance_black">
                                    Bảo hiểm vé máy bay
                                </div>
                                <div class="label_insurance">
                                    Nhà cung cấp:
                                </div>
                                <div class="info_insurance_black">
                                    Bảo hiểm Vietinbank (VBI)
                                </div>
                            </div>
                            <div class="row_info_insurance">
                                <div class="info_insurance_black">Hành khách được bảo hiểm</div>
                            </div>
                            <div class="box_table_insurance">
                                <div class="row_table_insurance">
                                    <div class="td_insurance_gray small">STT</div>
                                    <div class="td_insurance_gray big">Số hợp đồng</div>
                                    <div class="td_insurance_gray big">Họ tên</div>
                                    <div class="td_insurance_gray normal">Giới tính</div>
                                    <div class="td_insurance_gray big">Phí bảo hiểm</div>
                                    <div class="td_insurance_gray big">Giấy chứng nhận</div>
                                </div>
                                <div class="row_table_insurance">
                                    <div class="td_insurance_black_normal small">1</div>
                                    <div class="td_insurance_blue big">0238998706</div>
                                    <div class="td_insurance_black_normal big">Nguyễn Văn A</div>
                                    <div class="td_insurance_black_normal normal">Nam</div>
                                    <div class="td_insurance_black_bold big">đ 89.000</div>
                                    <div class="big">
                                        <button class="btn">Xem chi tiết</button>
                                    </div>
                                </div>
                                <div class="row_table_insurance">
                                    <div class="td_insurance_black_normal small">2</div>
                                    <div class="td_insurance_blue big">0238998707</div>
                                    <div class="td_insurance_black_normal big">Nguyễn Văn B</div>
                                    <div class="td_insurance_black_normal normal">Nam</div>
                                    <div class="td_insurance_black_bold big">đ 89.000</div>
                                    <div class="big">
                                        <button class="btn">Xem chi tiết</button>
                                    </div>
                                </div>
                                <div class="row_table_insurance">
                                    <div class="td_insurance_black_normal small">3</div>
                                    <div class="td_insurance_blue big">0238998708</div>
                                    <div class="td_insurance_black_normal big">Nguyễn Văn C</div>
                                    <div class="td_insurance_black_normal normal">Nam</div>
                                    <div class="td_insurance_black_bold big">đ 89.000</div>
                                    <div class="big">
                                        <button class="btn">Xem chi tiết</button>
                                    </div>
                                </div>
                            </div>
                            <div style="width: 100%; text-align: center;margin-top: 10px;">
                                <span style="font-family: 'Montserrat';
                                font-style: normal;
                                font-weight: 600;
                                font-size: 14px;
                                line-height: 24px;
                                text-align: center;
                                color: #000000">Tổng phí bảo hiểm</span><span style="margin-left: 20px;font-family: 'Montserrat';
                                                                                        font-style: normal;
                                                                                        font-weight: 800;
                                                                                        font-size: 14px;
                                                                                        line-height: 15px;
                                                                                        color: #000000;">đ
                                    267.000</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="margin-top: 10px;" class="wrapper">
                    <div style="padding: 10px" class="box_title">
                        <div data-toggle="collapse" data-target="#collapseAlternate" aria-expanded="true"
                            aria-controls="collapseAlternate" id="buttonAlternate" style="cursor: pointer;"
                            class="title">Lịch sử thay đổi (5) -- update version next</div>
                        <span id="iconAlternate" data-icon="ant-design:caret-down-filled"
                            style="transform: rotate(0deg);" class="iconify"></span>
                    </div>
                    <div id="collapseAlternate" class="collapse">
                        <div class="divider"></div>
                        <div>
                            <div class="box_alternate">
                                <div style="display: flex; align-items: center; justify-content: space-between;">
                                    <div class="info_blue_alternate"># F-3-20210821102832-A143870</div>
                                    <button class="btn button_option_alternate">Đơn hàng gốc</button>
                                </div>
                                <div class="row_info_alternate">
                                    <div class="fifty_alternate">
                                        <div class="wrapper_info_alternate">
                                            <div class="label_alternate forty_per_alternate">Ngày tạo:</div>
                                            <div class="info_black_normal_alternate sixty_per_alternate">10:30:28
                                                30/08/2021
                                            </div>
                                        </div>
                                        <div class="wrapper_info_alternate">
                                            <div class="label_alternate forty_per_alternate">Người tạo:</div>
                                            <div class="info_black_normal_alternate sixty_per_alternate">Nguyễn Văn C
                                            </div>
                                        </div>
                                        <div class="wrapper_info_alternate">
                                            <div class="label_alternate forty_per_alternate">Trạng thái:</div>
                                            <div class="info_green_alternate sixty_per_alternate">Đã thanh toán</div>
                                        </div>
                                    </div>
                                    <div class="fifty_alternate">
                                        <div class="wrapper_info_alternate">
                                            <div class="label_alternate forty_per_alternate">HT thanh toán:</div>
                                            <div class="info_black_bold_alternate sixty_per_alternate">Stripe</div>
                                        </div>
                                        <div class="wrapper_info_alternate">
                                            <div class="label_alternate forty_per_alternate">Thời gian:</div>
                                            <div class="info_black_normal_alternate sixty_per_alternate">10:31:28
                                                30/08/2021
                                            </div>
                                        </div>
                                        <div class="wrapper_info_alternate">
                                            <div class="label_alternate forty_per_alternate">Tổng tiền TT:</div>
                                            <div class="info_green_alternate sixty_per_alternate">đ 4,000,000</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="divider"></div>
                                <div class="wrapper_detail_alternate">
                                    <div class="fifty_alternate">
                                        <div class="info_gray_alternate">
                                            Chuyến bay 1
                                        </div>
                                        <div style="margin: 10px 0px;">
                                            <img class="image"
                                                src="https://image.thanhnien.vn/w1024/Uploaded/2022/sgozna/2022_03_02/bang-gia-ve-may-bay-tet-2022-vietjet-air-cap-nhat-moi-nhat-1-150531-3629.jpeg" />
                                            <span class="info_black_bold_alternate">SGN - HAN</span>
                                        </div>
                                        <div class="info_black_bold_alternate">Mã đặt chỗ: UJSYEH9</div>
                                    </div>
                                    <div class="fifty_alternate">
                                        <div class="info_gray_alternate">
                                            Chuyến bay 2
                                        </div>
                                        <div style="margin: 10px 0px;">
                                            <img class="image"
                                                src="https://image.thanhnien.vn/w1024/Uploaded/2022/sgozna/2022_03_02/bang-gia-ve-may-bay-tet-2022-vietjet-air-cap-nhat-moi-nhat-1-150531-3629.jpeg" />
                                            <span class="info_black_bold_alternate">HAN- SGN</span>
                                        </div>
                                        <div class="info_black_bold_alternate">Mã đặt chỗ: UJSYEH9</div>
                                    </div>
                                </div>
                            </div>
                            <div class="box_alternate">
                                <div style="display: flex; align-items: center; justify-content: space-between;">
                                    <div class="info_blue_alternate"># F-3-20210821102832-A143870</div>
                                    <button class="btn button_option_alternate">Đổi thông tin hành khách</button>
                                </div>
                                <div class="row_info_alternate">
                                    <div class="fifty_alternate">
                                        <div class="wrapper_info_alternate">
                                            <div class="label_alternate forty_per_alternate">Ngày tạo:</div>
                                            <div class="info_black_normal_alternate sixty_per_alternate">10:30:28
                                                30/08/2021
                                            </div>
                                        </div>
                                        <div class="wrapper_info_alternate">
                                            <div class="label_alternate forty_per_alternate">Người tạo:</div>
                                            <div class="info_black_normal_alternate sixty_per_alternate">Nguyễn Văn C
                                            </div>
                                        </div>
                                        <div class="wrapper_info_alternate">
                                            <div class="label_alternate forty_per_alternate">Trạng thái:</div>
                                            <div class="info_orange_alternate sixty_per_alternate">Đã thanh toán</div>
                                        </div>
                                        <div class="wrapper_info_alternate">
                                            <div class="label_alternate forty_per_alternate">Mã đặt chỗ:</div>
                                            <div class="info_green_alternate sixty_per_alternate">UJSYEH9</div>
                                        </div>
                                        <div class="wrapper_info_alternate">
                                            <button class="button_confirm">XN thanh toán</button>
                                            <button class="button_cancel">Hủy bỏ</button>
                                        </div>
                                    </div>
                                    <div class="fifty_alternate">
                                        <div class="wrapper_info_alternate">
                                            <div class="label_alternate forty_per_alternate">HT thanh toán:</div>
                                            <div class="info_black_bold_alternate sixty_per_alternate">Bitcoin</div>
                                        </div>
                                        <div class="wrapper_info_alternate">
                                            <div class="label_alternate forty_per_alternate">Thời gian:</div>
                                            <div class="info_black_normal_alternate sixty_per_alternate">10:31:28
                                                30/08/2021
                                            </div>
                                        </div>
                                        <div class="wrapper_info_alternate">
                                            <div class="label_alternate forty_per_alternate">Lệ phí thay đổi:</div>
                                            <div class="info_black_normal_alternate sixty_per_alternate">BTC 0,0008
                                                <span class="info_black_small_alternate">
                                                    (đ 300,000)
                                                </span>
                                            </div>
                                        </div>
                                        <div class="wrapper_info_alternate">
                                            <div class="label_alternate forty_per_alternate">Mua thêm hành lý:</div>
                                            <div class="info_black_normal_alternate sixty_per_alternate">BTC 0,000001
                                                <span class="info_black_small_alternate">
                                                    (đ 100,000)
                                                </span>
                                            </div>
                                        </div>
                                        <div class="wrapper_info_alternate">
                                            <div class="label_alternate forty_per_alternate">Tổng tiền TT:</div>
                                            <div class="info_green_alternate sixty_per_alternate">BTC 0,0010 <span
                                                    class="info_black_bold_alternate">(đ
                                                    400,000)</span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="divider"></div>
                                <div class="wrapper_detail_alternate">
                                    <div class="fifty_alternate">
                                        <div class="info_gray_alternate">
                                            Trước thay đổi
                                        </div>
                                        <div class="info_black_bold_alternate">Trần Văn B - Nam</div>
                                        <div class="info_black_bold_alternate">15kg hành lý - đ 50,000</div>
                                    </div>
                                    <div class="fifty_alternate">
                                        <div class="info_gray_alternate">
                                            sau thay đổi
                                        </div>
                                        <div class="info_black_bold_alternate">Nguyễn Thị N - Nữ</div>
                                        <div class="info_black_bold_alternate">35kg hành lý - đ 150,000</div>
                                    </div>
                                </div>
                            </div>
                            <div class="box_alternate">
                                <div style="display: flex; align-items: center; justify-content: space-between;">
                                    <div class="info_blue_alternate"># F-3-20210821102832-A143870</div>
                                    <button class="btn button_option_alternate">Đổi chuyến bay</button>
                                </div>
                                <div class="row_info_alternate">
                                    <div class="fifty_alternate">
                                        <div class="wrapper_info_alternate">
                                            <div class="label_alternate forty_per_alternate">Ngày tạo:</div>
                                            <div class="info_black_normal_alternate sixty_per_alternate">10:30:28
                                                30/08/2021
                                            </div>
                                        </div>
                                        <div class="wrapper_info_alternate">
                                            <div class="label_alternate forty_per_alternate">Người tạo:</div>
                                            <div class="info_black_normal_alternate sixty_per_alternate">Nguyễn Văn C
                                            </div>
                                        </div>
                                        <div class="wrapper_info_alternate">
                                            <div class="label_alternate forty_per_alternate">Trạng thái:</div>
                                            <div class="info_green_alternate sixty_per_alternate">Đã thanh toán</div>
                                        </div>
                                        <div class="wrapper_info_alternate">
                                            <div class="label_alternate forty_per_alternate">Mã đặt chỗ:</div>
                                            <div class="info_green_alternate sixty_per_alternate">UJSYEH9</div>
                                        </div>
                                    </div>
                                    <div class="fifty_alternate">
                                        <div class="wrapper_info_alternate">
                                            <div class="label_alternate forty_per_alternate">HT thanh toán:</div>
                                            <div class="info_black_bold_alternate sixty_per_alternate">Stripe</div>
                                        </div>
                                        <div class="wrapper_info_alternate">
                                            <div class="label_alternate forty_per_alternate">Thời gian:</div>
                                            <div class="info_black_normal_alternate sixty_per_alternate">10:31:28
                                                30/08/2021
                                            </div>
                                        </div>
                                        <div class="wrapper_info_alternate">
                                            <div class="label_alternate forty_per_alternate">Lệ phí thay đổi:</div>
                                            <div class="info_black_normal_alternate sixty_per_alternate">đ 300,000</div>
                                        </div>
                                        <div class="wrapper_info_alternate">
                                            <div class="label_alternate forty_per_alternate">Giá vé chênh lệch:</div>
                                            <div class="info_black_normal_alternate sixty_per_alternate">đ 700,000</div>
                                        </div>
                                        <div class="wrapper_info_alternate">
                                            <div class="label_alternate forty_per_alternate">Tổng tiền TT:</div>
                                            <div class="info_green_alternate sixty_per_alternate">đ 1,000,000
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="divider"></div>
                                <div class="wrapper_detail_alternate">
                                    <div style="padding: 0px 20px" class="fifty_alternate">
                                        <div class="info_gray_alternate">
                                            Trước thay đổi
                                        </div>
                                        <div
                                            style="margin: 10px 0px; display: flex; align-items: center; justify-content: space-between;">
                                            <div>
                                                <img class="image"
                                                    src="https://image.thanhnien.vn/w1024/Uploaded/2022/sgozna/2022_03_02/bang-gia-ve-may-bay-tet-2022-vietjet-air-cap-nhat-moi-nhat-1-150531-3629.jpeg" />
                                                <span class="info_gray_alternate">VJ645</span>
                                            </div>
                                            <div class="info_green_alternate">đ1,200,000</div>
                                        </div>
                                        <div
                                            style="display: flex; align-items: center; justify-content: space-between;">
                                            <div style="text-align: center;">
                                                <div class="info_black_bold_alternate">20:15</div>
                                                <div class="info_black_normal_alternate">
                                                    27/08/2021
                                                </div>
                                                <div class="info_black_bold_alternate">SGN</div>
                                            </div>
                                            <div style="display: flex; align-items: center;">
                                                <hr style="border-top: 1px dashed #bbb;width: 100px; margin: 0px;" />
                                                <span style="color: #bbb" class="iconify"
                                                    data-icon="ci:chevron-right"></span>
                                            </div>
                                            <div style="text-align: center;">
                                                <div class="info_black_bold_alternate">20:15</div>
                                                <div class="info_black_normal_alternate">
                                                    27/08/2021
                                                </div>
                                                <div class="info_black_bold_alternate">SGN</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="padding: 0px 20px" class="fifty_alternate">
                                        <div class="info_gray_alternate">
                                            Trước thay đổi
                                        </div>
                                        <div
                                            style="margin: 10px 0px; display: flex; align-items: center; justify-content: space-between;">
                                            <div>
                                                <img class="image"
                                                    src="https://image.thanhnien.vn/w1024/Uploaded/2022/sgozna/2022_03_02/bang-gia-ve-may-bay-tet-2022-vietjet-air-cap-nhat-moi-nhat-1-150531-3629.jpeg" />
                                                <span class="info_gray_alternate">VJ645</span>
                                            </div>
                                            <div class="info_green_alternate">đ1,200,000</div>
                                        </div>
                                        <div
                                            style="display: flex; align-items: center; justify-content: space-between;">
                                            <div style="text-align: center;">
                                                <div class="info_black_bold_alternate">20:15</div>
                                                <div class="info_black_normal_alternate">
                                                    27/08/2021
                                                </div>
                                                <div class="info_black_bold_alternate">SGN</div>
                                            </div>
                                            <div style="display: flex; align-items: center;">
                                                <hr style="border-top: 1px dashed #bbb;width: 100px; margin: 0px;" />
                                                <span style="color: #bbb" class="iconify"
                                                    data-icon="ci:chevron-right"></span>
                                            </div>
                                            <div style="text-align: center;">
                                                <div class="info_black_bold_alternate">20:15</div>
                                                <div class="info_black_normal_alternate">
                                                    27/08/2021
                                                </div>
                                                <div class="info_black_bold_alternate">SGN</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box_alternate">
                                <div style="display: flex; align-items: center; justify-content: space-between;">
                                    <div class="info_blue_alternate"># F-3-20210821102832-A143870</div>
                                    <button class="btn button_option_cancel_alternate">Huỷ chuyến bay</button>
                                </div>
                                <div class="row_info_alternate">
                                    <div class="fifty_alternate">
                                        <div class="wrapper_info_alternate">
                                            <div class="label_alternate forty_per_alternate">Ngày tạo:</div>
                                            <div class="info_black_normal_alternate sixty_per_alternate">10:30:28
                                                30/08/2021
                                            </div>
                                        </div>
                                        <div class="wrapper_info_alternate">
                                            <div class="label_alternate forty_per_alternate">Người tạo:</div>
                                            <div class="info_black_normal_alternate sixty_per_alternate">Nguyễn Văn C
                                            </div>
                                        </div>
                                        <div class="wrapper_info_alternate">
                                            <div class="label_alternate forty_per_alternate">Trạng thái:</div>
                                            <div class="info_green_alternate sixty_per_alternate">Đã thanh toán</div>
                                        </div>
                                        <div class="wrapper_info_alternate">
                                            <div class="label_alternate forty_per_alternate">Mã đặt chỗ:</div>
                                            <div class="info_green_alternate sixty_per_alternate">UJSYEH9</div>
                                        </div>
                                    </div>
                                    <div class="fifty_alternate">
                                        <div class="wrapper_info_alternate">
                                            <div class="label_alternate forty_per_alternate">HT thanh toán:</div>
                                            <div class="info_black_bold_alternate sixty_per_alternate">Chuyển khoản
                                            </div>
                                        </div>
                                        <div class="wrapper_info_alternate">
                                            <div class="label_alternate forty_per_alternate">Thời gian:</div>
                                            <div class="info_black_normal_alternate sixty_per_alternate">10:31:28
                                                30/08/2021
                                            </div>
                                        </div>
                                        <div class="wrapper_info_alternate">
                                            <div class="label_alternate forty_per_alternate">Lệ phí thay đổi:</div>
                                            <div class="info_black_normal_alternate sixty_per_alternate">đ 300,000</div>
                                        </div>
                                        <div class="wrapper_info_alternate">
                                            <div class="label_alternate forty_per_alternate">Phí hoàn dự kiến</div>
                                            <div class="info_red_alternate sixty_per_alternate">đ 700,000</div>
                                        </div>
                                        <div class="wrapper_info_alternate">
                                            <div class="label_alternate forty_per_alternate">Tổng tiền TT:</div>
                                            <div class="info_green_alternate sixty_per_alternate">đ 1,000,000
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="divider"></div>
                                <div class="wrapper_detail_alternate">
                                    <div style="padding: 0px 20px; width: 100%;">
                                        <div class="info_gray_alternate">
                                            Thông tin bị huỷ
                                        </div>
                                        <div
                                            style="margin: 10px 0px; display: flex; align-items: center; justify-content: space-between;">
                                            <div>
                                                <img class="image"
                                                    src="https://image.thanhnien.vn/w1024/Uploaded/2022/sgozna/2022_03_02/bang-gia-ve-may-bay-tet-2022-vietjet-air-cap-nhat-moi-nhat-1-150531-3629.jpeg" />
                                                <span class="info_gray_alternate">VJ645</span>
                                            </div>
                                            <div style="text-align: center;">
                                                <div class="info_black_bold_alternate">20:15</div>
                                                <div class="info_black_normal_alternate">
                                                    27/08/2021
                                                </div>
                                                <div class="info_black_bold_alternate">SGN</div>
                                            </div>
                                            <div style="display: flex; align-items: center;">
                                                <hr style="border-top: 1px dashed #bbb;width: 100px; margin: 0px;" />
                                                <span style="color: #bbb" class="iconify"
                                                    data-icon="ci:chevron-right"></span>
                                            </div>
                                            <div style="text-align: center;">
                                                <div class="info_black_bold_alternate">20:15</div>
                                                <div class="info_black_normal_alternate">
                                                    27/08/2021
                                                </div>
                                                <div class="info_black_bold_alternate">SGN</div>
                                            </div>
                                            <div class="info_green_alternate">đ1,200,000</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box_alternate">
                                <div style="display: flex; align-items: center; justify-content: space-between;">
                                    <div class="info_blue_alternate"># F-3-20210821102832-A143870</div>
                                    <button class="btn button_option_cancel_alternate">Huỷ thông tin hành khách</button>
                                </div>
                                <div class="row_info_alternate">
                                    <div class="fifty_alternate">
                                        <div class="wrapper_info_alternate">
                                            <div class="label_alternate forty_per_alternate">Ngày tạo:</div>
                                            <div class="info_black_normal_alternate sixty_per_alternate">10:30:28
                                                30/08/2021
                                            </div>
                                        </div>
                                        <div class="wrapper_info_alternate">
                                            <div class="label_alternate forty_per_alternate">Người tạo:</div>
                                            <div class="info_black_normal_alternate sixty_per_alternate">Nguyễn Văn C
                                            </div>
                                        </div>
                                        <div class="wrapper_info_alternate">
                                            <div class="label_alternate forty_per_alternate">Trạng thái:</div>
                                            <div class="info_green_alternate sixty_per_alternate">Đã thanh toán</div>
                                        </div>
                                        <div class="wrapper_info_alternate">
                                            <div class="label_alternate forty_per_alternate">Mã đặt chỗ:</div>
                                            <div class="info_green_alternate sixty_per_alternate">UJSYEH9</div>
                                        </div>
                                    </div>
                                    <div class="fifty_alternate">
                                        <div class="wrapper_info_alternate">
                                            <div class="label_alternate forty_per_alternate">HT thanh toán:</div>
                                            <div class="info_black_bold_alternate sixty_per_alternate">Chuyển khoản
                                            </div>
                                        </div>
                                        <div class="wrapper_info_alternate">
                                            <div class="label_alternate forty_per_alternate">Thời gian:</div>
                                            <div class="info_black_normal_alternate sixty_per_alternate">10:31:28
                                                30/08/2021
                                            </div>
                                        </div>
                                        <div class="wrapper_info_alternate">
                                            <div class="label_alternate forty_per_alternate">Lệ phí thay đổi:</div>
                                            <div class="info_black_normal_alternate sixty_per_alternate">đ 300,000</div>
                                        </div>
                                        <div class="wrapper_info_alternate">
                                            <div class="label_alternate forty_per_alternate">Phí hoàn dự kiến</div>
                                            <div class="info_red_alternate sixty_per_alternate">đ 700,000</div>
                                        </div>
                                        <div class="wrapper_info_alternate">
                                            <div class="label_alternate forty_per_alternate">Tổng tiền TT:</div>
                                            <div class="info_green_alternate sixty_per_alternate">đ 1,000,000
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="divider"></div>
                                <div class="wrapper_detail_alternate">
                                    <div style="padding: 0px 20px; width: 100%;">
                                        <div class="info_gray_alternate">
                                            Thông tin bị huỷ
                                        </div>
                                        <div class="info_black_bold_alternate">Trần Văn B - Nam (Người lớn)</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="margin-top: 10px;" class="wrapper">
                    <div style="padding: 10px" class="box_title">
                        <div data-toggle="collapse" data-target="#collapseDeal" aria-expanded="true"
                            aria-controls="collapseDeal" id="buttonDeal" style="cursor: pointer;" class="title">Lịch sử
                            giao dịch (4) -- update version next</div>
                        <span id="iconDeal" data-icon="ant-design:caret-down-filled" style="transform: rotate(0deg);"
                            class="iconify"></span>
                    </div>
                    <div id="collapseDeal" class="collapse">
                        <div class="divider"></div>
                        <div style="margin-top: 20px;">
                            <div class="row_deal">
                                <div class="td_deal_gray header_deal">Mã giao dịch</div>
                                <div class="td_deal_gray header_deal">Mô tả giao dịch</div>
                                <div class="td_deal_gray header_deal">Số tiền</div>
                                <div class="td_deal_gray header_deal">Phương thức</div>
                                <div class="td_deal_gray header_deal">Chứng từ</div>
                            </div>
                            <div class="row_deal">
                                <div class="row_info_deal">
                                    <div class="td_deal_black_bold">TS012345567</div>
                                    <div class="td_deal_gray">10:28:32 21/08/2021</div>
                                </div>
                                <div class="row_info_deal">
                                    <div class="td_deal_black_bold">Thanh toán đơn hàng </div>
                                    <div class="td_deal_blue">#F-3-20210821102832-A143870</div>
                                </div>
                                <div class="row_info_deal">
                                    <div class="td_deal_black_bold">VND 4,000,000</div>
                                    <div class="td_deal_green">Đã thanh toán</div>
                                </div>
                                <div class="row_info_deal">
                                    <div class="td_deal_normal_bold">Credit Card</div>
                                </div>
                                <div class="row_info_deal">
                                    <div class="td_deal_blue">ch_3JQsl8JUTgDYMTVw0lMBYga9</div>
                                </div>
                            </div>
                            <div class="row_deal">
                                <div class="row_info_deal">
                                    <div class="td_deal_black_bold">TS012345567</div>
                                    <div class="td_deal_gray">10:28:32 21/08/2021</div>
                                </div>
                                <div class="row_info_deal">
                                    <div class="td_deal_black_bold">Thanh toán đơn hàng </div>
                                    <div class="td_deal_blue">#F-3-20210821102832-A143870</div>
                                </div>
                                <div class="row_info_deal">
                                    <div class="td_deal_black_bold">VND 4,000,000</div>
                                    <div class="td_deal_red">Chờ xử lý hoàn</div>
                                </div>
                                <div class="row_info_deal">
                                    <div class="td_deal_normal_bold">Credit Card</div>
                                </div>
                                <div class="row_info_deal">
                                    <div class="td_deal_blue">ch_3JQsl8JUTgDYMTVw0lMBYga9</div>
                                </div>
                            </div>
                            <div class="row_deal">
                                <div class="row_info_deal">
                                    <div class="td_deal_black_bold">TS012345567</div>
                                    <div class="td_deal_gray">10:28:32 21/08/2021</div>
                                </div>
                                <div class="row_info_deal">
                                    <div class="td_deal_black_bold">Thanh toán đơn hàng </div>
                                    <div class="td_deal_blue">#F-3-20210821102832-A143870</div>
                                </div>
                                <div class="row_info_deal">
                                    <div class="td_deal_black_bold">VND 4,000,000</div>
                                    <div class="td_deal_green">Đã thanh toán</div>
                                </div>
                                <div class="row_info_deal">
                                    <div class="td_deal_normal_bold">Credit Card</div>
                                </div>
                                <div class="row_info_deal">
                                    <div class="td_deal_blue">ch_3JQsl8JUTgDYMTVw0lMBYga9</div>
                                </div>
                            </div>
                            <div class="row_deal">
                                <div class="row_info_deal">
                                    <div class="td_deal_black_bold">TS012345567</div>
                                    <div class="td_deal_gray">10:28:32 21/08/2021</div>
                                </div>
                                <div class="row_info_deal">
                                    <div class="td_deal_black_bold">Thanh toán đơn hàng </div>
                                    <div class="td_deal_blue">#F-3-20210821102832-A143870</div>
                                </div>
                                <div class="row_info_deal">
                                    <div class="td_deal_black_bold">VND 4,000,000</div>
                                    <div class="td_deal_green">Đã thanh toán</div>
                                </div>
                                <div class="row_info_deal">
                                    <div class="td_deal_normal_bold">Credit Card</div>
                                </div>
                                <div class="row_info_deal">
                                    <div class="td_deal_blue">ch_3JQsl8JUTgDYMTVw0lMBYga9</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box_right">
                <div style="padding: 10px;" class="wrapper">
                    <div class="box_title">
                        <div style="padding: 5px" class="title">Thông tin giá vé</div>
                    </div>
                    <div class="divider"></div>
                </div><?php
                $tong=0;
                ?>
                @foreach($detail_flight_info as $dfi)
                    <?php
                    $i++;
                    ?>
                <div class="box_room">
                    <div class="box_info">
                        <div class="wrapper_name">
                            <div class="name_room">
                                {{$dfi->start_point}} - {{$dfi->end_point}}
                            </div>
                            @if($i==1){
                            <div class="name_gray">Ngày đi: {{$dfi->start_date}}</div>
                            <div class="name_gray">Economy - M</div>
                            @else

                            <div class="name_gray">Ngày về: {{$dfi->start_date}}</div>
                            <div class="name_gray">Economy - M</div>
                            @endif
                        </div>
                        <div class="wrapper_image">
                            <img class="image" src="https://image.thanhnien.vn/w1024/Uploaded/2022/sgozna/2022_03_02/bang-gia-ve-may-bay-tet-2022-vietjet-air-cap-nhat-moi-nhat-1-150531-3629.jpeg" />
                            <div style="font-size: 13px;" class="name_gray">Vietjet Air</div>
                        </div>
                    </div>
                    <div class="box_price">
                        <div class="box_info_price">
                            <div class="box_info_price_parent">
                                <div class="label">Người lớn x 1</div>
                                <div class="name_black">đ {{number_format($dfi->fare_adt+$dfi->tax_adt+$dfi->fee_adt,0,',','.')}}</div>
                            </div>
                            <div class="box_info_price_child">
                                <div class="name_gray">Giá vé</div>
                                <div class="name_gray">đ {{number_format($dfi->fare_adt,0,',','.')}}</div>
                            </div>
                            <div class="box_info_price_child">
                                <div class="name_gray">Thuế và phí</div>
                                <div class="name_gray">đ {{number_format($dfi->tax_adt+$dfi->fee_adt,0,',','.')}}</div>
                            </div>
                        </div>
                        <div class="box_info_price">
                            <div class="box_info_price_parent">
                                <div class="label">Trẻ em x 1</div>
                                <div class="name_black">đ {{number_format($dfi->fare_chd+$dfi->tax_chd+$dfi->fee_chd,0,',','.')}}</div>
                            </div>
                            <div class="box_info_price_child">
                                <div class="name_gray">Giá vé</div>
                                <div class="name_gray">đ {{number_format($dfi->fare_chd,0,',','.')}}</div>
                            </div>
                            <div class="box_info_price_child">
                                <div class="name_gray">Thuế và phí</div>
                                <div class="name_gray">đ {{number_format($dfi->tax_chd+$dfi->fee_chd,0,',','.')}}</div>
                            </div>
                        </div>
                        <div class="box_info_price">
                            <div class="box_info_price_parent">
                                <div class="label">Em bé x 1</div>
                                <div class="name_black">đ {{number_format($dfi->fare_inf+$dfi->tax_inf+$dfi->fee_inf,0,',','.')}}</div>
                            </div>
                            <div class="box_info_price_child">
                                <div class="name_gray">Giá vé</div>
                                <div class="name_gray">đ {{number_format($dfi->fare_inf,0,',','.')}}</div>
                            </div>
                            <div class="box_info_price_child">
                                <div class="name_gray">Thuế và phí</div>
                                <div class="name_gray">đ {{number_format($dfi->tax_inf+$dfi->fee_inf,0,',','.')}}</div>
                            </div>
                        </div>
                        <div class="box_info_price">
                            <div class="box_info_price_parent">
                                <div class="label">Hành lý x 2 (U V N)</div>
                                <div class="name_black">đ 2,000,000 </div>
                            </div>
                            <div class="box_info_price_child">
                                <div class="name_gray">Giá vé</div>
                                <div class="name_gray">đ 1,400,000</div>
                            </div>
                            <div class="box_info_price_child">
                                <div class="name_gray">Thuế và phí</div>
                                <div class="name_gray">đ 500,000</div>
                            </div>
                        </div>
                    </div>
                    <div class="divider"></div>
                    <div class="box_total_room">
                        <div class="name_black">Tổng giá vé</div>
                        <?php

                        $tong=$tong+($dfi->fare_inf+$dfi->tax_inf+$dfi->fee_inf+$dfi->fare_adt+$dfi->tax_adt+$dfi->fee_adt+$dfi->fare_chd+$dfi->tax_chd+$dfi->fee_chd);

                        ?>
                        <div class="name_black">đ {{number_format($dfi->fare_inf+$dfi->tax_inf+$dfi->fee_inf+$dfi->fare_adt+$dfi->tax_adt+$dfi->fee_adt+$dfi->fare_chd+$dfi->tax_chd+$dfi->fee_chd,0,',','.')}}</div>
                    </div>
                </div>
                @endforeach
                <div style="padding: 10px;" class="wrapper">
                    <div class="box_info_price_parent">
                        <div class="name_black">KS khuyến mãi (U V N)</div>
                        <div class="name_black">đ 700,000</div>
                    </div>
                    <div class="box_info_price_parent">
                        <div class="name_black">Thay đổi phát sinh</div>
                        <div class="name_black">- đ 400,000</div>
                    </div>
                    <div class="box_info_price_parent">
                        <div class="name_black">Mã giảm giá</div>
                        <div class="name_black">- đ 400,000</div>
                    </div>
                    <div class="name_blue">(HOTSALE06)</div>
                </div>
                <div class="box_total">
                    <div class="total">TỔNG GIÁ TRỊ ĐƠN HÀNG</div>
                    <div class="total">đ {{number_format($tong,0,',','.')}}</div>
                </div>
                <div class="box_activity_log">
                    <div data-toggle="collapse" data-target="#collapseActivityLog" aria-expanded="true"
                        aria-controls="collapseActivityLog"
                        style="padding: 20px; display: flex; align-items: center; justify-content: space-between; cursor: pointer;">
                        <span class="activity_log">Nhật ký hoạt động (6)</span>
                        <span data-icon="ant-design:caret-down-filled"
                            style="color: #666666; width: 20px; height: 20px;" class="iconify"></span>
                    </div>
                    <div id="collapseActivityLog" class="collapse">
                        <div class="divider" style="background-color: #fff;"></div>
                        <div class="info_gray_bold_activity_log"> 25/07/2021 21:10:20: CS02 - Mai đã chỉnh sửa ghi chú
                        </div>
                        <div class="info_gray_bold_activity_log"> 24/07/2021 10:04:01: CS01 - Hương đã thêm ghi chú mới
                        </div>
                        <div class="info_gray_bold_activity_log"> 23/07/2021 10:04:20: Nguyễn Văn A xuất vé tự động
                            cho booking UJSYEH9 thành công
                        </div>
                        <div class="info_gray_bold_activity_log"> 23/07/2021 10:03:00: Booking UJSYEH9 bị lỗi xuất
                            vé
                        </div>
                        <div class="info_gray_bold_activity_log"> 23/07/2021 10:00:20: Nguyễn Văn A tạo đơn hàng vé
                            máy bay
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</body>

</html>
