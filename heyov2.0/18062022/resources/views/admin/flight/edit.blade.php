<!DOCTYPE html>
<html lang="en">
<head>
    <title>HF Admin</title>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="{{asset('public/admin/css/create.css')}}" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="https://code.iconify.design/2/2.2.1/iconify.min.js"></script>
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css"
          rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
    <link rel="stylesheet"
          href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
    <script
        src="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
    <script></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="{{asset('public/admin/js/main.js')}}"></script>
    <style>
        .toggle.ios,
        .toggle-on.ios,
        .toggle-off.ios {
            border-radius: 20rem;
        }
        .toggle.ios .toggle-handle {
            border-radius: 20rem;
        }
    </style>
</head>
<body>
<div class="menu_header">
    <div class="left">
        <a href="#" class="logo">HEYO TRIP</a>
    </div>
    <div class="right">
        <span class="name_menu"> Chương trình khuyến mãi </span>
        <div class="box_admin">
               <span class="iconify" style="width: 25px; height: 25px; color: #032044"
                     data-icon="ic:baseline-notifications-none"></span>
            <div class="badge badge-danger counter">9</div>
            <div id="btn_admin" class="wrapper_admin">
                <span style="width: 25px; height: 25px; color: #032044" class="iconify" data-icon="lucide:user"></span>
                <span class="admin">Admin</span>
                <span style="
                     width: 15px;
                     height: 15px;
                     color: #032044;
                     margin-left: 10px;
                     " class="iconify" data-icon="ant-design:caret-down-filled"></span>
            </div>
        </div>
    </div>
    <div id="profile" style="box-shadow: 10" class="box_profile">
            <span style="width: 30px; height: 30px; color: #032044" class="iconify"
                  data-icon="simple-line-icons:logout"></span>
        <span class="logout"><a href="{{asset('logout')}}" style="text-decoration: none;color: black"> Đăng xuất</a></span>
    </div>
</div>
<div style="display: flex">
    <div class="menu_left">
        <div class="top">
            <a href="{{asset('admin/hotel/order/all/1')}}" style="background-color: #f2f9ff; text-decoration: none" class="item">
                    <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                          data-icon="icon-park-outline:hotel"></span>
                <span style="color: #526a87" class="name_menu">Khách sạn</span>
            </a>
            <a href="{{asset('admin/flight/order/all/1')}}" style=" text-decoration: none" class="item">
                <span class="iconify" style="color: #526a87; width: 30px; height: 30px" data-icon="ps:plane"></span>
                <span  style="color: #526a87"class="name_menu">Vé máy bay</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
                <span class="iconify" style="color: #526a87; width: 30px; height: 30px" data-icon="clarity:car-line"></span>
                <span style="color: #526a87" class="name_menu">Thuê xe</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
               <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                     data-icon="clarity:data-cluster-line"></span>
                <span style="color: #526a87" class="name_menu">Crypto</span>
            </a>
        </div>
        <div class="top">
            <a href="#" style="text-decoration: none" class="item">
                <span class="iconify" style="color: #526a87; width: 30px; height: 30px" data-icon="ep:user-filled"></span>
                <span style="color: #526a87" class="name_menu">Tài khoản</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
               <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                     data-icon="dashicons:welcome-widgets-menus"></span>
                <span style="color: #526a87" class="name_menu">Danh mục</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
                <span class="iconify" style="color: #526a87; width: 30px; height: 30px" data-icon="gg:website"></span>
                <span style="color: #526a87" class="name_menu">Website</span>
            </a>
            <a href="{{asset('admin/website/list/quocgia')}}" style="text-decoration: none" class="item">
               <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                     data-icon="material-symbols:settings-suggest-outline-sharp"></span>
                <span style="color: #526a87" class="name_menu">Cấu hình</span>
            </a>
        </div>
    </div>
    <div class="menu_right">
        <div class="item">
               <span style="width: 25px; height: 25px; color: #032044" class="iconify"
                     data-icon="majesticons:analytics"></span>
            <span class="name_menu">Dashboard</span>
        </div>
        <a  href="{{asset('admin/hotel/searchHotel')}}"class="item">
            <span style="width: 25px; height: 25px; color: #032044" class="iconify" data-icon="mdi:bed"></span>
            <span class="name_menu">Tìm - Đặt phòng</span>
    </a>
        <a href="{{asset('admin/hotel/flight/all/1')}}" class="item">
               <span style="width: 25px; height: 25px; color: #032044" class="iconify"
                     data-icon="fa-solid:calendar-day"></span>
            <span class="name_menu">Quản lý đơn đặt vé</span>
        </a>
        <div class="item">
               <span style="width: 25px; height: 25px; color: #032044" class="iconify"
                     data-icon="fa-solid:money-check-alt"></span>
            <span class="name_menu">Tuỳ chỉnh giá phòng</span>
        </div>
        <div class="item">
               <span style="width: 25px; height: 25px; color: #329223" class="iconify"
                     data-icon="icomoon-free:price-tags"></span>
            <span  class="name_menu"><a style="color: #329223; font-weight: 800"href='{{asset("admin/flight/list")}}'>Chương trình khuyến mãi</a></span>
        </div>
        <div class="item">
            <span style="width: 25px; height: 25px; color: #032044" class="iconify" data-icon="eva:percent-fill"></span>
            <span class="name_menu">Quản lý hoa hồng</span>
        </div>
        <div class="item">
               <span style="width: 25px; height: 25px; color: #032044" class="iconify"
                     data-icon="fa6-solid:hand-holding-dollar"></span>
            <span class="name_menu">Huỷ - Hoàn tiền</span>
        </div>
        <div class="item">
               <span style="width: 25px; height: 25px; color: #032044" class="iconify"
                     data-icon="fluent:news-20-filled"></span>
            <span class="name_menu">Bài viết Marketing</span>
        </div>
    </div>                <form method="post"enctype="multipart/form-data">@csrf

        <div class="box_content">
            <div class="wrapper">
                <div class="content">
                    <div class="box_title">
                        <div class="title">Thông tin khuyến mãi</div>
                    </div>
                    <div style="margin: 0px" class="dropdown-divider"></div>

                    <div class="content">
                        <div class="row_input">
                            <div class="box_input">
                                <div class="label">Phân hệ<span class="star">*</span></div>
                                <input type="text" readonly  name="subsystem" class="input" value="Flight" placeholder="Hotel">

                            </div>
                            <div class="box_input">
                                <div class="label">
                                    Loại khuyến mãi<span class="star">*</span>
                                </div>
                                <select id="type_promo" name="type_promo">
                                    <option value="1" @if($promo->type_promo==1) selected @endif >Promo Code</option>
                                    <option value="2" @if($promo->ype_promo==2) selected @endif >Unique Code</option>
                                    <option value="3" @if($promo->type_promo==3) selected @endif >Partner Code</option>
                                    <option value="4" @if($promo->type_promo==4) selected @endif > Special Campaign</option>

                                </select>
                            </div>
                        </div>
                        <div class="row_input">
                            <div class="box_input">
                                <div class="label">
                                    Tên chương trình khuyến mãi
                                    <span class="star">*</span>
                                </div>
                                <input class="input" type="text" value="{{$promo->title}}" class="form-control" required   name="title" id="title_create"placeholder="Nhập" />
                                <div class="help_text_input">0/100</div>
                            </div>
                            @if(Session::get('titleE'))                                        <br>
                            <span class="alert alert-danger">{{Session::get('titleE')}}</span>
                            <?php Session::put('titleE','')?>
                            <?php
                            Session::put('title','')
                            ?>
                            @endif
                        </div>
                        <div style="margin-top: 20px" class="row_input">
                            <div class="box_input">
                                <div class="label">
                                    Nội dung khuyến mãi
                                    <span class="star">*</span>
                                </div>
                                <textarea class="input" rows="5" placeholder="Nhập"required name="content">{{$promo->content}}</textarea>
                            </div>
                            <?php
                            Session::put('content','')
                            ?>
                        </div>
                        <div class="row_input">
                            <div class="box_input">
                                <div class="label">
                                    Mã khuyến mãi
                                    <span class="star">*</span>
                                </div>
                                <input class="input" type="text" name="code_promotion" value="{{$promo->code_promotion}}" required id="code_pro" />
                                @if(Session::get('code_promotionE'))                                        <br>
                                <span class="alert alert-danger">{{Session::get('code_promotionE')}}</span>
                                <?php Session::put('code_promotionE','')?>
                                <?php
                                Session::put('code_promotion','')
                                ?>
                                @endif
                            </div>
                            <div class="box_input">
                                <div class="label">
                                    Mã chương trình
                                    <span class="star">*</span>
                                </div>
                                <input class="input" type="text"readonly placeholder="Nhập"name="program_code_promo" id="code_program" value="{{$promo->program_code_promo}}">
                            </div>
                            <div class="box_input">
                                <div class="label">Trạng thái<span class="star">*</span></div>
                                <select  name="status">
                                    <option @if($promo->status==0) selected @endif value="0">Inactive</option>
                                    <option @if($promo->status==1) selected @endif value="1">Active</option>
                                    <option @if($promo->status==2) selected @endif value="2">Deactive</option>
                                    <option @if($promo->status==3) selected @endif value="3">Expired</option>
                                    <?php
                                    Session::put('status','')
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="row_input">
                            <div class="box_input">
                                <div class="label">Hình ảnh Cover</div>
                                <div class="help_text_choose_file">
                                    Dimension (1242x464) Max 500KB
                                </div>
                                <button  type="button" id="btn_choose_file_cover" class="btn button_choose_file">
                           <span style="width: 20px; height: 20px; color: #fff" class="iconify"
                                 data-icon="fluent:add-16-filled"></span>
                                    <span class="name_button_choose_file">Add files</span>
                                </button>
                                <input style="display: none" id="coverFile"  name="images_cover" onchange="loadFile2(event)"    title=" " value="" type="file" />
                            </div>

                        </div> <br>

                        <img id="output_images_cover" src="{{$promo->images_cover}}" width="15%"/>                    <br>

                        <script>
                            var loadFile2 = function(event) {
                                var output = document.getElementById('output_images_cover');
                                output.src = URL.createObjectURL(event.target.files[0]);
                                output.onload = function() {
                                    URL.revokeObjectURL(output.src) // free memory
                                }
                            };</script><div class='clearfix'></div>
                        <?php

                        if(Session::get('ErrorImagecover')){
                            echo "<span class='alert-danger alert'>".Session::get('ErrorImagecover')."</span>";
                            Session::put('ErrorImagecover','');
                        }
                        ?><div class='clearfix'></div>
                        <br>
                        <div class="row_input">
                            <div class="box_input">
                                <div class="label">Hình ảnh Logo</div>
                                <div class="help_text_choose_file">
                                    Dimension (260x260) Max 500KB
                                </div>
                                <button type="button" id="btn_choose_file_logo" class="btn button_choose_file">
                           <span style="width: 20px; height: 20px; color: #fff" class="iconify"
                                 data-icon="fluent:add-16-filled"></span>
                                    <span class="name_button_choose_file">Add files</span>
                                </button>
                                <input style="display: none" id="logoFile" name="images_logo" onchange="loadFile(event)"  title=" "  value="" type="file" />
                            </div>
                        </div>
                        <br>
                        <img id="output_images_logo" src="{{$promo->images_logo}}" width="15%"/>                    <br>

                        <script>
                            var loadFile = function(event) {
                                var output = document.getElementById('output_images_logo');
                                output.src = URL.createObjectURL(event.target.files[0]);
                                output.onload = function() {
                                    URL.revokeObjectURL(output.src) // free memory
                                }
                            };</script>
                        <div class='clearfix'></div>
                        <?php

                        if(Session::get('ErrorImageLogo')){
                            echo "<span class='alert-danger alert'>".Session::get('ErrorImageLogo')."</span>";
                            Session::put('ErrorImageLogo','');
                        }
                        ?><div class='clearfix'></div> <br>
                    </div>
                </div>
            </div>
            <div class="wrapper">
                <div class="box_title">
                    <div class="title">Thiết lập khuyến mãi</div>
                </div>
                <div style="margin: 0px" class="dropdown-divider"></div>
                <div class="content">
                    <div class="row_input">
                        <div class="box_input">
                            <div class="label">
                                Kiểu hiển thị trên ứng dụng<span class="star">*</span>
                            </div>
                            <div style="display: flex; margin-top: 10px">
                                <div style="display: flex; align-items: center" class="form-check">
                                    <input style="width: 20px; height: 20px" class="form-check-input" type="radio" name="display" @if($promo->display==0) checked @endif value="0"
                                           id="display" />
                                    <label style="
                                 font-family: 'Montserrat';
                                 font-style: normal;
                                 font-weight: 400;
                                 font-size: 15px;
                                 color: #666666;
                                 margin-left: 10px;
                                 margin-top: 5px;
                                 " class="form-check-label" for="flexRadioDefault1">
                                        Hiển thị trong danh sách
                                    </label>
                                </div>
                                <div style="
                              display: flex;
                              align-items: center;
                              margin-left: 30px;
                              " class="form-check">
                                    <input style="width: 20px; height: 20px" class="form-check-input" type="radio"value="1"@if($promo->display==1) checked @endif  name="display"
                                           id="display1"  />
                                    <label style="
                                 font-family: 'Montserrat';
                                 font-style: normal;
                                 font-weight: 400;
                                 font-size: 15px;
                                 color: #666666;
                                 margin-left: 10px;
                                 margin-top: 5px;
                                 " class="form-check-label" for="flexRadioDefault2">
                                        Nhập mã để tìm
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row_input">
                        <div class="box_input">
                            <div class="label">Đường dẫn hiển thị</div>
                            <div class="pathname">
                                <div class="left">https://heyotrip.com/</div>
                                <input class="right" type="text" name="link" id="link" value="{{$promo->link}}"/>
                            </div>
                        </div>
                    </div>
                    <div class="row_input">
                        <div class="box_input">
                            <div class="label">Ngày bắt đầu </div>
                            <div class="wrapper_date">
                                <div style="width: 100%; margin-top: 5px" class="input-group date" id="datetimepicker1">
                                    <input type="text" name="date_start" required class="form-control" value="<?php
                                    $tach_day=explode(' ',$promo->date_start);
                                    $tach_day2=explode('-',$tach_day[0]);

                                    $date_start=$tach_day2[2].'-'.$tach_day2[1].'-'.$tach_day2[0].' '.$tach_day[1];
                                    ?>{{$date_start}}" class="form-control" value="" />
                                    <span class="input-group-addon">
                              <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                                </div>
                            </div>
                            @if(Session::get('Errordate_start'))                                        <br>
                            <span class="alert alert-danger">{{Session::get('Errordate_start')}}</span>

                            @endif
                        </div>
                        <div class="box_input">
                            <div class="label">Ngày kết thúc</div>
                            <div class="wrapper_date">
                                <div style="width: 100%; margin-top: 5px" class="input-group date" id="datetimepicker2">
                                    <input type="text" name="date_end" required class="form-control"value="<?php
                                    $tach_day=explode(' ',$promo->date_end);
                                    $tach_day2=explode('-',$tach_day[0]);

                                    $date_start=$tach_day2[2].'-'.$tach_day2[1].'-'.$tach_day2[0].' '.$tach_day[1];
                                    ?>{{$date_start}}" class="form-control" value="" />
                                    <span class="input-group-addon">
                              <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                                </div>
                            </div>

                            @if(Session::get('Errordate_end'))
                                <br>
                                <span class="alert alert-danger">{{Session::get('Errordate_end')}}</span>

                            @endif
                        </div>
                    </div>
                    <div class="row_input">
                        <div class="box_input">
                            <div class="label">
                                Số lượng phát hành<span class="star">*</span>
                            </div>
                            <input type="text"  class="input" name="number_max" id="uang_create" required  value="{{number_format($promo->number_max,0,',',',')}}" />
                        </div>
                        <div class="box_input">
                            <div class="label">
                                Giới hạn sử dụng trong ngày<span class="star">*</span>
                            </div>
                            <input type="text" class="input" name="number_limit_day" id="uang2"   value="{{number_format($promo->number_limit_day,0,',',',')}}" />
                        </div>
                    </div>
                    <div class="row_input">
                        <div class="box_input">
                            <div class="label">
                                Loại giảm giá<span class="star">*</span>
                            </div>
                            <?php
                                $tbl_type_discount=DB::table('tbl_type_discount')->where('id_type_discount',$promo->id_type_discount)->first();

                            ?>
                            <div class="box_discount">
                                <div style="display: flex">
                                    <div style="display: flex; align-items: center" class="form-check">
                                        <input style="width: 20px; height: 20px" class="form-check-input" type="radio" name="id_type_discount" @if($tbl_type_discount->percentage_reduction==null) checked @endif value="1" name="radioDiscount"
                                               id="radioDiscount1" checked />
                                        <label style="
                                    font-family: 'Montserrat';
                                    font-style: normal;
                                    font-weight: 400;
                                    font-size: 15px;
                                    color: #666666;
                                    margin-left: 10px;
                                    margin-top: 5px;
                                    " class="form-check-label" for="radioDiscount1">
                                            Giảm giá trị cố định
                                        </label>
                                    </div>
                                    <div style="
                                 display: flex;
                                 align-items: center;
                                 margin-left: 30px;
                                 " class="form-check">
                                        <input style="width: 20px; height: 20px" class="form-check-input" name="id_type_discount" @if($tbl_type_discount->percentage_reduction!=null) checked @endif value="2"type="radio" name="radioDiscount"
                                               id="radioDiscount2" />
                                        <label style="
                                    font-family: 'Montserrat';
                                    font-style: normal;
                                    font-weight: 400;
                                    font-size: 15px;
                                    color: #666666;
                                    margin-left: 10px;
                                    margin-top: 5px;
                                    " class="form-check-label" for="radioDiscount2">
                                            Giảm giá theo phần trăm (%)
                                        </label>
                                    </div>
                                </div>
                                <div id="box_discount1" class="@if($tbl_type_discount->percentage_reduction!=null) none @endif" >
                                    <div class="label">Mức tiền giảm</div>
                                    <div style="display: flex; margin-top: 5px">
                                        <input class="input" name="reduced_amount"id="uang4"  type="text"value="{{number_format($tbl_type_discount->reduced_amount,0,',',',')}}" />
                                        <div style="
                                    background: #e9ecef;
                                    border: 1px solid #dee2e6;
                                    border-radius: 5px;
                                    margin-top: 5px;
                                    font-family: 'Montserrat';
                                    font-style: normal;
                                    font-weight: 400;
                                    font-size: 13px;
                                    line-height: 20px;
                                    color: #666666;
                                    padding: 5px 10px;
                                    display: flex;
                                    align-items: center;
                                    justify-content: center;
                                    ">
                                            VND
                                        </div>
                                    </div>
                                </div>
                                <?php
                                ?>
                                <div id="box_discount2" class="@if($tbl_type_discount->percentage_reduction!=null) flex @endif @if($tbl_type_discount->percentage_reduction==null) none @endif ">
                                    <div style="padding: 0px 10px 0px 0px" class="box_input">
                                        <div class="label">Mức giảm theo %</div>
                                        <input class="input"  name="percentage_reduction" id='percentage_reduction' type="number"value="{{number_format($tbl_type_discount->percentage_reduction,0,',',',')}}"/>
                                    </div>
                                    <div style="padding: 0px 10px 0px 0px" class="box_input">
                                        <div class="label">Mức tiền giảm tối đa</div>
                                        <input class="input"  name="reduced_amount1"  id="uang5" type="text"value="{{number_format($tbl_type_discount->reduced_amount,0,',',',')}}" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row_input">
                        <div class="box_input">
                            <div class="label">Nhà cung cấp dịch vụ</div>
                            <div class="box_help_text_supplier">
                           <span style="
                              font-family: 'Montserrat';
                              font-style: normal;
                              font-weight: 400;
                              font-size: 14px;
                              line-height: 20px;
                              color: #666666;
                              ">Các đối tác cung cấp dịch vụ trên hệ thống cho
                           Heyotrip</span>
                                <span style="
                              font-family: 'Montserrat';
                              font-style: normal;
                              font-weight: 500;
                              font-size: 14px;
                              line-height: 20px;
                              text-align: right;
                              color: #007bff;
                              cursor: pointer;
                              " id="del_service">
                           Xoá hết
                           </span>
                            </div>
                            <!-- <div class="box_discount_chosen"> -->
                            <!-- <div class="item">
                               Travelport<span
                                 style="
                                   cursor: pointer;
                                   margin-left: 5px;
                                   width: 15px;
                                   height: 15px;
                                 "
                                 class="iconify"
                                 data-icon="eva:close-outline"
                               ></span>
                               </div> -->
                            <?Php

                            if($promo->services!=null){


                                $listSupplier = [];
                                $supplier =explode(',',$promo->services);
                                if(is_array($supplier)){
                                    foreach($supplier as $value){
                                        if($value=="TVP"){
                                            Session::put('TVP','TVP');
                                        }if($value=="EZC"){
                                            Session::put('EZC','EZC');
                                        }if($value=="HPL"){
                                            Session::put('HPL','HPL');
                                        }if($value=="VJ"){
                                            Session::put('VJ','VJ');
                                        }
                                    }
                                }
                            }

                            ?>
                            <select class='js-select2-multi'   name="services[]" id="service" multiple='multiple'>
                                <option @if(Session::get('TVP')!=null) selected @endif>TVP</option>
                                <option @if(Session::get('EZC')!=null) selected @endif>EZC</option>
                                <option  @if(Session::get('HPL')!=null) selected @endif>HPL</option>
                                <option  @if(Session::get('VJ')!=null) selected @endif>VJ</option>
                            </select>
                            <!-- </div> -->
                        </div>
                    </div>
                    <div class="row_input" id="payment">
                        <div class="box_input">
                            <div class="label">Đối tác thanh toán</div>
                            <div class="box_help_text_supplier">
                           <span style="
                              font-family: 'Montserrat';
                              font-style: normal;
                              font-weight: 400;
                              font-size: 14px;
                              line-height: 20px;
                              color: #666666;
                              ">Các đối tác thanh toán trên hệ thống cho
                           Heyotrip</span>
                                <span style="
                              font-family: 'Montserrat';
                              font-style: normal;
                              font-weight: 500;
                              font-size: 14px;
                              line-height: 20px;
                              text-align: right;
                              color: #007bff;
                              cursor: pointer;
                              " id="del_payment_partner">
                           Xoá hết
                           </span>
                            </div>
                            <!-- <div class="box_discount_chosen"> -->
                            <!-- <div class="item">
                               Travelport<span
                                 style="
                                   cursor: pointer;
                                   margin-left: 5px;
                                   width: 15px;
                                   height: 15px;
                                 "
                                 class="iconify"
                                 data-icon="eva:close-outline"
                               ></span>
                               </div> -->
                            <select class='js-select2-multi'   name="payment_partner[]" id="payment_partner" multiple='multiple'>
                                <?php
                                $partner_payment=DB::table('tbl_partner_payment')->get();

                                ?>
                                @foreach($partner_payment as $p)
                                    <option>{{$p->name}}</option>
                                @endforeach
                            </select>
                            <!-- </div> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="wrapper">
                <div class="box_title">
                    <div class="title">Điều kiện đơn hàng áp dụng</div>
                </div>
                <div style="margin: 0px" class="dropdown-divider"></div>
                <div class="content">
                    <div class="row_input">
                        <div class="box_input">
                            <div class="label">Quốc gia đi</div>
                            <select  name="location_from" class='js-select2-multi form-control' id="location_from">
                                <option value='0'>Tất cả</option>

                                @foreach($locations as $al)


                                    <option @if($promo->id_location_from==$al->id) selected @endif value="{{$al->id}}"> {{$al->country_name_vn}}</option>
                                @endforeach
                                <?php
                                Session::put('location_from','')
                                ?>
                            </select>
                        </div>
                        <div class="box_input">
                            <div class="label">Quốc gia đến</div>
                            <select class='js-select2-multi form-control'  name="location_to"  id="location_to">
                                <option value='0'>Tất cả</option>

                                @foreach($locations as $al)

                                    <option @if($promo->id_location_to==$al->id) selected @endif value="{{$al->id}}"> {{$al->country_name_vn}} </option>
                                @endforeach
                                <?php
                                Session::put('location_to','')
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row_input">
                        <div class="box_input">
                            <div class="label">Thành phố đi  </div>
                            <select  name="city_from" class='js-select2-multi form-control' id="city_from">
                                <option value='0'>Tất cả</option>
                                @if($promo->id_city)
                                    <?php
                                    $suppliers=DB::table('cities')->get();
                                    $listSupplier = [];
                                    $supplier =  $promo->city_from;

                                    $data=DB::table('cities')->where('id',$promo->id_city)->first();

                                    $listSupplier[] =$data->city_name;

                                    ?>
                                    @if(!empty($suppliers))
                                        @foreach($suppliers as $k => $v)
                                            <option value="{{$v->id}}" {{ (count($listSupplier) && in_array($v->city_name, $listSupplier))?'selected':'' }}> {{ $v->city_name}}  </option>
                                        @endforeach
                                    @endif
                                @endif

                            </select>



                            <?php
                            Session::put('city_from','')
                            ?>

                            <div id="dataErrorcityFrom"></div>
                        </div>
                        <div class="box_input">
                            <div class="label">Thành phố đến </div>
                            <select class='js-select2-multi form-control'  name="city_to"  id="city_to">
                                <option value='0'>Tất cả</option>
                                @if($promo->id_city_to)
                                    <?php

                                    $suppliers=DB::table('cities')->get();
                                    $listSupplier = [];

                                    $data=DB::table('cities')->where('id',$promo->id_city_to)->first();

                                    $listSupplier[] =$data->city_name;

                                    ?>
                                    @if(!empty($suppliers))
                                        @foreach($suppliers as $k => $v)
                                            <option value="{{$v->id}}" {{ (count($listSupplier) && in_array($v->city_name, $listSupplier))?'selected':'' }}> {{ $v->city_name}}  </option>
                                        @endforeach
                                    @endif
                                @endif

                                <?php
                                Session::put('city_to','')
                                ?>
                            </select>
                            <div id="dataErrorcity"></div>

                        </div>
                    </div>
                    <div class="row_input">
                        <div class="box_input">
                            <div class="label">Sân bay đi</div>
                            <select  name="airport_from" class='js-select2-multi form-control' id="airport_from">
                                <option value='0'>Tất cả</option>
                                @if($promo->id_airport_from)
                                <?php

                                    $airports=DB::table('airports')->get();?>
                                    @foreach($airports as $al)


                                        <option @if($promo->id_airport_from==$al->id) selected @endif value="{{$al->id}}"> {{$al->airport_name_vi}}</option>
                                    @endforeach

                                @endif
                                <?php
                                Session::put('airport_from','')
                                ?>
                            </select>                            <div id="dataErrorAirportFrom"></div>


                        </div>
                        <div class="box_input">
                            <div class="label">Sân bay đến</div>
                            <select class='js-select2-multi form-control'  name="airport_to"  id="airport_to">
                                <option value='0'>Tất cả</option>
                                @if($promo->id_airport_to)

                                    <?php

                                    $airports=DB::table('airports')->get();?>
                                    @foreach($airports as $al)


                                        <option @if($promo->id_airport_to==$al->id) selected @endif value="{{$al->id}}"> {{$al->airport_name_vi}}</option>
                                    @endforeach
                                @endif
                                <?php
                                Session::put('airport_to','')
                                ?>
                            </select>
                            <div id="dataErrorAirport"></div>
                        </div>
                    </div>
                    <div class="row_input">
                        <div class="box_input">
                            <div class="label">Ngày khởi hành từ</div>
                            <div class="wrapper_date">
                                <div style="width: 100%; margin-top: 5px" class="input-group date" id="datetimepicker3">
                                    <input type="text"  name="checkinFrom" required id="checkinFrom" class="form-control"value="{{$promo->checkinFrom}}" value="" />
                                    <span class="input-group-addon">
                              <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                                </div>

                            </div>
                            @if(Session::get('checkinFromE'))
                                <br>
                                <span class="alert alert-danger  ">{{Session::get('checkinFromE')}}</span>
                                <?php Session::put('checkinFromE','')?>

                            @endif
                        </div>
                        <div class="box_input">
                            <div class="label">Ngày khởi hành đến</div>
                            <div class="wrapper_date">
                                <div style="width: 100%; margin-top: 5px" class="input-group date" id="datetimepicker4">
                                    <input type="text" name="checkinTo" required id="checkinTo" class="form-control"value="{{$promo->checkinTo}}" />
                                    <span class="input-group-addon">
                              <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                                </div>

                            </div>
                            @if(Session::get('ErrorcheckinTo'))
                                <br>
                                <span class="alert alert-danger">{{Session::get('ErrorcheckinTo')}}</span>
                                <?php Session::put('ErrorcheckinTo','')?>

                            @endif
                        </div>
                    </div>
                    <div class="row_input">
                        <div class="box_input">
                            <div style="
                           display: flex;
                           align-items: center;
                           justify-content: space-between;
                           ">
                                <div class="label">
                                    Giá trị đơn hàng tối thiểu phải đạt<span class="star">*</span>
                                </div>
                                <div style="display: flex; align-items: center" class="form-check">
                                    <input style="width: 15px; height: 15px" class="form-check-input" type="checkbox" name="check_box_nolimit"   id="check_box_nolimit"
                                           id="radioPrice" />
                                    <label style="
                                 font-family: 'Montserrat';
                                 font-style: normal;
                                 font-weight: 400;
                                 font-size: 15px;
                                 color: #666666;
                                 margin-left: 10px;
                                 margin-top: 6px;
                                 " class="form-check-label" for="radioPrice">
                                        Không giới hạn
                                    </label>
                                </div>
                            </div>
                            <div style="display: flex; margin-top: 5px">
                                <input class="input" name="value_order_min" value="{{number_format($promo->value_order_min,0,',',',')}}" id="min_value" />
                                <div style="
                              background: #e9ecef;
                              border: 1px solid #dee2e6;
                              border-radius: 5px;
                              margin-top: 5px;
                              font-family: 'Montserrat';
                              font-style: normal;
                              font-weight: 400;
                              font-size: 13px;
                              line-height: 20px;
                              color: #666666;
                              padding: 5px 10px;
                              display: flex;
                              align-items: center;
                              justify-content: center;
                              ">
                                    VND
                                </div>
                            </div>
                        </div>
                        <div style="margin-top: 10px" class="box_input">
                            <div class="label">Số lượng khách hàng tối thiểu</div>
                            <input style="margin-top: 12px" class="input" name="number_user_min"id="number_user_min"  value="{{$promo->number_user_min}}" />
                        </div>
                    </div>
                </div>
                <div class="box_title">
                    <div class="title"></div>
                    <div class="box_option">
                        <button type="submit" class="btn button_save">Lưu</button>
                        <button class="btn button_watch" type="button" data-toggle="modal" data-target="#modal-review">Xem trước</button>
                    </div>
                </div>

            </div>
        </div>
    </form>
</div>
</body>
</html>
<script>
    $('.js-example-basic-single').select2();
</script>

<script>
    $(document).ready(function () {

        document.getElementById("uang2").required = true;
        function xoa_dau(str) {
            str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
            str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
            str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
            str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
            str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
            str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
            str = str.replace(/đ/g, "d");
            str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
            str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
            str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
            str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
            str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
            str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
            str = str.replace(/Đ/g, "D");
            return str;
        }
        $("#code_program").css('background','#eee')
        $('#code_pro').on('input', function() {
            var str= $('#code_pro').val();
            str=xoa_dau(str);

            $('#code_pro').val( str.toUpperCase());
        });
        $("#payment").addClass('none');

        $("#check_box_nolimit").change(function () {

            var checked = $(this).is(":checked");

            if (checked) {
                $("#min_value").prop('disabled',true);
            }else{
                $("#min_value").prop('disabled',false);
            }
        })
        $('#flexRadioDefault2_2').on('change', function() {
            $("#giamgiatheophantram").removeClass('none');
            $("#giamgiacodinh").addClass('none');
        });
        $('#flexRadioDefault1_2').on('change', function() {
            $("#giamgiatheophantram").addClass('none');
            $("#giamgiacodinh").removeClass('none');
        });

        $("#title_create").keyup(function () {
            var title =$(this).val();
            console.log(title);

            $.ajax({
                url:'http://localhost/testlocal/admin/hotel/customlink',
                method:"get",
                data:{title:title},
                success:function (data){
                    $("#link").val(data);

                }
            });
        });
        $('#nation').on('change', function() {

            var nation =$(this).val();
            $.ajax({
                url:'http://localhost/testlocal/admin/hotel/loadcity',
                method:"get",
                data:{nation:nation},
                success:function (data){
                    $("#result_city").html(data);

                }
            });
        });
        $('#result_city').on('change', function() {

            var result_city =$('#result_city').val();
            $.ajax({
                url:'http://localhost/testlocal/admin/hotel/loadhotelcity',
                method:"post",
                data:{result_city:result_city},
                success:function (data){
                    $("#hotel_apply").html(data);

                }
            });
        });
        $("#delete_search").click(function () {
            $('.form-control').val('');
        });
        $("#xoahotel_apply").click(function () {

            $('#hotel_apply').val('');
        });


        $("#code_pro").keyup(function () {
            var today = new Date();
            var  cod_pro=$("#code_pro").val().toUpperCase();
            var date = today.getDate()+''+(today.getMonth()+1)+""+today.getFullYear();
            var data=date+" F-"+cod_pro;
            $("#code_program").val(data);
        });


        document.getElementById("uang4").required = true;
        $("#radioDiscount2").click(function(){
            document.getElementById("percentage_reduction").required = true;
            document.getElementById("uang4").required = false;
        })
        $("#radioDiscount1").click(function(){
            console.log("tahnh "+11);
            document.getElementById("percentage_reduction").required = false;
            document.getElementById("uang4").required = true;
        })
        $('#type_promo').change(function () {
            var type_promo=$('#type_promo').val();


            if(type_promo==4){
                document.getElementById("box_discount1").style = "display: none";
                document.getElementById("box_discount2").style = "display: flex";
                $('#hotel_apply').attr('required',true)
            }else {
                $('#hotel_apply').attr('required',false)

                document.getElementById("box_discount1").style = "display: block";
                document.getElementById("box_discount2").style = "display: none";
            }
            if(type_promo==1){
                $("#uang2").prop('disabled',false);
                $("#partner").addClass('none');



            }



            if(type_promo==4){
                console.log('sss');
                $("#checkinTo").attr('disabled',true);
                $("#checkinFrom").attr('disabled',true);
                $("#min_value").attr('disabled',true);
                $("#number_user_min").attr('disabled',true);
                $("#uang_create").attr('disabled',true);
                $("#uang5").attr('disabled',true);
                $("#code_pro").attr('disabled',true);
                $("#code_program").attr('disabled',true);
                $("#uang5").css('background','#eee');
                $("#min_value").css('background','#eee');
                $("#code_pro").css('background','#eee');
                $("#code_program").css('background','#eee');
                $("#number_user_min").css('background','#eee');
                $("#uang2").css('background','#eee');
                $("#uang_create").css('background','#eee');
                $("#percentage_reduction").attr('required','true')
                document.getElementById("uang4").required = false;


                $("#display").attr('disabled',true);
                $("#display1").attr('disabled',true);
                $("#radioDiscount2").prop('checked', true);
                $("#radioDiscount1").prop('checked', false);
                $("#display1").prop('checked', false);
                $("#display").prop('checked', false);
                $("#box_discount1").addClass('none');
                $("#box_discount2").removeClass('none');
                $("#uang_create").attr('placeholder','');
                $("#checkinFrom").val('');
                $("#checkinTo").val('');
                $("#uang2").attr('placeholder','');
                $("#uang5").attr('placeholder','');
                $("#min_value").attr('placeholder','');
                $("#number_user_min").attr('placeholder','');
                $("#code_program").attr('placeholder','');
                $("#code_program").val('');
                $("#code_pro").val('');

            }else{


                $("#uang5").css('background','none');
                $("#code_pro").attr('disabled',false);
                $("#code_program").attr('disabled',false);
                $("#uang5").css('background','#fff');
                $("#min_value").css('background','#fff');

                $("#min_value").css('background','#fff');
                $("#uang2").css('background','#fff');
                $("#uang_create").css('background','#fff');
                $("#number_user_min").css('background','#fff');

                $("#display").attr('disabled',false);
                $("#display1").attr('disabled',false);
                $("#checkinTo").attr('disabled',false);
                $("#checkinFrom").attr('disabled',false);
                $("#min_value").attr('disabled',false);
                $("#number_user_min").attr('disabled',false);
                $("#uang_create").attr('disabled',false);
                $("#uang5").attr('disabled',false);
                $("#radioDiscount1").prop('checked', true);
                $("#radioDiscount22").prop('checked', false);
                $("#box_discount1").removeClass('none');
                $("#box_discount2").addClass('none');

                $("#display1").prop('checked', false);
                $("#display").prop('checked', true);

                $("#uang_create").attr('placeholder','Nhập');
                $("#uang2").attr('placeholder','Nhập');
                $("#uang5").attr('placeholder','Nhập');
                $("#min_value").attr('placeholder','Nhập');
                $("#code_program").attr('placeholder','Nhập');
                $("#number_user_min").attr('placeholder','Nhập');



                $("#code_program").css('background',"#eee");
                $("#code_pro").css('background',"#fff");
                var today = new Date();
                var date = today.getDate()+'-'+(today.getMonth()+1)+'-'+today.getFullYear();
                var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
                var dateTime = date+' '+time;


                $("#checkinFrom").val(dateTime);
                $("#checkinTo").val(dateTime);
            }
            if(type_promo==2){
                console.log(12);
                $( "#flexRadioDefault1" ).prop('checked', false);
                $( "#flexRadioDefault2" ).prop('checked', true);
                $("#uang2").css('background','#eee');
                $("#uang2").attr('placeholder','');

            }
            if(type_promo==2){
                $("#display").attr('disabled',true);

                $("#display1").prop('checked', true);
                $("#display").prop('checked', false);
            }
            if(type_promo!=4){
                $("#checkinTo").attr('disabled',false);
                $("#checkinFrom").attr('disabled',false);
                $("#min_value").attr('disabled',false);
                $("#number_user_min").attr('disabled',false);



            }
            if(type_promo==2){
                $("#flexRadioDefault1").prop('disabled',true);

                $("#class_service").addClass('none');

                $("#code_pro").attr('disabled',false);
                $("#code_program").css('background',"#eee");
                $("#code_pro").css('background',"#fff");

            }

            if(type_promo==2||type_promo==3||type_promo==4){
                $("#uang2").prop('disabled',true);
                $("#class_service").removeClass('none');



            }
            if(type_promo==3){
                $( "#partner" ).removeClass('none');
                $( "#uang2" ).attr('disabled',false);
                $( "#uang2" ).attr('disabled',false);
                $( "#service" ).attr('required',true);
                $( "#payment_partner" ).attr('required',true);

                $("#code_pro").attr('disabled',false);

            }else{
                $( "#service" ).attr('required',false);
                $( "#payment_partner" ).attr('required',false);

            }
            if(type_promo==4||type_promo==2){
                $( "#payment" ).addClass('none');


            }
            if(type_promo==3){
                $("#payment").removeClass('none');
            }
            if(type_promo==4){

                $( "#flexRadioDefault2_2" ).prop('checked', true);
                $( "#flexRadioDefault1_2" ).prop('checked', false);
                $( "#giamgiatheophantram" ).removeClass('none');
                $( "#giamgiacodinh" ).addClass('none');
                $( "#service" ).attr('required',true);
                $( "#result_city" ).attr('required',true);
                $( "#nation" ).attr('required',true);
                $( "#hotel_apply" ).attr('required',true);


            }else{

                $( "#result_city" ).attr('required',false);
                $( "#nation" ).attr('required',false);
                $( "#hotel_apply" ).attr('required',false);
            }
            if(type_promo==1||type_promo==2){
                $("#code_pro").attr('disabled',false);
                $("#code_pro").css('background',"#fff");

                $("#code_program").attr('disabled',false);
                $( "#service" ).attr('required',false);


            }
            if(type_promo==1||type_promo==3){
                $( "#uang2" ).attr('required',true);

            }else{
                $( "#uang2" ).attr('required',false);

            }
        })
        $(function(){

            $("#uang").keyup(function(e){
                $(this).val($(this).val().replace(/[^0-9]/g, ''));
                $(this).val(format($(this).val()));

            });
            $("#uang2").keyup(function(e){
                $(this).val($(this).val().replace(/[^0-9]/g, ''));
                $(this).val(format($(this).val()));

            });
            $("#number_user_min").keyup(function(e){
                $(this).val($(this).val().replace(/[^0-9]/g, ''));
                $(this).val(format($(this).val()));

            });
            $("#uang_create").keyup(function(e){

                $(this).val($(this).val().replace(/[^0-9]/g, ''));
                $(this).val(format($(this).val()));


            });
            $("#uang3").keyup(function(e){
                $(this).val($(this).val().replace(/[^0-9]/g, ''));
                $(this).val(format($(this).val()));

            });
            $("#uang4").keyup(function(e){
                $(this).val($(this).val().replace(/[^0-9]/g, ''));
                $(this).val(format($(this).val()));

            });


            $("#uang5").keyup(function(e){
                $(this).val($(this).val().replace(/[^0-9]/g, ''));
                $(this).val(format($(this).val()));

            });
            $("#min_value").keyup(function(e){
                $(this).val($(this).val().replace(/[^0-9]/g, ''));
                $(this).val(format($(this).val()));
            });
        });
        var format = function(num){
            var str = num.toString().replace("", ""), parts = false, output = [], i = 1, formatted = null;
            if(str.indexOf(".") > 0) {
                parts = str.split(".");
                str = parts[0];
            }
            str = str.split("").reverse();
            for(var j = 0, len = str.length; j < len; j++) {
                if(str[j] != ",") {
                    output.push(str[j]);
                    if(i%3 == 0 && j < (len - 1)) {
                        output.push(",");
                    }
                    i++;
                }
            }
            formatted = output.reverse().join("");
            return("" + formatted + ((parts) ? "." + parts[1].substr(0, 2) : ""));
        };

    })
</script>
<script>
    $(document).ready(function () {
        $('.button_watch').click(function(){
            alert('This function update last');
        });
        $('#class_service input').keyup(function () {
            var  a=$('#class_service input').val();
            var tach=count(a.split(" "));

            $("#numberservice").text(a);
        });
    });
</script>


@if(Session::get('date_start')=='')
    <script>
        $(document).ready(function () {
            $("#datetimepicker1").datetimepicker({
                format: "DD-MM-YYYY LT",
                minDate: new Date(),
            });
        });
    </script>
@endif

@if(Session::get('date_start')!='')
    <script>

        $(document).ready(function () {
            $("#datetimepicker1").datetimepicker({
                format: "DD-MM-YYYY LT"
            });
        });
    </script>
@endif
@if(Session::get('date_end')=='')
    <script>

        $(document).ready(function () {
            $("#datetimepicker2").datetimepicker({
                format: "DD-MM-YYYY LT",
                minDate: new Date(),
            });
        });
    </script>
@endif
@if(Session::get('date_end')!='')
    <script>

        $(document).ready(function () {
            console.log('12');
            $("#datetimepicker2").datetimepicker({
                format: "DD-MM-YYYY LT"
            });
        });
    </script>
@endif

@if(Session::get('checkinFrom')=='')
    <script>

        $(document).ready(function () {
            $("#datetimepicker3").datetimepicker({
                format: "DD-MM-YYYY LT",
                minDate: new Date(),
            });
        });
    </script>
@endif

@if(Session::get('checkinFrom')!='')
    <script>

        $(document).ready(function () {
            $("#datetimepicker3").datetimepicker({
                format: "DD-MM-YYYY LT"
            });
        });
    </script>
@endif
@if(Session::get('checkinTo')=='')
    <script>


        $(document).ready(function () {
            $("#datetimepicker4").datetimepicker({
                format: "DD-MM-YYYY LT",
                minDate: new Date(),
            });
        });
    </script>
@endif
@if(Session::get('checkinTo')!='')
    <script>


        $(document).ready(function () {
            $("#datetimepicker4").datetimepicker({
                format: "DD-MM-YYYY LT"
            });
        });
    </script>
@endif

<script>

    $('#del_payment_partner').click(function () {
        $("#payment_partner").val(null).trigger("change");

    });
    $('#del_hotel').click(function () {
        $("#hotel_apply").val(null).trigger("change");

    });
    $('#del_service').click(function () {
        $("#service").val(null).trigger("change");

    });
</script>

<script>

    const query = document.querySelector.bind(document);

    const removeComma = string => string.slice(0, string.length - 1).trim();



    const isInvalid = stringInput => {
        const inputs = Array.from(query('.tags').children).map(input => input.firstElementChild.textContent);

        return !/^[A-Za-z0-9]{3,}/.test(stringInput) ||
            inputs.some(name => name === removeComma(stringInput)) ||
            query('.tags').children.length >= 10;
    };



    function modifyTags(e) {
        if (e.key === ',') {

            if (isInvalid(e.target.value)) {
                e.target.value = '';
                return;
            }

            addTag(e.target.value);
            e.target.value = '';

        }

        if (e.key === 'Backspace' && !e.target.value.length) {
            deleteTag(null, query('.tags').children.length - 1);
        }

        query('.tags-count').textContent = `${1 + query('.tags').children.length}`;
    }



    function addTag(textValue) {
        const tag = document.createElement('div'),
            tagName = document.createElement('label'),
            remove = document.createElement('span');

        tagName.setAttribute('class', 'tag-name');
        tagName.textContent = removeComma(textValue);

        remove.setAttribute('class', 'remove');
        remove.textContent = 'X';
        remove.addEventListener('click', deleteTag);

        tag.setAttribute('class', 'tag');
        tag.appendChild(tagName);
        tag.appendChild(remove);

        query('.tags').appendChild(tag);
    }



    function deleteTag(e, i = Array.from(query('.tags').children).indexOf(e.target.parentElement)) {
        const index = query('.tags').getElementsByClassName('tag')[i];

        query('.tags').removeChild(index);
        query('.tags-count').textContent = `${10 - query('.tags').children.length}`;
    }



    function focus() {
        query('#tag').focus();
    }



</script>
<script type="text/javascript">
    function limitText(limitField, limitCount, limitNum) {
        if (limitField.value.length > limitNum) {
            limitField.value = limitField.value.substring(0, limitNum);
        } else {
            limitCount.value = limitNum - limitField.value.length;
        }
    }

</script>
<style>
    .group-info-pro,.group-setting-pro,.group-con-pro{
        border: 1px solid;
        padding: 10px;
        border-radius: 10px;
    }
    .none{
        display: none!important;}
    a{
        color:#333
    }
    a:hover{

        text-decoration: none;
    }
</style>
@if(Session::get('display')==1&&Session::get('type_promo')==2)
    <script>

        $("#display").attr('disabled',true);
        $("#display1").attr('disabled',true);

        $("#display1").prop('checked', true);
        $("#display").prop('checked', false);
    </script>
@endif
@if(Session::get('type_promo')==4)
    <script>
        $(document).ready(function(){
            console.log('sss');
            $("#checkinTo").attr('disabled',true);
            $("#checkinFrom").attr('disabled',true);
            $("#min_value").attr('disabled',true);
            $("#number_user_min").attr('disabled',true);
            $("#uang_create").attr('disabled',true);
            $("#uang5").attr('disabled',true);
            $("#code_pro").attr('disabled',true);
            $("#code_program").attr('disabled',true);
            $("#uang5").css('background','#eee');
            $("#min_value").css('background','#eee');
            $("#code_pro").css('background','#eee');
            $("#code_program").css('background','#eee');
            $("#number_user_min").css('background','#eee');
            $("#uang2").css('background','#eee');
            $("#uang_create").css('background','#eee');
            $("#percentage_reduction").attr('required','true')
            document.getElementById("uang4").required = false;


            $("#display").attr('disabled',true);
            $("#display1").attr('disabled',true);
            $("#radioDiscount2").prop('checked', true);
            $("#radioDiscount1").prop('checked', false);
            $("#display1").prop('checked', false);
            $("#display").prop('checked', false);
            $("#box_discount1").addClass('none');
            $("#box_discount2").removeClass('none');
            $("#uang_create").attr('placeholder','');
            $("#checkinFrom").val('');
            $("#checkinTo").val('');
            $("#uang2").attr('placeholder','');
            $("#uang5").attr('placeholder','');
            $("#min_value").attr('placeholder','');
            $("#number_user_min").attr('placeholder','');
            $("#code_program").attr('placeholder','');
            $("#code_program").val('');
            $("#code_pro").val('');

        });
    </script>
@endif
<?php

Session::put('Errordate_start','');
Session::put('date_start','')?>
<?php Session::put('Errordate_end','');
Session::put('date_end','')?>
<?php
Session::put('checkinTo','')
?>
<?php
Session::put('checkinFrom','')
?>
<?php
Session::put('services','')
?>
<?php
Session::put('TVP','');
Session::put('EZC','');
Session::put('HPL','');
Session::put('VJ','');
?>


<script>
    $(document).ready(function () {
        $('#location_from').change(function () {
            $.ajax({
                url : "{{asset('admin/load_city_by_location')}}",
                type : "get",
                dataType:"text",
                data : {
                    location_from :$('#location_from').val()
                },
                success : function (result){
                    $('#city_from').html(result);
                }
            });
        });
        $('#city_from').change(function () {

            let city_from= $('#city_from').val();
            let city_to= $('#city_to').val();
            if(city_from==city_to){
                $('#dataErrorcityFrom').html('<p style="color:red;">Lỗi ! Thành phố đến không được trừng với thành phố đi</p>');
            }else {
                $('#dataErrorcity').html('');

                $.ajax({
                    url : "{{asset('admin/load_airport_by_city')}}",
                    type : "get",
                    dataType:"text",
                    data : {
                        city_from :$('#city_from').val()
                    },
                    success : function (result){
                        $('#airport_from').html(result);
                    }
                });}
        });
        $('#location_to').change(function () {
            let location_to=$('#location_to').val();
            let location_from=$('#location_from').val();

            $.ajax({
                url: "{{asset('admin/load_city_by_location_to')}}",
                type: "get",
                dataType: "text",
                data: {
                    city_from: $('#city_from').val(),
                    location_to: $('#location_to').val(),
                },
                success: function (result) {
                    $('#city_to').html(result);
                }
            });
        });

        $('#city_to').change(function () {
            let city_from= $('#city_from').val();
            let city_to= $('#city_to').val();
            if(city_from==city_to){
                $('#dataErrorcity').html('<p style="color:red;">Lỗi ! Thành phố đến không được trừng với thành phố đi</p>');
            }else {
                $('#dataErrorcity').html('');
                $.ajax({
                    url: "{{asset('admin/load_airport_by_city_to')}}",
                    type: "get",
                    dataType: "text",
                    data: {
                        city_from: $('#city_from').val(),
                        city_to: $('#city_to').val()
                    },
                    success: function (result) {
                        $('#airport_to').html(result);
                    }
                });
            }
        });
        $('#airport_to').change(function () {
            let airport_from= $('#airport_from').val();
            let airport_to= $('#airport_to').val();
            if(airport_to==airport_from){
                $('#dataErrorAirport').html('<p style="color:red;">Lỗi ! Sân bay đến không được trừng với sân bay đi</p>');
            }else{
                $('#dataErrorAirport').html('');

            }
        });
        $('#airport_from').change(function () {
            let airport_from= $('#airport_from').val();
            let airport_to= $('#airport_to').val();
            if(airport_to==airport_from){
                $('#dataErrorAirportFrom').html('<p style="color:red;">Lỗi ! Sân bay đến không được trừng với sân bay đi</p>');
            }else{
                $('#dataErrorAirportFrom').html('');

            }
        });
    })</script>

