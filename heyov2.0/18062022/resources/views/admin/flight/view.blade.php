<!DOCTYPE html>
<html lang="en">

<head>
    <title>HF Admin</title>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('public/admin/css/detail.css')}}" />
    <link rel="stylesheet" href="{{asset('public/admin/css/bootstrap.min.css')}}" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="./js/bootstrap.min.js"></script>
    <script src="https://code.iconify.design/2/2.2.1/iconify.min.js"></script>
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css"
          rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
    <script src="{{asset('public/admin/js/main.js')}}"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <style>
        .toggle.ios,
        .toggle-on.ios,
        .toggle-off.ios {
            border-radius: 20rem;
        }

        .toggle.ios .toggle-handle {
            border-radius: 20rem;
        }
    </style>
</head>

<body>
<div class="menu_header">
    <div class="left">
        <a href="#" class="logo">HEYO TRIP</a>
    </div>
    <div class="right">
        <span class="name_menu"> Chương trình khuyến mãi </span>
        <div class="box_admin">
        <span class="iconify" style="width: 25px; height: 25px; color: #032044"
              data-icon="ic:baseline-notifications-none"></span>
            <div class="badge badge-danger counter">9</div>
            <div id="btn_admin" class="wrapper_admin">
                <span style="width: 25px; height: 25px; color: #032044" class="iconify" data-icon="lucide:user"></span>
                <span class="admin">Admin</span>
                <span style="
                width: 15px;
                height: 15px;
                color: #032044;
                margin-left: 10px;
              " class="iconify" data-icon="ant-design:caret-down-filled"></span>
            </div>
        </div>
    </div>
    <div id="profile" style="box-shadow: 10" class="box_profile">
      <span style="width: 30px; height: 30px; color: #032044" class="iconify"
            data-icon="simple-line-icons:logout"></span>
        <span class="logout"><a href="{{asset('logout')}}" style="text-decoration: none;color: black"> Đăng xuất</a></span>
    </div>
</div>
<div style="display: flex">
<div class="menu_left">
        <div class="top">
            <a href="{{asset('admin/hotel/order/all/1')}}" style=" text-decoration: none" class="item">
                    <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                          data-icon="icon-park-outline:hotel"></span>
                <span style="color: #526a87" class="name_menu">Khách sạn</span>
            </a>
            <a href="{{asset('admin/flight/order/all/1')}}" style="background-color: #f2f9ff; text-decoration: none" class="item">
                <span class="iconify" style="color: #526a87; width: 30px; height: 30px" data-icon="ps:plane"></span>
                <span  style="color: #526a87"class="name_menu">Vé máy bay</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
                <span class="iconify" style="color: #526a87; width: 30px; height: 30px" data-icon="clarity:car-line"></span>
                <span style="color: #526a87" class="name_menu">Thuê xe</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
               <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                     data-icon="clarity:data-cluster-line"></span>
                <span style="color: #526a87" class="name_menu">Crypto</span>
            </a>
        </div>
        <div class="top">
            <a href="#" style="text-decoration: none" class="item">
                <span class="iconify" style="color: #526a87; width: 30px; height: 30px" data-icon="ep:user-filled"></span>
                <span style="color: #526a87" class="name_menu">Tài khoản</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
               <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                     data-icon="dashicons:welcome-widgets-menus"></span>
                <span style="color: #526a87" class="name_menu">Danh mục</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
                <span class="iconify" style="color: #526a87; width: 30px; height: 30px" data-icon="gg:website"></span>
                <span style="color: #526a87" class="name_menu">Website</span>
            </a>
            <a href="{{asset('admin/website/list/quocgia')}}" style="text-decoration: none" class="item">
               <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                     data-icon="material-symbols:settings-suggest-outline-sharp"></span>
                <span style="color: #526a87" class="name_menu">Cấu hình</span>
            </a>
        </div>
    </div>
    <div class="menu_right">
        <div class="item">
               <span style="width: 25px; height: 25px; color: #032044" class="iconify"
                     data-icon="majesticons:analytics"></span>
            <span class="name_menu">Dashboard</span>
        </div>
        <a  href="{{asset('admin/hotel/searchHotel')}}"class="item">
            <span style="width: 25px; height: 25px; color: #032044" class="iconify" data-icon="mdi:bed"></span>
            <span class="name_menu">Tìm - Đặt phòng</span>
    </a>
        <a href="{{asset('admin/hotel/order/all/1')}}" class="item">
               <span style="width: 25px; height: 25px; color: #032044" class="iconify"
                     data-icon="fa-solid:calendar-day"></span>
            <span class="name_menu">Quản lý đơn đặt phòng</span>
    </a>
        <div class="item">
               <span style="width: 25px; height: 25px; color: #032044" class="iconify"
                     data-icon="fa-solid:money-check-alt"></span>
            <span class="name_menu">Tuỳ chỉnh giá phòng</span>
        </div>
        <div class="item">
               <span style="width: 25px; height: 25px; color: #329223" class="iconify"
                     data-icon="icomoon-free:price-tags"></span>
            <span  class="name_menu"><a style="color: #329223; font-weight: 800"href='{{asset("admin/hotel/list")}}'>Chương trình khuyến mãi</a></span>
        </div>
        <div class="item">
            <span style="width: 25px; height: 25px; color: #032044" class="iconify" data-icon="eva:percent-fill"></span>
            <span class="name_menu">Quản lý hoa hồng</span>
        </div>
        <div class="item">
               <span style="width: 25px; height: 25px; color: #032044" class="iconify"
                     data-icon="fa6-solid:hand-holding-dollar"></span>
            <span class="name_menu">Huỷ - Hoàn tiền</span>
        </div>
        <div class="item">
               <span style="width: 25px; height: 25px; color: #032044" class="iconify"
                     data-icon="fluent:news-20-filled"></span>
            <span class="name_menu">Bài viết Marketing</span>
        </div>
    </div>
    <div class="box_content">
        <div class="wrapper">
            <div class="box_title">
                <div class="title">Thông tin khuyến mãi</div>
                <div class="box_option">
                    <a href="{{asset('admin/hotel/edit/'.$promo->id_promo)}}" class="btn button_save">Chỉnh sửa</a>
                    <a href="{{asset('admin/flight/list')}}" class="btn button_close">Đóng</a>
                </div>
            </div>
            <div style="margin: 0px" class="dropdown-divider"></div>
            <div class="content">
                <div class="row_info">
                    <?php
                    $user=DB::table('tbl_admin')->where('id_admin',$promo->ud_user)->first();
                    ?>
                    <span class="label">Người tạo</span>
                    <span class="value">{{$user->admin_name}}</span>
                </div>
                <div class="row_info">
                    <span class="label">Phân hệ</span>
                    <span class="value">@if($promo->subsystem==0) Hotel @else Flight @endif</span>
                </div>
                <div class="row_info">
                    <span class="label">Loại khuyến mãi:</span>
                    <span class="value">@if($promo->type_promo==1) Promo code @elseif($promo->type_promo==2) Unique code @elseif($promo->type_promo==3) Partner code @else  Special Campaign @endif</span>
                </div>
                <div class="row_info">
                    <span class="label">Mã khuyến mãi:</span>
                    <span class="value">{{$promo->code_promotion}}</span>
                </div>
                <div class="row_info">
                    <span class="label">Mã chương trình:</span>
                    <span class="value">{{$promo->program_code_promo}}</span>
                </div>
                <div class="row_info">
                    <span class="label">Trạng thái:</span>
                    <span class="value">
              <span class="inactive">@if($promo->status==0)Inactive @elseif($promo->status==1) Active @elseif($promo->status==2) Deactive @else Expired @endif</span>
            </span>
                </div>
                <div class="row_info">
                    <span class="label">Tên chương trình khuyến mãi:</span>
                    <span class="value">{{$promo->title}}</span>
                </div>
                <div class="row_info">
                    <span class="label">Nội dung khuyến mãi:</span>
                    <span class="value">{{$promo->content}}
            </span>
                </div>
                <div class="row_info">
                    <span class="label">Hình ảnh Cover:</span>
                    <span class="value">
              <img src="{{$promo->images_cover}}" width="250px" class="cover" />
            </span>
                </div>
                <div class="row_info">
                    <span class="label">Hình ảnh Logo:</span>
                    <span class="value"><img src="{{$promo->images_logo}}" width="100px" class="logo" /></span>
                </div>
            </div>
        </div>
        <div class="wrapper">
            <div class="box_title">
                <div class="title">Thiết lập khuyến mãi</div>
                <div class="content"></div>
            </div>
            <div style="margin: 0px" class="dropdown-divider"></div>
            <div class="content">
                <div class="row_info">
                    <div class="label">
                        Kiểu hiển thị trên ứng dụng:
                    </div>
                    <div class="value">
                        @if($promo->display==0)                Hiển thị trong danh sách
                        @else                                                     Nhập mã để tìm
                        @endif
                    </div>
                </div>
                <div class="row_info">
                    <div class="label">
                        Đường dẫn hiển thị:
                    </div>
                    <div class="value">
                        {{$promo->link}}                    </div>
                </div>
                <div class="row_info">
                    <div class="label">
                        Ngày bắt đầu:
                    </div>
                    <div class="value">
                        {{$promo->date_start}}                    </div>
                </div>
                <div class="row_info">
                    <div class="label">
                        Ngày kết thúc:
                    </div>
                    <div class="value">
                        {{$promo->date_end}}                    </div>
                </div>
                <div class="row_info">
                    <div class="label">
                        Số lượng phát hành:
                    </div>
                    <div class="value">
                        {{number_format($promo->number_max,0,'.',',')}}                    </div>
                </div>
                <div class="row_info">
                    <div class="label">
                        Giới hạn sử dụng trong ngày:
                    </div>
                    <div class="value">
                        {{number_format($promo->number_limit_day,0,'.',',')}}                    </div>
                </div>
                <div class="row_info">
                    <div class="label">
                        Mức giảm theo %:
                    </div>
                    <?php
                    $type_discount=DB::table('tbl_type_discount')->where('id_type_discount',$promo->id_type_discount)->first();
                    ?>
                    <div class="value">
                        @if($type_discount->percentage_reduction==null) None @else {{$type_discount->percentage_reduction}} @endif
                    </div>
                </div>
                <div class="row_info">
                    <div class="label">
                        Mức tiền giảm tối đa:
                    </div>
                    <div class="value">
                        {{$type_discount->reduced_amount}} VND
                    </div>
                </div>
                <div class="row_info">
                    <div class="label">
                        Nhà cung cấp dịch vụ:
                    </div>
                    <div class="value">
                        <div class="box_item">
                            @if($promo->services==null) Tất cả nhà cung cấp trên hệ thống Heyotrip @else


                    <?php
                    $tach=explode(', ',$promo->services);
                    $data=[];
                    foreach($tach as $t){
                        if($t=='TVP'){
                            Session::put('TVP',"TVP");
                        }
                        if($t=='HPL'){
                            Session::put('HPL',"HPL");
                        }
                        if($t=='EZC'){
                            Session::put('EZC',"EZC");
                        }
                        if($t=='VJ'){
                            Session::put('VJ',"VJ");
                        }
                    }



                    ?>
                            @if(Session::get('TVP'))

                            <div class="item">{{Session::get('TVP')}}
                        </div>
                            @endif

                            @if(Session::get('HPL'))

<div class="item">{{Session::get('HPL')}}
</div>
@endif
@if(Session::get('EZC'))

<div class="item">{{Session::get('EZC')}}
</div>
@endif    @if(Session::get('VJ'))

<div class="item">{{Session::get('VJ')}}
</div>
@endif


                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper">
            <div class="box_title">
                <div class="title">
                    Điều kiện đơn hàng áp dụng
                </div>
                <div class="content"></div>
            </div>
            <div style="margin: 0px" class="dropdown-divider"></div>
            <div class="content">
                <div class="row_info">
                    <div class="label">
                        Quốc gia đi - đến:
                    </div>
                    <?php
                    $nationfrom=DB::table('locations')->where('id',$promo->id_location_from)->first();
                    $nationto=DB::table('locations')->where('id',$promo->id_location_to)->first();
                    ?>
                    <div class="value">
                        {{$nationfrom->name_vi}} -  {{$nationto->name_vi}}                  </div>
                </div>
                <div class="row_info">
                    <div class="label">
                        Thành phố đi - đến:
                    </div>
                    <?php
                        $name_cityfrom=DB::table('locations')->where('id',$promo->id_city)->first();
                        $name_cityto=DB::table('locations')->where('id',$promo->id_city_to)->first();
                    ?>
                    <div class="value">
                        <div class="box_item">
                                <div class="item">@if($name_cityfrom!=null && $name_cityto!=null){{$name_cityfrom->name_vi}} - {{$name_cityto->name_vi}} @endif</div>
                        </div>
                    </div>
                </div>
                <div class="row_info">
                    <div class="label">
                        Sân bay đi - đến:
                    </div>
                    <?php
                        $name_airport_from=DB::table('airports')->where('id',$promo->id_airport_from)->first();
                        $name_airport_to=DB::table('airports')->where('id',$promo->id_airport_to)->first();
                    ?>
                    <div class="value">
                        <div class="box_item">
                                <div class="item">{{$name_airport_from->airport_name_vi}} - {{$name_airport_to->airport_name_vi}} </div>
                        </div>
                    </div>
                </div>
                <div class="row_info">
                    <div class="label">
                        Ngày bắt đầu:
                    </div>
                    <div class="value">
                        {{$promo->checkinFrom}}                    </div>
                </div>
                <div class="row_info">
                    <div class="label">
                        Ngày kết thúc:
                    </div>
                    <div class="value">
                        {{$promo->checkinTo}}                    </div>
                </div>
                <div class="row_info">
                    <div class="label">
                        Số lượng phát hành:
                    </div>
                    <div class="value">
                        {{number_format($promo->number_max,0,'.',',')}}                    </div>
                </div>
                <div class="row_info">
                    <div class="label">
                        Giá trị đơn hàng tối thiểu phải đạt:
                    </div>
                    <div class="value">
                        @if($promo->value_order_min==0) Không giới hạn @else {{number_format($promo->value_order_min,0,'.',',')}} VND @endif
                    </div>
                </div>
                <div class="row_info">
                    <div class="label">
                        Số lượng khách tối thiểu:
                    </div>
                    <div class="value">
                        {{$promo->number_user_min}}                    </div>
                </div>
                <div class="row_info">
                    <div class="label">
                        Khách sạn áp dụng:
                    </div>
                    <div class="value">
                        <div class="box_item">
                        <?php
                    $tach=explode(', ',$promo->hotel_apply);

                        $name_city=DB::table('hotels')->where('hotel_name',$promo->id_city)->get();
                    ?>
                    <div class="value">
                        <div class="box_item">
                            @foreach($tach as $c)
                            <?php
                            $name_city=DB::table('hotels')->where('hotel_name',$c)->first();
                            if($name_city!=null){
                            ?>
                                <div class="item">{{$name_city->hotel_name}}</div>
                                <?php }?>
                            @endforeach

                        </div>
                    </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>

</html>
<style>
a{
    color:rgb(33, 37, 41)

}
a:hover{

    text-decoration: none;
    color:rgb(33, 37, 41)
}

</style>
