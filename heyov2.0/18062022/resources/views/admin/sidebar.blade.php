<div class="menu_left">
    <div class="top">
        <a href="#" style="background-color: #f2f9ff; text-decoration: none" class="item">
          <span class="iconify" style="color: #032044; width: 30px; height: 30px"
                data-icon="icon-park-outline:hotel"></span>
            <span class="name_menu">Khách sạn</span>
        </a>
        <a href="#" style="text-decoration: none" class="item">
            <span class="iconify" style="color: #526a87; width: 30px; height: 30px" data-icon="ps:plane"></span>
            <span style="color: #526a87" class="name_menu">Vé máy bay</span>
        </a>
        <a href="#" style="text-decoration: none" class="item">
            <span class="iconify" style="color: #526a87; width: 30px; height: 30px" data-icon="clarity:car-line"></span>
            <span style="color: #526a87" class="name_menu">Thuê xe</span>
        </a>
        <a href="#" style="text-decoration: none" class="item">
          <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                data-icon="clarity:data-cluster-line"></span>
            <span style="color: #526a87" class="name_menu">Crypto</span>
        </a>
    </div>
    <div class="top">
        <a href="#" style="text-decoration: none" class="item">
            <span class="iconify" style="color: #526a87; width: 30px; height: 30px" data-icon="ep:user-filled"></span>
            <span style="color: #526a87" class="name_menu">Tài khoản</span>
        </a>
        <a href="#" style="text-decoration: none" class="item">
          <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                data-icon="dashicons:welcome-widgets-menus"></span>
            <span style="color: #526a87" class="name_menu">Danh mục</span>
        </a>
        <a href="#" style="text-decoration: none" class="item">
            <span class="iconify" style="color: #526a87; width: 30px; height: 30px" data-icon="gg:website"></span>
            <span style="color: #526a87" class="name_menu">Website</span>
        </a>
        <a href="#" style="text-decoration: none" class="item">
          <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                data-icon="material-symbols:settings-suggest-outline-sharp"></span>
            <span style="color: #526a87" class="name_menu">Cấu hình</span>
        </a>
    </div>
</div>
<div class="menu_right">
    <a href="#" class="item">
        <span style="width: 25px; height: 25px; color: #032044" class="iconify"
              data-icon="majesticons:analytics"></span>
        <span class="name_menu">Dashboard</span>
    </a>
    <a href="#" class="item">
        <span style="width: 25px; height: 25px; color: #032044" class="iconify" data-icon="mdi:bed"></span>
        <span class="name_menu">Tìm - Đặt phòng</span>
    </a>
    <a href="#" class="item">
        <span style="width: 25px; height: 25px; color: #032044" class="iconify"
              data-icon="fa-solid:calendar-day"></span>
        <span class="name_menu">Quản lý đơn đặt phòng</span>
    </a>
    <div href="#" class="item">
        <span style="width: 25px; height: 25px; color: #032044" class="iconify"
              data-icon="fa-solid:money-check-alt"></span>
        <span class="name_menu">Tuỳ chỉnh giá phòng</span>
    </div>
    <div class="item">
        <span style="width: 25px; height: 25px; color: #329223" class="iconify"
              data-icon="icomoon-free:price-tags"></span>
        <span style="color: #329223; font-weight: 800" class="name_menu">Chương trình khuyến mãi</span>
    </div>
    <div class="item">
        <span style="width: 25px; height: 25px; color: #032044" class="iconify" data-icon="eva:percent-fill"></span>
        <span class="name_menu">Quản lý hoa hồng</span>
    </div>
    <div class="item">
        <span style="width: 25px; height: 25px; color: #032044" class="iconify"
              data-icon="fa6-solid:hand-holding-dollar"></span>
        <span class="name_menu">Huỷ - Hoàn tiền</span>
    </div>
    <div class="item">
        <span style="width: 25px; height: 25px; color: #032044" class="iconify"
              data-icon="fluent:news-20-filled"></span>
        <span class="name_menu">Bài viết Marketing</span>
    </div>
</div>

