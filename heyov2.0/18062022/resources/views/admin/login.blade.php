<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('public/admin/css/login.css')}}" />
    <link rel="stylesheet" href="{{asset('public/admin/css/bootstrap.min.css')}}" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="{{asset('public/admin/js/bootstrap.min.js')}}"></script>
    <script src="https://code.iconify.design/2/2.2.1/iconify.min.js"></script>
    <title>HF Admin - Login</title>
</head>

<body>
<div class="box_login">
    <div class="wrapper">
        <div class="logo">HEYO TRIP</div>
        <form class="wrapper_box_input" method="post">
            @csrf
            <div class="title">Đăng nhập vào hệ thống</div>
            <?php
            $message=Session::get('message');
            if($message){
                echo '<span class="alert alert-danger" style="text-align: center;color:red;width: 100%">'.$message.'</span>';
                Session::put('message','');
            }
            ?>

            <div class="box_input">
                <input class="input" name="Email" placeholder="Email" />
                <span class="iconify" data-icon="mdi:email-mark-as-unread"
                      style="color: gray;width: 30px; height: 30px"></span>
            </div>
            <div class="box_input">
                <input type="password" name="Password" class="input" placeholder="Mật khẩu" />
                <span class="iconify" data-icon="bxs:lock" style="color: gray;width: 30px; height: 30px"></span>
            </div>
            <div class="wrapper_login">
                <div class="remember">
                    <input type="checkbox" />
                    <span style=" margin-left: 10px;">Ghi nhớ</span>
                </div>
                <button type="submit" class="btn btn_login">
                    Đăng nhập
                </button>
            </div>
        </form>
    </div>
</div>
</body>

</html>
