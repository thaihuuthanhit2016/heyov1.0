@if ($paginator->hasPages())
    <ul class="pager">

        <?php
        $total=DB::table('tbl_promotion')->where('subsystem',1)->get();
        $count=count($total);
        $from=10;
        $tongpage=ceil($count/$from);
        ?>

        <li class=" disabled "><span>Trang đầu </span></li>
        <li class=" disabled "><span>← </span></li>

        @foreach ($elements as $element)

            @if (is_string($element))
                <li class="disabled"><span>{{ $element }}</span></li>
            @endif



            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="active my-active"><span>{{ $page }}</span></li>
                    @else
                        <?php
                        $link=$url ;
                        $tach=explode('?',$link);
                        $tach=explode('=',$tach[1]);
                        $url='http://localhost/testlocal/admin/flight/list/'.$tach[1];


                        $pre=$tach[1]-1;
                        $next=$tach[1]+1;

                        $urlnext='http://localhost/testlocal/admin/flight/list/'.$next;
                        $urlpre='http://localhost/testlocal/admin/flight/list/'.$pre;

                        ?>
                        <li><a href="{{ $url }}">{{ $page }} </a></li>

                    @endif
                @endforeach
            @endif
        @endforeach
        <?php
        $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $tach=explode('/',$actual_link);
        $i=1;
        if($tongpage==1){
        ?>
        <li class=" disabled "><span>Next →</span></li>

        <?php }else{  $i=$i+1;
        ?>
        <li><a href="http://localhost/testlocal/admin/flight/list/<?php echo $i;?> "rel="next"> →</a></li>
        <?php }?>
        <li><a href="http://localhost/testlocal/admin/flight/list/<?php echo $tongpage?>" rel="next">Trang cuối </a></li>


    </ul>
@endif
