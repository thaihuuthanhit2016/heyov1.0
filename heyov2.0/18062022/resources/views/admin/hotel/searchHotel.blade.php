<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Tìm kiếm khách sạn</title>
      <link rel="stylesheet" href="{{asset('public/admin/css/index.css')}}" />
      <link rel="stylesheet" href="{{asset('public/admin/css/pnotify.custom.min.css')}}" />
      <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
      <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
      <script src="https://code.iconify.design/2/2.2.1/iconify.min.js"></script>
      <script src="{{asset('public/admin/js/main.js')}}"></script>
      <script src="{{asset('public/admin/js/pnotify.custom.min.js')}}"></script>
      <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
      <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
      <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
      <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
      <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css"
         rel="stylesheet">
      <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
      <script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=true&l'ibraries=geometry&key=AIzaSyBy8Nn3utEHcAyEkA5gkrlIEKQt3EHsJRU"></script>
      <style>
         .toggle.ios,
         .toggle-on.ios,
         .toggle-off.ios {
         border-radius: 20rem;
         }
         .toggle.ios .toggle-handle {
         border-radius: 20rem;
         }
      </style>
      <script>
         $(document).ready(function () {
             $('.js-example-basic-single').select2();
         });
      </script>
   </head>
   <body>
      <?php
         define('URL_API_V1', 'https://heyotrip-api.xproz.com/v1.0/api/');
         ?>
      <input type="hidden" id="urlcreateVisitor" value="<?php echo URL_API_V1 . 'cms/customer/create-visitor'; ?>">
      <input type="hidden" id="urlGetDetail" value="https://admin.heyotrip.com/hotels/booking/detail">
      {{--<input type="hidden" id="urlGetDetail" value="<?php echo base_url('hotels/booking/detail') ?>">--}}
      <div class="menu_header">
         <div class="left">
            <a href="#" class="logo">HEYO TRIP</a>
         </div>
         <div class="right">
            <span class="name_menu"> Tìm kiếm khách sạn </span>
            <div class="box_admin">
               <span class="iconify" style="width: 25px; height: 25px; color: #032044"
                  data-icon="ic:baseline-notifications-none"></span>
               <div style="color: white;background-color: red" class="badge badge-danger counter">9</div>
               <div id="btn_admin" class="wrapper_admin">
                  <span style="width: 25px; height: 25px; color: #032044" class="iconify"
                     data-icon="lucide:user"></span>
                  <span class="admin">Admin</span>
                  <span style="
                     width: 15px;
                     height: 15px;
                     color: #032044;
                     margin-left: 10px;
                     " class="iconify" data-icon="ant-design:caret-down-filled"></span>
               </div>
            </div>
         </div>
         <div id="profile" style="box-shadow: 10;" class="box_profile">
            <span style="width: 30px; height: 30px;color: #032044;" class="iconify"
               data-icon="simple-line-icons:logout"></span>
            <span class="logout"><a href="{{asset('logout')}}" style="text-decoration: none;color: black">Đăng xuất</a></span>
         </div>
      </div>
      <div style="display: flex">
         <div class="menu_left">
            <div class="top">
               <a href="{{asset('admin/hotel/order/all/1')}}" style="background-color: #f2f9ff; text-decoration: none" class="item">
               <span class="iconify" style="color: #032044; width: 30px; height: 30px"
                  data-icon="icon-park-outline:hotel"></span>
               <span class="name_menu">Khách sạn</span>
               </a>
               <a href="{{asset('admin/flight/order/all/1')}}" style=" text-decoration: none" class="item">
               <span class="iconify" style="color: #526a87; width: 30px; height: 30px" data-icon="ps:plane"></span>
               <span style="color: #526a87" class="name_menu">Vé máy bay</span>
               </a>
               <a href="#" style="text-decoration: none" class="item">
               <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                  data-icon="clarity:car-line"></span>
               <span style="color: #526a87" class="name_menu">Thuê xe</span>
               </a>
               <a href="#" style="text-decoration: none" class="item">
               <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                  data-icon="clarity:data-cluster-line"></span>
               <span style="color: #526a87" class="name_menu">Crypto</span>
               </a>
            </div>
            <div class="top">
               <a href="#" style="text-decoration: none" class="item">
               <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                  data-icon="ep:user-filled"></span>
               <span style="color: #526a87" class="name_menu">Tài khoản</span>
               </a>
               <a href="#" style="text-decoration: none" class="item">
               <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                  data-icon="dashicons:welcome-widgets-menus"></span>
               <span style="color: #526a87" class="name_menu">Danh mục</span>
               </a>
               <a href="#" style="text-decoration: none" class="item">
               <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                  data-icon="gg:website"></span>
               <span style="color: #526a87" class="name_menu">Website</span>
               </a>
               <a href="#" style="text-decoration: none" class="item">
               <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                  data-icon="material-symbols:settings-suggest-outline-sharp"></span>
               <span style="color: #526a87" class="name_menu">Cấu hình</span>
               </a>
            </div>
         </div>
         <link rel="stylesheet" href="//code.jquery.com/ui/1.13.1/themes/base/jquery-ui.css">
         <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
         <script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>
         <div class="menu_right">
            <a href="#" class="item">
            <span class="iconify" data-icon="majesticons:analytics"></span>
            <span class="name_menu">Dashboard</span>
            </a>
            <a href="{{asset('admin/hotel/searchHotel')}}" class="item">
            <span class="iconify active" style="color: #329223; font-weight: 800;" data-icon="mdi:bed"></span>
            <span class="name_menu active" style="color: #329223; font-weight: 800;">Tìm - Đặt phòng</span>
            </a>
            <a href="{{asset('admin/hotel/order/all/1')}}" class="item">
            <span class="iconify" data-icon="fa-solid:calendar-day"></span>
            <span class="name_menu">Quản lý đơn đặt phòng</span>
            </a>
            <a href="#" href="#" class="item">
            <span class="iconify" data-icon="fa-solid:money-check-alt"></span>
            <span class="name_menu">Tuỳ chỉnh giá phòng</span>
            </a>
            <a href="{{asset("admin/hotel/list")}}" class="item">
            <span class="iconify" data-icon="icomoon-free:price-tags"></span>
            <span class="name_menu">Chương trình khuyến mãi</span>
            </a>
            <a href="#" class="item">
            <span s class="iconify" data-icon="eva:percent-fill"></span>
            <span class="name_menu">Quản lý hoa hồng</span>
            </a>
            <a href="#" class="item">
            <span class="iconify" data-icon="fa6-solid:hand-holding-dollar"></span>
            <span class="name_menu">Huỷ - Hoàn tiền</span>
            </a>
            <a href="#" class="item">
            <span class="iconify" data-icon="fluent:news-20-filled"></span>
            <span class="name_menu">Bài viết Marketing</span>
            </a>
         </div>
         <div class="box_content">
            <div id="search_box_search">
               <div id="hotels">
                  <form>
                     <div class="search_hotel">
                        <div class="row px-2 py-1">
                           <div class="col-7 row">
                              <div class="col-6 d-flex align-items-center py-2 item_search">
                                 <span class="iconify" data-icon="fluent:search-24-regular"></span>
                                 <input class="ml-2" type="text" id='locationsss' placeholder="Bạn muốn đi đâu?" />
                                 <input class="ml-2" type="hidden" id='id_locationss'  />
                                 <ul class="dropdown-menu" style="display: block;position: absolute;margin-top: 4px;width: 626px;margin-left: 10px;" id="serach_ajax2">
                                    <?php $i=0;
                                       $datalocation=DB::table('locations')->get();
                                       ?>
                                    @foreach($datalocation as $data)
                                    <?php $i++;?>
                                    <p class="dataLocation_click" data-test="{{$data->id}}"><a href="#">{{$data->name_vi}}</a></p>
                                    @endforeach
                                 </ul>
                                 <input type="hidden"value="" id="dataId">
                              </div>
                              <div class="col-6 d-flex align-items-center py-2 item_search">
                                 <span class="iconify"
                                    data-icon="healthicons:i-schedule-school-date-time-outline"></span>
                                 <input id="daterange" class="ml-2" placeholder="" readonly/>
                              </div>
                           </div>
                           <div class="row col-5 d-flex align-items-center justify-content-between pr-0">
                              <div class="col-9 d-flex align-items-center py-2">
                                 <span class="iconify" data-icon="majesticons:users-line"></span>
                                 <input style="width: 100%;" placeholder="1 phòng, 1 người lớn, 0 trẻ em"
                                    type="text" id="room_hotel" value data-value="1,1,0" readonly>
                              </div>
                              <div class="col-3 d-flex justify-content-end">
                                 <button type="button" class="btn search_home_submit">Tìm kiếm</button>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div id="errordate"></div>
                     <div class="row" id="custom_datepicker" >
                        <div class="col-lg-3"></div>
                        <div class="col-lg-6" style="margin-left: 70px;">
                           <div class="row"style="background: white;width: 643px;margin-left: 0px;">
                              <div class="col-md-3">
                                 <input class="form-control" id="datepiker1" value="{{date('d/m/Y')}}">
                              </div>
                              <div class="col-md-3">
                                 <input class="form-control"  id="datepiker2" value="{{date('d/m/Y')}}">
                              </div>
                              <div class="col-md-3">
                                 <p class="btn btn-success"  id="apply" >Xác nhận</p>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row" id="row_room_hotel" >
                        <div class="col-lg-7"></div>
                        <div class="col-lg-5" >
                           <div class="row" id="base_room_hotel" style="   margin-left: -41px;
                              ">
                              <div class="col-lg-7" style="background: white; ">
                                 <div class="col-md-6">
                                    <div class="row">
                                       <div class="col-lg-7"> Người lớn:</div>
                                       <div class="col-lg-3"> <input type="number" style="width: 37px;" id="number_nguoilon1" value="1" min="1"></div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="row">
                                       <div class="col-lg-7" > Trẻ em:<br> <span style="font-size: 12px"> (0-17 tuổi)</span></div>
                                       <div class="col-lg-3"><input type="number1" style="width: 37px;" id="number_treem" value="0" min="1"></div>
                                    </div>
                                 </div>
                                 <div class="col-md-6 action">
                                    <div class="row">
                                       <div class="col-lg-6">
                                          <span style="float:left;color: blue;cursor: pointer"  id="ok_room_hotel1" >chọn</span>
                                       </div>
                                       <div class="col-lg-6">
                                          <span   style="float:right;color: blue;cursor: pointer;font-size: 15px;" id="add_room" >Thêm phòng</span>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                 </div>
                              </div>
                              <div class="clearfix"></div>
                              <div class="col-lg-7 box2"style="background: white; ">
                                 <div class="col-md-6">
                                    <div class="row">
                                       <div class="col-lg-7"> Người lớn:</div>
                                       <div class="col-lg-3"> <input type="number" style="width: 37px;" id="number_nguoilon2" value="1" min="1"></div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="row">
                                       <div class="col-lg-7" > Trẻ em:<br> <span style="font-size: 12px"> (0-17 tuổi)</span></div>
                                       <div class="col-lg-3"><input type="number" style="width: 37px;" id="number_treem2" value="0" min="1"></div>
                                    </div>
                                 </div>
                                 <div class="col-md-6 action2">
                                    <div class="row">
                                       <div class="col-lg-6">
                                          <span style="float:left;color: blue;cursor: pointer"  id="ok_room_hotel2" >chọn</span>
                                       </div>
                                       <div class="col-lg-6">
                                          <span   style="float:left;color: blue;cursor: pointer;font-size: 15px;" id="add_room1" >Thêm phòng</span>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                 </div>
                              </div>
                              <div class="clearfix"></div>
                              <div class="col-lg-7 box3"style="background: white; ">
                                 <div class="col-md-6">
                                    <div class="row">
                                       <div class="col-lg-7"> Người lớn:</div>
                                       <div class="col-lg-3"> <input type="number" style="width: 37px;" id="number_nguoilon3" value="1" min="1"></div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="row">
                                       <div class="col-lg-7" > Trẻ em:<br> <span style="font-size: 12px"> (0-17 tuổi)</span></div>
                                       <div class="col-lg-3"><input type="number" style="width: 37px;" id="number_treem3" value="0" min="1"></div>
                                    </div>
                                 </div>
                                 <div class="col-md-6 action3">
                                    <div class="row">
                                       <div class="col-lg-6">
                                          <p style="float:left;color: blue;cursor: pointer"  id="ok_room_hotel3" >chọn</p>
                                       </div>
                                       <div class="col-lg-6">
                                          <p   style="float:left;color: blue;cursor: pointer;font-size: 15px;" id="add_room2" >Thêm phòng</p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                 </div>
                              </div>
                              <div class="clearfix"></div>
                              <div class="col-lg-7 box4"style="background: white; ">
                                 <div class="col-md-6">
                                    <div class="row">
                                       <div class="col-lg-7"> Người lớn:</div>
                                       <div class="col-lg-3"> <input type="number" style="width: 37px;" id="number_nguoilon4" value="1" min="1"></div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="row">
                                       <div class="col-lg-7" > Trẻ em:<br> <span style="font-size: 12px"> (0-17 tuổi)</span></div>
                                       <div class="col-lg-3"><input type="number" style="width: 37px;" id="number_treem4" value="0" min="1"></div>
                                    </div>
                                 </div>
                                 <div class="col-md-6 action4">
                                    <div class="row">
                                       <div class="col-lg-6">
                                          <span style="float:left;color: blue;cursor: pointer"  id="ok_room_hotel4" >chọn</span>
                                       </div>
                                       <div class="col-lg-6">
                                          <span   style="float:left;color: blue;cursor: pointer;font-size: 15px;" id="add_room3" >Thêm phòng</span>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                 </div>
                              </div>
                              <div class="clearfix"></div>
                              <div class="col-lg-7 box5"style="background: white; ">
                                 <div class="col-md-6">
                                    <div class="row">
                                       <div class="col-lg-7"> Người lớn:</div>
                                       <div class="col-lg-2"> <input type="number" style="width: 37px;" id="number_nguoilon5" value="1" min="1"></div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="row">
                                       <div class="col-lg-7" > Trẻ em:<br> <span style="font-size: 12px"> (0-17 tuổi)</span></div>
                                       <div class="col-lg-2"><input type="number5" style="width: 37px;" id="number_treem5" value="0" min="1"></div>
                                    </div>
                                 </div>
                                 <div class="col-md-6 action5">
                                    <div class="row">
                                       <div class="col-lg-6">
                                          <span style="float:left;color: blue;cursor: pointer"  id="ok_room_hotel5" >chọn</span>
                                       </div>
                                       <div class="col-lg-6">
                                          <span   style="float:left;color: blue;cursor: pointer;font-size: 15px;" id="add_room4" >Thêm phòng</span>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                 </div>
                              </div>
                              <div class="clearfix"></div>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
               <div id="box_result_search"  class="row mt-4">
                  <form class="col-12 col-xl-3 mb-2 " id="form-filter">
                     <div class="box_filter p-2">
                        <div class="d-flex align-items-center justify-content-between">
                           <div class="font500">BỘ LỌC</div>
                           <div>
                              <button class="btn btn-success p-1 font_size_12" type="button" id="search_filter_button">Áp dụng</button>
                              <button class="btn btn-info p-1 font_size_12">Reset</button>
                           </div>
                        </div>
                        <p class="mt-2 mb-1">Tìm khách sạn</p>
                        <div class="search_filter_input">
                           <span class="iconify" data-icon="fluent:search-24-regular"></span>
                           <input class="mt-1 py-1 font_size_15" name="hotel_name" placeholder="Nhập vào tên khách sạn" />
                        </div>
                        <div id="search_button_collapse_policy"
                           class="mt-3 py-1 d-flex align-items-center justify-content-between search_button_collapse">
                           <div class="font_size_16 font500">Chính sách</div>
                           <span id="search_button_collapse_policy_icon" style="transform: rotate(0deg);"
                              class="iconify" data-icon="fa-solid:chevron-up"></span>
                        </div>
                        <div id="search_box_collapse_policy" class="py-2 search_box_collapse show_collapse">
                           <div class="d-flex py-1 align-items-center">
                              <input class="search_checkbox_filter" type="checkbox" />
                              <div class="ml-3 font_size_15 font400">Hủy thanh toán</div>
                           </div>
                           <div class="d-flex py-1 align-items-center">
                              <input class="search_checkbox_filter" type="checkbox" />
                              <div class="ml-3 font_size_15 font400">Thanh toán sau</div>
                           </div>
                        </div>
                        <div id="search_button_collapse_price_per_night"
                           class="mt-3 py-1 d-flex align-items-center justify-content-between search_button_collapse">
                           <div class="font_size_16 font500">Giá mỗi đêm</div>
                           <span id="search_button_collapse_price_per_night_icon" style="transform: rotate(0deg);"
                              class="iconify" data-icon="fa-solid:chevron-up"></span>
                        </div>
                        <div id="search_box_collapse_price_per_night"
                           class="py-2 search_box_collapse show_collapse">
                           <div class="d-flex py-1 align-items-center">
                              <input class="search_checkbox_filter" type="checkbox" />
                              <div class="ml-3 font_size_13 font400">Dưới 1.000.000đ</div>
                           </div>
                           <div class="d-flex py-1 align-items-center">
                              <input class="search_checkbox_filter" type="checkbox" />
                              <div class="ml-3 font_size_13 font400">1.000.000đ đến 2.000.000đ</div>
                           </div>
                           <div class="d-flex py-1 align-items-center">
                              <input class="search_checkbox_filter" type="checkbox" />
                              <div class="ml-3 font_size_13 font400"> 2.000.000đ đến 4.000.000đ</div>
                           </div>
                           <div class="d-flex py-1 align-items-center">
                              <input class="search_checkbox_filter" type="checkbox" />
                              <div class="ml-3 font_size_13 font400">4.000.000đ đến 6.000.000đ</div>
                           </div>
                           <div class="d-flex py-1 align-items-center">
                              <input class="search_checkbox_filter" type="checkbox" />
                              <div class="ml-3 font_size_13 font400">Trên 6.000.000đ</div>
                           </div>
                        </div>
                        <div id="search_button_collapse_free_breakfast"
                           class="mt-3 py-1 d-flex align-items-center justify-content-between search_button_collapse">
                           <div class="font_size_16 font500">Gói bữa ăn có sẵn</div>
                           <span id="search_button_collapse_free_breakfast_icon" style="transform: rotate(0deg);"
                              class="iconify" data-icon="fa-solid:chevron-up"></span>
                        </div>
                        <div id="search_box_collapse_free_breakfast" class="py-2 search_box_collapse show_collapse">
                           <div class="d-flex py-1 align-items-center">
                              <input class="search_checkbox_filter" type="checkbox" />
                              <div class="ml-3 font_size_15 font400">Miễn phí bữa sáng</div>
                           </div>
                        </div>
                        <div id="search_button_collapse_quantity_star"
                           class="mt-3 py-1 d-flex align-items-center justify-content-between search_button_collapse">
                           <div class="font_size_16 font500">Hạng sao</div>
                           <span id="search_button_collapse_quantity_star_icon" style="transform: rotate(0deg);"
                              class="iconify" data-icon="fa-solid:chevron-up"></span>
                        </div>
                        <div id="search_box_collapse_quantity_star" class="py-2 search_box_collapse show_collapse">
                           <div class="d-flex py-1 align-items-center">
                              <button class="d-flex align-items-center" type="button">
                              <span class="font_size_13">0</span>
                              <span class="iconify" data-icon="emojione:star"></span>
                              </button>
                              <button class="d-flex align-items-center" type="button">
                              <span class="font_size_13">3</span>
                              <span class="iconify" data-icon="emojione:star"></span>
                              </button>
                              <button class="d-flex align-items-center" type="button">
                              <span class="font_size_13">4</span>
                              <span class="iconify" data-icon="emojione:star"></span>
                              </button>
                              <button class="d-flex align-items-center" type="button">
                              <span class="font_size_13">5</span>
                              <span class="iconify" data-icon="emojione:star"></span>
                              </button>
                           </div>
                        </div>
                        <div id="search_button_collapse_convenient"
                           class="mt-3 py-1 d-flex align-items-center justify-content-between search_button_collapse">
                           <div class="font_size_16 font500">Tiện nghi</div>
                           <span id="search_button_collapse_convenient_icon" style="transform: rotate(0deg);"
                              class="iconify" data-icon="fa-solid:chevron-up"></span>
                        </div>
                        <div id="search_box_collapse_convenient" class="py-2 search_box_collapse show_collapse">
                           <div class="d-flex py-1 align-items-center">
                              <input class="search_checkbox_filter" type="checkbox" />
                              <div class="ml-3 font_size_14 font400">Internet</div>
                           </div>
                           <div class="d-flex py-1 align-items-center">
                              <input class="search_checkbox_filter" type="checkbox" />
                              <div class="ml-3 font_size_13 font400">Dịch vụ Phòng</div>
                           </div>
                           <div class="d-flex py-1 align-items-center">
                              <input class="search_checkbox_filter" type="checkbox" />
                              <div class="ml-3 font_size_13 font400">Thang máy</div>
                           </div>
                           <div class="d-flex py-1 align-items-center">
                              <input class="search_checkbox_filter" type="checkbox" />
                              <div class="ml-3 font_size_13 font400">Gói gia đình</div>
                           </div>
                           <div class="d-flex py-1 align-items-center">
                              <input class="search_checkbox_filter" type="checkbox" />
                              <div class="ml-3 font_size_13 font400">Hồ bơi - bao gồm cả trong nhà và ngoài trời
                              </div>
                           </div>
                           <div class="d-flex py-1 align-items-center">
                              <input class="search_checkbox_filter" type="checkbox" />
                              <div class="ml-3 font_size_14 font400">Người khuân vác</div>
                           </div>
                           <div class="d-flex py-1 align-items-center">
                              <input class="search_checkbox_filter" type="checkbox" />
                              <div class="ml-3 font_size_13 font400">Phòng tắm riêng</div>
                           </div>
                           <div class="d-flex py-1 align-items-center">
                              <input class="search_checkbox_filter" type="checkbox" />
                              <div class="ml-3 font_size_13 font400">220 DC</div>
                           </div>
                           <div class="d-flex py-1 align-items-center">
                              <input class="search_checkbox_filter" type="checkbox" />
                              <div class="ml-3 font_size_13 font400">Vòi hoa sen</div>
                           </div>
                           <div class="d-flex py-1 align-items-center">
                              <input class="search_checkbox_filter" type="checkbox" />
                              <div class="ml-3 font_size_14 font400">Giường nước</div>
                           </div>
                           <div class="d-flex py-1 align-items-center">
                              <input class="search_checkbox_filter" type="checkbox" />
                              <div class="ml-3 font_size_14 font400">Bãi đậu xe miễn phí</div>
                           </div>
                           <div class="d-flex py-1 align-items-center">
                              <input class="search_checkbox_filter" type="checkbox" />
                              <div class="ml-3 font_size_14 font400">120 AC</div>
                           </div>
                           <div class="d-flex py-1 align-items-center">
                              <input class="search_checkbox_filter" type="checkbox" />
                              <div class="ml-3 font_size_14 font400">Két an toàn trong phòng</div>
                           </div>
                           <div class="d-flex py-1 align-items-center">
                              <input class="search_checkbox_filter" type="checkbox" />
                              <div class="ml-3 font_size_14 font400">TV, Cáp</div>
                           </div>
                           <div class="d-flex py-1 align-items-center">
                              <input class="search_checkbox_filter" type="checkbox" />
                              <div class="ml-3 font_size_14 font400">Giường sofa</div>
                           </div>
                        </div>
                     </div>
                  </form>
                  <div class="col-12 col-xl-9">
                     <form class="box_filter p-2">
                        <div class="font-weight-bold font_size_18">Chỗ ở phù hợp với tìm kiếm của bạn</div>
                        <div class="d-flex align-items-center justify-content-end">
                           <span class="mr-2 font400 font_size_16 color8">Sắp xếp: </span>
                           <div class="dropdown">
                              <button style="min-width: 200px;"
                                 class="d-flex align-items-center justify-content-between btn btn-primary dropdown-toggle"
                                 type="button" id="dropdownSortButton" data-toggle="dropdown"
                                 aria-haspopup="true" aria-expanded="false">
                              Sắp xếp
                              </button>
                              <div style="min-width: 200px;" class="dropdown-menu"
                                 aria-labelledby="dropdownSortButton" id="box_dropdown_sort_result_search_hotel">
                                 <button class="btn font500">Sắp xếp</button>
                                 <button class="btn font500">Giá thấp nhất</button>
                                 <button class="btn font500">Giá cao nhất</button>
                                 <button class="btn font500">Xếp hạng cao nhất</button>
                                 <button class="btn font500">Xếp hạng thấp nhất</button>
                                 <button class="btn font500">Khoảng cách gần nhất</button>
                                 <button class="btn font500">Khoảng cách xa nhất</button>
                              </div>
                           </div>
                        </div>
                        <div id="box_result_search1">
                        </div>
                        <div id="text_no_result">
                           <p>Không có kết quả tìm kiếm phù hợp</p>
                        </div>
                     </form>
                  </div>
               </div>
              
               <div id="search_box_choose">aaaa</div>

              
               <div style="display: none;" id="search_box_booking">
                  <form class="row">
                     <div class="col-12 col-sm-9">
                        <div class="p-3 box_filter">
                           <div class="font800 font_size_18">Contact Information</div>
                           <div class="divider_horizontal background_888 my-3"></div>
                           <div class="color8 font_size_14 font400">Enter the name of the person responsible for
                              keeping in touch with the
                              accommodation service provider. Full name as indicated on the ID card/Passport
                           </div>
                           <div class="row mt-3 px-3">
                              <div class="col-12 col-sm-6 col-lg-2 p-1">
                                 <div class="font_size_15">Title *</div>
                                 <select class="form-control mt-1">
                                    <option>Nam</option>
                                    <option>Nữ</option>
                                 </select>
                              </div>
                              <div class="col-12 col-sm-6 col-lg-3 p-1">
                                 <div class="font_size_15">Firstname *</div>
                                 <input class="form-control mt-1" type="text" />
                              </div>
                              <div class="col-12 col-sm-6 col-lg-3 p-1">
                                 <div class="font_size_15">Lastname *</div>
                                 <input class="form-control mt-1" type="text" />
                              </div>
                              <div class="col-12 col-sm-6 col-lg-4 p-1">
                                 <div class="font_size_15">Country</div>
                                 <div class="mt-1">
                                    <select class="js-example-basic-single form-control mt-1" name="country">
                                       <option value="AL">Alabama</option>
                                       <option value="WY">Wyoming</option>
                                    </select>
                                 </div>
                              </div>
                              <div class="col-12 col-sm-6 col-lg-4 mt-2 p-1">
                                 <div class="font_size_15">Phone number *</div>
                                 <input class="form-control mt-1" type="text" />
                              </div>
                              <div class="col-12 col-sm-6 col-lg-4 mt-2 p-1">
                                 <div class="font_size_15">Email *</div>
                                 <input class="form-control mt-1" type="text" />
                              </div>
                              <div class="col-12 col-sm-6 col-lg-4 mt-2 p-1">
                                 <div class="font_size_15">&nbsp;</div>
                                 <div class="mt-1">
                                    Booking confirmation will be sent to this email address.
                                 </div>
                              </div>
                           </div>
                           <div class="mt-4 d-flex align-items-center justify-content-between">
                              <div class="font800 font_size_18">Guest Information</div>
                              <button class="btn btn-link p-0">I'm making this booking for someone else</button>
                           </div>
                           <div class="divider_horizontal background_888 my-3"></div>
                           <div class="color8 font_size_14 font400">Enter the name of the person checking into the
                              room. Remember to bring a valid photo ID or passport at check-in
                           </div>
                           <div class="row mt-3 px-3">
                              <div class="col-12 col-sm-6 col-lg-2 p-1">
                                 <div class="font_size_15">Title *</div>
                                 <select class="form-control mt-1">
                                    <option>Nam</option>
                                    <option>Nữ</option>
                                 </select>
                              </div>
                              <div class="col-12 col-sm-6 col-lg-3 p-1">
                                 <div class="font_size_15">Firstname *</div>
                                 <input class="form-control mt-1" type="text" />
                              </div>
                              <div class="col-12 col-sm-6 col-lg-3 p-1">
                                 <div class="font_size_15">Lastname *</div>
                                 <input class="form-control mt-1" type="text" />
                              </div>
                           </div>
                           <div class="divider_horizontal background_888 mt-4 mb-2"> </div>
                           <div>
                              <span class="font_size_16 font500">Additional Request</span>
                              <span class="ml-2 font_size_14 font400">(Optional)</span>
                              <span class="ml-2">
                              <input id="toggle-state-switch" type="checkbox" value="true"
                                 onchange="test(value)" data-on=" " data-off=" " data-toggle="toggle"
                                 data-size="xs" data-onstyle="success" data-offstyle="dark" data-style="ios">
                              </span>
                           </div>
                           <div class="mt-4 mb-3">We will inform the accommodation that you are interested in these
                              services, so they
                              can provide you with detailed information. Your request cannot be guaranteed. Don't
                              be worried! The accommodation will try to fulfill your request.
                           </div>
                           <div>
                              <textarea class="form-control resize-none" rows="5"></textarea>
                           </div>
                           <div class="divider_horizontal background_888 my-4"></div>
                           <div>
                              <span class="iconify icon40x40 color_blue" data-icon="fa6-regular:handshake"></span>
                              <span>By completing this booking, you agree to the <a href="#">Booking
                              Conditions</a>, <a href="#">Terms and
                              Conditions</a> and <a href="#">Privacy Policy</a></span>
                           </div>
                           <div class="my-4">
                              <button type="button" class="btn btn-primary complete_booking">Complete
                              booking</button>
                           </div>
                        </div>
                     </div>
                     <div class="col-12 col-sm-3">
                        <div class="box_filter">
                           <div class="c-img">
                              <img
                                 src="https://travelport.leonardocontentcloud.com/imageRepo/7/0/139/101/468/01-cmv-deluxe-01_J.jpg" />
                           </div>
                           <div class="p-2 ">
                              <div class="font-weight-bold font_size_15">
                                 CENTARA MIRAGE MUI NE VIETNAM
                              </div>
                              <div class="mt-2">
                                 <span class="iconify" data-icon="bxs:star" style="color: #FFC95B"></span>
                                 <span class="iconify" data-icon="bxs:star" style="color: #FFC95B"></span>
                                 <span class="iconify" data-icon="bxs:star" style="color: #FFC95B"></span>
                                 <span class="iconify" data-icon="bxs:star" style="color: #FFC95B"></span>
                                 <span class="iconify" data-icon="bxs:star" style="color: #FFC95B"></span>
                              </div>
                              <div class="mt-4 mb-2 text-primary">HUYNH THUC KHANG STR</div>
                           </div>
                           <div class="py-2 background_blue">
                              <div class="py-2 d-flex align-items-center justify-content-around">
                                 <div class="text-center">
                                    <div class="color8 font_size_14 font800">Check-in</div>
                                    <div class="font_size_15 font800 mt-2">03 Jun 2022</div>
                                 </div>
                                 <span class="iconify icon30x30 color8"
                                    data-icon="akar-icons:arrow-right"></span>
                                 <div class="text-center">
                                    <div class="color8 font_size_14 font800">Check-out</div>
                                    <div class="font_size_15 font800 mt-2">04 Jun 2022</div>
                                 </div>
                              </div>
                              <div class="divider_horizontal background_888 my-2"></div>
                              <div class="px-2">
                                 <span class="iconify color8 icon20x20" data-icon="ep:moon"></span>
                                 <span class="font500">1 night</span>
                              </div>
                              <div class="px-2 my-2">
                                 <span class="iconify color8 icon20x20"
                                    data-icon="majesticons:users-line"></span>
                                 <span class="font500">1 room, 1 adults, 0 children</span>
                              </div>
                           </div>
                           <div class="p-2">
                              <div class="d-flex align-items-center justify-content-between">
                                 <div>
                                    <div class="font_size_15">Room Type</div>
                                    <div class="mt-2 font_size_15">Refundability</div>
                                 </div>
                                 <div class="text-right">
                                    <div class="font800 font_size_15">Choice Of King Bed Or Twin Beds -44 Sq Mtr
                                    </div>
                                    <div class="font_size_14 mt-1 font800 color_red">No-refund</div>
                                 </div>
                              </div>
                              <div class="divider_horizontal background_888 my-3"></div>
                              <div class="mb-4 mt-2">
                                 <button class="btn btn-outline-danger">Cancellation Policy</button>
                              </div>
                           </div>
                        </div>
                        <div class="box_filter mt-4 px-2 py-3">
                           <div class="d-flex align-items-center justify-content-between">
                              <div>
                                 <div class="font800 font_size_16">Payment Details</div>
                                 <div class="font_size_14 mt-2">1 room x 1 night</div>
                                 <div class="font_size_13 font800 color8 mt-1">Tax Not Included</div>
                              </div>
                              <div class="font800 font_size_14">
                                 4,989,600 VND
                              </div>
                           </div>
                           <div class="divider_horizontal background_888 mt-2 mb-3"></div>
                           <div class="d-flex align-items-center justify-content-between">
                              <div class="font800 font_size_15">Subtotal</div>
                              <div class="font800 font_size_14">
                                 4,989,600 VND
                              </div>
                           </div>
                           <div class="d-flex align-items-center justify-content-between mt-2">
                              <div class="font_size_16 color_green">Total</div>
                              <div class="font800 font_size_18 color_green text-right">4,989,600 VND</div>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>
<script>
   $(document).ready(function () {
       var app = app || {};
   
       $("#datepiker1").datepicker({
           dateFormat: 'dd/mm/yy',
           minDate: new Date(),
       });
       $("#datepiker2").datepicker({
           minDate: new Date(),
           dateFormat: 'dd/mm/yy'
       });
       $('#custom_datepicker').css('display','none');
       let d = new Date();
       let year = d.getFullYear();
       let month = d.getMonth() + 1;
       if(month<10){
           month='0'+month;
       }
       let day = d.getDate();
       let day1,day2;
       if(day<10){
           let day1p =(parseInt)(day)
           let c=day1p+2;
           let c2=day1p+4;
           day1='0'+c;
           day2='0'+c2;
           
       let str=day1 +'/'+month+'/'+year;
       let str2=day2 +'/'+month+'/'+year;
       let daytoday=str +' - '+str2;
   
       $("#daterange").val(daytoday);
       }else{
   
           let day1p =(parseInt)(day)
           let day1=day1p+2;
           let day2=day1p+4;
           
       let str=day1 +'/'+month+'/'+year;
       let str2=day2 +'/'+month+'/'+year;
       let daytoday=str +' - '+str2;
   
       $("#daterange").val(daytoday);
       }
       $("#apply").click(function () { console.log(1);
           $('#custom_datepicker').css('display','none');
   
           let dayfrom=$('#datepiker1').val();
           let dayto=$('#datepiker2').val();
           if((dayfrom)>(dayto)){
               $("#errordate").html("<p style='color: red'>Lỗi ngày bắt đầu lớn hơn ngày kết thúc</p>")     ;
           }else{
               $("#errordate").html('');
           }
           $('#daterange').val(dayfrom+' - '+dayto);
   
       });
       $("#locationsss").keyup(function (){
           var query =$(this).val();
           $("#serach_ajax").css('display','block')
   
           if(query!=''){
               $.ajax({
                   url:'{{asset('admin/loadlocation')}}',
                   method:"get",
                   data:{query:query},
                   success:function (data){
                       $("#serach_ajax").fadeIn();
                       $("#serach_ajax").html(data);
   
                   }
               });
           }else {
               $("#serach_ajax").fadeOut();
   
           }
           $(document).on('click','li',function (){
               $("#locationsss").val($(this).text());
               $("#serach_ajax").fadeOut();
   
           });
   
   
       });
       $(document).on('click','p',function (){
   
           $("#locationsss").val($(this).text());
           let id1 = $('#value_id_locationss_1').val();
   
           $("#serach_ajax2").fadeOut();
   
       });
       $("#daterange").click(function () {
           $('#custom_datepicker').css('display','flex')
           $('#serach_ajax2').css('display','none')
           $('#serach_ajax1').css('display','none')
           $('#row_room_hotel').css('display','none')
   
       });
       $("#row_room_hotel").css('display','none');
   
       $("#room_hotel").click(function () {
           $("#row_room_hotel").css('display','flex');
   
           $('#serach_ajax2').css('display','none')
           $('#serach_ajax1').css('display','none')
           $('#custom_datepicker').css('display','none')
   
   
       });
   
       $(".box2").css('display','none');
       $(".box3").css('display','none');
       $(".box4").css('display','none');
       $(".box5").css('display','none');
   
       $("#add_room").click(function () {
           console.log(1);
           $(".box2").css('display','block');
           $(".action").css('display','none');
   
       });
       $("#add_room1").click(function () {
           console.log(2);
           $(".box2").css('display','block');
           $(".box3").css('display','block');
           $(".action").css('display','none');
           $(".action1").css('display','none');
           $(".action2").css('display','none');
   
       });
       $("#add_room2").click(function () {
           console.log(3);
           $(".box2").css('display','block');
           $(".box3").css('display','block');
           $(".box4").css('display','block');
           $(".action").css('display','none');
           $(".action1").css('display','none');
           $(".action2").css('display','none');
           $(".action3").css('display','none');
   
       });
       $("#add_room3").click(function () {
           console.log(4);
   
           $(".box2").css('display','block');
           $(".box3").css('display','block');
           $(".box4").css('display','block');
           $(".box5").css('display','block');
           $(".action").css('display','none');
           $(".action1").css('display','none');
           $(".action2").css('display','none');
           $(".action3").css('display','none');
           $(".action4").css('display','none');
   
       });
       $("#ok_room_hotel1").click(function () {
           $("#row_room_hotel").css('display','none');
           let nguoilon=$("#number_nguoilon1").val();
           let treem=$("#number_treem1").val();
           $("#room_hotel").val('1 phòng '+nguoilon+' người lớn '+ treem + " trẻ em");
   
   
   
   
       });
       $("#ok_room_hotel2").click(function () {
           $("#row_room_hotel").css('display','none');
           let nguoilon=$("#number_nguoilon2").val();
           let treem=$("#number_treem2").val();
           $("#room_hotel").val('2 phòng '+nguoilon+' người lớn '+ treem + " trẻ em");
   
   
   
   
       });
       $("#ok_room_hotel3").click(function () {
           $("#row_room_hotel").css('display','none');
           let nguoilon=$("#number_nguoilon3").val();
           let treem=$("#number_treem3").val();
           $("#room_hotel").val('3 phòng '+nguoilon+' người lớn '+ treem + " trẻ em");
   
   
   
   
       });
       $("#ok_room_hotel4").click(function () {
           $("#row_room_hotel").css('display','none');
           let nguoilon=$("#number_nguoilon4").val();
           let treem=$("#number_treem4").val();
           $("#room_hotel").val('4 phòng '+nguoilon+' người lớn '+ treem + " trẻ em");
   
   
   
   
       });
       $("#ok_room_hotel5").click(function () {
           $("#row_room_hotel").css('display','none');
           let nguoilon=$("#number_nguoilon5").val();
           let treem=$("#number_treem5").val();
           $("#room_hotel").val('5 phòng '+nguoilon+' người lớn '+ treem + " trẻ em");
   
   
   
   
       });
   
   
   
       let txtChooseRoom = 'Chọn phòng';
       let txtError = 'Có lỗi xảy ra trong quá trình thực hiện';
       const NOTI_ERR = 0;
       let title_filter = {
           amenities: 'Tiện nghi',
           amenity_groups: 'Nhóm tiện nghi',
           hotel_policy: 'Chính sách',
           hotel_stars: 'Hạng sao',
           hotel_types: 'Loại chỗ ở',
           meals: 'Gói bữa ăn có sẵn',
           price: 'Giá mỗi đêm'
       }
       function showPageSearch() {
           $('.page-detail, .customer-infor').hide();
           $('.page-search').show();
       }
       function createHtmlFilter(data) {
           let html = ``;
           for (const property in data) {
               if(data[property].length) {
                   if(property == 'hotel_stars') {
                       html += `<div class="search_filter_box">
                              <input type="hidden" name="hotel_stars" id="hotel_star">
                              <h3 class="search_filter_title">${title_filter[property]} <i class="fa fa-angle-up"></i></h3>
                              <div class="search_filter_box_item">`
                       for (let i = 0; i < data[property].length; i++) {
                           html += `
                          <span class="search_filter_start" data-rate="${data[property][i]}">${data[property][i]} <img src="assets/images/booking/search1.png" alt=""></span>
                      `;
                       }
                       html += `</div></div>`;
                   } else {
                       html += `<div class="search_filter_box">
                              <h3 class="search_filter_title">${title_filter[property]} <i class="fa fa-angle-up"></i></h3>
                              <div class="search_filter_box_item">`
                       for (let i = 0; i < data[property].length; i++) {
                           html += `
                          <p class="mb-7"><input type="checkbox" name="${property}" class="iCheck" value="${data[property][i].value}"> ${data[property][i].name}</p>
                      `;
                       }
                       html += `</div></div>`;
                   }
               }
           }
           return html;
       }

       async function postApi(url = '', data = {}) {
           // Default options are marked with *
           const response = await fetch(url, {
               method: 'POST', // *GET, POST, PUT, DELETE, etc.
               mode: 'cors', // no-cors, *cors, same-origin
               cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
               credentials: 'same-origin', // include, *same-origin, omit
               headers: {
                   'Content-Type': 'application/json'
                   // 'Content-Type': 'application/x-www-form-urlencoded',
               },
               redirect: 'follow', // manual, *follow, error
               referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
               body: JSON.stringify(data) // body data type must match "Content-Type" header
           });
           return response.json(); // parses JSON response into native JavaScript objects
       }
      //  const test = async () => {
      //     console.log("test post api")
      //     try {
             
      //      function createVisitor() {
      //          console.log('createVisitor');
      //          const url = $('#urlcreateVisitor').val();
      //          return $.ajax({
      //              type: "POST",
      //              url: url
      //          });
      //      }
      //       let customerId = $('#customer_id').val();
      //      if(!customerId) {
      //          let user = await createVisitor();
      //          if(user.success) {
      //              $('#customer_id').val(user.data.user_id);
      //          }
      //      }
      //      const url = $('#urlGetDetail').val();
      //      const id = $(this).attr('data-id');
      //      console.log("id", $(this).attr('data-id'))
      //      console.log("abc", $(this).attr('data-abc'))
      //      let dataDetail = JSON.parse(sessionStorage.getItem('data_detail'));
      //      dataDetail.hotel_id = id;
      //      console.log('dataDetail', dataDetail)
      //      sessionStorage.setItem('data_detail', JSON.stringify(dataDetail));
      //      $.ajax({
      //          type: "POST",
      //          url: url,
      //          data: dataDetail,
      //          beforeSend: function() {
      //              $('.loading').show();
      //          },
      //          success: function (response) {
      //              $('.loading').hide();
      //              $('.page-search').hide();
      //              $('.page-detail').show();
      //              $('.page-detail').html(response)
      //             initDetail();
      //          }
      //      });
      //          let requestHotel = JSON.parse(sessionStorage.getItem('data_detail'));
      //          let nightCount = $('#detailNightCount').val();
      //          let taxInclude = $('#detailTax').val();
      //          let isSale = $('#detailSale').val();
      //          let currency = $('#detailCurrency').val();
      //      console.log('requestHotel', requestHotel);
      //                 const result = await postApi("https://api.heyotrip.com/v2.0/api/hotel/rooms", requestHotel);
      //          console.log("result rooms", result)
      //     } catch (error) {
      //        console.log("error rooms", error)
      //     }
      //  }
       $(document).on('click', '.sale_choose_room', async function() {
          try {
             
           function createVisitor() {
               console.log('createVisitor');
               const url = $('#urlcreateVisitor').val();
               return $.ajax({
                   type: "POST",
                   url: url
               });
           }
            let customerId = $('#customer_id').val();
           if(!customerId) {
               let user = await createVisitor();
               if(user.success) {
                   $('#customer_id').val(user.data.user_id);
               }
           }
           const url = $('#urlGetDetail').val();
           const id = $(this).attr('data-id');
           let dataDetail = JSON.parse(sessionStorage.getItem('data_detail'));
           dataDetail.hotel_id = id;
           sessionStorage.setItem('data_detail', JSON.stringify(dataDetail));
           console.log('thanhurl',url);
           $.ajax({
               type: "POST",
               url: url,
               data: dataDetail,
               beforeSend: function() {
                   $('.loading').show();
               },
               success: function (response) {
                  console.log('thanh',response);
                   $('.loading').hide();
                   $('.page-search').css('display','none');
                   $('#search_box_choose').css('display','flex');
                   $('#search_box_choose').html(response)
                  initDetail();
               }
           });
               let requestHotel = JSON.parse(sessionStorage.getItem('data_detail'));
               let nightCount = $('#detailNightCount').val();
               let taxInclude = $('#detailTax').val();
               let isSale = $('#detailSale').val();
               let currency = $('#detailCurrency').val();
                      const result = await postApi("https://api.heyotrip.com/v2.0/api/hotel/rooms", requestHotel);
          } catch (error) {
             console.log("error rooms", error)
          }
       })
       
       function showNotification(msg, type) {
           var typeText = 'error';
           if (type == 1 || type == 200) typeText = 'success';
   
       }
   
       function getFilter(data) {
           $.ajax({
               type: "POST",
               url: $('#urlGetFilter').val(),
               data: data,
               success: function (response) {
                   if(response.success) {
                       $('.list-filter').html(createHtmlFilter(response.data));
   
                   }
               }
           });
       }
   
       function genPagination(data) {
           let page = parseInt(data.page_id);
           let pageCount = Math.ceil(data.totals / data.perpage);
   
           let html = '';
           if (pageCount > 1) {
               html += '<div class="d-flex align-items-center justify-content-between my-4">'
               html += `<p class="text-result">${txtShowing} `
               html += ((page - 1) * data.perpage + 1) + ' - ' + ((page * data.perpage) < data.totals ? page * data.perpage : data.totals) + txtOf + data.totals
               html += '</p>'
               html += '<div class="pagination">'
               html += '<ul id="page_list">';
               if (page > 1) {
                   html += '<li><a data-page="' + (page - 1 > 0 ? page - 1 : 1) + '" href="javascript:void(0);"><</a></li>';
                   html += '<li><a data-page="' + 1 + '" href="javascript:void(0)">1</a></li>';
               }
               else html += '<li class="active"><a data-page="' + 1 + '" href="javascript:void(0)">1</a></li>';
               let start = (page > 1) ? (page - 1) : 1;
               if (start != 1) html += '<li><a href="javascript:void(0)">...</a></li>';
               for (let i = start + 1; i <= page + 3 && i <= pageCount; i++) {
                   if (i == page) html += '<li class="active"><a data-page="' + i + '" href="javascript:void(0)">' + i + '</a></li>';
                   else html += '<li><a data-page="' + i + '" href="javascript:void(0)">' + i + '</a></li>';
               }
               if (page + 3 < pageCount) {
                   html += '<li><a href="javascript:void(0)">...</a></li>';
                   html += '<li><a data-page="' + pageCount + '" href="javascript:void(0)">' + pageCount + '</a></li>';
               }
               if (page < pageCount) html += '<li><a data-page="' + ((page + 1) < pageCount ? (page + 1) : pageCount) + '" href="javascript:void(0);">></a></li>';
               html += '</ul>';
               html += '</div>'
               html += '</div>';
           } else {
               html += '<div class="d-flex align-items-center justify-content-between my-4">'
               html += `<p class="text-result">${txtShowing} `
               html += ((page - 1) * data.perpage + 1) + ' - ' + ((page * data.perpage) < data.totals ? page * data.perpage : data.totals) + txtOf + data.totals
               html += '</p>'
               html += '</div>';
           }
   
           $('#pagination_list_hotel').append(html);
       }
   
       function showColorStar() {
           $('.star-rate').each(function () {
               var rate = $(this).attr('data-rate');
               rate1 = rate * 16;
               $(this).css('width', rate1);
           });
       }
       function yellowStar(quantity) {
           let html = ``;
           for(let i = 0 ; i < quantity ; i++){
               html += `<span class="iconify" data-icon="bxs:star" style="color: #FFC95B"></span>`
           }
           return html;
       }
       function darkStar(quantity) {
           let html = ``;
           for(let i = 0 ; i < quantity ; i++){
               html += `<span class="iconify" data-icon="bxs:star" style="color: gray"></span>`
           }
           return html;
       }
   
       function formatPrice(price, currency = '') {
           if(!currency) return price.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
           if(currency == 'VND' || currency == 'VNĐ') return price.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') + ' ' + currency;
           if(currency == 'USD') return new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(price);
           if(currency == 'CNY') return new Intl.NumberFormat('zh-cn', { style: 'currency', currency: 'CNY' }).format(price);
       }
       function emptySearch() {
           $('#box_result_search1').removeClass(`d-block`);
           $('#box_result_search1').addClass(`d-none`);
           $('#text_no_result').css(`display`,'block');
   
       }
       $('#text_no_result').css(`display`,'none');
   
   
       function genHotel(data,index) {
           let html = '';
           var urlHotelDetail = $('input#urlHotelDetail').val() + '/';
           let star=data.star;
           let txtSeeMap = 'Xem bản đồ';
           let txtFreeBreakfast = 'Miễn phí bữa ăn sáng';
           let txtFreeLunch = 'Miễn phí bữa ăn trưa';
           let txtFreeDinner = 'Miễn phí bữa ăn tối';
           let txtFreeCancel = 'Miễn phí hủy';
           let txtRoom = ' phòng';
           let txtPriceRoomNight = 'Giá 1 đêm, 1 phòng';
   
           let txtFrom = 'Chỉ từ ';
           let txtFor = ' cho ';
           let txtNight = ' đêm';
           let htmlModalAmenity = '';
           let txtAmenities = 'Tiện nghi';
   
           html += `      <div class="mt-4 row item_result_search test${index} fade-in">
                                  <div class="col-12 col-sm-3">
                                      <img
                                          src=${data.image} />
                                  </div>
                                  <div class="col-12 col-sm-9">
                                      <div class="row">
                                         <div class="col-9">
                                              <div class="font_size_18 font800">${data.name}</div>
                                              <div class="font_size_16 font400 mt-2">${data.address}</div>
                                              <div class="mt-2">
                                                  ${yellowStar(data.star)}
                                                  ${darkStar(5 - data.star)}
                                                  ${data.lat && data.lng ? `
                                                   <a href="javascript:void(0);" data-lat="${data.lat}" data-long="${data.lng}" data-name="${data.name}"  data-toggle="modal" data-target="#myModal${data.id}" class="lam show-map">${txtSeeMap}</a>` : ''}
                                                  </div>
                                                    ${data.free_breakfast ? `
                                                          <div class="d-flex">
                                                              <div class="d-flex align-items-center background_green px-2">
                                                                  <span class="iconify color_green icon20x20"
                                                                      data-icon="akar-icons:check"></span>
                                                                  <span class="color_green font_size_14 ml-1">${txtFreeBreakfast}</span>
                                                              </div>
                                                          </div>` : ''}
                                                    ${data.free_lunch ? `
                                                          <div class="d-flex mt-2">
                                                              <div class="d-flex align-items-center background_green px-2">
                                                                  <span class="iconify color_green icon20x20"
                                                                      data-icon="akar-icons:check"></span>
                                                                  <span class="color_green font_size_14 ml-1">${txtFreeLunch}</span>
                                                              </div>
                                                          </div>
                                                          ` : ''}
                                                    ${data.free_dinner ? `
                                                          <div class="d-flex mt-2">
                                                              <div class="d-flex align-items-center background_green px-2">
                                                                  <span class="iconify color_green icon20x20"
                                                                      data-icon="akar-icons:check"></span>
                                                                  <span class="color_green font_size_14 ml-1">${txtFreeDinner}</span>
                                                              </div>
                                                          </div>
                                                        `: ''}
                                                    ${data.free_cancel ? `
                                                          <div class="d-flex mt-2">
                                                              <div class="d-flex align-items-center background_green px-2">
                                                                  <span class="iconify color_green icon20x20"
                                                                      data-icon="akar-icons:check"></span>
                                                                  <span class="color_green font_size_14 ml-1">${txtFreeCancel}</span>
                                                              </div>
                                                          </div>
                                                         ` : ''}
                                                   </div>
                                                 <div class="d-flex col-3 text-right flex-direction-column justify-content-end align-items-end">
                                                      <div class="font_size_15 color8">${txtPriceRoomNight}ss</div>
                                                      <div class="font_size_15 color_green mt-2">Chỉ từ ${formatPrice(data.price_from, data.currency)}</div>
                                                      <div class="font_size_15 color8 mt-2 line_through">${formatPrice(data.price_before_sale, data.currency)}</div>
                                                      <div class="font_size_15 color3"><b>${formatPrice(data.price_by_night, data.currency)}</b> ${txtFor} ${data.night_count} ${txtNight}</div>
                                                      <button  class="background_purple color_white px-3 border_radius_3 mt-3 font400 sale_choose_room" data-abc="${data.id}" data-id="${data.id}" type="button">Chọn phòng</button>
                                              </div>
                                         </div>
                                      <div class="mt-2 d-flex align-items-center">
                                              <div class="d-flex align-items-center">
                                                  <span class="iconify color8 icon20x20" data-icon="ri:hotel-line"></span>
                                                  <span class="font_size_13 color3 ml-1">${data.center_distance} km </span>
                                              </div>
                                              <div class="d-flex align-items-center ml-3">
                                                  <span class="iconify color8 icon20x20"
                                                      data-icon="ic:baseline-flight"></span>
                                                  <span class="font_size_13 color3 ml-1">${data.airport_distance} km  </span>
                                           </div>`
                                           if (data.top_amenities.length > 0) {
                                               let countTopAmenity = data.top_amenities.length <= 3 ? data.top_amenities.length : 3;
                                               for (let i = 0; i < countTopAmenity; i++) {
                                                   html += `<span class="aminjn"><img src="${data.top_amenities[i].icon}" alt=""> </span>`
                                               }
                                               html += `<span class="sale_choose_service_text" data-toggle="modal" data-target="#amenities_${data.id}">${txtAmenities}</span>`
                                               htmlModalAmenity += `<div class="modal fade amenities" id="amenities_${data.id}" tabindex="-1" role="dialog" aria-labelledby="amenities" aria-hidden="true">`
                                               htmlModalAmenity += `<div class="modal-dialog" role="document">`
                                               htmlModalAmenity += `<div class="modal-content">`
                                               htmlModalAmenity += `<button type="button" class="close" data-dismiss="modal" aria-label="Close">`
                                               htmlModalAmenity += `<span aria-hidden="true">&times;</span>`
                                               htmlModalAmenity +=`</button>`
                                               htmlModalAmenity += `<div class="row col-mar-5">`
                                               for (let j = 0; j < data.top_amenities.length; j++) {
                                                   htmlModalAmenity += `<div class="col-6 col-md-4 mb-3">`
                                                   htmlModalAmenity += `<p class="icon-modal"><img src="${data.top_amenities[j].icon}" class="iconsss" alt="">${data.top_amenities[j].name}</p>`
                                                   htmlModalAmenity += '</div>'
                                               }
                                               htmlModalAmenity += '</div>'
                                               htmlModalAmenity += '</div>'
                                               htmlModalAmenity += '</div>'
                                               htmlModalAmenity += '</div>'
                                            }`
                                          </div>
                                  </div>
                              <div class="modal fade" id="myModal${data.id}" >
                                  <div class="modal-dialog modal-dialog-centered" role="document">
                                      <div class="modal-content">
                                          <div class="modal-body">
                                              <div id="map-hotel"></div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
   
                        `;
   
           html += htmlModalAmenity;
           $('#box_result_search1').append(html);
           $('#box_result_search1').removeClass(`d-none`);
           $('#box_result_search1').addClass('d-block');
       }
       let text_no_result = 'Không có kết quả tìm kiếm phù hợp'
       function mergeObjInArr(arr = []) {
           let result = [];
           if(arr.length) {
               arr.forEach(function(item) {
                   var existing = result.filter(function(v, i) {
                       return v.name == item.name;
                   });
                   if (existing.length) {
                       var existingIndex = result.indexOf(existing[0]);
                       result[existingIndex].value = result[existingIndex].value.concat(item.value);
                   } else {
                       if (typeof item.value == 'string')
                           item.value = [item.value];
                       result.push(item);
                   }
               });
           }
           return result;
       }
       let quantityResult = 0;
      //  function searchHotel(dataRequest) {
      //      $('.loading').show();
      //      sessionStorage.setItem('data_request', JSON.stringify(dataRequest));
      //      postApi('https://api.heyotrip.com/v2.0/api/hotel/search', dataRequest)
      //          .then((res) => {
   
      //              $('.loading').hide();
      //              if (res.success) {
      //                  let data = res.data;
      //                  let listHotel = data.hotels ? data.hotels : [];
      //                  if (listHotel.length) {
      //                      quantityResult = listHotel.length;
      //                      $('.container-fluid').show();
      //                          for (let i = 0; i < listHotel.length; i++) {
      //                              genHotel(listHotel[i], i);
      //                          }
      //                      genPagination(data);
      //                      showColorStar();
      //                      $('.search_list').show();
      //                  } else {
      //                      emptySearch();
      //                      showNotification(text_no_result, 1);
      //                      $('.search_list').hide();
      //                  }
      //              } else {
      //                  console.log(2)
      //                  showNotification(res.messages, NOTI_ERR);
      //              }
      //          })
      //          .catch((err) => {
      //           showNotification(txtError, NOTI_ERR);
      //      });
      //      return false;
      //  }
      const searchHotel = async(dataRequest) => { 
         try {
            sessionStorage.setItem('data_request', JSON.stringify(dataRequest));
            const result = await postApi('https://api.heyotrip.com/v2.0/api/hotel/search', dataRequest);
            console.log('result', result)
                   if (result.success) {
                      console.log(result.data, result)
                       let data = result.data;
                       let listHotel = data.hotels ? data.hotels : [];
                       if (listHotel.length) {
                           quantityResult = listHotel.length;
                           $('.container-fluid').show();
                               for (let i = 0; i < listHotel.length; i++) {
                                   genHotel(listHotel[i], i);
                               }
                           genPagination(data);
                           showColorStar();
                           $('.search_list').show();
                       } else {
                           emptySearch();
                           showNotification(text_no_result, 1);
                           $('.search_list').hide();
                       }
                   } else {
                       showNotification(res.messages, NOTI_ERR);
                   }
         } catch (error) {
            console.log('error search', error)
         }
      }
      
       $('.search_home_submit').click( function (e) { // tìm kiếm ks
           $("#base_room_hotel").css('display','none')
           $("#search_box_choose").css('display','none')
           $("#box_result_search").css('display','flex')
           showPageSearch();
           let locationName = $('.search_going_name').val();
           let locationType = $('#locationType').val();
   
           let locationId = $('#dataId').val();
           // if(!Number(locationId)) {
           //     showNotification('Chưa chọn điểm đến',0)
           //     return;
           // }
           //
           // let timeCheckHotel = $('.time_check').val();
           // let timeChecks = timeCheckHotel.split(' - ');
           // let fromDate = $("#fromDate").val();
           let fromDate = "2022-06-02"
           // let toDate = $("#toDate").val();
           let toDate = "2022-06-04"
           let list_rooms = [];
           $('.customer-item').each(function (index, element) {
               let adt = $(this).find('.people_number.adults').val();
               let chd = $(this).find('.people_number.children').val();
               let ageOfChildren = [];
               if (chd > 0) {
                   for (let i = 1; i <= chd; i++) {
                       let ageVal = $(this).find('#select_age_' + i).val();
                       ageOfChildren.push(ageVal);
                   }
               }
               list_rooms.push({
                   adt: parseInt(adt),
                   chd: parseInt(chd),
                   chd_ages: ageOfChildren
               })
           });
           let requestHotel = {
               "area_id": locationId,
               "keyword_type": "0",
               "checkin_date": fromDate,
               "checkout_date": toDate,
               "list_rooms": list_rooms,
               "currency": "VND",
               "is_admin": 1,
               "page_id": 1
           };
           let dataGetFilter = {...requestHotel};
           delete dataGetFilter.is_admin;
           delete dataGetFilter.page_id;
   
           let dataGetDetail = {...dataGetFilter}
           $('#search_list_hotel').empty();
           $('#pagination_list_hotel').empty();
           sessionStorage.setItem('data_detail', JSON.stringify(dataGetDetail));
           searchHotel(requestHotel);
           getFilter(requestHotel);
       })
           .on('click', '#page_list li', function (e) { // click phân trang
               let currPage = $(this).children().attr('data-page');
   
               let requestHotel = JSON.parse(sessionStorage.getItem('data_request'));
               requestHotel.page_id = currPage;
   
               $('#search_list_hotel').empty();
               $('#pagination_list_hotel').empty();
               searchHotel(requestHotel);
           })
           .on('click', '.show_amenities', function (e) {
               $('html,body').animate({
                   scrollTop: $('#detail_amenities').offset().top
               });
           })
           .on('click', '.your_room_book', async function() { // chọn phòng
               $('.loading').show();
               const urlChooseRoom = $('#urlChooseRooms').val();
               const urlGetOrder = $('#urlGetOrder').val();
   
               let customerId = $('#customer_id').val();
               let rooms = JSON.parse(sessionStorage.getItem('rooms'));
               let dataRoomId = $(this).attr('data-room');
               let dataRoomPlanId = $(this).attr('room-plan');
               let roomSelected = rooms[dataRoomId];
               let dataDetail = JSON.parse(sessionStorage.getItem('data_detail'));
               let dataRoom = {...dataDetail};
               dataRoom.user_id = customerId;
               dataRoom.room_plans = [roomSelected.room_plans[dataRoomPlanId]];
               let roomChoosed = await postApi(urlChooseRoom, dataRoom);
               $('.loading').hide();
               if(roomChoosed.code == CODE_SUCCESS) {
                   sessionStorage.setItem('room_choosed', JSON.stringify(roomChoosed.data));
                   let hotelDetail = await postApi(urlGetOrder, {user_id: customerId, order_id: roomChoosed.data.order_id});
                   if(hotelDetail.code == CODE_SUCCESS) {
                       getViewInfoCustomer(hotelDetail.data);
                   } else {
                       showNotification(txtError, NOTI_ERR);
                       console.log('====================================');
                       console.log(hotelDetail.messages);
                       console.log('====================================');
                   }
               } else {
                   showNotification(txtError, NOTI_ERR);
                   console.log('====================================');
                   console.log(roomChoosed.messages);
                   console.log('====================================');
               }
           })
   
       $(document).on('click', '.show-map', function() {
   
           const lat = $(this).attr('data-lat');
           const lng = $(this).attr('data-long');
           const name = $(this).attr('data-name')
           var myLatlng = new google.maps.LatLng(lat,lng);
           var mapOptions = {
               zoom: 16,
               center: myLatlng
           }
           var map = new google.maps.Map(document.getElementById("map-hotel"), mapOptions);
   
           var marker = new google.maps.Marker({
               position: myLatlng
           });
   
           marker.setMap(map);
           $('#map-hotel').css('height', '500px');
           $('#modal-map').modal('show');
       });
       $("#serach_ajax2").css('display','none')
   
   
       function createVisitor() {
           
           console.log('createVisitor');
           const url = $('#urlcreateVisitor').val();
           return $.ajax({
               type: "POST",
               url: url
           });
       }
       app.initDetail = () => {
           console.log('initDetail')
           initMap();
           app.css();
           app.genRoomData();
       }
       app.genRoomData = function () {
           console.log('ok');
           let requestHotel = JSON.parse(sessionStorage.getItem('data_detail'));
           let nightCount = $('#detailNightCount').val();
           let taxInclude = $('#detailTax').val();
           let isSale = $('#detailSale').val();
           let currency = $('#detailCurrency').val();
           postApi($('#urlListRoom').val(), requestHotel)
               .then((result) => {
                   let res = result;
                   if (res.success) {
                       let data = res.data;
                       let listRoom = data.rooms ? data.rooms : [];
                       if (listRoom.length) {
                           sessionStorage.setItem('rooms', JSON.stringify(listRoom));
                           for (let i = 0; i < listRoom.length; i++) {
                               genRoom(listRoom[i], nightCount, taxInclude, isSale, currency, i);
                           }
                       } else {
                           $('#list_room_hotel').html(`<h3>${txtRoomEmpty}</h3>`);
                           sessionStorage.setItem('rooms', '');
                       }
                       if (data.amenity_groups && data.amenity_groups.length > 0) {
                           genAmenity(data.amenity_groups);
                       }
                   } else {
                       showNotification(res.messages, NOTI_ERR);
                   }
               }).catch((err) => {
               console.error(err);
               showNotification(txtError, NOTI_ERR);
           });
       }
       $(".dataLocation_click").click(function () {
           let text=$(this).data('test');
           $('#dataId').val(text);
   
       });
       $("#locationsss").click(function () {
           $("#serach_ajax2").css('display','block')
           $("#custom_datepicker").css('display','none')
           $("#row_room_hotel").css('display','none')
   
       })
       $('#search_filter_button').click(function () {
           for(let i = 0 ; i < quantityResult ; i++){
               $(`.test${i}`).remove();
           }
           let dataRequest = JSON.parse(sessionStorage.getItem('data_request'));
           let dataFilter = $('#form-filter').serializeArray();
   
           dataFilter = mergeObjInArr(dataFilter);
           let filters = {};
           for (let i = 0; i < dataFilter.length; i++) {
               const el = dataFilter[i];
               if(['hotel_name', 'hotel_stars'].includes(el.name)) el.value = el.value.toString();
               if(el.value || el.value.length) {
                   if(el.name == 'hotel_stars') filters[el.name] = Number(el.value);
                   else filters[el.name] = el.value;
               }
           }
           if(!$.isEmptyObject(filters)) $('.reset_filter_button').attr('disabled', false);
           dataRequest.filters = filters;
           dataRequest.page_id = 1;
           $('#search_list_hotel').empty();
           $('#pagination_list_hotel').empty();
           searchHotel(dataRequest);
       })
       $('#search_box_choose').css('display','none');
   
       $('#box_result_search1').click( async function() {
           let customerId = $('#customer_id').val();
           // if(!customerId) {
           //     let user = await createVisitor();
           //     if(user.success) {
           //         $('#customer_id').val(user.data.user_id);
           //     }
           // }
           const url = $('#urlGetDetail').val();
           console.log(url);
           const id = $(this).attr('data-id');
           let dataDetail = JSON.parse(sessionStorage.getItem('data_detail'));
           dataDetail.hotel_id = id;
           sessionStorage.setItem('data_detail', JSON.stringify(dataDetail));
           $.ajax({
               type: "POST",
               url: url,
               data: dataDetail,
               beforeSend: function() {
                   $('.loading').show();
               },
               success: function (response) {
                   $('.loading').hide();
                   $('.page-search').hide();
                   $('.page-detail').show();
                   $('.page-detail').html(response)
                   app.initDetail();
               }
           });
           $('#box_result_search').css('display','none');
           $('#search_box_choose').css('display','block');
       })
   
   
   });
</script>
<style>
   span img {
   width: 15px!important;
   height: 15px!important;
   object-fit: contain;
   }
   .aminjn img {
   vertical-align: middle!important;
   }
   .aminjn {
   padding-right: 10px;
   margin-right: 10px;
   display: inline-block;
   }
   .icon-modal{
   margin: 0 0 10px;
   }
   .iconsss{
   width: 30px!important;
   height: 30px!important;
   object-fit: contain!important;
   margin-right: 12px!important;
   }
   a .iconify {
   color: #000000;
   }
   #serach_ajax2 p a{
   margin-left: 10px;
   }
   #serach_ajax2 p a{
   color: black;
   }
   #serach_ajax2 p:hover a{
   color: white!important;
   }
   #serach_ajax2 p:hover {
   background: blue;
   width: 100%;
   }
</style>
<script>
   $(document).ready(function () {
       $(".search_home_submit").click(function () {
   
    	document.getElementById("box_result_search").classList.add("show");
       });
       let txtShowing = 'Hiển thị ';
   
       function genRoom(data, nightCount = 1, taxInclude = false, isSale = false, currency = "VND", data_order = 0) {
       let html = '';
   console.log('genRoom');
       html += `<div class="your_room_item">
               <h3 class="your_room_title" data-toggle="collapse" data-target="#hotel-${data.id}" aria-expanded="true">${data.name} <span><i class="fa fa-angle-down" aria-hidden="true"></i></span></h3>
               <div class="your_room_item_ct collapse in" id="hotel-${data.id}" aria-expanded="true">
               <div class="row col-mar-6">
               <div class="col-12 col-md-4">
               ${data.main_img ? `<a href="javascript:void(0)" class="c-img ptc-70 mb-24 hv-light"><img src="${data.main_img}" alt="${data.name}"></a>` : ''}
               <p class="mb-24"><img src="assets/images/booking/yr1.png" alt="" class="mr-12"> 1 King Bed</p>
               <p class="mb-24"><img src="assets/images/booking/yr0.png" alt="" class="mr-12"> Sea views</p>
               <a href="javascript:void(0)" class="room_link">${txtRoomDetailAndImage} <i class="fa fa-angle-right black" aria-hidden="true"></i></a>
               </div>
               <div class="col-12 col-md-8">`;
   
       let htmlAmenityInRoom = '';
       if (data.amenity_groups && data.amenity_groups.length) {
           let count = 0;
           let listAmenity = data.amenity_groups;
           for (let i = 0; i < listAmenity.length; i++) {
               if (listAmenity[i].amenities.length) {
                   let child = listAmenity[i].amenities;
                   for (let j = 0; j < child.length; j++) {
                       if (count < 6) {
                           htmlAmenityInRoom += '<div class="col-12 col-sm-6 col-md-4">'
                           htmlAmenityInRoom += `<p class="mb-20"><img src="assets/images/booking/yr5.png"> ${child[j].name}</p>`
                           htmlAmenityInRoom += '</div>'
                       }
                       count++;
                   }
               }
           }
       }
       let htmlRoom = '';
       if (data.room_plans.length) {
           let roomPlans = data.room_plans;
           for (let i = 0; i < roomPlans.length; i++) {
               if (i == 0 && roomPlans.length > 1) {
                   htmlRoom += `<div class="your_room_content border_red mb-20">`
                   htmlRoom += `<a javascript:void(0)" class="your_room_best_price">${txtBestPrice}</a>`
               } else {
                   htmlRoom += `<div class="your_room_content mb-20">`
               }
               htmlRoom += `<div class="row d-flex">`;
               htmlRoom += `<div class="col-12 col-sm-6">`;
               htmlRoom += `${roomPlans[i].free_breakfast ? `<p class="mb-12"><span class="your_room_tag"><img src="assets/images/booking/yr5.png"> ${txtFreeBreakfast}</span></p>` : ''}`
               htmlRoom += `${roomPlans[i].free_lunch ? `<p class="mb-12"><span class="your_room_tag"><img src="assets/images/booking/yr5.png"> ${txtFreeLunch}</span></p>` : ''}`
               htmlRoom += `${roomPlans[i].free_dinner ? `<p class="mb-12"><span class="your_room_tag"><img src="assets/images/booking/yr5.png"> ${txtFreeDinner}</span></p>` : ''}`
               htmlRoom += `${roomPlans[i].free_cancel ? `<p class="mb-12"><span class="your_room_tag"><img src="assets/images/booking/yr5.png"> ${txtFreeCancel}</span></p>` : ''}`
               htmlRoom += '</div>'
               htmlRoom += '<div class="col-12 col-sm-1 d-flex align-items-center">'
               htmlRoom += `${roomPlans[i].adults_included > 0 ? `<p class="mb-7 d-flex align-items-center"><img src="assets/images/booking/yr2.png" class="mr-12"> x${roomPlans[i].adults_included}</p>` : ''}`
               htmlRoom += '</div>'
               htmlRoom += '<div class="col-12 col-sm-1 d-flex align-items-center">'
               htmlRoom += `${roomPlans[i].children_included > 0 ? `<p class="mb-7 d-flex align-items-center"><img src="assets/images/booking/yr2.png" class="mr-12 icon-chd"> x${roomPlans[i].children_included}</p>` : ''}`
               htmlRoom += '</div>'
               htmlRoom += '<div class="col-12 col-sm-5 text-right">'
               htmlRoom += `<p class="font_small mb-7">${txtPriceRoomNight}</p>`
               htmlRoom += `<p class="your_room_price mb-7">${formatPrice(roomPlans[i].price, currency)}</p>`
               // htmlRoom += `<p class="your_room_price_old mb-7"></p>`
               htmlRoom += `<p class="mb-7"><strong>${formatPrice(roomPlans[i].price * nightCount, currency)} </strong> ${txtFor} 1 ${txtRoom}, ${nightCount} ${txtNight}</p>`
               htmlRoom += `${taxInclude ? `<p class="mb-24">${txtTaxIncluded}</p>` : ''}`
               htmlRoom += `${isSale ? '<img src="assets/images/booking/yr4.png">' : ''}`
               htmlRoom += '</div>'
               htmlRoom += '</div>'
   
               htmlRoom += '<div class="your_room_content_bot">'
               htmlRoom += '<div class="row d-flex">'
               htmlRoom += '<div class="col-12 col-sm-9 your_room_line">'
               htmlRoom += '<div class="row">'
               htmlRoom += htmlAmenityInRoom
               htmlRoom += '</div>'
               htmlRoom += '</div>'
               htmlRoom += '<div class="col-12 col-sm-3 d-flex align-items-center justify-content-end">'
               htmlRoom += `<a href="javascript:void(0)" class="your_room_book" data-room="${data_order}" room-plan="${i}">${txtBookNow}</a>`
               htmlRoom += '</div>'
               htmlRoom += '</div>'
               htmlRoom += '</div>';
               htmlRoom += '</div>';
           }
       }
       html += htmlRoom;
       html += '</div>'
       html += '</div>'
       html += '</div>'
       html += '</div>'
       html += '</div>'
       $('#list_room_hotel').append(html);
   }  
   
   const NOTI_ERR = 0;  let txtError = 'Có lỗi xảy ra trong quá trình thực hiện';
   function genPagination(data) {
    let page = parseInt(data.page_id);
    let pageCount = Math.ceil(data.totals / data.perpage);

    let html = '';
    if (pageCount > 1) {
        html += '<div class="d-flex align-items-center justify-content-between my-4">'
        html += `<p class="text-result">${txtShowing} `
        html += ((page - 1) * data.perpage + 1) + ' - ' + ((page * data.perpage) < data.totals ? page * data.perpage : data.totals) + txtOf + data.totals
        html += '</p>'
        html += '<div class="pagination">'
        html += '<ul id="page_list">';
        if (page > 1) {
            html += '<li><a data-page="' + (page - 1 > 0 ? page - 1 : 1) + '" href="javascript:void(0);"><</a></li>';
            html += '<li><a data-page="' + 1 + '" href="javascript:void(0)">1</a></li>';
        }
        else html += '<li class="active"><a data-page="' + 1 + '" href="javascript:void(0)">1</a></li>';
        let start = (page > 1) ? (page - 1) : 1;
        if (start != 1) html += '<li><a href="javascript:void(0)">...</a></li>';
        for (let i = start + 1; i <= page + 3 && i <= pageCount; i++) {
            if (i == page) html += '<li class="active"><a data-page="' + i + '" href="javascript:void(0)">' + i + '</a></li>';
            else html += '<li><a data-page="' + i + '" href="javascript:void(0)">' + i + '</a></li>';
        }
        if (page + 3 < pageCount) {
            html += '<li><a href="javascript:void(0)">...</a></li>';
            html += '<li><a data-page="' + pageCount + '" href="javascript:void(0)">' + pageCount + '</a></li>';
        }
        if (page < pageCount) html += '<li><a data-page="' + ((page + 1) < pageCount ? (page + 1) : pageCount) + '" href="javascript:void(0);">></a></li>';
        html += '</ul>';
        html += '</div>'
        html += '</div>';
    } else {
        html += '<div class="d-flex align-items-center justify-content-between my-4">'
        html += `<p class="text-result">${txtShowing} `
        html += ((page - 1) * data.perpage + 1) + ' - ' + ((page * data.perpage) < data.totals ? page * data.perpage : data.totals) + txtOf + data.totals
        html += '</p>'
        html += '</div>';
    }

    $('#pagination_list_hotel').append(html);
}

   function showNotification(msg, type) {
       console.log('showNotification');
       var typeText = 'error';
       if (type == 1 || type == 200) typeText = 'success';
       var notice = new PNotify({
           title: 'Thông báo',
           text: msg,
           type: typeText,
           delay: 2000,
           addclass: 'stack-bottomright',
           stack: {"dir1": "up", "dir2": "left", "firstpos1": 15, "firstpos2": 15}
       });
   }
   
      //  $(document).on('click', '.sale_choose_room', async function() {
      //      function createVisitor() {
      //          console.log('createVisitor');
      //          const url = $('#urlcreateVisitor').val();
      //          return $.ajax({
      //              type: "POST",
      //              url: url
      //          });
      //      }
      //       let customerId = $('#customer_id').val();
      //      if(!customerId) {
      //          let user = await createVisitor();
      //          if(user.success) {
      //              $('#customer_id').val(user.data.user_id);
      //          }
      //      }
      //      const url = $('#urlGetDetail').val();
      //      const id = $(this).attr('data-id');
      //      console.log("id", $(this).attr('data-id'))
      //      console.log("abc", $(this).attr('data-abc'))
      //      let dataDetail = JSON.parse(sessionStorage.getItem('data_detail'));
      //      dataDetail.hotel_id = id;
      //      sessionStorage.setItem('data_detail', JSON.stringify(dataDetail));
      //      $.ajax({
      //          type: "POST",
      //          url: url,
      //          data: dataDetail,
      //          beforeSend: function() {
      //              $('.loading').show();
      //          },
      //          success: function (response) {
      //              $('.loading').hide();
      //              $('.page-search').hide();
      //              $('.page-detail').show();
      //              $('.page-detail').html(response)
      //             initDetail();
      //          }
      //      });
      //          let requestHotel = JSON.parse(sessionStorage.getItem('data_detail'));
      //          let nightCount = $('#detailNightCount').val();
      //          let taxInclude = $('#detailTax').val();
      //          let isSale = $('#detailSale').val();
      //          let currency = $('#detailCurrency').val();
      //          postApi('https://api.heyotrip.com/v2.0/api/hotel/rooms', requestHotel)
      //              .then((result) => {
      //                  let res = result;
      //                  if (res.success) {
      //                      let data = res.data;
      //                      let listRoom = data.rooms ? data.rooms : [];
      //                      if (listRoom.length) {
      //                          sessionStorage.setItem('rooms', JSON.stringify(listRoom));
      //                          for (let i = 0; i < listRoom.length; i++) {
      //                             console.log('poooi');
      //                              genRoom(listRoom[i], nightCount, taxInclude, isSale, currency, i);
      //                          }
      //                      } else {
      //                          $('#list_room_hotel').html(`<h3>${txtRoomEmpty}</h3>`);
      //                          sessionStorage.setItem('rooms', '');
      //                      }
      //                      if (data.amenity_groups && data.amenity_groups.length > 0) {
      //                          genAmenity(data.amenity_groups);
      //                      }
      //                  } else {
      //                      showNotification(res.messages, NOTI_ERR);
      //                  }
      //              }).catch((err) => {
      //                 console.log('abc',requestHotel);
      //              console.error(err);
      //              showNotification(txtError, NOTI_ERR);
      //          });
      //      });
        }) ;
    
   
   
   
</script>
<style>
#serach_ajax2 {
  
  height: 400px;
  overflow: scroll;
}


</style>