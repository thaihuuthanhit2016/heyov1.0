@extends('admin.master');
@section('content')
    <div class="box_content">
        <div class="wrapper">
            <div class="box_title">
                <div class="title">Tra cứu khuyến mãi</div>
            </div>
            <div style="margin: 0px" class="dropdown-divider"></div>
            <form method="post">
                @csrf
                <div class="content">
                    <div class="row_input">
                        <div class="box_input">
                            <div class="label">Mã chương trình</div>
                            <input placeholder="Nhập chương trình"   name="program_code_promo"value="{{Session::get('program_code_promo')}}"class="input" />
                        </div>
                        <div class="box_input">
                            <div class="label">Mã khuyến mãi</div>
                            <input placeholder="Nhập mã chương trình"  name="code_promotion" value="{{Session::get('code_promotion')}}"class="input" />
                        </div>
                    </div>
                    <div class="row_input">
                        <div class="box_input">
                            <div class="label">Loại khuyến mãi</div>
                            <select id="type_promo" name="type_promo">
                                <option value>chọn</option>
                                <option value="1" @if(Session::get('type_promo')==1) selected @endif >Promo Code</option>
                                <option value="2" @if(Session::get('type_promo')==2) selected @endif >Unique Code</option>
                                <option value="3" @if(Session::get('type_promo')==3) selected @endif >Partner Code</option>
                                <option value="4" @if(Session::get('type_promo')==4) selected @endif > Special Campaign</option>
                            </select>
                        </div>
                        <div class="box_input">
                            <div class="label">Trạng thái</div>
                            <select name="status">
                                <option value>chọn</option>
                                <option @if(Session::get('status')=='Thanh') selected @endif value="0">Inactive</option>
                                <option @if(Session::get('status')==1) selected @endif value="1">Active</option>
                                <option @if(Session::get('status')==2) selected @endif value="2">Deactive</option>
                                <option @if(Session::get('status')==3) selected @endif value="3">Expired</option>
                            </select>
                        </div>
                        <?php
                        if(Session::put('status','')){
                            Session::put('status','Thanh');
                        }
                        ?>
                        <div class="box_input">
                            <div class="label">Quốc gia</div>
                            <select name="nation" >
                                <option value>chọn</option>
                                @foreach($locations as $l)
                                    <option @if(Session::get('nation')==$l->id)   selected @endif value="{{$l->id}}">{{$l->country_name_vn}} - {{$l->country_name_en}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row_input">
                        <div class="box_input">
                            <div class="label">Ngày bắt đầu</div>
                            <div class="wrapper_date">
                                <div style="width: 100%;margin-top: 5px;" class="input-group date" id="datetimepicker1">
                                    <input type="text" name="date_start"  value="{{Session::get('date_start')}}"  class="form-control" >
                                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                                </div>
                            </div>
                        </div>
                        <div class="box_input">
                            <div class="label">Ngày kết thúc</div>
                            <div class="wrapper_date">
                                <div style="width: 100%;margin-top: 5px;" class="input-group date" id="datetimepicker2">
                                    <input type="text"name="date_end"value="{{Session::get('date_end')}}"  class="form-control" >
                                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box_option">
                    <button type="submit" style="text-decoration: none;border: 0px;" class="button_option">
                        <span style="width: 20px; height: 20px; color: #fff;" class="iconify" data-icon="carbon:search"></span>
                        <span class="name_button_option">Tìm kiếm</span>
                    </button>
                    <p style="cursor: pointer" class="btn button_clear" id="delete_search">
                        Xoá bộ lọc
                    </p>
                </div>
            </form>
        </div>
        <div class="wrapper">
            <div class="box_title">
                <div class="title">4 khuyến mãi</div>
                <button data-toggle="modal" data-target="#modalCreate" style="text-decoration: none;" class="button_option">
                 <span style="width: 20px; height: 20px; color: #fff;" class="iconify"
               data-icon="fluent:add-16-filled"></span>
                    <span class="name_button_option" >Thêm mới</span>
                </button>
            </div>
            <div class="content">
                <table>
                    <tr>
                        <th class="small"> </th>
                        <th class="medium">Mã chương trình</th>
                        <th class="medium">Mã khuyến mãi</th>
                        <th class="big">Chương trình khuyến mãi</th>
                        <th class="medium">Loại khuyến mãi</th>
                        <th class="medium">Ngày bắt đầu</th>
                        <th class="medium">Ngày kết thúc</th>
                        <th class="medium">Số lượng</th>
                        <th class="medium">Người tạo</th>
                        <th class="small"> </th>
                    </tr>
                    <?php $i=0;?>
                    @if($promo!=null)
                        @foreach($promo as $po)
                            <?php
                            $i++;
                            $program_code_promo=$po->program_code_promo;
                            $tach=explode(' ',$program_code_promo);
                            $user=DB::table('tbl_admin')->where('id_admin',$po->ud_user)->first();
                            $countries=DB::table('countries')->where('id',$po->id_nation)->first();
                            $type_discount=DB::table('tbl_type_discount')->where('id_type_discount',$po->id_type_discount)->first();
                            ?>
                            <tr>
                                <td style="text-align: center;">
                                    <input id="toggle-state-switch" type="checkbox" data="1" value="true" onchange="test(value)"
                                           data-toggle="toggle" data-size="sm" data-style="ios">
                                </td>
                                <td data-toggle="modal" data-target="#modalDetail{{$i}}" style="cursor: pointer" class="ma_chuong_trinh">
                                    <div>{{$tach[0]}}</div>
                                    <div>{{$tach[1]}}</div>
                                </td>
                                <td data-toggle="modal" data-target="#modalDetail{{$i}}" style="cursor: pointer" class="ma_khuyen_mai">
                                    <div class="ten">{{$po->code_promotion}}</div>
                                    <div class="active">
                                        @if($po->status==0) <span style="color: orange">Inactive</span> @elseif($po->status==1)<span style="color: green">Active</span> @elseif($po->status==2)<span style="color: red">Deactive</span> @else<span style="color: grey"> Expired </span>@endif</a>
                                    </div>
                                </td>
                                <td data-toggle="modal" data-target="#modalDetail{{$i}}" style="cursor: pointer" class="chuong_trinh_khuyen_mai">
                                    {{$po->title }}        </a>
                                </td>
                                <td data-toggle="modal" data-target="#modalDetail{{$i}}" style="cursor: pointer" class="loai_khuyen_mai">
                                    <div class="ten"> @if($po->type_promo ==1)Promo Code @elseif($po->type_promo ==2) Unique Code @elseif($po->type_promo ==3)Partner Code @else Special Campaign @endif</a></div>
                                    <div class="country">{{$countries->country_name_vn}}</div>
                                </td>
                                <td data-toggle="modal" data-target="#modalDetail{{$i}}" style="cursor: pointer" class="loai_khuyen_mai">
                                    <span class="ten">{{$po->date_start}}</span>
                                </td>
                                <td data-toggle="modal" data-target="#modalDetail{{$i}}" style="cursor: pointer" class="loai_khuyen_mai">
                                    <span class="ten"> {{$po->date_end}}</span>
                                </td>
                                <td data-toggle="modal" data-target="#modalDetail{{$i}}" style="cursor: pointer" class="loai_khuyen_mai">
                                    <span class="ten">{{$po->number_max}}</span>
                                </td>
                                <td data-toggle="modal" data-target="#modalDetail{{$i}}" style="cursor: pointer" class="loai_khuyen_mai">
                  <span class="ten">                            @if($po->ud_user!=1) {{$user->admin_name}} @else Admin @endif</a>
                  </span>
                                </td>
                                <td>
                                    <div class="wrapper_icon">
                                        <div id="dots{{$i}}">
                        <span style="color: #C1C1C1; width: 25px; height: 25px;" class="iconify"
                              data-icon="bi:three-dots"></span>
                                        </div>
                                        <div id="box_option_discount{{$i}}">
                                            <div id="btn_edit_table_row{{$i}}" class="button_option_discount">
                                                <span style="width: 20px;height: 20px;" class="iconify" data-icon="clarity:note-edit-line"></span>
                                                <div class="name" data-toggle="modal" data-target="#modalEdit{{$i}}">Chỉnh sửa</div>
                                            </div>
                                            <div class="button_option_discount">
                           <span style="width: 20px;height: 20px;" class="iconify"
                                 data-icon="mdi-light:content-duplicate"></span>
                                                <div class="name"data-toggle="modal" data-target="#modalDuplicate{{$i}}">Duplicate</div>
                                            </div>
                                            <div class="button_option_discount">
                                                <span style="width: 20px;height: 20px;" class="iconify" data-icon="ion:trash-outline"></span>
                                                <div class="name"><a style="color:black;"  href="{{asset('admin/hotel/del/'.$po->id_promo)}}">Xoá</a></div>
                                            </div>
                                            <div class="triangle"></div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td>No data !!!</td>
                        </tr>
                    @endif
                </table>
            </div>
            {{$promo->links('admin.paginate')}}
        </div>
    </div>
    </div>
    {{--    Modal Edit 1--}}
    <?php $i=0;?>
    @if($promo!=null)
        @foreach($promo as $po)
            <?php $i++;?>
            <div class="modal fade" style="overflow: hidden;" id="modalEdit{{$i}}" role="dialog">
                <div style="width: 80%;margin-left:10%" class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div style="display: flex; align-items: center;justify-content: space-between;" class="modal-header">
                            <h4 class="modal-title">Cập nhật chương trình khuyến mãi {{$i}}</h4>
                            <button id="close_create" type="button" class="close1" data-dismiss="modal">
                                <span style="width: 30px; height:30px; color: #000" class="iconify" data-icon="ep:close-bold"></span>
                            </button>
                        </div>
                        <form method="post"enctype="multipart/form-data" action="{{asset('admin/hotel/edit/'.$po->id_promo)}}">
                            @csrf
                            <div style="overflow: auto; max-height: 90vh;" class="modal-body">
                                <div class="wrapper">
                                    <div class="content">
                                        <div class="box_title">
                                            <div class="title">Thông tin khuyến mãi</div>
                                        </div>
                                        <div style="margin: 0px" class="dropdown-divider"></div>
                                        <div class="content">
                                            <div class="row_input">
                                                <div class="box_input">
                                                    <div class="label">Phân hệ<span class="star">*</span></div>
                                                    <input type="text" readonly  name="subsystem" class="form-control" value="Hotel" placeholder="Hotel">
                                                </div>
                                                <div class="box_input">
                                                    <div class="label">
                                                        Loại khuyến mãi<span class="star">*</span>
                                                    </div>
                                                    <select id="type_promo" name="type_promo" onchange="handleChangeSelectTypePromotion(value)">
                                                        <option value="1" @if($po->type_promo==1) selected @endif >Promo Code</option>
                                                        <option value="2" @if($po->type_promo==2) selected @endif >Unique Code</option>
                                                        <option value="3" @if($po->type_promo==3) selected @endif >Partner Code</option>
                                                        <option value="4" @if($po->type_promo==4) selected @endif > Special Campaign</option>
                                                        <?php
                                                        Session::put('type_promo','')
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row_input">
                                                <div class="box_input">
                                                    <div class="label">
                                                        Tên chương trình khuyến mãi
                                                        <span class="star">*</span>
                                                    </div>
                                                    <input class="input" value="{{$po->title}}"  required onKeyUp="limitText(this.form.title,this.form.countdown,100);"onKeyDown="limitText(this.form.title,this.form.countdown,100);"  name="title" id="title_edit{{$i}}" placeholder="Hotel">
                                                    @if(Session::get('titleE'))                                        <br>
                                                    <span class="alert alert-danger">{{Session::get('titleE')}}</span>
                                                    <?php Session::put('titleE','')?>
                                                    <?php
                                                    Session::put('title','')
                                                    ?>
                                                    @endif
                                                    <div class="help_text_input">0/100</div>
                                                </div>
                                            </div>
                                            <div style="margin-top: 20px" class="row_input">
                                                <div class="box_input">
                                                    <div class="label">
                                                        Nội dung khuyến mãi
                                                        <span class="star">*</span>
                                                    </div>
                                                    <textarea class="input" rows="5"  required name="content">{{$po->content}}</textarea>
                                                    <?php
                                                    Session::put('content','')
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="row_input">
                                                <div class="box_input">
                                                    <div class="label">
                                                        Mã khuyến mãi
                                                        <span class="star">*</span>
                                                    </div>
                                                    <input class="input" type="text"name="code_promotion" value="{{$po->code_promotion}}" required id="code_pro"  placeholder="Hotel">
                                                    @if(Session::get('code_promotionE'))                                        <br>
                                                    <span class="alert alert-danger">{{Session::get('code_promotionE')}}</span>
                                                    <?php Session::put('code_promotionE','')?>
                                                    <?php
                                                    Session::put('code_promotion','')
                                                    ?>
                                                    @endif
                                                </div>
                                                <div class="box_input">
                                                    <div class="label">
                                                        Mã chương trình
                                                        <span class="star">*</span>
                                                    </div>
                                                    <input class="input" type="text" name="program_code_promo" id="code_program" value="{{$po->program_code_promo}}">
                                                </div>
                                                <div class="box_input">
                                                    <div class="label">Trạng thái<span class="star">*</span></div>
                                                    <select  name="status">
                                                        <option @if($po->status==0) selected @endif value="0">Inactive</option>
                                                        <option @if($po->status==1) selected @endif value="1">Active</option>
                                                        <option @if($po->status==2) selected @endif value="2">Deactive</option>
                                                        <option @if($po->status==3) selected @endif value="3">Expired</option>
                                                        <?php
                                                        Session::put('status','')
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row_input">
                                                <div class="box_input">
                                                    <div class="label">Hình ảnh Cover</div>
                                                    <div class="help_text_choose_file">
                                                        Dimension (1242x464) Max 500KB
                                                    </div>
                                                    <button type="button" id="btn_choose_file_cover_edit{{$i}}" class="btn button_choose_file">
                              <span style="width: 20px; height: 20px; color: #fff" class="iconify"
                                    data-icon="fluent:add-16-filled"></span>
                                                        <span class="name_button_choose_file">Add files</span>
                                                    </button>
                                                    <input style="display: none" name="images_cover"  id="coverFile_edit{{$i}}" title=" " value="" type="file" />
                                                </div>

                                            </div>

                                            <div class="row_input">
                                                <div class="input"><img src="{{asset('upload/'.$po->images_cover)}}" width="350px"></div>
                                            </div>
                                            <div class="row_input">
                                                <div class="box_input">
                                                    <div class="label">Hình ảnh Logo</div>
                                                    <div class="help_text_choose_file">
                                                        Dimension (260x260) Max 500KB
                                                    </div>
                                                    <button type="button"a id="btn_choose_file_logo_edit{{$i}}" class="btn button_choose_file">
                              <span style="width: 20px; height: 20px; color: #fff" class="iconify"
                                    data-icon="fluent:add-16-filled"></span>
                                                        <span class="name_button_choose_file">Add files</span>
                                                    </button>
                                                    <input style="display: none" id="logoFile_edit{{$i}}"name="images_logo"  title=" " value="" type="file" />
                                                </div>
                                            </div>

                                            <div class="row_input">
                                                <div class="input"><img src="{{asset('upload/'.$po->images_logo)}}" width="350px"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wrapper">
                                    <div class="box_title">
                                        <div class="title">Thiết lập khuyến mãi</div>
                                    </div>
                                    <div style="margin: 0px" class="dropdown-divider"></div>
                                    <div class="content">
                                        <div class="row_input">
                                            <div class="box_input">
                                                <div class="label">
                                                    Kiểu hiển thị trên ứng dụng<span class="star">*</span>
                                                </div>
                                                <div style="display: flex; margin-top: 10px">
                                                    <div style="display: flex; align-items: center" class="form-check">
                                                        <input style="width: 20px; height: 20px" class="form-check-input" type="radio"
                                                               name="display" value="0"  id="flexRadioDefault1" @if($po->display==0) checked @endif />
                                                        <label style="
                                    font-family: 'Montserrat';
                                    font-style: normal;
                                    font-weight: 400;
                                    font-size: 15px;
                                    color: #666666;
                                    margin-left: 10px;
                                    margin-top: 5px;
                                    " class="form-check-label" for="flexRadioDefault1">
                                                            Hiển thị trong danh sách
                                                        </label>
                                                    </div>
                                                    <div style="
                                 display: flex;
                                 align-items: center;
                                 margin-left: 30px;
                                 " class="form-check">
                                                        <input style="width: 20px; height: 20px" class="form-check-input" id="flexRadioDefault2" type="radio"
                                                               name="display" value="1" @if($po->display==1) checked @endif  />
                                                        <label style="
                                    font-family: 'Montserrat';
                                    font-style: normal;
                                    font-weight: 400;
                                    font-size: 15px;
                                    color: #666666;
                                    margin-left: 10px;
                                    margin-top: 5px;
                                    " class="form-check-label" for="flexRadioDefault2">
                                                            Nhập mã để tìm
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row_input">
                                            <div class="box_input">
                                                <div class="label">Đường dẫn hiển thị</div>
                                                <div class="pathname">
                                                    <div class="left">https://heyotrip.com/</div>
                                                    <input class="right" type="text" id="link_edit{{$i}}" name="link" value="{{$po->link}}" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row_input">
                                            <div class="box_input">
                                                <div class="label">Ngày bắt đầu</div>
                                                <div class="wrapper_date">
                                                    <div style="width: 100%; margin-top: 5px" class="input-group date" id="datetimepicker11_edit{{$i}}">
                                                        <input type="text" name="date_start" required="" name="date_start"  class="form-control" value="{{$po->date_start}}" />
                                                        <span class="input-group-addon">
                                 <span class="glyphicon glyphicon-calendar"></span>
                                 </span>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                @if(Session::get('Errordate_start'))                                        <br>
                                                <span class="alert alert-danger">{{Session::get('Errordate_start')}}</span>
                                                <?php Session::put('Errordate_start','');
                                                Session::put('date_start','')?>
                                                @endif
                                            </div>
                                            <div class="box_input">
                                                <div class="label">Ngày kết thúc</div>
                                                <div class="wrapper_date">
                                                    <div style="width: 100%; margin-top: 5px" class="input-group date" id="datetimepicker22_edit{{$i}}">
                                                        <input type="text" name="date_end" required="" class="form-control"value="{{$po->date_end}}" />
                                                        <span class="input-group-addon">
                                 <span class="glyphicon glyphicon-calendar"></span>
                                 </span>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                @if(Session::get('Errordate_end'))
                                                    <br>
                                                    <span class="alert alert-danger">{{Session::get('Errordate_end')}}</span>
                                                    <?php Session::put('Errordate_end','');
                                                    Session::put('date_end','')?>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="row_input">
                                            <div class="box_input">
                                                <div class="label">
                                                    Số lượng phát hành<span class="star">*</span>
                                                </div>
                                                <input type="text" placeholder="Nhập" class="input" name="number_max" id="uang"   value="{{$po->number_max}}"  />
                                                <?php
                                                Session::put('number_max','')
                                                ?>
                                            </div>
                                            <div class="box_input">
                                                <div class="label">
                                                    Giới hạn sử dụng trong ngày<span class="star">*</span>
                                                </div>
                                                <input type="text" placeholder="Nhập" class="input" name="number_limit_day" id="uang2"  required value="{{$po->number_limit_day}}" />
                                                <?php
                                                Session::put('number_limit_day','')
                                                ?>
                                            </div>
                                        </div>
                                        <div class="row_input">
                                            <div class="box_input">
                                                <div class="label">
                                                    Loại giảm giá<span class="star">*</span>
                                                </div>
                                                <div class="box_discount">
                                                    <div style="display: flex">
                                                        <div style="display: flex; align-items: center" class="form-check">
                                                            <input style="width: 20px; height: 20px" class="form-check-input" type="radio"
                                                                   name="id_type_discount" value="1" id="flexRadioDefault1_2" @if($type_discount->percentage_reduction==null) checked @endif  />
                                                            <label style="
                                       font-family: 'Montserrat';
                                       font-style: normal;
                                       font-weight: 400;
                                       font-size: 15px;
                                       color: #666666;
                                       margin-left: 10px;
                                       margin-top: 5px;
                                       " class="form-check-label" for="radioDiscount1">
                                                                Giảm giá trị cố định
                                                            </label>
                                                        </div>
                                                        <div style="
                                    display: flex;
                                    align-items: center;
                                    margin-left: 30px;
                                    " class="form-check">
                                                            <input style="width: 20px; height: 20px" class="form-check-input" type="radio"
                                                                   name="id_type_discount" @if($type_discount->percentage_reduction!=null)  checked @endif value="2" id="flexRadioDefault2_2"  />
                                                            <label style="
                                       font-family: 'Montserrat';
                                       font-style: normal;
                                       font-weight: 400;
                                       font-size: 15px;
                                       color: #666666;
                                       margin-left: 10px;
                                       margin-top: 5px;
                                       " class="form-check-label" for="radioDiscount2">
                                                                Giảm giá theo phần trăm (%)
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div id="box_discount1">
                                                        <div class="label">Mức tiền giảm</div>
                                                        <div style="display: flex; margin-top: 5px">
                                                            <input class="input" placeholder="Nhập" name="reduced_amount"  class="form-control"id="uang4" value="{{$type_discount->reduced_amount}}" />
                                                            <div style="
                                       background: #e9ecef;
                                       border: 1px solid #dee2e6;
                                       border-radius: 5px;
                                       margin-top: 5px;
                                       font-family: 'Montserrat';
                                       font-style: normal;
                                       font-weight: 400;
                                       font-size: 13px;
                                       line-height: 20px;
                                       color: #666666;
                                       padding: 5px 10px;
                                       display: flex;
                                       align-items: center;
                                       justify-content: center;
                                       ">
                                                                VND
                                                            </div>
                                                            <?php
                                                            Session::put('reduced_amount','')
                                                            ?>
                                                        </div>
                                                    </div>
                                                    <div id="box_discount2" @if($type_discount->percentage_reduction==null) class="none" @endif>
                                                        <div style="padding: 0px 10px 0px 0px" class="box_input">
                                                            <div class="label">Mức giảm theo %</div>
                                                            <input class="input" placeholder="Nhập" name="percentage_reduction" value="{{$type_discount->percentage_reduction}}" class="form-control" >
                                                            <?php
                                                            Session::put('percentage_reduction','')
                                                            ?>
                                                            <div style="padding: 0px 10px 0px 0px" class="box_input">
                                                                <div class="label">Mức tiền giảm tối đa</div>
                                                                <input class="input" placeholder="Nhập"name="reduced_amount1"   id="uang5" value="{{$type_discount->reduced_amount}}"  >
                                                                <?php
                                                                Session::put('reduced_amount1','');
                                                                Session::put('id_type_discount','')
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <?php
                                            Session::put('code_promotion','');
                                            ?>
                                        </div>
                                        <div class="row_input">
                                            <div class="box_input">
                                                <div class="label">Nhà cung cấp dịch vụ 1</div>
                                                <div class="box_help_text_supplier">
                           <span style="
                              font-family: 'Montserrat';
                              font-style: normal;
                              font-weight: 400;
                              font-size: 14px;
                              line-height: 20px;
                              color: #666666;
                              ">Các đối tác cung cấp dịch vụ trên hệ thống cho
                           Heyotrip</span>
                                                    <span style="
                              font-family: 'Montserrat';
                              font-style: normal;
                              font-weight: 500;
                              font-size: 14px;
                              line-height: 20px;
                              text-align: right;
                              color: #007bff;
                              cursor: pointer;
                              " id="del_service_edit{{$i}}" >
                           Xoá hết
                           </span>
                                                </div>
                                                <!-- <div class="box_discount_chosen"> -->
                                                <!-- <div class="item">
                                                   Travelport<span
                                                     style="
                                                       cursor: pointer;
                                                       margin-left: 5px;
                                                       width: 15px;
                                                       height: 15px;
                                                     "
                                                     class="iconify"
                                                     data-icon="eva:close-outline"
                                                   ></span>
                                                   </div> -->
                                                <select class='js-select2-multi' style="width: 100%;" required name="services[]" id="services_edit{{$i}}" multiple='multiple'>
                                                    <?php
                                                    $data_back=$po->services;

                                                    $tach=explode(',',$data_back);
                                                    $num=count($tach);
                                                    if($num==1){
                                                        $tach=$tach[0];
                                                        if ($tach=='TVP'){
                                                            Session::put('TVP','TVP');
                                                        }  if ($tach=='EZC'){
                                                            Session::put('EZC','EZC');
                                                        }if ($tach=='HPL'){
                                                            Session::put('HPL','HPL');
                                                        }if ($tach=='VJ'){
                                                            Session::put('VJ','VJ');
                                                        }
                                                    }

                                                    if($num==2){
                                                        $tach=$tach[0];
                                                        $tach1=$tach[1];
                                                        if ($tach=='TVP'||$tach1=='TVP'){
                                                            Session::put('TVP','TVP');
                                                        }  if ($tach=='EZC'||$tach1=='EZC'){
                                                            Session::put('EZC','EZC');
                                                        }if ($tach=='HPL'||$tach1=='HPL'){
                                                            Session::put('HPL','HPL');
                                                        }if ($tach=='VJ'||$tach1=='VJ'){
                                                            Session::put('VJ','VJ');
                                                        }
                                                    }
                                                    if($num==3){
                                                        $tach=$tach[0];
                                                        $tach1=$tach[1];
                                                        $tach2=$tach[2];
                                                        if ($tach=='TVP'||$tach1=='TVP'||$tach2=='TVP'){
                                                            Session::put('TVP','TVP');
                                                        }  if ($tach=='EZC'||$tach1=='EZC'||$tach2=='EZC'){
                                                            Session::put('EZC','EZC');
                                                        }if ($tach=='HPL'||$tach1=='HPL'||$tach2=='HPL'){
                                                            Session::put('HPL','HPL');
                                                        }if ($tach=='VJ'||$tach1=='VJ'||$tach2=='VJ'){
                                                            Session::put('VJ','VJ');
                                                        }
                                                    }
                                                    if($num==4){
                                                        $tach=$tach[0];
                                                        $tach1=$tach[1];
                                                        $tach2=$tach[2];
                                                        $tach3=$tach[2];
                                                        if ($tach=='TVP'||$tach1=='TVP'||$tach2=='TVP'||$tach3=='TVP'){
                                                            Session::put('TVP','TVP');
                                                        }  if ($tach=='EZC'||$tach1=='EZC'||$tach2=='EZC'||$tach3=='EZC'){
                                                            Session::put('EZC','EZC');
                                                        }if ($tach=='HPL'||$tach1=='HPL'||$tach2=='HPL'||$tach3=='HPL'){
                                                            Session::put('HPL','HPL');
                                                        }if ($tach=='VJ'||$tach1=='VJ'||$tach2=='VJ'||$tach3=='VJ'){
                                                            Session::put('VJ','VJ');
                                                        }
                                                    }
                                                    ?>
                                                    <option @if(Session::get('TVP')=='TVP') selected @endif >TVP</option>
                                                    <option @if(Session::get('EZC')=='EZC') selected @endif >EZC</option>
                                                    <option @if(Session::get('HPL')=='HPL')selected @endif >HPL</option>
                                                    <option @if(Session::get('VJ')=='VJ') selected @endif  >VJ</option>
                                                </select>
                                                <!-- </div> -->
                                            </div>
                                        </div>
                                        @if($po->payment_partner!=null)
                                            <div class="row_input" id="box_payment_partner">
                                                <div class="box_input">
                                                    <div class="label">Đối tác thanh toán {{$po->payment_partner}}</div>
                                                    <div class="box_help_text_supplier">
                           <span style="
                              font-family: 'Montserrat';
                              font-style: normal;
                              font-weight: 400;
                              font-size: 14px;
                              line-height: 20px;
                              color: #666666;
                              ">Các đối tác cung cấp dịch vụ trên hệ thống cho
                           Heyotrip</span>
                                                        <span style="
                              font-family: 'Montserrat';
                              font-style: normal;
                              font-weight: 500;
                              font-size: 14px;
                              line-height: 20px;
                              text-align: right;
                              color: #007bff;
                              cursor: pointer;
                              " id="del_payment_partner_edit{{$i}}" >
                           Xoá hết
                           </span>
                                                    </div>
                                                    <!-- <div class="box_discount_chosen"> -->
                                                    <!-- <div class="item">
                                                       Travelport<span
                                                         style="
                                                           cursor: pointer;
                                                           margin-left: 5px;
                                                           width: 15px;
                                                           height: 15px;
                                                         "
                                                         class="iconify"
                                                         data-icon="eva:close-outline"
                                                       ></span>
                                                       </div> -->
                                                    <select class='js-select2-multi'  style="width: 100%;"  name="payment_partner[]"  id="payment_partners_edit{{$i}}" multiple='multiple'>
                                                        <?php
                                                        $partner_payment=DB::table('tbl_partner_payment')->get();
                                                        $data=$po->payment_partner;
                                                        if ($data!=null){
                                                            $listSupplier = [];
                                                            $supplier = explode(',', $data);
                                                            if(is_array($supplier)){
                                                                foreach($supplier as $value){
                                                                    $listSupplier[] = $value;
                                                                }
                                                            }

                                                        }
                                                        ?>
                                                        @foreach($partner_payment as  $k => $v)
                                                            <option {{ (count($listSupplier) && in_array($v->name, $listSupplier))?'selected':'' }}>{{$v->name}}</option>
                                                        @endforeach
                                                    </select>
                                                <?php
                                                Session::put('services','')
                                                ?>
                                                <!-- </div> -->
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="wrapper">
                                        <div class="box_title">
                                            <div class="title">Điều kiện đơn hàng áp dụng</div>
                                        </div>
                                        <div style="margin: 0px" class="dropdown-divider"></div>
                                        <div class="content">
                                            <div class="row_input">
                                                <div class="box_input">
                                                    <div class="label">
                                                        Quốc gia du lịch<span class="star">*</span>
                                                    </div>
                                                    <select name="id_nation"  id="nation">
                                                        @foreach($locations as $l)
                                                            <option @if($po->id_nation==$l->id)  selected @endif value="{{$l->id}}">{{$l->country_name_vn}} - {{$l->country_name_en}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row_input">
                                                <div class="box_input">
                                                    <div style="
                              display: flex;
                              align-items: center;
                              justify-content: space-between;
                              ">
                                                        <div class="label">
                                                            Thành phố<span class="star">*</span><span style="
                                    font-family: 'Montserrat';
                                    font-style: normal;
                                    font-weight: 400;
                                    font-size: 13px;
                                    line-height: 20px;
                                    color: #666666;
                                    margin-left: 10px;
                                    ">(Đã chọn 1)</span>
                                                        </div>
                                                        <span id="id_city_edit{{$i}}"style="
                                 font-family: 'Montserrat';
                                 font-style: normal;
                                 font-weight: 500;
                                 font-size: 14px;
                                 line-height: 20px;
                                 text-align: right;D
                                 color: #007bff;
                                 cursor: pointer;
                                 ">
                              Xoá hết
                              </span>
                                                    </div>
                                                    <select class='js-select2-multi'style="width: 100%;" required name="id_city[]"  id="result_citys_edit{{$i}}" multiple='multiple'>
                                                        <?php
                                                        $data=$po->id_city;
                                                        $partner_payment2=DB::table('cities')->where('country_id',$po->id_nation)->get();


                                                        if ($data!=null){
                                                            $listSupplier = [];
                                                            $supplier = explode(', ', $data);
                                                            if(is_array($supplier)){
                                                                foreach($supplier as $value){
                                                                    $listSupplier[] = $value;
                                                                }
                                                            }

                                                        }?>
                                                        @foreach($partner_payment2 as  $k => $v)
                                                            <option {{ (count($listSupplier) && in_array($v->city_name, $listSupplier))?'selected':'' }}>{{$v->city_name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <?php
                                                Session::put('id_nation','')
                                                ?>
                                            </div>
                                            <div class="row_input">
                                                <div class="box_input">
                                                    <div class="label">Ngày nhận phòng từ</div>
                                                    <div class="wrapper_date">
                                                        <div style="width: 100%; margin-top: 5px" class="input-group date" id="datetimepicker33_edit{{$i}}">
                                                            <input type="text" name="checkinFrom"  required="" id="datetimepicker3_input" class="form-control" value="{{$po->checkinFrom}}"  />
                                                            <span class="input-group-addon">
                                 <span class="glyphicon glyphicon-calendar"></span>
                                 </span>
                                                        </div>
                                                    </div>
                                                    @if(Session::get('checkinFromE'))
                                                        <br>
                                                        <span class="alert alert-danger">{{Session::get('checkinFromE')}}</span>
                                                        <?php Session::put('checkinFromE','')?>
                                                        <?php
                                                        Session::put('checkinFrom','')
                                                        ?>
                                                    @endif
                                                </div>
                                                <div class="box_input">
                                                    <div class="label">Ngày nhận phòng đến
                                                    </div>
                                                    <div class="wrapper_date">
                                                        <div style="width: 100%; margin-top: 5px" class="input-group date" id="datetimepicker44_edit{{$i}}">
                                                            <input type="text" name="checkinTo"  required="" id="datetimepicker4_input"  class="form-control" value="{{$po->checkinTo}}" />
                                                            <span class="input-group-addon">
                                 <span class="glyphicon glyphicon-calendar"></span>
                                 </span>
                                                        </div>
                                                    </div>
                                                    @if(Session::get('ErrorcheckinTo'))
                                                        <br>
                                                        <span class="alert alert-danger">{{Session::get('ErrorcheckinTo')}}</span>
                                                        <?php Session::put('ErrorcheckinTo','')?>
                                                        <?php
                                                        Session::put('checkinTo','')
                                                        ?>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="row_input">
                                                <div class="box_input">
                                                    <div style="
                              display: flex;
                              align-items: center;
                              justify-content: space-between;
                              ">
                                                        <div class="label">
                                                            Giá trị đơn hàng tối thiểu phải đạt<span class="star">*</span>
                                                        </div>
                                                        <div style="display: flex; align-items: center" class="form-check">
                                                            <input style="width: 15px; height: 15px" class="form-check-input" type="checkbox"  name="check_box_nolimit"   id="check_box_nolimit"name="radioPrice"
                                                                   id="radioPrice" />
                                                            <label style="
                                    font-family: 'Montserrat';
                                    font-style: normal;
                                    font-weight: 400;
                                    font-size: 15px;
                                    color: #666666;
                                    margin-left: 10px;
                                    margin-top: 5px;
                                    " class="form-check-label" for="radioPrice">
                                                                Không giới hạn
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div style="display: flex; margin-top: 5px">
                                                        <input class="input" placeholder="Nhập" name="value_order_min" value="{{$po->value_order_min}}" id="min_value" />
                                                        <div style="
                                 background: #e9ecef;
                                 border: 1px solid #dee2e6;
                                 border-radius: 5px;
                                 margin-top: 5px;
                                 font-family: 'Montserrat';
                                 font-style: normal;
                                 font-weight: 400;
                                 font-size: 13px;
                                 line-height: 20px;
                                 color: #666666;
                                 padding: 5px 10px;
                                 display: flex;
                                 align-items: center;
                                 justify-content: center;
                                 ">
                                                            VND
                                                        </div>
                                                    </div>
                                                    <?php
                                                    Session::put('value_order_min','')
                                                    ?>
                                                </div>
                                                <div style="margin-top: 10px" class="box_input">
                                                    <div class="label">Số lượng khách hàng tối thiểu</div>
                                                    <input style="margin-top: 12px" placeholder="Nhập"   name="number_user_min" id="number_user_min"  value="{{$po->number_user_min}}"class="input" />
                                                </div>
                                            </div>
                                            <div class="row_input">
                                                <div class="box_input">
                                                    <div style="
                              display: flex;
                              align-items: center;
                              justify-content: space-between;
                              ">
                                                        <div class="label">
                                                            Khách sạn áp dụng<span style="
                                    font-family: 'Montserrat';
                                    font-style: normal;
                                    font-weight: 400;
                                    font-size: 13px;
                                    line-height: 20px;
                                    color: #666666;
                                    margin-left: 10px;
                                    ">(Đã chọn 1)</span>
                                                        </div>
                                                        <span id="del_hotel_edit{{$i}}" style="
                                 font-family: 'Montserrat';
                                 font-style: normal;
                                 font-weight: 500;
                                 font-size: 14px;
                                 line-height: 20px;
                                 text-align: right;
                                 color: #007bff;
                                 cursor: pointer;
                                 ">
                              Xoá hết
                              </span>
                                                    </div>
                                                    <select class='js-select2-multi'style="width: 100%;"  name="hotel_apply[]" id="hotel_applys_edit{{$i}}" multiple='multiple'>
                                                        <?php
                                                        $data_city=$po->id_city;
                                                        $data_hotel_apply=$po->hotel_apply;


                                                        if ($data!=null){
                                                            $listSupplier = [];
                                                            $partner_payment = [];
                                                            $partner_payment_city = [];
                                                            $supplier_city = explode(', ', $data_city);
                                                            $supplier_hotel_apply = explode(', ', $data_hotel_apply);
                                                            if(is_array($supplier)){

                                                                foreach($supplier_city as $value){
                                                                    $partner_payment=DB::table('cities')->where('city_name','Ho Chi Minh City')->first();

                                                                    if($partner_payment!=null){
                                                                        $partner_payment_city=DB::table('hotels')->where('city_id',$partner_payment->id)->get();
                                                                    }
                                                                    if($partner_payment!=null){
                                                                        foreach ($supplier_hotel_apply as $h){
                                                                            $partner_payment=DB::table('hotels')->where('hotel_name',$h)->first();
                                                                            if ($partner_payment!=null){
                                                                                $listSupplier[] = $partner_payment->hotel_name;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }

                                                        }
                                                        ?>
                                                        @if($partner_payment_city!=null)
                                                            @foreach($partner_payment_city as  $k => $v)
                                                                <option {{ (count($listSupplier) && in_array($v->hotel_name, $listSupplier))?'selected':'' }}>{{$v->hotel_name}}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                    <?php
                                                    Session::put('hotel_apply','')
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box_option">
                                        <button type="submit" class="btn button_save">Cập nhật</button>
                                        <button id="btn_watch" class="btn button_watch" data-toggle="modal" data-target="#modal-review">Xem
                                            trước</button>
                                    </div>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
            </div>
        @endforeach
    @endif
    <?php $i=0;?>
    @if($promo!=null)
        @foreach($promo as $po)
            <?php $i++;?>
            <div class="modal fade" style="overflow: hidden;" id="modalDuplicate{{$i}}" role="dialog">
                <div style="width: 80%;margin-left:10%" class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div style="display: flex; align-items: center;justify-content: space-between;" class="modal-header">
                            <h4 class="modal-title">Cập nhật chương trình khuyến mãi {{$i}}</h4>
                            <button id="close_create" type="button" class="close1" data-dismiss="modal">
                                <span style="width: 30px; height:30px; color: #000" class="iconify" data-icon="ep:close-bold"></span>
                            </button>
                        </div>
                        <form method="post"enctype="multipart/form-data" action="{{asset('admin/hotel/duplicate/'.$po->id_promo)}}">
                            @csrf
                            <div style="overflow: auto; max-height: 90vh;" class="modal-body">

                                <div class="wrapper">
                                    <div class="content">
                                        <div class="box_title">
                                            <div class="title">Thông tin khuyến mãi</div>
                                        </div>
                                        <div style="margin: 0px" class="dropdown-divider"></div>
                                        <div class="content">
                                            <div class="row_input">
                                                <div class="box_input">
                                                    <div class="label">Phân hệ<span class="star">*</span></div>
                                                    <input type="text" readonly  name="subsystem" class="form-control" value="Hotel" placeholder="Hotel">
                                                </div>
                                                <div class="box_input">
                                                    <div class="label">
                                                        Loại khuyến mãi<span class="star">*</span>
                                                    </div>
                                                    <select id="type_promo" name="type_promo" onchange="handleChangeSelectTypePromotion(value)">
                                                        <option value="1" @if($po->type_promo==1) selected @endif >Promo Code</option>
                                                        <option value="2" @if($po->type_promo==2) selected @endif >Unique Code</option>
                                                        <option value="3" @if($po->type_promo==3) selected @endif >Partner Code</option>
                                                        <option value="4" @if($po->type_promo==4) selected @endif > Special Campaign</option>
                                                        <?php
                                                        Session::put('type_promo','')
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row_input">
                                                <div class="box_input">
                                                    <div class="label">
                                                        Tên chương trình khuyến mãi
                                                        <span class="star">*</span>
                                                    </div>
                                                    <input class="input" value="{{$po->title}}"  required onKeyUp="limitText(this.form.title,this.form.countdown,100);"onKeyDown="limitText(this.form.title,this.form.countdown,100);"  name="title" id="title_duplicate{{$i}}" placeholder="Hotel">
                                                    @if(Session::get('titleE'))                                        <br>
                                                    <span class="alert alert-danger">{{Session::get('titleE')}}</span>
                                                    <?php Session::put('titleE','')?>
                                                    <?php
                                                    Session::put('title','')
                                                    ?>
                                                    @endif
                                                    <div class="help_text_input">0/100</div>
                                                </div>
                                            </div>
                                            <div style="margin-top: 20px" class="row_input">
                                                <div class="box_input">
                                                    <div class="label">
                                                        Nội dung khuyến mãi
                                                        <span class="star">*</span>
                                                    </div>
                                                    <textarea class="input" rows="5"  required name="content">{{$po->content}}</textarea>
                                                    <?php
                                                    Session::put('content','')
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="row_input">
                                                <div class="box_input">
                                                    <div class="label">
                                                        Mã khuyến mãi
                                                        <span class="star">*</span>
                                                    </div>
                                                    <input class="input" type="text"name="code_promotion" value="{{$po->code_promotion}}" required id="code_pro"  placeholder="Hotel">
                                                    @if(Session::get('code_promotionE'))                                        <br>
                                                    <span class="alert alert-danger">{{Session::get('code_promotionE')}}</span>
                                                    <?php Session::put('code_promotionE','')?>
                                                    <?php
                                                    Session::put('code_promotion','')
                                                    ?>
                                                    @endif
                                                </div>
                                                <div class="box_input">
                                                    <div class="label">
                                                        Mã chương trình
                                                        <span class="star">*</span>
                                                    </div>
                                                    <input class="input" type="text" name="program_code_promo" id="code_program" value="{{$po->program_code_promo}}">
                                                </div>
                                                <div class="box_input">
                                                    <div class="label">Trạng thái<span class="star">*</span></div>
                                                    <select  name="status">
                                                        <option @if($po->status==0) selected @endif value="0">Inactive</option>
                                                        <option @if($po->status==1) selected @endif value="1">Active</option>
                                                        <option @if($po->status==2) selected @endif value="2">Deactive</option>
                                                        <option @if($po->status==3) selected @endif value="3">Expired</option>
                                                        <?php
                                                        Session::put('status','')
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row_input">
                                                <div class="box_input">
                                                    <div class="label">Hình ảnh Cover</div>
                                                    <div class="help_text_choose_file">
                                                        Dimension (1242x464) Max 500KB
                                                    </div>
                                                    <button type="button" id="btn_choose_file_cover_duplicate{{$i}}" class="btn button_choose_file">
                              <span style="width: 20px; height: 20px; color: #fff" class="iconify"
                                    data-icon="fluent:add-16-filled"></span>
                                                        <span class="name_button_choose_file">Add files</span>
                                                    </button>
                                                    <input style="display: none" name="images_cover"  id="coverFile_duplicate{{$i}}" title=" " value="" type="file" />
                                                </div>
                                            </div>

                                            <div class="row_input">
                                                <div class="input"><img src="{{asset('upload/'.$po->images_cover)}}" width="350px"></div>
                                            </div>
                                            <div class="row_input">
                                                <div class="box_input">
                                                    <div class="label">Hình ảnh Logo</div>
                                                    <div class="help_text_choose_file">
                                                        Dimension (260x260) Max 500KB
                                                    </div>
                                                    <button type="button" id="btn_choose_file_logo_duplicate{{$i}}" class="btn button_choose_file">
                              <span style="width: 20px; height: 20px; color: #fff" class="iconify"
                                    data-icon="fluent:add-16-filled"></span>
                                                        <span class="name_button_choose_file">Add files</span>
                                                    </button>
                                                    <input style="display: none" id="logoFile_duplicate{{$i}}" name="images_logo"  title=" " value="" type="file" />
                                                </div>
                                            </div>
                                            <div class="row_input">
                                                <div class="input"><img src="{{asset('upload/'.$po->images_logo)}}" width="350px"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wrapper">
                                    <div class="box_title">
                                        <div class="title">Thiết lập khuyến mãi</div>
                                    </div>
                                    <div style="margin: 0px" class="dropdown-divider"></div>
                                    <div class="content">
                                        <div class="row_input">
                                            <div class="box_input">
                                                <div class="label">
                                                    Kiểu hiển thị trên ứng dụng<span class="star">*</span>
                                                </div>
                                                <div style="display: flex; margin-top: 10px">
                                                    <div style="display: flex; align-items: center" class="form-check">
                                                        <input style="width: 20px; height: 20px" class="form-check-input" type="radio"
                                                               name="display" value="0"  id="flexRadioDefault1" @if($po->display==0) checked @endif />
                                                        <label style="
                                    font-family: 'Montserrat';
                                    font-style: normal;
                                    font-weight: 400;
                                    font-size: 15px;
                                    color: #666666;
                                    margin-left: 10px;
                                    margin-top: 5px;
                                    " class="form-check-label" for="flexRadioDefault1">
                                                            Hiển thị trong danh sách
                                                        </label>
                                                    </div>
                                                    <div style="
                                 display: flex;
                                 align-items: center;
                                 margin-left: 30px;
                                 " class="form-check">
                                                        <input style="width: 20px; height: 20px" class="form-check-input" id="flexRadioDefault2" type="radio"
                                                               name="display" value="1" @if($po->display==1) checked @endif  />
                                                        <label style="
                                    font-family: 'Montserrat';
                                    font-style: normal;
                                    font-weight: 400;
                                    font-size: 15px;
                                    color: #666666;
                                    margin-left: 10px;
                                    margin-top: 5px;
                                    " class="form-check-label" for="flexRadioDefault2">
                                                            Nhập mã để tìm
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row_input">
                                            <div class="box_input">
                                                <div class="label">Đường dẫn hiển thị</div>
                                                <div class="pathname">
                                                    <div class="left">https://heyotrip.com/</div>
                                                    <input class="right" type="text" id="link_duplicate{{$i}}" name="link" value="{{$po->link}}" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row_input">
                                            <div class="box_input">
                                                <div class="label">Ngày bắt đầu</div>
                                                <div class="wrapper_date">
                                                    <div style="width: 100%; margin-top: 5px" class="input-group date" id="datetimepicker11_duplicate{{$i}}">
                                                        <input type="text" name="date_start" required="" name="date_start"  class="form-control" value="{{$po->date_start}}" />
                                                        <span class="input-group-addon">
                                 <span class="glyphicon glyphicon-calendar"></span>
                                 </span>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                @if(Session::get('Errordate_start'))                                        <br>
                                                <span class="alert alert-danger">{{Session::get('Errordate_start')}}</span>
                                                <?php Session::put('Errordate_start','');
                                                Session::put('date_start','')?>
                                                @endif
                                            </div>
                                            <div class="box_input">
                                                <div class="label">Ngày kết thúc</div>
                                                <div class="wrapper_date">
                                                    <div style="width: 100%; margin-top: 5px" class="input-group date" id="datetimepicker22_duplicate{{$i}}">
                                                        <input type="text" name="date_end" required="" class="form-control"value="{{$po->date_end}}" />
                                                        <span class="input-group-addon">
                                 <span class="glyphicon glyphicon-calendar"></span>
                                 </span>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                @if(Session::get('Errordate_end'))
                                                    <br>
                                                    <span class="alert alert-danger">{{Session::get('Errordate_end')}}</span>
                                                    <?php Session::put('Errordate_end','');
                                                    Session::put('date_end','')?>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="row_input">
                                            <div class="box_input">
                                                <div class="label">
                                                    Số lượng phát hành<span class="star">*</span>
                                                </div>
                                                <input type="text" placeholder="Nhập" class="input" name="number_max" id="uang"   value="{{$po->number_max}}"  />
                                                <?php
                                                Session::put('number_max','')
                                                ?>
                                            </div>
                                            <div class="box_input">
                                                <div class="label">
                                                    Giới hạn sử dụng trong ngày<span class="star">*</span>
                                                </div>
                                                <input type="text" placeholder="Nhập" class="input" name="number_limit_day" id="uang2"  required value="{{$po->number_limit_day}}" />
                                                <?php
                                                Session::put('number_limit_day','')
                                                ?>
                                            </div>
                                        </div>
                                        <div class="row_input">
                                            <div class="box_input">
                                                <div class="label">
                                                    Loại giảm giá<span class="star">*</span>
                                                </div>
                                                <div class="box_discount">
                                                    <div style="display: flex">
                                                        <div style="display: flex; align-items: center" class="form-check">
                                                            <input style="width: 20px; height: 20px" class="form-check-input" type="radio"
                                                                   name="id_type_discount" value="1" id="flexRadioDefault1_2" @if($type_discount->percentage_reduction==null) checked @endif  />
                                                            <label style="
                                       font-family: 'Montserrat';
                                       font-style: normal;
                                       font-weight: 400;
                                       font-size: 15px;
                                       color: #666666;
                                       margin-left: 10px;
                                       margin-top: 5px;
                                       " class="form-check-label" for="radioDiscount1">
                                                                Giảm giá trị cố định
                                                            </label>
                                                        </div>
                                                        <div style="
                                    display: flex;
                                    align-items: center;
                                    margin-left: 30px;
                                    " class="form-check">
                                                            <input style="width: 20px; height: 20px" class="form-check-input" type="radio"
                                                                   name="id_type_discount" @if($type_discount->percentage_reduction!=null)  checked @endif value="2" id="flexRadioDefault2_2"  />
                                                            <label style="
                                       font-family: 'Montserrat';
                                       font-style: normal;
                                       font-weight: 400;
                                       font-size: 15px;
                                       color: #666666;
                                       margin-left: 10px;
                                       margin-top: 5px;
                                       " class="form-check-label" for="radioDiscount2">
                                                                Giảm giá theo phần trăm (%)
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div id="box_discount1">
                                                        <div class="label">Mức tiền giảm</div>
                                                        <div style="display: flex; margin-top: 5px">
                                                            <input class="input" placeholder="Nhập" name="reduced_amount"  class="form-control"id="uang4" value="{{Session::get('reduced_amount')}}" />
                                                            <div style="
                                       background: #e9ecef;
                                       border: 1px solid #dee2e6;
                                       border-radius: 5px;
                                       margin-top: 5px;
                                       font-family: 'Montserrat';
                                       font-style: normal;
                                       font-weight: 400;
                                       font-size: 13px;
                                       line-height: 20px;
                                       color: #666666;
                                       padding: 5px 10px;
                                       display: flex;
                                       align-items: center;
                                       justify-content: center;
                                       ">
                                                                VND
                                                            </div>
                                                            <?php
                                                            Session::put('reduced_amount','')
                                                            ?>
                                                        </div>
                                                    </div>
                                                    <div id="box_discount2" @if($type_discount->percentage_reduction==null) class="none" @endif>
                                                        <div style="padding: 0px 10px 0px 0px" class="box_input">
                                                            <div class="label">Mức giảm theo %</div>
                                                            <input class="input" placeholder="Nhập" name="percentage_reduction" value="{{Session::get('percentage_reduction')}}" class="form-control" >
                                                            <?php
                                                            Session::put('percentage_reduction','')
                                                            ?>
                                                            <div style="padding: 0px 10px 0px 0px" class="box_input">
                                                                <div class="label">Mức tiền giảm tối đa</div>
                                                                <input class="input" placeholder="Nhập"name="reduced_amount1"   id="uang5" value="{{Session::get('reduced_amount1')}}"  >
                                                                <?php
                                                                Session::put('reduced_amount1','');
                                                                Session::put('id_type_discount','')
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <?php
                                            Session::put('code_promotion','');
                                            ?>
                                        </div>
                                        <div class="row_input">
                                            <div class="box_input">
                                                <div class="label">Nhà cung cấp dịch vụ 1</div>
                                                <div class="box_help_text_supplier">
                           <span style="
                              font-family: 'Montserrat';
                              font-style: normal;
                              font-weight: 400;
                              font-size: 14px;
                              line-height: 20px;
                              color: #666666;
                              ">Các đối tác cung cấp dịch vụ trên hệ thống cho
                           Heyotrip</span>
                                                    <span style="
                              font-family: 'Montserrat';
                              font-style: normal;
                              font-weight: 500;
                              font-size: 14px;
                              line-height: 20px;
                              text-align: right;
                              color: #007bff;
                              cursor: pointer;
                              " id="del_service_duplicate{{$i}}" >
                           Xoá hết
                           </span>
                                                </div>
                                                <!-- <div class="box_discount_chosen"> -->
                                                <!-- <div class="item">
                                                   Travelport<span
                                                     style="
                                                       cursor: pointer;
                                                       margin-left: 5px;
                                                       width: 15px;
                                                       height: 15px;
                                                     "
                                                     class="iconify"
                                                     data-icon="eva:close-outline"
                                                   ></span>
                                                   </div> -->
                                                <select class='js-select2-multi' style="width: 100%;" required name="services[]" id="service_duplicate{{$i}}" multiple='multiple'>
                                                    <?php
                                                    $data_back=$po->services;

                                                    $tach=explode(',',$data_back);
                                                    $num=count($tach);
                                                    if($num==1){
                                                        $tach=$tach[0];
                                                        if ($tach=='TVP'){
                                                            Session::put('TVP','TVP');
                                                        }  if ($tach=='EZC'){
                                                            Session::put('EZC','EZC');
                                                        }if ($tach=='HPL'){
                                                            Session::put('HPL','HPL');
                                                        }if ($tach=='VJ'){
                                                            Session::put('VJ','VJ');
                                                        }
                                                    }

                                                    if($num==2){
                                                        $tach=$tach[0];
                                                        $tach1=$tach[1];
                                                        if ($tach=='TVP'||$tach1=='TVP'){
                                                            Session::put('TVP','TVP');
                                                        }  if ($tach=='EZC'||$tach1=='EZC'){
                                                            Session::put('EZC','EZC');
                                                        }if ($tach=='HPL'||$tach1=='HPL'){
                                                            Session::put('HPL','HPL');
                                                        }if ($tach=='VJ'||$tach1=='VJ'){
                                                            Session::put('VJ','VJ');
                                                        }
                                                    }
                                                    if($num==3){
                                                        $tach=$tach[0];
                                                        $tach1=$tach[1];
                                                        $tach2=$tach[2];
                                                        if ($tach=='TVP'||$tach1=='TVP'||$tach2=='TVP'){
                                                            Session::put('TVP','TVP');
                                                        }  if ($tach=='EZC'||$tach1=='EZC'||$tach2=='EZC'){
                                                            Session::put('EZC','EZC');
                                                        }if ($tach=='HPL'||$tach1=='HPL'||$tach2=='HPL'){
                                                            Session::put('HPL','HPL');
                                                        }if ($tach=='VJ'||$tach1=='VJ'||$tach2=='VJ'){
                                                            Session::put('VJ','VJ');
                                                        }
                                                    }
                                                    if($num==4){
                                                        $tach=$tach[0];
                                                        $tach1=$tach[1];
                                                        $tach2=$tach[2];
                                                        $tach3=$tach[2];
                                                        if ($tach=='TVP'||$tach1=='TVP'||$tach2=='TVP'||$tach3=='TVP'){
                                                            Session::put('TVP','TVP');
                                                        }  if ($tach=='EZC'||$tach1=='EZC'||$tach2=='EZC'||$tach3=='EZC'){
                                                            Session::put('EZC','EZC');
                                                        }if ($tach=='HPL'||$tach1=='HPL'||$tach2=='HPL'||$tach3=='HPL'){
                                                            Session::put('HPL','HPL');
                                                        }if ($tach=='VJ'||$tach1=='VJ'||$tach2=='VJ'||$tach3=='VJ'){
                                                            Session::put('VJ','VJ');
                                                        }
                                                    }
                                                    ?>
                                                    <option @if(Session::get('TVP')=='TVP') selected @endif >TVP</option>
                                                    <option @if(Session::get('EZC')=='EZC') selected @endif >EZC</option>
                                                    <option @if(Session::get('HPL')=='HPL')selected @endif >HPL</option>
                                                    <option @if(Session::get('VJ')=='VJ') selected @endif  >VJ</option>
                                                </select>
                                                <!-- </div> -->
                                            </div>
                                        </div>
                                        @if($po->payment_partner!=null)
                                            <div class="row_input" id="box_payment_partner">
                                                <div class="box_input">
                                                    <div class="label">Đối tác thanh toán {{$po->payment_partner}}</div>
                                                    <div class="box_help_text_supplier">
                           <span style="
                              font-family: 'Montserrat';
                              font-style: normal;
                              font-weight: 400;
                              font-size: 14px;
                              line-height: 20px;
                              color: #666666;
                              ">Các đối tác cung cấp dịch vụ trên hệ thống cho
                           Heyotrip</span>
                                                        <span style="
                              font-family: 'Montserrat';
                              font-style: normal;
                              font-weight: 500;
                              font-size: 14px;
                              line-height: 20px;
                              text-align: right;
                              color: #007bff;
                              cursor: pointer;
                              " id="del_payment_partner_duplicate{{$i}}" >
                           Xoá hết
                           </span>
                                                    </div>
                                                    <!-- <div class="box_discount_chosen"> -->
                                                    <!-- <div class="item">
                                                       Travelport<span
                                                         style="
                                                           cursor: pointer;
                                                           margin-left: 5px;
                                                           width: 15px;
                                                           height: 15px;
                                                         "
                                                         class="iconify"
                                                         data-icon="eva:close-outline"
                                                       ></span>
                                                       </div> -->
                                                    <select class='js-select2-multi'  style="width: 100%;"  name="payment_partner[]"  id="payment_partner_duplicate{{$i}}" multiple='multiple'>
                                                        <?php
                                                        $partner_payment=DB::table('tbl_partner_payment')->get();
                                                        $data=$po->payment_partner;
                                                        if ($data!=null){
                                                            $listSupplier = [];
                                                            $supplier = explode(',', $data);
                                                            if(is_array($supplier)){
                                                                foreach($supplier as $value){
                                                                    $listSupplier[] = $value;
                                                                }
                                                            }

                                                        }
                                                        ?>
                                                        @foreach($partner_payment as  $k => $v)
                                                            <option {{ (count($listSupplier) && in_array($v->name, $listSupplier))?'selected':'' }}>{{$v->name}}</option>
                                                        @endforeach
                                                    </select>
                                                <?php
                                                Session::put('services','')
                                                ?>
                                                <!-- </div> -->
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="wrapper">
                                        <div class="box_title">
                                            <div class="title">Điều kiện đơn hàng áp dụng</div>
                                        </div>
                                        <div style="margin: 0px" class="dropdown-divider"></div>
                                        <div class="content">
                                            <div class="row_input">
                                                <div class="box_input">
                                                    <div class="label">
                                                        Quốc gia du lịch<span class="star">*</span>
                                                    </div>
                                                    <select name="id_nation"  id="nation">
                                                        @foreach($locations as $l)
                                                            <option @if($po->id_nation==$l->id)  selected @endif value="{{$l->id}}">{{$l->country_name_vn}} - {{$l->country_name_en}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row_input">
                                                <div class="box_input">
                                                    <div style="
                              display: flex;
                              align-items: center;
                              justify-content: space-between;
                              ">
                                                        <div class="label">
                                                            Thành phố<span class="star">*</span><span style="
                                    font-family: 'Montserrat';
                                    font-style: normal;
                                    font-weight: 400;
                                    font-size: 13px;
                                    line-height: 20px;
                                    color: #666666;
                                    margin-left: 10px;
                                    ">(Đã chọn 1)</span>
                                                        </div>
                                                        <span id="id_city_duplicate{{$i}}"style="
                                 font-family: 'Montserrat';
                                 font-style: normal;
                                 font-weight: 500;
                                 font-size: 14px;
                                 line-height: 20px;
                                 text-align: right;D
                                 color: #007bff;
                                 cursor: pointer;
                                 ">
                              Xoá hết
                              </span>
                                                    </div>
                                                    <select class='js-select2-multi'style="width: 100%;" required name="id_city[]"  id="result_city_duplicate{{$i}}" multiple='multiple'>
                                                        <?php
                                                        $data=$po->id_city;
                                                        $partner_payment2=DB::table('cities')->where('country_id',$po->id_nation)->get();


                                                        if ($data!=null){
                                                            $listSupplier = [];
                                                            $supplier = explode(', ', $data);
                                                            if(is_array($supplier)){
                                                                foreach($supplier as $value){
                                                                    $listSupplier[] = $value;
                                                                }
                                                            }

                                                        }?>
                                                        @foreach($partner_payment2 as  $k => $v)
                                                            <option {{ (count($listSupplier) && in_array($v->city_name, $listSupplier))?'selected':'' }}>{{$v->city_name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <?php
                                                Session::put('id_nation','')
                                                ?>
                                            </div>
                                            <div class="row_input">
                                                <div class="box_input">
                                                    <div class="label">Ngày nhận phòng từ</div>
                                                    <div class="wrapper_date">
                                                        <div style="width: 100%; margin-top: 5px" class="input-group date" id="datetimepicker33_duplicate{{$i}}">
                                                            <input type="text" name="checkinFrom"  required="" id="datetimepicker3_input" class="form-control" value="{{$po->checkinFrom}}"  />
                                                            <span class="input-group-addon">
                                 <span class="glyphicon glyphicon-calendar"></span>
                                 </span>
                                                        </div>
                                                    </div>
                                                    @if(Session::get('checkinFromE'))
                                                        <br>
                                                        <span class="alert alert-danger">{{Session::get('checkinFromE')}}</span>
                                                        <?php Session::put('checkinFromE','')?>
                                                        <?php
                                                        Session::put('checkinFrom','')
                                                        ?>
                                                    @endif
                                                </div>
                                                <div class="box_input">
                                                    <div class="label">Ngày nhận phòng đến
                                                    </div>
                                                    <div class="wrapper_date">
                                                        <div style="width: 100%; margin-top: 5px" class="input-group date" id="datetimepicker44_duplicate{{$i}}">
                                                            <input type="text" name="checkinTo"  required="" id="datetimepicker4_input"  class="form-control" value="{{$po->checkinTo}}" />
                                                            <span class="input-group-addon">
                                 <span class="glyphicon glyphicon-calendar"></span>
                                 </span>
                                                        </div>
                                                    </div>
                                                    @if(Session::get('ErrorcheckinTo'))
                                                        <br>
                                                        <span class="alert alert-danger">{{Session::get('ErrorcheckinTo')}}</span>
                                                        <?php Session::put('ErrorcheckinTo','')?>
                                                        <?php
                                                        Session::put('checkinTo','')
                                                        ?>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="row_input">
                                                <div class="box_input">
                                                    <div style="
                              display: flex;
                              align-items: center;
                              justify-content: space-between;
                              ">
                                                        <div class="label">
                                                            Giá trị đơn hàng tối thiểu phải đạt<span class="star">*</span>
                                                        </div>
                                                        <div style="display: flex; align-items: center" class="form-check">
                                                            <input style="width: 15px; height: 15px" class="form-check-input" type="checkbox"  name="check_box_nolimit"   id="check_box_nolimit"name="radioPrice"
                                                                   id="radioPrice" />
                                                            <label style="
                                    font-family: 'Montserrat';
                                    font-style: normal;
                                    font-weight: 400;
                                    font-size: 15px;
                                    color: #666666;
                                    margin-left: 10px;
                                    margin-top: 5px;
                                    " class="form-check-label" for="radioPrice">
                                                                Không giới hạn
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div style="display: flex; margin-top: 5px">
                                                        <input class="input" placeholder="Nhập" name="value_order_min" value="{{$po->value_order_min}}" id="min_value" />
                                                        <div style="
                                 background: #e9ecef;
                                 border: 1px solid #dee2e6;
                                 border-radius: 5px;
                                 margin-top: 5px;
                                 font-family: 'Montserrat';
                                 font-style: normal;
                                 font-weight: 400;
                                 font-size: 13px;
                                 line-height: 20px;
                                 color: #666666;
                                 padding: 5px 10px;
                                 display: flex;
                                 align-items: center;
                                 justify-content: center;
                                 ">
                                                            VND
                                                        </div>
                                                    </div>
                                                    <?php
                                                    Session::put('value_order_min','')
                                                    ?>
                                                </div>
                                                <div style="margin-top: 10px" class="box_input">
                                                    <div class="label">Số lượng khách hàng tối thiểu</div>
                                                    <input style="margin-top: 12px" placeholder="Nhập"   name="number_user_min" id="number_user_min"  value="{{$po->number_user_min}}"class="input" />
                                                </div>
                                            </div>
                                            <div class="row_input">
                                                <div class="box_input">
                                                    <div style="
                              display: flex;
                              align-items: center;
                              justify-content: space-between;
                              ">
                                                        <div class="label">
                                                            Khách sạn áp dụng<span style="
                                    font-family: 'Montserrat';
                                    font-style: normal;
                                    font-weight: 400;
                                    font-size: 13px;
                                    line-height: 20px;
                                    color: #666666;
                                    margin-left: 10px;
                                    ">(Đã chọn 1)</span>
                                                        </div>
                                                        <span id="del_hotel_duplicate{{$i}}" style="
                                 font-family: 'Montserrat';
                                 font-style: normal;
                                 font-weight: 500;
                                 font-size: 14px;
                                 line-height: 20px;
                                 text-align: right;
                                 color: #007bff;
                                 cursor: pointer;
                                 ">
                              Xoá hết
                              </span>
                                                    </div>
                                                    <select class='js-select2-multi'style="width: 100%;"  name="hotel_apply[]" id="hotel_apply_duplicate{{$i}}" multiple='multiple'>
                                                        <?php
                                                        $data_city=$po->id_city;
                                                        $data_hotel_apply=$po->hotel_apply;


                                                        if ($data!=null){
                                                            $listSupplier = [];
                                                            $partner_payment = [];
                                                            $partner_payment_city = [];
                                                            $supplier_city = explode(', ', $data_city);
                                                            $supplier_hotel_apply = explode(', ', $data_hotel_apply);
                                                            if(is_array($supplier)){

                                                                foreach($supplier_city as $value){
                                                                    $partner_payment=DB::table('cities')->where('city_name','Ho Chi Minh City')->first();

                                                                    if($partner_payment!=null){
                                                                        $partner_payment_city=DB::table('hotels')->where('city_id',$partner_payment->id)->get();
                                                                    }
                                                                    if($partner_payment!=null){
                                                                        foreach ($supplier_hotel_apply as $h){
                                                                            $partner_payment=DB::table('hotels')->where('hotel_name',$h)->first();
                                                                            if ($partner_payment!=null){
                                                                                $listSupplier[] = $partner_payment->hotel_name;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }

                                                        }
                                                        ?>
                                                        @if($partner_payment_city!=null)
                                                            @foreach($partner_payment_city as  $k => $v)
                                                                <option {{ (count($listSupplier) && in_array($v->hotel_name, $listSupplier))?'selected':'' }}>{{$v->hotel_name}}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                    <?php
                                                    Session::put('hotel_apply','')
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box_option">
                                        <button style="submit" class="btn button_save">Cập nhật</button>
                                        <button id="btn_watch" class="btn button_watch" data-toggle="modal" data-target="#modal-review">Xem
                                            trước</button>
                                    </div>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
            </div>
        @endforeach
    @endif
    <div class="modal fade" style="overflow: hidden;" id="modalCreate" role="dialog">
        <div style="width: 80%;margin-left:10%" class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div style="display: flex; align-items: center;justify-content: space-between;" class="modal-header">
                    <h4 class="modal-title">Tạo chương trình khuyến mãi</h4>
                    <button id="close_create" type="button" class="close1" data-dismiss="modal">
                        <span style="width: 30px; height:30px; color: #000" class="iconify" data-icon="ep:close-bold"></span>
                    </button>
                </div>
                <form method="get"enctype="multipart/form-data" action="{{asset('admin/hotel/add')}}">
                    @csrf
                    <div style="overflow: auto; max-height: 90vh;" class="modal-body">
                        <div class="wrapper">
                            <div class="content">
                                <div class="box_title">
                                    <div class="title">Thông tin khuyến mãi</div>
                                </div>
                                <div style="margin: 0px" class="dropdown-divider"></div>
                                <div class="content">
                                    <div class="row_input">
                                        <div class="box_input">
                                            <div class="label">Phân hệ<span class="star">*</span></div>
                                            <input type="text" readonly  name="subsystem" class="form-control" value="Hotel" placeholder="Hotel">
                                        </div>
                                        <div class="box_input">
                                            <div class="label">
                                                Loại khuyến mãi<span class="star">*</span>
                                            </div>
                                            <select id="type_promo" name="type_promo" onchange="handleChangeSelectTypePromotion(value)">
                                                <option value="1" @if(Session::get('type_promo')==1) selected @endif >Promo Code</option>
                                                <option value="2" @if(Session::get('type_promo')==2) selected @endif >Unique Code</option>
                                                <option value="3" @if(Session::get('type_promo')==3) selected @endif >Partner Code</option>
                                                <option value="4" @if(Session::get('type_promo')==4) selected @endif > Special Campaign</option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="row_input">
                                        <div class="box_input">
                                            <div class="label">
                                                Tên chương trình khuyến mãi
                                                <span class="star">*</span>
                                            </div>
                                            <input class="input" value="{{Session::get('title')}}"  required onKeyUp="limitText(this.form.title,this.form.countdown,100);"onKeyDown="limitText(this.form.title,this.form.countdown,100);"  name="title" id="title" placeholder="Hotel">
                                            @if(Session::get('titleE'))                                        <br>
                                            <span class="alert alert-danger">{{Session::get('titleE')}}</span>
                                            <?php Session::put('titleE','')?>

                                            @endif
                                            <div class="help_text_input">0/100</div>
                                        </div>
                                    </div>
                                    <div style="margin-top: 20px" class="row_input">
                                        <div class="box_input">
                                            <div class="label">
                                                Nội dung khuyến mãi
                                                <span class="star">*</span>
                                            </div>
                                            <textarea class="input" rows="5"  required name="content">{{Session::get('content')}}</textarea>

                                        </div>
                                    </div>
                                    <div class="row_input">
                                        <div class="box_input">
                                            <div class="label">
                                                Mã khuyến mãi
                                                <span class="star">*</span>
                                            </div>
                                            <input class="input" type="text"name="code_promotion" value="{{Session::get('code_promotion')}}" required id="code_pro"  placeholder="Hotel">
                                            @if(Session::get('code_promotionE'))                                        <br>
                                            <span class="alert alert-danger">{{Session::get('code_promotionE')}}</span>
                                            <?php ?>

                                            @endif
                                        </div>
                                        <div class="box_input">
                                            <div class="label">
                                                Mã chương trình
                                                <span class="star">*</span>
                                            </div>
                                            <input class="input" type="text" name="program_code_promo" id="code_program" value="{{date('dmY')}} H-MAKHUYENMAI">
                                        </div>
                                        <div class="box_input">
                                            <div class="label">Trạng thái<span class="star">*</span></div>
                                            <select  name="status">
                                                <option @if(Session::get('status')==0) selected @endif value="0">Inactive</option>
                                                <option @if(Session::get('status')==1) selected @endif value="1">Active</option>
                                                <option @if(Session::get('status')==2) selected @endif value="2">Deactive</option>
                                                <option @if(Session::get('status')==3) selected @endif value="3">Expired</option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="row_input">
                                        <div class="box_input">
                                            <div class="label">Hình ảnh Cover</div>
                                            <div class="help_text_choose_file">
                                                Dimension (1242x464) Max 500KB
                                            </div>
                                            <button type="button" id="btn_choose_file_cover_create" class="btn button_choose_file">
                              <span style="width: 20px; height: 20px; color: #fff" class="iconify"
                                    data-icon="fluent:add-16-filled"></span>
                                                <span class="name_button_choose_file">Add files</span>
                                            </button>
                                            <input style="display: none" name="images_cover"  id="coverFile_create" title=" " value="" type="file" />
                                        </div>
                                    </div>
                                    <div class="row_input">
                                        <div class="box_input">
                                            <div class="label">Hình ảnh Logo</div>
                                            <div class="help_text_choose_file">
                                                Dimension (260x260) Max 500KB
                                            </div>
                                            <button type="button" id="btn_choose_file_logo_create" class="btn button_choose_file">
                              <span style="width: 20px; height: 20px; color: #fff" class="iconify"
                                    data-icon="fluent:add-16-filled"></span>
                                                <span class="name_button_choose_file">Add files</span>
                                            </button>
                                            <input style="display: none" id="logoFile_create"name="images_logo" required title=" " value="" type="file" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wrapper">
                            <div class="box_title">
                                <div class="title">Thiết lập khuyến mãi</div>
                            </div>
                            <div style="margin: 0px" class="dropdown-divider"></div>
                            <div class="content">
                                <div class="row_input">
                                    <div class="box_input">
                                        <div class="label">
                                            Kiểu hiển thị trên ứng dụng<span class="star">*</span>
                                        </div>
                                        <div style="display: flex; margin-top: 10px">
                                            <div style="display: flex; align-items: center" class="form-check">
                                                <input style="width: 20px; height: 20px" class="form-check-input" type="radio"
                                                       name="display" value="0"  id="flexRadioDefault1" checked />
                                                <label style="
                                    font-family: 'Montserrat';
                                    font-style: normal;
                                    font-weight: 400;
                                    font-size: 15px;
                                    color: #666666;
                                    margin-left: 10px;
                                    margin-top: 5px;
                                    " class="form-check-label" for="flexRadioDefault1">
                                                    Hiển thị trong danh sách
                                                </label>
                                            </div>
                                            <div style="
                                 display: flex;
                                 align-items: center;
                                 margin-left: 30px;
                                 " class="form-check">
                                                <input style="width: 20px; height: 20px" class="form-check-input" id="flexRadioDefault2" type="radio"
                                                       name="display" value="1" />
                                                <label style="
                                    font-family: 'Montserrat';
                                    font-style: normal;
                                    font-weight: 400;
                                    font-size: 15px;
                                    color: #666666;
                                    margin-left: 10px;
                                    margin-top: 5px;
                                    " class="form-check-label" for="flexRadioDefault2">
                                                    Nhập mã để tìm
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row_input">
                                    <div class="box_input">
                                        <div class="label">Đường dẫn hiển thị</div>
                                        <div class="pathname">
                                            <div class="left">https://heyotrip.com/</div>
                                            <input class="right" type="text" id="link" name="link" value="{{Session::get('link')}}" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row_input">
                                    <div class="box_input">
                                        <div class="label">Ngày bắt đầu</div>
                                        <div class="wrapper_date">
                                            <div style="width: 100%; margin-top: 5px" class="input-group date" id="datetimepicker5">
                                                <input type="text" name="date_start" required="" name="date_start"  class="form-control" value="{{Session::get('date_start')}}" />
                                                <span class="input-group-addon">
                                 <span class="glyphicon glyphicon-calendar"></span>
                                 </span>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        @if(Session::get('Errordate_start'))                                        <br>
                                        <span class="alert alert-danger">{{Session::get('Errordate_start')}}</span>

                                        @endif
                                    </div>
                                    <div class="box_input">
                                        <div class="label">Ngày kết thúc</div>
                                        <div class="wrapper_date">
                                            <div style="width: 100%; margin-top: 5px" class="input-group date" id="datetimepicker6">
                                                <input type="text" name="date_end" required="" class="form-control"value="{{Session::get('date_end')}}" />
                                                <span class="input-group-addon">
                                 <span class="glyphicon glyphicon-calendar"></span>
                                 </span>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        @if(Session::get('Errordate_end'))
                                            <br>
                                            <span class="alert alert-danger">{{Session::get('Errordate_end')}}</span>

                                        @endif
                                    </div>
                                </div>
                                <div class="row_input">
                                    <div class="box_input">
                                        <div class="label">
                                            Số lượng phát hành<span class="star">*</span>
                                        </div>
                                        <input type="text" placeholder="Nhập" class="input" name="number_max" id="uang"   value="{{Session::get('number_max')}}"  />

                                    </div>
                                    <div class="box_input">
                                        <div class="label">
                                            Giới hạn sử dụng trong ngày<span class="star">*</span>
                                        </div>
                                        <input type="text" placeholder="Nhập" class="input" name="number_limit_day" id="uang2"  required value="{{Session::get('number_limit_day')}}" />
                                        <?php

                                        ?>
                                    </div>
                                </div>
                                <div class="row_input">
                                    <div class="box_input">
                                        <div class="label">
                                            Loại giảm giá<span class="star">*</span>
                                        </div>
                                        <div class="box_discount">
                                            <div style="display: flex">
                                                <div style="display: flex; align-items: center" class="form-check">
                                                    <input style="width: 20px; height: 20px" class="form-check-input" type="radio"
                                                           name="id_type_discount" value="1" id="flexRadioDefault1_2" @if(Session::get('id_type_discount')!=2) checked @endif  />
                                                    <label style="
                                       font-family: 'Montserrat';
                                       font-style: normal;
                                       font-weight: 400;
                                       font-size: 15px;
                                       color: #666666;
                                       margin-left: 10px;
                                       margin-top: 5px;
                                       " class="form-check-label" for="radioDiscount1">
                                                        Giảm giá trị cố định
                                                    </label>
                                                </div>
                                                <div style="
                                    display: flex;
                                    align-items: center;
                                    margin-left: 30px;
                                    " class="form-check">
                                                    <input style="width: 20px; height: 20px" class="form-check-input" type="radio"
                                                           name="id_type_discount" @if(Session::get('id_type_discount')==2) checked @endif value="2" id="flexRadioDefault2_2"  />
                                                    <label style="
                                       font-family: 'Montserrat';
                                       font-style: normal;
                                       font-weight: 400;
                                       font-size: 15px;
                                       color: #666666;
                                       margin-left: 10px;
                                       margin-top: 5px;
                                       " class="form-check-label" for="radioDiscount2">
                                                        Giảm giá theo phần trăm (%)
                                                    </label>
                                                </div>
                                            </div>
                                            <div id="box_discount1">
                                                <div class="label">Mức tiền giảm</div>
                                                <div style="display: flex; margin-top: 5px">
                                                    <input class="input" placeholder="Nhập" name="reduced_amount"  class="form-control"id="uang4" value="{{Session::get('reduced_amount')}}" />
                                                    <div style="
                                       background: #e9ecef;
                                       border: 1px solid #dee2e6;
                                       border-radius: 5px;
                                       margin-top: 5px;
                                       font-family: 'Montserrat';
                                       font-style: normal;
                                       font-weight: 400;
                                       font-size: 13px;
                                       line-height: 20px;
                                       color: #666666;
                                       padding: 5px 10px;
                                       display: flex;
                                       align-items: center;
                                       justify-content: center;
                                       ">
                                                        VND
                                                    </div>

                                                </div>
                                            </div>
                                            <div id="box_discount2">
                                                <div style="padding: 0px 10px 0px 0px" class="box_input">
                                                    <div class="label">Mức giảm theo %</div>
                                                    <input class="input" placeholder="Nhập" name="percentage_reduction" value="{{Session::get('percentage_reduction')}}" class="form-control" >

                                                    <div style="padding: 0px 10px 0px 0px" class="box_input">
                                                        <div class="label">Mức tiền giảm tối đa</div>
                                                        <input class="input" placeholder="Nhập"name="reduced_amount1"   id="uang5" value="{{Session::get('reduced_amount1')}}"  >

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>

                                </div>
                                <div class="row_input">
                                    <div class="box_input">
                                        <div class="label">Nhà cung cấp dịch vụ </div>
                                        <div class="box_help_text_supplier">
                              <span style="
                                 font-family: 'Montserrat';
                                 font-style: normal;
                                 font-weight: 400;
                                 font-size: 14px;
                                 line-height: 20px;
                                 color: #666666;
                                 ">Các đối tác cung cấp dịch vụ trên hệ thống cho
                              Heyotrip</span>
                                            <span style="
                                 font-family: 'Montserrat';
                                 font-style: normal;
                                 font-weight: 500;
                                 font-size: 14px;
                                 line-height: 20px;
                                 text-align: right;
                                 color: #007bff;
                                 cursor: pointer;
                                 " id="del_service" >
                              Xoá hết
                              </span>
                                        </div>
                                        <!-- <div class="box_discount_chosen"> -->
                                        <!-- <div class="item">
                                           Travelport<span
                                             style="
                                               cursor: pointer;
                                               margin-left: 5px;
                                               width: 15px;
                                               height: 15px;
                                             "
                                             class="iconify"
                                             data-icon="eva:close-outline"
                                           ></span>
                                           </div> -->
                                        <select class='js-select2-multi'  style="width: 100%;" required name="services[]" id="service" multiple='multiple'>
                                            <?php
                                            $data_back=Session::get('services');

                                            $tach=explode(', ',$data_back);?>
                                            @if (is_array(old('services')))
                                                @foreach (old('services') as $tag)
                                                    @if ($tag == 'TVP'||$tag == 'EZC'||$tag == 'HPL'||$tag == 'VJ')
                                                        <option value="{{ $tag }}" selected>{{$tag }}</option>
                                                    @else
                                                        <option value="{{ $tag}}">{{ $tag }}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                            <option  >TVP</option>
                                            <option >EZC</option>
                                            <option >HPL</option>
                                            <option  >VJ</option>
                                        </select>

                                    <!-- </div> -->
                                    </div>
                                </div>
                                <div class="row_input" id="box_payment_partner">
                                    <div class="box_input">
                                        <div class="label">Đối tác thanh toán</div>
                                        <div class="box_help_text_supplier">
                              <span style="
                                 font-family: 'Montserrat';
                                 font-style: normal;
                                 font-weight: 400;
                                 font-size: 14px;
                                 line-height: 20px;
                                 color: #666666;
                                 ">Các đối tác cung cấp dịch vụ trên hệ thống cho
                              Heyotrip</span>
                                            <span style="
                                 font-family: 'Montserrat';
                                 font-style: normal;
                                 font-weight: 500;
                                 font-size: 14px;
                                 line-height: 20px;
                                 text-align: right;
                                 color: #007bff;
                                 cursor: pointer;
                                 " id="del_payment_partner" >
                              Xoá hết
                              </span>
                                        </div>
                                        <!-- <div class="box_discount_chosen"> -->
                                        <!-- <div class="item">
                                           Travelport<span
                                             style="
                                               cursor: pointer;
                                               margin-left: 5px;
                                               width: 15px;
                                               height: 15px;
                                             "
                                             class="iconify"
                                             data-icon="eva:close-outline"
                                           ></span>
                                           </div> -->
                                        <select class='js-select2-multi'  style="width: 100%;"  name="payment_partner[]"  id="payment_partner" multiple='multiple'>
                                            <?php
                                            $partner_payment=DB::table('tbl_partner_payment')->get();

                                            $listSupplier = [];

                                            if(old('payment_partner')){
                                                foreach(old('payment_partner') as $value){
                                                    $listSupplier[]= $value;

                                                }
                                            }?>
                                            @foreach($partner_payment as  $k => $v)
                                                <option {{ (count($listSupplier) && in_array($v->name, $listSupplier))?'selected':'' }}>{{$v->name}}</option>
                                            @endforeach
                                        </select>

                                    <!-- </div> -->
                                    </div>
                                </div>
                            </div>
                            <div class="wrapper">
                                <div class="box_title">
                                    <div class="title">Điều kiện đơn hàng áp dụng</div>
                                </div>
                                <div style="margin: 0px" class="dropdown-divider"></div>
                                <div class="content">
                                    <div class="row_input">
                                        <div class="box_input">
                                            <div class="label">
                                                Quốc gia du lịch<span class="star">*</span>
                                            </div>
                                            <select name="id_nation"  id="nation">
                                                @foreach($locations as $l)
                                                    <option @if(Session::get('id_nation')==$l->id)  selected @endif value="{{$l->id}}">{{$l->country_name_vn}} - {{$l->country_name_en}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row_input">
                                        <div class="box_input">
                                            <div style="
                                 display: flex;
                                 align-items: center;
                                 justify-content: space-between;
                                 ">
                                                <div class="label">
                                                    Thành phố<span class="star">*</span><span style="
                                       font-family: 'Montserrat';
                                       font-style: normal;
                                       font-weight: 400;
                                       font-size: 13px;
                                       line-height: 20px;
                                       color: #666666;
                                       margin-left: 10px;
                                       ">(Đã chọn 1)</span>
                                                </div>
                                                <span id="id_city"style="
                                    font-family: 'Montserrat';
                                    font-style: normal;
                                    font-weight: 500;
                                    font-size: 14px;
                                    line-height: 20px;
                                    text-align: right;
                                    color: #007bff;
                                    cursor: pointer;
                                    ">
                                 Xoá hết
                                 </span>
                                            </div>
                                            <select class='js-select2-multi'style="width: 100%;" required name="id_city[]"  id="result_city" multiple='multiple'>
                                                <?php
                                                if(Session::get('id_nation')!=null){
                                                $partner_payment=DB::table('countries')->where('id',Session::get('id_nation'))->first();

                                                $partner_payment2=DB::table('cities')->where('country_id',$partner_payment->id)->get();

                                                $city=DB::table('cities')->get();
                                                $listSupplier = [];

                                                if(old('id_city')){
                                                    foreach(old('id_city') as $value){
                                                        $listSupplier[]= $value;

                                                    }
                                                }
                                                ?>
                                                @foreach($partner_payment2 as  $k => $v)
                                                    <option {{ (count($listSupplier) && in_array($v->city_name, $listSupplier))?'selected':'' }}>{{$v->city_name}}</option>
                                                @endforeach
                                                <?php }?>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="row_input">
                                        <div class="box_input">
                                            <div class="label">Ngày nhận phòng từ</div>
                                            <div class="wrapper_date">
                                                <div style="width: 100%; margin-top: 5px" class="input-group date" id="datetimepicker3">
                                                    <input type="text" name="checkinFrom"  required="" id="datetimepicker3_input" class="form-control" value="{{Session::get('checkinFrom')}}"  />
                                                    <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                                </div>
                                            </div>
                                            @if(Session::get('checkinFromE'))
                                                <br>
                                                <span class="alert alert-danger">{{Session::get('checkinFromE')}}</span>
                                                <?php Session::put('checkinFromE','')?>

                                            @endif
                                        </div>
                                        <div class="box_input">
                                            <div class="label">Ngày nhận phòng đến
                                            </div>
                                            <div class="wrapper_date">
                                                <div style="width: 100%; margin-top: 5px" class="input-group date" id="datetimepicker4">
                                                    <input type="text" name="checkinTo"  required="" id="datetimepicker4_input"  class="form-control" value="{{Session::get('checkinTo')}}" />
                                                    <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                                </div>
                                            </div>
                                            @if(Session::get('ErrorcheckinTo'))
                                                <br>
                                                <span class="alert alert-danger">{{Session::get('ErrorcheckinTo')}}</span>
                                                ?>

                                            @endif
                                        </div>
                                    </div>
                                    <div class="row_input">
                                        <div class="box_input">
                                            <div style="
                                 display: flex;
                                 align-items: center;
                                 justify-content: space-between;
                                 ">
                                                <div class="label">
                                                    Giá trị đơn hàng tối thiểu phải đạt<span class="star">*</span>
                                                </div>
                                                <div style="display: flex; align-items: center" class="form-check">
                                                    <input style="width: 15px; height: 15px" class="form-check-input" type="checkbox"  name="check_box_nolimit"   id="check_box_nolimit"name="radioPrice"
                                                           id="radioPrice" />
                                                    <label style="
                                       font-family: 'Montserrat';
                                       font-style: normal;
                                       font-weight: 400;
                                       font-size: 15px;
                                       color: #666666;
                                       margin-left: 10px;
                                       margin-top: 5px;
                                       " class="form-check-label" for="radioPrice">
                                                        Không giới hạn
                                                    </label>
                                                </div>
                                            </div>
                                            <div style="display: flex; margin-top: 5px">
                                                <input class="input" placeholder="Nhập" name="value_order_min" value="{{Session::get('value_order_min')}}" id="min_value" />
                                                <div style="
                                    background: #e9ecef;
                                    border: 1px solid #dee2e6;
                                    border-radius: 5px;
                                    margin-top: 5px;
                                    font-family: 'Montserrat';
                                    font-style: normal;
                                    font-weight: 400;
                                    font-size: 13px;
                                    line-height: 20px;
                                    color: #666666;
                                    padding: 5px 10px;
                                    display: flex;
                                    align-items: center;
                                    justify-content: center;
                                    ">
                                                    VND
                                                </div>
                                            </div>

                                        </div>
                                        <div style="margin-top: 10px" class="box_input">
                                            <div class="label">Số lượng khách hàng tối thiểu</div>
                                            <input style="margin-top: 12px" placeholder="Nhập"   name="number_user_min" id="number_user_min"  value="{{Session::get('number_user_min')}}"class="input" />
                                        </div>
                                    </div>
                                    <div class="row_input">
                                        <div class="box_input">
                                            <div style="
                                 display: flex;
                                 align-items: center;
                                 justify-content: space-between;
                                 ">
                                                <div class="label">
                                                    Khách sạn áp dụng<span style="
                                       font-family: 'Montserrat';
                                       font-style: normal;
                                       font-weight: 400;
                                       font-size: 13px;
                                       line-height: 20px;
                                       color: #666666;
                                       margin-left: 10px;
                                       ">(Đã chọn 1)</span>
                                                </div>
                                                <span id="del_hotel" style="
                                    font-family: 'Montserrat';
                                    font-style: normal;
                                    font-weight: 500;
                                    font-size: 14px;
                                    line-height: 20px;
                                    text-align: right;
                                    color: #007bff;
                                    cursor: pointer;
                                    ">
                                 Xoá hết
                                 </span>
                                            </div>
                                            <select class='js-select2-multi'style="width: 100%;"  name="hotel_apply[]" id="hotel_apply" multiple='multiple'>
                                                <?php
                                                $partner_payment=DB::table('hotels')->get();

                                                $listSupplier = [];

                                                if(old('hotel_apply')){
                                                    foreach(old('hotel_apply') as $value){
                                                        $listSupplier[]= $value;

                                                    }
                                                }?>
                                                @foreach($partner_payment as  $k => $v)
                                                    <option {{ (count($listSupplier) && in_array($v->hotel_name, $listSupplier))?'selected':'' }}>{{$v->hotel_name}}</option>
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box_option">
                                <button style="submit" class="btn button_save">Lưu</button>
                                <button id="btn_watch" class="btn button_watch" data-toggle="modal" data-target="#modal-review">Xem
                                    trước</button>
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>
    </div>
    <div style="padding: 0px; margin: 0px;" class="modal fade" id="modal-review" role="dialog">
        <div style="margin-left: calc(50% - 455px);" class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div style="padding: 0px;" class="modal-header">
                    <button type="button" class="close1" data-dismiss="modal">
                        <span style="width: 30px; height:30px; color: #000" class="iconify" data-icon="ep:close-bold"></span>
                    </button>
                    <img src="https://res.edu.vn/wp-content/uploads/2022/01/unit-60-nature-environment.jpg" />
                </div>
                <div style="padding: 0px;" class="modal-body">
                    <div class="title1">
                        Lorem ipsum dolor sit amet
                    </div>
                    <div class="title2">
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut
                        laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam,
                    </div>
                    <div style="display: flex; align-items: center; justify-content: center; flex-direction: column;">
                        <div class="box_id_discount">
                            <div class="title1">
                                Lorem ipsum dolor sit amet
                            </div>
                            <div class="box_id">
                                <div class="title">CODE1234</div>
                                <button style="background: transparent; border: 0px">
                     <span style="width: 20px; height: 20px; color: #0770cd" class="iconify"
                           data-icon="ic:outline-content-copy"></span>
                                </button>
                            </div>
                            <div class="title2">
                                Lorem ipsum dolor sit amet, consectetuer adipiscing
                            </div>
                        </div>
                        <div class="box_id_discount">
                            <div class="title1">
                                Lorem ipsum dolor sit amet
                            </div>
                            <div class="box_id">
                                <div class="title">CODE1234</div>
                                <button style="background: transparent; border: 0px">
                     <span style="width: 20px; height: 20px; color: #0770cd" class="iconify"
                           data-icon="ic:outline-content-copy"></span>
                                </button>
                            </div>
                            <div class="title2">
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt u.
                            </div>
                        </div>
                    </div>
                    <div class="box_expiry_date">
                        <div class="expiry_date">
                            Expiry date
                        </div>
                        <div class="date">
                            11/02/20222 - 31/03/2022
                        </div>
                    </div>
                    <div class="box_term_and_condition">
                        <div class="title">Terms and Condition</div>
                        <div class="accordion" id="accordion_term_and_condition">
                            <div class="card">
                                <div class="card-header" id="heading_term">
                                    <button id="btn_term" class="btn" type="button" data-toggle="collapse" data-target="#collapse_term"
                                            aria-expanded="true" aria-controls="collapse_term">
                                        Terms
                                    </button>
                                    <span id="icon_term" class="iconify" style="width: 25px; height: 25px;transform: rotate(0deg);"
                                          data-icon="akar-icons:chevron-down"></span>
                                </div>
                                <div id="collapse_term" class="collapse" aria-labelledby="heading_term"
                                     data-parent="#accordion_term_and_condition">
                                    <div class="card-body">
                                        <div class="box_info">
                                            <span class="iconify" data-icon="bi:dot"></span>
                                            <div class="info">Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy
                                                nibh.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingTwo">
                                    <button id="btn_condition" class="btn collapsed" type="button" data-toggle="collapse"
                                            data-target="#collapse_condition" aria-expanded="false" aria-controls="collapse_condition">
                                        Condition
                                    </button>
                                    <span id="icon_condition" class="iconify" style="width: 25px; height: 25px;transform: rotate(0deg);"
                                          data-icon="akar-icons:chevron-down"></span>
                                </div>
                                <div id="collapse_condition" class="collapse" aria-labelledby="collapse_condition"
                                     data-parent="#accordion_term_and_condition">
                                    <div class="card-body">
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
                                        3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt
                                        laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin
                                        coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes
                                        anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings
                                        occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard
                                        of them accusamus labore sustainable VHS.
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
                                        3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt
                                        laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin
                                        coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes
                                        anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings
                                        occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard
                                        of them accusamus labore sustainable VHS.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

    <?php $i=0;?>
    @if($promo!=null)
        @foreach($promo as $po)
            <?php $i++;
            $user=DB::table('tbl_admin')->where('id_admin',$po->ud_user)->first();
            ?>
            <div class="modal fade" id="modalDetail{{$i}}" role="dialog">
                <div style="width: 80%;margin-left:10%" class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title">Thông tin chi tiết chương trình khuyến mãi</h3>
                            <button type="button" class="close1" data-dismiss="modal">
                                <span style="width: 30px; height:30px; color: #000" class="iconify" data-icon="ep:close-bold"></span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="wrapper">
                                <div class="box_title">
                                    <div class="title">Thông tin khuyến mãi</div>
                                    <div class="box_option">
                                    </div>
                                </div>
                                <div class="dropdown-divider"> </div>
                                <div class="content">
                                    <div class="row_info">
                                        <span class="label">Người tạo</span>
                                        <span class="value">@if($po->ud_user!=0) {{$user->admin_name}} @else ADMIN @endif</span>
                                    </div>
                                    <div class="row_info">
                                        <span class="label">Phân hệ</span>
                                        <span class="value">@if($po->subsystem==0) Hotel @else Flight @endif</span>
                                    </div>
                                    <div class="row_info">
                                        <span class="label">Loại khuyến mãi:</span>
                                        <span class="value">@if($po->type_promo==1)Promo Code @elseif($po->type_promo==2) Unique Code @elseif($po->type_promo==3) Partner Code@else Special Campaign @endif</span>
                                    </div>
                                    <div class="row_info">
                                        <span class="label">Mã khuyến mãi:</span>
                                        <span class="value">{{$po->code_promotion}}</span>
                                    </div>
                                    <div class="row_info">
                                        <span class="label">Mã chương trình:</span>
                                        <span class="value">{{$po->code_promotion}}</span>
                                    </div>
                                    <div class="row_info">
                                        <span class="label">Trạng thái:</span>
                                        <span class="value">
                  <span class="inactive @if($po->status==0) inactive @elseif($po->status==1) active @elseif($po->status==2) deactivate @else expired @endif">@if($po->status==0)Inactive @elseif($po->status==1) Active @elseif($po->status==2) Deactive @else Expired @endif</span>
                </span>
                                    </div>
                                    <div class="row_info">
                                        <span class="label">Tên chương trình khuyến mãi:</span>
                                        <span class="value">{{$po->title}}</span>
                                    </div>
                                    <div class="row_info">
                                        <span class="label">Nội dung khuyến mãi:</span>
                                        <span class="value">{{$po->content}}
                </span>
                                    </div>
                                    <div class="row_info">
                                        <span class="label">Hình ảnh Cover:</span>
                                        <span class="value">
                  <img src="{{asset("upload/".$po->images_cover)}}" class="cover" />
                </span>
                                    </div>
                                    <div class="row_info">
                                        <span class="label">Hình ảnh Logo:</span>
                                        <span class="value"><img src="{{asset("upload/".$po->images_logo)}}" class="logo" /></span>
                                    </div>
                                </div>
                            </div>
                            <div class="dropdown-divider wrapper_box"> </div>
                            <div class="wrapper">
                                <div class="box_title">
                                    <div class="title">Thiết lập khuyến mãi</div>
                                    <div class="content"></div>
                                </div>
                                <div style="margin: 0px" class="dropdown-divider"></div>
                                <div class="content">
                                    <div class="row_info">
                                        <div class="label">
                                            Kiểu hiển thị trên ứng dụng:
                                        </div>

                                        @if($po->display==0)
                                            <div class="value">
                                                Hiển thị trong danh sách
                                            </div>
                                        @else
                                            <div class="value">
                                                Nhập mã để tìm
                                            </div>
                                        @endif
                                    </div>
                                    <div class="row_info">
                                        <div class="label">
                                            Đường dẫn hiển thị:
                                        </div>
                                        <div class="value">
                                            {{$po->link}}
                                        </div>
                                    </div>
                                    <div class="row_info">
                                        <div class="label">
                                            Ngày bắt đầu:
                                        </div>
                                        <div class="value">
                                            {{$po->date_start}}
                                        </div>
                                    </div>
                                    <div class="row_info">
                                        <div class="label">
                                            Ngày kết thúc:
                                        </div>
                                        <div class="value">
                                            {{$po->date_end}}
                                        </div>
                                    </div>
                                    <div class="row_info">
                                        <div class="label">
                                            Số lượng phát hành:
                                        </div>
                                        <div class="value">
                                            {{$po->number_max}}                                </div>
                                    </div>
                                    <div class="row_info">
                                        <div class="label">
                                            Giới hạn sử dụng trong ngày:
                                        </div>
                                        <div class="value">
                                            {{$po->number_limit_day}}                                </div>
                                    </div>
                                    <div class="row_info">
                                        <div class="label">
                                            Mức giảm theo %:
                                        </div>
                                        <?php
                                        $type_discount=DB::table('tbl_type_discount')->where('id_type_discount',$po->id_type_discount)->first();
                                        ?>

                                        <div class="value">
                                            @if($type_discount->percentage_reduction==null) None @else {{$type_discount->percentage_reduction}} @endif
                                        </div>
                                    </div>
                                    <div class="row_info">
                                        <div class="label">
                                            Mức tiền giảm tối đa:
                                        </div>
                                        <div class="value">
                                            {{$type_discount->reduced_amount}} VND
                                        </div>
                                    </div>
                                    <div class="row_info">
                                        <div class="label">
                                            Nhà cung cấp dịch vụ:
                                        </div>
                                        <div class="value">
                                            <div class="box_item">
                                                <div class="item">@if($po->services==null) Tất cả nhà cung cấp trên hệ thống Heyotrip @else {{$po->services}} @endif</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="dropdown-divider wrapper_box"> </div>
                            <div class="wrapper">
                                <div class="box_title">
                                    <div class="title">
                                        Điều kiện đơn hàng áp dụng
                                    </div>
                                    <div class="content"></div>
                                </div>
                                <div style="margin: 0px" class="dropdown-divider"></div>
                                <div class="content">
                                    <div class="row_info">
                                        <div class="label">
                                            Quốc gia du lịch:
                                        </div>
                                        <div class="value">

                                            <?php
                                            $nation=DB::table('countries')->where('id',$po->id_nation)->first();
                                            ?>{{$nation->country_name_vn}}                                </div>
                                    </div>
                                    <div class="row_info">
                                        <div class="label">
                                            Thành phố:
                                        </div>
                                        <div class="value">
                                            <div class="box_item">
                                                <div class="item">{{$po->id_city}}</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row_info">
                                        <div class="label">
                                            Ngày nhận phòng từ                                </div>
                                        <div class="value">
                                            {{$po->checkinFrom}}                                </div>
                                    </div>
                                    <div class="row_info">
                                        <div class="label">
                                            Ngày nhận phòng đến                                </div>
                                        <div class="value">
                                            {{$po->checkinTo}}                                </div>
                                    </div>
                                    <div class="row_info">
                                        <div class="label">
                                            Số lượng phát hành:
                                        </div>
                                        <div class="value">
                                            {{$po->number_max}}                                </div>
                                    </div>
                                    <div class="row_info">
                                        <div class="label">
                                            Giá trị đơn hàng tối thiểu phải đạt:
                                        </div>
                                        <div class="value">
                                            @if($po->value_order_min==0) Không giới hạn @else {{$po->value_order_min}} @endif VND
                                        </div>
                                    </div>
                                    <div class="row_info">
                                        <div class="label">
                                            Số lượng khách tối thiểu:
                                        </div>
                                        <div class="value">
                                            {{$po->number_user_min}}
                                        </div>
                                    </div>
                                    <div class="row_info">
                                        <div class="label">
                                            Khách sạn áp dụng:
                                        </div>
                                        <div class="value">
                                            <div class="box_item">
                                                <div class="item">{{$po->hotel_apply}}</div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            </div>
        @endforeach
    @endif

    <script>
        $(document).ready(function () {


            $("#code_pro").keyup(function () {
                var today = new Date();
                var  cod_pro=$("#code_pro").val().toUpperCase();
                var date = today.getDate()+''+(today.getMonth()+1)+""+today.getFullYear();
                var data=date+" H-"+cod_pro;
                $("#code_program").val(data);
            });
            $('#box_discount2').addClass('none');
            $('#box_payment_partner').addClass('none');
            $("#title_edit1").keyup(function () {
                var title =$(this).val();
                console.log(title);

                $.ajax({
                    url:'https://test.heyo.group/admin/hotel/customlink',
                    method:"get",
                    data:{title:title},
                    success:function (data){
                        $("#link_edit1").val(data);

                    }
                });
            }); $("#title_edit2").keyup(function () {
                var title =$(this).val();
                console.log(title);

                $.ajax({
                    url:'https://test.heyo.group/admin/hotel/customlink',
                    method:"get",
                    data:{title:title},
                    success:function (data){
                        $("#link_edit2").val(data);

                    }
                });
            }); $("#title_edit3").keyup(function () {
                var title =$(this).val();
                console.log(title);

                $.ajax({
                    url:'https://test.heyo.group/admin/hotel/customlink',
                    method:"get",
                    data:{title:title},
                    success:function (data){
                        $("#link_edit3").val(data);

                    }
                });
            }); $("#title_edit4").keyup(function () {
                var title =$(this).val();
                console.log(title);

                $.ajax({
                    url:'https://test.heyo.group/admin/hotel/customlink',
                    method:"get",
                    data:{title:title},
                    success:function (data){
                        $("#link_edit4").val(data);

                    }
                });
            }); $("#title_edit5").keyup(function () {
                var title =$(this).val();
                console.log(title);

                $.ajax({
                    url:'https://test.heyo.group/admin/hotel/customlink',
                    method:"get",
                    data:{title:title},
                    success:function (data){
                        $("#link_edit5").val(data);

                    }
                });
            });$("#title_edit6").keyup(function () {
                var title =$(this).val();
                console.log(title);

                $.ajax({
                    url:'https://test.heyo.group/admin/hotel/customlink',
                    method:"get",
                    data:{title:title},
                    success:function (data){
                        $("#link_edit6").val(data);

                    }
                });
            }); $("#title_edit7").keyup(function () {
                var title =$(this).val();
                console.log(title);

                $.ajax({
                    url:'https://test.heyo.group/admin/hotel/customlink',
                    method:"get",
                    data:{title:title},
                    success:function (data){
                        $("#link_edit7").val(data);

                    }
                });
            }); $("#title_edit8").keyup(function () {
                var title =$(this).val();
                console.log(title);

                $.ajax({
                    url:'https://test.heyo.group/admin/hotel/customlink',
                    method:"get",
                    data:{title:title},
                    success:function (data){
                        $("#link_edit8").val(data);

                    }
                });
            }); $("#title_edit9").keyup(function () {
                var title =$(this).val();
                console.log(title);

                $.ajax({
                    url:'https://test.heyo.group/admin/hotel/customlink',
                    method:"get",
                    data:{title:title},
                    success:function (data){
                        $("#link_edit9").val(data);

                    }
                });
            }); $("#title_edit10").keyup(function () {
                var title =$(this).val();
                console.log(title);

                $.ajax({
                    url:'https://test.heyo.group/admin/hotel/customlink',
                    method:"get",
                    data:{title:title},
                    success:function (data){
                        $("#link_edit10").val(data);

                    }
                });
            });$("#title_duplicate1").keyup(function () {
                var title =$(this).val();
                console.log(title);

                $.ajax({
                    url:'https://test.heyo.group/admin/hotel/customlink',
                    method:"get",
                    data:{title:title},
                    success:function (data){
                        $("#link_duplicate1").val(data);

                    }
                });
            }); $("#title_duplicate2").keyup(function () {
                var title =$(this).val();
                console.log(title);

                $.ajax({
                    url:'https://test.heyo.group/admin/hotel/customlink',
                    method:"get",
                    data:{title:title},
                    success:function (data){
                        $("#link_duplicate2").val(data);

                    }
                });
            }); $("#title_duplicate3").keyup(function () {
                var title =$(this).val();
                console.log(title);

                $.ajax({
                    url:'https://test.heyo.group/admin/hotel/customlink',
                    method:"get",
                    data:{title:title},
                    success:function (data){
                        $("#link_duplicate3").val(data);

                    }
                });
            }); $("#title_duplicate4").keyup(function () {
                var title =$(this).val();
                console.log(title);

                $.ajax({
                    url:'https://test.heyo.group/admin/hotel/customlink',
                    method:"get",
                    data:{title:title},
                    success:function (data){
                        $("#link_duplicate4").val(data);

                    }
                });
            }); $("#title_duplicate5").keyup(function () {
                var title =$(this).val();
                console.log(title);

                $.ajax({
                    url:'https://test.heyo.group/admin/hotel/customlink',
                    method:"get",
                    data:{title:title},
                    success:function (data){
                        $("#link_duplicate5").val(data);

                    }
                });
            });$("#title_duplicate6").keyup(function () {
                var title =$(this).val();
                console.log(title);

                $.ajax({
                    url:'https://test.heyo.group/admin/hotel/customlink',
                    method:"get",
                    data:{title:title},
                    success:function (data){
                        $("#link_duplicate6").val(data);

                    }
                });
            }); $("#title_duplicate7").keyup(function () {
                var title =$(this).val();
                console.log(title);

                $.ajax({
                    url:'https://test.heyo.group/admin/hotel/customlink',
                    method:"get",
                    data:{title:title},
                    success:function (data){
                        $("#link_duplicate7").val(data);

                    }
                });
            }); $("#title_duplicate8").keyup(function () {
                var title =$(this).val();
                console.log(title);

                $.ajax({
                    url:'https://test.heyo.group/admin/hotel/customlink',
                    method:"get",
                    data:{title:title},
                    success:function (data){
                        $("#link_duplicate8").val(data);

                    }
                });
            }); $("#title_duplicate9").keyup(function () {
                var title =$(this).val();
                console.log(title);

                $.ajax({
                    url:'https://test.heyo.group/admin/hotel/customlink',
                    method:"get",
                    data:{title:title},
                    success:function (data){
                        $("#link_duplicate9").val(data);

                    }
                });
            }); $("#title_duplicate10").keyup(function () {
                var title =$(this).val();
                console.log(title);

                $.ajax({
                    url:'https://test.heyo.group/admin/hotel/customlink',
                    method:"get",
                    data:{title:title},
                    success:function (data){
                        $("#link_duplicate10").val(data);

                    }
                });
            });
            $('#nation').on('change', function() {

                var nation =$(this).val();
                $.ajax({
                    url:'https://test.heyo.group/admin/hotel/loadcity',
                    method:"get",
                    data:{nation:nation},
                    success:function (data){
                        $("#result_city").html(data);

                    }
                });
            });

            $("#check_box_nolimit").change(function () {

                var checked = $(this).is(":checked");

                if (checked) {
                    $("#min_value").prop('disabled',true);
                }else{
                    $("#min_value").prop('disabled',false);
                }
            })


        })
        $('#type_promo').on('change',function () {

        })

        function handleChangeSelectTypePromotion(value) {
            var type_promo=value;
            if(type_promo==2){
                console.log(1);
                $( "#flexRadioDefault1" ).prop('checked', false);
                $( "#flexRadioDefault2" ).prop('checked', true);

            }  if(type_promo!=2){
                console.log(1);
                $("#flexRadioDefault1").prop('disabled',false);

                $( "#flexRadioDefault2" ).prop('checked', false);
                $( "#flexRadioDefault1" ).prop('checked', true);

            }
            if(type_promo==1){
                $("#uang2").prop('disabled',false);
                $("#hotel_apply").prop('required',false);
                $("#number_user_min").prop('required',false);
                $("#partner").addClass('none');



            }
            if(type_promo==2){
                $("#flexRadioDefault1").prop('disabled',true);

                $("#class_service").addClass('none');

            }


            if(type_promo==2||type_promo==3||type_promo==4){
                $("#uang2").prop('disabled',true);
                $("#class_service").removeClass('none');

            }
            if(type_promo==3){
                $( "#box_payment_partner" ).removeClass('none');

            }
            if(type_promo!=3){
                $( "#box_payment_partner" ).addClass('none');

            }
            if(type_promo==4||type_promo==2){
                $( "#partner" ).addClass('none');


            }
            if(type_promo==4){

                $( "#flexRadioDefault2_2" ).prop('checked', true);
                $( "#flexRadioDefault1_2" ).prop('checked', false);
                $( "#code_pro" ).prop('disabled', true);
                $( "#code_program" ).prop('disabled', true);
                $( "#uang" ).prop('disabled', true);
                $( "#uang2" ).prop('disabled', true);
                $( "#uang5" ).prop('disabled', true);
                $('#datetimepicker4_input').attr('readonly', true);
                $('#datetimepicker3_input').attr('readonly', true);
                $( "#min_value" ).prop('disabled', true);
                $( "#number_user_min" ).prop('disabled', true);
                $("#box_discount2").removeClass('none');
                $("#box_reaonly2").addClass('none');
                $("#box_reaonly").removeClass('none');

            }}
        if(type_promo!=4){

            $('#datetimepicker4_input').attr('readonly', false);
            $('#datetimepicker3_input').attr('readonly', false);

        }
        $('#del_payment_partner').click(function () {
            $("#payment_partner").val(null).trigger("change");

        });
        $('#del_payment_partner_edit1').click(function () {
            $("#payment_partners_edit1").val(null).trigger("change");

        });

        $('#del_payment_partner_edit2').click(function () {
            $("#payment_partners_edit2").val(null).trigger("change");

        });

        $('#del_payment_partner_edit3').click(function () {
            $("#payment_partners_edit3").val(null).trigger("change");

        });

        $('#del_payment_partner_edit4').click(function () {
            $("#payment_partners_edit4").val(null).trigger("change");

        });

        $('#del_payment_partner_edit5').click(function () {
            $("#payment_partners_edit5").val(null).trigger("change");

        });

        $('#del_payment_partner_edit6').click(function () {
            $("#payment_partners_edit6").val(null).trigger("change");

        });

        $('#del_payment_partner_edit7').click(function () {
            $("#payment_partners_edit7").val(null).trigger("change");

        });

        $('#del_payment_partner_edit8').click(function () {
            $("#payment_partners_edit8").val(null).trigger("change");

        });

        $('#del_payment_partner_edit9').click(function () {
            $("#payment_partners_edit9").val(null).trigger("change");

        });

        $('#del_payment_partner_edit10').click(function () {
            $("#payment_partners_edit10").val(null).trigger("change");

        });

        $('#del_hotel_edit1').click(function () {
            $("#hotel_applys_edit1").val(null).trigger("change");

        });
        $('#del_hotel_edit2').click(function () {
            $("#hotel_applys_edit2").val(null).trigger("change");

        });
        $('#del_hotel_edit3').click(function () {
            $("#hotel_applys_edit3").val(null).trigger("change");

        });
        $('#del_hotel_edit4').click(function () {
            $("#hotel_applys_edit4").val(null).trigger("change");

        });
        $('#del_hotel_edit5').click(function () {
            $("#hotel_applys_edit5").val(null).trigger("change");

        });
        $('#del_hotel_edit6').click(function () {
            $("#hotel_applys_edit6").val(null).trigger("change");

        });
        $('#del_hotel_edit7').click(function () {
            $("#hotel_applys_edit7").val(null).trigger("change");

        });
        $('#del_hotel_edit8').click(function () {
            $("#hotel_applys_edit8").val(null).trigger("change");

        });
        $('#del_hotel_edit10').click(function () {
            $("#hotel_applys_edit10").val(null).trigger("change");

        });
        $('#del_hotel_edit9').click(function () {
            $("#hotel_applys_edit9").val(null).trigger("change");

        });
        $('#del_hotel').click(function () {
            $("#hotel_apply").val(null).trigger("change");

        });
        $('#id_city_edit1').click(function () {
            $("#result_citys_edit1").val(null).trigger("change");

        });
        $('#id_city_edit2').click(function () {
            $("#result_citys_edit2").val(null).trigger("change");

        });
        $('#id_city_edit3').click(function () {
            $("#result_citys_edit3").val(null).trigger("change");

        });
        $('#id_city_edit4').click(function () {
            $("#result_citys_edit4").val(null).trigger("change");

        });
        $('#id_city_edit5').click(function () {
            $("#result_citys_edit5").val(null).trigger("change");

        });
        $('#id_city_edit6').click(function () {
            $("#result_citys_edit6").val(null).trigger("change");

        });
        $('#id_city_edit7').click(function () {
            $("#result_citys_edit7").val(null).trigger("change");

        });
        $('#id_city_edit8').click(function () {
            $("#result_citys_edit8").val(null).trigger("change");

        });
        $('#id_city_edit9').click(function () {
            $("#result_citys_edit9").val(null).trigger("change");

        });
        $('#id_city_edit10').click(function () {
            $("#result_citys_edit10").val(null).trigger("change");

        });
        $('#id_city').click(function () {
            $("#result_city").val(null).trigger("change");

        });
        $('#del_service').click(function () {
            $("#service").val(null).trigger("change");

        });
        $('#del_service_edit1').click(function () {
            $("#services_edit1").val(null).trigger("change");

        });
        $('#del_service_edit2').click(function () {
            $("#services_edit2").val(null).trigger("change");

        });
        $('#del_service_edit3').click(function () {
            $("#services_edit3").val(null).trigger("change");

        });
        $('#del_service_edit4').click(function () {
            $("#services_edit4").val(null).trigger("change");

        });
        $('#del_service_edit5').click(function () {
            $("#services_edit5").val(null).trigger("change");

        });
        $('#del_service_edit6').click(function () {
            $("#services_edit6").val(null).trigger("change");

        });
        $('#del_service_edit7').click(function () {
            $("#services_edit7").val(null).trigger("change");

        });
        $('#del_service_edit8').click(function () {
            $("#services_edit8").val(null).trigger("change");

        });
        $('#del_service_edit9').click(function () {
            $("#services_edit9").val(null).trigger("change");

        });

        $('#del_service_edit10').click(function () {
            $("#services_edit10").val(null).trigger("change");

        });




        $('#del_payment_partner_edit1').click(function () {
            $("#payment_partner_duplicate1").val(null).trigger("change");

        });

        $('#del_payment_partner_edit2').click(function () {
            $("#payment_partner_duplicate2").val(null).trigger("change");

        });

        $('#del_payment_partner_edit3').click(function () {
            $("#payment_partner_duplicate3").val(null).trigger("change");

        });

        $('#del_payment_partner_edit4').click(function () {
            $("#payment_partner_duplicate4").val(null).trigger("change");

        });

        $('#del_payment_partner_edit5').click(function () {
            $("#payment_partner_duplicate5").val(null).trigger("change");

        });

        $('#del_payment_partner_edit6').click(function () {
            $("#payment_partner_duplicate6").val(null).trigger("change");

        });

        $('#del_payment_partner_edit7').click(function () {
            $("#payment_partner_duplicate7").val(null).trigger("change");

        });

        $('#del_payment_partner_edit8').click(function () {
            $("#payment_partner_duplicate8").val(null).trigger("change");

        });

        $('#del_payment_partner_edit9').click(function () {
            $("#payment_partner_duplicate9").val(null).trigger("change");

        });

        $('#del_payment_partner_edit10').click(function () {
            $("#payment_partner_duplicate10").val(null).trigger("change");

        });

        $('#del_hotel_edit1').click(function () {
            $("#hotel_apply_duplicate1").val(null).trigger("change");

        });
        $('#del_hotel_edit2').click(function () {
            $("#hotel_apply_duplicate2").val(null).trigger("change");

        });
        $('#del_hotel_edit3').click(function () {
            $("#hotel_apply_duplicate3").val(null).trigger("change");

        });
        $('#del_hotel_edit4').click(function () {
            $("#hotel_apply_duplicate4").val(null).trigger("change");

        });
        $('#del_hotel_edit5').click(function () {
            $("#hotel_apply_duplicate5").val(null).trigger("change");

        });
        $('#del_hotel_edit6').click(function () {
            $("#hotel_apply_duplicate6").val(null).trigger("change");

        });
        $('#del_hotel_edit7').click(function () {
            $("#hotel_apply_duplicate7").val(null).trigger("change");

        });
        $('#del_hotel_edit8').click(function () {
            $("#hotel_apply_duplicate8").val(null).trigger("change");

        });
        $('#del_hotel_edit10').click(function () {
            $("#hotel_apply_duplicate10").val(null).trigger("change");

        });
        $('#del_hotel_edit9').click(function () {
            $("#hotel_apply_duplicate9").val(null).trigger("change");

        });
        $('#del_hotel').click(function () {
            $("#hotel_apply").val(null).trigger("change");

        });
        $('#id_city_edit1').click(function () {
            $("#result_city_duplicate1").val(null).trigger("change");

        });
        $('#id_city_edit2').click(function () {
            $("#result_city_duplicate2").val(null).trigger("change");

        });
        $('#id_city_edit3').click(function () {
            $("#result_city_duplicate3").val(null).trigger("change");

        });
        $('#id_city_edit4').click(function () {
            $("#result_city_duplicate4").val(null).trigger("change");

        });
        $('#id_city_edit5').click(function () {
            $("#result_city_duplicate5").val(null).trigger("change");

        });
        $('#id_city_edit6').click(function () {
            $("#result_city_duplicate6").val(null).trigger("change");

        });
        $('#id_city_edit7').click(function () {
            $("#result_city_duplicate7").val(null).trigger("change");

        });
        $('#id_city_edit8').click(function () {
            $("#result_city_duplicate8").val(null).trigger("change");

        });
        $('#id_city_edit9').click(function () {
            $("#result_city_duplicate9").val(null).trigger("change");

        });
        $('#id_city_edit10').click(function () {
            $("#result_city_duplicate10").val(null).trigger("change");

        });
        $('#id_city').click(function () {
            $("#result_city").val(null).trigger("change");

        });
        $('#del_service').click(function () {
            $("#service").val(null).trigger("change");

        });
        $('#del_service_edit1').click(function () {
            $("#service_duplicate1").val(null).trigger("change");

        });
        $('#del_service_edit2').click(function () {
            $("#service_duplicate2").val(null).trigger("change");

        });
        $('#del_service_edit3').click(function () {
            $("#service_duplicate3").val(null).trigger("change");

        });
        $('#del_service_edit4').click(function () {
            $("#service_duplicate4").val(null).trigger("change");

        });
        $('#del_service_edit5').click(function () {
            $("#service_duplicate5").val(null).trigger("change");

        });
        $('#del_service_edit6').click(function () {
            $("#service_duplicate6").val(null).trigger("change");

        });
        $('#del_service_edit7').click(function () {
            $("#service_duplicate7").val(null).trigger("change");

        });
        $('#del_service_edit8').click(function () {
            $("#service_duplicate8").val(null).trigger("change");

        });
        $('#del_service_edit9').click(function () {
            $("#service_duplicate9").val(null).trigger("change");

        });
        $('#del_service_edit10').click(function () {
            $("#service_duplicate10").val(null).trigger("change");

        });
        function limitText(limitField, limitCount, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            } else {
                limitCount.value = limitNum - limitField.value.length;
            }
        }
        $('#flexRadioDefault2_2').on('change', function() {
            $("#box_discount2").removeClass('none');
            $("#box_discount1").addClass('none');
        });
        $('#flexRadioDefault1_2').on('change', function() {
            $("#box_discount2").addClass('none');
            $("#box_discount1").removeClass('none');
        });
        $('#datetimepicker11_edit1').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker11_edit2').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker11_edit3').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker11_edit4').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker11_edit5').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker11_edit6').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker11_edit7').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker11_edit8').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker11_edit9').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker11_edit10').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker22_edit1').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker22_edit2').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker22_edit3').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker22_edit4').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker22_edit5').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker22_edit6').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker22_edit7').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker22_edit8').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker22_edit9').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker22_edit10').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker33_edit1').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker33_edit2').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker33_edit3').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker33_edit4').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker33_edit5').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker33_edit6').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker33_edit7').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker33_edit8').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker33_edit9').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker33_edit10').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker44_edit1').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker44_edit2').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker44_edit3').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker44_edit4').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker44_edit5').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker44_edit6').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker44_edit7').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker44_edit8').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker44_edit9').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker44_edit10').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });

        $('#datetimepicker11_duplicate1').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker11_duplicate2').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker11_duplicate3').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker11_duplicate4').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker11_duplicate5').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker11_duplicate6').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker11_duplicate7').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker11_duplicate8').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker11_duplicate9').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker11_duplicate10').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker22_duplicate1').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker22_duplicate2').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker22_duplicate3').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker22_duplicate4').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker22_duplicate5').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker22_duplicate6').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker22_duplicate7').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker22_duplicate8').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker22_duplicate9').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker22_duplicate10').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker33_duplicate1').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker33_duplicate2').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker33_duplicate3').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker33_duplicate4').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker33_duplicate5').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker33_duplicate6').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker33_duplicate7').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker33_duplicate8').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker33_duplicate9').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker33_duplicate10').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker44_duplicate1').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker44_duplicate2').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker44_duplicate3').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker44_duplicate4').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker44_duplicate5').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker44_duplicate6').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker44_duplicate7').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker44_duplicate8').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker44_duplicate9').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
        $('#datetimepicker44_duplicate10').datetimepicker({ format: 'DD-MM-YYYY LT',minDate:new Date() });
    </script>
    <script>

        $(document).ready(function () {

            $(".name_button_option").click();});
    </script>
    </body>
    </html>
@endsection
