<!DOCTYPE html>
<html lang="en">
   <head>
      <title>HF Admin</title>
      <meta charset="UTF-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <link rel="stylesheet" href="{{asset('public/admin/css/index.css')}}" />
      <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
      <script src="https://code.iconify.design/2/2.2.1/iconify.min.js"></script>
      <script src="https://unpkg.com/@popperjs/core@2"></script>
      <link rel='stylesheet'
         href='https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css'>
      <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js'></script>
      <script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js'></script>
      <script
         src='https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js'></script>
      <script></script>
      <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css"
         rel="stylesheet">
      <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
      <script src="{{asset('public/admin/js/main.js')}}"></script>
      <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
      <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
      <!-- <link rel="stylesheet" type="text/css" href="/jquery.datetimepicker.css"/ >
         <script src="/jquery.js"></script>
         <script src="/build/jquery.datetimepicker.full.min.js"></script> -->
      <style>
         .toggle.ios,
         .toggle-on.ios,
         .toggle-off.ios {
         border-radius: 20rem;
         }
         .toggle.ios .toggle-handle {
         border-radius: 20rem;
         }
      </style>
      <script>
         function test1(value) {
         
         $.ajax({
         url: "https://test.heyo.group/admin/hotel/onoffstatus",
         type: "get",
         dataType: "text",
         data: {
         id_promo: $('#id_promo1').val()
         },
         success: function (result) {
         if (result != 'no') {
            alert('Đã cập nhật thành công');
            location.reload()
         } else {
            $("#checkbox2_1").attr('checked', true);
         
            alert('Chưa tới ngày bắt đầu');
            location.reload()
         
         }
         }
         })
         }
         
           function test2(value) {
         
                $.ajax({
                    url: "https://test.heyo.group/admin/hotel/onoffstatus",
                    type: "get",
                    dataType: "text",
                    data: {
                        id_promo: $('#id_promo2').val()
                    },
                    success: function (result) {
                        if (result != 'no') {
                            alert('Đã cập nhật thành công');
                            location.reload()
                        } else {
                            $("#checkbox2_2").attr('checked', true);
         
                            alert('Chưa tới ngày bắt đầu');
                            location.reload()
         
                        }
                    }
                })
            }function test3(value) {
         
                $.ajax({
                    url: "https://test.heyo.group/admin/hotel/onoffstatus",
                    type: "get",
                    dataType: "text",
                    data: {
                        id_promo: $('#id_promo3').val()
                    },
                    success: function (result) {
                        if (result != 'no') {
                            alert('Đã cập nhật thành công');
         
                            location.reload()
                        } else {
                            $("#checkbox2_3").attr('checked', true);
         
                            alert('Chưa tới ngày bắt đầu');
                            location.reload()
         
                        }
                    }
                })
            }function test4(value) {
                $.ajax({
                    url: "https://test.heyo.group/admin/hotel/onoffstatus",
                    type: "get",
                    dataType: "text",
                    data: {
                        id_promo: $('#id_promo4').val()
                    },
                    success: function (result) {
                        if (result != 'no') {
                            alert('Đã cập nhật thành công');
                            location.reload()
                        } else {
                            $("#checkbox2_4").attr('checked', true);
         
                            alert('Chưa tới ngày bắt đầu');
                            location.reload()
         
                        }
                    }
                })
            }function test5(value) {
         
                $.ajax({
                    url: "https://test.heyo.group/admin/hotel/onoffstatus",
                    type: "get",
                    dataType: "text",
                    data: {
                        id_promo: $('#id_promo5').val()
                    },
                    success: function (result) {
                        if (result != 'no') {
                            alert('Đã cập nhật thành công');
                            location.reload()
                        } else {
                            $("#checkbox2_5").attr('checked', true);
         
                            alert('Chưa tới ngày bắt đầu');
                            location.reload()
         
                        }
                    }
                })
            }  function test6(value) {
         
                $.ajax({
                    url: "https://test.heyo.group/admin/hotel/onoffstatus",
                    type: "get",
                    dataType: "text",
                    data: {
                        id_promo: $('#id_promo6').val()
                    },
                    success: function (result) {
                        if (result != 'no') {
                            alert('Đã cập nhật thành công');
                            location.reload()
                        } else {
                            $("#checkbox2_6").attr('checked', true);
         
                            alert('Chưa tới ngày bắt đầu');
                            location.reload()
         
                        }
                    }
                })
            }function test7(value) {
         
                $.ajax({
                    url: "https://test.heyo.group/admin/hotel/onoffstatus",
                    type: "get",
                    dataType: "text",
                    data: {
                        id_promo: $('#id_promo7').val()
                    },
                    success: function (result) {
                        if (result != 'no') {
                            alert('Đã cập nhật thành công');
         
                            location.reload()
                        } else {
                            $("#checkbox2_7").attr('checked', true);
         
                            alert('Chưa tới ngày bắt đầu');
                            location.reload()
         
                        }
                    }
                })
            }function test8(value) {
         
                $.ajax({
                    url: "https://test.heyo.group/admin/hotel/onoffstatus",
                    type: "get",
                    dataType: "text",
                    data: {
                        id_promo: $('#id_promo8').val()
                    },
                    success: function (result) {
                        if (result != 'no') {
                            alert('Đã cập nhật thành công');
         
                            location.reload()
                        } else {
                            $("#checkbox2_8").attr('checked', true);
         
                            alert('Chưa tới ngày bắt đầu');
                            location.reload()
         
                        }
                    }
                })
            }function test9(value) {
         
                $.ajax({
                    url: "https://test.heyo.group/admin/hotel/onoffstatus",
                    type: "get",
                    dataType: "text",
                    data: {
                        id_promo: $('#id_promo9').val()
                    },
                    success: function (result) {
                        if (result != 'no') {
                            alert('Đã cập nhật thành công');
         
                            location.reload()
                        } else {
                            $("#checkbox2_9").attr('checked', true);
         
                            alert('Chưa tới ngày bắt đầu');
                            location.reload()
         
                        }
                    }
                })
            }function test10(value) {
         
                $.ajax({
                    url: "https://test.heyo.group/admin/hotel/onoffstatus",
                    type: "get",
                    dataType: "text",
                    data: {
                        id_promo: $('#id_promo10').val()
                    },
                    success: function (result) {
                        if (result != 'no') {
                            alert('Đã cập nhật thành công');
         
         
                            location.reload()
                        } else {
                            $("#checkbox2_10").attr('checked', true);
         
                            alert('Chưa tới ngày bắt đầu');
                            location.reload()
         
                        }
                    }
                })
            }
      </script>
   </head>
   <body>
      <div class="menu_header">
         <div class="left">
            <a href="#" class="logo">HEYO TRIP</a>
         </div>
         <div class="right">
            <span class="name_menu"> Chương trình khuyến mãi </span>
            <div class="box_admin">
               <span class="iconify" style="width: 25px; height: 25px; color: #032044"
                  data-icon="ic:baseline-notifications-none"></span>
               <div class="badge badge-danger counter">9</div>
               <div id="btn_admin" class="wrapper_admin">
                  <span style="width: 25px; height: 25px; color: #032044" class="iconify" data-icon="lucide:user"></span>
                  <span class="admin">Admin</span>
                  <span style="
                     width: 15px;
                     height: 15px;
                     color: #032044;
                     margin-left: 10px;
                     " class="iconify" data-icon="ant-design:caret-down-filled"></span>
               </div>
            </div>
         </div>
         <div id="profile" style="box-shadow: 10;" class="box_profile">
            <span style="width: 30px; height: 30px;color: #032044;" class="iconify"
               data-icon="simple-line-icons:logout"></span>
            <span class="logout"><a href="{{asset('logout')}}" style="text-decoration: none;color: black"> Đăng xuất</a></span>
         </div>
      </div>
      <div style="display: flex">
      <div class="menu_left">
         <div class="top">
            <a href="{{asset('admin/hotel/order/all/1')}}" style="background-color: #f2f9ff; text-decoration: none" class="item">
            <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
               data-icon="icon-park-outline:hotel"></span>
            <span style="color: #526a87" class="name_menu">Khách sạn</span>
            </a>
            <a href="{{asset('admin/flight/order/all/1')}}" style=" text-decoration: none" class="item">
            <span class="iconify" style="color: #526a87; width: 30px; height: 30px" data-icon="ps:plane"></span>
            <span  style="color: #526a87"class="name_menu">Vé máy bay</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
            <span class="iconify" style="color: #526a87; width: 30px; height: 30px" data-icon="clarity:car-line"></span>
            <span style="color: #526a87" class="name_menu">Thuê xe</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
            <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
               data-icon="clarity:data-cluster-line"></span>
            <span style="color: #526a87" class="name_menu">Crypto</span>
            </a>
         </div>
         <div class="top">
            <a href="#" style="text-decoration: none" class="item">
            <span class="iconify" style="color: #526a87; width: 30px; height: 30px" data-icon="ep:user-filled"></span>
            <span style="color: #526a87" class="name_menu">Tài khoản</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
            <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
               data-icon="dashicons:welcome-widgets-menus"></span>
            <span style="color: #526a87" class="name_menu">Danh mục</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
            <span class="iconify" style="color: #526a87; width: 30px; height: 30px" data-icon="gg:website"></span>
            <span style="color: #526a87" class="name_menu">Website</span>
            </a>
            <a href="{{asset('admin/website/list/quocgia')}}" style="text-decoration: none" class="item">
            <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
               data-icon="material-symbols:settings-suggest-outline-sharp"></span>
            <span style="color: #526a87" class="name_menu">Cấu hình</span>
            </a>
         </div>
      </div>
      <div class="menu_right">
         <a href="#" class="item">
         <span style="width: 25px; height: 25px; color: #032044" class="iconify"
            data-icon="majesticons:analytics"></span>
         <span class="name_menu">Dashboard</span>
         </a>
         <a  href="{{asset('admin/hotel/searchHotel')}}"class="item">
         <span style="width: 25px; height: 25px; color: #032044" class="iconify" data-icon="mdi:bed"></span>
         <span class="name_menu">Tìm - Đặt phòng</span>
         </a>
         <a href="{{asset('admin/hotel/order/all/1')}}" class="item">
         <span style="width: 25px; height: 25px; color: #032044" class="iconify"
            data-icon="fa-solid:calendar-day"></span>
         <span class="name_menu">Quản lý đơn đặt phòng</span>
         </a>
         <div href="#" class="item">
            <span style="width: 25px; height: 25px; color: #032044" class="iconify"
               data-icon="fa-solid:money-check-alt"></span>
            <span class="name_menu">Tuỳ chỉnh giá phòng</span>
         </div>
         <div class="item">
            <span style="width: 25px; height: 25px; color: #329223" class="iconify"
               data-icon="icomoon-free:price-tags"></span>
            <span  class="name_menu"><a style="color: #329223; font-weight: 800;tex"href='{{asset("admin/hotel/list")}}'>Chương trình khuyến mãi</a></span>
         </div>
         <div class="item">
            <span style="width: 25px; height: 25px; color: #032044" class="iconify" data-icon="eva:percent-fill"></span>
            <span class="name_menu">Quản lý hoa hồng</span>
         </div>
         <div class="item">
            <span style="width: 25px; height: 25px; color: #032044" class="iconify"
               data-icon="fa6-solid:hand-holding-dollar"></span>
            <span class="name_menu">Huỷ - Hoàn tiền</span>
         </div>
         <div class="item">
            <span style="width: 25px; height: 25px; color: #032044" class="iconify"
               data-icon="fluent:news-20-filled"></span>
            <span class="name_menu">Bài viết Marketing</span>
         </div>
      </div>

      <?Php
      
      Session::put('program_code_promo','');
      Session::put('code_promotion','');
      Session::put('type_promo','');
      Session::put('status','');
      Session::put('nation','');
      Session::put('date_start_from','');
      Session::put('date_start_to','');
      Session::put('date_end_from','');
      Session::put('date_end_to','');
      ?>      <div class="box_content">
         <div class="wrapper">
            <div class="box_title">
               <div class="title">Tra cứu khuyến mãi</div>
            </div>
            <form method="post">
            @csrf
            <div style="margin: 0px" class="dropdown-divider"></div>
            <div class="content">
               <div class="row_input">
                  <div class="box_input">
                     <div class="label">Mã chương trình</div>
                     <input placeholder="Nhập mã chương trình"name="program_code_promo"value="{{Session::get('program_code_promo')}}"  class="input" />
                  </div>
                  <div class="box_input">
                     <div class="label">Mã khuyến mãi</div>
                     <input placeholder="Nhập mã Khuyến mãi"name="code_promotion" value="{{Session::get('code_promotion')}}"  class="input" />
                  </div>
               </div>
               <div class="row_input">
                  <div class="box_input">
                     <div class="label">Loại khuyến mãi</div>
                     <select id="type_promo" name="type_promo">
                        <option value>chọn</option>
                        <option value="1" @if(Session::get('type_promo')==1) selected @endif >Promo Code</option>
                        <option value="2" @if(Session::get('type_promo')==2) selected @endif >Unique Code</option>
                        <option value="3" @if(Session::get('type_promo')==3) selected @endif >Partner Code</option>
                        <option value="4" @if(Session::get('type_promo')==4) selected @endif > Special Campaign</option>
                     </select>
                  </div>
                  <div class="box_input">
                     <div class="label">Trạng thái</div>
                     <select  name="status">
                        <option value>chọn</option>
                        <option @if(Session::get('status')=='Thanh') selected @endif value="0">Inactive</option>
                        <option @if(Session::get('status')==1) selected @endif value="1">Active</option>
                        <option @if(Session::get('status')==2) selected @endif value="2">Deactive</option>
                        <option @if(Session::get('status')==3) selected @endif value="3">Expired</option>
                     </select>
                  </div>
                  <?php
                     if(Session::put('status','')){
                         Session::put('status','Thanh');
                     }
                     ?>
                  <div class="box_input">
                     <div class="label">Quốc gia</div>
                     <select  name="nation"  class='form-control'>
                        <option value>chọn</option>
                        @foreach($locations as $l)
                        <option @if(Session::get('nation')==$l->id)   selected @endif value="{{$l->id}}">{{$l->country_name_vn}} - {{$l->country_name_en}}</option>
                        @endforeach
                     </select>
                  </div>
               </div>
               <div class="row_input">
                    <div class="box_input">
                     <div class="label">Ngày bắt đầu</div>
                     <input type='text' class="form-control " value="@if(Session::get('date_start_from')){{Session::get('date_start_from')}}-{{Session::get('date_start_to')}}@endif" style="width:100%"id="start_from">
                  </div>
                  <div id="err_start"></div>
                  
                  <div class="box_input">
                     <div class="label">Ngày kết thúc</div>
                     <input type='text' class="form-control " value="@if(Session::get('date_end_from')){{Session::get('date_end_from')}}-{{Session::get('date_end_to')}}@endif" style="width:100%"id="end_from">
                  </div>
                  <div id="err_end"></div>
               </div>
               <div class="row">
                  <div class="wrapper_date" id='box_start_from' style='margin-left:25px;background:#fff; position: absolute;  width: 100%;'>
                     <div style="float:left;width: 15%;margin-top: 5px;
                        margin-right: 15px;"  class="input-group date" id="datetimepicker1_333_1">
                        <input type="text" name="date_start_from"  value="" id="date_start_from"  class="form-control" value="">
                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                     </div>
                     <div style="float:left;width: 15%;margin-top: 5px;
                        margin-right: 15px;"  class="input-group date" id="datetimepicker1_333_2">
                        <input type="text" name="date_start_to"  value=""  id="date_start_to"   class="form-control" value="">
                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                     </div>
                     <div style="float:left;width: 15%;margin-top: 5px;"  class="input-group date" id="datetimepicker1_333_2">
                        <button type="button" id='btn_xn_start' value=""    class="btn btn-success" value=""> Xác nhận</button>
                     </div>
                  </div>
               </div>
               
               <div class="row">
                  <div class="wrapper_date" id='box_end_from' style='margin-left:1131px;background:#fff; position: absolute; width: 100%;'>
                     <div style="float:left;width: 15%;margin-top: 5px;
                        margin-right: 15px;"  class="input-group date" id="datetimepicker2_333_1">
                        <input type="text" name="date_end_from"  value="" id="date_end_from"  class="form-control" value="">
                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                     </div>
                     <div style="float:left;width: 15%;margin-top: 5px;
                        margin-right: 15px;"  class="input-group date" id="datetimepicker2_333_2">
                        <input type="text" name="date_end_to"  value=""  id="date_end_to"   class="form-control" value="">
                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                     </div>
                     <div style="float:left;width: 15%;margin-top: 5px;"  class="input-group date" id="">
                        <button type="button" id='btn_xn_end' value=""    class="btn btn-success" value=""> Xác nhận</button>
                     </div>
                  </div>
               </div>
               <div class="box_option" style="justify-content:end">
                  <button type="submit" style="text-decoration: none;border: 0px;margin-right:10px" class="button_option">
                  <span style="width: 20px; height: 20px; color: #fff;" class="iconify" data-icon="carbon:search"></span>
                  <span class="name_button_option">Tìm kiếm</span>
                  </button>
                  <a class="btn button_clear" href="{{asset('admin/hotel/list')}}"type="button" id="delete_search">
                  Xoá bộ lọc
                  </a>
               </div>
            </div>
            <div class="wrapper">
               <div class="box_title">
                  <div class="title">{{Session::get('soluong')}} khuyến mãi</div>
                  <a href="{{asset('admin/hotel/add')}}"style="text-decoration: none;" class="button_option">
                  <span style="width: 20px; height: 20px; color: #fff;" class="iconify"
                     data-icon="fluent:add-16-filled"></span>
                  <span class="name_button_option">Thêm mới</span>
                  </a>
               </div>
               <div class="content">
                  <table>
                     <tr>
                        <th class="small"> </th>
                        <th class="medium">Mã chương trình</th>
                        <th class="medium">Mã khuyến mãi</th>
                        <th class="big">Chương trình khuyến mãi</th>
                        <th class="medium">Loại khuyến mãi</th>
                        <th class="medium">Ngày bắt đầu</th>
                        <th class="medium">Ngày kết thúc</th>
                        <th class="medium">Số lượng</th>
                        <th class="medium">Người tạo</th>
                        <th class="small"> </th>
                     </tr>
                     <?php $i=0;?>
                     @if($promo!=null)
                     @foreach($promo as $po)
                     <?php
                        $i++;
                        $program_code_promo=$po->program_code_promo;
                        $tach=explode(' ',$program_code_promo);
                        
                        $user=DB::table('tbl_admin')->where('id_admin',$po->ud_user)->first();
                        $id_nation=DB::table('countries')->where('id',$po->id_nation)->first();
                        if($po->subsystem==0){
                        ?>
                     <tr>
                        <td style="text-align: center;">
                           @if($po->status==1)
                           <input id="toggle-state-switch checkbox1" type="checkbox" data="1" checked value="true" onchange="test{{$i}}(value)"
                              data-toggle="toggle" data-size="sm" data-style="ios">
                           @elseif($po->status==3)
                           <input id="toggle-state-switch checkbox2_{{$i}}" type="checkbox" data="0" value="true" disabled onchange="test{{$i}}(value)"
                              data-toggle="toggle" data-size="sm" data-style="ios">
                           @else
                           <input id="toggle-state-switch checkbox2_{{$i}}" type="checkbox" data="0"  value="true" onchange="test{{$i}}(value)"
                              data-toggle="toggle" data-size="sm" data-style="ios">
                           @endif
                           <input type="hidden" id="id_promo{{$i}}" value="{{$po->id_promo}}">
                        </td>

                        <td class="ma_chuong_trinh">
                            @if(count($tach)>1)
                           <div>{{$tach[0]}}</div>
                           <div>{{$tach[1]}}  </div>
                           @endif
                        </td>
                        <td class="ma_khuyen_mai">
                           <div class="ten">{{$po->code_promotion}}</div>
                           <div class=" @if($po->status==0)inactive @elseif($po->status==1)active  @elseif($po->status==2)deactive @else expired @endif " >
                              @if($po->status==0) Inactive @elseif($po->status==1)Active @elseif($po->status==2)Deactive @else Expired @endif
                           </div>
                        </td>
                        <td class="chuong_trinh_khuyen_mai">
                           {{$po->title }}                        
                        </td>
                        <td class="loai_khuyen_mai">
                           <div class="ten"> @if($po->type_promo ==1)Promo Code @elseif($po->type_promo ==2) Unique Code @elseif($po->type_promo ==3)Partner Code @else Special Campaign @endif</div>
                           <div class="country">{{$id_nation->country_name_vn}}</div>
                        </td>
                        <td class="loai_khuyen_mai">
                                    <span class="ten"> <?php $tach=explode(' ',$po->date_start);
                                    $day=explode('-',$tach[0]);
                                    $custom_day=$day[2].'-'.$day[1].'-'.$day[0].' '.$tach[1];
                                    
                                    
                                    ?>{{$custom_day}}</span>
                        </td>
                        <td class="loai_khuyen_mai">
                           <span class="ten">  <?php $tach=explode(' ',$po->date_end);
                                    $day=explode('-',$tach[0]);
                                    $custom_day=$day[2].'-'.$day[1].'-'.$day[0].' '.$tach[1];
                                    
                                    
                                    ?>{{$custom_day}}</span>
                        </td>
                        <td class="loai_khuyen_mai">
                           <span class="ten">{{number_format($po->number_max,0,'.',',')}}</span>
                        </td>
                        <td class="loai_khuyen_mai">
                           <span class="ten">                            @if($po->ud_user!=1) {{$user->admin_name}} @else Admin @endif
                           </span>
                        </td>
                        <td>
                           <div class="wrapper_icon">
                              <div id="dots{{$i}}">
                                 <span style="color: #C1C1C1; width: 25px; height: 25px;" class="iconify"
                                    data-icon="bi:three-dots"></span>
                              </div>
                              <div id="box_option_discount{{$i}}">
                                 <a href='{{asset('admin/hotel/edit/'.$po->id_promo)}}'>
                                 <div class="button_option_discount">
                                    <span style="width: 20px;height: 20px;" class="iconify" data-icon="clarity:note-edit-line"></span>
                                    <div class="name"> Chỉnh sửa</div>
                                 </div>
                                 </a>
                                 <a href='{{asset('admin/hotel/view/'.$po->id_promo)}}'> 
                                 <div class="button_option_discount">
                                    <span style="width: 20px;height: 20px;" class="iconify" data-icon="clarity:note-edit-line"></span>
                                    <div class="name"> Xem</div>
                                 </div>
                                 </a>
                                 <a href='{{asset('admin/hotel/duplicate/'.$po->id_promo)}}'> 
                                 <div class="button_option_discount">
                                    <span style="width: 20px;height: 20px;" class="iconify"
                                       data-icon="mdi-light:content-duplicate"></span>
                                    <div class="name"> Duplicate</div>
                                 </div>
                                 </a>
                                 <a href='{{asset('admin/hotel/del/'.$po->id_promo)}}'>
                                 <div class="button_option_discount">
                                    <span style="width: 20px;height: 20px;" class="iconify" data-icon="ion:trash-outline"></span>
                                    <div class="name"> Xoá</div>
                                 </div>
                                 </a>
                                 <div class="triangle"></div>
                              </div>
                           </div>
                        </td>
                     </tr>
                     <?php }?>
                     @endforeach
                     @else
                     <tr>
                        <td>No data !!!</td>
                     </tr>
                     @endif
                  </table>
               </div>
               {{$promo->links('admin.paginate')}}
            </div>
         </div>
      </div>
   </body>
</html>
<style>
   .pager{
   text-align: right!important;
   }
   a{
   color:#888888;
   }
   a:hover{
   text-decoration: none;
   color:#888888;
   }
</style>
<script>
      $('document').ready(function () {
          $("#delete_search").click(function () {
              $('.form-control').val('');
              $('.input').val('');
          });
          $('.duplicate').click(function(){
              alert('This function update last')
          });
          $('.edit').click(function(){
              alert('This function update last')
          });
          $("#datetimepicker1_333_1").datetimepicker({
          format: "DD-MM-YYYY LT",
      });
          $("#datetimepicker1_333_2").datetimepicker({
          format: "DD-MM-YYYY LT",
      });
      $("#datetimepicker2_333_1").datetimepicker({
          format: "DD-MM-YYYY LT",
      });
      $("#datetimepicker2_333_2").datetimepicker({
          format: "DD-MM-YYYY LT",
      });
      $("#box_start_from").css('display','none');
      $("#start_from").click(function(){
       $("#box_start_from").css('display','block')
       $("#box_end_from").css('display','none')

       $(".box_option").css('margin-top','55px');
      });
      function parseDate(str) {
var mdy = str.split('-');
return new Date(mdy[2], mdy[1], mdy[0]);
}
      $('#btn_xn_start').click(function(){
        $(".box_option").css('margin-top','0px');
       let date_start_from=$("#date_start_from").val();
       let date_start_to=$("#date_start_to").val();
       let tach_start_from=date_start_from.split(' ');
       let tach2_start_from=tach_start_from[0].split('-');
       let ngay_start_from,thang_start_from,nam_start_from;
       
       ngay_start_from=tach2_start_from[0];
       thang_start_from=start_from=tach2_start_from[1];
       nam_start_from=tach2_start_from[2];
   
       
       let tach_start_to=date_start_to.split(' ');
       let tach2_start_to=tach_start_to[0].split('-');
       let ngay_start_to,thang_start_to,nam_start_to
       
       ngay_start_to=tach2_start_to[0];
       thang_start_to=start_to=tach2_start_to[1];
       nam_start_to=tach2_start_to[2];
   
       if(parseDate(date_start_from).getTime()<parseDate(date_start_to).getTime()){
           alert("Lỗi chọn ngày tháng năm. Vui lòng chọn đúng ngày");
           $("#box_start_from").css('display','none');
   return false;
       }else{
   
       
           
       let data=date_start_from +' - '+date_start_to;
   
       $('#start_from').val(data);
       $("#box_start_from").css('display','none');
       
       }
      })
      
      
      $("#box_end_from").css('display','none');
      $("#end_from").click(function(){
       $("#box_end_from").css('display','block')
       $("#box_start_from").css('display','none')
       $(".box_option").css('margin-top','55px');
      });
   
      $('#btn_xn_end').click(function(){
        $(".box_option").css('margin-top','0px');

       let date_end_from=$("#date_end_from").val();
       let date_end_to=$("#date_end_to").val();
       let tach_end_from=date_end_from.split(' ');
       let tach2_end_from=tach_end_from[0].split('-');
       let ngay_end_from,thang_end_from,nam_end_from;
       
       ngay_end_from=tach2_end_from[0];
       thang_end_from=end_from=tach2_end_from[1];
       nam_end_from=tach2_end_from[2];
   
       
       let tach_end_to=date_end_to.split(' ');
       let tach2_end_to=tach_end_to[0].split('-');
       let ngay_end_to,thang_end_to,nam_end_to
       
       ngay_end_to=tach2_end_to[0];
       thang_end_to=end_to=tach2_end_to[1];
       nam_end_to=tach2_end_to[2];
   
       if(parseDate(date_end_from).getTime()<parseDate(date_end_to).getTime()){
           alert("Lỗi chọn ngày tháng năm. Vui lòng chọn đúng ngày");
           $("#box_end_from").css('display','none');
   return false;
       }else{
   
       
           
       let data=date_end_from +' - '+date_end_to;
   
       $('#end_from').val(data);
       $("#box_end_from").css('display','none');
       
       }
      })
      
      });
      
</script>