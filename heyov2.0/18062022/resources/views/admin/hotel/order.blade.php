<!DOCTYPE html>
<html lang="en">

<head>
    <title>HF Admin</title>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="{{asset('public/admin/css/manage-book.css')}}" />
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="https://code.iconify.design/2/2.2.1/iconify.min.js"></script>
    <script src="https://unpkg.com/@popperjs/core@2"></script>
    <link rel='stylesheet'
          href='https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css'>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js'></script>
    <script
        src='https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js'></script>
    <script></script>
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css"
          rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
    <script src="{{asset('public/admin/js/main.js')}}"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <style>
        .toggle.ios,
        .toggle-on.ios,
        .toggle-off.ios {
            border-radius: 20rem;
        }

        .toggle.ios .toggle-handle {
            border-radius: 20rem;
        }
    </style>
    <script>
        function test(value) {
            alert(typeof value)
        }
        let arrtotal=['code_order','name_customer','create_fromto','phone_customer','name_hotel','location','nhanvienphutrach','statuspayment','partner','revice_fromto'];
        let arr = ["code_order","name_customer","statuspayment"];
        function handleChooseCriteria(value,id){
            if($("#"+id).is(':checked')){
                arr.push(value)
                console.log(arr)
            }else{
               const data = arr.filter((item) => {
                   return item !== value
               })
                arr = data;
               console.log(arr)
            }
        }
        $(document).ready(function () {
            $("#button_done_criteria").click(function () {
                arrtotal.forEach(function(value){
                    if(arr.filter((item) => {
                        return item === value
                    }).length > 0){
                        $("#"+value).removeClass('none');
                    }else{
                        $("#"+value).addClass('none');
                    }
                })
            })
            $("#buttonWaiting").click();
            $("#buttonAccepted").click();
            $("#buttonCancelled").click();
            $("#buttonDone").click();
        })
    </script>
</head>

<body>
<div class="menu_header">
    <div class="left">
        <a href="#" class="logo">HEYO TRIP</a>
    </div>
    <div class="right">
        <span class="name_menu"> Quản lý đơn đặt phòng </span>
        <div class="box_admin">
                <span class="iconify" style="width: 25px; height: 25px; color: #032044"
                      data-icon="ic:baseline-notifications-none"></span>
            <div style="color: white;background-color: red" class="badge badge-danger counter">9</div>
            <div id="btn_admin" class="wrapper_admin">
                    <span style="width: 25px; height: 25px; color: #032044" class="iconify"
                          data-icon="lucide:user"></span>
                <span class="admin">Admin</span>
                <span style="
                width: 15px;
                height: 15px;
                color: #032044;
                margin-left: 10px;
              " class="iconify" data-icon="ant-design:caret-down-filled"></span>
            </div>
        </div>
    </div>
    <div id="profile" style="box-shadow: 10;" class="box_profile">
            <span style="width: 30px; height: 30px;color: #032044;" class="iconify"
                  data-icon="simple-line-icons:logout"></span>
        <span class="logout">Đăng xuất</span>
    </div>
</div>
<div style="display: flex">
    <div class="menu_left">
        <div class="top">
            <a href="#" style="background-color: #f2f9ff; text-decoration: none" class="item">
                    <span class="iconify" style="color: #032044; width: 30px; height: 30px"
                          data-icon="icon-park-outline:hotel"></span>
                <span class="name_menu">Khách sạn</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
                <span class="iconify" style="color: #526a87; width: 30px; height: 30px" data-icon="ps:plane"></span>
                <span style="color: #526a87" class="name_menu">Vé máy bay</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
                    <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                          data-icon="clarity:car-line"></span>
                <span style="color: #526a87" class="name_menu">Thuê xe</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
                    <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                          data-icon="clarity:data-cluster-line"></span>
                <span style="color: #526a87" class="name_menu">Crypto</span>
            </a>
        </div>
        <div class="top">
            <a href="#" style="text-decoration: none" class="item">
                    <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                          data-icon="ep:user-filled"></span>
                <span style="color: #526a87" class="name_menu">Tài khoản</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
                    <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                          data-icon="dashicons:welcome-widgets-menus"></span>
                <span style="color: #526a87" class="name_menu">Danh mục</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
                    <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                          data-icon="gg:website"></span>
                <span style="color: #526a87" class="name_menu">Website</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
                    <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                          data-icon="material-symbols:settings-suggest-outline-sharp"></span>
                <span style="color: #526a87" class="name_menu">Cấu hình</span>
            </a>
        </div>
    </div>
    <div class="menu_right">
        <a href="#" class="item">
                <span style="width: 25px; height: 25px; color: #032044" class="iconify"
                      data-icon="majesticons:analytics"></span>
            <span class="name_menu">Dashboard</span>
        </a>
        <a  href="{{asset('admin/hotel/searchHotel')}}"class="item">
            <span style="width: 25px; height: 25px; color: #032044" class="iconify" data-icon="mdi:bed"></span>
            <span class="name_menu">Tìm - Đặt phòng</span>
        </a>
        <a href="#" class="item">
                <span style="width: 25px; height: 25px; color: #329223" class="iconify"
                      data-icon="fa-solid:calendar-day"></span>
            <span class="name_menu" style="color: #329223; font-weight: 800">Quản lý đơn đặt phòng</span>
        </a>
        <a href="#" href="#" class="item">
                <span style="width: 25px; height: 25px; color: #032044" class="iconify"
                      data-icon="fa-solid:money-check-alt"></span>
            <span class="name_menu">Tuỳ chỉnh giá phòng</span>
        </a>
        <a href="index.html" class="item">
                <span style="width: 25px; height: 25px; color: #032044" class="iconify"
                      data-icon="icomoon-free:price-tags"></span>
            <span class="name_menu">Chương trình khuyến mãi</span>
        </a>
        <a href="#" class="item">
                <span style="width: 25px; height: 25px; color: #032044" class="iconify"
                      data-icon="eva:percent-fill"></span>
            <span class="name_menu">Quản lý hoa hồng</span>
        </a>
        <a href="#" class="item">
                <span style="width: 25px; height: 25px; color: #032044" class="iconify"
                      data-icon="fa6-solid:hand-holding-dollar"></span>
            <span class="name_menu">Huỷ - Hoàn tiền</span>
        </a>
        <a href="#" class="item">
                <span style="width: 25px; height: 25px; color: #032044" class="iconify"
                      data-icon="fluent:news-20-filled"></span>
            <span class="name_menu">Bài viết Marketing</span>
        </a>
    </div>
    <?Php
    $order_hotel=DB::table('hotel_order_details')->get();
    $order_hotel_by_5=DB::table('hotel_order_details')->orderby('id',"DESC")->paginate(5);
    $i=0;
    $j=0;
    $k=0;
    $l=0;
    foreach ($order_hotel as $h){
        $o=DB::table('orders')->where('id',$h->order_id)->first();

        if($o->order_status_id==1){
            $i++;

        }
        if($o->order_status_id==2){
            $j++;
        }
        if($o->order_status_id==4){
            $k++;
        }
        if($o->order_status_id==5){
            $l++;
        }

    }
    Session::put('choxuly',$i);
    Session::put('daxacnhan',$j);
    Session::put('dahuy',$k);
    Session::put('dahoanthanh',$l);




    ?>
    <div class="box_content">
        <div class="box_option">
            <button id="button_tab_all" class="btn option tab_active">
                Tất cả (100)
            </button>
            <button id="button_tab_waiting" class="btn option">Chờ xử lý <span
                    style="color: red">({{Session::get('choxuly')}})</span></button>
            <button id="button_tab_accepted" class="btn option">Đã xác nhận ({{Session::get('daxacnhan')}})</button>
            <button id="button_tab_canceled" class="btn option">Đã hủy ({{Session::get('dahuy')}})</button>
            <button id="button_tab_done" class="btn option">Hoàn thành ({{Session::get('dahoanthanh')}})</button>
        </div>
            <div class="wrapper">
                <form class="box_search" method="post">
                    <div class="dropdown">
                        <button type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false" class="btn button_criteria">
                            <span>Tiêu chí đánh giá</span>
                            <span style="margin-left: 10px;" class="iconify"
                                  data-icon="ant-design:caret-down-outlined"></span>
                        </button>
                        <div style="padding: 10px; top: 35px;" class="dropdown-menu box_choose_criteria"
                             aria-labelledby="dropdownMenuButton">
                            <div class="choose_criteria">
                                <input type="checkbox"onclick="handleChooseCriteria(value,id)" value="code_order" id="code_order_checkbox" value="1" checked />
                                <span class="name_criteria">Mã đơn hàng</span>
                            </div>
                            <div class="choose_criteria">
                                <input type="checkbox"onclick="handleChooseCriteria(value,id)" value="create_fromto"  id="date_start_end_checkbox" value="2"/>
                                <span class="name_criteria">Ngày tạo từ - đến</span>
                            </div>
                            <div class="choose_criteria">
                                <input type="checkbox"onclick="handleChooseCriteria(value,id)" value="name_customer" id="name_customer_checkbox" value="3" checked />
                                <span class="name_criteria">Tên khách</span>
                            </div>
                            <div class="choose_criteria">
                                <input type="checkbox" onclick="handleChooseCriteria(value,id)" value="phone_customer"value="4" id="phone_customer_checkbox"/>
                                <span class="name_criteria">Sđt khách</span>
                            </div>
                            <div class="choose_criteria">
                                <input type="checkbox"onclick="handleChooseCriteria(value,id)" value="name_hotel" value="5" id="name_hotel_checkbox" />
                                <span class="name_criteria">Tên khách sạn</span>
                            </div>
                            <div class="choose_criteria">
                                <input type="checkbox" onclick="handleChooseCriteria(value,id)" value="location"  id="location_checkbox" />
                                <span class="name_criteria">Điểm đến</span>
                            </div>
                            <div class="choose_criteria">
                                <input type="checkbox"onclick="handleChooseCriteria(value,id)" value="revice_fromto" id="revice_fromto_checkbox" />
                                <span class="name_criteria">Ngày nhận phòng từ - đến</span>
                            </div>
                            <div class="choose_criteria">
                                <input type="checkbox"  onclick="handleChooseCriteria(value,id)" value="partner" id="partner_checkbox"/>
                                <span class="name_criteria">Đối tác</span>
                            </div>
                            <div class="choose_criteria">
                                <input type="checkbox" onclick="handleChooseCriteria(value,id)" value="nhanvienphutrach" id="nhanvienphutrach_checkbox"/>
                                <span class="name_criteria">Nhân viên phụ trách</span>
                            </div>
                            <div class="choose_criteria">
                                <input onclick="handleChooseCriteria(value,id)" type="checkbox" value="statuspayment" id="status_payment_checkbox"checked />
                                <span class="name_criteria">Trạng thái thanh toán</span>
                            </div>
                            <div class="box_option_criteria">
                                <button type="button" class="btn dropdown-item">Chọn lại</button>
                                <button class="btn dropdown-item" id='button_done_criteria'type="button">Xong</button>
                            </div>
                        </div>
                    </div>
                    <div class="label_criteria">Hãy chọn tiêu chí để tìm kiếm thông tin đơn hàng. Bạn có thể chọn
                        nhiều
                        tiêu chí cùng lúc.</div>
                    <div class="row box_input_search">
                        <div class="col col-md-6 col-lg-4 col-xl-3" id="code_order">
                            <input placeholder="Mã đơn hàng" name="code_order" class="input_search" />
                        </div>
                        <div class="col col-md-6 col-lg-4 col-xl-3" id="name_customer">
                            <input placeholder="Tên khách" name="name_customer" class="input_search" />
                        </div>
                        <div class="col col-md-6 col-lg-4 col-xl-3" id="phone_customer">
                            <input placeholder="SĐT khách" name="phone_customer" class="input_search" />
                        </div>
                        <div class="col col-md-6 col-lg-4 col-xl-3" id="name_hotel">
                            <input type="text" name="name_hotel" id="hotel_serach" class="input_search" placeholder="Tên khách sạn">
                            <div id="hotel_serach_ajax"></div>

                        </div>
                        <div id="revice_fromto" class="col col-md-6 col-lg-4 col-xl-3">
                                <div class="col col-md-6 col-lg-6 col-xl-6" >
                                    <input placeholder="Mã đơn hàng" name="revice_from" title="Ngày nhận từ" id="datetimepicker3" class="input_search" />
                                </div>
                                <div class="col col-md-6 col-lg-6 col-xl-6" >
                                    <input placeholder="Mã đơn hàng" name="revice_to" id="datetimepicker4"title="Ngày nhận đến" class="input_search" />
                                </div>
                        </div>
                        <div class="col col-md-6 col-lg-4 col-xl-3" id="create_fromto">
                            <div class="col col-md-6 col-lg-6 col-xl-6" >
                                <input placeholder="Mã đơn hàng" name="create_from" title="Ngày tạo từ" id="datetimepicker1" class="input_search" /></div>
                            <div class="col col-md-6 col-lg-6 col-xl-6" >
                                <input placeholder="Mã đơn hàng" name="create_to" title="Ngày tạo đến" id="datetimepicker2" class="input_search" />
                            </div>
                        </div>
                        <div class="col col-md-6 col-lg-4 col-xl-3" id="location" >
                            <input type="text" name="locations" id="locations_serach" class="input_search" placeholder="Điểm đến">
                            <div id="location_serach_ajax"></div>

                        </div>
                        <div class="col col-md-6 col-lg-4 col-xl-3" id="partner">
                            <select name="partner" style="height: 35px; width: 100%;  border: 1px solid #cccccc; border-radius: 2px; padding: 0px 10px;
                                font-family: 'Montserrat';
                                font-style: normal;
                                font-weight: 400;
                                font-size: 14px;
                                line-height: 20px;
                                margin-bottom: 10px;
                                color: #666666" class="select_criteria">
                                <option value="" disabled selected hidden>Đối tác</option>
                                <option value="1" disabled selected hidden>TVP</option>
                                <option value="2" disabled selected hidden>EZC</option>
                                <option value="3" >VJ</option>
                                <option value="4" disabled selected hidden>HPL</option>
                            </select>
                        </div>
                        <div class="col col-md-6 col-lg-4 col-xl-3" id="nhanvienphutrach">
                            <select name="nhanvienphutrach" style="height: 35px; width: 100%;  border: 1px solid #cccccc; border-radius: 2px; padding: 0px 10px;
                                font-family: 'Montserrat';
                                font-style: normal;
                                font-weight: 400;
                                font-size: 14px;
                                line-height: 20px;
                                margin-bottom: 10px;
                                color: #666666" class="select_criteria">
                                <option value="" >Nhân viên phụ trách</option>
                                <?php
                                $user=DB::table('tbl_admin')->get();
                                ?>
                                @foreach($user as $u)
                                <option value="" >{{$u->admin_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col col-md-6 col-lg-4 col-xl-3" id="statuspayment">
                            <select style="height: 35px; width: 100%;  border: 1px solid #cccccc; border-radius: 2px; padding: 0px 10px;
                                font-family: 'Montserrat';
                                font-style: normal;
                                font-weight: 400;
                                font-size: 14px;
                                line-height: 20px;
                                margin-bottom: 10px;
                                color: #666666" class="select_criteria">
                                <option value="" disabled selected hidden>Trạng thái thanh toán</option>
                                <option value="1" >Đã Thanh toán</option>
                                <option value="0" >Chưa Thanh toán</option>
                                <option value="2" >Thanh Toán Thất Bại</option>
                                <option value="3" >Hết Hạn Thanh Toán</option>
                                <option value="4" >Đang Hoàn Huỷ</option>
                                <option value="5" >Đã Hoàn Tiền</option>
                            </select>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col col-md-6 col-lg-4 col-xl-3 box_button_search">
                            <button type="submit" style="text-decoration: none;border: 0px;" class="button_search">
                                    <span style="width: 20px; height: 20px; color: #fff;" class="iconify"
                                          data-icon="fluent:search-24-filled"></span>
                                <span class="name_button_search">Tìm kiếm</span>
                            </button>
                            <button type="button" class="button_type_again">
                                Nhập lại
                            </button>
                        </div>
                    </div>
                </form>
                <div class="box_title">
                    <span class="title_small">Đơn hàng</span>
                    <span class="title_small">Người liên hệ</span>
                    <span class="title_big">Khách sạn</span>
                    <span class="title_small">Ngày nhận phòng</span>
                    <span class="title_small">Thanh toán</span>
                    <span class="title_small">Đối tác</span>
                    <span class="title_small">Phụ trách</span>
                    <span style="width: 6%;"> </span>
                </div>
            </div>

        <div id="all_order" class="fade-in show">
            <div class="box_button_collapse">
                <div id="buttonWaiting" class="button_collapse" data-toggle="collapse"
                     data-target="#collapseWaiting" aria-expanded="true" aria-controls="collapseWaiting">
                    Chờ xử lý (@if(Session::get('choxuly')!=null){{Session::get('choxuly')}} @else 0 @endif)
                </div>
                <div style="width: 90%; background-color: #D5E2EA;height: 1px;margin-right: 20px;"></div>
                <span id="iconWaiting" style="color: #888888; cursor: pointer; transform: rotate(0deg);"
                      class="iconify" data-icon="ant-design:caret-down-outlined" data-toggle="collapse"
                      data-target="#collapseWaiting" aria-expanded="true" aria-controls="collapseWaiting"></span>
            </div>
            <div class="collapse" id="collapseWaiting">
            @if(Session::get('choxuly')!=null)
            <div class="collapse_body">
                    @foreach ($order_hotel_by_5 as $h)
                        <?php
                    $o=DB::table('orders')->where('id',$h->order_id)->first();
                    $hotel=DB::table('hotels')->where('id',$h->hotel_id)->first();
                    $countries=DB::table('locations')->where('id',$hotel->country_id)->first();

                    if($o->order_status_id==1){


                        ?>
    <div class="box_information">
        <div class="info_small">
            <div class="text_blue">{{$o->order_code}}</div>
            <div class="text_gray">{{$h->created_at}}</div>
            <div style="display: flex;align-items: center; justify-content: center;">
                <div class="box_info_book">@if($o->order_status_id==1) Chờ xử lý @endif</div>
            </div>
        </div>
        <div class="info_small">
            <div class="text_black_bold">{{$h->first_name}} {{$h->last_name}}</div>
            <div class="text_gray">{{$h->phone_number}}</div>
        </div>
        <div class="info_big">
            <div class="text_black_normal">{{$hotel->hotel_name}}</div>
        </div>
        <div class="info_small">
            <div class="text_black_normal">{{$h->check_in_date}}</div>
            <div class="text_gray">{{$countries->name_vi}}</div>
        </div>
        <div class="info_small">
            <div class="text_black_bold">{{number_format($o->total_price,0,'.','.') }} đ</div>
            <div class="text_green">@if($h->payment_status_id==0) Chưa thanh toán @elseif($h->payment_status_id==1) Đã thanh toán @elseif($h->payment_status_id==2) Thanh toán thất bại @elseif($h->payment_status_id==3)Hết hạn thanh toán @elseif($h->payment_status_id==4)Đang hoàn hủy @else Đã hoàn tiền @endif</div>
        </div>
        <div class="info_small">
            <div class="text_black_normal">@if($o->vendor_id==1)TPV @elseif($o->vendor_id==2) EZC @elseif($o->vendor_id) HPL @else VJ @endif</div>
        </div>
        <div class="info_small">
            <div class="text_black_normal">CS01 - Hương</div>
            <div class="text_red">Chờ xử lý</div>
        </div>
        <div class="info_small" style="width: 6%;">
            <button id="open_popover" type="button"
                    style="background-color: transparent;border-radius: 100px;" class="btn"><span
                    style="width: 25px;height: 25px; color: #C1C1C1" class="iconify"
                    data-icon="entypo:dots-three-horizontal"></span></button>
            <div id="box_popover_id" class="box_popover">
                <a href="#" class="btn item_popover">Đặt phòng lại</a>
                <a href="#" class="btn item_popover">Xác nhận đã thanh toán</a>
                <a href="#" class="btn item_popover">Huỷ đặt phòng</a>
            </div>

        </div>
    </div>
    <div style="margin: 0px" class="dropdown-divider"></div>
                        <?php
                        }?>
                @endforeach
</div>
                @else
                <p class="nodata">Không có dữ liệu !!!</p>
                @endif
</div>
<div class="box_button_collapse">
<div id="buttonAccepted" class="button_collapse" data-toggle="collapse"
     data-target="#collapseAccepted" aria-expanded="true" aria-controls="collapseAccepted">
    Đã xác nhận (@if(Session::get('daxacnhan')!=null) {{Session::get('daxacnhan')}} @else 0 @endif)
</div>
<div style="width: 90%; background-color: #D5E2EA;height: 1px;margin-right: 20px;"></div>
<span id="iconAccepted" style="color: #888888; cursor: pointer; transform: rotate(0deg);"
      class="iconify" data-icon="ant-design:caret-down-outlined" data-toggle="collapse"
      data-target="#collapseAccepted" aria-expanded="true" aria-controls="collapseAccepted"></span>
</div>
<div class="collapse" id="collapseAccepted">
<div class="collapse_body">
    @if(Session::get('daxacnhan')!=null)
    <div class="box_information">
        <div class="info_small">
            <div class="text_blue">H-20210821102832</div>
            <div class="text_gray">10:28:32 21/08/2021</div>
        </div>
        <div class="info_small">
            <div class="text_black_bold">Nguyễn Văn A</div>
            <div class="text_gray">0987654321</div>
        </div>
        <div class="info_big">
            <div class="text_black_normal">SHANGRI LA JAKARTA</div>
        </div>
        <div class="info_small">
            <div class="text_black_normal">30/08/2021</div>
            <div class="text_gray">Indonesia</div>
        </div>
        <div class="info_small">
            <div class="text_black_bold">1.730.400 đ</div>
            <div class="text_green">Đã thanh toán</div>
        </div>
        <div class="info_small">
            <div class="text_black_normal">TVP</div>
        </div>
        <div class="info_small">
            <div class="text_black_normal">CS02 - Mai</div>
            <div class="text_red">Chờ xử lý</div>
        </div>
        <div class="info_small" style="width: 6%;">
            <buttons style="background-color: transparent;border-radius: 100px;" class="btn"><span
                    style="width: 25px;height: 25px; color: #C1C1C1" class="iconify"
                    data-icon="entypo:dots-three-horizontal"></span></buttons>

        </div>
    </div>
    <div style="margin: 0px" class="dropdown-divider"></div>
    <div class="box_information">
        <div class="info_small">
            <div class="text_blue">H-20210821102832</div>
            <div class="text_gray">10:28:32 21/08/2021</div>
        </div>
        <div class="info_small">
            <div class="text_black_bold">Nguyễn Văn A</div>
            <div class="text_gray">0987654321</div>
        </div>
        <div class="info_big">
            <div class="text_black_normal">SHERATON GRAND GANDARIA CITY</div>
        </div>
        <div class="info_small">
            <div class="text_black_normal">30/08/2021</div>
            <div class="text_gray">USA</div>
        </div>
        <div class="info_small">
            <div class="text_black_bold">1.730.400 đ</div>
            <div class="text_green">Đã thanh toán</div>
        </div>
        <div class="info_small">
            <div class="text_black_normal">TVP</div>
        </div>
        <div class="info_small">
            <div class="box_info_receive">Tiếp nhận xử lý</div>
            <div class="text_red">Chờ xử lý</div>
        </div>
        <div class="info_small" style="width: 6%;">
            <buttons style="background-color: transparent;border-radius: 100px;" class="btn"><span
                    style="width: 25px;height: 25px; color: #C1C1C1" class="iconify"
                    data-icon="entypo:dots-three-horizontal"></span></buttons>

        </div>
    </div>
    <div style="margin: 0px" class="dropdown-divider"></div>
    <div class="box_information">
        <div class="info_small">
            <div class="text_blue">H-20210821102832</div>
            <div class="text_gray">10:28:32 21/08/2021</div>
        </div>
        <div class="info_small">
            <div class="text_black_bold">Nguyễn Văn A</div>
            <div class="text_gray">0987654321</div>
        </div>
        <div class="info_big">
            <div class="text_black_normal">SHERATON GRAND GANDARIA CITY</div>
        </div>
        <div class="info_small">
            <div class="text_black_normal">30/08/2021</div>
            <div class="text_gray">USA</div>
        </div>
        <div class="info_small">
            <div class="text_black_bold">1.730.400 đ</div>
            <div class="text_green">Đã thanh toán</div>
        </div>
        <div class="info_small">
            <div class="text_black_normal">TVP</div>
        </div>
        <div class="info_small">
            <div class="box_info_receive">Tiếp nhận xử lý</div>
            <div class="text_red">Chờ xử lý</div>
        </div>
        <div class="info_small" style="width: 6%;">
            <buttons style="background-color: transparent;border-radius: 100px;" class="btn"><span
                    style="width: 25px;height: 25px; color: #C1C1C1" class="iconify"
                    data-icon="entypo:dots-three-horizontal"></span></buttons>

        </div>
    </div>
    <div style="margin: 0px" class="dropdown-divider"></div>
    @else
    <p class="nodata">Không có dữ liệu !!!</p>
    @endif
</div>
</div>
<div class="box_button_collapse">
<div id="buttonCancelled" class="button_collapse" data-toggle="collapse"
     data-target="#collapseCancelled" aria-expanded="true" aria-controls="collapseCancelled">
    Đã huỷ (@if(Session::get('dahuy')!=null) {{Session::get('dahuy')}} @else 0 @endif )
</div>
<div style="width: 90%; background-color: #D5E2EA;height: 1px;margin-right: 20px;"></div>
<span id="iconCancelled" style="color: #888888; cursor: pointer; transform: rotate(0deg);"
      class="iconify" data-icon="ant-design:caret-down-outlined" data-toggle="collapse"
      data-target="#collapseCancelled" aria-expanded="true" aria-controls="collapseCancelled"></span>
</div>
<div class="collapse" id="collapseCancelled">

    @if(Session::get('dahuy')!=null)
<div class="collapse_body">
    <div class="box_information">
        <div class="info_small">
            <div class="text_blue">H-20210821102832</div>
            <div class="text_gray">10:28:32 21/08/2021</div>
        </div>
        <div class="info_small">
            <div class="text_black_bold">Nguyễn Văn A</div>
            <div class="text_gray">0987654321</div>
        </div>
        <div class="info_big">
            <div class="text_black_normal">CROWNE PLAZA DUBAI</div>
        </div>
        <div class="info_small">
            <div class="text_black_normal">30/08/2021</div>
            <div class="text_gray">Dubai</div>
        </div>
        <div class="info_small">
            <div class="text_black_bold">1.730.400 đ</div>
            <div class="text_orange">Chưa thanh toán</div>
        </div>
        <div class="info_small">
            <div class="text_black_normal">TVP</div>
        </div>
        <div class="info_small">
            <div class="text_black_normal">CS01 - Hương</div>
            <div class="text_red">Đã hủy</div>
        </div>
        <div class="info_small" style="width: 6%;">
            <buttons style="background-color: transparent;border-radius: 100px;" class="btn"><span
                    style="width: 25px;height: 25px; color: #C1C1C1" class="iconify"
                    data-icon="entypo:dots-three-horizontal"></span></buttons>

        </div>
    </div>
    <div style="margin: 0px" class="dropdown-divider"></div>
</div>
    @else
    <p class="nodata">Không có dữ liệu !!!</p>
    @endif
</div>
<div class="box_button_collapse">
<div id="buttonDone" class="button_collapse" data-toggle="collapse" data-target="#collapseDone"
     aria-expanded="true" aria-controls="collapseDone">
    Đã hoàn thành (@if(Session::get('dahoanthanh')!=null) {{Session::get('dahoanthanh')}} @else 0 @endif )
</div>
<div style="width: 90%; background-color: #D5E2EA;height: 1px;margin-right: 20px;"></div>
<span id="iconDone" style="color: #888888; cursor: pointer; transform: rotate(0deg);"
      class="iconify" data-icon="ant-design:caret-down-outlined" data-toggle="collapse"
      data-target="#collapseDone" aria-expanded="true" aria-controls="collapseDone"></span>
</div>
<div class="collapse" id="collapseDone">
    @if(Session::get('dahoanthanh')!=null)
<div class="collapse_body">
    <div class="box_information">
        <div class="info_small">
            <div class="text_blue">H-20210821102832</div>
            <div class="text_gray">10:28:32 21/08/2021</div>
        </div>
        <div class="info_small">
            <div class="text_black_bold">Nguyễn Văn A</div>
            <div class="text_gray">0987654321</div>
        </div>
        <div class="info_big">
            <div class="text_black_normal">CROWNE PLAZA DUBAI</div>
        </div>
        <div class="info_small">
            <div class="text_black_normal">30/08/2021</div>
            <div class="text_gray">Dubai</div>
        </div>
        <div class="info_small">
            <div class="text_black_bold">1.730.400 đ</div>
            <div class="text_orange">Chưa thanh toán</div>
        </div>
        <div class="info_small">
            <div class="text_black_normal">TVP</div>
        </div>
        <div class="info_small">
            <div class="text_black_normal">CS01 - Hương</div>
            <div class="text_brown">Đã hoàn thành</div>
        </div>
        <div class="info_small" style="width: 6%;">
            <buttons style="background-color: transparent;border-radius: 100px;" class="btn"><span
                    style="width: 25px;height: 25px; color: #C1C1C1" class="iconify"
                    data-icon="entypo:dots-three-horizontal"></span></buttons>

        </div>
    </div>
    <div style="margin: 0px" class="dropdown-divider"></div>
</div>
    @else
    <p class="nodata">Không có dữ liệu</p>
    @endif
</div>
</div>
<div id="waiting_order" class="fade-in hide">
<div class="wrapper">
    <?php
    $order_hotel=DB::table('hotel_order_details')->paginate(10);

    ?>
    @foreach ($order_hotel as $h)
        <?php
        $o=DB::table('orders')->where('id',$h->order_id)->first();
        if ($o!=null){
        $hotel=DB::table('hotels')->where('id',$h->hotel_id)->first();
            if ($hotel!=null){

            $countries=DB::table('locations')->where('id',$hotel->country_id)->first();

        if($o->order_status_id==1){


            ?>
            <div class="box_information">
                <div class="info_small">
                    <div class="text_blue">{{$o->order_code}}</div>
                    <div class="text_gray">{{$h->created_at}}</div>
                    <div style="display: flex;align-items: center; justify-content: center;">
                        <div class="box_info_book">@if($o->order_status_id==1) Chờ xử lý @endif</div>
                    </div>
                </div>
                <div class="info_small">
                    <div class="text_black_bold">{{$h->first_name}} {{$h->last_name}}</div>
                    <div class="text_gray">{{$h->phone_number}}</div>
                </div>
                <div class="info_big">
                    <div class="text_black_normal">{{$hotel->hotel_name}}</div>
                </div>
                <div class="info_small">
                    <div class="text_black_normal">{{$h->check_in_date}}</div>
                    <div class="text_gray">{{$countries->name_vi}}</div>
                </div>
                <div class="info_small">
                    <div class="text_black_bold">{{number_format($o->total_price,0,'.','.') }} đ</div>
                    <div class="text_green">@if($h->payment_status_id==0) Chưa thanh toán @elseif($h->payment_status_id==1) Đã thanh toán @elseif($h->payment_status_id==2) Thanh toán thất bại @elseif($h->payment_status_id==3)Hết hạn thanh toán @elseif($h->payment_status_id==4)Đang hoàn hủy @else Đã hoàn tiền @endif</div>
                </div>
                <div class="info_small">
                    <div class="text_black_normal">@if($o->vendor_id==1)TPV @elseif($o->vendor_id==2) EZC @elseif($o->vendor_id) HPL @else VJ @endif</div>
                </div>
                <div class="info_small">
                    <div class="text_black_normal">CS01 - Hương</div>
                    <div class="text_red">Chờ xử lý</div>
                </div>
                <div class="info_small" style="width: 6%;">
                    <button id="open_popover" type="button"
                            style="background-color: transparent;border-radius: 100px;" class="btn"><span
                            style="width: 25px;height: 25px; color: #C1C1C1" class="iconify"
                            data-icon="entypo:dots-three-horizontal"></span></button>
                    <div id="box_popover_id" class="box_popover">
                        <a href="#" class="btn item_popover">Đặt phòng lại</a>
                        <a href="#" class="btn item_popover">Xác nhận đã thanh toán</a>
                        <a href="#" class="btn item_popover">Huỷ đặt phòng</a>
                    </div>

                </div>
            </div>
            <div style="margin: 0px" class="dropdown-divider"></div>
        <?php
        }
        }
        }
        ?>
    @endforeach
    {{$order_hotel->links('admin.paginate_waiting_order')}}
</div>
</div>
<div id="accepted_order" class="fade-in hide">accepted_order</div>
<div id="canceled_order" class="fade-in hide">canceled_order</div>
<div id="done_order" class="fade-in hide">done_order</div>
</div>
</div>
<div id="modalReceiveAccess" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title">Tiếp Nhận Xử Lý Đơn Hàng</h4>
<button type="button" class="close1" data-dismiss="modal">
        <span style="width: 30px; height:30px; color: #000" class="iconify"
              data-icon="ep:close-bold"></span>
</button>
</div>
<div class="modal-body">
<div class="content_modal_receive_access">
    Nhân viên CSKH được phân công sẽ chịu trách nhiệm xử lý các vấn đề phát sinh của đơn hàng và
    chăm sóc hỗ trợ khách hàng. Bạn sẽ tiếp nhận xử lý đơn hàng <b
        style="color: #3982d1">H-20210821102832</b>?
</div>
</div>
<div class="modal-footer">
<button type="button" class="button_receive" data-dismiss="modal">Tiếp nhận</button>
</div>
</div>

</div>
</div>
</body>

</html>
<script>
</script>
