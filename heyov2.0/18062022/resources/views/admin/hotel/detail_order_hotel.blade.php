<!DOCTYPE html>
<html lang="en">

<head>
    <title>HF Admin</title>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="{{asset('public/admin/css/order-detail.css')}}" />
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="https://code.iconify.design/2/2.2.1/iconify.min.js"></script>
    <script src="https://unpkg.com/@popperjs/core@2"></script>
    <link rel='stylesheet'
          href='https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css'>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js'></script>
    <script
        src='https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js'></script>
    <script></script>
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css"
          rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
    <script src="{{asset('public/admin/js/main.js')}}"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <style>
        .toggle.ios,
        .toggle-on.ios,
        .toggle-off.ios {
            border-radius: 20rem;
        }

        .toggle.ios .toggle-handle {
            border-radius: 20rem;
        }
    </style>
    <script>
        $(document).ready(function () {
            $("#buttonCheckin").click();
        })
    </script>
</head>

<body>
<div class="menu_header">
    <div class="left">
        <a href="#" class="logo">HEYO TRIP</a>
    </div>
<?Php
    $order=DB::table('orders')->where('id',$detail_order_hotel->order_id)->first();
    ?>
    <div class="right">
        <span class="name_menu"> Chi tiết đơn hàng # {{$order->order_code}}     </span>
        <div class="box_admin">
                <span class="iconify" style="width: 25px; height: 25px; color: #032044"
                      data-icon="ic:baseline-notifications-none"></span>
            <div style="color: white;background-color: red" class="badge badge-danger counter">9</div>
            <div id="btn_admin" class="wrapper_admin">
                    <span style="width: 25px; height: 25px; color: #032044" class="iconify"
                          data-icon="lucide:user"></span>
                <span class="admin">Admin</span>
                <span style="
                width: 15px;
                height: 15px;
                color: #032044;
                margin-left: 10px;
              " class="iconify" data-icon="ant-design:caret-down-filled"></span>
            </div>
        </div>
    </div>
    <div id="profile" style="box-shadow: 10;" class="box_profile">
            <span style="width: 30px; height: 30px;color: #032044;" class="iconify"
                  data-icon="simple-line-icons:logout"></span>
        <span class="logout">Đăng xuất</span>
    </div>
</div>
<div style="display: flex">
<div class="menu_left">
        <div class="top">
            <a href="{{asset('admin/hotel/order/all/1')}}" style="background-color: #f2f9ff; text-decoration: none" class="item">
                    <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                          data-icon="icon-park-outline:hotel"></span>
                <span style="color: #526a87" class="name_menu">Khách sạn</span>
            </a>
            <a href="{{asset('admin/flight/order/all/1')}}" style=" text-decoration: none" class="item">
                <span class="iconify" style="color: #526a87; width: 30px; height: 30px" data-icon="ps:plane"></span>
                <span  style="color: #526a87"class="name_menu">Vé máy bay</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
                <span class="iconify" style="color: #526a87; width: 30px; height: 30px" data-icon="clarity:car-line"></span>
                <span style="color: #526a87" class="name_menu">Thuê xe</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
               <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                     data-icon="clarity:data-cluster-line"></span>
                <span style="color: #526a87" class="name_menu">Crypto</span>
            </a>
        </div>
        <div class="top">
            <a href="#" style="text-decoration: none" class="item">
                <span class="iconify" style="color: #526a87; width: 30px; height: 30px" data-icon="ep:user-filled"></span>
                <span style="color: #526a87" class="name_menu">Tài khoản</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
               <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                     data-icon="dashicons:welcome-widgets-menus"></span>
                <span style="color: #526a87" class="name_menu">Danh mục</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
                <span class="iconify" style="color: #526a87; width: 30px; height: 30px" data-icon="gg:website"></span>
                <span style="color: #526a87" class="name_menu">Website</span>
            </a>
            <a href="{{asset('admin/website/list/quocgia')}}" style="text-decoration: none" class="item">
               <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                     data-icon="material-symbols:settings-suggest-outline-sharp"></span>
                <span style="color: #526a87" class="name_menu">Cấu hình</span>
            </a>
        </div>
    </div>
    <div class="menu_right">
        <div class="item">
               <span style="width: 25px; height: 25px; color: #032044" class="iconify"
                     data-icon="majesticons:analytics"></span>
            <span class="name_menu">Dashboard</span>
        </div>
        <a  href="{{asset('admin/hotel/searchHotel')}}"class="item">
            <span style="width: 25px; height: 25px; color: #032044" class="iconify" data-icon="mdi:bed"></span>
            <span class="name_menu">Tìm - Đặt phòng</span>
    </a>
        <a href="{{asset('admin/hotel/order/all/1')}}" class="item">
               <span style="width: 25px; height: 25px; color: #032044" style="width: 25px; height: 25px; color: #329223" class="iconify"
                     data-icon="fa-solid:calendar-day"></span>
            <span class="name_menu" style="color: #329223; font-weight: 800">Quản lý đơn đặt phòng</span>
    </a>
        <div class="item">
               <span style="width: 25px; height: 25px; color: #032044" class="iconify"
                     data-icon="fa-solid:money-check-alt"></span>
            <span class="name_menu">Tuỳ chỉnh giá phòng</span>
        </div>
        <div class="item">
               <span  class="iconify"
                     data-icon="icomoon-free:price-tags"></span>
            <span  class="name_menu"><a href='{{asset("admin/hotel/list")}}' style="width: 25px; height: 25px; color: rgb(3, 32, 68);">Chương trình khuyến mãi</a></span>
        </div>
        <div class="item">
            <span style="width: 25px; height: 25px; color: #032044" class="iconify" data-icon="eva:percent-fill"></span>
            <span class="name_menu">Quản lý hoa hồng</span>
        </div>
        <div class="item">
               <span style="width: 25px; height: 25px; color: #032044" class="iconify"
                     data-icon="fa6-solid:hand-holding-dollar"></span>
            <span class="name_menu">Huỷ - Hoàn tiền</span>
        </div>
        <div class="item">
               <span style="width: 25px; height: 25px; color: #032044" class="iconify"
                     data-icon="fluent:news-20-filled"></span>
            <span class="name_menu">Bài viết Marketing</span>
        </div>
    </div>      
    <div class="box_content">
        <div class="box_left">
            <div class="wrapper">
                <div class="box_title">
                    <div class="title">Thông tin đơn hàng</div>
                    <div class="box_option">
                        <button class="button_confirm">XN thanh toán</button>
                        <button class="button_cancel">Hủy đơn</button>
                    </div>
                </div>
                <div class="divider"></div>
                <div class="row_info">
                    <div class="box_info">
                        <div class="label">Mã đơn hàng:</div>
                        <?php
                        $tach=explode('-',$order->order_code)
                        ?>
                        <div class="info_black">{{$tach[1]}}</div>
                    </div>
                    <div class="box_info">
                        <div class="label">Trạng thái đơn hàng:</div>
                        <div class="@if($order->order_status_id==1)info_red @elseif($order->order_status_id==2) info_green @elseif($order->order_status_id==4) info_red @elseif($order->order_status_id==5) info_green  @elseif($order->order_status_id==6) info_red @endif">@if($order->order_status_id==1) Chờ xử lý @elseif($order->order_status_id==2) Đã book  @elseif($order->order_status_id==4) Đã Hủy  @elseif($order->order_status_id==5) Đã hoàn thành @elseif($order->order_status_id==6) Thất bại @endif</div>
                    </div>
                </div>
                <div class="row_info">
                    <div class="box_info">
                        <div class="label">Ngày tạo:</div>
                        <div class="info_black">{{$detail_order_hotel->created_at}}</div>
                    </div>
                    <div class="box_info">
                        <div class="label">Lý do cần xử lý:</div>
                        <div class="info_red">Đặt phòng thất bại --  -- update version next</div>
                    </div>
                </div>
                <div class="row_info">
                    <div class="box_info">
                        <div class="label">Người tạo:</div>
                        <div class="info_black">{{$detail_order_hotel->first_name}} {{$detail_order_hotel->last_name}}</div>
                    </div>
                    <div class="box_info">
                        <div class="label">Người phụ trách:</div>
                        <div class="info_black">CS01 - Hương -- update version next </div>
                    </div>
                </div>
                <div class="row_info">
                    <div class="box_info">
                        <div class="label">Ngày cập nhật:</div>
                        <div class="info_black">{{$detail_order_hotel->updated_at}}</div>
                    </div>
                    <div class="box_info">
                        <div class="label">Đối tác:</div>
                        <div class="info_black">@if($order->vendor_id==1) TVP @elseif($order->vendor_id==2) EZC @elseif($order->vendor_id==3) HPL @else VJ @endif </div>
                    </div>
                </div>
                <div class="row_info">
                    <div class="box_info">
                        <div class="label">TT thanh toán:</div>
                        <div class="@if($detail_order_hotel->payment_status_id==0)info_red @elseif($detail_order_hotel->payment_status_id==1) info_green @elseif($detail_order_hotel->payment_status_id==2) info_red @elseif($detail_order_hotel->payment_status_id==3) info_red @elseif($detail_order_hotel->payment_status_id==4) info_red @elseif($detail_order_hotel->payment_status_id==5) info_green @endif">@if($detail_order_hotel->payment_status_id==0) Chưa thanh toán @elseif($detail_order_hotel->payment_status_id==1) Đã thanh toán @elseif($detail_order_hotel->payment_status_id==2) Thanh toán thất bại @elseif($detail_order_hotel->payment_status_id==3) Hết hạn thanh toán @elseif($detail_order_hotel->payment_status_id==4) Đang hoàn hủy @elseif($detail_order_hotel->payment_status_id==5) Đã hoàn tiền @endif</div>
                    </div>
                    <div class="box_info">
                        <div class="label">Mã đơn đối tác:</div>
                        <div class="info_black">HL79B - 505582 --  -- update version next</div>
                    </div>
                </div>
                <div style="justify-content: space-between;" class="row_info">
                    <div class="info_black">Ghi chú</div>
                    <button class="button_edit">
                            <span style="color: #0770CD; width: 20px; height: 20px;" class="iconify"
                                  data-icon="ci:edit"></span>
                        <span>Chỉnh sửa</span>
                    </button>
                </div>
                <div class="box_note">đơn hàng chuyển giao từ CS03 - Minh sang cho CS01 Hương  --  -- update version next</div>
            </div>
            <div style="margin-top: 10px;" class="wrapper">
                <div style="padding: 5px 0px" class="box_title">
                    <div class="title">Thông tin liên hệ</div>
                </div>
                <div class="divider"></div>
                <div class="row_info">
                    <div class="box_info">
                        <div class="label">Tên - Họ:</div>
                        <div class="info_black">{{$detail_order_hotel->last_name}} {{$detail_order_hotel->first_name}} </div>
                    </div>
                    <div class="box_info">
                        <div class="label">Email:</div>
                        <div class="info_black">{{$order->email}}</div>
                    </div>
                </div>
                <div class="row_info">
                    <div class="box_info">
                        <div class="label">Quốc tịch:</div>
                        <?Php
                        if($order->country_id!=0){
                        $country=DB::table('locations')->where('id',$order->country_id)->first();
                        ?>
                        <div class="info_black">{{$country->name_vi}} - {{$country->name_en}}</div>
                        <?php }?>
                    </div>
                    <div class="box_info">
                        <div class="label">Số điện thoại:</div>
                        <div class="info_black">{{$detail_order_hotel->phone_number}}</div>
                    </div>
                </div>
            </div>
            <div style="margin-top: 10px;" class="wrapper">
                <div style="padding: 5px 0px" class="box_title">
                    <div class="title">Thông tin khách sạn</div>
                </div>
                <div class="divider"></div>
                <div class="box_hotel">
                    <div style="width: 100%; display: flex; justify-content: space-between;">
                        <div>
                            <?php
                            $hotel=DB::table('hotels')->where('id',$detail_order_hotel->hotel_id)->first();
                            if($hotel!=null){
                            ?>
                            <div class="hotel_name">
{{$hotel->hotel_name}}                            </div>

                            <div class="box_rating">
                                @for($i=1;$i<$hotel->hotel_star;$i++)
                                <span class="iconify" data-icon="bxs:star" style="color: #FFC95B"></span>
                                @endfor
                            </div>
                            <div class="label">@if($hotel->area!=null){{$hotel->area}} @elseif($hotel->address!=null) {{$hotel->address}} @else {{$hotel->district_name}},{{$hotel->city_name}},{{$hotel->country_name}}@endif</div>
                            <div class="row_info">
                                <div class="label">Số điện thoại:</div>
                                <div class="info_black">62-21-29921234 ---  -- update version next</div>
                            </div>
                            <?php }?>
                            <div style="font-size: 15px;" class="name_blue">Liên hệ khách sạn</div>
                        </div>
                        <div>
                            <button type="button" class="btn button_option">
                                Tùy chọn
                                <span data-icon="ant-design:caret-down-filled"
                                      style="color: #888888; width: 15px; height: 15px;margin-left: 10px;"
                                      class="iconify"></span>
                            </button>
                        </div>
                    </div>
                    <div class="box_checkin">
                        <div>
                            <div style="font-weight: bold; text-align: right;" class="name_green">Mã đặt phòng:
                                UJSYEH9 --- update version next</div>
                            <div
                                style="display: flex; align-items: center; justify-content: space-around; margin-top: 20px;">
                                <div>
                                    <div class="checkin">Check in </div>

                                    <?php
                                    $tach=explode('-',$detail_order_hotel->check_in_date);

                                    if($tach[1]=='01'){
                                        $thang='January';
                                    }
                                    if($tach[1]=='02'){
                                        $thang='February';
                                    }
                                    if($tach[1]=='03'){
                                        $thang='March';
                                    }
                                    if($tach[1]=='04'){
                                        $thang='April';
                                    }
                                    if($tach[1]=='05'){
                                        $thang='May';
                                    }
                                    if($tach[1]=='06'){
                                        $thang='June';
                                    }
                                    if($tach[1]=='07'){
                                        $thang='July';
                                    }
                                    if($tach[1]=='08'){
                                        $thang='August';
                                    }
                                    if($tach[1]=='09'){
                                        $thang='September';
                                    }
                                    if($tach[1]=='10'){
                                        $thang='October';
                                    }
                                    if($tach[1]=='11'){
                                        $thang='November';
                                    }
                                    if($tach[1]=='12'){
                                        $thang='December';
                                    }
                                    $tach2=explode(' ',$tach[2]);
                                    ?>
                                    <div class="date_checkin">{{$tach2[0]}} {{$thang}} {{$tach[0]}}</div>
                                </div>
                                <div>
                                    <?php
                                    $tach2_1=explode('-',$detail_order_hotel->check_out_date);

                                    if($tach2_1[1]=='01'){
                                        $thang='January';
                                    }
                                    if($tach2_1[1]=='02'){
                                        $thang='February';
                                    }
                                    if($tach2_1[1]=='03'){
                                        $thang='March';
                                    }
                                    if($tach2_1[1]=='04'){
                                        $thang='April';
                                    }
                                    if($tach2_1[1]=='05'){
                                        $thang='May';
                                    }
                                    if($tach2_1[1]=='06'){
                                        $thang='June';
                                    }
                                    if($tach2_1[1]=='07'){
                                        $thang='July';
                                    }
                                    if($tach2_1[1]=='08'){
                                        $thang='August';
                                    }
                                    if($tach2_1[1]=='09'){
                                        $thang='September';
                                    }
                                    if($tach2_1[1]=='10'){
                                        $thang='October';
                                    }
                                    if($tach2_1[1]=='11'){
                                        $thang='November';
                                    }
                                    if($tach2_1[1]=='12'){
                                        $thang='December';
                                    }
                                    $tach2_2=explode(' ',$tach2_1[2]);
                                    ?>
                                        <span style="width: 50px; height: 50px;" class="iconify"
                                              data-icon="akar-icons:arrow-right"></span>
                                    <div class="checkin">{{$tach2_2[0]-$tach2[0]}} đêm   </div>
                                </div>

                                <div>
                                    <div class="checkin">Check in</div>
                                    <div class="date_checkin">{{$tach2_2[0]}} {{$thang}} {{$tach2_1[0]}}</div>
                                </div>
                            </div>
                            <div
                                style="display: flex; align-items: center; justify-content: space-between; margin-top: 20px;">
                                <div class="note_checkin">Lỗi!! Xử lý đặt phòng lại trước 18:25:00 25/08/2021 -- -- update version next</div>
                                <button id="buttonCheckin" data-toggle="collapse" data-target="#collapseCheckin"
                                        aria-expanded="true" aria-controls="collapseCheckin" type="button"
                                        class="btn button_show_checkin">
                                        <span>
                                            Hiển
                                            thị chi tiết
                                        </span>
                                    <span id="iconCheckin" class="iconify" style="transform: rotate(0deg);"
                                          data-icon="akar-icons:chevron-down"></span>
                                </button>
                            </div>
                            <div id="collapseCheckin" class="collapse">
                                <div class="divider"></div>
                                <div class="box_info_checkin">
                                    <div class="info_checkin_blue">2 phòng  -- update version next - {{$order->adt}} người lớn, {{$order->chd}} trẻ em ,{{$order->inf}} trẻ sơ sinh</div>
                                    <div class="row_info">
                                        <div style="width: 50%;" class="box_info">
                                            <div class="label">Khách nhận phòng:</div>
                                            <div class="info_black">{{$detail_order_hotel->first_name}} {{$detail_order_hotel->last_name}}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="divider"></div>
                                <div class="box_info_checkin">
                                    <div class="info_checkin_blue">Chính sách hủy phòng -- update version next</div>
                                    <div class="policy">
                                        Free cancellation until 29 May 2021 18:00 (GMT +07:00).
                                        Cancellations made after 29 May 2021 18:00 (GMT +07:00) will result in a
                                        100% penalty of the stay charges and fees.
                                        If you fail to check-in for this reservation or cancel or change this
                                        reservation after check-in, you may incur penalty charges at the discretion
                                        of the property of up to 100% of the booking value.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="margin-top: 10px;" class="wrapper">
                <div style="padding: 10px" class="box_title">
                    <div data-toggle="collapse" data-target="#collapseAlternate" aria-expanded="true"
                         aria-controls="collapseAlternate" id="buttonAlternate" style="cursor: pointer;"
                         class="title">Lịch sử thay đổi -- update version next</div>
                    <span id="iconAlternate" data-icon="ant-design:caret-down-filled"
                          style="transform: rotate(0deg);" class="iconify"></span>
                </div>
                <div id="collapseAlternate" class="collapse">
                    <div class="divider"></div>
                    <div>
                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad
                        squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa
                        nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid
                        single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft
                        beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice
                        lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you
                        probably haven't heard of them accusamus labore sustainable VHS.
                    </div>
                </div>
            </div>
            <div style="margin-top: 10px;" class="wrapper">
                <div style="padding: 10px" class="box_title">
                    <div data-toggle="collapse" data-target="#collapseDeal" aria-expanded="true"
                         aria-controls="collapseDeal" id="buttonDeal" style="cursor: pointer;"
                         class="title">Lịch sử giao dịch --update version next</div>
                    <span id="iconDeal" data-icon="ant-design:caret-down-filled"
                          style="transform: rotate(0deg);" class="iconify"></span>
                </div>
                <div id="collapseDeal" class="collapse">
                    <div class="divider"></div>
                    <div>
                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad
                        squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa
                        nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid
                        single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft
                        beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice
                        lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you
                        probably haven't heard of them accusamus labore sustainable VHS.
                    </div>
                </div>
            </div>
        </div>
        <div class="box_right">
            <div style="padding: 10px;" class="wrapper">
                <div class="box_title">
                    <div style="padding: 5px" class="title">Thông tin giá phòng</div>
                </div>
                <div class="divider"></div>
            </div>
            <?php
            $room_plan_orders=DB::table('room_plan_orders')->where('hotel_order_detail_id',$detail_order_hotel->id)->first();
            if($room_plan_orders!=null){
            ?>
            <div class="box_room">
                <div class="box_info">
                    <div class="wrapper_name">
                        <div class="name_room">
{{$room_plan_orders->title}}           
             </div>
                        <div class="name_gray">Sức chứa: {{$room_plan_orders->quantity}} người </div>
                        <div class="name_green">Refundable  --  update version next </div>
                    </div>
                    <div class="wrapper_image">
                        <img class="image"
                             src="https://q-xx.bstatic.com/xdata/images/hotel/840x460/167534297.jpg?k=ca14cfb44b0a9e23dcf611a2326d73f40a97e568a62d468281f3b588c6aec5c5&o=" />
                        <div class="name_blue">Xem tiện nghi phòng -- update version next </div>
                    </div>
                </div>
                <div class="box_price">
                    <div class="box_info_price">
                        <div class="label">Giá phòng</div>
                        <div class="name_black">đ {{number_format($room_plan_orders->unit_price,0,',','.')}}</div>
                    </div>
                    <div class="box_info_price">
                        <div class="label">Thuế và phí</div>
                        <div class="name_black">đ 300,000 --update version next </div>
                    </div>
                    <div class="box_info_price">
                        <div class="label">Phụ thu</div>
                        <div class="name_black">đ 0 --update version next </div>
                    </div>
                </div>
                <div class="divider"></div>
                <div class="box_total_room">
                    <div class="name_black">Giá 1 phòng x 2 đêm</div>
                    <div class="name_black">đ {{number_format($room_plan_orders->unit_price *($tach2_2[0]-$tach2[0]),0,',','.') }}</div>
                </div>
            </div>
            <div style="padding: 10px;" class="wrapper">
                <div class="box_info_price">
                    <div class="name_black">KS khuyến mãi</div>
                    <div class="name_black">- đ 200,000 --update version next</div>
                </div>
                <div class="box_info_price">
                    <div class="name_black">Mã giảm giá --update version next</div>
                    <div class="name_black">- đ 400,000</div>
                </div>
                <div class="name_blue">(HOTSALE06)</div>
            </div>
            <div class="box_total">
                <div class="total">TỔNG GIÁ TRỊ ĐƠN HÀNG</div>
                <div class="total">đ {{number_format($room_plan_orders->unit_price *($tach2_2[0]-$tach2[0]),0,',','.') }}</div>
            </div>
            
          <?Php }else{?>
          Đang cập nhật dữ liệu...
          <?php
          }
          ?>
            <div class="box_activity_log">
                <div data-toggle="collapse" data-target="#collapseActivityLog" aria-expanded="true"
                     aria-controls="collapseActivityLog"
                     style="padding: 20px; display: flex; align-items: center; justify-content: space-between; cursor: pointer;">
                    <span class="activity_log">Nhật ký hoạt động (6) --update version next</span>
                    <span data-icon="ant-design:caret-down-filled"
                          style="color: #666666; width: 20px; height: 20px;" class="iconify"></span>
                </div>
                <div id="collapseActivityLog" class="collapse">
                    <div>
                        dsa</div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</body>

</html>
