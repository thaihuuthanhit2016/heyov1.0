<!DOCTYPE html>
<html lang="en">

<head>
    <title>HF Admin</title>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="{{asset('public/admin/css/manage-book.css')}}" />
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="https://code.iconify.design/2/2.2.1/iconify.min.js"></script>
    <script src="https://unpkg.com/@popperjs/core@2"></script>
    <link rel='stylesheet'
          href='https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css'>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js'></script>
    <script
        src='https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js'></script>
    <script></script>
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css"
          rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
    <script src="{{asset('public/admin/js/main.js')}}"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <style>
        .toggle.ios,
        .toggle-on.ios,
        .toggle-off.ios {
            border-radius: 20rem;
        }

        .toggle.ios .toggle-handle {
            border-radius: 20rem;
        }
    </style>
    <script>
        function test(value) {
            alert(typeof value)
        }
        let arrtotal=['code_order','name_customer','create_fromto','phone_customer','name_hotel','location','nhanvienphutrach','statuspayment','partner','revice_fromto'];
        let arr = ["code_order","name_customer","statuspayment"];
        function handleChooseCriteria(value,id){
            if($("#"+id).is(':checked')){
                arr.push(value)
            }else{
                const data = arr.filter((item) => {
                    return item !== value
                })
                arr = data;
            }
        }
        function showTextSearch(sessionArrSearch){
            if(!window.sessionStorage.getItem(sessionArrSearch)){
                arrtotal.forEach(function(value){
                    if(arr.filter((item) => {
                        return item === value
                    }).length > 0){
                        $("#"+value).removeClass('none');
                    }else{
                        $("#"+value).addClass('none');
                    }
                })
            }else{
                const arrNew = JSON.parse(window.sessionStorage.getItem(sessionArrSearch));
                arrtotal.forEach(function(value){
                    if(arrNew.filter((item) => {
                        return item === value
                    }).length > 0){
                        $("#"+value).removeClass('none');
                        if($("#"+value+"_checkbox").is(':checked')){
                            $("#"+value+"_checkbox").click();
                        }
                        $("#"+value+"_checkbox").click();
                    }else{
                        $("#"+value).addClass('none');
                    }
                })
            }
            // arrtotal.forEach(function(value){
            //     if(arr.filter((item) => {
            //         return item === value
            //     }).length > 0){
            //         $("#"+value).removeClass('none');
            //     }else{
            //         $("#"+value).addClass('none');
            //     }
            // })
        }
        setTimeout(() => {
            showTextSearch('arr_search_hotel')
        }, 50)
        $(document).ready(function () {
            $("#search").click(function () {
                if(document.referrer !== "https://test.heyo.group/admin/hotel/order/all/1"){
                     window.sessionStorage.setItem('arr_search_hotel', JSON.stringify(arr))
                    if(!$('#date_start_end_checkbox').is(':checked')){
                        $("#datetimepicker1").remove();
                        $("#datetimepicker2").remove();

                    }
              if(!$('#revice_fromto_checkbox').is(':checked')){
                        $("#datetimepicker3").remove();
                  $("#datetimepicker4").remove();

                    }
                      setTimeout(() => {
                         let newSession = window.sessionStorage.getItem('temp_search');
                         if(newSession)
                             window.sessionStorage.setItem('temp_search', newSession + "1")
                         else
                             window.sessionStorage.setItem('temp_search', "1")
                        $("#searchForm").submit();
                     }, 500)
                }

                // setTimeout(() => {
                //     alert('ok')
                //
                // },1500)
                // window.sessionStorage.setItem('arr_search_hotel', JSON.stringify(arr))
            })
            $("#button_done_criteria").click(function () {
                arrtotal.forEach(function(value){
                    if(arr.filter((item) => {
                        return item === value
                    }).length > 0){
                        $("#"+value).removeClass('none');
                    }else{
                        $("#"+value).addClass('none');
                    }
                })
            });
            $("#buttonWaiting").click();
            $("#buttonAccepted").click();
            $("#buttonCancelled").click();
            $("#buttonDone").click();
        })
    </script>
</head>

<body>
<div class="menu_header">
    <div class="left">
        <a href="#" class="logo">HEYO TRIP</a>
    </div>
    <div class="right">
        <span class="name_menu"> Quản lý đơn đặt phòng </span>
        <div class="box_admin">
                <span class="iconify" style="width: 25px; height: 25px; color: #032044"
                      data-icon="ic:baseline-notifications-none"></span>
            <div style="color: white;background-color: red" class="badge badge-danger counter">9</div>
            <div id="btn_admin" class="wrapper_admin">
                    <span style="width: 25px; height: 25px; color: #032044" class="iconify"
                          data-icon="lucide:user"></span>
                <span class="admin">Admin</span>
                <span style="
                width: 15px;
                height: 15px;
                color: #032044;
                margin-left: 10px;
              " class="iconify" data-icon="ant-design:caret-down-filled"></span>
            </div>
        </div>
    </div>
    <div id="profile" style="box-shadow: 10;" class="box_profile">
            <span style="width: 30px; height: 30px;color: #032044;" class="iconify"
                  data-icon="simple-line-icons:logout"></span>
        <span class="logout">Đăng xuất</span>
    </div>
</div>
<div style="display: flex">
    <div class="menu_left">
        <div class="top">
            <a href="{{asset('admin/hotel/order/all/1')}}" style="background-color: #f2f9ff; text-decoration: none" class="item">
                    <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                          data-icon="icon-park-outline:hotel"></span>
                <span style="color: #526a87" class="name_menu">Khách sạn</span>
            </a>
            <a href="{{asset('admin/flight/order/all/1')}}" style=" text-decoration: none" class="item">
                <span class="iconify" style="color: #526a87; width: 30px; height: 30px" data-icon="ps:plane"></span>
                <span  style="color: #526a87"class="name_menu">Vé máy bay</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
                    <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                          data-icon="clarity:car-line"></span>
                <span style="color: #526a87" class="name_menu">Thuê xe</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
                    <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                          data-icon="clarity:data-cluster-line"></span>
                <span style="color: #526a87" class="name_menu">Crypto</span>
            </a>
        </div>
        <div class="top">
            <a href="#" style="text-decoration: none" class="item">
                    <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                          data-icon="ep:user-filled"></span>
                <span style="color: #526a87" class="name_menu">Tài khoản</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
                    <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                          data-icon="dashicons:welcome-widgets-menus"></span>
                <span style="color: #526a87" class="name_menu">Danh mục</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
                    <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                          data-icon="gg:website"></span>
                <span style="color: #526a87" class="name_menu">Website</span>
            </a>
            <a href="{{asset('admin/website/list/quocgia')}}" style="text-decoration: none" class="item">
                    <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                          data-icon="material-symbols:settings-suggest-outline-sharp"></span>
                <span style="color: #526a87" class="name_menu">Cấu hình</span>
            </a>
        </div>
    </div>
    <div class="menu_right">
        <a href="#" class="item">
                <span style="width: 25px; height: 25px; color: #032044" class="iconify"
                      data-icon="majesticons:analytics"></span>
            <span class="name_menu">Dashboard</span>
        </a>
        <a  href="{{asset('admin/hotel/searchHotel')}}"class="item">
            <span style="width: 25px; height: 25px; color: #032044" class="iconify" data-icon="mdi:bed"></span>
            <span class="name_menu">Tìm - Đặt phòng</span>
        </a>
        <a href="{{asset('admin/hotel/order/all/1')}}" class="item">
                <span style="width: 25px; height: 25px; color: #329223" class="iconify"
                      data-icon="fa-solid:calendar-day"></span>
            <span class="name_menu" style="color: #329223; font-weight: 800">Quản lý đơn đặt phòng</span>
        </a>
        <a href="#" href="#" class="item">
                <span style="width: 25px; height: 25px; color: #032044" class="iconify"
                      data-icon="fa-solid:money-check-alt"></span>
            <span class="name_menu">Tuỳ chỉnh giá phòng</span>
        </a>
        <a href="{{asset('admin/hotel/list')}}" class="item">
                <span style="width: 25px; height: 25px; color: #032044" class="iconify"
                      data-icon="icomoon-free:price-tags"></span>
            <span class="name_menu">Chương trình khuyến mãi</span>
        </a>
        <a href="#" class="item">
                <span style="width: 25px; height: 25px; color: #032044" class="iconify"
                      data-icon="eva:percent-fill"></span>
            <span class="name_menu">Quản lý hoa hồng</span>
        </a>
        <a href="#" class="item">
                <span style="width: 25px; height: 25px; color: #032044" class="iconify"
                      data-icon="fa6-solid:hand-holding-dollar"></span>
            <span class="name_menu">Huỷ - Hoàn tiền</span>
        </a>
        <a href="#" class="item">
                <span style="width: 25px; height: 25px; color: #032044" class="iconify"
                      data-icon="fluent:news-20-filled"></span>
            <span class="name_menu">Bài viết Marketing</span>
        </a>
    </div>
    <?Php

    $order_hotel=DB::table('hotel_order_details')->join('orders','orders.id','hotel_order_details.order_id')->where('orders.order_status_id',1)->get();
    $i=0;
    $j=0;
    $k=0;
    $l=0;
    foreach ($order_hotel as $h){
        $o=DB::table('orders')->where('id',$h->order_id)->first();

        if($o->order_status_id==1){
            $i++;

        }
        if($o->order_status_id==2){
            $j++;
        }
        if($o->order_status_id==4){
            $k++;
        }
        if($o->order_status_id==5){
            $l++;
        }

    }


    $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

    $tach=explode('/',$actual_link);


    ?>
    <div class="box_content">
        <div class="box_option">
            <a href="{{asset('admin/hotel/order/all/1')}}" id="button_tab_all" class="btn option @if($tach[6]=='all') tab_active @endif ">
                Tất cả ({{count($order_hotel_all)}})
            </a>
            <a href="{{asset('admin/hotel/order/waiting/1')}}" id="button_tab_waiting" class="btn option @if($tach[6]=='waiting') tab_active @endif">Chờ xử lý <span
                    style="color: red">({{count($order_hotel_by_5_all)}})</span></a>
            <a href="{{asset('admin/hotel/order/accepted/1')}}" id="button_tab_accepted" class="btn option @if($tach[6]=='accepted') tab_active @endif">Đã xác nhận ({{count($order_hotel_accept_all)}})</a>
            <a href="{{asset('admin/hotel/order/cancel/1')}}" id="button_tab_canceled" class="btn option @if($tach[6]=='cancel') tab_active @endif">Đã hủy ({{count($order_hotel_cancel_all)}})</a>
            <a href="{{asset('admin/hotel/order/done/1')}}" id="button_tab_done" class="btn option @if($tach[6]=='done') tab_active @endif">Hoàn thành ({{count($order_hotel_doen_all)}})</a>
        </div>
        <div class="wrapper">
            <form id="searchForm" class="box_search" method="post">
                <div class="dropdown">
                    <button type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false" class="btn button_criteria">
                        <span>Tiêu chí đánh giá</span>
                        <span style="margin-left: 10px;" class="iconify"
                              data-icon="ant-design:caret-down-outlined"></span>
                    </button>
                    <div style="padding: 10px; top: 35px;" class="dropdown-menu box_choose_criteria"
                         aria-labelledby="dropdownMenuButton">
                        <div class="choose_criteria">
                            <input type="checkbox"onclick="handleChooseCriteria(value,id)" value="code_order" id="code_order_checkbox" value="1" checked />
                            <span class="name_criteria">Mã đơn hàng</span>
                        </div>
                        <div class="choose_criteria">
                            <input type="checkbox"onclick="handleChooseCriteria(value,id)" value="create_fromto"  id="date_start_end_checkbox" value="2"/>
                            <span class="name_criteria">Ngày tạo từ - đến</span>
                        </div>
                        <div class="choose_criteria">
                            <input type="checkbox"onclick="handleChooseCriteria(value,id)" value="name_customer" id="name_customer_checkbox" value="3" checked />
                            <span class="name_criteria">Tên khách</span>
                        </div>
                        <div class="choose_criteria">
                            <input type="checkbox" onclick="handleChooseCriteria(value,id)" value="phone_customer"value="4" id="phone_customer_checkbox"/>
                            <span class="name_criteria">Sđt khách</span>
                        </div>
                        <div class="choose_criteria">
                            <input type="checkbox"onclick="handleChooseCriteria(value,id)" value="name_hotel" value="5" id="name_hotel_checkbox" />
                            <span class="name_criteria">Tên khách sạn</span>
                        </div>
                        <div class="choose_criteria">
                            <input type="checkbox" onclick="handleChooseCriteria(value,id)" value="location"  id="location_checkbox" />
                            <span class="name_criteria">Điểm đến</span>
                        </div>
                        <div class="choose_criteria">
                            <input type="checkbox"onclick="handleChooseCriteria(value,id)" value="revice_fromto" id="revice_fromto_checkbox" />
                            <span class="name_criteria">Ngày nhận phòng từ - đến</span>
                        </div>
                        <div class="choose_criteria">
                            <input type="checkbox"  onclick="handleChooseCriteria(value,id)" value="partner" id="partner_checkbox"/>
                            <span class="name_criteria">Đối tác</span>
                        </div>
                        <div class="choose_criteria">
                            <input type="checkbox" onclick="handleChooseCriteria(value,id)" value="nhanvienphutrach" id="nhanvienphutrach_checkbox"/>
                            <span class="name_criteria">Nhân viên phụ trách</span>
                        </div>
                        <div class="choose_criteria">
                            <input onclick="handleChooseCriteria(value,id)" type="checkbox" value="statuspayment" id="statuspayment_checkbox"checked />
                            <span class="name_criteria">Trạng thái thanh toán</span>
                        </div>
                        <div class="box_option_criteria">
                            <button type="button" class="btn dropdown-item">Chọn lại</button>
                            <button class="btn dropdown-item" id='button_done_criteria'type="button">Xong</button>
                        </div>
                    </div>
                </div>
                <div class="label_criteria">Hãy chọn tiêu chí để tìm kiếm thông tin đơn hàng. Bạn có thể chọn
                    nhiều
                    tiêu chí cùng lúc.</div>
                <div class="row box_input_search">
                    <div class="col col-md-6 col-lg-4 col-xl-3" id="code_order">
                        <input placeholder="Mã đơn hàng"  value="{{Session::get('code_order')}}" name="code_order" class="input_search" />
                    </div>
                    <div class="col col-md-6 col-lg-4 col-xl-3" id="name_customer">
                        <input placeholder="Tên khách" value="{{Session::get('name_customer')}}" name="name_customer" class="input_search" />
                    </div>
                    <div class="col col-md-6 col-lg-4 col-xl-3" id="phone_customer">
                        <input placeholder="SĐT khách" name="phone_customer"value="{{Session::get('phone_customer')}}" class="input_search" />
                    </div>
                    <div class="col col-md-6 col-lg-4 col-xl-3" id="name_hotel">
                        <input type="text" name="name_hotel" id="hotel_serach" value="{{Session::get('name_hotel')}}"class="input_search" placeholder="Tên khách sạn">
                        <div id="hotel_serach_ajax"></div>

                    </div>
                    <div id="revice_fromto" class="col col-md-6 col-lg-4 col-xl-3">
                        <div class="col col-md-6 col-lg-6 col-xl-6" >
                            <input placeholder="Ngày nhận từ" name="revice_from" value="{{Session::get('revice_from')}}" title="Ngày nhận từ" id="datetimepicker3" class="input_search" />
                        </div>
                        <div class="col col-md-6 col-lg-6 col-xl-6" >
                            <input placeholder="Ngày nhận đến" name="revice_to" value="{{Session::get('revice_to')}}"id="datetimepicker4"title="Ngày nhận đến" class="input_search" />
                        </div>
                    </div>
                    <div class="col col-md-6 col-lg-4 col-xl-3" id="create_fromto">
                        <div class="col col-md-6 col-lg-6 col-xl-6" >
                            <input placeholder="Ngày tạo từ" name="create_from" title="Ngày tạo từ" id="datetimepicker1_search"value="{{Session::get('create_from')}}" class="input_search" /></div>
                        <div class="col col-md-6 col-lg-6 col-xl-6" >
                            <input placeholder="Ngày tạo đến" name="create_to" title="Ngày tạo đến" id="datetimepicker2_search" value="{{Session::get('create_to')}}"class="input_search" />
                        </div>
                    </div>
                    <div class="col col-md-6 col-lg-4 col-xl-3" id="location" >
                        <input type="text" name="locations" value="{{Session::get('locations')}}" id="locations_serach" class="input_search" placeholder="Điểm đến">
                        <div id="location_serach_ajax"></div>

                    </div>

                    <div class="col col-md-6 col-lg-4 col-xl-3" id="partner">
                        <select name="partner" style="height: 35px; width: 100%;  border: 1px solid #cccccc; border-radius: 2px; padding: 0px 10px;
                                font-family: 'Montserrat';
                                font-style: normal;
                                font-weight: 400;
                                font-size: 14px;
                                line-height: 20px;
                                margin-bottom: 10px;
                                color: #666666" class="select_criteria">
                            <option value="" >Đối tác</option>
                            <option value="1" @if(Session::get('partner')=='TVP') selected @endif>TVP</option>
                            <option value="2" @if(Session::get('partner')=='EZC') selected @endif >EZC</option>
                            <option value="4" @if(Session::get('partner')=='VJ') selected @endif >VJ</option>
                            <option value="3" @if(Session::get('partner')=='HPL') selected @endif >HPL</option>
                        </select>
                    </div>
                    <div class="col col-md-6 col-lg-4 col-xl-3" id="nhanvienphutrach">
                        <select name="nhanvienphutrach" style="height: 35px; width: 100%;  border: 1px solid #cccccc; border-radius: 2px; padding: 0px 10px;
                                font-family: 'Montserrat';
                                font-style: normal;
                                font-weight: 400;
                                font-size: 14px;
                                line-height: 20px;
                                margin-bottom: 10px;
                                color: #666666" class="select_criteria">
                            <option value="" >Nhân viên phụ trách</option>
                            <?php
                            $user=DB::table('tbl_admin')->get();
                            ?>
                            @foreach($user as $u)
                                <option value="" @if(Session::get('nhanvienphutrach')==$u->admin_name) selected @endif >{{$u->admin_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col col-md-6 col-lg-4 col-xl-3" id="statuspayment">
                        <select name="statuspayment" style="height: 35px; width: 100%;  border: 1px solid #cccccc; border-radius: 2px; padding: 0px 10px;
                                font-family: 'Montserrat';
                                font-style: normal;
                                font-weight: 400;
                                font-size: 14px;
                                line-height: 20px;
                                margin-bottom: 10px;
                                color: #666666" class="select_criteria">
                            <option value="" >Trạng thái thanh toán</option>
                            <option value="1" @if(Session::get('statuspayment')==1) selected @endif >Đã Thanh toán</option>
                            <option value="0"@if(Session::get('statuspayment')==0) selected @endif >Chưa Thanh toán</option>
                            <option value="2" @if(Session::get('statuspayment')==2) selected @endif>Thanh Toán Thất Bại</option>
                            <option value="3"@if(Session::get('statuspayment')==3) selected @endif >Hết Hạn Thanh Toán</option>
                            <option value="4"@if(Session::get('statuspayment')==4) selected @endif >Đang Hoàn Huỷ</option>
                            <option value="5" @if(Session::get('statuspayment')==5) selected @endif>Đã Hoàn Tiền</option>
                        </select>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col col-md-6 col-lg-4 col-xl-3 box_button_search">
                        <button id="search" type="button" style="text-decoration: none;border: 0px;" class="button_search">
                                    <span style="width: 20px; height: 20px; color: #fff;" class="iconify"
                                          data-icon="fluent:search-24-filled"></span>
                            <span class="name_button_search">Tìm kiếm</span>
                        </button>
                        <button type="button" class="button_type_again">
                            Nhập lại
                        </button>
                    </div>
                </div>
            </form>
            <div class="box_title">
                <span class="title_small">Đơn hàng</span>
                <span class="title_small">Người liên hệ</span>
                <span class="title_big">Khách sạn</span>
                <span class="title_small">Ngày nhận phòng</span>
                <span class="title_small">Thanh toán</span>
                <span class="title_small">Đối tác</span>
                <span class="title_small">Phụ trách</span>
                <span style="width: 6%;"> </span>
            </div>
        </div>

        <script>
            $(document).ready(function () {
                $(".button_type_again").click(function () {
                    $('.select_criteria').val('');
                    $('.input_search').val('');
                });
            })
        </script>
        <div id="all_order" class="fade-in hide">
            <div class="box_button_collapse">
                <div id="buttonWaiting" class="button_collapse" data-toggle="collapse"
                     data-target="#collapseWaiting" aria-expanded="true" aria-controls="collapseWaiting">
                    Chờ xử lý (@if(count($order_hotel_by_5_all)>0){{count($order_hotel_by_5_all)}} @else 0 @endif)
                </div>
                <div style="width: 90%; background-color: #D5E2EA;height: 1px;margin-right: 20px;"></div>
                <span id="iconWaiting" style="color: #888888; cursor: pointer; transform: rotate(0deg);"
                      class="iconify" data-icon="ant-design:caret-down-outlined" data-toggle="collapse"
                      data-target="#collapseWaiting" aria-expanded="true" aria-controls="collapseWaiting"></span>
            </div>
            <div class="collapse" id="collapseWaiting">
                @if(count($order_hotel_by_get_5)>0)
                    <div class="collapse_body">
                        <?php
                            $i=0;
                        ?>
                        @foreach ($order_hotel_by_get_5 as $h)
                            <?php
$i++;
                            $o=DB::table('orders')->where('id',$h->order_id)->first();
                            if ($o!=null){
                            $hotel=DB::table('hotels')->where('id',$h->hotel_id)->first();
                            if ($hotel!=null){
                            $countries=DB::table('locations')->where('id',$hotel->country_id)->first();

                            if($o->order_status_id==1){


                            ?>
                            <div class="box_information">
                                <div class="info_small">
                                    <div class="text_blue">{{$o->order_code}}</div>
                                    <div class="text_gray">{{$h->created_at}}</div>
                                    <div style="display: flex;align-items: center; justify-content: center;">
                                        <div class="box_info_book">@if($o->order_status_id==1) Chờ xử lý @endif</div>
                                    </div>
                                </div>
                                <div class="info_small">
                                    <div class="text_black_bold">{{$h->first_name}} {{$h->last_name}}</div>
                                    <div class="text_gray">{{$h->phone_number}}</div>
                                </div>
                                <div class="info_big">
                                    <div class="text_black_normal">{{$hotel->hotel_name}}</div>
                                </div>
                                <div class="info_small">
                                    <div class="text_black_normal">{{$h->check_in_date}}</div>
                                    <div class="text_gray">{{$countries->name_vi}}</div>
                                </div>
                                <div class="info_small">
                                    <div class="text_black_bold">{{number_format($o->total_price,0,'.','.') }} đ</div>
                                    <div class="text_green">@if($h->payment_status_id==0) Chưa thanh toán @elseif($h->payment_status_id==1) Đã thanh toán @elseif($h->payment_status_id==2) Thanh toán thất bại @elseif($h->payment_status_id==3)Hết hạn thanh toán @elseif($h->payment_status_id==4)Đang hoàn hủy @else Đã hoàn tiền @endif</div>
                                </div>
                                <div class="info_small">
                                    <div class="text_black_normal">@if($o->vendor_id==1)TPV @elseif($o->vendor_id==2) EZC @elseif($o->vendor_id) HPL @else VJ @endif</div>
                                </div>
                                <div class="info_small">
                                    <div class="text_black_normal">CS01 - Hương</div>
                                    <div class="text_red">Chờ xử lý</div>
                                </div>
                                <div class="info_small" style="width: 6%;">
                                    <button id="open_popover{{$i}}" type="button"
                                            style="background-color: transparent;border-radius: 100px;" class="btn"><span
                                            style="width: 25px;height: 25px; color: #C1C1C1" class="iconify"
                                            data-icon="entypo:dots-three-horizontal"></span></button>
                                    <div id="box_popover_id{{$i}}" class="box_popover">
                                        <a href="{{asset('admin/hotel/order/detail/'.$o->id)}}" class="btn item_popover">Chi tiết</a>
                                        <a href="#" class="btn item_popover">Đặt phòng lại</a>
                                        <a href="#" class="btn item_popover">Xác nhận đã thanh toán</a>
                                        <a href="#" class="btn item_popover">Huỷ đặt phòng</a>
                                    </div>

                                </div>
                            </div>
                            <div style="margin: 0px" class="dropdown-divider"></div>
                            <?php
                            }
                            }
                            }?>
                        @endforeach
                    </div>
                @else
                    <div class="nodata">
                        <span class="iconify" data-icon="fa6-regular:folder-open"></span>
                        <div>
                            Không có dữ liệu !!!
                        </div>
                    </div>
                @endif
            </div>
            <div class="box_button_collapse">
                <div id="buttonAccepted" class="button_collapse" data-toggle="collapse"
                     data-target="#collapseAccepted" aria-expanded="true" aria-controls="collapseAccepted">
                    Đã xác nhận (@if(count($order_hotel_accept_all)>0) {{count($order_hotel_accept_all)}} @else 0 @endif)
                </div>
                <div style="width: 90%; background-color: #D5E2EA;height: 1px;margin-right: 20px;"></div>
                <span id="iconAccepted" style="color: #888888; cursor: pointer; transform: rotate(0deg);"
                      class="iconify" data-icon="ant-design:caret-down-outlined" data-toggle="collapse"
                      data-target="#collapseAccepted" aria-expanded="true" aria-controls="collapseAccepted"></span>
            </div>
            <div class="collapse" id="collapseAccepted">
                <div class="collapse_body">
                    @if(count($order_hotel_accept_get_5)>0)
<?php $i=0;?>
                        <div class="collapse_body">
                            @foreach ($order_hotel_accept_get_5 as $h)

                                <?php
                                $i++;
                                $o=DB::table('orders')->where('id',$h->order_id)->first();
                                if ($o!=null){
                                $hotel=DB::table('hotels')->where('id',$h->hotel_id)->first();
                                if ($hotel!=null){
                                $countries=DB::table('locations')->where('id',$hotel->country_id)->first();

                                if($o->order_status_id==2){


                                ?>
                                <div class="box_information">
                                    <div class="info_small">
                                        <div class="text_blue">{{$o->order_code}}</div>
                                        <div class="text_gray">{{$h->created_at}}</div>
                                        <div style="display: flex;align-items: center; justify-content: center;">
                                            <div class="box_info_book_accept" >@if($o->order_status_id==2) Đã Xác nhận @endif</div>
                                        </div>
                                    </div>
                                    <div class="info_small">
                                        <div class="text_black_bold">{{$h->first_name}} {{$h->last_name}}</div>
                                        <div class="text_gray">{{$h->phone_number}}</div>
                                    </div>
                                    <div class="info_big">
                                        <div class="text_black_normal">{{$hotel->hotel_name}}</div>
                                    </div>
                                    <div class="info_small">
                                        <div class="text_black_normal">{{$h->check_in_date}}</div>
                                        <div class="text_gray">{{$countries->name_vi}}</div>
                                    </div>
                                    <div class="info_small">
                                        <div class="text_black_bold">{{number_format($o->total_price,0,'.','.') }} đ</div>
                                        <div class="text_green">@if($h->payment_status_id==0) Chưa thanh toán @elseif($h->payment_status_id==1) Đã thanh toán @elseif($h->payment_status_id==2) Thanh toán thất bại @elseif($h->payment_status_id==3)Hết hạn thanh toán @elseif($h->payment_status_id==4)Đang hoàn hủy @else Đã hoàn tiền @endif</div>
                                    </div>
                                    <div class="info_small">
                                        <div class="text_black_normal">@if($o->vendor_id==1)TPV @elseif($o->vendor_id==2) EZC @elseif($o->vendor_id) HPL @else VJ @endif</div>
                                    </div>
                                    <div class="info_small">
                                        <div class="text_black_normal">CS01 - Hương</div>
                                        <div class="text_red">Chờ xử lý</div>
                                    </div>
                                    <div class="info_small" style="width: 6%;">
                                        <button id="open_popover_accepted{{$i}}" type="button"
                                                style="background-color: transparent;border-radius: 100px;" class="btn"><span
                                                style="width: 25px;height: 25px; color: #C1C1C1" class="iconify"
                                                data-icon="entypo:dots-three-horizontal"></span></button>
                                        <div id="box_popover_id_accepted{{$i}}" class="box_popover">
                                        <a href="{{asset('admin/hotel/order/detail/'.$o->id)}}" class="btn item_popover">Chi tiết</a>

                                            <a href="#" class="btn item_popover">Đặt phòng lại</a>
                                            <a href="#" class="btn item_popover">Xác nhận đã thanh toán</a>
                                            <a href="#" class="btn item_popover">Huỷ đặt phòng</a>
                                        </div>

                                    </div>
                                </div>
                                <div style="margin: 0px" class="dropdown-divider"></div>
                                <?php
                                }
                                }
                                }?>
                            @endforeach
                        </div>
                    @else
                        <div class="nodata">
                            <span class="iconify" data-icon="fa6-regular:folder-open"></span>
                            <div>
                                Không có dữ liệu !!!
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            <div class="box_button_collapse">
                <div id="buttonCancelled" class="button_collapse" data-toggle="collapse"
                     data-target="#collapseCancelled" aria-expanded="true" aria-controls="collapseCancelled">
                    Đã huỷ (@if(count($order_hotel_cancel_all)>0) {{count($order_hotel_cancel_all)}} @else 0 @endif )
                </div>
                <div style="width: 90%; background-color: #D5E2EA;height: 1px;margin-right: 20px;"></div>
                <span id="iconCancelled" style="color: #888888; cursor: pointer; transform: rotate(0deg);"
                      class="iconify" data-icon="ant-design:caret-down-outlined" data-toggle="collapse"
                      data-target="#collapseCancelled" aria-expanded="true" aria-controls="collapseCancelled"></span>
            </div>
            <div class="collapse" id="collapseCancelled">

                @if(count($order_hotel_cancel_get_5)>0)
                    <?php $i=0;?>
                    <div class="collapse_body">
                        @foreach ($order_hotel_cancel_get_5 as $h)
                            <?php
                            $i++;
                            $o=DB::table('orders')->where('id',$h->order_id)->first();
                            if ($o!=null){
                            $hotel=DB::table('hotels')->where('id',$h->hotel_id)->first();
                            if ($hotel!=null){
                            $countries=DB::table('locations')->where('id',$hotel->country_id)->first();

                            if($o->order_status_id==4){


                            ?>
                            <div class="box_information">
                                <div class="info_small">
                                    <div class="text_blue">{{$o->order_code}}</div>
                                    <div class="text_gray">{{$h->created_at}}</div>
                                    <div style="display: flex;align-items: center; justify-content: center;">
                                        <div class="box_info_book">@if($o->order_status_id==4) Đã hủy @endif</div>
                                    </div>
                                </div>
                                <div class="info_small">
                                    <div class="text_black_bold">{{$h->first_name}} {{$h->last_name}}</div>
                                    <div class="text_gray">{{$h->phone_number}}</div>
                                </div>
                                <div class="info_big">
                                    <div class="text_black_normal">{{$hotel->hotel_name}}</div>
                                </div>
                                <div class="info_small">
                                    <div class="text_black_normal">{{$h->check_in_date}}</div>
                                    <div class="text_gray">{{$countries->name_vi}}</div>
                                </div>
                                <div class="info_small">
                                    <div class="text_black_bold">{{number_format($o->total_price,0,'.','.') }} đ</div>
                                    <div class="text_green">@if($h->payment_status_id==0) Chưa thanh toán @elseif($h->payment_status_id==1) Đã thanh toán @elseif($h->payment_status_id==2) Thanh toán thất bại @elseif($h->payment_status_id==3)Hết hạn thanh toán @elseif($h->payment_status_id==4)Đang hoàn hủy @else Đã hoàn tiền @endif</div>
                                </div>
                                <div class="info_small">
                                    <div class="text_black_normal">@if($o->vendor_id==1)TPV @elseif($o->vendor_id==2) EZC @elseif($o->vendor_id) HPL @else VJ @endif</div>
                                </div>
                                <div class="info_small">
                                    <div class="text_black_normal">CS01 - Hương</div>
                                    <div class="text_red">Chờ xử lý</div>
                                </div>
                                <div class="info_small" style="width: 6%;">
                                    <button id="open_popover_cancle{{$i}}" type="button"
                                            style="background-color: transparent;border-radius: 100px;" class="btn"><span
                                            style="width: 25px;height: 25px; color: #C1C1C1" class="iconify"
                                            data-icon="entypo:dots-three-horizontal"></span></button>
                                    <div id="box_popover_id_cancle{{$i}}" class="box_popover">
                                    <a href="{{asset('admin/hotel/order/detail/'.$o->id)}}" class="btn item_popover">Chi tiết</a>

                                        <a href="#" class="btn item_popover">Đặt phòng lại</a>
                                        <a href="#" class="btn item_popover">Xác nhận đã thanh toán</a>
                                        <a href="#" class="btn item_popover">Huỷ đặt phòng</a>
                                    </div>

                                </div>
                            </div>
                            <div style="margin: 0px" class="dropdown-divider"></div>
                            <?php
                            }
                            }
                            }?>
                        @endforeach
                    </div>
                @else
                    <div class="nodata">
                        <span class="iconify" data-icon="fa6-regular:folder-open"></span>
                        <div>
                            Không có dữ liệu !!!
                        </div>
                    </div>
                @endif
            </div>
            <div class="box_button_collapse">
                <div id="buttonDone" class="button_collapse" data-toggle="collapse" data-target="#collapseDone"
                     aria-expanded="true" aria-controls="collapseDone">
                    Đã hoàn thành (@if(count($order_hotel_doen_all)>0) {{count($order_hotel_doen_all)}} @else 0 @endif )
                </div>
                <div style="width: 90%; background-color: #D5E2EA;height: 1px;margin-right: 20px;"></div>
                <span id="iconDone" style="color: #888888; cursor: pointer; transform: rotate(0deg);"
                      class="iconify" data-icon="ant-design:caret-down-outlined" data-toggle="collapse"
                      data-target="#collapseDone" aria-expanded="true" aria-controls="collapseDone"></span>
            </div>
            <div class="collapse" id="collapseDone">
                @if(count($order_hotel_doen_get_5)>0)
                    <?Php $i=0?>
                    <div class="collapse_body">
                        @foreach ($order_hotel_doen_get_5 as $h)
                            <?php
                            $i++;
                            $o=DB::table('orders')->where('id',$h->order_id)->first();
                            if ($o!=null){
                            $hotel=DB::table('hotels')->where('id',$h->hotel_id)->first();
                            if ($hotel!=null){
                            $countries=DB::table('locations')->where('id',$hotel->country_id)->first();

                            if($o->order_status_id==5){


                            ?>
                            <div class="box_information">
                                <div class="info_small">
                                    <div class="text_blue">{{$o->order_code}}</div>
                                    <div class="text_gray">{{$h->created_at}}</div>
                                    <div style="display: flex;align-items: center; justify-content: center;">
                                        <div class="box_info_book">@if($o->order_status_id==5) Đã hoàn thành @endif</div>
                                    </div>
                                </div>
                                <div class="info_small">
                                    <div class="text_black_bold">{{$h->first_name}} {{$h->last_name}}</div>
                                    <div class="text_gray">{{$h->phone_number}}</div>
                                </div>
                                <div class="info_big">
                                    <div class="text_black_normal">{{$hotel->hotel_name}}</div>
                                </div>
                                <div class="info_small">
                                    <div class="text_black_normal">{{$h->check_in_date}}</div>
                                    <div class="text_gray">{{$countries->name_vi}}</div>
                                </div>
                                <div class="info_small">
                                    <div class="text_black_bold">{{number_format($o->total_price,0,'.','.') }} đ</div>
                                    <div class="text_green">@if($h->payment_status_id==0) Chưa thanh toán @elseif($h->payment_status_id==1) Đã thanh toán @elseif($h->payment_status_id==2) Thanh toán thất bại @elseif($h->payment_status_id==3)Hết hạn thanh toán @elseif($h->payment_status_id==4)Đang hoàn hủy @else Đã hoàn tiền @endif</div>
                                </div>
                                <div class="info_small">
                                    <div class="text_black_normal">@if($o->vendor_id==1)TPV @elseif($o->vendor_id==2) EZC @elseif($o->vendor_id) HPL @else VJ @endif</div>
                                </div>
                                <div class="info_small">
                                    <div class="text_black_normal">CS01 - Hương</div>
                                    <div class="text_red">Chờ xử lý</div>
                                </div>
                                <div class="info_small" style="width: 6%;">
                                    <button id="open_popover_done{{$i}}" type="button"
                                            style="background-color: transparent;border-radius: 100px;" class="btn"><span
                                            style="width: 25px;height: 25px; color: #C1C1C1" class="iconify"
                                            data-icon="entypo:dots-three-horizontal"></span></button>
                                    <div id="box_popover_id_done{{$i}}" class="box_popover">                 
                                                               <a href="{{asset('admin/hotel/order/detail/'.$o->id)}}" class="btn item_popover">Chi tiết</a>

                                        <a href="#" class="btn item_popover">Đặt phòng lại</a>
                                        <a href="#" class="btn item_popover">Xác nhận đã thanh toán</a>
                                        <a href="#" class="btn item_popover">Huỷ đặt phòng</a>
                                    </div>

                                </div>
                            </div>
                            <div style="margin: 0px" class="dropdown-divider"></div>
                            <?php
                            }
                            }
                            }?>
                        @endforeach
                            <ul class="pager">
                                <?php
                                $total=DB::table('hotel_order_details')->join('orders','orders.id','hotel_order_details.order_id')->where('orders.order_status_id',5)->get();
                                $count=count($total);
                                $from=10;
                                $tongpage=ceil($count/$from);
                                
                    $tachtongpage=explode('.',$tongpage);

                    $tongpage=$tachtongpage[0];
                                $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                                $tach=explode('/',$actual_link);
                                ?>

                                @if($tach[7]==1)
                                    <li class=" disabled "><span>← Trang đầu </span></li>
                                @else
                                    <li><a href="https://test.heyo.group/admin/hotel/order/waiting/1" rel="next">Trang đầu </a></li>

                                @endif
                                @if($tach[7]==1)
                                    <li class=" disabled "><span>← </span></li>
                                @else
                                    <li><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $tach[8]-1;?>" rel="prev">← </a></li>

                                @endif
                                @for($i=1;$i<=$tongpage;$i++)
                                    @if($i==$tach[7])
                                        <li class="active my-active"><span>{{$i}}</span></li>
                                    @else
                                    
                                        <li ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                                    @endif
                                @endfor

                                @if($tach[7]==$tongpage)
                                    <li class=" disabled "><span> →</span></li>
                                @else
                                    <li><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $tach[7]+1;?>" rel="next">→</a></li>

                                @endif

                                @if($tach[7]==$tongpage)
                                    <li class=" disabled "><span>Trang cuối </span></li>
                                @else
                                    <li><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $tongpage?>" rel="next">Trang cuối </a></li>

                                @endif
                            </ul>
                    </div>
                @else

                    <div class="nodata">
                        <span class="iconify" data-icon="fa6-regular:folder-open"></span>
                        <div>
                            Không có dữ liệu !!!
                        </div>
                    </div>
                @endif
            </div>
        </div>
        <div id="waiting_order" class="fade-in @if($tach[6]=='waiting') show @else hide @endif">
            <div class="wrapper">
                <?php

                ?>
                @foreach ($order_hotel_by_5 as $h)
                    <?php
                    $o=DB::table('orders')->where('id',$h->order_id)->first();
                    if ($o!=null){
                    $hotel=DB::table('hotels')->where('id',$h->hotel_id)->first();
                    if ($hotel!=null){

                    $countries=DB::table('locations')->where('id',$hotel->country_id)->first();

                    if($o->order_status_id==1){


                    ?>
                    <div class="box_information">
                        <div class="info_small">
                            <div class="text_blue">{{$o->order_code}}</div>
                            <div class="text_gray">{{$h->created_at}}</div>
                            <div style="display: flex;align-items: center; justify-content: center;">
                                <div class="box_info_book">@if($o->order_status_id==1) Chờ xử lý @endif</div>
                            </div>
                        </div>
                        <div class="info_small">
                            <div class="text_black_bold">{{$h->first_name}} {{$h->last_name}}</div>
                            <div class="text_gray">{{$h->phone_number}}</div>
                        </div>
                        <div class="info_big">
                            <div class="text_black_normal">{{$hotel->hotel_name}}</div>
                        </div>
                        <div class="info_small">
                            <div class="text_black_normal">{{$h->check_in_date}}</div>
                            <div class="text_gray">{{$countries->name_vi}}</div>
                        </div>
                        <div class="info_small">
                            <div class="text_black_bold">{{number_format($o->total_price,0,'.','.') }} đ</div>
                            <div class="text_green">@if($h->payment_status_id==0) Chưa thanh toán @elseif($h->payment_status_id==1) Đã thanh toán @elseif($h->payment_status_id==2) Thanh toán thất bại @elseif($h->payment_status_id==3)Hết hạn thanh toán @elseif($h->payment_status_id==4)Đang hoàn hủy @else Đã hoàn tiền @endif</div>
                        </div>
                        <div class="info_small">
                            <div class="text_black_normal">@if($o->vendor_id==1)TPV @elseif($o->vendor_id==2) EZC @elseif($o->vendor_id) HPL @else VJ @endif</div>
                        </div>
                        <div class="info_small">
                            <div class="text_black_normal">CS01 - Hương</div>
                            <div class="text_red">Chờ xử lý</div>
                        </div>
                        <div class="info_small" style="width: 6%;">
                            <button id="open_popover_waiting{{$i}}" type="button"
                                    style="background-color: transparent;border-radius: 100px;" class="btn"><span
                                    style="width: 25px;height: 25px; color: #C1C1C1" class="iconify"
                                    data-icon="entypo:dots-three-horizontal"></span></button>
                            <div id="box_popover_id_waiting{{$i}}" class="box_popover">
                            <a href="{{asset('admin/hotel/order/detail/'.$o->id)}}" class="btn item_popover">Chi tiết</a>

                                <a href="#" class="btn item_popover">Đặt phòng lại</a>
                                <a href="#" class="btn item_popover">Xác nhận đã thanh toán</a>
                                <a href="#" class="btn item_popover">Huỷ đặt phòng</a>
                            </div>

                        </div>
                    </div>
                    <div style="margin: 0px" class="dropdown-divider"></div>
                    <?php
                    }
                    }
                    }
                    ?>
                @endforeach
                    <ul class="pager">
                        <?php
                        $total=DB::table('hotel_order_details')->join('orders','orders.id','hotel_order_details.order_id')->where('orders.order_status_id',1)->get();
                        $count=count($total);
                        $from=10;
                        $tongpage=ceil($count/$from);
                        
                    $tachtongpage=explode('.',$tongpage);

                    $tongpage=$tachtongpage[0];
                        $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                        $tach=explode('/',$actual_link);
                        ?>
                    <input type='hidden' id="totalpage_waiting"  value="{{$tongpage}}">

                        @if($tach[7]==1)
                            <li class=" disabled "><span>← Trang đầu </span></li>
                        @else
                            <li><a href="https://test.heyo.group/admin/hotel/order/waiting/1" rel="next">Trang đầu </a></li>

                        @endif
                        @if($tach[7]==1)
                            <li class=" disabled "><span>← </span></li>
                        @else
                            <li><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $tach[7]-1;?>" rel="prev">← </a></li>

                        @endif
 
@if($tach[7]>$tongpage )
<script>
    $(document).ready(function(){
        let totalpage=$("#totalpage_waiting").val();
      setTimeout(() => {
        window.location.href='https://test.heyo.group/admin/hotel/order/waiting/'+totalpage;

      }, 1);  
    })
  
  </script>
@endif
                        <?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=1 && $tach[7]<10)
@for($i=1;$i<=10;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>=11 )
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @else
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=10 && $tach[7]<20)
@for($i=10;$i<=20;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>=21 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=20 && $tach[7]<30)
@for($i=20;$i<=30;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>=31  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=30 && $tach[7]<40)
@for($i=30;$i<=40;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>41   )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=40 && $tach[7]<50)
@for($i=40;$i<=50;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>51    )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=50 && $tach[7]<60)
@for($i=50;$i<=60;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>61)
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=60 && $tach[7]<70)
@for($i=60;$i<=70;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>71  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=70 && $tach[7]<80)
@for($i=70;$i<=80;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>81 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=80 && $tach[7]<90)
@for($i=80;$i<=90;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>91   )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=90 && $tach[7]<100)
@for($i=90;$i<=100;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>101 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=100 && $tach[7]<110)
@for($i=100;$i<=110;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>111  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=110 && $tach[7]<120)
@for($i=110;$i<=120;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>121   )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=120 && $tach[7]<130)
@for($i=120;$i<=130;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>131 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=130 && $tach[7]<140)
@for($i=130;$i<=140;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>141  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=140 && $tach[7]<150)
@for($i=140;$i<=150;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>151&&$i!=$tongpage)
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');

?>
@if($tach[7]>=150 && $tach[7]<160)
@for($i=150;$i<=160;$i++)
                        @if($i==$tach[7] )
                        
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>161))
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=160 && $tach[7]<170)
@for($i=160;$i<=170;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>171)
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=170 && $tach[7]<180)
@for($i=170;$i<=180;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>181  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=180 && $tach[7]<190)
@for($i=180;$i<=190;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>191 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=190 && $tach[7]<200)
@for($i=190;$i<=200;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>201  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=200 && $tach[7]<210)
@for($i=200;$i<=210;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>211   )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=210 && $tach[7]<220)
@for($i=210;$i<=220;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>221 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=220 && $tach[7]<230)
@for($i=220;$i<=230;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>231 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=230 && $tach[7]<240)
@for($i=230;$i<=240;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>241 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=240 && $tach[7]<250)
@for($i=240;$i<=$tongpage;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>251  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=250 && $tach[7]<260)
@for($i=250;$i<=260;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>261)
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=260 && $tach[7]<270)
@for($i=260;$i<=270;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>271)
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=270 && $tach[7]<280)
@for($i=270;$i<=280;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>281)
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=280 && $tach[7]<290)
@for($i=280;$i<=290;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>291 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=290 && $tach[7]<300)
@for($i=290;$i<=300;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>301 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=300 && $tach[7]<310)
@for($i=300;$i<=310;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>310)
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=310 && $tach[7]<320)
@for($i=310;$i<=320;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>321  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=320 && $tach[7]<330)
@for($i=320;$i<=330;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>331)
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=330 && $tach[7]<340)
@for($i=330;$i<=340;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>341   )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=340 && $tach[7]<350)
@for($i=340;$i<=350;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>351  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=350 && $tach[7]<360)
@for($i=350;$i<=360;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>361  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=360 && $tach[7]<370)
@for($i=360;$i<=370;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>371  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=370 && $tach[7]<380)
@for($i=370;$i<=380;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>381 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=380 && $tach[7]<390)
@for($i=380;$i<=390;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>391 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=390 && $tach[7]<400)
@for($i=390;$i<=400;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>401   )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=400 && $tach[7]<410)
@for($i=400;$i<=410;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>411 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=410 && $tach[7]<420)
@for($i=410;$i<=420;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>421  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=420 && $tach[7]<430)
@for($i=420;$i<=430;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>431  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=430 && $tach[7]<440)
@for($i=430;$i<=440;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>441  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=440 && $tach[7]<450)
@for($i=440;$i<=450;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>451 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=450 && $tach[7]<460)
@for($i=450;$i<=460;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>461  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=460 && $tach[7]<470)
@for($i=460;$i<=470;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>471  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=470 && $tach[7]<480)
@for($i=470;$i<=480;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>481  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=480 && $tach[7]<490)
@for($i=480;$i<=490;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>491  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=490 && $tach[7]<500)
@for($i=490;$i<=500;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>501  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=500 && $tach[7]<510)
@for($i=500;$i<=510;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>511  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangwaiting{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

                        @if($tach[7]==$tongpage)
                            <li class=" disabled "><span> →</span></li>
                        @else
                            <li><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $tach[7]+1;?>" rel="next">→</a></li>

                        @endif

                        @if($tach[7]==$tongpage)
                            <li class=" disabled "><span>Trang cuối </span></li>
                        @else
                            <li><a href="https://test.heyo.group/admin/hotel/order/waiting/<?php echo $tongpage?>" rel="next">Trang cuối </a></li>

                        @endif
                    </ul>                </div>
        </div>
        <div id="accepted_order" class="fade-in @if($tach[6]=='accepted') show @else hide @endif">
            <div class="wrapper">
                <?php
if(count($order_hotel_accept)>0){
                ?>
                @foreach ($order_hotel_accept as $h)
                    <?php
                    $o=DB::table('orders')->where('id',$h->order_id)->first();
                    if ($o!=null){
                    $hotel=DB::table('hotels')->where('id',$h->hotel_id)->first();
                    if ($hotel!=null){

                    $countries=DB::table('locations')->where('id',$hotel->country_id)->first();

                    if($o->order_status_id==2){


                    ?>
                    <div class="box_information">
                        <div class="info_small">
                            <div class="text_blue">{{$o->order_code}}</div>
                            <div class="text_gray">{{$h->created_at}}</div>
                            <div style="display: flex;align-items: center; justify-content: center;">
                                <div class="box_info_book_accept">@if($o->order_status_id==2) Đã xác nhận @endif</div>
                            </div>
                        </div>
                        <div class="info_small">
                            <div class="text_black_bold">{{$h->first_name}} {{$h->last_name}}</div>
                            <div class="text_gray">{{$h->phone_number}}</div>
                        </div>
                        <div class="info_big">
                            <div class="text_black_normal">{{$hotel->hotel_name}}</div>
                        </div>
                        <div class="info_small">
                            <div class="text_black_normal">{{$h->check_in_date}}</div>
                            <div class="text_gray">{{$countries->name_vi}}</div>
                        </div>
                        <div class="info_small">
                            <div class="text_black_bold">{{number_format($o->total_price,0,'.','.') }} đ</div>
                            <div class="text_green">@if($h->payment_status_id==0) Chưa thanh toán @elseif($h->payment_status_id==1) Đã thanh toán @elseif($h->payment_status_id==2) Thanh toán thất bại @elseif($h->payment_status_id==3)Hết hạn thanh toán @elseif($h->payment_status_id==4)Đang hoàn hủy @else Đã hoàn tiền @endif</div>
                        </div>
                        <div class="info_small">
                            <div class="text_black_normal">@if($o->vendor_id==1)TPV @elseif($o->vendor_id==2) EZC @elseif($o->vendor_id) HPL @else VJ @endif</div>
                        </div>
                        <div class="info_small">
                            <div class="text_black_normal">CS01 - Hương</div>
                            <div class="text_red">Chờ xử lý</div>
                        </div>
                        <div class="info_small" style="width: 6%;">
                            <button id="open_popover_accepted{{$i}}" type="button"
                                    style="background-color: transparent;border-radius: 100px;" class="btn"><span
                                    style="width: 25px;height: 25px; color: #C1C1C1" class="iconify"
                                    data-icon="entypo:dots-three-horizontal"></span></button>
                            <div id="box_popover_id_accepted{{$i}}" class="box_popover">
                            <a href="{{asset('admin/hotel/order/detail/'.$o->id)}}" class="btn item_popover">Chi tiết</a>

                                <a href="#" class="btn item_popover">Đặt phòng lại</a>
                                <a href="#" class="btn item_popover">Xác nhận đã thanh toán</a>
                                <a href="#" class="btn item_popover">Huỷ đặt phòng</a>
                            </div>

                        </div>
                    </div>
                    <div style="margin: 0px" class="dropdown-divider"></div>
                    <?php
                    }

                    }
                    }
                    ?>
                @endforeach
                   <?php }else{?>

                    <div class="nodata">
                        <span class="iconify" data-icon="fa6-regular:folder-open"></span>
                        <div>
                            Không có dữ liệu !!!
                        </div>
                    </div>
                <?php }?>
                <ul class="pager">
                    <?php
                    $total=DB::table('hotel_order_details')->join('orders','orders.id','hotel_order_details.order_id')->where('orders.order_status_id',2)->get();
                    $count=count($total);
                    $from=10;
                    $tongpage=ceil($count/$from);
                    
                    $tachtongpage=explode('.',$tongpage);

                    $tongpage=$tachtongpage[0];
                    $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                    $tach=explode('/',$actual_link);

                    ?>
                    <input type='hidden' id="totalpage_accepted"  value="{{$tongpage}}">

                    @if($tach[7]==1)
                        <li class=" disabled "><span>← Trang đầu </span></li>
                    @else
                        <li><a href="https://test.heyo.group/admin/hotel/order/accepted/1" rel="next">Trang đầu </a></li>

                    @endif
                    @if($tach[7]==1)
                        <li class=" disabled "><span>← </span></li>
                    @else
                        <li><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $tach[7]-1;?>" rel="prev">← </a></li>

                    @endif
                    
                    <?php $bacham='...';
Session::put('bacham','false');

?>
@if($tach[7]>$tongpage )
<script>
    $(document).ready(function(){
        let totalpage=$("#totalpage_accepted").val();
      setTimeout(() => {
        window.location.href='https://test.heyo.group/admin/hotel/order/accepted/'+totalpage;

      }, 1);  
    })
  
  </script>
@endif
    
@if($tach[7]>=1 && $tach[7]<10)

@for($i=1;$i<=10;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>=11 )
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=10 && $tach[7]<20)
@for($i=10;$i<=20;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>=21 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=20 && $tach[7]<30)
@for($i=20;$i<=30;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>=31  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=30 && $tach[7]<40)
@for($i=30;$i<=40;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>41   )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=40 && $tach[7]<50)
@for($i=40;$i<=50;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>51    )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=50 && $tach[7]<60)
@for($i=50;$i<=60;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>61)
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=60 && $tach[7]<70)
@for($i=60;$i<=70;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>71  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=70 && $tach[7]<80)
@for($i=70;$i<=80;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>81 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=80 && $tach[7]<90)
@for($i=80;$i<=90;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>91   )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=90 && $tach[7]<100)
@for($i=90;$i<=100;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>101 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=100 && $tach[7]<110)
@for($i=100;$i<=110;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>111  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=110 && $tach[7]<120)
@for($i=110;$i<=120;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>121   )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=120 && $tach[7]<130)
@for($i=120;$i<=130;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>131 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=130 && $tach[7]<140)
@for($i=130;$i<=140;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>141  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=140 && $tach[7]<150)
@for($i=140;$i<=150;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>151&&$i!=$tongpage)
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');

?>
@if($tach[7]>=150 && $tach[7]<160)
@for($i=150;$i<=160;$i++)
                        @if($i==$tach[7] )
                        
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>161))
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=160 && $tach[7]<170)
@for($i=160;$i<=170;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>171)
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=170 && $tach[7]<180)
@for($i=170;$i<=180;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>181  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=180 && $tach[7]<190)
@for($i=180;$i<=190;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>191 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=190 && $tach[7]<200)
@for($i=190;$i<=200;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>201  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=200 && $tach[7]<210)
@for($i=200;$i<=210;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>211   )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=210 && $tach[7]<220)
@for($i=210;$i<=220;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>221 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=220 && $tach[7]<230)
@for($i=220;$i<=230;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>231 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=230 && $tach[7]<240)
@for($i=230;$i<=240;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>241 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=240 && $tach[7]<250)
@for($i=240;$i<=$tongpage;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>251  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=250 && $tach[7]<260)
@for($i=250;$i<=260;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>261)
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=260 && $tach[7]<270)
@for($i=260;$i<=270;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>271)
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=270 && $tach[7]<280)
@for($i=270;$i<=280;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>281)
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=280 && $tach[7]<290)
@for($i=280;$i<=290;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>291 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=290 && $tach[7]<300)
@for($i=290;$i<=300;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>301 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=300 && $tach[7]<310)
@for($i=300;$i<=310;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>310)
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=310 && $tach[7]<320)
@for($i=310;$i<=320;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>321  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=320 && $tach[7]<330)
@for($i=320;$i<=330;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>331)
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=330 && $tach[7]<340)
@for($i=330;$i<=340;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>341   )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=340 && $tach[7]<350)
@for($i=340;$i<=350;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>351  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=350 && $tach[7]<360)
@for($i=350;$i<=360;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>361  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=360 && $tach[7]<370)
@for($i=360;$i<=370;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>371  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=370 && $tach[7]<380)
@for($i=370;$i<=380;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>381 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=380 && $tach[7]<390)
@for($i=380;$i<=390;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>391 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=390 && $tach[7]<400)
@for($i=390;$i<=400;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>401   )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=400 && $tach[7]<410)
@for($i=400;$i<=410;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>411 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=410 && $tach[7]<420)
@for($i=410;$i<=420;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>421  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=420 && $tach[7]<430)
@for($i=420;$i<=430;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>431  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=430 && $tach[7]<440)
@for($i=430;$i<=440;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>441  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=440 && $tach[7]<450)
@for($i=440;$i<=450;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>451 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=450 && $tach[7]<460)
@for($i=450;$i<=460;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>461  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=460 && $tach[7]<470)
@for($i=460;$i<=470;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>471  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=470 && $tach[7]<480)
@for($i=470;$i<=480;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>481  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=480 && $tach[7]<490)
@for($i=480;$i<=490;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>491  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=490 && $tach[7]<500)
@for($i=490;$i<=500;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>501  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=500 && $tach[7]<510)
@for($i=500;$i<=510;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>511  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangaccepted{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

                    @if($tach[7]==$tongpage)
                        <li class=" disabled "><span> →</span></li>
                    @else
                        <li><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $tach[7]+1;?>" rel="next">→</a></li>

                    @endif

                    @if($tach[7]==$tongpage)
                        <li class=" disabled "><span>Trang cuối </span></li>
                    @else
                        <li><a href="https://test.heyo.group/admin/hotel/order/accepted/<?php echo $tongpage?>" rel="next">Trang cuối </a></li>

                    @endif
                </ul>                </div>
        </div>
        <div id="canceled_order" class="fade-in @if($tach[6]=='cancel') show @else hide @endif">
            <div class="wrapper">
                <?php
                if(count($order_hotel_cancel)>0){
                    $i=0;
                ?>
                @foreach ($order_hotel_cancel as $h)
                    <?php
                        $i++;
                    $o=DB::table('orders')->where('id',$h->order_id)->first();
                    if ($o!=null){
                    $hotel=DB::table('hotels')->where('id',$h->hotel_id)->first();
                    if ($hotel!=null){

                    $countries=DB::table('locations')->where('id',$hotel->country_id)->first();

                    if($o->order_status_id==4){


                    ?>
                    <div class="box_information">
                        <div class="info_small">
                            <div class="text_blue">{{$o->order_code}}</div>
                            <div class="text_gray">{{$h->created_at}}</div>
                            <div style="display: flex;align-items: center; justify-content: center;">
                                <div class="box_info_book">@if($o->order_status_id==2) Đã xác nhận @endif</div>
                            </div>
                        </div>
                        <div class="info_small">
                            <div class="text_black_bold">{{$h->first_name}} {{$h->last_name}}</div>
                            <div class="text_gray">{{$h->phone_number}}</div>
                        </div>
                        <div class="info_big">
                            <div class="text_black_normal">{{$hotel->hotel_name}}</div>
                        </div>
                        <div class="info_small">
                            <div class="text_black_normal">{{$h->check_in_date}}</div>
                            <div class="text_gray">{{$countries->name_vi}}</div>
                        </div>
                        <div class="info_small">
                            <div class="text_black_bold">{{number_format($o->total_price,0,'.','.') }} đ</div>
                            <div class="text_green">@if($h->payment_status_id==0) Chưa thanh toán @elseif($h->payment_status_id==1) Đã thanh toán @elseif($h->payment_status_id==2) Thanh toán thất bại @elseif($h->payment_status_id==3)Hết hạn thanh toán @elseif($h->payment_status_id==4)Đang hoàn hủy @else Đã hoàn tiền @endif</div>
                        </div>
                        <div class="info_small">
                            <div class="text_black_normal">@if($o->vendor_id==1)TPV @elseif($o->vendor_id==2) EZC @elseif($o->vendor_id) HPL @else VJ @endif</div>
                        </div>
                        <div class="info_small">
                            <div class="text_black_normal">CS01 - Hương</div>
                            <div class="text_red">Chờ xử lý</div>
                        </div>
                        <div class="info_small" style="width: 6%;">
                            <button id="open_popover_cancled{{$i}}" type="button"
                                    style="background-color: transparent;border-radius: 100px;" class="btn"><span
                                    style="width: 25px;height: 25px; color: #C1C1C1" class="iconify"
                                    data-icon="entypo:dots-three-horizontal"></span></button>
                            <div id="box_popover_id_cancled{{$i}}" class="box_popover">         
                                                               <a href="{{asset('admin/hotel/order/detail/'.$o->id)}}" class="btn item_popover">Chi tiết</a>

                                <a href="#" class="btn item_popover">Đặt phòng lại</a>
                                <a href="#" class="btn item_popover">Xác nhận đã thanh toán</a>
                                <a href="#" class="btn item_popover">Huỷ đặt phòng</a>
                            </div>

                        </div>
                    </div>
                    <div style="margin: 0px" class="dropdown-divider"></div>
                    <?php
                    }

                    }
                    }
                    ?>
                @endforeach
                <?php }else{?>

                    <div class="nodata">
                        <span class="iconify" data-icon="fa6-regular:folder-open"></span>
                        <div>
                            Không có dữ liệu !!!
                        </div>
                    </div>                <?php }?>
                <ul class="pager">
                    <?php
                    $total=DB::table('hotel_order_details')->join('orders','orders.id','hotel_order_details.order_id')->where('orders.order_status_id',4)->get();
                    $count=count($total);
                    $from=10;
                    $tongpage=ceil($count/$from);
                    
                    $tachtongpage=explode('.',$tongpage);

                    $tongpage=$tachtongpage[0];
                    $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                    $tach=explode('/',$actual_link);

                    ?>
                    <input type='hidden' id="totalpage_cancel"  value="{{$tongpage}}">

                    @if($tach[7]==1)
                        <li class=" disabled "><span>← Trang đầu </span></li>
                    @else
                        <li><a href="https://test.heyo.group/admin/hotel/order/cancel/1" rel="next">Trang đầu </a></li>

                    @endif
                    @if($tach[7]==1)
                        <li class=" disabled "><span>← </span></li>
                    @else
                        <li><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $tach[7]-1;?>" rel="prev">← </a></li>

                    @endif

            
                                            

                    <?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=1 && $tach[7]<10)
@for($i=1;$i<=10;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>=11 )
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @else
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=10 && $tach[7]<20)
@for($i=10;$i<=20;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>=21 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=20 && $tach[7]<30)
@for($i=20;$i<=30;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>=31  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=30 && $tach[7]<40)
@for($i=30;$i<=40;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>41   )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=40 && $tach[7]<50)
@for($i=40;$i<=50;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>51    )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=50 && $tach[7]<60)
@for($i=50;$i<=60;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>61)
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=60 && $tach[7]<70)
@for($i=60;$i<=70;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>71  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=70 && $tach[7]<80)
@for($i=70;$i<=80;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>81 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=80 && $tach[7]<90)
@for($i=80;$i<=90;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>91   )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=90 && $tach[7]<100)
@for($i=90;$i<=100;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>101 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=100 && $tach[7]<110)
@for($i=100;$i<=110;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>111  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=110 && $tach[7]<120)
@for($i=110;$i<=120;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>121   )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=120 && $tach[7]<130)
@for($i=120;$i<=130;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>131 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=130 && $tach[7]<140)
@for($i=130;$i<=140;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>141  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=140 && $tach[7]<150)
@for($i=140;$i<=150;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>151&&$i!=$tongpage)
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');

?>
@if($tach[7]>=150 && $tach[7]<160)
@for($i=150;$i<=160;$i++)
                        @if($i==$tach[7] )
                        
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>161))
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=160 && $tach[7]<170)
@for($i=160;$i<=170;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>171)
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=170 && $tach[7]<180)
@for($i=170;$i<=180;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>181  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=180 && $tach[7]<190)
@for($i=180;$i<=190;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>191 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=190 && $tach[7]<200)
@for($i=190;$i<=200;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>201  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=200 && $tach[7]<210)
@for($i=200;$i<=210;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>211   )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=210 && $tach[7]<220)
@for($i=210;$i<=220;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>221 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=220 && $tach[7]<230)
@for($i=220;$i<=230;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>231 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=230 && $tach[7]<240)
@for($i=230;$i<=240;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>241 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=240 && $tach[7]<250)
@for($i=240;$i<=$tongpage;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>251  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=250 && $tach[7]<260)
@for($i=250;$i<=260;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>261)
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=260 && $tach[7]<270)
@for($i=260;$i<=270;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>271)
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=270 && $tach[7]<280)
@for($i=270;$i<=280;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>281)
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=280 && $tach[7]<290)
@for($i=280;$i<=290;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>291 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=290 && $tach[7]<300)
@for($i=290;$i<=300;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>301 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=300 && $tach[7]<310)
@for($i=300;$i<=310;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>310)
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=310 && $tach[7]<320)
@for($i=310;$i<=320;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>321  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=320 && $tach[7]<330)
@for($i=320;$i<=330;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>331)
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=330 && $tach[7]<340)
@for($i=330;$i<=340;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>341   )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=340 && $tach[7]<350)
@for($i=340;$i<=350;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>351  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=350 && $tach[7]<360)
@for($i=350;$i<=360;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>361  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=360 && $tach[7]<370)
@for($i=360;$i<=370;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>371  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=370 && $tach[7]<380)
@for($i=370;$i<=380;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>381 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=380 && $tach[7]<390)
@for($i=380;$i<=390;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>391 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=390 && $tach[7]<400)
@for($i=390;$i<=400;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>401   )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=400 && $tach[7]<410)
@for($i=400;$i<=410;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>411 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=410 && $tach[7]<420)
@for($i=410;$i<=420;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>421  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=420 && $tach[7]<430)
@for($i=420;$i<=430;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>431  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=430 && $tach[7]<440)
@for($i=430;$i<=440;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>441  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=440 && $tach[7]<450)
@for($i=440;$i<=450;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>451 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=450 && $tach[7]<460)
@for($i=450;$i<=460;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>461  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=460 && $tach[7]<470)
@for($i=460;$i<=470;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>471  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=470 && $tach[7]<480)
@for($i=470;$i<=480;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>481  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=480 && $tach[7]<490)
@for($i=480;$i<=490;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>491  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=490 && $tach[7]<500)
@for($i=490;$i<=500;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>501  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=500 && $tach[7]<510)
@for($i=500;$i<=510;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>511  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangcancel{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

                    @if($tach[7]==$tongpage)
                        <li class=" disabled "><span> →</span></li>
                    @else
                        <li><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $tach[7]+1;?>" rel="next">→</a></li>

                    @endif

                    @if($tach[7]==$tongpage)
                        <li class=" disabled "><span>Trang cuối </span></li>
                    @else
                        <li><a href="https://test.heyo.group/admin/hotel/order/cancel/<?php echo $tongpage?>" rel="next">Trang cuối </a></li>

                    @endif
                </ul>                </div>
        </div>
        <div id="done_order" class="fade-in @if($tach[6]=='done') show @else hide @endif">

            <div class="wrapper">
                <?php
                if(count($order_hotel_doen)>0){
                ?>
                @foreach ($order_hotel_doen as $h)
                    <?php
                    $o=DB::table('orders')->where('id',$h->order_id)->first();
                    if ($o!=null){
                    $hotel=DB::table('hotels')->where('id',$h->hotel_id)->first();
                    if ($hotel!=null){

                    $countries=DB::table('locations')->where('id',$hotel->country_id)->first();

                    if($o->order_status_id==5){


                    ?>
                    <div class="box_information">
                        <div class="info_small">
                            <div class="text_blue">{{$o->order_code}}</div>
                            <div class="text_gray">{{$h->created_at}}</div>
                            <div style="display: flex;align-items: center; justify-content: center;">
                                <div class="box_info_book">@if($o->order_status_id==2) Đã xác nhận @endif</div>
                            </div>
                        </div>
                        <div class="info_small">
                            <div class="text_black_bold">{{$h->first_name}} {{$h->last_name}}</div>
                            <div class="text_gray">{{$h->phone_number}}</div>
                        </div>
                        <div class="info_big">
                            <div class="text_black_normal">{{$hotel->hotel_name}}</div>
                        </div>
                        <div class="info_small">
                            <div class="text_black_normal">{{$h->check_in_date}}</div>
                            <div class="text_gray">{{$countries->name_vi}}</div>
                        </div>
                        <div class="info_small">
                            <div class="text_black_bold">{{number_format($o->total_price,0,'.','.') }} đ</div>
                            <div class="text_green">@if($h->payment_status_id==0) Chưa thanh toán @elseif($h->payment_status_id==1) Đã thanh toán @elseif($h->payment_status_id==2) Thanh toán thất bại @elseif($h->payment_status_id==3)Hết hạn thanh toán @elseif($h->payment_status_id==4)Đang hoàn hủy @else Đã hoàn tiền @endif</div>
                        </div>
                        <div class="info_small">
                            <div class="text_black_normal">@if($o->vendor_id==1)TPV @elseif($o->vendor_id==2) EZC @elseif($o->vendor_id) HPL @else VJ @endif</div>
                        </div>
                        <div class="info_small">
                            <div class="text_black_normal">CS01 - Hương</div>
                            <div class="text_red">Chờ xử lý</div>
                        </div>
                        <div class="info_small" style="width: 6%;">
                            <button id="open_popover_doned{{$i}}" type="button"
                                    style="background-color: transparent;border-radius: 100px;" class="btn"><span
                                    style="width: 25px;height: 25px; color: #C1C1C1" class="iconify"
                                    data-icon="entypo:dots-three-horizontal"></span></button>
                            <div id="box_popover_id_doned{{$i}}" class="box_popover">
                            <a href="{{asset('admin/hotel/order/detail/'.$o->id)}}" class="btn item_popover">Chi tiết</a>

                                <a href="#" class="btn item_popover">Đặt phòng lại</a>
                                <a href="#" class="btn item_popover">Xác nhận đã thanh toán</a>
                                <a href="#" class="btn item_popover">Huỷ đặt phòng</a>
                            </div>

                        </div>
                    </div>
                    <div style="margin: 0px" class="dropdown-divider"></div>
                    <?php
                    }

                    }
                    }
                    ?>
                @endforeach
                <?php }else{?>

                    <div class="nodata">
                        <span class="iconify" data-icon="fa6-regular:folder-open"></span>
                        <div>
                            Không có dữ liệu !!!
                        </div>
                    </div>                <?php }?>
                <ul class="pager">
                    <?php
                    $total=DB::table('hotel_order_details')->join('orders','orders.id','hotel_order_details.order_id')->where('orders.order_status_id',5)->get();
                    $count=count($total);
                    $from=10;
                    $tongpage=ceil($count/$from);
                    $tachtongpage=explode('.',$tongpage);

                    $tongpage=$tachtongpage[0];
                    $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                    $tach=explode('/',$actual_link);

                    ?>
                    <input type='hidden' id="totalpage_done"  value="{{$tongpage}}">

                    @if($tach[7]==1)
                        <li class=" disabled "><span>← Trang đầu </span></li>
                    @else
                        <li><a href="https://test.heyo.group/admin/hotel/order/done/1" rel="next">Trang đầu </a></li>

                    @endif
                    @if($tach[7]==1)
                        <li class=" disabled "><span>← </span></li>
                    @else
                        <li><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $tach[7]-1;?>" rel="prev">← </a></li>

                    @endif
                       
                                  

                    <?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=1 && $tach[7]<10)
@for($i=1;$i<=10;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>=11 )
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @else
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=10 && $tach[7]<20)
@for($i=10;$i<=20;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>=21 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=20 && $tach[7]<30)
@for($i=20;$i<=30;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>=31  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=30 && $tach[7]<40)
@for($i=30;$i<=40;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>41   )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=40 && $tach[7]<50)
@for($i=40;$i<=50;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>51    )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=50 && $tach[7]<60)
@for($i=50;$i<=60;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>61)
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=60 && $tach[7]<70)
@for($i=60;$i<=70;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>71  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=70 && $tach[7]<80)
@for($i=70;$i<=80;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>81 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=80 && $tach[7]<90)
@for($i=80;$i<=90;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>91   )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=90 && $tach[7]<100)
@for($i=90;$i<=100;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>101 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=100 && $tach[7]<110)
@for($i=100;$i<=110;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>111  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=110 && $tach[7]<120)
@for($i=110;$i<=120;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>121   )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=120 && $tach[7]<130)
@for($i=120;$i<=130;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>131 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=130 && $tach[7]<140)
@for($i=130;$i<=140;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>141  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=140 && $tach[7]<150)
@for($i=140;$i<=150;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>151&&$i!=$tongpage)
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');

?>
@if($tach[7]>=150 && $tach[7]<160)
@for($i=150;$i<=160;$i++)
                        @if($i==$tach[7] )
                        
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>161))
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=160 && $tach[7]<170)
@for($i=160;$i<=170;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>171)
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=170 && $tach[7]<180)
@for($i=170;$i<=180;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>181  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=180 && $tach[7]<190)
@for($i=180;$i<=190;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>191 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=190 && $tach[7]<200)
@for($i=190;$i<=200;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>201  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=200 && $tach[7]<210)
@for($i=200;$i<=210;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>211   )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=210 && $tach[7]<220)
@for($i=210;$i<=220;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>221 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=220 && $tach[7]<230)
@for($i=220;$i<=230;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>231 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=230 && $tach[7]<240)
@for($i=230;$i<=240;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>241 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=240 && $tach[7]<250)
@for($i=240;$i<=$tongpage;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>251  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=250 && $tach[7]<260)
@for($i=250;$i<=260;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>261)
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=260 && $tach[7]<270)
@for($i=260;$i<=270;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>271)
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=270 && $tach[7]<280)
@for($i=270;$i<=280;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>281)
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=280 && $tach[7]<290)
@for($i=280;$i<=290;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>291 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=290 && $tach[7]<300)
@for($i=290;$i<=300;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>301 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=300 && $tach[7]<310)
@for($i=300;$i<=310;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>310)
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=310 && $tach[7]<320)
@for($i=310;$i<=320;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>321  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=320 && $tach[7]<330)
@for($i=320;$i<=330;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>331)
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=330 && $tach[7]<340)
@for($i=330;$i<=340;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>341   )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=340 && $tach[7]<350)
@for($i=340;$i<=350;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>351  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=350 && $tach[7]<360)
@for($i=350;$i<=360;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>361  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=360 && $tach[7]<370)
@for($i=360;$i<=370;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>371  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=370 && $tach[7]<380)
@for($i=370;$i<=380;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>381 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=380 && $tach[7]<390)
@for($i=380;$i<=390;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>391 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=390 && $tach[7]<400)
@for($i=390;$i<=400;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>401   )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=400 && $tach[7]<410)
@for($i=400;$i<=410;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>411 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=410 && $tach[7]<420)
@for($i=410;$i<=420;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>421  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=420 && $tach[7]<430)
@for($i=420;$i<=430;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>431  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=430 && $tach[7]<440)
@for($i=430;$i<=440;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>441  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=440 && $tach[7]<450)
@for($i=440;$i<=450;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>451 )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=450 && $tach[7]<460)
@for($i=450;$i<=460;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>461  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=460 && $tach[7]<470)
@for($i=460;$i<=470;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>471  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif

<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=470 && $tach[7]<480)
@for($i=470;$i<=480;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>481  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=480 && $tach[7]<490)
@for($i=480;$i<=490;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>491  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=490 && $tach[7]<500)
@for($i=490;$i<=500;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>501  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
<?php $bacham='...';
Session::put('bacham','false');
?>
@if($tach[7]>=500 && $tach[7]<510)
@for($i=500;$i<=510;$i++)
                        @if($i==$tach[7])
                            <li class="active my-active"><span>{{$i}}</span></li>
                        @elseif($i>511  )
                           
                            @if(Session::get('bacham')=='false')    
                              <?php  Session::put('bacham','true');?>

                                <li>{{$bacham}}</li>
                            @endif
                        @elseif($i<=$tongpage)
                        <li class="sophantrangdone{{$i}}" ><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $i?>">{{$i}}</a></li>

                        @endif
                    @endfor

@endif
                    @if($tach[7]==$tongpage)
                        <li class=" disabled "><span> →</span></li>
                    @else
                        <li><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $tach[7]+1;?>" rel="next">→</a></li>

                    @endif

                    @if($tach[7]==$tongpage)
                        <li class=" disabled "><span>Trang cuối </span></li>
                    @else
                        <li><a href="https://test.heyo.group/admin/hotel/order/done/<?php echo $tongpage?>" rel="next">Trang cuối </a></li>

                    @endif
                </ul>                </div>
        </div>
    </div>
</div>
<div id="modalReceiveAccess" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tiếp Nhận Xử Lý Đơn Hàng</h4>
                <button type="button" class="close1" data-dismiss="modal">
        <span style="width: 30px; height:30px; color: #000" class="iconify"
              data-icon="ep:close-bold"></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="content_modal_receive_access">
                    Nhân viên CSKH được phân công sẽ chịu trách nhiệm xử lý các vấn đề phát sinh của đơn hàng và
                    chăm sóc hỗ trợ khách hàng. Bạn sẽ tiếp nhận xử lý đơn hàng <b
                        style="color: #3982d1">H-20210821102832</b>?
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="button_receive" data-dismiss="modal">Tiếp nhận</button>
            </div>
        </div>

    </div>
</div>
</body>

</html>
{{--<script>--}}
{{--    $(document).ready(function () {--}}
{{--        $("#button_tab_waiting").click(function () {--}}
{{--            location.href='https://test.heyo.group/admin/hotel/order/waiting/1';--}}
{{--        });--}}
{{--        $("#button_tab_accepted").click(function () {--}}
{{--            location.href='https://test.heyo.group/admin/hotel/order/accepted/1';--}}
{{--        });--}}
{{--        $("#button_tab_canceled").click(function () {--}}
{{--            location.href='https://test.heyo.group/admin/hotel/order/cancel/1';--}}
{{--        });--}}
{{--        $("#button_tab_done").click(function () {--}}
{{--            location.href='https://test.heyo.group/admin/hotel/order/done/1';--}}
{{--        });--}}
{{--        $("#all_order").click(function () {--}}
{{--            location.href='https://test.heyo.group/admin/hotel/order/all/1';--}}
{{--        });--}}
{{--    });--}}
{{--</script>--}}
@if($tach[6]=="waiting")

<script>
    $(document).ready(function () {
        $("#waiting_order").removeClass("hide");
        $("#waiting_order").addClass("show");

    });
</script>
@endif
@if($tach[6]=="accepted")

<script>
    $(document).ready(function () {

        $("#accepted_order").removeClass("hide");
        $("#accepted_order").addClass("show");
    });
</script>
@endif
@if($tach[6]=="cancel")

<script>
    $(document).ready(function () {

        $("#canceled_order").removeClass("hide");
        $("#canceled_order").addClass("show");
    });
</script>
@endif
@if($tach[6]=="done")

<script>
    $(document).ready(function () {

        $("#done_order").removeClass("hide");
        $("#done_order").addClass("show");
    });
</script>
@endif
@if($tach[6]=="all")

<script>
    $(document).ready(function () {

        $("#all_order").removeClass("hide");
        $("#all_order").addClass("show");
    });
</script>
@endif
