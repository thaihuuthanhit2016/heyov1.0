<!DOCTYPE html>
<html lang="en">
<head>
    <title>HF Admin</title>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="{{asset('public/admin/css/create.css')}}" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="https://code.iconify.design/2/2.2.1/iconify.min.js"></script>
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css"
          rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
    <link rel="stylesheet"
          href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
    <script
        src="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
    <script></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="{{asset('public/admin/js/main.js')}}"></script>
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
    <style>
        .toggle.ios,
        .toggle-on.ios,
        .toggle-off.ios {
            border-radius: 20rem;
        }
        .toggle.ios .toggle-handle {
            border-radius: 20rem;
        }
    </style>
</head>
<body>
<div class="menu_header">
    <div class="left">
        <a href="#" class="logo">HEYO TRIP</a>
    </div>
    <div class="right">
        <span class="name_menu"> Quận huyện</span>
        <div class="box_admin">
               <span class="iconify" style="width: 25px; height: 25px; color: #032044"
                     data-icon="ic:baseline-notifications-none"></span>
            <div class="badge badge-danger counter">9</div>
            <div id="btn_admin" class="wrapper_admin">
                <span style="width: 25px; height: 25px; color: #032044" class="iconify" data-icon="lucide:user"></span>
                <span class="admin">Admin</span>
                <span style="
                     width: 15px;
                     height: 15px;
                     color: #032044;
                     margin-left: 10px;
                     " class="iconify" data-icon="ant-design:caret-down-filled"></span>
            </div>
        </div>
    </div>
    <div id="profile" style="box-shadow: 10" class="box_profile">
            <span style="width: 30px; height: 30px; color: #032044" class="iconify"
                  data-icon="simple-line-icons:logout"></span>
        <span class="logout"><a href="{{asset('logout')}}" style="text-decoration: none;color: black"> Đăng xuất</a></span>
    </div>
</div>
<div style="display: flex">
    <div class="menu_left">
        <div class="top">
            <a href="{{asset('admin/hotel/order/all/1')}}" class="item">
            <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                  data-icon="icon-park-outline:hotel"></span>
                <span style="color: #526a87" class="name_menu">Khách sạn</span>
            </a>
            <a href="{{asset('admin/flight/order/all/1')}}" style=" text-decoration: none" class="item">
                <span class="iconify" style="color: #526a87; width: 30px; height: 30px" data-icon="ps:plane"></span>
                <span  style="color: #526a87"class="name_menu">Vé máy bay</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
                <span class="iconify" style="color: #526a87; width: 30px; height: 30px" data-icon="clarity:car-line"></span>
                <span style="color: #526a87" class="name_menu">Thuê xe</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
            <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                  data-icon="clarity:data-cluster-line"></span>
                <span style="color: #526a87" class="name_menu">Crypto</span>
            </a>
        </div>
        <div class="top">
            <a href="#" style="text-decoration: none" class="item">
                <span class="iconify" style="color: #526a87; width: 30px; height: 30px" data-icon="ep:user-filled"></span>
                <span style="color: #526a87" class="name_menu">Tài khoản</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
            <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                  data-icon="dashicons:welcome-widgets-menus"></span>
                <span style="color: #526a87" class="name_menu">Danh mục</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
                <span class="iconify" style="color: #526a87; width: 30px; height: 30px" data-icon="gg:website"></span>
                <span style="color: #526a87" class="name_menu">Website</span>
            </a>
            <a href="{{asset('admin/website/list/quocgia')}}" style="background-color: #f2f9ff; text-decoration: none"  class="item">
            <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                  data-icon="material-symbols:settings-suggest-outline-sharp"></span>
                <span style="color: #526a87" class="name_menu">Cấu hình</span>
            </a>
        </div>
    </div>

    <div class="menu_right">
        <a href="#" class="item">
         <span style="width: 25px; height: 25px; color: #032044" class="iconify"
               data-icon="majesticons:analytics"></span>
            <span class="name_menu">Dashboard</span>
        </a>
        <a href="{{asset('admin/website/list/quocgia')}}" class="item" >
            <i class="fa fa-globe"></i>         <span class="name_menu" >Quốc gia</span>
        </a> <a href="{{asset('admin/website/list/tinhthanhpho')}}" class="item" >
         <span class="iconify" data-icon="bxs:city"></span>    <span class="name_menu" >Tỉnh thành phố</span>
         </a>
         <a href="{{asset('admin/website/list/quanhuyen')}}" class="item" >
         <span class="iconify" data-icon="bxs:map"></span>         <span class="name_menu" style="color: #329223; font-weight: 800;">Quận huyện</span>
         </a>

    </div>
    <form method="post"enctype="multipart/form-data">@csrf

        <div class="box_content">
        <div class="wrapper">
            <div class="content">
                <div class="box_title">
                    <div class="title">Thông tin Quận huyện</div>
                </div>

                <div style="margin: 0px" class="dropdown-divider"></div>
                @if(Session::get('error'))
                    <p class="alert-success alert">{{Session::get('error')}}</p>
                    @endif
                <div class="content">

                    <div class="row_input">
                        <div class="box_input">
                            <div class="label">
                                Tên quận huyện (VI)
                                <span class="star">*</span>
                            </div>
                            <input class="input" required type="text" name="name_vi" value="{{$locations->name_vi}}" class="form-control" />

                        </div>
                        <div class="box_input">
                            <div class="label">
                                Tên quận huyện (EN)
                                <span class="star">*</span>
                            </div>
                            <input class="input" required type="text" name="name_en" value="{{$locations->name_en}}" class="form-control" />

                        </div>
                        <div class="box_input">
                            <div class="label">
                                Mã quận huyện
                                <span class="star">*</span>
                            </div>
                            <input class="input" required type="text" name="code_location" value="{{$locations->code_location}}" class="form-control" />

                        </div>
                    </div>


                    <?php
                    $city=DB::table('locations')->where('type_id',1)->get();
                    $quocgia=DB::table('locations')->where('parent_id',0)->get();
                    ?>
                    <div class="row_input">
                    <div class="box_input">
                            <div class="label">
Thành phố                                <span class="star">*</span>
                            </div>
                            <select name="parent_id" class="input" id="city">
                                @foreach($city as $cv)
                                    <option value="{{$cv->id}}" @if($locations->parent_id==$cv->id ) <?php Session::put('city',$cv->parent_id)?>selected @endif>{{$cv->name_vi}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="box_input">
                            <div class="label">
Quốc gia                              <span class="star">*</span>
                            </div>
                            <select name="" class="input" id="quocgia">
                                @foreach($quocgia as $cv)
                                    <option value="{{$cv->id}}" @if(Session::get('city')==$cv->id ) selected @endif>{{$cv->name_vi}}</option>
                                @endforeach
                            </select>
                        </div>


                        <div class="box_input">
                            <div class="label">
                                Trạng thái
                                <span class="star">*</span>
                            </div>
                            <select class="form-control" name="status"><option value="0" @if($locations->status==0) selected @endif>Đang hoạt động</option><option value="1" @if($locations->status==1) selected @endif>Ngừng hoạt động</option></select>

                        </div>
                        </div>

                    <div class="row_input">


                    </div>
                </div>
            </div>
            <div class="box_title">
                <div class="title"></div>
                <div class="box_option">
                <a  href="{{Session::get('url')}}" class="btn button_watch">Hủy</a>

                    <button type="submit" class="btn button_save">Cập nhật</button>
                </div>
            </div>

        </div>
    </div>
    </form>
</div>
</body>
</html>
<script>

$(document).ready(function(){
    $('#quocgia').change(function(){
        let quocgia=$('#quocgia').val();
        
        $.ajax({
                    url : "https://test.heyo.group/admin/loadcity",
                    type : "get",
                    dataType:"text",
                    data : {
                        quocgia : quocgia
                    },
                    success : function (result){
                        $('#city').html(result);
                    }
                });
    });
});
</script>
<style>
    span.name_menu {
    color: #000;
}
</style>