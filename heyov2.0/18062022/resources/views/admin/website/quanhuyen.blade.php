<!DOCTYPE html>
<html lang="en">
   <head>
      <title>HF Admin</title>
      <meta charset="UTF-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <link rel="stylesheet" href="{{asset('public/admin/css/index.css')}}" />
      <link rel="stylesheet" href="{{asset('public/admin/css/onoffstatus.css')}}" />
      <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
      <script src="https://code.iconify.design/2/2.2.1/iconify.min.js"></script>
      <script src="https://unpkg.com/@popperjs/core@2"></script>
      <link rel='stylesheet'
         href='https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css'>
      <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js'></script>
      <script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js'></script>
      <script
         src='https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js'></script>
      <script></script>
      <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css"
         rel="stylesheet">
      <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
      <script src="{{asset('public/admin/js/main.js')}}"></script>
      <script src="{{asset('public/admin/js/onoffstatus.js')}}"></script>
      <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
      <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
      <!-- <link rel="stylesheet" type="text/css" href="/jquery.datetimepicker.css"/ >
         <script src="/jquery.js"></script>
         <script src="/build/jquery.datetimepicker.full.min.js"></script> -->
      <style>
         .toggle.ios,
         .toggle-on.ios,
         .toggle-off.ios {
         border-radius: 20rem;
         }
         .toggle.ios .toggle-handle {
         border-radius: 20rem;
         }
      </style>
   </head>
   <body>
      <div class="menu_header">
         <div class="left">
            <a href="#" class="logo">HEYO TRIP</a>
         </div>
         <div class="right">
            <span class="name_menu"> Quận/huyện</span>
            <div class="box_admin">
               <span class="iconify" style="width: 25px; height: 25px; color: #032044"
                  data-icon="ic:baseline-notifications-none"></span>
               <div class="badge badge-danger counter">9</div>
               <div id="btn_admin" class="wrapper_admin">
                  <span style="width: 25px; height: 25px; color: #032044" class="iconify" data-icon="lucide:user"></span>
                  <span class="admin">Admin</span>
                  <span style="
                     width: 15px;
                     height: 15px;
                     color: #032044;
                     margin-left: 10px;
                     " class="iconify" data-icon="ant-design:caret-down-filled"></span>
               </div>
            </div>
         </div>
         <div id="profile" style="box-shadow: 10;" class="box_profile">
            <span style="width: 30px; height: 30px;color: #032044;" class="iconify"
               data-icon="simple-line-icons:logout"></span>
            <span class="logout"><a href="{{asset('logout')}}" style="text-decoration: none;color: black"> Đăng xuất</a></span>
         </div>
      </div>
      <div style="display: flex">
      <div class="menu_left">
         <div class="top">
            <a href="{{asset('admin/hotel/order/all/1')}}" class="item">
            <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
               data-icon="icon-park-outline:hotel"></span>
            <span style="color: #526a87" class="name_menu">Khách sạn</span>
            </a>
            <a href="{{asset('admin/flight/order/all/1')}}" style=" text-decoration: none" class="item">
            <span class="iconify" style="color: #526a87; width: 30px; height: 30px" data-icon="ps:plane"></span>
            <span  style="color: #526a87"class="name_menu">Vé máy bay</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
            <span class="iconify" style="color: #526a87; width: 30px; height: 30px" data-icon="clarity:car-line"></span>
            <span style="color: #526a87" class="name_menu">Thuê xe</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
            <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
               data-icon="clarity:data-cluster-line"></span>
            <span style="color: #526a87" class="name_menu">Crypto</span>
            </a>
         </div>
         <div class="top">
            <a href="#" style="text-decoration: none" class="item">
            <span class="iconify" style="color: #526a87; width: 30px; height: 30px" data-icon="ep:user-filled"></span>
            <span style="color: #526a87" class="name_menu">Tài khoản</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
            <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
               data-icon="dashicons:welcome-widgets-menus"></span>
            <span style="color: #526a87" class="name_menu">Danh mục</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
            <span class="iconify" style="color: #526a87; width: 30px; height: 30px" data-icon="gg:website"></span>
            <span style="color: #526a87" class="name_menu">Website</span>
            </a>
            <a href="{{asset('admin/website/list/quocgia')}}" style="background-color: #f2f9ff; text-decoration: none"  class="item">
            <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
               data-icon="material-symbols:settings-suggest-outline-sharp"></span>
            <span style="color: #526a87" class="name_menu">Cấu hình</span>
            </a>
         </div>
      </div>
      <div class="menu_right">
         <a href="#" class="item">
         <span style="width: 25px; height: 25px; color: #032044" class="iconify"
            data-icon="majesticons:analytics"></span>
         <span class="name_menu">Dashboard</span>
         </a>
         <a href="{{asset('admin/website/list/quocgia')}}" class="item" >
         <i class="fa fa-globe"></i>         <span class="name_menu" >Quốc gia</span>
         </a> <a href="{{asset('admin/website/list/tinhthanhpho')}}" class="item" >
         <span class="iconify" data-icon="bxs:city"></span>    <span class="name_menu" >Tỉnh thành phố</span>
         </a>
         <a href="{{asset('admin/website/list/quanhuyen')}}" class="item" >
         <span class="iconify" data-icon="bxs:map"></span>     <span class="name_menu" style="color: #329223; font-weight: 800;">Quận huyện</span>
         </a>
      </div>
      <?Php
         Session::put('program_code_promo','');
         Session::put('code_promotion','');
         Session::put('type_promo','');
         Session::put('status','');
         Session::put('nation','');
         Session::put('date_start_from','');
         Session::put('date_start_to','');
         Session::put('date_end_from','');
         Session::put('date_end_to','');
         ?>      
      <div class="box_content">
         <div class="wrapper">
         <div class="wrapper">
         <div class="box_title">
          <div class="title">Tra cứu quận huyện</div>
        </div>
        <form method="post">
        <div style="margin: 0px" class="dropdown-divider"></div>
        <div class="content">
        <div class="row_input">
            <div class="box_input">
              <div class="label">Tên quận huyện </div>
              <select  name="quocgia" class='js-select2-multi form-control' id="location_from">
                                <option value=''>Tất cả</option>
                                <?php
                                    $location=DB::table('locations')->where('type_id',0)->get();
                                ?>
                                @foreach($location as $al)


                                    <option @if(Session::get('name_vi')==$al->name_vi) selected @endif value="{{$al->name_vi}}"> {{$al->name_vi}}</option>
                                @endforeach
                                <?php
                                Session::put('name_vi','')
                                ?>
                            </select>

            </div>
            <div class="box_input">
              <div class="label">Mã quận huyện </div>
              <select  name="code_location" class='js-select2-multi form-control' >
                                <option value=''>Tất cả</option>
                                <?php
                                    $location=DB::table('locations')->where('type_id',0)->get();
                                ?>
                                @foreach($location as $al)
                                @if($al->code_location!=null)


                                    <option @if(Session::get('code_location')==$al->code_location) selected @endif value="{{$al->code_location}}"> {{$al->code_location}}</option>
                                    @endif

                                @endforeach
                                <?php
                                Session::put('code_location','')
                                ?>
                            </select>
            </div>
            <div class="box_input">
              <div class="label">Tỉnh thành phố </div>
              <select  name="type_id" class='js-select2-multi form-control' >
                                <option value=''>Tất cả</option>
                                <?php
                                    $location=DB::table('locations')->where('type_id',1)->get();

                                ?>
                                @foreach($location as $al)

                                    <option @if(Session::get('type_id')==$al->id) selected @endif value="{{$al->id}}">{{$al->name_vi}}</option>
                                  
                                @endforeach
                                <?php
                                Session::put('phone_code','')
                                ?>
                            </select>
            </div>
            
          </div>
          <div class="row_input">
            <div class="box_input">
            <div class="label">Quốc gia</div>
              <select  name="parent_id" class='js-select2-multi form-control' id="location_from">
                                <option value=''>Tất cả</option>
                                <?php
                                    $location=DB::table('locations')->where('parent_id',0)->get();
                                    ?>
                                @foreach($location as $al)


                                <option @if(Session::get('parent_id')==$al->id) selected @endif value="{{$al->id}}">{{$al->name_vi}}</option>
                            
                                @endforeach
                                <?php
                                Session::put('phone_code','')
                                ?>
                            </select>

            </div>
            <div class="box_input">
            <div class="label">Trạng thái </div>
            <select name='status'>

<option value='' @if(Session::get('status')==null) selected @endif >Tất cả</option>
<option value='0' @if(Session::get('status')==2) selected @endif >Đang hoạt động</option>
<option value='1' @if(Session::get('status')==1) selected @endif >Ngưng hoạt động</option>
</select>
            </div>
            
            
          </div>
        </div>
       
         
        </div>
        <div class="box_option">
          <button type="submit" style="text-decoration: none;border: 0px;" class="button_option">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" role="img" class="iconify iconify--carbon" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 32 32" data-icon="carbon:search" style="width: 20px; height: 20px; color: rgb(255, 255, 255);"><path fill="currentColor" d="m29 27.586l-7.552-7.552a11.018 11.018 0 1 0-1.414 1.414L27.586 29ZM4 13a9 9 0 1 1 9 9a9.01 9.01 0 0 1-9-9Z"></path></svg>
            <span class="name_button_option">Tìm kiếm</span>
          </button>
          <a href="{{asset('admin/website/list/quocgia')}}" class="btn button_clear" >
            Xoá bộ lọc
          </a>
        </div>
      </div>
        </form>
            <div class="wrapper">
               <div class="box_title">
                  <div class="title">{{Session::get('soluong')}} Quận huyện</div>
                  <a href="{{asset('admin/website/add/quanhuyen')}}"style="text-decoration: none;" class="button_option">
                  <span style="width: 20px; height: 20px; color: #fff;" class="iconify"
                     data-icon="fluent:add-16-filled"></span>
                  <span class="name_button_option">Thêm mới</span>
                  </a>
               </div>
               <div class="content">
                  <table>
                     <tr>
                        <th class="medium">Tên quận huyện</th>
                        <th class="medium">Mã quận huyện</th>
                        <th class="medium">Tỉnh / thành phố</th>
                        <th class="medium">Quốc gia</th>
                        <th class="medium"style="text-align:center">Trạng thái</th>
                        <th class="medium"style="text-align:center"> Chỉnh sửa </th>
                     </tr>
                     <?php
                        $i=0;
                        ?>
                     @foreach( $locations as $l)
                     <?php
                        $city=DB::table('locations')->where('id',$l->parent_id)->first();
                        if($city!=null){
                            $countries=DB::table('locations')->where('id',$city->parent_id)->first();
                        }else{
                            $countries=[];
                        }
                        
                        $i++;
                        ?>
                     <tr>
                        <td class="ma_khuyen_mai">
                           <div class="ten">{{$l->name_vi}} </div>
                        </td>
                        <td class="ma_khuyen_mai">
                           <div class="ten"> {{$l->code_location}}</div>
                        </td>
                        <td class="ma_khuyen_mai">
                           <div class="ten"> @if($city!=null){{$city->name_vi}} @endif</div>
                        </td>
                        <td class="ma_khuyen_mai">
                           <div class="ten">@if($countries!=null) {{$countries->name_vi}}@endif</div>
                        </td>
                        <input type="hidden" id="id_promo{{$i}}" value="{{$l->id}}">
                        <td class="ma_khuyen_mai" style="display: flex; align-items-center; justify-content-center; flex-direction: column">
                           @if($l->status==0)
                           <div class="text-center">
                              <label class="switch">
                              <input type="checkbox" checked name="graduate" id="check_qh{{$i}}">
                              <span class="slider round"></span>
                              </label>
                              <div class="col-md-12 ug mt-5" style="">
                              </div>
                           </div>
                           @else
                           <div class="text-center">
                              <label class="switch">
                              <input type="checkbox"  name="graduate" id="check_qh{{$i}}">
                              <span class="slider round"></span>
                              </label>
                              <div class="col-md-12 ug mt-5" style="">
                              </div>
                           </div>
                           @endif
                           <div class="popover_status{{$i}}" id='content{{$i}}'style="">
                              <div style="display: flex; algin-items-center: center">
                                 <span class="iconify" style="color: blue; width: 30px; height: 30px" data-icon="carbon:information-filled"></span>
                                 <span style="margin-left: 10px" id='datashowinfo{{$i}}'></span>
                              </div>
                              <div style="margin-top: 10px;margin-left: 39px">
                                 <button class="btn btn-light" id='cancel{{$i}}'>Hủy bỏ</button>
                                 <button class="btn btn-danger" id='ok_qh{{$i}}'style="margin-left: 10px">Đồng ý</button>
                              </div>
                           </div>
                        </td>
                        <td class="ma_khuyen_mai" style="text-align:center">
                           <div class="ten">
                              <a href='{{asset('admin/website/edit/quanhuyen/'.$l->id)}}'>
                              <div class="button_option_discount">
                                 <span style="width: 20px;height: 20px;" class="iconify" title="Chỉnh sửa" data-icon="clarity:note-edit-line"></span>
                              </div>
                              </a>
                              <!-- <div id="box_option_discount{{$i}}">
                                 <a href='{{asset('admin/website/del/quanhuyen/'.$l->id)}}'>
                                  <div class="button_option_discount">
                                     <span style="width: 20px;height: 20px;" class="iconify" data-icon="ion:trash-outline"></span>
                                     <div class="name"> Xoá</div>
                                  </div>
                                  </a>
                                  <div class="triangle"></div>
                                 </div> -->
                           </div>
                        </td>
                     </tr>
                     @endforeach
                  </table>
                  {{$locations->links('admin.paginate_quanhuyen')}}
               </div>
            </div>
         </div>
      </div>
   </body>
</html>
<style>
   .box_content .wrapper .content .row_input .box_input select {
    width: 100%;
    height: 35px;
    padding: 0px 10px;
    border: 1px solid #dee2e6;
    border-radius: 5px;
    margin-top: 0px!important;
    color: gray;
    font-size: 14px;
    height: 29px!important;
    }
</style>
<script>

    
   $(document).ready(function(){
     $('#check_qh1').change(function(){
                if($('#check_qh1:checked').is(":checked")){
                    $('.ug').hide();
         $("#content1").css('display','block');
                    $("#content2").css('display','none');
                    $("#content3").css('display','none');
                    $("#content4").css('display','none');
                    $("#content5").css('display','none');
                    $("#content6").css('display','none');
                    $("#content7").css('display','none');
                    $("#content8").css('display','none');
                    $("#content9").css('display','none');
                    $("#content10").css('display','none');
                    $("#content11").css('display','none');
                    $("#content12").css('display','none');
                    $("#content13").css('display','none');
                    $("#content14").css('display','none');
                    $("#content15").css('display','none');
                    $("#content16").css('display','none');
                    $("#content17").css('display','none');
                    $("#content18").css('display','none');
                    $("#content19").css('display','none');
                    $("#content20").css('display','none');
         $(".popover_status1").css('display','block');
         $(".popover_status2").css('display','none');
         $(".popover_status3").css('display','none');
         $(".popover_status4").css('display','none');
         $(".popover_status5").css('display','none');
         $(".popover_status6").css('display','none');
         $(".popover_status7").css('display','none');
         $(".popover_status8").css('display','none');
         $(".popover_status9").css('display','none');
         $(".popover_status10").css('display','none');
         $(".popover_status11").css('display','none');
         $(".popover_status12").css('display','none');
         $(".popover_status13").css('display','none');
         $(".popover_status14").css('display','none');
         $(".popover_status15").css('display','none');
         $(".popover_status16").css('display','none');
         $(".popover_status17").css('display','none');
         $(".popover_status18").css('display','none');
         $(".popover_status19").css('display','none');
         $(".popover_status20").css('display','none');
                    $('.phd').show();       
                                 $('#datashowinfo1').text("Bạn có chắc muốn hoạt động lại?");
   
                
                }else{
                    $('.ug').show();
                    $('.phd').hide();
         $(".popover_status1").css('display','block');
         $(".popover_status2").css('display','none');
         $(".popover_status3").css('display','none');
         $(".popover_status4").css('display','none');
         $(".popover_status5").css('display','none');
         $(".popover_status6").css('display','none');
         $(".popover_status7").css('display','none');
         $(".popover_status8").css('display','none');
         $(".popover_status9").css('display','none');
         $(".popover_status10").css('display','none');
         $(".popover_status11").css('display','none');
         $(".popover_status12").css('display','none');
         $(".popover_status13").css('display','none');
         $(".popover_status14").css('display','none');
         $(".popover_status15").css('display','none');
         $(".popover_status16").css('display','none');
         $(".popover_status17").css('display','none');
         $(".popover_status18").css('display','none');
         $(".popover_status19").css('display','none');
         $(".popover_status20").css('display','none');
   
                    $("#content1").css('display','block');
                    $("#content2").css('display','none');
                    $("#content3").css('display','none');
                    $("#content4").css('display','none');
                    $("#content5").css('display','none');
                    $("#content6").css('display','none');
                    $("#content7").css('display','none');
                    $("#content8").css('display','none');
                    $("#content9").css('display','none');
                    $("#content10").css('display','none');
                    $("#content11").css('display','none');
                    $("#content12").css('display','none');
                    $("#content13").css('display','none');
                    $("#content14").css('display','none');
                    $("#content15").css('display','none');
                    $("#content16").css('display','none');
                    $("#content17").css('display','none');
                    $("#content18").css('display','none');
                    $("#content19").css('display','none');
                    $("#content20").css('display','none');
                    $('#datashowinfo1').text("Bạn có chắc muốn ngưng hoạt động?");
   
                }
            });

            $("#cancel1").click(function(){
               location.reload();
   
            });
               $("#ok_qh1").click(function(){
                $("#content1").css('display','none');
                $("#content2").css('display','none');
                $("#content3").css('display','none');
                $("#content4").css('display','none');
                $("#content5").css('display','none');
                $("#content6").css('display','none');
                $("#content7").css('display','none');
                $("#content8").css('display','none');
                $("#content9").css('display','none');
                $("#content10").css('display','none');
                $("#content11").css('display','none');
                $("#content12").css('display','none');
                $("#content13").css('display','none');
                $("#content14").css('display','none');
                $("#content15").css('display','none');
                $("#content16").css('display','none');
                $("#content17").css('display','none');
                $("#content18").css('display','none');
                $("#content19").css('display','none');
                $("#content20").css('display','none');
              
   
            $.ajax({
                        url: "https://test.heyo.group/admin/website/onoffstatus",
                        type: "get",
                        dataType: "text",
                        data: {
                            id_promo: $('#id_promo1').val()
                        },
                        success: function (result) {
                            if (result != 'no') {
                                alert('Đã cập nhật thành công');
                                location.reload()
                            } else {
   
                                alert('Chưa tới ngày bắt đầu');
                                location.reload()
   
                            }
                        }
                    })
                })

        $('#check_qh2').change(function(){
            if($('#check_qh2:checked').is(":checked")){
                    $('.ug').hide();
         $(".popover_status1").css('display','none');
         $(".popover_status2").css('display','block');
         $(".popover_status3").css('display','none');
         $(".popover_status4").css('display','none');
         $(".popover_status5").css('display','none');
         $(".popover_status6").css('display','none');
         $(".popover_status7").css('display','none');
         $(".popover_status8").css('display','none');
         $(".popover_status9").css('display','none');
         $(".popover_status10").css('display','none');
         $(".popover_status11").css('display','none');
         $(".popover_status12").css('display','none');
         $(".popover_status13").css('display','none');
         $(".popover_status14").css('display','none');
         $(".popover_status15").css('display','none');
         $(".popover_status16").css('display','none');
         $(".popover_status17").css('display','none');
         $(".popover_status18").css('display','none');
         $(".popover_status19").css('display','none');
         $(".popover_status20").css('display','none');
   
         $("#content1").css('display','none');
                    $("#content2").css('display','block');
                    $("#content3").css('display','none');
                    $("#content4").css('display','none');
                    $("#content5").css('display','none');
                    $("#content6").css('display','none');
                    $("#content7").css('display','none');
                    $("#content8").css('display','none');
                    $("#content9").css('display','none');
                    $("#content10").css('display','none');
                    $("#content11").css('display','none');
                    $("#content12").css('display','none');
                    $("#content13").css('display','none');
                    $("#content14").css('display','none');
                    $("#content15").css('display','none');
                    $("#content16").css('display','none');
                    $("#content17").css('display','none');
                    $("#content18").css('display','none');
                    $("#content19").css('display','none');
                    $("#content20").css('display','none');
         $("#content2").css('display','block');
                    $('.phd').show();       
                                 $('#datashowinfo2').text("Bạn có chắc muốn hoạt động lại?");
                
                }else{
         $(".popover_status1").css('display','none');
         $(".popover_status2").css('display','block');
         $(".popover_status3").css('display','none');
         $(".popover_status4").css('display','none');
         $(".popover_status5").css('display','none');
         $(".popover_status6").css('display','none');
         $(".popover_status7").css('display','none');
         $(".popover_status8").css('display','none');
         $(".popover_status9").css('display','none');
         $(".popover_status10").css('display','none');
         $(".popover_status11").css('display','none');
         $(".popover_status12").css('display','none');
         $(".popover_status13").css('display','none');
         $(".popover_status14").css('display','none');
         $(".popover_status15").css('display','none');
         $(".popover_status16").css('display','none');
         $(".popover_status17").css('display','none');
         $(".popover_status18").css('display','none');
         $(".popover_status19").css('display','none');
         $(".popover_status20").css('display','none');
   
                    $('.ug').show(); 

                    $('.phd').hide();
                    $("#content1").css('display','none');
                    $("#content2").css('display','block');
                    $("#content3").css('display','none');
                    $("#content4").css('display','none');
                    $("#content5").css('display','none');
                    $("#content6").css('display','none');
                    $("#content7").css('display','none');
                    $("#content8").css('display','none');
                    $("#content9").css('display','none');
                    $("#content10").css('display','none');
                    $("#content11").css('display','none');
                    $("#content12").css('display','none');
                    $("#content13").css('display','none');
                    $("#content14").css('display','none');
                    $("#content15").css('display','none');
                    $("#content16").css('display','none');
                    $("#content17").css('display','none');
                    $("#content18").css('display','none');
                    $("#content19").css('display','none');
                    $("#content20").css('display','none');
                    $('#datashowinfo2').text("Bạn có chắc muốn ngưng hoạt động?");
   
                }
            });
            $("#cancel2").click(function(){
               location.reload();
   
            });
               $("#ok_qh2").click(function(){
                   
                $("#content1").css('display','none');
                $("#content2").css('display','none');
                $("#content3").css('display','none');
                $("#content4").css('display','none');
                $("#content5").css('display','none');
                $("#content6").css('display','none');
                $("#content7").css('display','none');
                $("#content8").css('display','none');
                $("#content9").css('display','none');
                $("#content10").css('display','none');
                $("#content11").css('display','none');
                $("#content12").css('display','none');
                $("#content13").css('display','none');
                $("#content14").css('display','none');
                $("#content15").css('display','none');
                $("#content16").css('display','none');
                $("#content17").css('display','none');
                $("#content18").css('display','none');
                $("#content19").css('display','none');
                $("#content20").css('display','none');
              
            $.ajax({
                        url: "https://test.heyo.group/admin/website/onoffstatus",
                        type: "get",
                        dataType: "text",
                        data: {
                            id_promo: $('#id_promo2').val()
                        },
                        success: function (result) {
                            if (result != 'no') {
                                alert('Đã cập nhật thành công');
                                location.reload()
                            } else {
   
                                alert('Chưa tới ngày bắt đầu');
                                location.reload()
   
                            }
                        }
                    })
         })
         $('#check_qh3').change(function(){
            if($('#check_qh3:checked').is(":checked")){
                    $('.ug').hide();
         $(".popover_status1").css('display','none');
         $(".popover_status3").css('display','block');
         $(".popover_status2").css('display','none');
         $(".popover_status4").css('display','none');
         $(".popover_status5").css('display','none');
         $(".popover_status6").css('display','none');
         $(".popover_status7").css('display','none');
         $(".popover_status8").css('display','none');
         $(".popover_status9").css('display','none');
         $(".popover_status10").css('display','none');
         $(".popover_status11").css('display','none');
         $(".popover_status12").css('display','none');
         $(".popover_status13").css('display','none');
         $(".popover_status14").css('display','none');
         $(".popover_status15").css('display','none');
         $(".popover_status16").css('display','none');
         $(".popover_status17").css('display','none');
         $(".popover_status18").css('display','none');
         $(".popover_status19").css('display','none');
         $(".popover_status20").css('display','none');
   
         $("#content1").css('display','none');
                    $("#content3").css('display','block');
                    $("#content2").css('display','none');
                    $("#content4").css('display','none');
                    $("#content5").css('display','none');
                    $("#content6").css('display','none');
                    $("#content7").css('display','none');
                    $("#content8").css('display','none');
                    $("#content9").css('display','none');
                    $("#content10").css('display','none');
                    $("#content11").css('display','none');
                    $("#content12").css('display','none');
                    $("#content13").css('display','none');
                    $("#content14").css('display','none');
                    $("#content15").css('display','none');
                    $("#content16").css('display','none');
                    $("#content17").css('display','none');
                    $("#content18").css('display','none');
                    $("#content19").css('display','none');
                    $("#content20").css('display','none');
         $("#content3").css('display','block');
                    $('.phd').show();       
                                 $('#datashowinfo3').text("Bạn có chắc muốn hoạt động lại?");
                
                }else{
         $(".popover_status1").css('display','none');
         $(".popover_status3").css('display','block');
         $(".popover_status2").css('display','none');
         $(".popover_status4").css('display','none');
         $(".popover_status5").css('display','none');
         $(".popover_status6").css('display','none');
         $(".popover_status7").css('display','none');
         $(".popover_status8").css('display','none');
         $(".popover_status9").css('display','none');
         $(".popover_status10").css('display','none');
         $(".popover_status11").css('display','none');
         $(".popover_status12").css('display','none');
         $(".popover_status13").css('display','none');
         $(".popover_status14").css('display','none');
         $(".popover_status15").css('display','none');
         $(".popover_status16").css('display','none');
         $(".popover_status17").css('display','none');
         $(".popover_status18").css('display','none');
         $(".popover_status19").css('display','none');
         $(".popover_status20").css('display','none');
   
                    $('.ug').show();
                    $('.phd').hide();
                    $("#content1").css('display','none');
                    $("#content3").css('display','block');
                    $("#content2").css('display','none');
                    $("#content4").css('display','none');
                    $("#content5").css('display','none');
                    $("#content6").css('display','none');
                    $("#content7").css('display','none');
                    $("#content8").css('display','none');
                    $("#content9").css('display','none');
                    $("#content10").css('display','none');
                    $("#content11").css('display','none');
                    $("#content12").css('display','none');
                    $("#content13").css('display','none');
                    $("#content14").css('display','none');
                    $("#content15").css('display','none');
                    $("#content16").css('display','none');
                    $("#content17").css('display','none');
                    $("#content18").css('display','none');
                    $("#content19").css('display','none');
                    $("#content20").css('display','none');
                    $('#datashowinfo3').text("Bạn có chắc muốn ngưng hoạt động?");
   
                }
            });
            $("#cancel3").click(function(){
               location.reload();
   
            });
               $("#ok_qh3").click(function(){
                   
                $("#content1").css('display','none');
                $("#content2").css('display','none');
                $("#content3").css('display','none');
                $("#content4").css('display','none');
                $("#content5").css('display','none');
                $("#content6").css('display','none');
                $("#content7").css('display','none');
                $("#content8").css('display','none');
                $("#content9").css('display','none');
                $("#content10").css('display','none');
                $("#content11").css('display','none');
                $("#content12").css('display','none');
                $("#content13").css('display','none');
                $("#content14").css('display','none');
                $("#content15").css('display','none');
                $("#content16").css('display','none');
                $("#content17").css('display','none');
                $("#content18").css('display','none');
                $("#content19").css('display','none');
                $("#content20").css('display','none');
              
            $.ajax({
                        url: "https://test.heyo.group/admin/website/onoffstatus",
                        type: "get",
                        dataType: "text",
                        data: {
                            id_promo: $('#id_promo3').val()
                        },
                        success: function (result) {
                            if (result != 'no') {
                                alert('Đã cập nhật thành công');
                                location.reload()
                            } else {
   
                                alert('Chưa tới ngày bắt đầu');
                                location.reload()
   
                            }
                        }
                    })
         })
         
         $('#check_qh4').change(function(){
            if($('#check_qh4:checked').is(":checked")){
                    $('.ug').hide();
         $(".popover_status1").css('display','none');
         $(".popover_status4").css('display','block');
         $(".popover_status2").css('display','none');
         $(".popover_status4").css('display','none');
         $(".popover_status5").css('display','none');
         $(".popover_status6").css('display','none');
         $(".popover_status7").css('display','none');
         $(".popover_status8").css('display','none');
         $(".popover_status9").css('display','none');
         $(".popover_status10").css('display','none');
         $(".popover_status11").css('display','none');
         $(".popover_status12").css('display','none');
         $(".popover_status13").css('display','none');
         $(".popover_status14").css('display','none');
         $(".popover_status15").css('display','none');
         $(".popover_status16").css('display','none');
         $(".popover_status17").css('display','none');
         $(".popover_status18").css('display','none');
         $(".popover_status19").css('display','none');
         $(".popover_status20").css('display','none');
   
         $("#content1").css('display','none');
                    $("#content4").css('display','block');
                    $("#content2").css('display','none');
                    $("#content3").css('display','none');
                    $("#content5").css('display','none');
                    $("#content6").css('display','none');
                    $("#content7").css('display','none');
                    $("#content8").css('display','none');
                    $("#content9").css('display','none');
                    $("#content10").css('display','none');
                    $("#content11").css('display','none');
                    $("#content12").css('display','none');
                    $("#content13").css('display','none');
                    $("#content14").css('display','none');
                    $("#content15").css('display','none');
                    $("#content16").css('display','none');
                    $("#content17").css('display','none');
                    $("#content18").css('display','none');
                    $("#content19").css('display','none');
                    $("#content20").css('display','none');
                    $('.phd').show();       
                                 $('#datashowinfo4').text("Bạn có chắc muốn hoạt động lại?");
                
                }else{
         $(".popover_status1").css('display','none');
         $(".popover_status4").css('display','block');
         $(".popover_status2").css('display','none');
         $(".popover_status3").css('display','none');
         $(".popover_status5").css('display','none');
         $(".popover_status6").css('display','none');
         $(".popover_status7").css('display','none');
         $(".popover_status8").css('display','none');
         $(".popover_status9").css('display','none');
         $(".popover_status10").css('display','none');
         $(".popover_status11").css('display','none');
         $(".popover_status12").css('display','none');
         $(".popover_status13").css('display','none');
         $(".popover_status14").css('display','none');
         $(".popover_status15").css('display','none');
         $(".popover_status16").css('display','none');
         $(".popover_status17").css('display','none');
         $(".popover_status18").css('display','none');
         $(".popover_status19").css('display','none');
         $(".popover_status20").css('display','none');
   
                    $('.ug').show();
                    $('.phd').hide();
                    $("#content1").css('display','none');
                    $("#content4").css('display','block');
                    $("#content2").css('display','none');
                    $("#content3").css('display','none');
                    $("#content5").css('display','none');
                    $("#content6").css('display','none');
                    $("#content7").css('display','none');
                    $("#content8").css('display','none');
                    $("#content9").css('display','none');
                    $("#content10").css('display','none');
                    $("#content11").css('display','none');
                    $("#content12").css('display','none');
                    $("#content13").css('display','none');
                    $("#content14").css('display','none');
                    $("#content15").css('display','none');
                    $("#content16").css('display','none');
                    $("#content17").css('display','none');
                    $("#content18").css('display','none');
                    $("#content19").css('display','none');
                    $("#content20").css('display','none');
                    $('#datashowinfo4').text("Bạn có chắc muốn ngưng hoạt động?");
   
                }
            });
            $("#cancel4").click(function(){
               location.reload();
   
            });
               $("#ok_qh4").click(function(){
                   
                $("#content1").css('display','none');
                $("#content2").css('display','none');
                $("#content3").css('display','none');
                $("#content4").css('display','none');
                $("#content5").css('display','none');
                $("#content6").css('display','none');
                $("#content7").css('display','none');
                $("#content8").css('display','none');
                $("#content9").css('display','none');
                $("#content10").css('display','none');
                $("#content11").css('display','none');
                $("#content12").css('display','none');
                $("#content13").css('display','none');
                $("#content14").css('display','none');
                $("#content15").css('display','none');
                $("#content16").css('display','none');
                $("#content17").css('display','none');
                $("#content18").css('display','none');
                $("#content19").css('display','none');
                $("#content20").css('display','none');
              
            $.ajax({
                        url: "https://test.heyo.group/admin/website/onoffstatus",
                        type: "get",
                        dataType: "text",
                        data: {
                            id_promo: $('#id_promo4').val()
                        },
                        success: function (result) {
                            if (result != 'no') {
                                alert('Đã cập nhật thành công');
                                location.reload()
                            } else {
   
                                alert('Chưa tới ngày bắt đầu');
                                location.reload()
   
                            }
                        }
                    })
         })
         
         $('#check_qh5').change(function(){
            if($('#check_qh5:checked').is(":checked")){
                    $('.ug').hide();
         $(".popover_status1").css('display','none');
         $(".popover_status5").css('display','block');
         $(".popover_status2").css('display','none');
         $(".popover_status3").css('display','none');
         $(".popover_status4").css('display','none');
         $(".popover_status6").css('display','none');
         $(".popover_status7").css('display','none');
         $(".popover_status8").css('display','none');
         $(".popover_status9").css('display','none');
         $(".popover_status10").css('display','none');
         $(".popover_status11").css('display','none');
         $(".popover_status12").css('display','none');
         $(".popover_status13").css('display','none');
         $(".popover_status14").css('display','none');
         $(".popover_status15").css('display','none');
         $(".popover_status16").css('display','none');
         $(".popover_status17").css('display','none');
         $(".popover_status18").css('display','none');
         $(".popover_status19").css('display','none');
         $(".popover_status20").css('display','none');
   
         $("#content1").css('display','none');
                    $("#content5").css('display','block');
                    $("#content2").css('display','none');
                    $("#content3").css('display','none');
                    $("#content4").css('display','none');
                    $("#content6").css('display','none');
                    $("#content7").css('display','none');
                    $("#content8").css('display','none');
                    $("#content9").css('display','none');
                    $("#content10").css('display','none');
                    $("#content11").css('display','none');
                    $("#content12").css('display','none');
                    $("#content13").css('display','none');
                    $("#content14").css('display','none');
                    $("#content15").css('display','none');
                    $("#content16").css('display','none');
                    $("#content17").css('display','none');
                    $("#content18").css('display','none');
                    $("#content19").css('display','none');
                    $("#content20").css('display','none');
         $("#content5").css('display','block');
                    $('.phd').show();       
                                 $('#datashowinfo5').text("Bạn có chắc muốn hoạt động lại?");
                
                }else{
         $(".popover_status1").css('display','none');
         $(".popover_status5").css('display','block');
         $(".popover_status2").css('display','none');
         $(".popover_status3").css('display','none');
         $(".popover_status4").css('display','none');
         $(".popover_status6").css('display','none');
         $(".popover_status7").css('display','none');
         $(".popover_status8").css('display','none');
         $(".popover_status9").css('display','none');
         $(".popover_status10").css('display','none');
         $(".popover_status11").css('display','none');
         $(".popover_status12").css('display','none');
         $(".popover_status13").css('display','none');
         $(".popover_status14").css('display','none');
         $(".popover_status15").css('display','none');
         $(".popover_status16").css('display','none');
         $(".popover_status17").css('display','none');
         $(".popover_status18").css('display','none');
         $(".popover_status19").css('display','none');
         $(".popover_status20").css('display','none');
   
                    $('.ug').show();
                    $('.phd').hide();
                    $("#content1").css('display','none');
                    $("#content5").css('display','block');
                    $("#content2").css('display','none');
                    $("#content3").css('display','none');
                    $("#content4").css('display','none');
                    $("#content6").css('display','none');
                    $("#content7").css('display','none');
                    $("#content8").css('display','none');
                    $("#content9").css('display','none');
                    $("#content10").css('display','none');
                    $("#content11").css('display','none');
                    $("#content12").css('display','none');
                    $("#content13").css('display','none');
                    $("#content14").css('display','none');
                    $("#content15").css('display','none');
                    $("#content16").css('display','none');
                    $("#content17").css('display','none');
                    $("#content18").css('display','none');
                    $("#content19").css('display','none');
                    $("#content20").css('display','none');
                    $('#datashowinfo5').text("Bạn có chắc muốn ngưng hoạt động?");
   
                }
            });
            $("#cancel5").click(function(){
               location.reload();
   
            });
               $("#ok_qh5").click(function(){
                   
                $("#content1").css('display','none');
                $("#content2").css('display','none');
                $("#content3").css('display','none');
                $("#content4").css('display','none');
                $("#content5").css('display','none');
                $("#content6").css('display','none');
                $("#content7").css('display','none');
                $("#content8").css('display','none');
                $("#content9").css('display','none');
                $("#content10").css('display','none');
                $("#content11").css('display','none');
                $("#content12").css('display','none');
                $("#content13").css('display','none');
                $("#content14").css('display','none');
                $("#content15").css('display','none');
                $("#content16").css('display','none');
                $("#content17").css('display','none');
                $("#content18").css('display','none');
                $("#content19").css('display','none');
                $("#content20").css('display','none');
              
            $.ajax({
                        url: "https://test.heyo.group/admin/website/onoffstatus",
                        type: "get",
                        dataType: "text",
                        data: {
                            id_promo: $('#id_promo5').val()
                        },
                        success: function (result) {
                            if (result != 'no') {
                                alert('Đã cập nhật thành công');
                                location.reload()
                            } else {
   
                                alert('Chưa tới ngày bắt đầu');
                                location.reload()
   
                            }
                        }
                    })
         })
         $('#check_qh6').change(function(){
            if($('#check_qh6:checked').is(":checked")){
                    $('.ug').hide();
         $(".popover_status1").css('display','none');
         $(".popover_status6").css('display','block');
         $(".popover_status2").css('display','none');
         $(".popover_status3").css('display','none');
         $(".popover_status4").css('display','none');
         $(".popover_status5").css('display','none');
         $(".popover_status7").css('display','none');
         $(".popover_status8").css('display','none');
         $(".popover_status9").css('display','none');
         $(".popover_status10").css('display','none');
         $(".popover_status11").css('display','none');
         $(".popover_status12").css('display','none');
         $(".popover_status13").css('display','none');
         $(".popover_status14").css('display','none');
         $(".popover_status15").css('display','none');
         $(".popover_status16").css('display','none');
         $(".popover_status17").css('display','none');
         $(".popover_status18").css('display','none');
         $(".popover_status19").css('display','none');
         $(".popover_status20").css('display','none');
   
         $("#content1").css('display','none');
                    $("#content6").css('display','block');
                    $("#content2").css('display','none');
                    $("#content3").css('display','none');
                    $("#content4").css('display','none');
                    $("#content5").css('display','none');
                    $("#content7").css('display','none');
                    $("#content8").css('display','none');
                    $("#content9").css('display','none');
                    $("#content10").css('display','none');
                    $("#content11").css('display','none');
                    $("#content12").css('display','none');
                    $("#content13").css('display','none');
                    $("#content14").css('display','none');
                    $("#content15").css('display','none');
                    $("#content16").css('display','none');
                    $("#content17").css('display','none');
                    $("#content18").css('display','none');
                    $("#content19").css('display','none');
                    $("#content20").css('display','none');
         $("#content6").css('display','block');
                    $('.phd').show();       
                                 $('#datashowinfo6').text("Bạn có chắc muốn hoạt động lại?");
                
                }else{
         $(".popover_status1").css('display','none');
         $(".popover_status6").css('display','block');
         $(".popover_status2").css('display','none');
         $(".popover_status3").css('display','none');
         $(".popover_status4").css('display','none');
         $(".popover_status5").css('display','none');
         $(".popover_status7").css('display','none');
         $(".popover_status8").css('display','none');
         $(".popover_status9").css('display','none');
         $(".popover_status10").css('display','none');
         $(".popover_status11").css('display','none');
         $(".popover_status12").css('display','none');
         $(".popover_status13").css('display','none');
         $(".popover_status14").css('display','none');
         $(".popover_status15").css('display','none');
         $(".popover_status16").css('display','none');
         $(".popover_status17").css('display','none');
         $(".popover_status18").css('display','none');
         $(".popover_status19").css('display','none');
         $(".popover_status20").css('display','none');
   
                    $('.ug').show();
                    $('.phd').hide();
                    $("#content1").css('display','none');
                    $("#content6").css('display','block');
                    $("#content2").css('display','none');
                    $("#content3").css('display','none');
                    $("#content4").css('display','none');
                    $("#content5").css('display','none');
                    $("#content7").css('display','none');
                    $("#content8").css('display','none');
                    $("#content9").css('display','none');
                    $("#content10").css('display','none');
                    $("#content11").css('display','none');
                    $("#content12").css('display','none');
                    $("#content13").css('display','none');
                    $("#content14").css('display','none');
                    $("#content15").css('display','none');
                    $("#content16").css('display','none');
                    $("#content17").css('display','none');
                    $("#content18").css('display','none');
                    $("#content19").css('display','none');
                    $("#content20").css('display','none');
                    $('#datashowinfo6').text("Bạn có chắc muốn ngưng hoạt động?");
   
                }
            });
            $("#cancel6").click(function(){
               location.reload();
   
            });
               $("#ok_qh6").click(function(){
                   
                $("#content1").css('display','none');
                $("#content2").css('display','none');
                $("#content3").css('display','none');
                $("#content4").css('display','none');
                $("#content5").css('display','none');
                $("#content6").css('display','none');
                $("#content7").css('display','none');
                $("#content8").css('display','none');
                $("#content9").css('display','none');
                $("#content10").css('display','none');
                $("#content11").css('display','none');
                $("#content12").css('display','none');
                $("#content13").css('display','none');
                $("#content14").css('display','none');
                $("#content15").css('display','none');
                $("#content16").css('display','none');
                $("#content17").css('display','none');
                $("#content18").css('display','none');
                $("#content19").css('display','none');
                $("#content20").css('display','none');
              
            $.ajax({
                        url: "https://test.heyo.group/admin/website/onoffstatus",
                        type: "get",
                        dataType: "text",
                        data: {
                            id_promo: $('#id_promo6').val()
                        },
                        success: function (result) {
                            if (result != 'no') {
                                alert('Đã cập nhật thành công');
                                location.reload()
                            } else {
   
                                alert('Chưa tới ngày bắt đầu');
                                location.reload()
   
                            }
                        }
                    })
         })
         $('#check_qh7').change(function(){
            if($('#check_qh7:checked').is(":checked")){
                    $('.ug').hide();
         $(".popover_status1").css('display','none');
         $(".popover_status7").css('display','block');
         $(".popover_status2").css('display','none');
         $(".popover_status3").css('display','none');
         $(".popover_status4").css('display','none');
         $(".popover_status5").css('display','none');
         $(".popover_status6").css('display','none');
         $(".popover_status8").css('display','none');
         $(".popover_status9").css('display','none');
         $(".popover_status10").css('display','none');
         $(".popover_status11").css('display','none');
         $(".popover_status12").css('display','none');
         $(".popover_status13").css('display','none');
         $(".popover_status14").css('display','none');
         $(".popover_status15").css('display','none');
         $(".popover_status16").css('display','none');
         $(".popover_status17").css('display','none');
         $(".popover_status18").css('display','none');
         $(".popover_status19").css('display','none');
         $(".popover_status20").css('display','none');
   
         $("#content1").css('display','none');
                    $("#content7").css('display','block');
                    $("#content2").css('display','none');
                    $("#content3").css('display','none');
                    $("#content4").css('display','none');
                    $("#content5").css('display','none');
                    $("#content6").css('display','none');
                    $("#content8").css('display','none');
                    $("#content9").css('display','none');
                    $("#content10").css('display','none');
                    $("#content11").css('display','none');
                    $("#content12").css('display','none');
                    $("#content13").css('display','none');
                    $("#content14").css('display','none');
                    $("#content15").css('display','none');
                    $("#content16").css('display','none');
                    $("#content17").css('display','none');
                    $("#content18").css('display','none');
                    $("#content19").css('display','none');
                    $("#content20").css('display','none');
         $("#content7").css('display','block');
                    $('.phd').show();       
                                 $('#datashowinfo7').text("Bạn có chắc muốn hoạt động lại?");
                
                }else{
         $(".popover_status1").css('display','none');
         $(".popover_status7").css('display','block');
         $(".popover_status2").css('display','none');
         $(".popover_status3").css('display','none');
         $(".popover_status4").css('display','none');
         $(".popover_status5").css('display','none');
         $(".popover_status6").css('display','none');
         $(".popover_status8").css('display','none');
         $(".popover_status9").css('display','none');
         $(".popover_status10").css('display','none');
         $(".popover_status11").css('display','none');
         $(".popover_status12").css('display','none');
         $(".popover_status13").css('display','none');
         $(".popover_status14").css('display','none');
         $(".popover_status15").css('display','none');
         $(".popover_status16").css('display','none');
         $(".popover_status17").css('display','none');
         $(".popover_status18").css('display','none');
         $(".popover_status19").css('display','none');
         $(".popover_status20").css('display','none');
   
                    $('.ug').show();
                    $('.phd').hide();
                    $("#content1").css('display','none');
                    $("#content7").css('display','block');
                    $("#content2").css('display','none');
                    $("#content3").css('display','none');
                    $("#content4").css('display','none');
                    $("#content5").css('display','none');
                    $("#content6").css('display','none');
                    $("#content8").css('display','none');
                    $("#content9").css('display','none');
                    $("#content10").css('display','none');
                    $("#content11").css('display','none');
                    $("#content12").css('display','none');
                    $("#content13").css('display','none');
                    $("#content14").css('display','none');
                    $("#content15").css('display','none');
                    $("#content16").css('display','none');
                    $("#content17").css('display','none');
                    $("#content18").css('display','none');
                    $("#content19").css('display','none');
                    $("#content20").css('display','none');
                    $('#datashowinfo7').text("Bạn có chắc muốn ngưng hoạt động?");
   
                }
            });
            $("#cancel7").click(function(){
               location.reload();
   
            });
               $("#ok_qh7").click(function(){
                   
                $("#content1").css('display','none');
                $("#content2").css('display','none');
                $("#content3").css('display','none');
                $("#content4").css('display','none');
                $("#content5").css('display','none');
                $("#content6").css('display','none');
                $("#content7").css('display','none');
                $("#content8").css('display','none');
                $("#content9").css('display','none');
                $("#content10").css('display','none');
                $("#content11").css('display','none');
                $("#content12").css('display','none');
                $("#content13").css('display','none');
                $("#content14").css('display','none');
                $("#content15").css('display','none');
                $("#content16").css('display','none');
                $("#content17").css('display','none');
                $("#content18").css('display','none');
                $("#content19").css('display','none');
                $("#content20").css('display','none');
              
            $.ajax({
                        url: "https://test.heyo.group/admin/website/onoffstatus",
                        type: "get",
                        dataType: "text",
                        data: {
                            id_promo: $('#id_promo7').val()
                        },
                        success: function (result) {
                            if (result != 'no') {
                                alert('Đã cập nhật thành công');
                                location.reload()
                            } else {
   
                                alert('Chưa tới ngày bắt đầu');
                                location.reload()
   
                            }
                        }
                    })
                })

                $('#check_qh8').change(function(){
            if($('#check_qh8:checked').is(":checked")){
                    $('.ug').hide();
         $(".popover_status1").css('display','none');
         $(".popover_status8").css('display','block');
         $(".popover_status2").css('display','none');
         $(".popover_status3").css('display','none');
         $(".popover_status4").css('display','none');
         $(".popover_status5").css('display','none');
         $(".popover_status6").css('display','none');
         $(".popover_status7").css('display','none');
         $(".popover_status9").css('display','none');
         $(".popover_status10").css('display','none');
         $(".popover_status11").css('display','none');
         $(".popover_status12").css('display','none');
         $(".popover_status13").css('display','none');
         $(".popover_status14").css('display','none');
         $(".popover_status15").css('display','none');
         $(".popover_status16").css('display','none');
         $(".popover_status17").css('display','none');
         $(".popover_status18").css('display','none');
         $(".popover_status19").css('display','none');
         $(".popover_status20").css('display','none');
   
         $("#content1").css('display','none');
                    $("#content8").css('display','block');
                    $("#content2").css('display','none');
                    $("#content3").css('display','none');
                    $("#content4").css('display','none');
                    $("#content5").css('display','none');
                    $("#content6").css('display','none');
                    $("#content7").css('display','none');
                    $("#content9").css('display','none');
                    $("#content10").css('display','none');
                    $("#content11").css('display','none');
                    $("#content12").css('display','none');
                    $("#content13").css('display','none');
                    $("#content14").css('display','none');
                    $("#content15").css('display','none');
                    $("#content16").css('display','none');
                    $("#content17").css('display','none');
                    $("#content18").css('display','none');
                    $("#content19").css('display','none');
                    $("#content20").css('display','none');
         $("#content8").css('display','block');
                    $('.phd').show();       
                                 $('#datashowinfo8').text("Bạn có chắc muốn hoạt động lại?");
                
                }else{
         $(".popover_status1").css('display','none');
         $(".popover_status8").css('display','block');
         $(".popover_status2").css('display','none');
         $(".popover_status3").css('display','none');
         $(".popover_status4").css('display','none');
         $(".popover_status5").css('display','none');
         $(".popover_status6").css('display','none');
         $(".popover_status7").css('display','none');
         $(".popover_status9").css('display','none');
         $(".popover_status10").css('display','none');
         $(".popover_status11").css('display','none');
         $(".popover_status12").css('display','none');
         $(".popover_status13").css('display','none');
         $(".popover_status14").css('display','none');
         $(".popover_status15").css('display','none');
         $(".popover_status16").css('display','none');
         $(".popover_status17").css('display','none');
         $(".popover_status18").css('display','none');
         $(".popover_status19").css('display','none');
         $(".popover_status20").css('display','none');
   
                    $('.ug').show();
                    $('.phd').hide();
                    $("#content1").css('display','none');
                    $("#content8").css('display','block');
                    $("#content2").css('display','none');
                    $("#content3").css('display','none');
                    $("#content4").css('display','none');
                    $("#content5").css('display','none');
                    $("#content6").css('display','none');
                    $("#content7").css('display','none');
                    $("#content9").css('display','none');
                    $("#content10").css('display','none');
                    $("#content11").css('display','none');
                    $("#content12").css('display','none');
                    $("#content13").css('display','none');
                    $("#content14").css('display','none');
                    $("#content15").css('display','none');
                    $("#content16").css('display','none');
                    $("#content17").css('display','none');
                    $("#content18").css('display','none');
                    $("#content19").css('display','none');
                    $("#content20").css('display','none');
                    $('#datashowinfo8').text("Bạn có chắc muốn ngưng hoạt động?");
   
                }
            });
            $("#cancel8").click(function(){
               location.reload();
   
            });
               $("#ok_qh8").click(function(){
                   
                $("#content1").css('display','none');
                $("#content2").css('display','none');
                $("#content3").css('display','none');
                $("#content4").css('display','none');
                $("#content5").css('display','none');
                $("#content6").css('display','none');
                $("#content7").css('display','none');
                $("#content8").css('display','none');
                $("#content9").css('display','none');
                $("#content10").css('display','none');
                $("#content11").css('display','none');
                $("#content12").css('display','none');
                $("#content13").css('display','none');
                $("#content14").css('display','none');
                $("#content15").css('display','none');
                $("#content16").css('display','none');
                $("#content17").css('display','none');
                $("#content18").css('display','none');
                $("#content19").css('display','none');
                $("#content20").css('display','none');
              
            $.ajax({
                        url: "https://test.heyo.group/admin/website/onoffstatus",
                        type: "get",
                        dataType: "text",
                        data: {
                            id_promo: $('#id_promo8').val()
                        },
                        success: function (result) {
                            if (result != 'no') {
                                alert('Đã cập nhật thành công');
                                location.reload()
                            } else {
   
                                alert('Chưa tới ngày bắt đầu');
                                location.reload()
   
                            }
                        }
                    })
         })
         $('#check_qh9').change(function(){
            if($('#check_qh9:checked').is(":checked")){
                    $('.ug').hide();
         $(".popover_status1").css('display','none');
         $(".popover_status9").css('display','block');
         $(".popover_status2").css('display','none');
         $(".popover_status3").css('display','none');
         $(".popover_status4").css('display','none');
         $(".popover_status5").css('display','none');
         $(".popover_status6").css('display','none');
         $(".popover_status7").css('display','none');
         $(".popover_status8").css('display','none');
         $(".popover_status10").css('display','none');
         $(".popover_status11").css('display','none');
         $(".popover_status12").css('display','none');
         $(".popover_status13").css('display','none');
         $(".popover_status14").css('display','none');
         $(".popover_status15").css('display','none');
         $(".popover_status16").css('display','none');
         $(".popover_status17").css('display','none');
         $(".popover_status19").css('display','none');
         $(".popover_status19").css('display','none');
         $(".popover_status20").css('display','none');
   
         $("#content1").css('display','none');
                    $("#content9").css('display','block');
                    $("#content2").css('display','none');
                    $("#content3").css('display','none');
                    $("#content4").css('display','none');
                    $("#content5").css('display','none');
                    $("#content6").css('display','none');
                    $("#content7").css('display','none');
                    $("#content8").css('display','none');
                    $("#content10").css('display','none');
                    $("#content11").css('display','none');
                    $("#content12").css('display','none');
                    $("#content13").css('display','none');
                    $("#content14").css('display','none');
                    $("#content15").css('display','none');
                    $("#content16").css('display','none');
                    $("#content17").css('display','none');
                    $("#content19").css('display','none');
                    $("#content19").css('display','none');
                    $("#content20").css('display','none');
         $("#content9").css('display','block');
                    $('.phd').show();       
                                 $('#datashowinfo9').text("Bạn có chắc muốn hoạt động lại?");
                
                }else{
         $(".popover_status1").css('display','none');
         $(".popover_status9").css('display','block');
         $(".popover_status2").css('display','none');
         $(".popover_status3").css('display','none');
         $(".popover_status4").css('display','none');
         $(".popover_status5").css('display','none');
         $(".popover_status6").css('display','none');
         $(".popover_status7").css('display','none');
         $(".popover_status8").css('display','none');
         $(".popover_status10").css('display','none');
         $(".popover_status11").css('display','none');
         $(".popover_status12").css('display','none');
         $(".popover_status13").css('display','none');
         $(".popover_status14").css('display','none');
         $(".popover_status15").css('display','none');
         $(".popover_status16").css('display','none');
         $(".popover_status17").css('display','none');
         $(".popover_status19").css('display','none');
         $(".popover_status19").css('display','none');
         $(".popover_status20").css('display','none');
   
                    $('.ug').show();
                    $('.phd').hide();
                    $("#content1").css('display','none');
                    $("#content9").css('display','block');
                    $("#content2").css('display','none');
                    $("#content3").css('display','none');
                    $("#content4").css('display','none');
                    $("#content5").css('display','none');
                    $("#content6").css('display','none');
                    $("#content7").css('display','none');
                    $("#content8").css('display','none');
                    $("#content10").css('display','none');
                    $("#content11").css('display','none');
                    $("#content12").css('display','none');
                    $("#content13").css('display','none');
                    $("#content14").css('display','none');
                    $("#content15").css('display','none');
                    $("#content16").css('display','none');
                    $("#content17").css('display','none');
                    $("#content19").css('display','none');
                    $("#content19").css('display','none');
                    $("#content20").css('display','none');
                    $('#datashowinfo9').text("Bạn có chắc muốn ngưng hoạt động?");
   
                }
            });
            $("#cancel9").click(function(){
               location.reload();
   
            });
               $("#ok_qh9").click(function(){
                   
                $("#content1").css('display','none');
                $("#content2").css('display','none');
                $("#content3").css('display','none');
                $("#content4").css('display','none');
                $("#content5").css('display','none');
                $("#content6").css('display','none');
                $("#content7").css('display','none');
                $("#content8").css('display','none');
                $("#content9").css('display','none');
                $("#content10").css('display','none');
                $("#content11").css('display','none');
                $("#content12").css('display','none');
                $("#content13").css('display','none');
                $("#content14").css('display','none');
                $("#content15").css('display','none');
                $("#content16").css('display','none');
                $("#content17").css('display','none');
                $("#content18").css('display','none');
                $("#content19").css('display','none');
                $("#content20").css('display','none');
              
            $.ajax({
                        url: "https://test.heyo.group/admin/website/onoffstatus",
                        type: "get",
                        dataType: "text",
                        data: {
                            id_promo: $('#id_promo9').val()
                        },
                        success: function (result) {
                            if (result != 'no') {
                                alert('Đã cập nhật thành công');
                                location.reload()
                            } else {
   
                                alert('Chưa tới ngày bắt đầu');
                                location.reload()
   
                            }
                        }
                    })
                    })
                    $('#check_qh10').change(function(){
            if($('#check_qh10:checked').is(":checked")){
                    $('.ug').hide();
         $(".popover_status1").css('display','none');
         $(".popover_status10").css('display','block');
         $(".popover_status2").css('display','none');
         $(".popover_status3").css('display','none');
         $(".popover_status4").css('display','none');
         $(".popover_status5").css('display','none');
         $(".popover_status6").css('display','none');
         $(".popover_status7").css('display','none');
         $(".popover_status8").css('display','none');
         $(".popover_status9").css('display','none');
         $(".popover_status11").css('display','none');
         $(".popover_status12").css('display','none');
         $(".popover_status13").css('display','none');
         $(".popover_status14").css('display','none');
         $(".popover_status15").css('display','none');
         $(".popover_status16").css('display','none');
         $(".popover_status17").css('display','none');
         $(".popover_status18").css('display','none');
         $(".popover_status19").css('display','none');
         $(".popover_status20").css('display','none');
   
         $("#content1").css('display','none');
                    $("#content10").css('display','block');
                    $("#content2").css('display','none');
                    $("#content3").css('display','none');
                    $("#content4").css('display','none');
                    $("#content5").css('display','none');
                    $("#content6").css('display','none');
                    $("#content7").css('display','none');
                    $("#content8").css('display','none');
                    $("#content9").css('display','none');
                    $("#content11").css('display','none');
                    $("#content12").css('display','none');
                    $("#content13").css('display','none');
                    $("#content14").css('display','none');
                    $("#content15").css('display','none');
                    $("#content16").css('display','none');
                    $("#content17").css('display','none');
                    $("#content18").css('display','none');
                    $("#content19").css('display','none');
                    $("#content20").css('display','none');
         $("#content10").css('display','block');
                    $('.phd').show();       
                                 $('#datashowinfo10').text("Bạn có chắc muốn hoạt động lại?");
                
                }else{
         $(".popover_status1").css('display','none');
         $(".popover_status10").css('display','block');
         $(".popover_status2").css('display','none');
         $(".popover_status3").css('display','none');
         $(".popover_status4").css('display','none');
         $(".popover_status5").css('display','none');
         $(".popover_status6").css('display','none');
         $(".popover_status7").css('display','none');
         $(".popover_status8").css('display','none');
         $(".popover_status9").css('display','none');
         $(".popover_status11").css('display','none');
         $(".popover_status12").css('display','none');
         $(".popover_status13").css('display','none');
         $(".popover_status14").css('display','none');
         $(".popover_status15").css('display','none');
         $(".popover_status16").css('display','none');
         $(".popover_status17").css('display','none');
         $(".popover_status18").css('display','none');
         $(".popover_status19").css('display','none');
         $(".popover_status20").css('display','none');
   
                    $('.ug').show();
                    $('.phd').hide();
                    $("#content1").css('display','none');
                    $("#content10").css('display','block');
                    $("#content2").css('display','none');
                    $("#content3").css('display','none');
                    $("#content4").css('display','none');
                    $("#content5").css('display','none');
                    $("#content6").css('display','none');
                    $("#content7").css('display','none');
                    $("#content8").css('display','none');
                    $("#content9").css('display','none');
                    $("#content11").css('display','none');
                    $("#content12").css('display','none');
                    $("#content13").css('display','none');
                    $("#content14").css('display','none');
                    $("#content15").css('display','none');
                    $("#content16").css('display','none');
                    $("#content17").css('display','none');
                    $("#content18").css('display','none');
                    $("#content19").css('display','none');
                    $("#content20").css('display','none');
                    $('#datashowinfo10').text("Bạn có chắc muốn ngưng hoạt động?");
   
                }
            });
            $("#cancel10").click(function(){
               location.reload();
   
            });
               $("#ok_qh10").click(function(){
                   
                $("#content1").css('display','none');
                $("#content2").css('display','none');
                $("#content3").css('display','none');
                $("#content4").css('display','none');
                $("#content5").css('display','none');
                $("#content6").css('display','none');
                $("#content7").css('display','none');
                $("#content8").css('display','none');
                $("#content9").css('display','none');
                $("#content10").css('display','none');
                $("#content11").css('display','none');
                $("#content12").css('display','none');
                $("#content13").css('display','none');
                $("#content14").css('display','none');
                $("#content15").css('display','none');
                $("#content16").css('display','none');
                $("#content17").css('display','none');
                $("#content18").css('display','none');
                $("#content19").css('display','none');
                $("#content20").css('display','none');
              
            $.ajax({
                        url: "https://test.heyo.group/admin/website/onoffstatus",
                        type: "get",
                        dataType: "text",
                        data: {
                            id_promo: $('#id_promo10').val()
                        },
                        success: function (result) {
                            if (result != 'no') {
                                alert('Đã cập nhật thành công');
                                location.reload()
                            } else {
   
                                alert('Chưa tới ngày bắt đầu');
                                location.reload()
   
                            }
                        }
                    })
         })

         $('#check_qh11').change(function(){
            if($('#check_qh11:checked').is(":checked")){
                    $('.ug').hide();
         $(".popover_status1").css('display','none');
         $(".popover_status11").css('display','block');
         $(".popover_status2").css('display','none');
         $(".popover_status3").css('display','none');
         $(".popover_status4").css('display','none');
         $(".popover_status5").css('display','none');
         $(".popover_status6").css('display','none');
         $(".popover_status7").css('display','none');
         $(".popover_status8").css('display','none');
         $(".popover_status9").css('display','none');
         $(".popover_status10").css('display','none');
         $(".popover_status12").css('display','none');
         $(".popover_status13").css('display','none');
         $(".popover_status14").css('display','none');
         $(".popover_status15").css('display','none');
         $(".popover_status16").css('display','none');
         $(".popover_status17").css('display','none');
         $(".popover_status18").css('display','none');
         $(".popover_status19").css('display','none');
         $(".popover_status20").css('display','none');
   
         $("#content1").css('display','none');
                    $("#content11").css('display','block');
                    $("#content2").css('display','none');
                    $("#content3").css('display','none');
                    $("#content4").css('display','none');
                    $("#content5").css('display','none');
                    $("#content6").css('display','none');
                    $("#content7").css('display','none');
                    $("#content8").css('display','none');
                    $("#content9").css('display','none');
                    $("#content10").css('display','none');
                    $("#content12").css('display','none');
                    $("#content13").css('display','none');
                    $("#content14").css('display','none');
                    $("#content15").css('display','none');
                    $("#content16").css('display','none');
                    $("#content17").css('display','none');
                    $("#content18").css('display','none');
                    $("#content19").css('display','none');
                    $("#content20").css('display','none');
         $("#content11").css('display','block');
                    $('.phd').show();       
                                 $('#datashowinfo11').text("Bạn có chắc muốn hoạt động lại?");
                
                }else{
         $(".popover_status1").css('display','none');
         $(".popover_status11").css('display','block');
         $(".popover_status2").css('display','none');
         $(".popover_status3").css('display','none');
         $(".popover_status4").css('display','none');
         $(".popover_status5").css('display','none');
         $(".popover_status6").css('display','none');
         $(".popover_status7").css('display','none');
         $(".popover_status8").css('display','none');
         $(".popover_status9").css('display','none');
         $(".popover_status10").css('display','none');
         $(".popover_status12").css('display','none');
         $(".popover_status13").css('display','none');
         $(".popover_status14").css('display','none');
         $(".popover_status15").css('display','none');
         $(".popover_status16").css('display','none');
         $(".popover_status17").css('display','none');
         $(".popover_status18").css('display','none');
         $(".popover_status19").css('display','none');
         $(".popover_status20").css('display','none');
   
                    $('.ug').show();
                    $('.phd').hide();
                    $("#content1").css('display','none');
                    $("#content11").css('display','block');
                    $("#content2").css('display','none');
                    $("#content3").css('display','none');
                    $("#content4").css('display','none');
                    $("#content5").css('display','none');
                    $("#content6").css('display','none');
                    $("#content7").css('display','none');
                    $("#content8").css('display','none');
                    $("#content9").css('display','none');
                    $("#content10").css('display','none');
                    $("#content12").css('display','none');
                    $("#content13").css('display','none');
                    $("#content14").css('display','none');
                    $("#content15").css('display','none');
                    $("#content16").css('display','none');
                    $("#content17").css('display','none');
                    $("#content18").css('display','none');
                    $("#content19").css('display','none');
                    $("#content20").css('display','none');
                    $('#datashowinfo11').text("Bạn có chắc muốn ngưng hoạt động?");
   
                }
            });
            $("#cancel11").click(function(){
               location.reload();
   
            });
               $("#ok_qh11").click(function(){
                   
                $("#content1").css('display','none');
                $("#content2").css('display','none');
                $("#content3").css('display','none');
                $("#content4").css('display','none');
                $("#content5").css('display','none');
                $("#content6").css('display','none');
                $("#content7").css('display','none');
                $("#content8").css('display','none');
                $("#content9").css('display','none');
                $("#content10").css('display','none');
                $("#content11").css('display','none');
                $("#content12").css('display','none');
                $("#content13").css('display','none');
                $("#content14").css('display','none');
                $("#content15").css('display','none');
                $("#content16").css('display','none');
                $("#content17").css('display','none');
                $("#content18").css('display','none');
                $("#content19").css('display','none');
                $("#content20").css('display','none');
              
            $.ajax({
                        url: "https://test.heyo.group/admin/website/onoffstatus",
                        type: "get",
                        dataType: "text",
                        data: {
                            id_promo: $('#id_promo11').val()
                        },
                        success: function (result) {
                            if (result != 'no') {
                                alert('Đã cập nhật thành công');
                                location.reload()
                            } else {
   
                                alert('Chưa tới ngày bắt đầu');
                                location.reload()
   
                            }
                        }
                    })
         })
         $('#check_qh12').change(function(){
            if($('#check_qh12:checked').is(":checked")){
                    $('.ug').hide();
         $(".popover_status1").css('display','none');
         $(".popover_status12").css('display','block');
         $(".popover_status2").css('display','none');
         $(".popover_status3").css('display','none');
         $(".popover_status4").css('display','none');
         $(".popover_status5").css('display','none');
         $(".popover_status6").css('display','none');
         $(".popover_status7").css('display','none');
         $(".popover_status8").css('display','none');
         $(".popover_status9").css('display','none');
         $(".popover_status10").css('display','none');
         $(".popover_status11").css('display','none');
         $(".popover_status13").css('display','none');
         $(".popover_status14").css('display','none');
         $(".popover_status15").css('display','none');
         $(".popover_status16").css('display','none');
         $(".popover_status17").css('display','none');
         $(".popover_status18").css('display','none');
         $(".popover_status19").css('display','none');
         $(".popover_status20").css('display','none');
   
         $("#content1").css('display','none');
                    $("#content12").css('display','block');
                    $("#content2").css('display','none');
                    $("#content3").css('display','none');
                    $("#content4").css('display','none');
                    $("#content5").css('display','none');
                    $("#content6").css('display','none');
                    $("#content7").css('display','none');
                    $("#content8").css('display','none');
                    $("#content9").css('display','none');
                    $("#content10").css('display','none');
                    $("#content11").css('display','none');
                    $("#content13").css('display','none');
                    $("#content14").css('display','none');
                    $("#content15").css('display','none');
                    $("#content16").css('display','none');
                    $("#content17").css('display','none');
                    $("#content18").css('display','none');
                    $("#content19").css('display','none');
                    $("#content20").css('display','none');
         $("#content12").css('display','block');
                    $('.phd').show();       
                                 $('#datashowinfo12').text("Bạn có chắc muốn hoạt động lại?");
                
                }else{
         $(".popover_status1").css('display','none');
         $(".popover_status12").css('display','block');
         $(".popover_status2").css('display','none');
         $(".popover_status3").css('display','none');
         $(".popover_status4").css('display','none');
         $(".popover_status5").css('display','none');
         $(".popover_status6").css('display','none');
         $(".popover_status7").css('display','none');
         $(".popover_status8").css('display','none');
         $(".popover_status9").css('display','none');
         $(".popover_status10").css('display','none');
         $(".popover_status11").css('display','none');
         $(".popover_status13").css('display','none');
         $(".popover_status14").css('display','none');
         $(".popover_status15").css('display','none');
         $(".popover_status16").css('display','none');
         $(".popover_status17").css('display','none');
         $(".popover_status18").css('display','none');
         $(".popover_status19").css('display','none');
         $(".popover_status20").css('display','none');
   
                    $('.ug').show();
                    $('.phd').hide();
                    $("#content1").css('display','none');
                    $("#content12").css('display','block');
                    $("#content2").css('display','none');
                    $("#content3").css('display','none');
                    $("#content4").css('display','none');
                    $("#content5").css('display','none');
                    $("#content6").css('display','none');
                    $("#content7").css('display','none');
                    $("#content8").css('display','none');
                    $("#content9").css('display','none');
                    $("#content10").css('display','none');
                    $("#content11").css('display','none');
                    $("#content13").css('display','none');
                    $("#content14").css('display','none');
                    $("#content15").css('display','none');
                    $("#content16").css('display','none');
                    $("#content17").css('display','none');
                    $("#content18").css('display','none');
                    $("#content19").css('display','none');
                    $("#content20").css('display','none');
                    $('#datashowinfo12').text("Bạn có chắc muốn ngưng hoạt động?");
   
                }
            });
            $("#cancel12").click(function(){
               location.reload();
   
            });
               $("#ok_qh12").click(function(){
                   
                $("#content1").css('display','none');
                $("#content2").css('display','none');
                $("#content3").css('display','none');
                $("#content4").css('display','none');
                $("#content5").css('display','none');
                $("#content6").css('display','none');
                $("#content7").css('display','none');
                $("#content8").css('display','none');
                $("#content9").css('display','none');
                $("#content10").css('display','none');
                $("#content11").css('display','none');
                $("#content12").css('display','none');
                $("#content13").css('display','none');
                $("#content14").css('display','none');
                $("#content15").css('display','none');
                $("#content16").css('display','none');
                $("#content17").css('display','none');
                $("#content18").css('display','none');
                $("#content19").css('display','none');
                $("#content20").css('display','none');
              
            $.ajax({
                        url: "https://test.heyo.group/admin/website/onoffstatus",
                        type: "get",
                        dataType: "text",
                        data: {
                            id_promo: $('#id_promo12').val()
                        },
                        success: function (result) {
                            if (result != 'no') {
                                alert('Đã cập nhật thành công');
                                location.reload()
                            } else {
   
                                alert('Chưa tới ngày bắt đầu');
                                location.reload()
   
                            }
                        }
                    })
         })
         $('#check_qh13').change(function(){
            if($('#check_qh13:checked').is(":checked")){
                    $('.ug').hide();
         $(".popover_status1").css('display','none');
         $(".popover_status13").css('display','block');
         $(".popover_status2").css('display','none');
         $(".popover_status3").css('display','none');
         $(".popover_status4").css('display','none');
         $(".popover_status5").css('display','none');
         $(".popover_status6").css('display','none');
         $(".popover_status7").css('display','none');
         $(".popover_status8").css('display','none');
         $(".popover_status9").css('display','none');
         $(".popover_status10").css('display','none');
         $(".popover_status11").css('display','none');
         $(".popover_status12").css('display','none');
         $(".popover_status14").css('display','none');
         $(".popover_status15").css('display','none');
         $(".popover_status16").css('display','none');
         $(".popover_status17").css('display','none');
         $(".popover_status18").css('display','none');
         $(".popover_status19").css('display','none');
         $(".popover_status20").css('display','none');
   
         $("#content1").css('display','none');
                    $("#content13").css('display','block');
                    $("#content2").css('display','none');
                    $("#content3").css('display','none');
                    $("#content4").css('display','none');
                    $("#content5").css('display','none');
                    $("#content6").css('display','none');
                    $("#content7").css('display','none');
                    $("#content8").css('display','none');
                    $("#content9").css('display','none');
                    $("#content10").css('display','none');
                    $("#content11").css('display','none');
                    $("#content12").css('display','none');
                    $("#content14").css('display','none');
                    $("#content15").css('display','none');
                    $("#content16").css('display','none');
                    $("#content17").css('display','none');
                    $("#content18").css('display','none');
                    $("#content19").css('display','none');
                    $("#content20").css('display','none');
         $("#content13").css('display','block');
                    $('.phd').show();       
                                 $('#datashowinfo13').text("Bạn có chắc muốn hoạt động lại?");
                
                }else{
         $(".popover_status1").css('display','none');
         $(".popover_status13").css('display','block');
         $(".popover_status2").css('display','none');
         $(".popover_status3").css('display','none');
         $(".popover_status4").css('display','none');
         $(".popover_status5").css('display','none');
         $(".popover_status6").css('display','none');
         $(".popover_status7").css('display','none');
         $(".popover_status8").css('display','none');
         $(".popover_status9").css('display','none');
         $(".popover_status10").css('display','none');
         $(".popover_status11").css('display','none');
         $(".popover_status12").css('display','none');
         $(".popover_status14").css('display','none');
         $(".popover_status15").css('display','none');
         $(".popover_status16").css('display','none');
         $(".popover_status17").css('display','none');
         $(".popover_status18").css('display','none');
         $(".popover_status19").css('display','none');
         $(".popover_status20").css('display','none');
   
                    $('.ug').show();
                    $('.phd').hide();
                    $("#content1").css('display','none');
                    $("#content13").css('display','block');
                    $("#content2").css('display','none');
                    $("#content3").css('display','none');
                    $("#content4").css('display','none');
                    $("#content5").css('display','none');
                    $("#content6").css('display','none');
                    $("#content7").css('display','none');
                    $("#content8").css('display','none');
                    $("#content9").css('display','none');
                    $("#content10").css('display','none');
                    $("#content11").css('display','none');
                    $("#content12").css('display','none');
                    $("#content14").css('display','none');
                    $("#content15").css('display','none');
                    $("#content16").css('display','none');
                    $("#content17").css('display','none');
                    $("#content18").css('display','none');
                    $("#content19").css('display','none');
                    $("#content20").css('display','none');
                    $('#datashowinfo13').text("Bạn có chắc muốn ngưng hoạt động?");
   
                }
            });
            $("#cancel13").click(function(){
               location.reload();
   
            });
               $("#ok_qh13").click(function(){
                   
                $("#content1").css('display','none');
                $("#content2").css('display','none');
                $("#content3").css('display','none');
                $("#content4").css('display','none');
                $("#content5").css('display','none');
                $("#content6").css('display','none');
                $("#content7").css('display','none');
                $("#content8").css('display','none');
                $("#content9").css('display','none');
                $("#content10").css('display','none');
                $("#content11").css('display','none');
                $("#content12").css('display','none');
                $("#content13").css('display','none');
                $("#content14").css('display','none');
                $("#content15").css('display','none');
                $("#content16").css('display','none');
                $("#content17").css('display','none');
                $("#content18").css('display','none');
                $("#content19").css('display','none');
                $("#content20").css('display','none');
              
            $.ajax({
                        url: "https://test.heyo.group/admin/website/onoffstatus",
                        type: "get",
                        dataType: "text",
                        data: {
                            id_promo: $('#id_promo13').val()
                        },
                        success: function (result) {
                            if (result != 'no') {
                                alert('Đã cập nhật thành công');
                                location.reload()
                            } else {
   
                                alert('Chưa tới ngày bắt đầu');
                                location.reload()
   
                            }
                        }
                    })
         })
         
         $('#check_qh14').change(function(){
            if($('#check_qh14:checked').is(":checked")){
                    $('.ug').hide();
         $(".popover_status1").css('display','none');
         $(".popover_status14").css('display','block');
         $(".popover_status2").css('display','none');
         $(".popover_status3").css('display','none');
         $(".popover_status4").css('display','none');
         $(".popover_status5").css('display','none');
         $(".popover_status6").css('display','none');
         $(".popover_status7").css('display','none');
         $(".popover_status8").css('display','none');
         $(".popover_status9").css('display','none');
         $(".popover_status10").css('display','none');
         $(".popover_status11").css('display','none');
         $(".popover_status12").css('display','none');
         $(".popover_status13").css('display','none');
         $(".popover_status15").css('display','none');
         $(".popover_status16").css('display','none');
         $(".popover_status17").css('display','none');
         $(".popover_status18").css('display','none');
         $(".popover_status19").css('display','none');
         $(".popover_status20").css('display','none');
   
         $("#content1").css('display','none');
                    $("#content14").css('display','block');
                    $("#content2").css('display','none');
                    $("#content3").css('display','none');
                    $("#content4").css('display','none');
                    $("#content5").css('display','none');
                    $("#content6").css('display','none');
                    $("#content7").css('display','none');
                    $("#content8").css('display','none');
                    $("#content9").css('display','none');
                    $("#content10").css('display','none');
                    $("#content11").css('display','none');
                    $("#content12").css('display','none');
                    $("#content13").css('display','none');
                    $("#content15").css('display','none');
                    $("#content16").css('display','none');
                    $("#content17").css('display','none');
                    $("#content18").css('display','none');
                    $("#content19").css('display','none');
                    $("#content20").css('display','none');
         $("#content14").css('display','block');
                    $('.phd').show();       
                                 $('#datashowinfo14').text("Bạn có chắc muốn hoạt động lại?");
                
                }else{
         $(".popover_status1").css('display','none');
         $(".popover_status14").css('display','block');
         $(".popover_status2").css('display','none');
         $(".popover_status3").css('display','none');
         $(".popover_status4").css('display','none');
         $(".popover_status5").css('display','none');
         $(".popover_status6").css('display','none');
         $(".popover_status7").css('display','none');
         $(".popover_status8").css('display','none');
         $(".popover_status9").css('display','none');
         $(".popover_status10").css('display','none');
         $(".popover_status11").css('display','none');
         $(".popover_status12").css('display','none');
         $(".popover_status13").css('display','none');
         $(".popover_status15").css('display','none');
         $(".popover_status16").css('display','none');
         $(".popover_status17").css('display','none');
         $(".popover_status18").css('display','none');
         $(".popover_status19").css('display','none');
         $(".popover_status20").css('display','none');
   
                    $('.ug').show();
                    $('.phd').hide();
                    $("#content1").css('display','none');
                    $("#content14").css('display','block');
                    $("#content2").css('display','none');
                    $("#content3").css('display','none');
                    $("#content4").css('display','none');
                    $("#content5").css('display','none');
                    $("#content6").css('display','none');
                    $("#content7").css('display','none');
                    $("#content8").css('display','none');
                    $("#content9").css('display','none');
                    $("#content10").css('display','none');
                    $("#content11").css('display','none');
                    $("#content12").css('display','none');
                    $("#content13").css('display','none');
                    $("#content15").css('display','none');
                    $("#content16").css('display','none');
                    $("#content17").css('display','none');
                    $("#content18").css('display','none');
                    $("#content19").css('display','none');
                    $("#content20").css('display','none');
                    $('#datashowinfo14').text("Bạn có chắc muốn ngưng hoạt động?");
   
                }
            });
            $("#cancel14").click(function(){
               location.reload();
   
            });
               $("#ok_qh14").click(function(){
                   
                $("#content1").css('display','none');
                $("#content2").css('display','none');
                $("#content3").css('display','none');
                $("#content4").css('display','none');
                $("#content5").css('display','none');
                $("#content6").css('display','none');
                $("#content7").css('display','none');
                $("#content8").css('display','none');
                $("#content9").css('display','none');
                $("#content10").css('display','none');
                $("#content11").css('display','none');
                $("#content12").css('display','none');
                $("#content13").css('display','none');
                $("#content14").css('display','none');
                $("#content15").css('display','none');
                $("#content16").css('display','none');
                $("#content17").css('display','none');
                $("#content18").css('display','none');
                $("#content19").css('display','none');
                $("#content20").css('display','none');
              
            $.ajax({
                        url: "https://test.heyo.group/admin/website/onoffstatus",
                        type: "get",
                        dataType: "text",
                        data: {
                            id_promo: $('#id_promo14').val()
                        },
                        success: function (result) {
                            if (result != 'no') {
                                alert('Đã cập nhật thành công');
                                location.reload()
                            } else {
   
                                alert('Chưa tới ngày bắt đầu');
                                location.reload()
   
                            }
                        }
                    })
         })
        
         $('#check_qh15').change(function(){
            if($('#check_qh15:checked').is(":checked")){
                    $('.ug').hide();
         $(".popover_status1").css('display','none');
         $(".popover_status15").css('display','block');
         $(".popover_status2").css('display','none');
         $(".popover_status3").css('display','none');
         $(".popover_status4").css('display','none');
         $(".popover_status5").css('display','none');
         $(".popover_status6").css('display','none');
         $(".popover_status7").css('display','none');
         $(".popover_status8").css('display','none');
         $(".popover_status9").css('display','none');
         $(".popover_status10").css('display','none');
         $(".popover_status11").css('display','none');
         $(".popover_status12").css('display','none');
         $(".popover_status13").css('display','none');
         $(".popover_status14").css('display','none');
         $(".popover_status16").css('display','none');
         $(".popover_status17").css('display','none');
         $(".popover_status18").css('display','none');
         $(".popover_status19").css('display','none');
         $(".popover_status20").css('display','none');
   
         $("#content1").css('display','none');
                    $("#content15").css('display','block');
                    $("#content2").css('display','none');
                    $("#content3").css('display','none');
                    $("#content4").css('display','none');
                    $("#content5").css('display','none');
                    $("#content6").css('display','none');
                    $("#content7").css('display','none');
                    $("#content8").css('display','none');
                    $("#content9").css('display','none');
                    $("#content10").css('display','none');
                    $("#content11").css('display','none');
                    $("#content12").css('display','none');
                    $("#content13").css('display','none');
                    $("#content14").css('display','none');
                    $("#content16").css('display','none');
                    $("#content17").css('display','none');
                    $("#content18").css('display','none');
                    $("#content19").css('display','none');
                    $("#content20").css('display','none');
         $("#content15").css('display','block');
                    $('.phd').show();       
                                 $('#datashowinfo15').text("Bạn có chắc muốn hoạt động lại?");
                
                }else{
         $(".popover_status1").css('display','none');
         $(".popover_status15").css('display','block');
         $(".popover_status2").css('display','none');
         $(".popover_status3").css('display','none');
         $(".popover_status4").css('display','none');
         $(".popover_status5").css('display','none');
         $(".popover_status6").css('display','none');
         $(".popover_status7").css('display','none');
         $(".popover_status8").css('display','none');
         $(".popover_status9").css('display','none');
         $(".popover_status10").css('display','none');
         $(".popover_status11").css('display','none');
         $(".popover_status12").css('display','none');
         $(".popover_status13").css('display','none');
         $(".popover_status14").css('display','none');
         $(".popover_status16").css('display','none');
         $(".popover_status17").css('display','none');
         $(".popover_status18").css('display','none');
         $(".popover_status19").css('display','none');
         $(".popover_status20").css('display','none');
   
                    $('.ug').show();
                    $('.phd').hide();
                    $("#content1").css('display','none');
                    $("#content15").css('display','block');
                    $("#content2").css('display','none');
                    $("#content3").css('display','none');
                    $("#content4").css('display','none');
                    $("#content5").css('display','none');
                    $("#content6").css('display','none');
                    $("#content7").css('display','none');
                    $("#content8").css('display','none');
                    $("#content9").css('display','none');
                    $("#content10").css('display','none');
                    $("#content11").css('display','none');
                    $("#content12").css('display','none');
                    $("#content13").css('display','none');
                    $("#content14").css('display','none');
                    $("#content16").css('display','none');
                    $("#content17").css('display','none');
                    $("#content18").css('display','none');
                    $("#content19").css('display','none');
                    $("#content20").css('display','none');
                    $('#datashowinfo15').text("Bạn có chắc muốn ngưng hoạt động?");
   
                }
            });
            $("#cancel15").click(function(){
               location.reload();
   
            });
               $("#ok_qh15").click(function(){
                   
                $("#content1").css('display','none');
                $("#content2").css('display','none');
                $("#content3").css('display','none');
                $("#content4").css('display','none');
                $("#content5").css('display','none');
                $("#content6").css('display','none');
                $("#content7").css('display','none');
                $("#content8").css('display','none');
                $("#content9").css('display','none');
                $("#content10").css('display','none');
                $("#content11").css('display','none');
                $("#content12").css('display','none');
                $("#content13").css('display','none');
                $("#content14").css('display','none');
                $("#content15").css('display','none');
                $("#content16").css('display','none');
                $("#content17").css('display','none');
                $("#content18").css('display','none');
                $("#content19").css('display','none');
                $("#content20").css('display','none');
              
            $.ajax({
                        url: "https://test.heyo.group/admin/website/onoffstatus",
                        type: "get",
                        dataType: "text",
                        data: {
                            id_promo: $('#id_promo15').val()
                        },
                        success: function (result) {
                            if (result != 'no') {
                                alert('Đã cập nhật thành công');
                                location.reload()
                            } else {
   
                                alert('Chưa tới ngày bắt đầu');
                                location.reload()
   
                            }
                        }
                    })
         })
        
         $('#check_qh16').change(function(){
            if($('#check_qh16:checked').is(":checked")){
                    $('.ug').hide();
         $(".popover_status1").css('display','none');
         $(".popover_status16").css('display','block');
         $(".popover_status2").css('display','none');
         $(".popover_status3").css('display','none');
         $(".popover_status4").css('display','none');
         $(".popover_status5").css('display','none');
         $(".popover_status6").css('display','none');
         $(".popover_status7").css('display','none');
         $(".popover_status8").css('display','none');
         $(".popover_status9").css('display','none');
         $(".popover_status10").css('display','none');
         $(".popover_status11").css('display','none');
         $(".popover_status12").css('display','none');
         $(".popover_status13").css('display','none');
         $(".popover_status14").css('display','none');
         $(".popover_status15").css('display','none');
         $(".popover_status17").css('display','none');
         $(".popover_status18").css('display','none');
         $(".popover_status19").css('display','none');
         $(".popover_status20").css('display','none');
   
         $("#content1").css('display','none');
                    $("#content16").css('display','block');
                    $("#content2").css('display','none');
                    $("#content3").css('display','none');
                    $("#content4").css('display','none');
                    $("#content5").css('display','none');
                    $("#content6").css('display','none');
                    $("#content7").css('display','none');
                    $("#content8").css('display','none');
                    $("#content9").css('display','none');
                    $("#content10").css('display','none');
                    $("#content11").css('display','none');
                    $("#content12").css('display','none');
                    $("#content13").css('display','none');
                    $("#content14").css('display','none');
                    $("#content15").css('display','none');
                    $("#content17").css('display','none');
                    $("#content18").css('display','none');
                    $("#content19").css('display','none');
                    $("#content20").css('display','none');
         $("#content16").css('display','block');
                    $('.phd').show();       
                                 $('#datashowinfo16').text("Bạn có chắc muốn hoạt động lại?");
                
                }else{
         $(".popover_status1").css('display','none');
         $(".popover_status16").css('display','block');
         $(".popover_status2").css('display','none');
         $(".popover_status3").css('display','none');
         $(".popover_status4").css('display','none');
         $(".popover_status5").css('display','none');
         $(".popover_status6").css('display','none');
         $(".popover_status7").css('display','none');
         $(".popover_status8").css('display','none');
         $(".popover_status9").css('display','none');
         $(".popover_status10").css('display','none');
         $(".popover_status11").css('display','none');
         $(".popover_status12").css('display','none');
         $(".popover_status13").css('display','none');
         $(".popover_status14").css('display','none');
         $(".popover_status15").css('display','none');
         $(".popover_status17").css('display','none');
         $(".popover_status18").css('display','none');
         $(".popover_status19").css('display','none');
         $(".popover_status20").css('display','none');
   
                    $('.ug').show();
                    $('.phd').hide();
                    $("#content1").css('display','none');
                    $("#content16").css('display','block');
                    $("#content2").css('display','none');
                    $("#content3").css('display','none');
                    $("#content4").css('display','none');
                    $("#content5").css('display','none');
                    $("#content6").css('display','none');
                    $("#content7").css('display','none');
                    $("#content8").css('display','none');
                    $("#content9").css('display','none');
                    $("#content10").css('display','none');
                    $("#content11").css('display','none');
                    $("#content12").css('display','none');
                    $("#content13").css('display','none');
                    $("#content14").css('display','none');
                    $("#content15").css('display','none');
                    $("#content17").css('display','none');
                    $("#content18").css('display','none');
                    $("#content19").css('display','none');
                    $("#content20").css('display','none');
                    $('#datashowinfo16').text("Bạn có chắc muốn ngưng hoạt động?");
   
                }
            });
            $("#cancel16").click(function(){
               location.reload();
   
            });
               $("#ok_qh16").click(function(){
                   
                $("#content1").css('display','none');
                $("#content2").css('display','none');
                $("#content3").css('display','none');
                $("#content4").css('display','none');
                $("#content5").css('display','none');
                $("#content6").css('display','none');
                $("#content7").css('display','none');
                $("#content8").css('display','none');
                $("#content9").css('display','none');
                $("#content10").css('display','none');
                $("#content11").css('display','none');
                $("#content12").css('display','none');
                $("#content13").css('display','none');
                $("#content14").css('display','none');
                $("#content15").css('display','none');
                $("#content16").css('display','none');
                $("#content17").css('display','none');
                $("#content18").css('display','none');
                $("#content19").css('display','none');
                $("#content20").css('display','none');
              
            $.ajax({
                        url: "https://test.heyo.group/admin/website/onoffstatus",
                        type: "get",
                        dataType: "text",
                        data: {
                            id_promo: $('#id_promo16').val()
                        },
                        success: function (result) {
                            if (result != 'no') {
                                alert('Đã cập nhật thành công');
                                location.reload()
                            } else {
   
                                alert('Chưa tới ngày bắt đầu');
                                location.reload()
   
                            }
                        }
                    })
         })
        
         $('#check_qh17').change(function(){
            if($('#check_qh17:checked').is(":checked")){
                    $('.ug').hide();
         $(".popover_status1").css('display','none');
         $(".popover_status17").css('display','block');
         $(".popover_status2").css('display','none');
         $(".popover_status3").css('display','none');
         $(".popover_status4").css('display','none');
         $(".popover_status5").css('display','none');
         $(".popover_status6").css('display','none');
         $(".popover_status7").css('display','none');
         $(".popover_status8").css('display','none');
         $(".popover_status9").css('display','none');
         $(".popover_status10").css('display','none');
         $(".popover_status11").css('display','none');
         $(".popover_status12").css('display','none');
         $(".popover_status13").css('display','none');
         $(".popover_status14").css('display','none');
         $(".popover_status15").css('display','none');
         $(".popover_status16").css('display','none');
         $(".popover_status18").css('display','none');
         $(".popover_status19").css('display','none');
         $(".popover_status20").css('display','none');
   
         $("#content1").css('display','none');
                    $("#content17").css('display','block');
                    $("#content2").css('display','none');
                    $("#content3").css('display','none');
                    $("#content4").css('display','none');
                    $("#content5").css('display','none');
                    $("#content6").css('display','none');
                    $("#content7").css('display','none');
                    $("#content8").css('display','none');
                    $("#content9").css('display','none');
                    $("#content10").css('display','none');
                    $("#content11").css('display','none');
                    $("#content12").css('display','none');
                    $("#content13").css('display','none');
                    $("#content14").css('display','none');
                    $("#content15").css('display','none');
                    $("#content16").css('display','none');
                    $("#content18").css('display','none');
                    $("#content19").css('display','none');
                    $("#content20").css('display','none');
         $("#content17").css('display','block');
                    $('.phd').show();       
                                 $('#datashowinfo17').text("Bạn có chắc muốn hoạt động lại?");
                
                }else{
         $(".popover_status1").css('display','none');
         $(".popover_status17").css('display','block');
         $(".popover_status2").css('display','none');
         $(".popover_status3").css('display','none');
         $(".popover_status4").css('display','none');
         $(".popover_status5").css('display','none');
         $(".popover_status6").css('display','none');
         $(".popover_status7").css('display','none');
         $(".popover_status8").css('display','none');
         $(".popover_status9").css('display','none');
         $(".popover_status10").css('display','none');
         $(".popover_status11").css('display','none');
         $(".popover_status12").css('display','none');
         $(".popover_status13").css('display','none');
         $(".popover_status14").css('display','none');
         $(".popover_status15").css('display','none');
         $(".popover_status16").css('display','none');
         $(".popover_status18").css('display','none');
         $(".popover_status19").css('display','none');
         $(".popover_status20").css('display','none');
   
                    $('.ug').show();
                    $('.phd').hide();
                    $("#content1").css('display','none');
                    $("#content17").css('display','block');
                    $("#content2").css('display','none');
                    $("#content3").css('display','none');
                    $("#content4").css('display','none');
                    $("#content5").css('display','none');
                    $("#content6").css('display','none');
                    $("#content7").css('display','none');
                    $("#content8").css('display','none');
                    $("#content9").css('display','none');
                    $("#content10").css('display','none');
                    $("#content11").css('display','none');
                    $("#content12").css('display','none');
                    $("#content13").css('display','none');
                    $("#content14").css('display','none');
                    $("#content15").css('display','none');
                    $("#content16").css('display','none');
                    $("#content18").css('display','none');
                    $("#content19").css('display','none');
                    $("#content20").css('display','none');
                    $('#datashowinfo17').text("Bạn có chắc muốn ngưng hoạt động?");
   
                }
            });
            $("#cancel17").click(function(){
               location.reload();
   
            });
               $("#ok_qh17").click(function(){
                   
                $("#content1").css('display','none');
                $("#content2").css('display','none');
                $("#content3").css('display','none');
                $("#content4").css('display','none');
                $("#content5").css('display','none');
                $("#content6").css('display','none');
                $("#content7").css('display','none');
                $("#content8").css('display','none');
                $("#content9").css('display','none');
                $("#content10").css('display','none');
                $("#content11").css('display','none');
                $("#content12").css('display','none');
                $("#content13").css('display','none');
                $("#content14").css('display','none');
                $("#content15").css('display','none');
                $("#content16").css('display','none');
                $("#content17").css('display','none');
                $("#content18").css('display','none');
                $("#content19").css('display','none');
                $("#content20").css('display','none');
              
            $.ajax({
                        url: "https://test.heyo.group/admin/website/onoffstatus",
                        type: "get",
                        dataType: "text",
                        data: {
                            id_promo: $('#id_promo17').val()
                        },
                        success: function (result) {
                            if (result != 'no') {
                                alert('Đã cập nhật thành công');
                                location.reload()
                            } else {
   
                                alert('Chưa tới ngày bắt đầu');
                                location.reload()
   
                            }
                        }
                    })
         })
        
         $('#check_qh18').change(function(){
            if($('#check_qh18:checked').is(":checked")){
                    $('.ug').hide();
         $(".popover_status1").css('display','none');
         $(".popover_status18").css('display','block');
         $(".popover_status2").css('display','none');
         $(".popover_status3").css('display','none');
         $(".popover_status4").css('display','none');
         $(".popover_status5").css('display','none');
         $(".popover_status6").css('display','none');
         $(".popover_status7").css('display','none');
         $(".popover_status8").css('display','none');
         $(".popover_status9").css('display','none');
         $(".popover_status10").css('display','none');
         $(".popover_status11").css('display','none');
         $(".popover_status12").css('display','none');
         $(".popover_status13").css('display','none');
         $(".popover_status14").css('display','none');
         $(".popover_status15").css('display','none');
         $(".popover_status16").css('display','none');
         $(".popover_status17").css('display','none');
         $(".popover_status19").css('display','none');
         $(".popover_status20").css('display','none');
   
         $("#content1").css('display','none');
                    $("#content18").css('display','block');
                    $("#content2").css('display','none');
                    $("#content3").css('display','none');
                    $("#content4").css('display','none');
                    $("#content5").css('display','none');
                    $("#content6").css('display','none');
                    $("#content7").css('display','none');
                    $("#content8").css('display','none');
                    $("#content9").css('display','none');
                    $("#content10").css('display','none');
                    $("#content11").css('display','none');
                    $("#content12").css('display','none');
                    $("#content13").css('display','none');
                    $("#content14").css('display','none');
                    $("#content15").css('display','none');
                    $("#content16").css('display','none');
                    $("#content17").css('display','none');
                    $("#content19").css('display','none');
                    $("#content20").css('display','none');
         $("#content18").css('display','block');
                    $('.phd').show();       
                                 $('#datashowinfo18').text("Bạn có chắc muốn hoạt động lại?");
                
                }else{
         $(".popover_status1").css('display','none');
         $(".popover_status18").css('display','block');
         $(".popover_status2").css('display','none');
         $(".popover_status3").css('display','none');
         $(".popover_status4").css('display','none');
         $(".popover_status5").css('display','none');
         $(".popover_status6").css('display','none');
         $(".popover_status7").css('display','none');
         $(".popover_status8").css('display','none');
         $(".popover_status9").css('display','none');
         $(".popover_status10").css('display','none');
         $(".popover_status11").css('display','none');
         $(".popover_status12").css('display','none');
         $(".popover_status13").css('display','none');
         $(".popover_status14").css('display','none');
         $(".popover_status15").css('display','none');
         $(".popover_status16").css('display','none');
         $(".popover_status17").css('display','none');
         $(".popover_status19").css('display','none');
         $(".popover_status20").css('display','none');
   
                    $('.ug').show();
                    $('.phd').hide();
                    $("#content1").css('display','none');
                    $("#content18").css('display','block');
                    $("#content2").css('display','none');
                    $("#content3").css('display','none');
                    $("#content4").css('display','none');
                    $("#content5").css('display','none');
                    $("#content6").css('display','none');
                    $("#content7").css('display','none');
                    $("#content8").css('display','none');
                    $("#content9").css('display','none');
                    $("#content10").css('display','none');
                    $("#content11").css('display','none');
                    $("#content12").css('display','none');
                    $("#content13").css('display','none');
                    $("#content14").css('display','none');
                    $("#content15").css('display','none');
                    $("#content16").css('display','none');
                    $("#content17").css('display','none');
                    $("#content19").css('display','none');
                    $("#content20").css('display','none');
                    $('#datashowinfo18').text("Bạn có chắc muốn ngưng hoạt động?");
   
                }
            });
            $("#cancel18").click(function(){
               location.reload();
   
            });
               $("#ok_qh18").click(function(){
                   
                $("#content1").css('display','none');
                $("#content2").css('display','none');
                $("#content3").css('display','none');
                $("#content4").css('display','none');
                $("#content5").css('display','none');
                $("#content6").css('display','none');
                $("#content7").css('display','none');
                $("#content8").css('display','none');
                $("#content9").css('display','none');
                $("#content10").css('display','none');
                $("#content11").css('display','none');
                $("#content12").css('display','none');
                $("#content13").css('display','none');
                $("#content14").css('display','none');
                $("#content15").css('display','none');
                $("#content16").css('display','none');
                $("#content17").css('display','none');
                $("#content18").css('display','none');
                $("#content19").css('display','none');
                $("#content20").css('display','none');
              
            $.ajax({
                        url: "https://test.heyo.group/admin/website/onoffstatus",
                        type: "get",
                        dataType: "text",
                        data: {
                            id_promo: $('#id_promo18').val()
                        },
                        success: function (result) {
                            if (result != 'no') {
                                alert('Đã cập nhật thành công');
                                location.reload()
                            } else {
   
                                alert('Chưa tới ngày bắt đầu');
                                location.reload()
   
                            }
                        }
                    })
         })
        
         $('#check_qh19').change(function(){
            if($('#check_qh19:checked').is(":checked")){
                    $('.ug').hide();
         $(".popover_status1").css('display','none');
         $(".popover_status19").css('display','block');
         $(".popover_status2").css('display','none');
         $(".popover_status3").css('display','none');
         $(".popover_status4").css('display','none');
         $(".popover_status5").css('display','none');
         $(".popover_status6").css('display','none');
         $(".popover_status7").css('display','none');
         $(".popover_status8").css('display','none');
         $(".popover_status9").css('display','none');
         $(".popover_status10").css('display','none');
         $(".popover_status11").css('display','none');
         $(".popover_status12").css('display','none');
         $(".popover_status13").css('display','none');
         $(".popover_status14").css('display','none');
         $(".popover_status15").css('display','none');
         $(".popover_status16").css('display','none');
         $(".popover_status17").css('display','none');
         $(".popover_status18").css('display','none');
         $(".popover_status20").css('display','none');
   
         $("#content1").css('display','none');
                    $("#content19").css('display','block');
                    $("#content2").css('display','none');
                    $("#content3").css('display','none');
                    $("#content4").css('display','none');
                    $("#content5").css('display','none');
                    $("#content6").css('display','none');
                    $("#content7").css('display','none');
                    $("#content8").css('display','none');
                    $("#content9").css('display','none');
                    $("#content10").css('display','none');
                    $("#content11").css('display','none');
                    $("#content12").css('display','none');
                    $("#content13").css('display','none');
                    $("#content14").css('display','none');
                    $("#content15").css('display','none');
                    $("#content16").css('display','none');
                    $("#content17").css('display','none');
                    $("#content18").css('display','none');
                    $("#content20").css('display','none');
         $("#content19").css('display','block');
                    $('.phd').show();       
                                 $('#datashowinfo19').text("Bạn có chắc muốn hoạt động lại?");
                
                }else{
         $(".popover_status1").css('display','none');
         $(".popover_status19").css('display','block');
         $(".popover_status2").css('display','none');
         $(".popover_status3").css('display','none');
         $(".popover_status4").css('display','none');
         $(".popover_status5").css('display','none');
         $(".popover_status6").css('display','none');
         $(".popover_status7").css('display','none');
         $(".popover_status8").css('display','none');
         $(".popover_status9").css('display','none');
         $(".popover_status10").css('display','none');
         $(".popover_status11").css('display','none');
         $(".popover_status12").css('display','none');
         $(".popover_status13").css('display','none');
         $(".popover_status14").css('display','none');
         $(".popover_status15").css('display','none');
         $(".popover_status16").css('display','none');
         $(".popover_status17").css('display','none');
         $(".popover_status18").css('display','none');
         $(".popover_status20").css('display','none');
   
                    $('.ug').show();
                    $('.phd').hide();
                    $("#content1").css('display','none');
                    $("#content19").css('display','block');
                    $("#content2").css('display','none');
                    $("#content3").css('display','none');
                    $("#content4").css('display','none');
                    $("#content5").css('display','none');
                    $("#content6").css('display','none');
                    $("#content7").css('display','none');
                    $("#content8").css('display','none');
                    $("#content9").css('display','none');
                    $("#content10").css('display','none');
                    $("#content11").css('display','none');
                    $("#content12").css('display','none');
                    $("#content13").css('display','none');
                    $("#content14").css('display','none');
                    $("#content15").css('display','none');
                    $("#content16").css('display','none');
                    $("#content17").css('display','none');
                    $("#content18").css('display','none');
                    $("#content20").css('display','none');
                    $('#datashowinfo19').text("Bạn có chắc muốn ngưng hoạt động?");
   
                }
            });
            $("#cancel19").click(function(){
               location.reload();
   
            });
               $("#ok_qh19").click(function(){
                   
                $("#content1").css('display','none');
                $("#content2").css('display','none');
                $("#content3").css('display','none');
                $("#content4").css('display','none');
                $("#content5").css('display','none');
                $("#content6").css('display','none');
                $("#content7").css('display','none');
                $("#content8").css('display','none');
                $("#content9").css('display','none');
                $("#content10").css('display','none');
                $("#content11").css('display','none');
                $("#content12").css('display','none');
                $("#content13").css('display','none');
                $("#content14").css('display','none');
                $("#content15").css('display','none');
                $("#content16").css('display','none');
                $("#content17").css('display','none');
                $("#content18").css('display','none');
                $("#content19").css('display','none');
                $("#content20").css('display','none');
              
            $.ajax({
                        url: "https://test.heyo.group/admin/website/onoffstatus",
                        type: "get",
                        dataType: "text",
                        data: {
                            id_promo: $('#id_promo19').val()
                        },
                        success: function (result) {
                            if (result != 'no') {
                                alert('Đã cập nhật thành công');
                                location.reload()
                            } else {
   
                                alert('Chưa tới ngày bắt đầu');
                                location.reload()
   
                            }
                        }
                    })
         })
        
         $('#check_qh20').change(function(){
            if($('#check_qh20:checked').is(":checked")){
                    $('.ug').hide();
         $(".popover_status1").css('display','none');
         $(".popover_status20").css('display','block');
         $(".popover_status2").css('display','none');
         $(".popover_status3").css('display','none');
         $(".popover_status4").css('display','none');
         $(".popover_status5").css('display','none');
         $(".popover_status6").css('display','none');
         $(".popover_status7").css('display','none');
         $(".popover_status8").css('display','none');
         $(".popover_status9").css('display','none');
         $(".popover_status10").css('display','none');
         $(".popover_status11").css('display','none');
         $(".popover_status12").css('display','none');
         $(".popover_status13").css('display','none');
         $(".popover_status14").css('display','none');
         $(".popover_status15").css('display','none');
         $(".popover_status16").css('display','none');
         $(".popover_status17").css('display','none');
         $(".popover_status18").css('display','none');
         $(".popover_status19").css('display','none');
   
         $("#content1").css('display','none');
                    $("#content20").css('display','block');
                    $("#content2").css('display','none');
                    $("#content3").css('display','none');
                    $("#content4").css('display','none');
                    $("#content5").css('display','none');
                    $("#content6").css('display','none');
                    $("#content7").css('display','none');
                    $("#content8").css('display','none');
                    $("#content9").css('display','none');
                    $("#content10").css('display','none');
                    $("#content11").css('display','none');
                    $("#content12").css('display','none');
                    $("#content13").css('display','none');
                    $("#content14").css('display','none');
                    $("#content15").css('display','none');
                    $("#content16").css('display','none');
                    $("#content17").css('display','none');
                    $("#content18").css('display','none');
                    $("#content19").css('display','none');
         $("#content20").css('display','block');
                    $('.phd').show();       
                                 $('#datashowinfo20').text("Bạn có chắc muốn hoạt động lại?");
                
                }else{
         $(".popover_status1").css('display','none');
         $(".popover_status20").css('display','block');
         $(".popover_status2").css('display','none');
         $(".popover_status3").css('display','none');
         $(".popover_status4").css('display','none');
         $(".popover_status5").css('display','none');
         $(".popover_status6").css('display','none');
         $(".popover_status7").css('display','none');
         $(".popover_status8").css('display','none');
         $(".popover_status9").css('display','none');
         $(".popover_status10").css('display','none');
         $(".popover_status11").css('display','none');
         $(".popover_status12").css('display','none');
         $(".popover_status13").css('display','none');
         $(".popover_status14").css('display','none');
         $(".popover_status15").css('display','none');
         $(".popover_status16").css('display','none');
         $(".popover_status17").css('display','none');
         $(".popover_status18").css('display','none');
         $(".popover_status19").css('display','none');
   
                    $('.ug').show(); 
                    $('.phd').hide();
                    $("#content1").css('display','none');
                    $("#content20").css('display','block');
                    $("#content2").css('display','none');
                    $("#content3").css('display','none');
                    $("#content4").css('display','none');
                    $("#content5").css('display','none');
                    $("#content6").css('display','none');
                    $("#content7").css('display','none');
                    $("#content8").css('display','none');
                    $("#content9").css('display','none');
                    $("#content10").css('display','none');
                    $("#content11").css('display','none');
                    $("#content12").css('display','none');
                    $("#content13").css('display','none');
                    $("#content14").css('display','none');
                    $("#content15").css('display','none');
                    $("#content16").css('display','none');
                    $("#content17").css('display','none');
                    $("#content18").css('display','none');
                    $("#content19").css('display','none');
                    $('#datashowinfo20').text("Bạn có chắc muốn ngưng hoạt động?");
   
                }
            });
            $("#cancel20").click(function(){
               location.reload();
   
            });
               $("#ok_qh20").click(function(){
                   
                $("#content1").css('display','none');
                $("#content2").css('display','none');
                $("#content3").css('display','none');
                $("#content4").css('display','none');
                $("#content5").css('display','none');
                $("#content6").css('display','none');
                $("#content7").css('display','none');
                $("#content8").css('display','none');
                $("#content9").css('display','none');
                $("#content10").css('display','none');
                $("#content11").css('display','none');
                $("#content12").css('display','none');
                $("#content13").css('display','none');
                $("#content14").css('display','none');
                $("#content15").css('display','none');
                $("#content16").css('display','none');
                $("#content17").css('display','none');
                $("#content18").css('display','none');
                $("#content19").css('display','none');
                $("#content20").css('display','none');
              
            $.ajax({
                        url: "https://test.heyo.group/admin/website/onoffstatus",
                        type: "get",
                        dataType: "text",
                        data: {
                            id_promo: $('#id_promo20').val()
                        },
                        success: function (result) {
                            if (result != 'no') {
                                alert('Đã cập nhật thành công');
                                location.reload()
                            } else {
   
                                alert('Chưa tới ngày bắt đầu');
                                location.reload()
   
                            }
                        }
                    })
                })      
                   })
</script>
<style>
        .switch {
            position: relative;
            display: inline-block;
            width: 50px;
            height: 28px;
    margin-top: 15px
        }
        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 20px;
            width: 20px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked + .slider {
            background-color: #2196F3;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }

    </style>

<style>
   .pager{
   text-align: right!important;
   }
   a{
   color:#888888;
   }
   a:hover{
   text-decoration: none;
   color:#888888;
   }.my-active{
   border: 1px solid #ddd;
   border-radius: 15px;
   color: white!important;
   }
   .my-active span{
   background-color: #007bff!important;
   }
</style>
<script>
   $('document').ready(function () {
       $("#delete_search").click(function () {
           $('.form-control').val('');
           $('.input').val('');
       });
       $('.duplicate').click(function(){
           alert('This function update last')
       });
       $('.edit').click(function(){
           alert('This function update last')
       });
       $("#datetimepicker1_333_1").datetimepicker({
       format: "DD-MM-YYYY LT",
   });
       $("#datetimepicker1_333_2").datetimepicker({
       format: "DD-MM-YYYY LT",
   });
   $("#datetimepicker2_333_1").datetimepicker({
       format: "DD-MM-YYYY LT",
   });
   $("#datetimepicker2_333_2").datetimepicker({
       format: "DD-MM-YYYY LT",
   });
   $("#box_start_from").css('display','none');
   $("#start_from").click(function(){
    $("#box_start_from").css('display','block')
    $("#box_end_from").css('display','none')
   
    $(".box_option").css('margin-top','55px');
   });
   function parseDate(str) {
   var mdy = str.split('-');
   return new Date(mdy[2], mdy[1], mdy[0]);
   }
   $('#btn_xn_start').click(function(){
     $(".box_option").css('margin-top','0px');
    let date_start_from=$("#date_start_from").val();
    let date_start_to=$("#date_start_to").val();
    let tach_start_from=date_start_from.split(' ');
    let tach2_start_from=tach_start_from[0].split('-');
    let ngay_start_from,thang_start_from,nam_start_from;
   
    ngay_start_from=tach2_start_from[0];
    thang_start_from=start_from=tach2_start_from[1];
    nam_start_from=tach2_start_from[2];
   
   
    let tach_start_to=date_start_to.split(' ');
    let tach2_start_to=tach_start_to[0].split('-');
    let ngay_start_to,thang_start_to,nam_start_to
   
    ngay_start_to=tach2_start_to[0];
    thang_start_to=start_to=tach2_start_to[1];
    nam_start_to=tach2_start_to[2];
   
    if(parseDate(date_start_from).getTime()<parseDate(date_start_to).getTime()){
        alert("Lỗi chọn ngày tháng năm. Vui lòng chọn đúng ngày");
        $("#box_start_from").css('display','none');
   return false;
    }else{
   
   
   
    let data=date_start_from +' - '+date_start_to;
   
    $('#start_from').val(data);
    $("#box_start_from").css('display','none');
   
    }
   })
   
   
   $("#box_end_from").css('display','none');
   $("#end_from").click(function(){
    $("#box_end_from").css('display','block')
    $("#box_start_from").css('display','none')
    $(".box_option").css('margin-top','55px');
   });
   
   $('#btn_xn_end').click(function(){
     $(".box_option").css('margin-top','0px');
   
    let date_end_from=$("#date_end_from").val();
    let date_end_to=$("#date_end_to").val();
    let tach_end_from=date_end_from.split(' ');
    let tach2_end_from=tach_end_from[0].split('-');
    let ngay_end_from,thang_end_from,nam_end_from;
   
    ngay_end_from=tach2_end_from[0];
    thang_end_from=end_from=tach2_end_from[1];
    nam_end_from=tach2_end_from[2];
   
   
    let tach_end_to=date_end_to.split(' ');
    let tach2_end_to=tach_end_to[0].split('-');
    let ngay_end_to,thang_end_to,nam_end_to
   
    ngay_end_to=tach2_end_to[0];
    thang_end_to=end_to=tach2_end_to[1];
    nam_end_to=tach2_end_to[2];
   
    if(parseDate(date_end_from).getTime()<parseDate(date_end_to).getTime()){
        alert("Lỗi chọn ngày tháng năm. Vui lòng chọn đúng ngày");
        $("#box_end_from").css('display','none');
   return false;
    }else{
   
   
   
    let data=date_end_from +' - '+date_end_to;
   
    $('#end_from').val(data);
    $("#box_end_from").css('display','none');
   
    }
   })
       $("#dots11").click(function (e) {
           e.stopPropagation();console.log('10');
           var position = $("#profile").css(["top", "right"]);
           console.log($("#dots").position());
           document.getElementById("box_option_discount11").style = "display: block";
           document.getElementById("box_option_discount12").style = "display: none";
           document.getElementById("box_option_discount13").style = "display: none";
           document.getElementById("box_option_discount14").style = "display: none";
           document.getElementById("box_option_discount15").style = "display: none";
           document.getElementById("box_option_discount16").style = "display: none";
           document.getElementById("box_option_discount17").style = "display: none";
           document.getElementById("box_option_discount18").style = "display: none";
           document.getElementById("box_option_discount19").style = "display: none";
           document.getElementById("box_option_discount20").style = "display: none";
   
   
           document.getElementById("box_option_discount21").style = "display: none";
           document.getElementById("box_option_discount22").style = "display: none";
           document.getElementById("box_option_discount23").style = "display: none";
           document.getElementById("box_option_discount24").style = "display: none";
           document.getElementById("box_option_discount25").style = "display: none";
           document.getElementById("box_option_discount26").style = "display: none";
           document.getElementById("box_option_discount27").style = "display: none";
           document.getElementById("box_option_discount28").style = "display: none";
           document.getElementById("box_option_discount29").style = "display: none";
           document.getElementById("box_option_discount30").style = "display: none";
   
           document.getElementById("box_option_discount2").style = "display: none";
           document.getElementById("box_option_discount3").style = "display: none";
           document.getElementById("box_option_discount4").style = "display: none";
           document.getElementById("box_option_discount5").style = "display: none";
           document.getElementById("box_option_discount6").style = "display: none";
           document.getElementById("box_option_discount1").style = "display: none";
           document.getElementById("box_option_discount8").style = "display: none";
           document.getElementById("box_option_discount9").style = "display: none";
           document.getElementById("box_option_discount7").style = "display: none";
           document.getElementById("box_option_discount10").style = "display: none";
       });
   
       $("#dots12").click(function (e) {
           e.stopPropagation();console.log('10');
           var position = $("#profile").css(["top", "right"]);
           console.log($("#dots").position());
           document.getElementById("box_option_discount12").style = "display: block";
           document.getElementById("box_option_discount11").style = "display: none";
           document.getElementById("box_option_discount13").style = "display: none";
           document.getElementById("box_option_discount14").style = "display: none";
           document.getElementById("box_option_discount15").style = "display: none";
           document.getElementById("box_option_discount16").style = "display: none";
           document.getElementById("box_option_discount17").style = "display: none";
           document.getElementById("box_option_discount18").style = "display: none";
           document.getElementById("box_option_discount19").style = "display: none";
           document.getElementById("box_option_discount20").style = "display: none";
   
   
           document.getElementById("box_option_discount21").style = "display: none";
           document.getElementById("box_option_discount22").style = "display: none";
           document.getElementById("box_option_discount23").style = "display: none";
           document.getElementById("box_option_discount24").style = "display: none";
           document.getElementById("box_option_discount25").style = "display: none";
           document.getElementById("box_option_discount26").style = "display: none";
           document.getElementById("box_option_discount27").style = "display: none";
           document.getElementById("box_option_discount28").style = "display: none";
           document.getElementById("box_option_discount29").style = "display: none";
           document.getElementById("box_option_discount30").style = "display: none";
   
           document.getElementById("box_option_discount2").style = "display: none";
           document.getElementById("box_option_discount3").style = "display: none";
           document.getElementById("box_option_discount4").style = "display: none";
           document.getElementById("box_option_discount5").style = "display: none";
           document.getElementById("box_option_discount6").style = "display: none";
           document.getElementById("box_option_discount1").style = "display: none";
           document.getElementById("box_option_discount8").style = "display: none";
           document.getElementById("box_option_discount9").style = "display: none";
           document.getElementById("box_option_discount7").style = "display: none";
           document.getElementById("box_option_discount10").style = "display: none";
       });
       $("#dots13").click(function (e) {
           e.stopPropagation();console.log('10');
           var position = $("#profile").css(["top", "right"]);
           console.log($("#dots").position());
           document.getElementById("box_option_discount13").style = "display: block";
           document.getElementById("box_option_discount11").style = "display: none";
           document.getElementById("box_option_discount12").style = "display: none";
           document.getElementById("box_option_discount14").style = "display: none";
           document.getElementById("box_option_discount15").style = "display: none";
           document.getElementById("box_option_discount16").style = "display: none";
           document.getElementById("box_option_discount17").style = "display: none";
           document.getElementById("box_option_discount18").style = "display: none";
           document.getElementById("box_option_discount19").style = "display: none";
           document.getElementById("box_option_discount20").style = "display: none";
   
   
           document.getElementById("box_option_discount21").style = "display: none";
           document.getElementById("box_option_discount22").style = "display: none";
           document.getElementById("box_option_discount23").style = "display: none";
           document.getElementById("box_option_discount24").style = "display: none";
           document.getElementById("box_option_discount25").style = "display: none";
           document.getElementById("box_option_discount26").style = "display: none";
           document.getElementById("box_option_discount27").style = "display: none";
           document.getElementById("box_option_discount28").style = "display: none";
           document.getElementById("box_option_discount29").style = "display: none";
           document.getElementById("box_option_discount30").style = "display: none";
   
           document.getElementById("box_option_discount2").style = "display: none";
           document.getElementById("box_option_discount3").style = "display: none";
           document.getElementById("box_option_discount4").style = "display: none";
           document.getElementById("box_option_discount5").style = "display: none";
           document.getElementById("box_option_discount6").style = "display: none";
           document.getElementById("box_option_discount1").style = "display: none";
           document.getElementById("box_option_discount8").style = "display: none";
           document.getElementById("box_option_discount9").style = "display: none";
           document.getElementById("box_option_discount7").style = "display: none";
           document.getElementById("box_option_discount10").style = "display: none";
       });
       $("#dots14").click(function (e) {
           e.stopPropagation();console.log('10');
           var position = $("#profile").css(["top", "right"]);
           console.log($("#dots").position());
           document.getElementById("box_option_discount14").style = "display: block";
           document.getElementById("box_option_discount11").style = "display: none";
           document.getElementById("box_option_discount12").style = "display: none";
           document.getElementById("box_option_discount13").style = "display: none";
           document.getElementById("box_option_discount15").style = "display: none";
           document.getElementById("box_option_discount16").style = "display: none";
           document.getElementById("box_option_discount17").style = "display: none";
           document.getElementById("box_option_discount18").style = "display: none";
           document.getElementById("box_option_discount19").style = "display: none";
           document.getElementById("box_option_discount20").style = "display: none";
   
   
           document.getElementById("box_option_discount21").style = "display: none";
           document.getElementById("box_option_discount22").style = "display: none";
           document.getElementById("box_option_discount23").style = "display: none";
           document.getElementById("box_option_discount24").style = "display: none";
           document.getElementById("box_option_discount25").style = "display: none";
           document.getElementById("box_option_discount26").style = "display: none";
           document.getElementById("box_option_discount27").style = "display: none";
           document.getElementById("box_option_discount28").style = "display: none";
           document.getElementById("box_option_discount29").style = "display: none";
           document.getElementById("box_option_discount30").style = "display: none";
   
           document.getElementById("box_option_discount2").style = "display: none";
           document.getElementById("box_option_discount3").style = "display: none";
           document.getElementById("box_option_discount4").style = "display: none";
           document.getElementById("box_option_discount5").style = "display: none";
           document.getElementById("box_option_discount6").style = "display: none";
           document.getElementById("box_option_discount1").style = "display: none";
           document.getElementById("box_option_discount8").style = "display: none";
           document.getElementById("box_option_discount9").style = "display: none";
           document.getElementById("box_option_discount7").style = "display: none";
           document.getElementById("box_option_discount10").style = "display: none";
       });
       $("#dots15").click(function (e) {
           e.stopPropagation();console.log('10');
           var position = $("#profile").css(["top", "right"]);
           console.log($("#dots").position());
           document.getElementById("box_option_discount15").style = "display: block";
           document.getElementById("box_option_discount11").style = "display: none";
           document.getElementById("box_option_discount12").style = "display: none";
           document.getElementById("box_option_discount13").style = "display: none";
           document.getElementById("box_option_discount14").style = "display: none";
           document.getElementById("box_option_discount16").style = "display: none";
           document.getElementById("box_option_discount17").style = "display: none";
           document.getElementById("box_option_discount18").style = "display: none";
           document.getElementById("box_option_discount19").style = "display: none";
           document.getElementById("box_option_discount20").style = "display: none";
   
   
           document.getElementById("box_option_discount21").style = "display: none";
           document.getElementById("box_option_discount22").style = "display: none";
           document.getElementById("box_option_discount23").style = "display: none";
           document.getElementById("box_option_discount24").style = "display: none";
           document.getElementById("box_option_discount25").style = "display: none";
           document.getElementById("box_option_discount26").style = "display: none";
           document.getElementById("box_option_discount27").style = "display: none";
           document.getElementById("box_option_discount28").style = "display: none";
           document.getElementById("box_option_discount29").style = "display: none";
           document.getElementById("box_option_discount30").style = "display: none";
   
           document.getElementById("box_option_discount2").style = "display: none";
           document.getElementById("box_option_discount3").style = "display: none";
           document.getElementById("box_option_discount4").style = "display: none";
           document.getElementById("box_option_discount5").style = "display: none";
           document.getElementById("box_option_discount6").style = "display: none";
           document.getElementById("box_option_discount1").style = "display: none";
           document.getElementById("box_option_discount8").style = "display: none";
           document.getElementById("box_option_discount9").style = "display: none";
           document.getElementById("box_option_discount7").style = "display: none";
           document.getElementById("box_option_discount10").style = "display: none";
       });
       $("#dots16").click(function (e) {
           e.stopPropagation();console.log('10');
           var position = $("#profile").css(["top", "right"]);
           console.log($("#dots").position());
           document.getElementById("box_option_discount16").style = "display: block";
           document.getElementById("box_option_discount11").style = "display: none";
           document.getElementById("box_option_discount12").style = "display: none";
           document.getElementById("box_option_discount13").style = "display: none";
           document.getElementById("box_option_discount14").style = "display: none";
           document.getElementById("box_option_discount15").style = "display: none";
           document.getElementById("box_option_discount17").style = "display: none";
           document.getElementById("box_option_discount18").style = "display: none";
           document.getElementById("box_option_discount19").style = "display: none";
           document.getElementById("box_option_discount20").style = "display: none";
   
   
           document.getElementById("box_option_discount21").style = "display: none";
           document.getElementById("box_option_discount22").style = "display: none";
           document.getElementById("box_option_discount23").style = "display: none";
           document.getElementById("box_option_discount24").style = "display: none";
           document.getElementById("box_option_discount25").style = "display: none";
           document.getElementById("box_option_discount26").style = "display: none";
           document.getElementById("box_option_discount27").style = "display: none";
           document.getElementById("box_option_discount28").style = "display: none";
           document.getElementById("box_option_discount29").style = "display: none";
           document.getElementById("box_option_discount30").style = "display: none";
   
           document.getElementById("box_option_discount2").style = "display: none";
           document.getElementById("box_option_discount3").style = "display: none";
           document.getElementById("box_option_discount4").style = "display: none";
           document.getElementById("box_option_discount5").style = "display: none";
           document.getElementById("box_option_discount6").style = "display: none";
           document.getElementById("box_option_discount1").style = "display: none";
           document.getElementById("box_option_discount8").style = "display: none";
           document.getElementById("box_option_discount9").style = "display: none";
           document.getElementById("box_option_discount7").style = "display: none";
           document.getElementById("box_option_discount10").style = "display: none";
       });
       $("#dots17").click(function (e) {
           e.stopPropagation();console.log('10');
           var position = $("#profile").css(["top", "right"]);
           console.log($("#dots").position());
           document.getElementById("box_option_discount17").style = "display: block";
           document.getElementById("box_option_discount11").style = "display: none";
           document.getElementById("box_option_discount12").style = "display: none";
           document.getElementById("box_option_discount13").style = "display: none";
           document.getElementById("box_option_discount14").style = "display: none";
           document.getElementById("box_option_discount15").style = "display: none";
           document.getElementById("box_option_discount16").style = "display: none";
           document.getElementById("box_option_discount18").style = "display: none";
           document.getElementById("box_option_discount19").style = "display: none";
           document.getElementById("box_option_discount20").style = "display: none";
   
   
           document.getElementById("box_option_discount21").style = "display: none";
           document.getElementById("box_option_discount22").style = "display: none";
           document.getElementById("box_option_discount23").style = "display: none";
           document.getElementById("box_option_discount24").style = "display: none";
           document.getElementById("box_option_discount25").style = "display: none";
           document.getElementById("box_option_discount26").style = "display: none";
           document.getElementById("box_option_discount27").style = "display: none";
           document.getElementById("box_option_discount28").style = "display: none";
           document.getElementById("box_option_discount29").style = "display: none";
           document.getElementById("box_option_discount30").style = "display: none";
   
           document.getElementById("box_option_discount2").style = "display: none";
           document.getElementById("box_option_discount3").style = "display: none";
           document.getElementById("box_option_discount4").style = "display: none";
           document.getElementById("box_option_discount5").style = "display: none";
           document.getElementById("box_option_discount6").style = "display: none";
           document.getElementById("box_option_discount1").style = "display: none";
           document.getElementById("box_option_discount8").style = "display: none";
           document.getElementById("box_option_discount9").style = "display: none";
           document.getElementById("box_option_discount7").style = "display: none";
           document.getElementById("box_option_discount10").style = "display: none";
       });
       $("#dots18").click(function (e) {
           e.stopPropagation();console.log('10');
           var position = $("#profile").css(["top", "right"]);
           console.log($("#dots").position());
           document.getElementById("box_option_discount18").style = "display: block";
           document.getElementById("box_option_discount11").style = "display: none";
           document.getElementById("box_option_discount12").style = "display: none";
           document.getElementById("box_option_discount13").style = "display: none";
           document.getElementById("box_option_discount14").style = "display: none";
           document.getElementById("box_option_discount15").style = "display: none";
           document.getElementById("box_option_discount17").style = "display: none";
           document.getElementById("box_option_discount16").style = "display: none";
           document.getElementById("box_option_discount19").style = "display: none";
           document.getElementById("box_option_discount20").style = "display: none";
   
   
           document.getElementById("box_option_discount21").style = "display: none";
           document.getElementById("box_option_discount22").style = "display: none";
           document.getElementById("box_option_discount23").style = "display: none";
           document.getElementById("box_option_discount24").style = "display: none";
           document.getElementById("box_option_discount25").style = "display: none";
           document.getElementById("box_option_discount26").style = "display: none";
           document.getElementById("box_option_discount27").style = "display: none";
           document.getElementById("box_option_discount28").style = "display: none";
           document.getElementById("box_option_discount29").style = "display: none";
           document.getElementById("box_option_discount30").style = "display: none";
   
           document.getElementById("box_option_discount2").style = "display: none";
           document.getElementById("box_option_discount3").style = "display: none";
           document.getElementById("box_option_discount4").style = "display: none";
           document.getElementById("box_option_discount5").style = "display: none";
           document.getElementById("box_option_discount6").style = "display: none";
           document.getElementById("box_option_discount1").style = "display: none";
           document.getElementById("box_option_discount8").style = "display: none";
           document.getElementById("box_option_discount9").style = "display: none";
           document.getElementById("box_option_discount7").style = "display: none";
           document.getElementById("box_option_discount10").style = "display: none";
       });
       $("#dots19").click(function (e) {
           e.stopPropagation();console.log('10');
           var position = $("#profile").css(["top", "right"]);
           console.log($("#dots").position());
           document.getElementById("box_option_discount19").style = "display: block";
           document.getElementById("box_option_discount11").style = "display: none";
           document.getElementById("box_option_discount12").style = "display: none";
           document.getElementById("box_option_discount13").style = "display: none";
           document.getElementById("box_option_discount14").style = "display: none";
           document.getElementById("box_option_discount15").style = "display: none";
           document.getElementById("box_option_discount17").style = "display: none";
           document.getElementById("box_option_discount18").style = "display: none";
           document.getElementById("box_option_discount16").style = "display: none";
           document.getElementById("box_option_discount20").style = "display: none";
   
   
           document.getElementById("box_option_discount21").style = "display: none";
           document.getElementById("box_option_discount22").style = "display: none";
           document.getElementById("box_option_discount23").style = "display: none";
           document.getElementById("box_option_discount24").style = "display: none";
           document.getElementById("box_option_discount25").style = "display: none";
           document.getElementById("box_option_discount26").style = "display: none";
           document.getElementById("box_option_discount27").style = "display: none";
           document.getElementById("box_option_discount28").style = "display: none";
           document.getElementById("box_option_discount29").style = "display: none";
           document.getElementById("box_option_discount30").style = "display: none";
   
           document.getElementById("box_option_discount2").style = "display: none";
           document.getElementById("box_option_discount3").style = "display: none";
           document.getElementById("box_option_discount4").style = "display: none";
           document.getElementById("box_option_discount5").style = "display: none";
           document.getElementById("box_option_discount6").style = "display: none";
           document.getElementById("box_option_discount1").style = "display: none";
           document.getElementById("box_option_discount8").style = "display: none";
           document.getElementById("box_option_discount9").style = "display: none";
           document.getElementById("box_option_discount7").style = "display: none";
           document.getElementById("box_option_discount10").style = "display: none";
       });
       $("#dots20").click(function (e) {
           e.stopPropagation();console.log('10');
           var position = $("#profile").css(["top", "right"]);
           console.log($("#dots").position());
           document.getElementById("box_option_discount20").style = "display: block";
           document.getElementById("box_option_discount11").style = "display: none";
           document.getElementById("box_option_discount12").style = "display: none";
           document.getElementById("box_option_discount13").style = "display: none";
           document.getElementById("box_option_discount14").style = "display: none";
           document.getElementById("box_option_discount15").style = "display: none";
           document.getElementById("box_option_discount17").style = "display: none";
           document.getElementById("box_option_discount18").style = "display: none";
           document.getElementById("box_option_discount19").style = "display: none";
           document.getElementById("box_option_discount16").style = "display: none";
   
   
           document.getElementById("box_option_discount21").style = "display: none";
           document.getElementById("box_option_discount22").style = "display: none";
           document.getElementById("box_option_discount23").style = "display: none";
           document.getElementById("box_option_discount24").style = "display: none";
           document.getElementById("box_option_discount25").style = "display: none";
           document.getElementById("box_option_discount26").style = "display: none";
           document.getElementById("box_option_discount27").style = "display: none";
           document.getElementById("box_option_discount28").style = "display: none";
           document.getElementById("box_option_discount29").style = "display: none";
           document.getElementById("box_option_discount30").style = "display: none";
   
           document.getElementById("box_option_discount2").style = "display: none";
           document.getElementById("box_option_discount3").style = "display: none";
           document.getElementById("box_option_discount4").style = "display: none";
           document.getElementById("box_option_discount5").style = "display: none";
           document.getElementById("box_option_discount6").style = "display: none";
           document.getElementById("box_option_discount1").style = "display: none";
           document.getElementById("box_option_discount8").style = "display: none";
           document.getElementById("box_option_discount9").style = "display: none";
           document.getElementById("box_option_discount7").style = "display: none";
           document.getElementById("box_option_discount10").style = "display: none";
       });
       $("#dots21").click(function (e) {
           e.stopPropagation();console.log('10');
           var position = $("#profile").css(["top", "right"]);
           console.log($("#dots").position());
           document.getElementById("box_option_discount21").style = "display: block";
           document.getElementById("box_option_discount11").style = "display: none";
           document.getElementById("box_option_discount12").style = "display: none";
           document.getElementById("box_option_discount13").style = "display: none";
           document.getElementById("box_option_discount14").style = "display: none";
           document.getElementById("box_option_discount15").style = "display: none";
           document.getElementById("box_option_discount17").style = "display: none";
           document.getElementById("box_option_discount18").style = "display: none";
           document.getElementById("box_option_discount19").style = "display: none";
           document.getElementById("box_option_discount16").style = "display: none";
   
           document.getElementById("box_option_discount20").style = "display: none";
   
           document.getElementById("box_option_discount22").style = "display: none";
           document.getElementById("box_option_discount23").style = "display: none";
           document.getElementById("box_option_discount24").style = "display: none";
           document.getElementById("box_option_discount25").style = "display: none";
           document.getElementById("box_option_discount26").style = "display: none";
           document.getElementById("box_option_discount27").style = "display: none";
           document.getElementById("box_option_discount28").style = "display: none";
           document.getElementById("box_option_discount29").style = "display: none";
           document.getElementById("box_option_discount30").style = "display: none";
   
           document.getElementById("box_option_discount2").style = "display: none";
           document.getElementById("box_option_discount3").style = "display: none";
           document.getElementById("box_option_discount4").style = "display: none";
           document.getElementById("box_option_discount5").style = "display: none";
           document.getElementById("box_option_discount6").style = "display: none";
           document.getElementById("box_option_discount1").style = "display: none";
           document.getElementById("box_option_discount8").style = "display: none";
           document.getElementById("box_option_discount9").style = "display: none";
           document.getElementById("box_option_discount7").style = "display: none";
           document.getElementById("box_option_discount10").style = "display: none";
       });
       $("#dots22").click(function (e) {
           e.stopPropagation();console.log('10');
           var position = $("#profile").css(["top", "right"]);
           console.log($("#dots").position());
           document.getElementById("box_option_discount22").style = "display: block";
           document.getElementById("box_option_discount11").style = "display: none";
           document.getElementById("box_option_discount12").style = "display: none";
           document.getElementById("box_option_discount13").style = "display: none";
           document.getElementById("box_option_discount14").style = "display: none";
           document.getElementById("box_option_discount15").style = "display: none";
           document.getElementById("box_option_discount17").style = "display: none";
           document.getElementById("box_option_discount18").style = "display: none";
           document.getElementById("box_option_discount19").style = "display: none";
           document.getElementById("box_option_discount16").style = "display: none";
   
           document.getElementById("box_option_discount20").style = "display: none";
   
           document.getElementById("box_option_discount21").style = "display: none";
           document.getElementById("box_option_discount23").style = "display: none";
           document.getElementById("box_option_discount24").style = "display: none";
           document.getElementById("box_option_discount25").style = "display: none";
           document.getElementById("box_option_discount26").style = "display: none";
           document.getElementById("box_option_discount27").style = "display: none";
           document.getElementById("box_option_discount28").style = "display: none";
           document.getElementById("box_option_discount29").style = "display: none";
           document.getElementById("box_option_discount30").style = "display: none";
   
           document.getElementById("box_option_discount2").style = "display: none";
           document.getElementById("box_option_discount3").style = "display: none";
           document.getElementById("box_option_discount4").style = "display: none";
           document.getElementById("box_option_discount5").style = "display: none";
           document.getElementById("box_option_discount6").style = "display: none";
           document.getElementById("box_option_discount1").style = "display: none";
           document.getElementById("box_option_discount8").style = "display: none";
           document.getElementById("box_option_discount9").style = "display: none";
           document.getElementById("box_option_discount7").style = "display: none";
           document.getElementById("box_option_discount10").style = "display: none";
       });
       $("#dots23").click(function (e) {
           e.stopPropagation();console.log('10');
           var position = $("#profile").css(["top", "right"]);
           console.log($("#dots").position());
           document.getElementById("box_option_discount23").style = "display: block";
           document.getElementById("box_option_discount11").style = "display: none";
           document.getElementById("box_option_discount12").style = "display: none";
           document.getElementById("box_option_discount13").style = "display: none";
           document.getElementById("box_option_discount14").style = "display: none";
           document.getElementById("box_option_discount15").style = "display: none";
           document.getElementById("box_option_discount17").style = "display: none";
           document.getElementById("box_option_discount18").style = "display: none";
           document.getElementById("box_option_discount19").style = "display: none";
           document.getElementById("box_option_discount16").style = "display: none";
           document.getElementById("box_option_discount20").style = "display: none";
   
   
           document.getElementById("box_option_discount21").style = "display: none";
           document.getElementById("box_option_discount22").style = "display: none";
           document.getElementById("box_option_discount24").style = "display: none";
           document.getElementById("box_option_discount25").style = "display: none";
           document.getElementById("box_option_discount26").style = "display: none";
           document.getElementById("box_option_discount27").style = "display: none";
           document.getElementById("box_option_discount28").style = "display: none";
           document.getElementById("box_option_discount29").style = "display: none";
           document.getElementById("box_option_discount30").style = "display: none";
   
           document.getElementById("box_option_discount2").style = "display: none";
           document.getElementById("box_option_discount3").style = "display: none";
           document.getElementById("box_option_discount4").style = "display: none";
           document.getElementById("box_option_discount5").style = "display: none";
           document.getElementById("box_option_discount6").style = "display: none";
           document.getElementById("box_option_discount1").style = "display: none";
           document.getElementById("box_option_discount8").style = "display: none";
           document.getElementById("box_option_discount9").style = "display: none";
           document.getElementById("box_option_discount7").style = "display: none";
           document.getElementById("box_option_discount10").style = "display: none";
       });
       $("#dots24").click(function (e) {
           e.stopPropagation();console.log('10');
           var position = $("#profile").css(["top", "right"]);
           console.log($("#dots").position());
           document.getElementById("box_option_discount24").style = "display: block";
           document.getElementById("box_option_discount11").style = "display: none";
           document.getElementById("box_option_discount12").style = "display: none";
           document.getElementById("box_option_discount13").style = "display: none";
           document.getElementById("box_option_discount14").style = "display: none";
           document.getElementById("box_option_discount15").style = "display: none";
           document.getElementById("box_option_discount17").style = "display: none";
           document.getElementById("box_option_discount18").style = "display: none";
           document.getElementById("box_option_discount19").style = "display: none";
           document.getElementById("box_option_discount16").style = "display: none";
   
           document.getElementById("box_option_discount20").style = "display: none";
   
           document.getElementById("box_option_discount21").style = "display: none";
           document.getElementById("box_option_discount22").style = "display: none";
           document.getElementById("box_option_discount23").style = "display: none";
           document.getElementById("box_option_discount25").style = "display: none";
           document.getElementById("box_option_discount26").style = "display: none";
           document.getElementById("box_option_discount27").style = "display: none";
           document.getElementById("box_option_discount28").style = "display: none";
           document.getElementById("box_option_discount29").style = "display: none";
           document.getElementById("box_option_discount30").style = "display: none";
   
           document.getElementById("box_option_discount2").style = "display: none";
           document.getElementById("box_option_discount3").style = "display: none";
           document.getElementById("box_option_discount4").style = "display: none";
           document.getElementById("box_option_discount5").style = "display: none";
           document.getElementById("box_option_discount6").style = "display: none";
           document.getElementById("box_option_discount1").style = "display: none";
           document.getElementById("box_option_discount8").style = "display: none";
           document.getElementById("box_option_discount9").style = "display: none";
           document.getElementById("box_option_discount7").style = "display: none";
           document.getElementById("box_option_discount10").style = "display: none";
       });
       $("#dots25").click(function (e) {
           e.stopPropagation();console.log('10');
           var position = $("#profile").css(["top", "right"]);
           console.log($("#dots").position());
           document.getElementById("box_option_discount25").style = "display: block";
           document.getElementById("box_option_discount11").style = "display: none";
           document.getElementById("box_option_discount12").style = "display: none";
           document.getElementById("box_option_discount13").style = "display: none";
           document.getElementById("box_option_discount14").style = "display: none";
           document.getElementById("box_option_discount15").style = "display: none";
           document.getElementById("box_option_discount17").style = "display: none";
           document.getElementById("box_option_discount18").style = "display: none";
           document.getElementById("box_option_discount19").style = "display: none";
           document.getElementById("box_option_discount16").style = "display: none";
   
           document.getElementById("box_option_discount20").style = "display: none";
   
           document.getElementById("box_option_discount21").style = "display: none";
           document.getElementById("box_option_discount22").style = "display: none";
           document.getElementById("box_option_discount23").style = "display: none";
           document.getElementById("box_option_discount24").style = "display: none";
           document.getElementById("box_option_discount26").style = "display: none";
           document.getElementById("box_option_discount27").style = "display: none";
           document.getElementById("box_option_discount28").style = "display: none";
           document.getElementById("box_option_discount29").style = "display: none";
           document.getElementById("box_option_discount30").style = "display: none";
   
           document.getElementById("box_option_discount2").style = "display: none";
           document.getElementById("box_option_discount3").style = "display: none";
           document.getElementById("box_option_discount4").style = "display: none";
           document.getElementById("box_option_discount5").style = "display: none";
           document.getElementById("box_option_discount6").style = "display: none";
           document.getElementById("box_option_discount1").style = "display: none";
           document.getElementById("box_option_discount8").style = "display: none";
           document.getElementById("box_option_discount9").style = "display: none";
           document.getElementById("box_option_discount7").style = "display: none";
           document.getElementById("box_option_discount10").style = "display: none";
       });
       $("#dots26").click(function (e) {
           e.stopPropagation();console.log('10');
           var position = $("#profile").css(["top", "right"]);
           console.log($("#dots").position());
           document.getElementById("box_option_discount26").style = "display: block";
           document.getElementById("box_option_discount11").style = "display: none";
           document.getElementById("box_option_discount12").style = "display: none";
           document.getElementById("box_option_discount13").style = "display: none";
           document.getElementById("box_option_discount14").style = "display: none";
           document.getElementById("box_option_discount15").style = "display: none";
           document.getElementById("box_option_discount17").style = "display: none";
           document.getElementById("box_option_discount18").style = "display: none";
           document.getElementById("box_option_discount19").style = "display: none";
           document.getElementById("box_option_discount16").style = "display: none";
   
           document.getElementById("box_option_discount20").style = "display: none";
   
           document.getElementById("box_option_discount21").style = "display: none";
           document.getElementById("box_option_discount22").style = "display: none";
           document.getElementById("box_option_discount23").style = "display: none";
           document.getElementById("box_option_discount24").style = "display: none";
           document.getElementById("box_option_discount25").style = "display: none";
           document.getElementById("box_option_discount27").style = "display: none";
           document.getElementById("box_option_discount28").style = "display: none";
           document.getElementById("box_option_discount29").style = "display: none";
           document.getElementById("box_option_discount30").style = "display: none";
   
           document.getElementById("box_option_discount2").style = "display: none";
           document.getElementById("box_option_discount3").style = "display: none";
           document.getElementById("box_option_discount4").style = "display: none";
           document.getElementById("box_option_discount5").style = "display: none";
           document.getElementById("box_option_discount6").style = "display: none";
           document.getElementById("box_option_discount1").style = "display: none";
           document.getElementById("box_option_discount8").style = "display: none";
           document.getElementById("box_option_discount9").style = "display: none";
           document.getElementById("box_option_discount7").style = "display: none";
           document.getElementById("box_option_discount10").style = "display: none";
       });
       $("#dots27").click(function (e) {
           e.stopPropagation();console.log('10');
           var position = $("#profile").css(["top", "right"]);
           console.log($("#dots").position());
           document.getElementById("box_option_discount27").style = "display: block";
           document.getElementById("box_option_discount11").style = "display: none";
           document.getElementById("box_option_discount12").style = "display: none";
           document.getElementById("box_option_discount13").style = "display: none";
           document.getElementById("box_option_discount14").style = "display: none";
           document.getElementById("box_option_discount15").style = "display: none";
           document.getElementById("box_option_discount17").style = "display: none";
           document.getElementById("box_option_discount18").style = "display: none";
           document.getElementById("box_option_discount19").style = "display: none";
           document.getElementById("box_option_discount16").style = "display: none";
   
   
           document.getElementById("box_option_discount21").style = "display: none";
           document.getElementById("box_option_discount22").style = "display: none";
           document.getElementById("box_option_discount23").style = "display: none";
           document.getElementById("box_option_discount24").style = "display: none";
           document.getElementById("box_option_discount25").style = "display: none";
           document.getElementById("box_option_discount26").style = "display: none";
           document.getElementById("box_option_discount28").style = "display: none";
           document.getElementById("box_option_discount29").style = "display: none";
           document.getElementById("box_option_discount30").style = "display: none";
           document.getElementById("box_option_discount20").style = "display: none";
   
           document.getElementById("box_option_discount2").style = "display: none";
           document.getElementById("box_option_discount3").style = "display: none";
           document.getElementById("box_option_discount4").style = "display: none";
           document.getElementById("box_option_discount5").style = "display: none";
           document.getElementById("box_option_discount6").style = "display: none";
           document.getElementById("box_option_discount1").style = "display: none";
           document.getElementById("box_option_discount8").style = "display: none";
           document.getElementById("box_option_discount9").style = "display: none";
           document.getElementById("box_option_discount7").style = "display: none";
           document.getElementById("box_option_discount10").style = "display: none";
       });
       $("#dots28").click(function (e) {
           e.stopPropagation();console.log('10');
           var position = $("#profile").css(["top", "right"]);
           console.log($("#dots").position());
           document.getElementById("box_option_discount28").style = "display: block";
           document.getElementById("box_option_discount11").style = "display: none";
           document.getElementById("box_option_discount12").style = "display: none";
           document.getElementById("box_option_discount13").style = "display: none";
           document.getElementById("box_option_discount14").style = "display: none";
           document.getElementById("box_option_discount15").style = "display: none";
           document.getElementById("box_option_discount17").style = "display: none";
           document.getElementById("box_option_discount18").style = "display: none";
           document.getElementById("box_option_discount19").style = "display: none";
           document.getElementById("box_option_discount16").style = "display: none";
   
           document.getElementById("box_option_discount20").style = "display: none";
   
           document.getElementById("box_option_discount21").style = "display: none";
           document.getElementById("box_option_discount22").style = "display: none";
           document.getElementById("box_option_discount23").style = "display: none";
           document.getElementById("box_option_discount24").style = "display: none";
           document.getElementById("box_option_discount25").style = "display: none";
           document.getElementById("box_option_discount26").style = "display: none";
           document.getElementById("box_option_discount27").style = "display: none";
           document.getElementById("box_option_discount29").style = "display: none";
           document.getElementById("box_option_discount30").style = "display: none";
   
           document.getElementById("box_option_discount2").style = "display: none";
           document.getElementById("box_option_discount3").style = "display: none";
           document.getElementById("box_option_discount4").style = "display: none";
           document.getElementById("box_option_discount5").style = "display: none";
           document.getElementById("box_option_discount6").style = "display: none";
           document.getElementById("box_option_discount1").style = "display: none";
           document.getElementById("box_option_discount8").style = "display: none";
           document.getElementById("box_option_discount9").style = "display: none";
           document.getElementById("box_option_discount7").style = "display: none";
           document.getElementById("box_option_discount10").style = "display: none";
       });
       $("#dots29").click(function (e) {
           e.stopPropagation();console.log('10');
           var position = $("#profile").css(["top", "right"]);
           console.log($("#dots").position());
           document.getElementById("box_option_discount29").style = "display: block";
           document.getElementById("box_option_discount11").style = "display: none";
           document.getElementById("box_option_discount12").style = "display: none";
           document.getElementById("box_option_discount13").style = "display: none";
           document.getElementById("box_option_discount14").style = "display: none";
           document.getElementById("box_option_discount15").style = "display: none";
           document.getElementById("box_option_discount17").style = "display: none";
           document.getElementById("box_option_discount18").style = "display: none";
           document.getElementById("box_option_discount19").style = "display: none";
           document.getElementById("box_option_discount16").style = "display: none";
   
           document.getElementById("box_option_discount20").style = "display: none";
   
           document.getElementById("box_option_discount21").style = "display: none";
           document.getElementById("box_option_discount22").style = "display: none";
           document.getElementById("box_option_discount23").style = "display: none";
           document.getElementById("box_option_discount24").style = "display: none";
           document.getElementById("box_option_discount25").style = "display: none";
           document.getElementById("box_option_discount26").style = "display: none";
           document.getElementById("box_option_discount27").style = "display: none";
           document.getElementById("box_option_discount28").style = "display: none";
           document.getElementById("box_option_discount30").style = "display: none";
   
           document.getElementById("box_option_discount2").style = "display: none";
           document.getElementById("box_option_discount3").style = "display: none";
           document.getElementById("box_option_discount4").style = "display: none";
           document.getElementById("box_option_discount5").style = "display: none";
           document.getElementById("box_option_discount6").style = "display: none";
           document.getElementById("box_option_discount1").style = "display: none";
           document.getElementById("box_option_discount8").style = "display: none";
           document.getElementById("box_option_discount9").style = "display: none";
           document.getElementById("box_option_discount7").style = "display: none";
           document.getElementById("box_option_discount10").style = "display: none";
       });
       $("#dots30").click(function (e) {
           e.stopPropagation();console.log('10');
           var position = $("#profile").css(["top", "right"]);
           console.log($("#dots").position());
           document.getElementById("box_option_discount30").style = "display: block";
           document.getElementById("box_option_discount11").style = "display: none";
           document.getElementById("box_option_discount12").style = "display: none";
           document.getElementById("box_option_discount13").style = "display: none";
           document.getElementById("box_option_discount14").style = "display: none";
           document.getElementById("box_option_discount15").style = "display: none";
           document.getElementById("box_option_discount17").style = "display: none";
           document.getElementById("box_option_discount18").style = "display: none";
           document.getElementById("box_option_discount19").style = "display: none";
           document.getElementById("box_option_discount16").style = "display: none";
   
   
           document.getElementById("box_option_discount20").style = "display: none";
           document.getElementById("box_option_discount21").style = "display: none";
           document.getElementById("box_option_discount22").style = "display: none";
           document.getElementById("box_option_discount23").style = "display: none";
           document.getElementById("box_option_discount24").style = "display: none";
           document.getElementById("box_option_discount25").style = "display: none";
           document.getElementById("box_option_discount26").style = "display: none";
           document.getElementById("box_option_discount27").style = "display: none";
           document.getElementById("box_option_discount29").style = "display: none";
           document.getElementById("box_option_discount28").style = "display: none";
   
           document.getElementById("box_option_discount2").style = "display: none";
           document.getElementById("box_option_discount3").style = "display: none";
           document.getElementById("box_option_discount4").style = "display: none";
           document.getElementById("box_option_discount5").style = "display: none";
           document.getElementById("box_option_discount6").style = "display: none";
           document.getElementById("box_option_discount1").style = "display: none";
           document.getElementById("box_option_discount8").style = "display: none";
           document.getElementById("box_option_discount9").style = "display: none";
           document.getElementById("box_option_discount7").style = "display: none";
           document.getElementById("box_option_discount10").style = "display: none";
       });
   
   });
   
</script>