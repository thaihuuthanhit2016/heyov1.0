@extends('admin.master');
@section('content')

    <div class="box_content">
        <div class="wrapper">
            <div class="box_title">
                <div class="title">Tra cứu khuyến mãi</div>
            </div>
            <div style="margin: 0px" class="dropdown-divider"></div>

            <form method="post">
                @csrf
            <div class="content">
                <div class="row_input">
                    <div class="box_input">
                        <div class="label">Mã chương trình</div>
                        <input placeholder="Nhập chương trình" name="program_code_promo"value="{{Session::get('program_code_promo')}}" class="input" />
                    </div>
                    <div class="box_input">
                        <div class="label">Mã khuyến mãi</div>
                        <input placeholder="Nhập mã chương trình" class="input" name="code_promotion" value="{{Session::get('code_promotion')}}"  />
                    </div>
                </div>
                <div class="row_input">
                    <div class="box_input">
                        <div class="label">Loại khuyến mãi</div>
                        <select id="type_promo" name="type_promo">
                        <option value>chọn</option>

                        <option value="1" @if(Session::get('type_promo')==1) selected @endif >Promo Code</option>
                        <option value="2" @if(Session::get('type_promo')==2) selected @endif >Unique Code</option>
                        <option value="3" @if(Session::get('type_promo')==3) selected @endif >Partner Code</option>
                        <option value="4" @if(Session::get('type_promo')==4) selected @endif > Special Campaign</option>
                        </select>
                    </div>
                    <div class="box_input">
                        <div class="label">Trạng thái</div>
                        <select  name="status">

                            <option value>chọn</option>
                            <option @if(Session::get('status')=='Thanh') selected @endif value="0">Inactive</option>
                            <option @if(Session::get('status')==1) selected @endif value="1">Active</option>
                            <option @if(Session::get('status')==2) selected @endif value="2">Deactive</option>
                            <option @if(Session::get('status')==3) selected @endif value="3">Expired</option>
                        </select>
                    </div>

                    <?php

                    if(Session::put('status','')){
                        Session::put('status','Thanh');
                    }
                    ?>
                    <div class="box_input">
                        <div class="label">Quốc gia</div>
                        <select name="nation" >
                            <option value>chọn</option>

                            @foreach($locations as $l)
                                <option @if(Session::get('nation')==$l->id)   selected @endif value="{{$l->id}}">{{$l->country_name_vn}} - {{$l->country_name_en}}</option>
                            @endforeach                            </select>
                    </div>
                </div>
                <div class="row_input">
                    <div class="box_input">
                        <div class="label">Ngày bắt đầu</div>
                        <div class="wrapper_date">
                            <div style="width: 100%;margin-top: 5px;" class="input-group date" id="datetimepicker1">
                                <input type="text" name="date_start" name="date_start"  value="{{Session::get('date_start')}}" class="form-control" value="">
                                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                            </div>
                        </div>
                    </div>
                    <div class="box_input">
                        <div class="label">Ngày kết thúc</div>
                        <div class="wrapper_date">
                            <div style="width: 100%;margin-top: 5px;" class="input-group date" id="datetimepicker2">
                                <input type="text" name="date_start" name="date_end"value="{{Session::get('date_end')}}" class="form-control" value="">
                                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box_option">
                <button type="submit" style="text-decoration: none;border: 0px;" class="button_option">
                    <span style="width: 20px; height: 20px; color: #fff;" class="iconify" data-icon="carbon:search"></span>
                    <span class="name_button_option">Tìm kiếm</span>
                </button>
                <button class="btn button_clear" id="delete_search">
                    Xoá bộ lọc
                </button>
            </div>
        </div>
        <div class="wrapper">
            <div class="box_title">
                <div class="title">4 khuyến mãi</div>
                <a href="{{asset('admin/hotel/add')}}" style="text-decoration: none;" class="button_option">
            <span style="width: 20px; height: 20px; color: #fff;" class="iconify"
                  data-icon="fluent:add-16-filled"></span>
                    <span class="name_button_option">Thêm mới</span>
                </a>
            </div>
            <div class="content">
                <table>

                    <tr>
                        <th class="small"> </th>
                        <th class="medium">Mã chương trình</th>
                        <th class="medium">Mã khuyến mãi</th>
                        <th class="big">Chương trình khuyến mãi</th>
                        <th class="medium">Loại khuyến mãi</th>
                        <th class="medium">Ngày bắt đầu</th>
                        <th class="medium">Ngày kết thúc</th>
                        <th class="medium">Số lượng</th>
                        <th class="medium">Người tạo</th>
                        <th class="small"> </th>
                    </tr>
                    <?php $i=0;?>

                    @if($promo!=null)
                        @foreach($promo as $po)

                            <?php
                            $i++;
                            $program_code_promo=$po->program_code_promo;
                            $tach=explode(' ',$program_code_promo);
                            $user=DB::table('tbl_admin')->where('id_admin',$po->ud_user)->first();
                            $nation=DB::table('countries')->where('id',$po->id_nation)->first();
                            ?>
                    <tr>
                        <td>
                            <input id="toggle-state-switch" type="checkbox" data="1" value="true" onchange="test(value)"
                                   data-toggle="toggle" data-size="xs" data-style="ios">
                        </td>
                        <td class="ma_chuong_trinh">
                            <a href="{{asset('admin/hotel/view/'.$po->id_promo)}}"> {{$tach[0]}}<br> {{$tach[1]}}</a>
                        </td>
                        <td class="ma_khuyen_mai">
                            <div class="ten"> <a href="{{asset('admin/hotel/view/'.$po->id_promo)}}">{{$po->code_promotion}}</a></div>
                            <div class="active">
                                <a href="{{asset('admin/hotel/view/'.$po->id_promo)}}"> @if($po->status==0) <span style="color: orange">Inactive</span> @elseif($po->status==1)<span style="color: green">Active</span> @elseif($po->status==2)<span style="color: red">Deactive</span> @else<span style="color: grey"> Expired </span>@endif</a>
                            </div>
                        </td>
                        <td class="chuong_trinh_khuyen_mai">
                            <a href="{{asset('admin/hotel/view/'.$po->id_promo)}}">{{$po->title }}</a>
                        </td>
                        <td class="loai_khuyen_mai">
                            <div class="ten"> <a href="{{asset('admin/hotel/view/'.$po->id_promo)}}">@if($po->type_promo ==1)Promo Code @elseif($po->type_promo ==2) Unique Code @elseif($po->type_promo ==3)Partner Code @else Special Campaign @endif</a></div>
                            <div class="country"> <a href="{{asset('admin/hotel/view/'.$po->id_promo)}}">{{$nation->country_name_vn}}</a></div>
                        </td>
                        <td class="loai_khuyen_mai">
                            <span class="ten"> <a href="{{asset('admin/hotel/view/'.$po->id_promo)}}">{{$po->date_start}}</a></span>
                        </td>
                        <td class="loai_khuyen_mai">
                            <span class="ten"> <a href="{{asset('admin/hotel/view/'.$po->id_promo)}}">{{$po->date_end}}</a></span>
                        </td>
                        <td class="loai_khuyen_mai">
                            <span class="ten"> <a href="{{asset('admin/hotel/view/'.$po->id_promo)}}">{{$po->number_max}}</a></span>
                        </td>
                        <td class="loai_khuyen_mai">
                            <span class="ten"> <a href="{{asset('admin/hotel/view/'.$po->id_promo)}}">@if($po->ud_user!=1) {{$user->admin_name}} @else Admin @endif</a></span>
                        </td>
                        <td>
                            <div class="wrapper_icon">
                                <div id="dots{{$i}}">
                    <span style="color: #C1C1C1; width: 25px; height: 25px;" class="iconify"
                          data-icon="bi:three-dots"></span>
                                </div>
                                <div id="box_option_discount{{$i}}">
                                   <div class="button_option_discount">
                                        <span style="width: 20px;height: 20px;" class="iconify" data-icon="clarity:note-edit-line"></span>
                                       <div class="name"><a href="{{asset('admin/hotel/edit/'.$po->id_promo)}}" >Chỉnh sửa</a></div>
                                        </div>

                                     <div class="button_option_discount">
                      <span style="width: 20px;height: 20px;" class="iconify"
                            data-icon="mdi-light:content-duplicate"></span>
                                         <div class="name"><a href="{{asset('admin/hotel/duplicate/'.$po->id_promo)}}" >Duplicate</a></div>
                                        </div>

                                    <div class="button_option_discount">
                                        <span style="width: 20px;height: 20px;" class="iconify" data-icon="ion:trash-outline"></span>
                                        <div class="name"><a href="{{asset('admin/hotel/del/'.$po->id_promo)}}" >Xoá</a></div>
                                    </div>
                                    <div class="triangle"></div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                    @endif
                </table>
                {{$promo->links('admin.paginate')}}

            </div>
            <div class="box_title"> </div>
        </div>
    </div>
@endsection
