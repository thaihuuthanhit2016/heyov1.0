<!DOCTYPE html>
<html lang="en">

<head>
    <title>HF Admin</title>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="{{asset('public/admin/css/index.css')}}" />
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="https://code.iconify.design/2/2.2.1/iconify.min.js"></script>
    <script src="https://unpkg.com/@popperjs/core@2"></script>
    <link rel='stylesheet'
          href='https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css'>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js'></script>
    <script
        src='https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js'></script>
    <script></script>
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css"
          rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
    <script src="{{asset('public/admin/js/main.js')}}"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <!-- <link rel="stylesheet" type="text/css" href="/jquery.datetimepicker.css"/ >
      <script src="/jquery.js"></script>
      <script src="/build/jquery.datetimepicker.full.min.js"></script> -->
    <style>
        .toggle.ios,
        .toggle-on.ios,
        .toggle-off.ios {
            border-radius: 20rem;
        }

        .toggle.ios .toggle-handle {
            border-radius: 20rem;
        }
    </style>
    <script>
        function test(value) {
            alert(typeof value)
        }
    </script>
</head>

<body>
<div class="menu_header">
    <div class="left">
        <a href="#" class="logo">HEYO TRIP</a>
    </div>
    <div class="right">
        <span class="name_menu"> Chương trình khuyến mãi </span>
        <div class="box_admin">
        <span class="iconify" style="width: 25px; height: 25px; color: #032044"
              data-icon="ic:baseline-notifications-none"></span>
            <div class="badge badge-danger counter">9</div>
            <div id="btn_admin" class="wrapper_admin">
                <span style="width: 25px; height: 25px; color: #032044" class="iconify" data-icon="lucide:user"></span>
                <span class="admin">Admin</span>
                <span style="
                width: 15px;
                height: 15px;
                color: #032044;
                margin-left: 10px;
              " class="iconify" data-icon="ant-design:caret-down-filled"></span>
            </div>
        </div>
    </div>
    <div id="profile" style="box-shadow: 10;" class="box_profile">
      <span style="width: 30px; height: 30px;color: #032044;" class="iconify"
            data-icon="simple-line-icons:logout"></span>
        <span class="logout">Đăng xuất</span>
    </div>
</div>
<div style="display: flex">
    <div class="menu_left">
        <div class="top">
            <a href="#" style="background-color: #f2f9ff; text-decoration: none" class="item">
          <span class="iconify" style="color: #032044; width: 30px; height: 30px"
                data-icon="icon-park-outline:hotel"></span>
                <span class="name_menu">Khách sạn</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
                <span class="iconify" style="color: #526a87; width: 30px; height: 30px" data-icon="ps:plane"></span>
                <span style="color: #526a87" class="name_menu">Vé máy bay</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
                <span class="iconify" style="color: #526a87; width: 30px; height: 30px" data-icon="clarity:car-line"></span>
                <span style="color: #526a87" class="name_menu">Thuê xe</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
          <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                data-icon="clarity:data-cluster-line"></span>
                <span style="color: #526a87" class="name_menu">Crypto</span>
            </a>
        </div>
        <div class="top">
            <a href="#" style="text-decoration: none" class="item">
                <span class="iconify" style="color: #526a87; width: 30px; height: 30px" data-icon="ep:user-filled"></span>
                <span style="color: #526a87" class="name_menu">Tài khoản</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
          <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                data-icon="dashicons:welcome-widgets-menus"></span>
                <span style="color: #526a87" class="name_menu">Danh mục</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
                <span class="iconify" style="color: #526a87; width: 30px; height: 30px" data-icon="gg:website"></span>
                <span style="color: #526a87" class="name_menu">Website</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
          <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                data-icon="material-symbols:settings-suggest-outline-sharp"></span>
                <span style="color: #526a87" class="name_menu">Cấu hình</span>
            </a>
        </div>
    </div>
    <div class="menu_right">
        <a href="#" class="item">
        <span style="width: 25px; height: 25px; color: #032044" class="iconify"
              data-icon="majesticons:analytics"></span>
            <span class="name_menu">Dashboard</span>
        </a>
        <a href="#" class="item">
            <span style="width: 25px; height: 25px; color: #032044" class="iconify" data-icon="mdi:bed"></span>
            <span class="name_menu">Tìm - Đặt phòng</span>
        </a>
        <a href="#" class="item">
        <span style="width: 25px; height: 25px; color: #032044" class="iconify"
              data-icon="fa-solid:calendar-day"></span>
            <span class="name_menu">Quản lý đơn đặt phòng</span>
        </a>
        <div href="#" class="item">
        <span style="width: 25px; height: 25px; color: #032044" class="iconify"
              data-icon="fa-solid:money-check-alt"></span>
            <span class="name_menu">Tuỳ chỉnh giá phòng</span>
        </div>
        <div class="item">
        <span style="width: 25px; height: 25px; color: #329223" class="iconify"
              data-icon="icomoon-free:price-tags"></span>
            <span style="color: #329223; font-weight: 800" class="name_menu">Chương trình khuyến mãi</span>
        </div>
        <div class="item">
            <span style="width: 25px; height: 25px; color: #032044" class="iconify" data-icon="eva:percent-fill"></span>
            <span class="name_menu">Quản lý hoa hồng</span>
        </div>
        <div class="item">
        <span style="width: 25px; height: 25px; color: #032044" class="iconify"
              data-icon="fa6-solid:hand-holding-dollar"></span>
            <span class="name_menu">Huỷ - Hoàn tiền</span>
        </div>
        <div class="item">
        <span style="width: 25px; height: 25px; color: #032044" class="iconify"
              data-icon="fluent:news-20-filled"></span>
            <span class="name_menu">Bài viết Marketing</span>
        </div>
    </div>
    <div class="box_content">
        <div class="wrapper">
            <div class="box_title">
                <div class="title">Tra cứu khuyến mãi</div>
            </div>

            <form method="post">
                @csrf
            <div style="margin: 0px" class="dropdown-divider"></div>
            <div class="content">
                <div class="row_input">
                    <div class="box_input">
                        <div class="label">Mã chương trình</div>
                        <input placeholder="Nhập chương trình"name="program_code_promo"value="{{Session::get('program_code_promo')}}"  class="input" />
                    </div>
                    <div class="box_input">
                        <div class="label">Mã khuyến mãi</div>
                        <input placeholder="Nhập mã chương trình"name="code_promotion" value="{{Session::get('code_promotion')}}"  class="input" />
                    </div>
                </div>
                <div class="row_input">
                    <div class="box_input">
                        <div class="label">Loại khuyến mãi</div>
                        <select id="type_promo" name="type_promo">
                            <option value>chọn</option>

                            <option value="1" @if(Session::get('type_promo')==1) selected @endif >Promo Code</option>
                            <option value="2" @if(Session::get('type_promo')==2) selected @endif >Unique Code</option>
                            <option value="3" @if(Session::get('type_promo')==3) selected @endif >Partner Code</option>
                            <option value="4" @if(Session::get('type_promo')==4) selected @endif > Special Campaign</option>
                        </select>
                    </div>
                    <div class="box_input">
                        <div class="label">Trạng thái</div>
                        <select  name="status">

                        <option value>chọn</option>
                        <option @if(Session::get('status')=='Thanh') selected @endif value="0">Inactive</option>
                        <option @if(Session::get('status')==1) selected @endif value="1">Active</option>
                        <option @if(Session::get('status')==2) selected @endif value="2">Deactive</option>
                        <option @if(Session::get('status')==3) selected @endif value="3">Expired</option>
                        </select>
                    </div> <?php

                    if(Session::put('status','')){
                        Session::put('status','Thanh');
                    }
                    ?>
                    <div class="box_input">
                        <div class="label">Quốc gia</div>
                        <select  name="nation" >
                            <option value>chọn</option>

                            @foreach($locations as $l)
                                <option @if(Session::get('nation')==$l->id)   selected @endif value="{{$l->id}}">{{$l->country_name_vn}} - {{$l->country_name_en}}</option>
                        @endforeach
                        </select>
                    </div>
                </div>
                <div class="row_input">
                    <div class="box_input">
                        <div class="label">Ngày bắt đầu</div>
                        <div class="wrapper_date">
                            <div style="width: 100%;margin-top: 5px;"  class="input-group date" id="datetimepicker1">
                                <input type="text" name="date_start"  value="{{Session::get('date_start')}}"  required="" class="form-control" value="">
                                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                            </div>
                        </div>
                    </div>
                    <div class="box_input">
                        <div class="label">Ngày kết thúc</div>
                        <div class="wrapper_date">
                            <div style="width: 100%;margin-top: 5px;" class="input-group date" id="datetimepicker2">
                                <input type="text" name="date_end"value="{{Session::get('date_end')}}" required="" class="form-control" value="">
                                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box_option">
                <button type="submit" style="text-decoration: none;border: 0px;" class="button_option">
                    <span style="width: 20px; height: 20px; color: #fff;" class="iconify" data-icon="carbon:search"></span>
                    <span class="name_button_option">Tìm kiếm</span>
                </button>
                <button class="btn button_clear"id="delete_search">
                    Xoá bộ lọc
                </button>
            </div>
        </div>
        <div class="wrapper">
            <div class="box_title">
                <div class="title">{{Session::get('soluong')}} khuyến mãi</div>
                <a href="{{asset('admin/hotel/add')}}"style="text-decoration: none;" class="button_option">
            <span style="width: 20px; height: 20px; color: #fff;" class="iconify"
                  data-icon="fluent:add-16-filled"></span>
                    <span class="name_button_option">Thêm mới</span>
                    </a>
            </div>
            <div class="content">
                <table>
                    <tr>
                        <th class="small"> </th>
                        <th class="medium">Mã chương trình</th>
                        <th class="medium">Mã khuyến mãi</th>
                        <th class="big">Chương trình khuyến mãi</th>
                        <th class="medium">Loại khuyến mãi</th>
                        <th class="medium">Ngày bắt đầu</th>
                        <th class="medium">Ngày kết thúc</th>
                        <th class="medium">Số lượng</th>
                        <th class="medium">Người tạo</th>
                        <th class="small"> </th>
                    </tr>
                    <?php $i=0;?>

                    @if($promo!=null)
                        @foreach($promo as $po)

                            <?php
                            $i++;
                            $program_code_promo=$po->program_code_promo;
                            $tach=explode(' ',$program_code_promo);
                            $user=DB::table('tbl_admin')->where('id_admin',$po->ud_user)->first();
                            $id_nation=DB::table('countries')->where('id',$po->id_nation)->first();
                            ?>
                    <tr>
                        <td>
                            <input id="toggle-state-switch" type="checkbox" data="1" value="true" onchange="test(value)"
                                   data-toggle="toggle" data-size="sm" data-style="ios">
                        </td>
                        <td class="ma_chuong_trinh">
                            <div>{{$tach[0]}}</div>
                            <div>{{$tach[1]}} </div>
                        </td>
                        <td class="ma_khuyen_mai">
                            <div class="ten">{{$po->code_promotion}}</div>
                            <div class=" @if($po->status==0)inactive @elseif($po->status==1)active  @elseif($po->status==2)deactive @else expired @endif">
                                @if($po->status==0) Inactive @elseif($po->status==1)Active @elseif($po->status==2)Deactive @else Expired @endif
                             </div>
                        </td>
                        <td class="chuong_trinh_khuyen_mai">
                            {{$po->title }}                        </td>
                        <td class="loai_khuyen_mai">
                            <div class="ten"> @if($po->type_promo ==1)Promo Code @elseif($po->type_promo ==2) Unique Code @elseif($po->type_promo ==3)Partner Code @else Special Campaign @endif</div>
                            <div class="country">{{$id_nation->country_name_vn}}</div>
                        </td>
                        <td class="loai_khuyen_mai">
                            <span class="ten"> {{$po->date_start}}</span>
                        </td>
                        <td class="loai_khuyen_mai">
                            <span class="ten">  {{$po->date_end}}</span>
                        </td>
                        <td class="loai_khuyen_mai">
                            <span class="ten">{{$po->number_max}}</span>
                        </td>
                        <td class="loai_khuyen_mai">
                            <span class="ten">                            @if($po->ud_user!=1) {{$user->admin_name}} @else Admin @endif
</span>
                        </td>
                        <td>
                            <div class="wrapper_icon">
                                <div id="dots">
                    <span style="color: #C1C1C1; width: 25px; height: 25px;" class="iconify"
                          data-icon="bi:three-dots"></span>
                                </div>
                                <div id="box_option_discount">
                                    <div class="button_option_discount">
                                        <span style="width: 20px;height: 20px;" class="iconify" data-icon="clarity:note-edit-line"></span>
                                        <div class="name"><a href='{{asset('admin/hotel/edit/'.$po->id_promo)}}'> Chỉnh sửa</a></div>
                                    </div>
                                    <div class="button_option_discount">
                      <span style="width: 20px;height: 20px;" class="iconify"
                            data-icon="mdi-light:content-duplicate"></span>
                                        <div class="name"><a href='{{asset('admin/hotel/duplicate/'.$po->id_promo)}}'> Duplicate</a></div>
                                    </div>
                                    <div class="button_option_discount">
                                        <span style="width: 20px;height: 20px;" class="iconify" data-icon="ion:trash-outline"></span>
                                        <div class="name"><a href='{{asset('admin/hotel/del/'.$po->id_promo)}}'> Xoá</a></div>
                                    </div>
                                    <div class="triangle"></div>
                                </div>
                            </div>
                        </td>
                    </tr>
                        @endforeach
                    @else
                        <tr><td>No data !!!</td></tr>
                    @endif
                </table>
            </div>
            {{$promo->links('admin.paginate')}}
        </div>
    </div>
</div>
</body>

</html>

<style>
    .pager{
        text-align: right!important;
    }
</style>
