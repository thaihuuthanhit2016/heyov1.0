<!DOCTYPE html>
<html lang="en">
<head>
    <title>HF Admin</title>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="{{asset('public/admin/css/create.css')}}" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="https://code.iconify.design/2/2.2.1/iconify.min.js"></script>
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css"
          rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
    <link rel="stylesheet"
          href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
    <script
        src="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
    <script></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="{{asset('public/admin/js/main.js')}}"></script>
    <style>
        .toggle.ios,
        .toggle-on.ios,
        .toggle-off.ios {
            border-radius: 20rem;
        }
        .toggle.ios .toggle-handle {
            border-radius: 20rem;
        }
    </style>
</head>
<body>
<div class="menu_header">
    <div class="left">
        <a href="#" class="logo">HEYO TRIP</a>
    </div>
    <div class="right">
        <span class="name_menu"> Chương trình khuyến mãi </span>
        <div class="box_admin">
               <span class="iconify" style="width: 25px; height: 25px; color: #032044"
                     data-icon="ic:baseline-notifications-none"></span>
            <div class="badge badge-danger counter">9</div>
            <div id="btn_admin" class="wrapper_admin">
                <span style="width: 25px; height: 25px; color: #032044" class="iconify" data-icon="lucide:user"></span>
                <span class="admin">Admin</span>
                <span style="
                     width: 15px;
                     height: 15px;
                     color: #032044;
                     margin-left: 10px;
                     " class="iconify" data-icon="ant-design:caret-down-filled"></span>
            </div>
        </div>
    </div>
    <div id="profile" style="box-shadow: 10" class="box_profile">
            <span style="width: 30px; height: 30px; color: #032044" class="iconify"
                  data-icon="simple-line-icons:logout"></span>
        <span class="logout">Đăng xuất</span>
    </div>
</div>
<div style="display: flex">
    <div class="menu_left">
        <div class="top">
            <a href="#" style="background-color: #f2f9ff; text-decoration: none" class="item">
               <span class="iconify" style="color: #032044; width: 30px; height: 30px"
                     data-icon="icon-park-outline:hotel"></span>
                <span class="name_menu">Khách sạn</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
                <span class="iconify" style="color: #526a87; width: 30px; height: 30px" data-icon="ps:plane"></span>
                <span style="color: #526a87" class="name_menu">Vé máy bay</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
                <span class="iconify" style="color: #526a87; width: 30px; height: 30px" data-icon="clarity:car-line"></span>
                <span style="color: #526a87" class="name_menu">Thuê xe</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
               <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                     data-icon="clarity:data-cluster-line"></span>
                <span style="color: #526a87" class="name_menu">Crypto</span>
            </a>
        </div>
        <div class="top">
            <a href="#" style="text-decoration: none" class="item">
                <span class="iconify" style="color: #526a87; width: 30px; height: 30px" data-icon="ep:user-filled"></span>
                <span style="color: #526a87" class="name_menu">Tài khoản</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
               <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                     data-icon="dashicons:welcome-widgets-menus"></span>
                <span style="color: #526a87" class="name_menu">Danh mục</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
                <span class="iconify" style="color: #526a87; width: 30px; height: 30px" data-icon="gg:website"></span>
                <span style="color: #526a87" class="name_menu">Website</span>
            </a>
            <a href="#" style="text-decoration: none" class="item">
               <span class="iconify" style="color: #526a87; width: 30px; height: 30px"
                     data-icon="material-symbols:settings-suggest-outline-sharp"></span>
                <span style="color: #526a87" class="name_menu">Cấu hình</span>
            </a>
        </div>
    </div>
    <div class="menu_right">
        <div class="item">
               <span style="width: 25px; height: 25px; color: #032044" class="iconify"
                     data-icon="majesticons:analytics"></span>
            <span class="name_menu">Dashboard</span>
        </div>
        <div class="item">
            <span style="width: 25px; height: 25px; color: #032044" class="iconify" data-icon="mdi:bed"></span>
            <span class="name_menu">Tìm - Đặt phòng</span>
        </div>
        <div class="item">
               <span style="width: 25px; height: 25px; color: #032044" class="iconify"
                     data-icon="fa-solid:calendar-day"></span>
            <span class="name_menu">Quản lý đơn đặt phòng</span>
        </div>
        <div class="item">
               <span style="width: 25px; height: 25px; color: #032044" class="iconify"
                     data-icon="fa-solid:money-check-alt"></span>
            <span class="name_menu">Tuỳ chỉnh giá phòng</span>
        </div>
        <div class="item">
               <span style="width: 25px; height: 25px; color: #329223" class="iconify"
                     data-icon="icomoon-free:price-tags"></span>
            <span style="color: #329223; font-weight: 800" class="name_menu">Chương trình khuyến mãi</span>
        </div>
        <div class="item">
            <span style="width: 25px; height: 25px; color: #032044" class="iconify" data-icon="eva:percent-fill"></span>
            <span class="name_menu">Quản lý hoa hồng</span>
        </div>
        <div class="item">
               <span style="width: 25px; height: 25px; color: #032044" class="iconify"
                     data-icon="fa6-solid:hand-holding-dollar"></span>
            <span class="name_menu">Huỷ - Hoàn tiền</span>
        </div>
        <div class="item">
               <span style="width: 25px; height: 25px; color: #032044" class="iconify"
                     data-icon="fluent:news-20-filled"></span>
            <span class="name_menu">Bài viết Marketing</span>
        </div>
    </div>                <form method="post"enctype="multipart/form-data">@csrf

        <div class="box_content">
            <div class="wrapper">
                <div class="content">
                    <div class="box_title">
                        <div class="title">Thông tin khuyến mãi</div>
                    </div>
                    <div style="margin: 0px" class="dropdown-divider"></div>

                    <div class="content">
                        <div class="row_input">
                            <div class="box_input">
                                <div class="label">Phân hệ<span class="star">*</span></div>
                                <input type="text" readonly  name="subsystem" class="input" value="Hotel" placeholder="Hotel">

                            </div>
                            <div class="box_input">
                                <div class="label">
                                    Loại khuyến mãi<span class="star">*</span>
                                </div>
                                <select id="type_promo" name="type_promo">
                                    <option value="1" @if(Session::get('type_promo')==1) selected @endif >Promo Code</option>
                                    <option value="2" @if(Session::get('type_promo')==2) selected @endif >Unique Code</option>
                                    <option value="3" @if(Session::get('type_promo')==3) selected @endif >Partner Code</option>
                                    <option value="4" @if(Session::get('type_promo')==4) selected @endif > Special Campaign</option>
                                    <?php
                                    Session::put('type_promo','')
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="row_input">
                            <div class="box_input">
                                <div class="label">
                                    Tên chương trình khuyến mãi
                                    <span class="star">*</span>
                                </div>
                                <input class="input" type="text" value="{{Session::get('title')}}" class="form-control" required onKeyUp="limitText(this.form.title,this.form.countdown,100);"onKeyDown="limitText(this.form.title,this.form.countdown,100);"  name="title" id="title"placeholder="Nhập" />
                                <div class="help_text_input">0/100</div>
                            </div>
                            @if(Session::get('titleE'))                                        <br>
                            <span class="alert alert-danger">{{Session::get('titleE')}}</span>
                            <?php Session::put('titleE','')?>
                            <?php
                            Session::put('title','')
                            ?>
                            @endif
                        </div>
                        <div style="margin-top: 20px" class="row_input">
                            <div class="box_input">
                                <div class="label">
                                    Nội dung khuyến mãi
                                    <span class="star">*</span>
                                </div>
                                <textarea class="input" rows="5" placeholder="Nhập"required name="content">{{Session::get('content')}}</textarea>
                            </div>
                            <?php
                            Session::put('content','')
                            ?>
                        </div>
                        <div class="row_input">
                            <div class="box_input">
                                <div class="label">
                                    Mã khuyến mãi
                                    <span class="star">*</span>
                                </div>
                                <input class="input" type="text" name="code_promotion" value="{{Session::get('code_promotion')}}" required id="code_pro" />
                                @if(Session::get('code_promotionE'))                                        <br>
                                <span class="alert alert-danger">{{Session::get('code_promotionE')}}</span>
                                <?php Session::put('code_promotionE','')?>
                                <?php
                                Session::put('code_promotion','')
                                ?>
                                @endif
                            </div>
                            <div class="box_input">
                                <div class="label">
                                    Mã chương trình
                                    <span class="star">*</span>
                                </div>
                                <input class="input" type="text"readonly placeholder="Nhập"name="program_code_promo" id="code_program" value="{{date('dmY')}} H-MAKHUYENMAI">
                            </div>
                            <div class="box_input">
                                <div class="label">Trạng thái<span class="star">*</span></div>
                                <select  name="status">
                                    <option @if(Session::get('status')==0) selected @endif value="0">Inactive</option>
                                    <option @if(Session::get('status')==1) selected @endif value="1">Active</option>
                                    <option @if(Session::get('status')==2) selected @endif value="2">Deactive</option>
                                    <option @if(Session::get('status')==3) selected @endif value="3">Expired</option>
                                    <?php
                                    Session::put('status','')
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="row_input">
                            <div class="box_input">
                                <div class="label">Hình ảnh Cover</div>
                                <div class="help_text_choose_file">
                                    Dimension (1242x464) Max 500KB
                                </div>
                                <button  type="button" id="btn_choose_file_cover" class="btn button_choose_file">
                           <span style="width: 20px; height: 20px; color: #fff" class="iconify"
                                 data-icon="fluent:add-16-filled"></span>
                                    <span class="name_button_choose_file">Add files</span>
                                </button>
                                <input style="display: none" id="coverFile"  name="images_cover" title=" " value="" type="file" />
                            </div>
                        </div>
                        <div class="row_input">
                            <div class="box_input">
                                <div class="label">Hình ảnh Logo</div>
                                <div class="help_text_choose_file">
                                    Dimension (260x260) Max 500KB
                                </div>
                                <button type="button" id="btn_choose_file_logo" class="btn button_choose_file">
                           <span style="width: 20px; height: 20px; color: #fff" class="iconify"
                                 data-icon="fluent:add-16-filled"></span>
                                    <span class="name_button_choose_file">Add files</span>
                                </button>
                                <input style="display: none" id="logoFile" name="images_logo"  title=" " value="" type="file" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wrapper">
                <div class="box_title">
                    <div class="title">Thiết lập khuyến mãi</div>
                </div>
                <div style="margin: 0px" class="dropdown-divider"></div>
                <div class="content">
                    <div class="row_input">
                        <div class="box_input">
                            <div class="label">
                                Kiểu hiển thị trên ứng dụng<span class="star">*</span>
                            </div>
                            <div style="display: flex; margin-top: 10px">
                                <div style="display: flex; align-items: center" class="form-check">
                                    <input style="width: 20px; height: 20px" class="form-check-input" type="radio" name="display" value="0"
                                           id="display"checked />
                                    <label style="
                                 font-family: 'Montserrat';
                                 font-style: normal;
                                 font-weight: 400;
                                 font-size: 15px;
                                 color: #666666;
                                 margin-left: 10px;
                                 margin-top: 5px;
                                 " class="form-check-label" for="flexRadioDefault1">
                                        Hiển thị trong danh sách
                                    </label>
                                </div>
                                <div style="
                              display: flex;
                              align-items: center;
                              margin-left: 30px;
                              " class="form-check">
                                    <input style="width: 20px; height: 20px" class="form-check-input" type="radio"value="1" name="display"
                                           id="display"  />
                                    <label style="
                                 font-family: 'Montserrat';
                                 font-style: normal;
                                 font-weight: 400;
                                 font-size: 15px;
                                 color: #666666;
                                 margin-left: 10px;
                                 margin-top: 5px;
                                 " class="form-check-label" for="flexRadioDefault2">
                                        Nhập mã để tìm
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row_input">
                        <div class="box_input">
                            <div class="label">Đường dẫn hiển thị</div>
                            <div class="pathname">
                                <div class="left">https://heyotrip.com/</div>
                                <input class="right" type="text" name="link" id="link" value="{{Session::get('link')}}"/>
                            </div>
                        </div>
                    </div>
                    <div class="row_input">
                        <div class="box_input">
                            <div class="label">Ngày bắt đầu</div>
                            <div class="wrapper_date">
                                <div style="width: 100%; margin-top: 5px" class="input-group date" id="datetimepicker1">
                                    <input type="text" name="date_start" name="date_start" required class="form-control"value="{{Session::get('date_start')}}" required="" class="form-control" value="" />
                                    <span class="input-group-addon">
                              <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                                </div>
                            </div>
                            @if(Session::get('Errordate_start'))                                        <br>
                            <span class="alert alert-danger">{{Session::get('Errordate_start')}}</span>
                            <?php Session::put('Errordate_start','');
                            Session::put('date_start','')?>
                            @endif
                        </div>
                        <div class="box_input">
                            <div class="label">Ngày kết thúc</div>
                            <div class="wrapper_date">
                                <div style="width: 100%; margin-top: 5px" class="input-group date" id="datetimepicker2">
                                    <input type="text" name="date_end" required class="form-control"value="{{Session::get('date_end')}}" class="form-control" value="" />
                                    <span class="input-group-addon">
                              <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                                </div>
                            </div>

                            @if(Session::get('Errordate_end'))
                                <br>
                                <span class="alert alert-danger">{{Session::get('Errordate_end')}}</span>
                                <?php Session::put('Errordate_end','');
                                Session::put('date_end','')?>
                            @endif
                        </div>
                    </div>
                    <div class="row_input">
                        <div class="box_input">
                            <div class="label">
                                Số lượng phát hành<span class="star">*</span>
                            </div>
                            <input type="text" placeholder="Nhập" class="input" name="number_max" id="uang" required  value="{{Session::get('number_max')}}" />
                        </div>
                        <div class="box_input">
                            <div class="label">
                                Giới hạn sử dụng trong ngày<span class="star">*</span>
                            </div>
                            <input type="text" placeholder="Nhập" class="input" name="number_limit_day" id="uang2"  required value="{{Session::get('number_limit_day')}}" />
                        </div>
                    </div>
                    <div class="row_input">
                        <div class="box_input">
                            <div class="label">
                                Loại giảm giá<span class="star">*</span>
                            </div>
                            <div class="box_discount">
                                <div style="display: flex">
                                    <div style="display: flex; align-items: center" class="form-check">
                                        <input style="width: 20px; height: 20px" class="form-check-input" type="radio" name="id_type_discount" value="1" name="radioDiscount"
                                               id="radioDiscount1" checked />
                                        <label style="
                                    font-family: 'Montserrat';
                                    font-style: normal;
                                    font-weight: 400;
                                    font-size: 15px;
                                    color: #666666;
                                    margin-left: 10px;
                                    margin-top: 5px;
                                    " class="form-check-label" for="radioDiscount1">
                                            Giảm giá trị cố định
                                        </label>
                                    </div>
                                    <div style="
                                 display: flex;
                                 align-items: center;
                                 margin-left: 30px;
                                 " class="form-check">
                                        <input style="width: 20px; height: 20px" class="form-check-input" name="id_type_discount" value="2"type="radio" name="radioDiscount"
                                               id="radioDiscount2" />
                                        <label style="
                                    font-family: 'Montserrat';
                                    font-style: normal;
                                    font-weight: 400;
                                    font-size: 15px;
                                    color: #666666;
                                    margin-left: 10px;
                                    margin-top: 5px;
                                    " class="form-check-label" for="radioDiscount2">
                                            Giảm giá theo phần trăm (%)
                                        </label>
                                    </div>
                                </div>
                                <div id="box_discount1">
                                    <div class="label">Mức tiền giảm</div>
                                    <div style="display: flex; margin-top: 5px">
                                        <input class="input" placeholder="Nhập" name="reduced_amount"id="uang4" value="{{Session::get('reduced_amount')}}" />
                                        <div style="
                                    background: #e9ecef;
                                    border: 1px solid #dee2e6;
                                    border-radius: 5px;
                                    margin-top: 5px;
                                    font-family: 'Montserrat';
                                    font-style: normal;
                                    font-weight: 400;
                                    font-size: 13px;
                                    line-height: 20px;
                                    color: #666666;
                                    padding: 5px 10px;
                                    display: flex;
                                    align-items: center;
                                    justify-content: center;
                                    ">
                                            VND
                                        </div>
                                    </div>
                                </div>
                                <div id="box_discount2">
                                    <div style="padding: 0px 10px 0px 0px" class="box_input">
                                        <div class="label">Mức giảm theo %</div>
                                        <input class="input" placeholder="Nhập" name="percentage_reduction" value="{{Session::get('percentage_reduction')}}"/>
                                    </div>
                                    <div style="padding: 0px 10px 0px 0px" class="box_input">
                                        <div class="label">Mức tiền giảm tối đa</div>
                                        <input class="input" placeholder="Nhập"  name="reduced_amount1"  id="uang5" value="{{Session::get('reduced_amount1')}}" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row_input">
                        <div class="box_input">
                            <div class="label">Nhà cung cấp dịch vụ</div>
                            <div class="box_help_text_supplier">
                           <span style="
                              font-family: 'Montserrat';
                              font-style: normal;
                              font-weight: 400;
                              font-size: 14px;
                              line-height: 20px;
                              color: #666666;
                              ">Các đối tác cung cấp dịch vụ trên hệ thống cho
                           Heyotrip</span>
                                <span style="
                              font-family: 'Montserrat';
                              font-style: normal;
                              font-weight: 500;
                              font-size: 14px;
                              line-height: 20px;
                              text-align: right;
                              color: #007bff;
                              cursor: pointer;
                              " id="del_service">
                           Xoá hết
                           </span>
                            </div>
                            <!-- <div class="box_discount_chosen"> -->
                            <!-- <div class="item">
                               Travelport<span
                                 style="
                                   cursor: pointer;
                                   margin-left: 5px;
                                   width: 15px;
                                   height: 15px;
                                 "
                                 class="iconify"
                                 data-icon="eva:close-outline"
                               ></span>
                               </div> -->
                            <select class='js-select2-multi'  required name="services[]" id="service" multiple='multiple'>
                                <option>TVP</option>
                                <option>EZC</option>
                                <option>HPL</option>
                                <option>VJ</option>
                            </select>
                        <?php
                        Session::put('services','')
                        ?>
                        <!-- </div> -->
                        </div>
                    </div>
                    <div class="row_input" id="payment">
                        <div class="box_input">
                            <div class="label">Đối tác thanh toán</div>
                            <div class="box_help_text_supplier">
                           <span style="
                              font-family: 'Montserrat';
                              font-style: normal;
                              font-weight: 400;
                              font-size: 14px;
                              line-height: 20px;
                              color: #666666;
                              ">Các đối tác thanh toán trên hệ thống cho
                           Heyotrip</span>
                                <span style="
                              font-family: 'Montserrat';
                              font-style: normal;
                              font-weight: 500;
                              font-size: 14px;
                              line-height: 20px;
                              text-align: right;
                              color: #007bff;
                              cursor: pointer;
                              " id="del_payment_partner">
                           Xoá hết
                           </span>
                            </div>
                            <!-- <div class="box_discount_chosen"> -->
                            <!-- <div class="item">
                               Travelport<span
                                 style="
                                   cursor: pointer;
                                   margin-left: 5px;
                                   width: 15px;
                                   height: 15px;
                                 "
                                 class="iconify"
                                 data-icon="eva:close-outline"
                               ></span>
                               </div> -->
                            <select class='js-select2-multi'  required name="payment_partner[]" id="payment_partner" multiple='multiple'>
                                <?php
                                $partner_payment=DB::table('tbl_partner_payment')->get();
                                ?>
                                @foreach($partner_payment as $p)
                                    <option>{{$p->name}}</option>
                                @endforeach
                            </select>
                        <?php
                        Session::put('services','')
                        ?>
                        <!-- </div> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="wrapper">
                <div class="box_title">
                    <div class="title">Điều kiện đơn hàng áp dụng</div>
                </div>
                <div style="margin: 0px" class="dropdown-divider"></div>
                <div class="content">
                    <div class="row_input">
                        <div class="box_input">
                            <div class="label">
                                Quốc gia du lịch<span class="star">*</span>
                            </div>
                            <select  name="id_nation"  id="nation">
                                @foreach($locations as $l)
                                    <option @if(Session::get('id_nation')==$l->id)  selected @endif value="{{$l->id}}">{{$l->country_name_vn}} - {{$l->country_name_en}}</option>
                                @endforeach
                                <?php
                                Session::put('id_nation','')
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row_input">
                        <div class="box_input">
                            <div style="
                           display: flex;
                           align-items: center;
                           justify-content: space-between;
                           ">
                                <div class="label">
                                    Thành phố<span class="star">*</span><span style="
                                 font-family: 'Montserrat';
                                 font-style: normal;
                                 font-weight: 400;
                                 font-size: 13px;
                                 line-height: 20px;
                                 color: #666666;
                                 margin-left: 10px;
                                 ">(Đã chọn 1)</span>
                                </div>
                                <span style="
                              font-family: 'Montserrat';
                              font-style: normal;
                              font-weight: 500;
                              font-size: 14px;
                              line-height: 20px;
                              text-align: right;
                              color: #007bff;
                              cursor: pointer;
                              ">
                           Xoá hết
                           </span>
                            </div>
                            <select class='js-select2-multi' required name="id_city[]"  id="result_city"multiple='multiple'>
                            </select>
                        </div>
                    </div>
                    <div class="row_input">
                        <div class="box_input">
                            <div class="label">Ngày bắt đầu</div>
                            <div class="wrapper_date">
                                <div style="width: 100%; margin-top: 5px" class="input-group date" id="datetimepicker3">
                                    <input type="text"  name="checkinFrom" required id="checkinFrom" class="form-control"value="{{Session::get('checkinFrom')}}" value="" />
                                    <span class="input-group-addon">
                              <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                                </div>
                                @if(Session::get('checkinFromE'))
                                    <br>
                                    <span class="alert alert-danger">{{Session::get('checkinFromE')}}</span>
                                    <?php Session::put('checkinFromE','')?>
                                    <?php
                                    Session::put('checkinFrom','')
                                    ?>
                                @endif
                            </div>
                        </div>
                        <div class="box_input">
                            <div class="label">Ngày kết thúc</div>
                            <div class="wrapper_date">
                                <div style="width: 100%; margin-top: 5px" class="input-group date" id="datetimepicker4">
                                    <input type="text" name="checkinTo" required id="checkinTo" class="form-control"value="{{Session::get('checkinTo')}}" />
                                    <span class="input-group-addon">
                              <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                                </div>
                                @if(Session::get('ErrorcheckinTo'))
                                    <br>
                                    <span class="alert alert-danger">{{Session::get('ErrorcheckinTo')}}</span>
                                    <?php Session::put('ErrorcheckinTo','')?>
                                    <?php
                                    Session::put('checkinTo','')
                                    ?>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row_input">
                        <div class="box_input">
                            <div style="
                           display: flex;
                           align-items: center;
                           justify-content: space-between;
                           ">
                                <div class="label">
                                    Giá trị đơn hàng tối thiểu phải đạt<span class="star">*</span>
                                </div>
                                <div style="display: flex; align-items: center" class="form-check">
                                    <input style="width: 15px; height: 15px" class="form-check-input" type="checkbox" name="check_box_nolimit"   id="check_box_nolimit"
                                           id="radioPrice" />
                                    <label style="
                                 font-family: 'Montserrat';
                                 font-style: normal;
                                 font-weight: 400;
                                 font-size: 15px;
                                 color: #666666;
                                 margin-left: 10px;
                                 margin-top: 5px;
                                 " class="form-check-label" for="radioPrice">
                                        Không giới hạn
                                    </label>
                                </div>
                            </div>
                            <div style="display: flex; margin-top: 5px">
                                <input class="input" placeholder="Nhập"name="value_order_min" value="{{Session::get('value_order_min')}}" id="min_value" />
                                <div style="
                              background: #e9ecef;
                              border: 1px solid #dee2e6;
                              border-radius: 5px;
                              margin-top: 5px;
                              font-family: 'Montserrat';
                              font-style: normal;
                              font-weight: 400;
                              font-size: 13px;
                              line-height: 20px;
                              color: #666666;
                              padding: 5px 10px;
                              display: flex;
                              align-items: center;
                              justify-content: center;
                              ">
                                    VND
                                </div>
                            </div>
                        </div>
                        <div style="margin-top: 5px" class="box_input">
                            <div class="label">Số lượng khách hàng tối thiểu</div>
                            <input style="margin-top: 12px" placeholder="Nhập" class="input" name="number_user_min"id="number_user_min" required value="{{Session::get('number_user_min')}}" />
                        </div>
                    </div>
                    <div class="row_input">
                        <div class="box_input">
                            <div style="
                           display: flex;
                           align-items: center;
                           justify-content: space-between;
                           ">
                                <div class="label">
                                    Khách sạn áp dụng<span style="
                                 font-family: 'Montserrat';
                                 font-style: normal;
                                 font-weight: 400;
                                 font-size: 13px;
                                 line-height: 20px;
                                 color: #666666;
                                 margin-left: 10px;
                                 ">(Đã chọn 1)</span>
                                </div>
                                <span style="
                              font-family: 'Montserrat';
                              font-style: normal;
                              font-weight: 500;
                              font-size: 14px;
                              line-height: 20px;
                              text-align: right;
                              color: #007bff;
                              cursor: pointer;
                              ">
                           Xoá hết
                           </span>
                            </div>
                            <select class='js-select2-multi' required name="hotel_apply[]" id="hotel_apply" multiple='multiple'>
                                <?php
                                $hotel=DB::table('hotels')->get();
                                ?>
                                @foreach($hotel as $h)
                                    <option>{{$h->hotel_name}}</option>
                                @endforeach
                            </select>
                            <?php
                            Session::put('hotel_apply','')
                            ?>
                        </div>
                    </div>
                </div>
                <div class="box_title">
                    <div class="title"></div>
                    <div class="box_option">
                        <button class="btn button_save">Lưu</button>
                        <button class="btn button_watch" data-toggle="modal" data-target="#modal-review">Xem trước</button>
                    </div>
                </div>

            </div>
        </div>
    </form>
</div>
<div style="padding: 0px; margin: 0px;" class="modal fade" id="modal-review" role="dialog">
    <div style="margin-left: calc(50% - 455px);" class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div style="padding: 0px;" class="modal-header">
                <button type="button" class="close1" data-dismiss="modal">
                    <span style="width: 30px; height:30px; color: #fff" class="iconify" data-icon="ep:close-bold"></span>
                </button>
                <img src="https://res.edu.vn/wp-content/uploads/2022/01/unit-60-nature-environment.jpg" />
            </div>
            <div style="padding: 0px;" class="modal-body">
                <div class="title1">
                    Lorem ipsum dolor sit amet
                </div>
                <div class="title2">
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut
                    laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam,
                </div>
                <div style="display: flex; align-items: center; justify-content: center; flex-direction: column;">
                    <div class="box_id_discount">
                        <div class="title1">
                            Lorem ipsum dolor sit amet
                        </div>
                        <div class="box_id">
                            <div class="title">CODE1234</div>
                            <button style="background: transparent; border: 0px">
                           <span style="width: 20px; height: 20px; color: #0770cd" class="iconify"
                                 data-icon="ic:outline-content-copy"></span>
                            </button>
                        </div>
                        <div class="title2">
                            Lorem ipsum dolor sit amet, consectetuer adipiscing
                        </div>
                    </div>
                    <div class="box_id_discount">
                        <div class="title1">
                            Lorem ipsum dolor sit amet
                        </div>
                        <div class="box_id">
                            <div class="title">CODE1234</div>
                            <button style="background: transparent; border: 0px">
                           <span style="width: 20px; height: 20px; color: #0770cd" class="iconify"
                                 data-icon="ic:outline-content-copy"></span>
                            </button>
                        </div>
                        <div class="title2">
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt u.
                        </div>
                    </div>
                </div>
                <div class="box_expiry_date">
                    <div class="expiry_date">
                        Expiry date
                    </div>
                    <div class="date">
                        11/02/20222 - 31/03/2022
                    </div>
                </div>
                <div class="box_term_and_condition">
                    <div class="title">Terms and Condition</div>
                    <div class="accordion" id="accordion_term_and_condition">
                        <div class="card">
                            <div class="card-header" id="heading_term">
                                <button class="btn" type="button" data-toggle="collapse" data-target="#collapse_term"
                                        aria-expanded="true" aria-controls="collapse_term">
                                    Terms
                                </button>
                            </div>
                            <div id="collapse_term" class="collapse" aria-labelledby="heading_term"
                                 data-parent="#accordion_term_and_condition">
                                <div class="card-body">
                                    <div class="box_info">
                                        <span class="iconify" data-icon="bi:dot"></span>
                                        <div class="info">Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy
                                            nibh.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingTwo">
                                <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapse_condition"
                                        aria-expanded="false" aria-controls="collapse_condition">
                                    Condition
                                </button>
                            </div>
                            <div id="collapse_condition" class="collapse" aria-labelledby="collapse_condition"
                                 data-parent="#accordion_term_and_condition">
                                <div class="card-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
                                    3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt
                                    laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin
                                    coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes
                                    anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings
                                    occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard
                                    of them accusamus labore sustainable VHS.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

<script>
    $('.js-example-basic-single').select2();
</script>

<script>
    $(document).ready(function () {
        $("#payment").addClass('none');

        $("#check_box_nolimit").change(function () {

            var checked = $(this).is(":checked");

            if (checked) {
                $("#min_value").prop('disabled',true);
            }else{
                $("#min_value").prop('disabled',false);
            }
        })
        $('#flexRadioDefault2_2').on('change', function() {
            $("#giamgiatheophantram").removeClass('none');
            $("#giamgiacodinh").addClass('none');
        });
        $('#flexRadioDefault1_2').on('change', function() {
            $("#giamgiatheophantram").addClass('none');
            $("#giamgiacodinh").removeClass('none');
        });

        $("#title").keyup(function () {
            var title =$(this).val();
            console.log(title);

            $.ajax({
                url:'http://localhost/testlocal/admin/hotel/customlink',
                method:"get",
                data:{title:title},
                success:function (data){
                    $("#link").val(data);

                }
            });
        });
        $('#nation').on('change', function() {

            var nation =$(this).val();
            $.ajax({
                url:'http://localhost/testlocal/admin/hotel/loadcity',
                method:"get",
                data:{nation:nation},
                success:function (data){
                    $("#result_city").html(data);

                }
            });
        });
        $("#delete_search").click(function () {
            $('.form-control').val('');
        });
        $("#xoahotel_apply").click(function () {

            $('#hotel_apply').val('');
        });


        $("#code_pro").keyup(function () {
            var today = new Date();
            var  cod_pro=$("#code_pro").val().toUpperCase();
            var date = today.getDate()+''+(today.getMonth()+1)+""+today.getFullYear();
            var data=date+" H-"+cod_pro;
            $("#code_program").val(data);
        });

        $('#type_promo').change(function () {
            var type_promo=$('#type_promo').val();

            if(type_promo==2){
                console.log(1);
                $( "#flexRadioDefault1" ).prop('checked', false);
                $( "#flexRadioDefault2" ).prop('checked', true);

            }
            if(type_promo==1){
                $("#uang2").prop('disabled',false);
                $("#partner").addClass('none');



            } if(type_promo==4){
                $("#checkinTo").attr('disabled',true);
                $("#checkinFrom").attr('disabled',true);
                $("#min_value").attr('disabled',true);
                $("#number_user_min").attr('disabled',true);



            }if(type_promo!=4){
                $("#checkinTo").attr('disabled',false);
                $("#checkinFrom").attr('disabled',false);
                $("#min_value").attr('disabled',false);
                $("#number_user_min").attr('disabled',false);



            }
            if(type_promo==2){
                $("#flexRadioDefault1").prop('disabled',true);

                $("#class_service").addClass('none');

            }

            if(type_promo==2||type_promo==3||type_promo==4){
                $("#uang2").prop('disabled',true);
                $("#class_service").removeClass('none');

            }
            if(type_promo==3){
                $( "#partner" ).removeClass('none');

            }
            if(type_promo==4||type_promo==2){
                $( "#payment" ).addClass('none');


            }
            if(type_promo==3){
                $("#payment").removeClass('none');
            }
            if(type_promo==4){

                $( "#flexRadioDefault2_2" ).prop('checked', true);
                $( "#flexRadioDefault1_2" ).prop('checked', false);
                $( "#giamgiatheophantram" ).removeClass('none');
                $( "#giamgiacodinh" ).addClass('none');

            }
        })

        $(function(){

            $("#uang").keyup(function(e){
                $(this).val(format($(this).val()));
            });
            $("#uang2").keyup(function(e){
                $(this).val(format($(this).val()));
            });
            $("#uang3").keyup(function(e){
                $(this).val(format($(this).val()));
            });
            $("#uang4").keyup(function(e){
                $(this).val(format($(this).val()));
            });
            $("#uang5").keyup(function(e){
                $(this).val(format($(this).val()));
            });
            $("#min_value").keyup(function(e){
                $(this).val(format($(this).val()));
            });
        });
        var format = function(num){
            var str = num.toString().replace("", ""), parts = false, output = [], i = 1, formatted = null;
            if(str.indexOf(".") > 0) {
                parts = str.split(".");
                str = parts[0];
            }
            str = str.split("").reverse();
            for(var j = 0, len = str.length; j < len; j++) {
                if(str[j] != ",") {
                    output.push(str[j]);
                    if(i%3 == 0 && j < (len - 1)) {
                        output.push(",");
                    }
                    i++;
                }
            }
            formatted = output.reverse().join("");
            return("" + formatted + ((parts) ? "." + parts[1].substr(0, 2) : ""));
        };

    })
</script>
<script>
    $(document).ready(function () {
        $('#class_service input').keyup(function () {
            var  a=$('#class_service input').val();
            var tach=count(a.split(" "));

            $("#numberservice").text(a);
        })
    })
</script>

<script>
    $('#del_payment_partner').click(function () {
        $("#payment_partner").val(null).trigger("change");

    });
    $('#del_hotel').click(function () {
        $("#hotel_apply").val(null).trigger("change");

    });
    $('#del_service').click(function () {
        $("#service").val(null).trigger("change");

    });
    $('#datetimepicker1').datetimepicker({
        format: 'DD-MM-YYYY LT',minDate:new Date()
    });
    $('#datetimepicker2').datetimepicker({
        format: 'DD-MM-YYYY LT',minDate:new Date()
    });
    $('#datetimepicker3').datetimepicker({
        format: 'DD-MM-YYYY LT',minDate:new Date()

    });
    $('#datetimepicker4').datetimepicker({
        format: 'DD-MM-YYYY LT',
        minDate:new Date()

    });

    const query = document.querySelector.bind(document);

    const removeComma = string => string.slice(0, string.length - 1).trim();



    const isInvalid = stringInput => {
        const inputs = Array.from(query('.tags').children).map(input => input.firstElementChild.textContent);

        return !/^[A-Za-z0-9]{3,}/.test(stringInput) ||
            inputs.some(name => name === removeComma(stringInput)) ||
            query('.tags').children.length >= 10;
    };



    function modifyTags(e) {
        if (e.key === ',') {

            if (isInvalid(e.target.value)) {
                e.target.value = '';
                return;
            }

            addTag(e.target.value);
            e.target.value = '';

        }

        if (e.key === 'Backspace' && !e.target.value.length) {
            deleteTag(null, query('.tags').children.length - 1);
        }

        query('.tags-count').textContent = `${1 + query('.tags').children.length}`;
    }



    function addTag(textValue) {
        const tag = document.createElement('div'),
            tagName = document.createElement('label'),
            remove = document.createElement('span');

        tagName.setAttribute('class', 'tag-name');
        tagName.textContent = removeComma(textValue);

        remove.setAttribute('class', 'remove');
        remove.textContent = 'X';
        remove.addEventListener('click', deleteTag);

        tag.setAttribute('class', 'tag');
        tag.appendChild(tagName);
        tag.appendChild(remove);

        query('.tags').appendChild(tag);
    }



    function deleteTag(e, i = Array.from(query('.tags').children).indexOf(e.target.parentElement)) {
        const index = query('.tags').getElementsByClassName('tag')[i];

        query('.tags').removeChild(index);
        query('.tags-count').textContent = `${10 - query('.tags').children.length}`;
    }



    function focus() {
        query('#tag').focus();
    }



</script>
<script type="text/javascript">
    function limitText(limitField, limitCount, limitNum) {
        if (limitField.value.length > limitNum) {
            limitField.value = limitField.value.substring(0, limitNum);
        } else {
            limitCount.value = limitNum - limitField.value.length;
        }
    }

</script>
<style>
    .group-info-pro,.group-setting-pro,.group-con-pro{
        border: 1px solid;
        padding: 10px;
        border-radius: 10px;
    }
    .none{
        display: none!important;}
</style>
