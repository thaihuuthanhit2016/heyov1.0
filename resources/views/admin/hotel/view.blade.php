@extends('admin.master');
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    Chi tiết Thông tin khuyến mãi
                </header>
                <br>
                @include('admin.errors.error')

                <div class="panel-body">
                    <div class="position-center">
                    <div class="info-promo">
                       <a href="{{asset('admin/hotel/list')}}" style="float:right" class="btn btn-primary" ui-toggle-class=""> Đóng</a><br><br>

                        <a href="{{asset('admin/hotel/edit/'.$promo->id_promo)}}" style="float:right" class="active" ui-toggle-class=""><i class="fa fa-pencil-square-o text-success text-active"></i> Chỉnh sửa</a>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Thông tin khuyến mãi:</label>

                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span for="exampleInputEmail1">Người tạo:</span>
                                    </div>
                                    <div class="col-md-3">
                                        <span for="exampleInputEmail1"><b>@if($promo->ud_user!=0) Nguyễn văn A @else ADMIN @endif</b></span>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span for="exampleInputEmail1">Phân hệ:</span>
                                    </div>
                                    <div class="col-md-3">
                                        <span for="exampleInputEmail1"><b>@if($promo->subsystem==0) Hotel @else Flight @endif</b></span>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">

                                        <span for="exampleInputEmail1">Loại khuyến mãi:</span>
                                    </div>
                                    <div class="col-md-3">
                                        <span for="exampleInputEmail1"> <b>@if($promo->type_promo==1)Promo Code @elseif($promo->type_promo==2) Unique Code @elseif($promo->type_promo==3) Partner Code@else Special Campaign @endif</b></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">

                                <div class="row">
                                    <div class="col-md-3">

                                        <span for="exampleInputEmail1">Mã Khuyến mãi:</span>
                                    </div>
                                    <div class="col-md-3">
                                        <span for="exampleInputEmail1">  <b>{{$promo->code_promotion}}</b></span>

                                    </div>

                                </div>
                            </div>
                            <div class="form-group">

                                <div class="row">
                                    <div class="col-md-3">

                                        <span for="exampleInputEmail1">Mã chương trình:</span>
                                    </div>

                                    <div class="col-md-3">
                                        <span for="exampleInputEmail1"><b>{{$promo->program_code_promo}}</b></span>
                                        </div>
                                </div>

                            </div>
                            <div class="form-group">


                                <div class="row">
                                    <div class="col-md-3">

                                        <span for="exampleInputEmail1">Trạng thái:</span>
                                    </div>

                                    <div class="col-md-3">

                                        <span for="exampleInputEmail1"> <b style="@if($promo->status==0) color:orange; @elseif($promo->status==1) color:green; @elseif($promo->status==2) color:red; @else color:grey;@endif">@if($promo->status==0)Inactive @elseif($promo->status==1) Active @elseif($promo->status==2) Deactive @else Expired @endif</b> </span>
                                    </div>
                                </div>


                            </div>
                            <div class="form-group">


                                <div class="row">
                                    <div class="col-md-3">

                                        <span for="exampleInputEmail1">Tên chương trình khuyến mãi:</span>
                                    </div>

                                    <div class="col-md-3">

                                        <span for="exampleInputEmail1"> <b>{{$promo->title}}</b> </span>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group">


                                <div class="row">
                                    <div class="col-md-3">

                                        <span for="exampleInputEmail1">Nội dung khuyến mãi:</span>
                                    </div>

                                    <div class="col-md-3">

                                        <span for="exampleInputEmail1"> <b>{{$promo->content}}</b> </span>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group">


                                <div class="row">
                                    <div class="col-md-3">

                                        <span for="exampleInputEmail1">Hình ảnh cover</span>
                                    </div>

                                    <div class="col-md-3">

                                        <span for="exampleInputEmail1"> <img src="{{asset("upload/".$promo->images_cover)}}" width="250px" ></span>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group">


                                <div class="row">
                                    <div class="col-md-3">

                                        <span for="exampleInputEmail1">Hình ảnh logo</span>
                                    </div>

                                    <div class="col-md-3">

                                        <span for="exampleInputEmail1"> <img src="{{asset("upload/".$promo->images_logo)}}" width="100px" ></span>
                                    </div>
                                </div>

                            </div>
                        </div><br>
                        <div class="info-promo">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Thiết lập khuyến mãi:</label>

                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span for="exampleInputEmail1">Kiểu hiển thị trên ứng dung:</span>
                                    </div>
                                    <div class="col-md-3">
                                        <span for="exampleInputEmail1"><b>@if($promo->display==0)                Hiển thị trong danh sách
                                                @else                                                     Nhập mã để tìm
                                                @endif</b></span>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">

                                        <span for="exampleInputEmail1">Đường dẫn hiển thị:</span>
                                    </div>
                                    <div class="col-md-3">
                                        <span for="exampleInputEmail1"> <b>{{$promo->link}}</b></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">

                                <div class="row">
                                    <div class="col-md-3">

                                        <span for="exampleInputEmail1">Ngày bắt đầu:</span>
                                    </div>
                                    <div class="col-md-3">
                                        <span for="exampleInputEmail1">  <b>{{$promo->date_start}}</b></span>

                                    </div>

                                </div>
                            </div>
                            <div class="form-group">

                                <div class="row">
                                    <div class="col-md-3">

                                        <span for="exampleInputEmail1">Ngày kết thúc:</span>
                                    </div>

                                    <div class="col-md-3">
                                        <span for="exampleInputEmail1"><b>{{$promo->date_end}}</b></span>
                                        </div>
                                </div>

                            </div>
                            <div class="form-group">


                                <div class="row">
                                    <div class="col-md-3">

                                        <span for="exampleInputEmail1">Số lượng phát hành:</span>
                                    </div>

                                    <div class="col-md-3">

                                        <span for="exampleInputEmail1"> <b>{{$promo->number_max}}</b> </span>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group">


                                <div class="row">
                                    <div class="col-md-3">

                                        <span for="exampleInputEmail1">Giới hạn sử dụng trong ngày:</span>
                                    </div>

                                    <div class="col-md-3">

                                        <span for="exampleInputEmail1"> <b>{{$promo->number_limit_day}}</b> </span>
                                    </div>
                                </div>

                            </div>

                            <div class="form-group">


                                <div class="row">
                                    <div class="col-md-3">

                                        <span for="exampleInputEmail1">Mức giảm theo %:</span>
                                    </div>
                                    <?php
                                    $type_discount=DB::table('tbl_type_discount')->where('id_type_discount',$promo->id_type_discount)->first();
                                    ?>
                                    <div class="col-md-3">

                                        <span for="exampleInputEmail1"> <b>@if($type_discount->percentage_reduction==null) None @else {{$type_discount->percentage_reduction}} @endif</b> </span>
                                    </div>
                                </div>

                            </div>

                            <div class="form-group">


                                <div class="row">
                                    <div class="col-md-3">

                                        <span for="exampleInputEmail1">Mức tiền giảm tối đa:</span>
                                    </div>
                                    <div class="col-md-3">

                                        <span for="exampleInputEmail1"> <b>{{$type_discount->reduced_amount}}</b> </span>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group">


                                <div class="row">
                                    <div class="col-md-3">

                                        <span for="exampleInputEmail1">Nhà cung cấp dịch vụ:</span>
                                    </div>
                                    <div class="col-md-3">

                                        <span for="exampleInputEmail1"> <b>@if($promo->services==null) Tất cả nhà cung cấp trên hệ thống Heyotrip @else {{$promo->services}} @endif</b> </span>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="info-promo">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Điều kiện đơn hàng áp dụng:</label>

                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">

                                        <span for="exampleInputEmail1">Quốc gia du lịch:</span>
                                    </div>

                                    <?php
                                    $nation=DB::table('countries')->where('id',$promo->id_nation)->first();
                                    ?>
                                    <div class="col-md-3">
                                        <span for="exampleInputEmail1"> <b>{{$nation->country_name_vn}}</b></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">

                                <div class="row">
                                    <div class="col-md-3">

                                        <span for="exampleInputEmail1">Thành phố:</span>
                                    </div>
                                    <div class="col-md-3">
                                        <span for="exampleInputEmail1">  <b>{{$promo->id_city}}</b></span>

                                    </div>

                                </div>
                            </div>
                            <div class="form-group">

                                <div class="row">
                                    <div class="col-md-3">

                                        <span for="exampleInputEmail1">Ngày nhận phòng từ:</span>
                                    </div>

                                    <div class="col-md-3">
                                        <span for="exampleInputEmail1"><b>{{$promo->checkinFrom}}</b></span>
                                        </div>
                                </div>

                            </div>
                            <div class="form-group">


                                <div class="row">
                                    <div class="col-md-3">

                                        <span for="exampleInputEmail1">Ngày nhận phòng đến:</span>
                                    </div>

                                    <div class="col-md-3">

                                        <span for="exampleInputEmail1"> <b>{{$promo->checkinTo}}</b> </span>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group">


                                <div class="row">
                                    <div class="col-md-3">

                                        <span for="exampleInputEmail1">Số lượng phát hành:</span>
                                    </div>

                                    <div class="col-md-3">

                                        <span for="exampleInputEmail1"> <b>{{$promo->number_max}}</b> </span>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group">


                                <div class="row">
                                    <div class="col-md-3">

                                        <span for="exampleInputEmail1">Giá trị đơn hàng tối thiểu phải đạt:</span>
                                    </div>

                                    <div class="col-md-3">

                                        <span for="exampleInputEmail1"> <b>@if($promo->value_order_min==0) Không giới hạn @else {{$promo->value_order_min}} @endif</b> </span>
                                    </div>
                                </div>

                            </div>


                            <div class="form-group">


                                <div class="row">
                                    <div class="col-md-3">

                                        <span for="exampleInputEmail1">Số lượng khách tối thiểu:</span>
                                    </div>
                                    <div class="col-md-3">

                                        <span for="exampleInputEmail1"> <b>{{$promo->number_user_min}}</b> </span>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group">


                                <div class="row">
                                    <div class="col-md-3">

                                        <span for="exampleInputEmail1">Khách sạn áp dụng</span>
                                    </div>
                                    <div class="col-md-3">

                                        <span for="exampleInputEmail1"> <b>{{$promo->hotel_apply}}</b> </span>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>

                </div>
            </section>

        </div>

    </div>
@endsection

<style>
    .info-promo{
        border: 1px solid;
        padding: 10px;
        border-radius: 10px;
    }
</style>
