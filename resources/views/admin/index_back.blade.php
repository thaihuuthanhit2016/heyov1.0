@extends('admin.master')
@section('content')
    <center><h3>Thống kê đơn hàng doanh số</h3></center>
    <form autocomplete="off">
        <div class="col-md-2">
            <p>Từ ngày: <input type="text" id="datepicker" class="form-control"></p>

        </div>
        <div class="col-md-2">
            <p>Đến ngày: <input type="text" id="datepicker2" class="form-control"></p>
        </div>

        <div class="col-md-2" style="margin-top: 22px;">
            <p><a style="cursor: pointer" id="btn-dashboard-filter" class="btn btn-primary btn-sm" >Lọc kết quả</a></p>
        </div>
        <div class="col-md-2" style="float:right;">
            <p>Lọc theo:
                <select class="dashboard-filter form-control">
                    <option>--- Chọn ---</option>
                    <option value="7ngay">7 ngày</option>
                    <option value="thangtruoc">Tháng trước</option>
                    <option value="thangnay">Tháng này</option>
                    <option value="365ngayqua">1 năm</option>
                </select>
            </p>
        </div>
        <div class="col-md-3" style="float:right;">
            <p>Tổng doanh thu:<br><span style="font-weight: bold">{{number_format($sales_vnd,0,',','.')}} VNĐ</span>
                <br><span style="font-weight: bold">{{number_format($sales_coin,0,',','.')}} DemoCrm</span>
            </p>
        </div>
    </form>

    <div class="clearfix"></div>

    <br>
    <div class="col-md-12">
        <div id="myfirstchat" style="height: 250px; background: white"></div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
        <br>
        <center><h3>Thống kê truy cập</h3></center>
        <br>
        <div class="col-md-12" style="background: white">
            <table class="table">
                <thead>
                <tr>
                    <th>Đang online</th>
                    <th>Tổng tháng trước</th>
                    <th>Tổng tháng này</th>
                    <th>Tổng 1 năm</th>
                    <th>Tổng truy cập</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{{$vitors_count}}</td>
                    <td>{{$vitors_of_lastMonth_count}}</td>
                    <td>{{$vitors_of_thisMonth_count}}</td>
                    <td>{{$vitors_of_year_count}}</td>
                    <td>{{$vitors_total}}</td>
                </tr>
                </tbody>
            </table>

        </div>
    </div>
    <div class="row">
        @if(!Session::get('admin_id'))
            <div class="col-md-4">
                <div id="donut"></div>
            </div>
        @endif

        @if(Session::get('admin_id'))
            <div class="col-md-4">
                <div id="donut1"></div>
            </div>
        @endif
    </div>
@endsection
<style>
    text
    {
        color: red!important;
    }
</style>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script src='https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.2/raphael-min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.0/morris.min.js'></script>
<!-- //calendar -->
<script>
    $(document).ready(function (){
        $(function (){
            $("#datepicker").datepicker({
                prevText:"Tháng trước",
                nextText:"Tháng sau",
                dateFormat: 'yy-mm-dd',
                dayNamesMin:['Thứ 2','Thứ 3','Thứ 4','Thứ 5','Thứ 6','Thứ 7','Chủ nhật'],
                duration:'slow'

            });
            $("#datepicker2").datepicker({
                prevText:"Tháng trước",
                nextText:"Tháng sau",
                dateFormat: 'yy-mm-dd' ,
                dayNamesMin:['Thứ 2','Thứ 3','Thứ 4','Thứ 5','Thứ 6','Thứ 7','Chủ nhật'],
                duration:'slow'

            });
        });

        chart30daysorder();
        var chart=new Morris.Bar({
            element:'myfirstchat',
            parseTime:false,
            hideHover:"auto",
            xkey:'period',
            pointStrokeColors:['black'],
            ykeys:['order','sales','profit','quantity'],
            labels:['đơn hàng','doanh số','lợi nhuận','số lương']

        });

        @if(!Session::get('admin_id'))
        var donut=Morris.Donut({
            element: 'donut',
            resize:true,
            color:[
                '#a8328e'
            ],
            data:[
                {label:"Sản phẩm", value:{{$product}}}
            ]
        });
        @endif
        @if(Session::get('admin_id'))

        var donut1=Morris.Donut({
            element: 'donut1',
            resize:true,
            color:[
                '#ea0610','#0c13a3','#1addad','#ea0610',
            ],
            data:[
                {label:"Tài khoản đã KYC", value:{{$account}}},
                {label:"Tài khoản chưa KYC", value:{{$account_u}}},
                {label:"Tài khoản chờ duyệt KYC", value:{{$account_p}}},
                {label:"Tài khoản từ chối KYC", value:{{$account_r}}}
            ]
        });
        @endif
        $('div svg text').css({
            "font-family" : "'Roboto",

        });

        function chart30daysorder(){
            $.ajax({
                url:"{{asset('admin/30day')}}",
                method:'Get',
                dataType:'JSON',
                success:function (data){
                    chart.setData(data);

                }
            });
        }

        $('.dashboard-filter').change(function (){
            var dashboard_value=$(this).val();

            $.ajax({
                url: "{{asset('admin/dashboard-filter')}}",
                method: "get",
                dataType: "JSON",
                data: {dashboard_value: dashboard_value},
                success: function (data) {
                    chart.setData(data);

                }
            });
        });
        $('#btn-dashboard-filter').click(function (){

            var from_date=$('#datepicker').val();
            var to_date=$('#datepicker2').val();

            $.ajax({
                url:"{{asset('admin/filter-by-date')}}",
                method:'get',
                dataType:"JSON",
                data:{from_date:from_date,to_date:to_date},
                success:function (data){
                    chart.setData(data);

                }
            })
        });
    });
</script>
