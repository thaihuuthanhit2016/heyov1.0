$(document).ready(function () {
  $("#btn_admin").click(function (e) {
    e.stopPropagation();
    var display = $("#profile").css(["display"]);
    if (display.display === "none") {
      document.getElementById("profile").style = "display: flex";
    } else {
      document.getElementById("profile").style = "display: none";
    }
  });
});

$(document).ready(function () {
  $("body").click(function () {
    document.getElementById("profile").style = "display: none";
    if (document.getElementById("box_option_discount"))
      document.getElementById("box_option_discount").style = "display: none";
  });
  $("#dots").click(function (e) {
    e.stopPropagation();
    var position = $("#profile").css(["top", "right"]);
    console.log($("#dots").position());
    document.getElementById("box_option_discount").style = "display: block";
  });
  $("#box_option_discount").click(function (e) {
    e.stopPropagation();
  });
  $("#btn_choose_file_cover").click(function (e) {
    e.stopPropagation();
    $("#coverFile").click();
  });
  $("#btn_choose_file_logo").click(function (e) {
    e.stopPropagation();
    $("#logoFile").click();
  });
  $("#radioDiscount1").click(function (e) {
    document.getElementById("box_discount1").style = "display: block";
    document.getElementById("box_discount2").style = "display: none";
  });
  $("#radioDiscount2").click(function (e) {
    document.getElementById("box_discount1").style = "display: none";
    document.getElementById("box_discount2").style = "display: flex";
  });
  $("#del_payment_partner").click(function () {
    $("#payment_partner").val(null).trigger("change");
  });
  $("#del_hotel").click(function () {
    $("#hotel_apply").val(null).trigger("change");
  });
  $("#del_service").click(function () {
    $("#service").val(null).trigger("change");
  });
  $("#datetimepicker1").datetimepicker({
    format: "DD-MM-YYYY LT",
    minDate: new Date(),
  });
  $("#datetimepicker2").datetimepicker({
    format: "DD-MM-YYYY LT",
    minDate: new Date(),
  });
  $("#datetimepicker3").datetimepicker({
    format: "DD-MM-YYYY LT",
    minDate: new Date(),
  });
  $("#datetimepicker4").datetimepicker({
    format: "DD-MM-YYYY LT",
    minDate: new Date(),
  });
  $("#close_review").click(function () {
    document.getElementById("modal-review").style = "display: none";
  });
  $("#btn_watch").click(function () {
    console.log("first");
    document.getElementById("modal-review").style = "display: block";
  });
  $("#close_create").click(function () {
    $("div").remove(".modal-backdrop");
  });
  $("#btn_term").click(function () {
    var rotate = parseInt(
      $("#icon_term").attr("style").split("rotate(")[1].split("deg)")[0]
    );
    if (rotate === 0) {
      document.getElementById("icon_term").style =
        "transform: rotate(180deg); width: 25px; height: 25px";
    } else {
      document.getElementById("icon_term").style =
        "transform: rotate(0deg); width: 25px; height: 25px";
    }
  });
  $("#btn_condition").click(function () {
    var rotate = parseInt(
      $("#icon_condition").attr("style").split("rotate(")[1].split("deg)")[0]
    );
    if (rotate === 0) {
      document.getElementById("icon_condition").style =
        "transform: rotate(180deg); width: 25px; height: 25px";
    } else {
      document.getElementById("icon_condition").style =
        "transform: rotate(0deg); width: 25px; height: 25px";
    }
  });
});
$(document).ready(function () {
  $(".js-select2-multi").select2();
  $("#toggle-state-switch").change(function () {
    console.log("first");
  });
});
