import React from 'react';
import { Outlet } from 'react-router-dom';
import { Box, Grid, styled } from '@mui/material';
import HotelNavbar from './HotelNavbar';
import HotelSidebar from './HotelSidebar';

const RootStyle = styled(Box)(({ theme }) => ({
  width: `100%`
}));
const BoxMain = styled(Box)(({ theme }) => ({
  width: '100%',
  display: 'flex'
}));
const BoxContent = styled(Grid)(({ theme }) => ({
  width: '100%'
}));
function HotelLayout() {
  return (
    <RootStyle>
      <HotelNavbar />
      <BoxMain>
        <HotelSidebar />
        <BoxContent>
          <Outlet />
        </BoxContent>
      </BoxMain>
    </RootStyle>
  );
}

export default HotelLayout;
