import React from 'react';
import { Box, Grid, styled } from '@mui/material';
import sideBarConfigTop from './SidebarConfigTop';
import MenuLeftItem from '../components/dashboard/MenuLeftItem';
import sideBarConfigBottom from './SidebarConfigBottom';

const RootStyle = styled(Box)(({ theme }) => ({
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'space-between',
  alignContent: 'center',
  backgroundColor: theme.palette.main,
  minHeight: 'calc(100vh - 62px)',
  width: '80px'
}));

function HotelSidebar() {
  return (
    <RootStyle>
      <Box sx={{ width: '100%' }}>
        {sideBarConfigTop.map((item, index) => (
          <MenuLeftItem key={index} menu={item} />
        ))}
      </Box>
      <Box>
        {sideBarConfigBottom.map((item, index) => (
          <MenuLeftItem key={index} menu={item} />
        ))}
      </Box>
    </RootStyle>
  );
}

export default HotelSidebar;
