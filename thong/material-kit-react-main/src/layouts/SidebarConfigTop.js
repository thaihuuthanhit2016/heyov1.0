const sideBarConfigTop = [
  {
    path: '/hotels',
    icon: 'icon-park-outline:hotel',
    name: 'Khách sạn'
  },
  {
    path: '/flight',
    icon: 'ps:plane',
    name: 'Vé máy bay'
  },
  {
    path: '/cars',
    icon: 'clarity:car-line',
    name: 'Thuê xe'
  },
  {
    path: '/crypto',
    icon: 'clarity:data-cluster-line',
    name: 'Crypto'
  }
];

export default sideBarConfigTop;
