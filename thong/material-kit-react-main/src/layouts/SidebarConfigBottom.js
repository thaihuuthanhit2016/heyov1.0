const sideBarConfigBottom = [
  {
    path: '/account',
    icon: 'ep:user-filled',
    name: 'Tài khoản'
  },
  {
    path: '/menu',
    icon: 'dashicons:welcome-widgets-menus',
    name: 'Danh mục'
  },
  {
    path: '/website',
    icon: 'gg:website',
    name: 'Website'
  },
  {
    path: '/setting',
    icon: 'material-symbols:settings-suggest-outline-sharp',
    name: 'Cấu hình'
  }
];

export default sideBarConfigBottom;
