import React from 'react';
import { Badge, Box, Grid, IconButton, styled, Typography } from '@mui/material';
import { Icon } from '@iconify/react';

const RootStyle = styled(Box)(({ theme }) => ({
  width: '100%',
  height: '62px',
  display: 'flex',
  alignItems: 'center'
}));
const BoxLeft = styled(Box)(({ theme }) => ({
  background: theme.palette.main,
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  width: '295px',
  height: '62px'
}));
const Title = styled(Typography)(({ theme }) => ({
  fontSize: '25px',
  lineHeight: '35px',
  fontFamily: theme.typography.fontFamily.primary,
  color: theme.palette.white,
  fontWeight: '800'
}));
const BoxRight = styled(Grid)(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'space-between',
  padding: '0px 5%',
  background: '#fff',
  width: `calc(100% - 295px)`
}));
const NameMenu = styled(Typography)(({ theme }) => ({
  fontFamily: theme.typography.fontFamily.primary,
  fontWeight: '700',
  fontSize: '20px',
  lineHeight: '19.5px'
}));
const BoxOption = styled(Box)(({ theme }) => ({
  display: 'flex',
  alignItems: 'center'
}));
const IconNotification = styled(Icon)(({ theme }) => ({
  color: theme.palette.main,
  width: '20px',
  height: '20px'
}));
const BoxAdmin = styled(Box)(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'space-between'
}));
const Admin = styled(Typography)(({ theme }) => ({
  fontSize: '16px',
  lineHeight: '24px',
  fontWeight: '600',
  fontFamily: theme.typography.fontFamily.primary,
  marginLeft: '5px'
}));
function HotelNavbar() {
  return (
    <RootStyle>
      <BoxLeft>
        <Title>HEYO TRIP</Title>
      </BoxLeft>
      <BoxRight>
        <NameMenu>Chương trình khuyến mãi</NameMenu>
        <BoxOption>
          <IconButton sx={{ marginRight: '20px' }}>
            <Badge
              sx={{ width: '21px', height: '22px', fontSize: '16px' }}
              badgeContent={9}
              color="error"
            >
              <IconNotification icon="ic:outline-notifications-none" />
            </Badge>
          </IconButton>
          <BoxAdmin>
            <IconNotification icon="lucide:user" />
            <Admin>Admin</Admin>
            <IconNotification
              icon="ant-design:caret-down-filled"
              sx={{ width: '15px', height: '15px', marginLeft: '15px' }}
            />
          </BoxAdmin>
        </BoxOption>
      </BoxRight>
    </RootStyle>
  );
}

export default HotelNavbar;
