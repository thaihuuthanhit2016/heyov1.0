import React from 'react';
import { Box, Grid, styled } from '@mui/material';
import PropTypes from 'prop-types';
import MenuItemRight from './MenuItemRight';

const RootStyle = styled(Box)(({ theme }) => ({
  display: 'flex',
  flexDirection: 'column',
  alignContent: 'center',
  backgroundColor: theme.palette.backgroundBlue,
  minHeight: 'calc(100vh - 62px)',
  width: '215px'
}));
SidebarMenuRight.prototype = {
  sidebarConfig: PropTypes.array
};
function SidebarMenuRight({ sidebarConfig }) {
  return (
    <RootStyle>
      {sidebarConfig.map((item, index) => (
        <MenuItemRight index={index} key={index} menu={item} />
      ))}
    </RootStyle>
  );
}

export default SidebarMenuRight;
