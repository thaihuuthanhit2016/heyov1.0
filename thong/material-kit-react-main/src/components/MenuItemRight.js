import React from 'react';
import { Box, styled, Typography } from '@mui/material';
import PropTypes from 'prop-types';
import { Icon } from '@iconify/react';
import { useLocation, useNavigate } from 'react-router-dom';
import hotelConfig from './hotel/hotelConfig';

const RootStyle = styled(Box)(({ theme }) => ({
  width: '100%',
  padding: '15px 10%',
  display: 'flex',
  alignItems: 'center',
  cursor: 'pointer'
}));
const IconMenu = styled(Icon)(({ theme }) => ({
  width: '20px',
  height: '20px',
  color: theme.palette.primary
}));
const NameMenu = styled(Typography)(({ theme }) => ({
  fontSize: '12px',
  lineHeight: '19.5px',
  color: theme.palette.main,
  fontFamily: theme.typography.fontFamily.primary,
  fontWeight: 'bold',
  marginLeft: '10px'
}));
MenuItemRight.prototype = {
  menu: PropTypes.object,
  index: PropTypes.number
};
function MenuItemRight({ menu, index }) {
  const { pathname } = useLocation();
  const navigate = useNavigate();
  const handleChoose = () => {
    navigate(menu.path);
  };
  return (
    <RootStyle
      sx={{ borderBottom: index < hotelConfig.length - 1 && `1px solid #D5E2EA` }}
      onClick={handleChoose}
    >
      <IconMenu sx={{ color: menu.path.includes(pathname) && `#329223` }} icon={menu.icon} />
      <NameMenu sx={{ color: menu.path.includes(pathname) && `#329223` }}>{menu.name}</NameMenu>
    </RootStyle>
  );
}

export default MenuItemRight;
