import React from 'react';
import PropTypes from 'prop-types';
import { Box, styled, Typography } from '@mui/material';
import { Icon } from '@iconify/react';
import { useLocation, useNavigate } from 'react-router-dom';

const RootStyle = styled(Box)(({ theme }) => ({
  height: '80px',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  flexDirection: 'column',
  cursor: 'pointer',
  width: '80px'
}));
const IconMenu = styled(Icon)(({ theme }) => ({
  width: '30px',
  height: '30px',
  color: theme.palette.second
}));
const NameIcon = styled(Typography)(({ theme }) => ({
  fontSize: '12px',
  fontWeight: '600',
  fontFamily: theme.typography.fontFamily.primary,
  color: theme.palette.second,
  marginTop: '5px'
}));
MenuLeftItem.prototype = {
  menu: PropTypes.object
};
function MenuLeftItem({ menu }) {
  const { pathname } = useLocation();
  const navigate = useNavigate();
  const handleChoose = () => {
    navigate(menu.path);
  };
  return (
    <RootStyle
      onClick={handleChoose}
      sx={{ backgroundColor: pathname.includes(menu.path) && '#F2F9FF' }}
    >
      <IconMenu icon={menu.icon} />
      <NameIcon>{menu.name}</NameIcon>
    </RootStyle>
  );
}

export default MenuLeftItem;
