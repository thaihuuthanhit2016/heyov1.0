import React from 'react';
import {
  Autocomplete,
  Box,
  Button,
  Divider,
  Grid,
  styled,
  TextField,
  Typography,
  Switch,
  IconButton,
  Popper,
  Card,
  ListItemButton
} from '@mui/material';
import { Icon } from '@iconify/react';

const RootStyle = styled(Box)(({ theme }) => ({
  width: '100%'
}));
const BoxSearch = styled(Box)(({ theme }) => ({
  width: '100%',
  border: `1px solid ${theme.palette.border}`,
  borderRadius: '5px',
  background: theme.palette.white
}));
const Title = styled(Typography)(({ theme }) => ({
  fontSize: '18px',
  lineHeight: '19.5px',
  fontFamily: theme.typography.fontFamily.primary
}));
const BoxTitle = styled(Box)(({ theme }) => ({
  padding: '10px',
  display: 'flex',
  alignItems: 'center'
}));
const Separate = styled(Divider)(({ theme }) => ({
  backgroundColor: '#DEE2E6'
}));
const GridInput = styled(Grid)(({ theme }) => ({
  padding: '10px'
}));
const Row = styled(Grid)(({ theme }) => ({
  width: '100%'
}));
const BoxInput = styled(Box)(({ theme }) => ({
  width: '100%'
}));
const LabelInput = styled(Typography)(({ theme }) => ({
  fontWeight: 'bold',
  fontSize: '12px',
  lineHeight: '19.5px',
  fontFamily: theme.typography.fontFamily.primary
}));
const Input = styled('input')(({ theme }) => ({
  width: '100%',
  height: '35px',
  padding: '0px 10px',
  border: `1px solid #DEE2E6`,
  marginTop: '5px',
  borderRadius: '5px',
  boxSizing: 'border-box'
}));
const BoxComboBox = styled('select')(({ theme }) => ({
  width: '100%',
  height: '35px',
  padding: '0px 10px',
  border: `1px solid #DEE2E6`,
  borderRadius: '5px',
  marginTop: '5px'
}));
const ButtonDate = styled(Box)(({ theme }) => ({
  width: '35px',
  height: '35px',
  background: '#E9ECEF',
  border: `1px solid #DEE2E6`,
  boxSizing: `border-box`,
  borderRadius: `5px`,
  marginTop: '5px',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  cursor: 'pointer'
}));
const IconDate = styled(Icon)(({ theme }) => ({
  color: '#333333',
  width: '20px',
  height: '20px'
}));
const BoxSearchOption = styled(Box)(({ theme }) => ({
  width: '100%',
  backgroundColor: theme.palette.background,
  padding: '10px 20px',
  borderRadius: '5px',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'space-between'
}));
const ButtonSearch = styled('button')(({ theme }) => ({
  backgroundColor: theme.palette.mainBlue,
  border: `1px solid #3982D1`,
  borderRadius: '5px',
  boxSizing: `border-box`,
  height: '35px',
  display: 'flex',
  alignItems: 'center',
  padding: '0px 5px',
  cursor: 'pointer'
}));
const IconSearch = styled(Icon)(({ theme }) => ({
  width: '20px',
  height: '20px',
  color: theme.palette.white
}));
const NameSearch = styled(Typography)(({ theme }) => ({
  fontSize: '14px',
  color: theme.palette.white,
  lineHeight: '20px',
  fontFamily: theme.typography.fontFamily.primary,
  marginLeft: '5px'
}));
const ButtonClear = styled('button')(({ theme }) => ({
  backgroundColor: theme.palette.white,
  border: `1px solid #DEE2E6`,
  borderRadius: '5px',
  boxSizing: `border-box`,
  color: '#666666',
  fontSize: '14px',
  height: '35px',
  padding: '0px 10px',
  cursor: 'pointer'
}));
const BoxTable = styled(Box)(({ theme }) => ({
  width: '100%',
  border: `1px solid ${theme.palette.border}`,
  borderRadius: '5px',
  background: theme.palette.white,
  marginTop: '20px'
}));
const ButtonAdd = styled('button')(({ theme }) => ({
  backgroundColor: theme.palette.mainBlue,
  border: `1px solid #3982D1`,
  borderRadius: '5px',
  boxSizing: `border-box`,
  height: '35px',
  display: 'flex',
  alignItems: 'center',
  padding: '0px 5px',
  cursor: 'pointer'
}));
const Table = styled('table')(({ theme }) => ({
  width: '100%',
  borderCollapse: `collapse`
}));
const TableRow = styled('tr')(({ theme }) => ({
  width: '100%',
  borderBottom: `1px solid #DEE2E6`
}));
const CellHeader = styled('th')(({ theme }) => ({
  fontStyle: 'normal',
  fontWeight: '700',
  fontFamily: theme.typography.fontFamily.primary,
  fontSize: '13px',
  lineHeight: '20px',
  padding: '5px',
  textAlign: 'left'
}));
function DiscountRow({ row }) {
  const Status = styled('span')(({ theme }) => ({
    fontWeight: 'bold',
    fontSize: '13px'
  }));
  const Date = styled(Typography)(({ theme }) => ({
    fontSize: '14px',
    fontFamily: theme.typography.fontFamily.primary,
    fontWeight: '500',
    lineHeight: '19.5px'
  }));
  const ButtonOption = styled(ListItemButton)(({ theme }) => ({
    display: 'flex',
    alignItems: 'center'
  }));
  const NameOption = styled(Typography)(({ theme }) => ({
    fontSize: '13px',
    color: '#888888',
    fontWeight: '500',
    fontFamily: theme.typography.fontFamily.primary,
    marginLeft: '5px'
  }));
  const checkStatus = () => {
    if (row.maKhuyenMai.status === 'Active') return '#329223';
    if (row.maKhuyenMai.status === 'Inactive') return '#FB7604';
    if (row.maKhuyenMai.status === 'Expired') return '#555555';
    return '#CA0101';
  };
  const [anchorEl, setAnchorEl] = React.useState(null);
  const handleClick = (event) => {
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };
  const open = Boolean(anchorEl);
  return (
    <TableRow>
      <td>
        <Switch />
      </td>
      <td style={{ color: '#3982D1', fontSize: '14px' }}>{row.maChuongTring}</td>
      <td>
        <Typography style={{ fontSize: '14px' }}>{row.maKhuyenMai.name}</Typography>
        <Status sx={{ color: checkStatus() }}>{row.maKhuyenMai.status}</Status>
      </td>
      <td>
        <Typography sx={{ fontSize: '14px', fontWeight: '500' }}>
          {row.chuongTrinhKhuyenMai}
        </Typography>
      </td>
      <td>
        <Typography sx={{ fontSize: '14px', fontWeight: '500' }}>
          {row.loaiKhuyenMai.name}
        </Typography>
        <Typography sx={{ fontSize: '12px', color: '#888888' }}>
          {row.loaiKhuyenMai.country}
        </Typography>
      </td>
      <td>
        <Date>{row.ngayBatDat}</Date>
      </td>
      <td>
        <Date>{row.ngayKetThuc}</Date>
      </td>
      <td>
        <Date>{row.soLuong}</Date>
      </td>
      <td>
        <IconButton onClick={handleClick}>
          <Icon icon="bi:three-dots" />
        </IconButton>
        <Popper placement="bottom-end" open={open} anchorEl={anchorEl}>
          <Card
            sx={{
              background: '#fff',
              zIndex: 2,
              padding: '10px',
              boxShadow: 3,
              border: `1px solid #DEE2E6`
            }}
          >
            <ButtonOption>
              <Icon style={{ width: '20px', height: '20px' }} icon="clarity:note-edit-line" />
              <NameOption>Chỉnh sửa</NameOption>
            </ButtonOption>
            <ButtonOption>
              <Icon style={{ width: '20px', height: '20px' }} icon="mdi-light:content-duplicate" />
              <NameOption>Duplicate</NameOption>
            </ButtonOption>
            <ButtonOption>
              <Icon style={{ width: '20px', height: '20px' }} icon="ion:trash-outline" />
              <NameOption>Xoá</NameOption>
            </ButtonOption>
          </Card>
          {/* <Icon
            style={{
              position: 'absolute',
              right: 10,
              top: -11,
              boxShadow: 3,
              zIndex: 1,
              color: '#C4C4C4'
            }}
            icon="bi:caret-up"
          /> */}
        </Popper>
      </td>
    </TableRow>
  );
}
function Discount() {
  const header = [
    {
      name: '',
      width: '5%'
    },
    {
      name: 'Mã chương trình',
      width: '13%'
    },
    {
      name: 'Mã khuyến mãi',
      width: '13%'
    },
    {
      name: 'Chương trình khuyến mãi',
      width: '22%'
    },
    {
      name: 'Loại khuyến mãi',
      width: '13%'
    },
    {
      name: 'Ngày bắt đầu',
      width: '13%'
    },
    {
      name: 'Ngày kết thúc',
      width: '13%'
    },
    {
      name: 'Số lượng',
      width: '5%'
    },
    {
      name: '',
      width: '3%'
    }
  ];
  const data = [
    {
      maChuongTring: '08032022 H-VNPHUNU83',
      maKhuyenMai: {
        name: 'PHUNU83',
        status: 'Active'
      },
      chuongTrinhKhuyenMai: 'Đẹp là không chờ đợi',
      loaiKhuyenMai: {
        name: 'Promo Code',
        country: 'Indonesia'
      },
      ngayBatDat: '01/03/2022 20:00 AM',
      ngayKetThuc: '20/05/2022 20:00 PM',
      soLuong: 200
    },
    {
      maChuongTring: '08032022 H-VNPHUNU83',
      maKhuyenMai: {
        name: 'PHUNU83',
        status: 'Inactive'
      },
      chuongTrinhKhuyenMai: 'Đẹp là không chờ đợi',
      loaiKhuyenMai: {
        name: 'Promo Code',
        country: 'Indonesia'
      },
      ngayBatDat: '01/03/2022 20:00 AM',
      ngayKetThuc: '20/05/2022 20:00 PM',
      soLuong: 200
    },
    {
      maChuongTring: '08032022 H-VNPHUNU83',
      maKhuyenMai: {
        name: 'PHUNU83',
        status: 'Expired'
      },
      chuongTrinhKhuyenMai: 'Đẹp là không chờ đợi',
      loaiKhuyenMai: {
        name: 'Promo Code',
        country: 'Indonesia'
      },
      ngayBatDat: '01/03/2022 20:00 AM',
      ngayKetThuc: '20/05/2022 20:00 PM',
      soLuong: 200
    },
    {
      maChuongTring: '08032022 H-VNPHUNU83',
      maKhuyenMai: {
        name: 'PHUNU83',
        status: 'Deactivate'
      },
      chuongTrinhKhuyenMai: 'Đẹp là không chờ đợi',
      loaiKhuyenMai: {
        name: 'Promo Code',
        country: 'Indonesia'
      },
      ngayBatDat: '01/03/2022 20:00 AM',
      ngayKetThuc: '20/05/2022 20:00 PM',
      soLuong: 200
    }
  ];
  return (
    <RootStyle>
      <BoxSearch>
        <BoxTitle>
          <Title>Tra cứu khuyến mãi</Title>
        </BoxTitle>
        <Separate />
        <Box sx={{ padding: '10px' }}>
          <Row container>
            <GridInput item xs={12} sm={12} md={6} lg={6} xl={6}>
              <BoxInput>
                <LabelInput>Mã chương trình</LabelInput>
                <Input fullWidth size="small" placeholder="Nhập chương trình" />
              </BoxInput>
            </GridInput>
            <GridInput item xs={12} sm={12} md={6} lg={6} xl={6}>
              <BoxInput>
                <LabelInput>Mã khuyễn mãi</LabelInput>
                <Input fullWidth size="small" placeholder="Nhập mã khuyễn mãi" />
              </BoxInput>
            </GridInput>
          </Row>
          <Row container>
            <GridInput item xs={12} sm={12} md={4} lg={4} xl={4}>
              <BoxInput>
                <LabelInput>Loại khuyến mãi</LabelInput>
                <BoxComboBox style={{ color: 'gray' }}>
                  <option value="" disabled selected hidden>
                    Chọn khuyến mãi
                  </option>
                  {[1, 2, 3].map((item, index) => (
                    <option>{item}</option>
                  ))}
                </BoxComboBox>
              </BoxInput>
            </GridInput>
            <GridInput item xs={12} sm={12} md={4} lg={4} xl={4}>
              <BoxInput>
                <LabelInput>Trạng thái</LabelInput>
                <BoxComboBox style={{ color: 'gray' }}>
                  <option value="" disabled selected hidden>
                    Chọn trạng thái
                  </option>
                  {[1, 2, 3].map((item, index) => (
                    <option>{item}</option>
                  ))}
                </BoxComboBox>
              </BoxInput>
            </GridInput>
            <GridInput item xs={12} sm={12} md={4} lg={4} xl={4}>
              <BoxInput>
                <LabelInput>Quốc gia</LabelInput>
                <BoxComboBox style={{ color: 'gray' }}>
                  <option value="" disabled selected hidden>
                    Chọn quốc gia du lịch
                  </option>
                  {[1, 2, 3].map((item, index) => (
                    <option>{item}</option>
                  ))}
                </BoxComboBox>
              </BoxInput>
            </GridInput>
          </Row>
          <Row container>
            <GridInput item xs={12} sm={12} md={6} lg={6} xl={6}>
              <BoxInput>
                <LabelInput>Ngày bắt đầu</LabelInput>
                <Box sx={{ display: 'flex', alignItems: 'center' }}>
                  <Input disabled fullWidth size="small" placeholder="Nhập" />
                  <ButtonDate>
                    <IconDate icon="fa6-solid:calendar-days" />
                  </ButtonDate>
                </Box>
              </BoxInput>
            </GridInput>
            <GridInput item xs={12} sm={12} md={6} lg={6} xl={6}>
              <BoxInput>
                <LabelInput>Ngày kết thúc</LabelInput>
                <Box sx={{ display: 'flex', alignItems: 'center' }}>
                  <Input disabled fullWidth size="small" placeholder="Nhập" />
                  <ButtonDate>
                    <IconDate icon="fa6-solid:calendar-days" />
                  </ButtonDate>
                </Box>
              </BoxInput>
            </GridInput>
          </Row>
        </Box>
        <BoxSearchOption>
          <ButtonSearch>
            <IconSearch icon="carbon:search" />
            <NameSearch>Tìm kiếm</NameSearch>
          </ButtonSearch>
          <ButtonClear>Xoá bộ lọc</ButtonClear>
        </BoxSearchOption>
      </BoxSearch>
      <BoxTable>
        <BoxTitle sx={{ justifyContent: 'space-between' }}>
          <Title>4 khuyến mãi</Title>
          <ButtonAdd>
            <IconSearch icon="fluent:add-16-filled" />
            <NameSearch>Thêm mới</NameSearch>
          </ButtonAdd>
        </BoxTitle>
        <Separate />
        <Box sx={{ width: '100%' }}>
          <Table>
            <TableRow style={{ width: '100%' }}>
              {header.map((item, index) => (
                <CellHeader key={index} style={{ width: item.width }}>
                  {item.name}
                </CellHeader>
              ))}
            </TableRow>
            {data.map((item, index) => (
              <DiscountRow row={item} key={index} />
            ))}
          </Table>
        </Box>
        <Box sx={{ height: '50px' }}> </Box>
      </BoxTable>
    </RootStyle>
  );
}

export default Discount;
