const hotelConfig = [
  {
    path: '/hotels/dashboard',
    icon: 'majesticons:analytics',
    name: 'Dashboard'
  },
  {
    path: '/hotels/booking',
    icon: 'mdi:bed',
    name: 'Tìm - Đặt phòng'
  },
  {
    path: '/hotels/books',
    icon: 'fa-solid:calendar-day',
    name: 'Quản lý đơn đặt phòng'
  },
  {
    path: '/hotels/price',
    icon: 'fa-solid:money-check-alt',
    name: 'Tuỳ chỉnh giá phòng'
  },
  {
    path: '/hotels/discount',
    icon: 'icomoon-free:price-tags',
    name: 'Chương trình khuyến mãi'
  },
  {
    path: '/hotels/percent',
    icon: 'eva:percent-fill',
    name: 'Quản lý hoa hồng'
  },
  {
    path: '/hotels/cancel',
    icon: 'fa6-solid:hand-holding-dollar',
    name: 'Huỷ - Hoàn tiền'
  },
  {
    path: '/hotels/marketing',
    icon: 'fluent:news-20-filled',
    name: 'Bài viết Marketing'
  }
];

export default hotelConfig;
