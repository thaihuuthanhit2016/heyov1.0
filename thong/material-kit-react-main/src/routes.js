import { Navigate, useRoutes } from 'react-router-dom';
import Dashboard from './components/hotel/Dashboard';
import Discount from './components/hotel/Discount';
import HotelLayout from './layouts';
import Flight from './page/Flight';
import Hotel from './page/Hotel';

export default function Router() {
  return useRoutes([
    {
      path: '/',
      element: <HotelLayout />,
      children: [
        { path: '/', element: <Navigate to="/hotels" /> },
        {
          path: 'hotels',
          element: <Hotel />,
          children: [
            {
              path: '',
              element: <Navigate to="/hotels/dashboard" />
            },
            {
              path: 'dashboard',
              element: <Dashboard />
            },
            {
              path: 'discount',
              element: <Discount />
            }
          ]
        },
        { path: 'flight', element: <Flight /> }
      ]
    }
  ]);
}
