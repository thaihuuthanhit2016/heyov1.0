const palette = {
  main: '#032044',
  second: '#526A87',
  white: '#fff',
  black: '#000',
  gray: 'gray',
  lightgrey: '#D9DDE9',
  mainBlue: '#007BFF',
  backgroundBlue: '#F2F9FF',
  background: '#F7F7F7',
  mainGreen: '#329223',
  mainOrange: 'FB7604',
  mainCrown: '#555555',
  mainRed: '#CA0101',
  border: '#DEE2E6'
};

export default palette;
