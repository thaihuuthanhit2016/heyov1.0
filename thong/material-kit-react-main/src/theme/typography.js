const typography = {
  fontFamily: {
    primary: 'Montserrat',
    second: 'garamond'
  }
};

export default typography;
