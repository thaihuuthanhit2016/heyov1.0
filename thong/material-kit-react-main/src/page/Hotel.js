import React from 'react';
import { Box, Grid, styled } from '@mui/material';
import { Outlet } from 'react-router-dom';
import SidebarMenuRight from '../components/SidebarMenuRight';
import hotelConfig from '../components/hotel/hotelConfig';

const RootStyle = styled(Box)(({ theme }) => ({
  width: '100%',
  display: 'flex'
}));
const BoxContent = styled(Box)(({ theme }) => ({
  minWidth: 'calc(100vw - 312px)',
  background: theme.palette.background,
  padding: '10px'
}));
function Hotel() {
  return (
    <RootStyle>
      <SidebarMenuRight sidebarConfig={hotelConfig} />
      <BoxContent>
        <Outlet />
      </BoxContent>
    </RootStyle>
  );
}

export default Hotel;
