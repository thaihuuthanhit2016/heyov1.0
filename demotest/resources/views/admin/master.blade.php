<!--A Design by W3layouts
   Author: W3layout
   Author URL: http://w3layouts.com
   License: Creative Commons Attribution 3.0 Unported
   License URL: http://creativecommons.org/licenses/by/3.0/
   -->
<!DOCTYPE html>
<head>
    <title>Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Visitors Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
      Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- bootstrap-css -->
    <link rel="stylesheet" href="http://localhost/testlocal/public/admin/css/bootstrap.min.css" >
{{--    <link rel="stylesheet" href="http://localhost/testlocal/public/Eshopper/css/style_chat.css" >--}}
    <!-- //bootstrap-css -->
    <!-- Custom CSS -->
    <link href="http://localhost/testlocal/public/admin/css/bootstrap-tagsinput_.css" rel='stylesheet' type='text/css' />
    <link href="http://localhost/testlocal/public/admin/css/style.css" rel='stylesheet' type='text/css' />
    <link href="http://localhost/testlocal/public/admin/css/style-responsive.css" rel="stylesheet"/>
    <!-- font CSS -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js" type="text/javascript"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css'>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.9.0/bootstrap-table.min.css'>
    <link rel='stylesheet' href='https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css'>
    <link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <!-- font-awesome icons -->
    <link rel="stylesheet" href="http://localhost/testlocal/public/admin/css/font.css" type="text/css"/>
    <link href="http://localhost/testlocal/public/admin/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="http://localhost/testlocal/public/admin/css/morris.css" type="text/css"/>
    <!-- calendar -->
    <link rel="stylesheet" href="http://localhost/testlocal/public/admin/css/monthly.css">
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/chosen/1.7.0/chosen.min.css'>
    <link rel='stylesheet' href='//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css'>
    <!-- //calendar -->
    <!-- //font-awesome icons -->
    <script src="http://localhost/testlocal/public/admin/js/jquery2.0.3.min.js"></script>
    <script src="http://localhost/testlocal/public/admin/js/raphael-min.js"></script>
    <script src="http://localhost/testlocal/public/admin/js/morris.js"></script>
    <script src="http://localhost/testlocal/public/admin/ckeditor/ckeditor.js"></script>
    <script src="http://localhost/testlocal/public/admin/ckfinder/ckfinder.js"></script>
</head>
<body>
<section id="container">
    <!--header start-->
    <header class="header fixed-top clearfix" style="margin-top: -6px; ">
        <!--logo start-->
        <div class="brand">
            <a href="#" class="logo">
                VISITORS
            </a>
            <div class="sidebar-toggle-box">
                <div class="fa fa-bars"></div>
            </div>
        </div>
        <!--logo end-->
        <div class="top-nav clearfix">
            <!--search & user info start-->
            <ul class="nav pull-right top-menu">
                <li>
                    <input type="text" class="form-control search" placeholder=" Search">
                </li>
                <!-- user login dropdown start-->
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <img alt="" src="http://localhost/testlocal/public/admin/images/2.png">
                        <span class="username">@if(Session::get('id_admin')==1) Admin @else {{Session::get('admin_name')}}@endif</span>
                        <b class="caret"></b>
                    </a>
                <ul class="dropdown-menu extended logout">
                    {{--   <li><a href="@if(Session::get('username')!=null) http://localhost/testlocal/admin/shop/info @endif"><i class=" fa fa-suitcase"></i>Thông tin</a></li>
                   <li><a href="http://localhost/testlocal/admin/editpassword"><i class="fa fa-cog"></i> Edit password</a></li>
                   <li><a href="#"><i class="fa fa-cog"></i> SettingsSettings</a></li>--}}
                        <li><a href="http://localhost/testlocal/logout"><i class="fa fa-key"></i> Log Out</a></li>
                    </ul>
                </li>
                <!-- user login dropdown end -->
            </ul>
            <!--search & user info end-->
        </div>
    </header>
    <!--header end-->
    <!--sidebar start-->
    <aside>
        <div id="sidebar" class="nav-collapse">
            <!-- sidebar menu start-->
            <div class="leftside-navigation">
                <ul class="sidebar-menu demo" id="nav-accordion">
                    <li>
                        <a class="active" href="http://localhost/testlocal/admin/dashboard">
                            <i class="fa fa-dashboard"></i>
                            <span>Tổng quan</span>
                        </a>
                    </li>
                    <li class="sub-menu">
                        <a href="javascript:;">
                            <i class="fa fa-book"></i>
                            <span>Hotel</span>
                        </a>
                        <ul class="sub">
                            <li><a href="http://localhost/testlocal/admin/hotel/add">Thêm</a></li>
                            <li><a href="http://localhost/testlocal/admin/hotel/list">Danh sách</a></li>
                        </ul>
                    </li>

                </ul>
            </div>
            <!-- sidebar menu end-->
        </div>
    </aside>
    <!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            @yield('content')
        </section>
        <!-- footer -->
        <div class="footer">
            <div class="wthree-copyright">
                <p>© Demo CRM | Design by HuuThanh</p>
            </div>
        </div>
        <!-- / footer -->
    </section>
    <!--main content end-->
</section>
<script src="http://localhost/testlocal/public/admin/js/bootstrap.js"></script>
<script src="http://localhost/testlocal/public/admin/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="http://localhost/testlocal/public/admin/js/scripts.js"></script>
<script src="http://localhost/testlocal/public/admin/js/jquery.slimscroll.js"></script>
<script src="http://localhost/testlocal/public/admin/js/jquery.nicescroll.js"></script>
<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="http://localhost/testlocal/public/admin/js/flot-chart/excanvas.min.js')}}"></script><![endif]-->
<script src="http://localhost/testlocal/public/admin/js/jquery.scrollTo.js"></script>
<!-- morris JavaScript -->
<script>
    $(document).ready(function() {
        //BOX BUTTON SHOW AND CLOSE
        jQuery('.small-graph-box').hover(function() {
            jQuery(this).find('.box-button').fadeIn('fast');
        }, function() {
            jQuery(this).find('.box-button').fadeOut('fast');
        });
        jQuery('.small-graph-box .box-close').click(function() {
            jQuery(this).closest('.small-graph-box').fadeOut(200);
            return false;
        });

        //CHARTS
        function gd(year, day, month) {
            return new Date(year, month - 1, day).getTime();
        }

        graphArea2 = Morris.Area({
            element: 'hero-area',
            padding: 10,
            behaveLikeLine: true,
            gridEnabled: false,
            gridLineColor: '#dddddd',
            axes: true,
            resize: true,
            smooth:true,
            pointSize: 0,
            lineWidth: 0,
            fillOpacity:0.85,
            data: [
                {period: '2015 Q1', iphone: 2668, ipad: null, itouch: 2649},
                {period: '2015 Q2', iphone: 15780, ipad: 13799, itouch: 12051},
                {period: '2015 Q3', iphone: 12920, ipad: 10975, itouch: 9910},
                {period: '2015 Q4', iphone: 8770, ipad: 6600, itouch: 6695},
                {period: '2016 Q1', iphone: 10820, ipad: 10924, itouch: 12300},
                {period: '2016 Q2', iphone: 9680, ipad: 9010, itouch: 7891},
                {period: '2016 Q3', iphone: 4830, ipad: 3805, itouch: 1598},
                {period: '2016 Q4', iphone: 15083, ipad: 8977, itouch: 5185},
                {period: '2017 Q1', iphone: 10697, ipad: 4470, itouch: 2038},

            ],
            lineColors:['#eb6f6f','#926383','#eb6f6f'],
            xkey: 'period',
            redraw: true,
            ykeys: ['iphone', 'ipad', 'itouch'],
            labels: ['All Visitors', 'Returning Visitors', 'Unique Visitors'],
            pointSize: 2,
            hideHover: 'auto',
            resize: true
        });


    });
</script>
<!-- calendar -->
<script type="text/javascript" src="http://localhost/testlocal/public/admin/js/monthly.js"></script>
<script type="text/javascript">
    $(window).load( function() {
    });

</script>

<script>
 var option={
     filebrowserImageBrowseUrl:'http://localhost/DemoCrm/laravel-filemanager?type=Images',
     filebrowserImageUploadUrl:'http://localhost/DemoCrm/laravel-filemanager/upload?type=Images&_token=',
     filebrowserBrowseUrl:'http://localhost/DemoCrm/laravel-filemanager/upload?type=Files',
     filebrowserUploadUrl:'http://localhost/DemoCrm/laravel-filemanager/upload?type=Files&_token='

 };
 CKEDITOR.replace('product_content',option);
    $('#check').click(function (){
        var  address_wallet=$("#address_wallet33").val();

        var  coin_deposit=$("#coin_deposit").val();
        if(address_wallet==''){
            var error='<div class="alert alert-danger">You must enter the address DemoCrm</div>'
            $('#kq').html(error);
            return false;
        }
        if(coin_deposit==''){
            var error='<div class="alert alert-danger">You must enter the number of coins to deposit</div>'
            $('#kq').html(error);
            return false;
        }
        $.ajax({
            url : "http://localhost/DemoCrm/apigetallcoin.php?address="+address_wallet, // gửi ajax đến file result.php
            type : "get", // chọn phương thức gửi là get
            dateType:"text", // dữ liệu trả về dạng text

            beforeSend: function () {

                $("#check").addClass('none');
                $("#check2").removeClass('none');
            },
            success : function (result){
                // Sau khi gửi và kết quả trả về thành công thì gán nội dung trả về
                // đó vào thẻ div có id = result
                var tach=result.split('wallet:');
                var tach1=tach[0].split('coin: ');

                var tach2=tach1[1].split('<br />');

                if(tach2[0]!='false'){
                    var error='<div class="alert alert-success">Verified your wallet </div>';
                    $('#kq').html(error);
                    $('#address_wallet_back').html('ok');
                    $('#lll').prop('disabled', false);

                    var tach=result.split('wallet');
                    var tach1=tach[0].split('coin: ');
                    var tach2=tach1[1].split('<br />');

                }else{
                    var error='<div class="alert alert-danger">Your wallet address does not exist </div>';

                    $('#kq').html(error);
                }
            },
            complete: function () {
                $("#check").removeClass('none');
                $("#check2").addClass('none');
            },
        });
    });
</script>

</body>
</html>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script src='https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.2/raphael-min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.0/morris.min.js'></script>
<!-- //calendar -->
<script>
    $(document).ready(function (){


        chart30daysorder();
        var chart=new Morris.Bar({
            element:'myfirstchat',
            parseTime:false,
            hideHover:"auto",
            xkey:'period',
            pointStrokeColors:['black'],
            ykeys:['order','sales','profit','quantity'],
            labels:['đơn hàng','doanh số','lợi nhuận','số lương']

        });

        @if(!Session::get('admin_id'))
        var donut=Morris.Donut({
            element: 'donut',
            resize:true,
            color:[
                '#a8328e'
            ],
            data:[
                {label:"Sản phẩm", value:{{$product}}}
            ]
        });
        @endif

        $('div svg text').css({
            "font-family" : "'Roboto",

        });

        $(function (){
            $("#datepicker").datepicker({
                prevText:"Tháng trước",
                nextText:"Tháng sau",
                dateFormat: 'yy-mm-dd',
                dayNamesMin:['Thứ 2','Thứ 3','Thứ 4','Thứ 5','Thứ 6','Thứ 7','Chủ nhật'],
                duration:'slow'

            });
            $("#datepicker2").datepicker({
                prevText:"Tháng trước",
                nextText:"Tháng sau",
                dateFormat: 'yy-mm-dd' ,
                dayNamesMin:['Thứ 2','Thứ 3','Thứ 4','Thứ 5','Thứ 6','Thứ 7','Chủ nhật'],
                duration:'slow'

            });
        });
    });


</script>


<script src='https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js'></script>
<script src="http://localhost/testlocal/public/admin/dist_datatable/script.js"></script>



<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

<script src='https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js'></script>
<script src="http://localhost/testlocal/public/admin/js/bootstrap-tagsinput.js"></script>


<script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>
<script>
    $( function() {
        $( "#datepicker1" ).datepicker({
            minDate:new Date();
        });
        $( "#datepicker2" ).datepicker({
            minDate:new Date();
        });
        $( "#datepicker3" ).datepicker({
            minDate:new Date();
        });
        $( "#datepicker4" ).datepicker({
            minDate:new Date();
        });
        $( "#search_datepicker1" ).datepicker();
        $( "#search_datepicker2" ).datepicker();
    } );
</script>

<script>
    $('.js-example-basic-single').select2();
</script>

<script>
    $(document).ready(function () {

        $("#check_box_nolimit").change(function () {

            var checked = $(this).is(":checked");

            if (checked) {
                $("#min_value").prop('disabled',true);
            }else{
                $("#min_value").prop('disabled',false);
            }
        })
        $('#flexRadioDefault2_2').on('change', function() {
            $("#giamgiatheophantram").removeClass('none');
            $("#giamgiacodinh").addClass('none');
        });
        $('#flexRadioDefault1_2').on('change', function() {
            $("#giamgiatheophantram").addClass('none');
            $("#giamgiacodinh").removeClass('none');
        });

        $("#title").keyup(function () {
            var title =$(this).val();
            console.log(title);

            $.ajax({
                url:'http://localhost/testlocal/admin/hotel/customlink',
                method:"get",
                data:{title:title},
                success:function (data){
                    $("#link").val(data);

                }
            });
        });
        $('#nation').on('change', function() {

            var nation =$(this).val();
            $.ajax({
                url:'http://localhost/testlocal/admin/hotel/loadcity',
                method:"get",
                data:{nation:nation},
                success:function (data){
                    $("#result_city").html(data);

                }
            });
        });
        $("#delete_search").click(function () {
            $('.form-control').val('');
        });
        $("#xoahotel_apply").click(function () {

            $('#hotel_apply').val('');
        });


        $("#code_pro").keyup(function () {
            var today = new Date();
            var  cod_pro=$("#code_pro").val().toUpperCase();
            var date = today.getDate()+''+(today.getMonth()+1)+""+today.getFullYear();
            var data=date+" H-"+cod_pro;
            $("#code_program").val(data);
        });

        $('#type_promo').change(function () {
            var type_promo=$('#type_promo').val();

            if(type_promo==2){
                console.log(1);
                $( "#flexRadioDefault1" ).prop('checked', false);
                $( "#flexRadioDefault2" ).prop('checked', true);

            }
            if(type_promo==1){
                $("#uang2").prop('disabled',false);
                $("#partner").addClass('none');



            }
                if(type_promo==2){
                    $("#flexRadioDefault1").prop('disabled',true);

                    $("#class_service").addClass('none');

            }

            if(type_promo==2||type_promo==3||type_promo==4){
                $("#uang2").prop('disabled',true);
                $("#class_service").removeClass('none');

            }
            if(type_promo==3){
                $( "#partner" ).removeClass('none');

            }
            if(type_promo==4||type_promo==2){
                $( "#partner" ).addClass('none');


            }
            if(type_promo==4){

                $( "#flexRadioDefault2_2" ).prop('checked', true);
                $( "#flexRadioDefault1_2" ).prop('checked', false);
                $( "#giamgiatheophantram" ).removeClass('none');
                $( "#giamgiacodinh" ).addClass('none');

            }
        })

        $(function(){

            $("#uang").keyup(function(e){
                $(this).val(format($(this).val()));
            });
            $("#uang2").keyup(function(e){
                $(this).val(format($(this).val()));
            });
            $("#uang3").keyup(function(e){
                $(this).val(format($(this).val()));
            });
            $("#uang4").keyup(function(e){
                $(this).val(format($(this).val()));
            });
            $("#uang5").keyup(function(e){
                $(this).val(format($(this).val()));
            });
            $("#min_value").keyup(function(e){
                $(this).val(format($(this).val()));
            });
        });
        var format = function(num){
            var str = num.toString().replace("", ""), parts = false, output = [], i = 1, formatted = null;
            if(str.indexOf(".") > 0) {
                parts = str.split(".");
                str = parts[0];
            }
            str = str.split("").reverse();
            for(var j = 0, len = str.length; j < len; j++) {
                if(str[j] != ",") {
                    output.push(str[j]);
                    if(i%3 == 0 && j < (len - 1)) {
                        output.push(",");
                    }
                    i++;
                }
            }
            formatted = output.reverse().join("");
            return("" + formatted + ((parts) ? "." + parts[1].substr(0, 2) : ""));
        };

    })
</script>
<script>
    $(document).ready(function () {
        $('#class_service input').keyup(function () {
            var  a=$('#class_service input').val();
            var tach=count(a.split(" "));

            $("#numberservice").text(a);
        })
    })
</script>
