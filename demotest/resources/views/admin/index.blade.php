@extends('admin.master')
@section('content')
    <center><h3>Thống kê đơn hàng doanh số</h3></center>
    <form autocomplete="off">
        <div class="col-md-2">
            <p>Từ ngày: <input type="text" id="datepicker" class="form-control"></p>

        </div>
        <div class="col-md-2">
            <p>Đến ngày: <input type="text" id="datepicker2" class="form-control"></p>
        </div>

        <div class="col-md-2" style="margin-top: 22px;">
            <p><a style="cursor: pointer" id="btn-dashboard-filter" class="btn btn-primary btn-sm" >Lọc kết quả</a></p>
        </div>
        <div class="col-md-2" style="float:right;">
            <p>Lọc theo:
                <select class="dashboard-filter form-control">
                    <option>--- Chọn ---</option>
                    <option value="7ngay">7 ngày</option>
                    <option value="thangtruoc">Tháng trước</option>
                    <option value="thangnay">Tháng này</option>
                    <option value="365ngayqua">1 năm</option>
                </select>
            </p>
        </div>
        <div class="col-md-3" style="float:right;">
            <p>Tổng doanh thu:<br><span style="font-weight: bold">{{number_format($sales_vnd,0,',','.')}} VNĐ</span>
            </p><p>Tổng lợi nhuận:<br><span style="font-weight: bold">{{number_format($profit_vnd,0,',','.')}} VNĐ</span>
            </p>
        </div>
    </form>

    <div class="clearfix"></div>

    <br>
    <div class="col-md-12">
        <div id="myfirstchat" style="height: 250px; background: white"></div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
        <br>
        <center><h3>Thống kê truy cập</h3></center>
        <br>
        <div class="col-md-12" style="background: white">
            <table class="table">
                <thead>
                <tr>
                    <th>Đang online</th>
                    <th>Tổng tháng trước</th>
                    <th>Tổng tháng này</th>
                    <th>Tổng 1 năm</th>
                    <th>Tổng truy cập</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{{$vitors_count}}</td>
                    <td>{{$vitors_of_lastMonth_count}}</td>
                    <td>{{$vitors_of_thisMonth_count}}</td>
                    <td>{{$vitors_of_year_count}}</td>
                    <td>{{$vitors_total}}</td>
                </tr>
                </tbody>
            </table>

        </div>
    </div>
    <div class="row">
        @if(!Session::get('admin_id'))
            <div class="col-md-4">
                <div id="donut"></div>
            </div>
        @endif

        @if(Session::get('admin_id'))
            <div class="col-md-4">
                <div id="donut1"></div>
            </div>
        @endif
    </div>
@endsection
<style>
    text
    {
        color: red!important;
    }
</style>
