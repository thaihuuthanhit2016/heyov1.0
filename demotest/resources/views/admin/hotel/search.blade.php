@extends('admin.master');
@section('content')
    <div class="table-agile-info">
        <div class="panel panel-default">
            <div class="panel-heading">
                Chương trình khuyến mãi
            </div>

            <div class="table-responsive">
                @include('admin.errors.error')
                <form method="post">
                    @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Mã chương trình</label>
                            <input type="text" class="form-control" name="program_code_promo"value="{{Session::get('program_code_promo')}}" id="exampleInputEmail1" placeholder="Tên danh mục">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Mã khuyến mãi</label>
                            <input type="text" class="form-control" name="code_promotion" value="{{Session::get('code_promotion')}}" id="exampleInputEmail1" placeholder="Tên danh mục">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Loại khuyến mãi</label>

                            <select class="form-control" id="type_promo" name="type_promo">
                            <option value>chọn</option>

                                <option value="1" @if(Session::get('type_promo')==1) selected @endif >Promo Code</option>
                                <option value="2" @if(Session::get('type_promo')==2) selected @endif >Unique Code</option>
                                <option value="3" @if(Session::get('type_promo')==3) selected @endif >Partner Code</option>
                                <option value="4" @if(Session::get('type_promo')==4) selected @endif > Special Campaign</option>
                            </select>                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Trạng thái</label>

                            <select class="form-control" name="status">
                                
                            <option value>chọn</option>
                                <option @if(Session::get('status')=='Thanh') selected @endif value="0">Inactive</option>
                                <option @if(Session::get('status')==1) selected @endif value="1">Active</option>
                                <option @if(Session::get('status')==2) selected @endif value="2">Deactive</option>
                                <option @if(Session::get('status')==3) selected @endif value="3">Expired</option>
                            </select>
                        </div>
                    </div>
                    <?php
                    
                    if(Session::put('status','')){
                        Session::put('status','Thanh');
                    }
                    ?>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Quốc gia</label>
                            <select class="form-control" name="nation" >
                            <option value>chọn</option>

                                @foreach($locations as $l)
                                    <option @if(Session::get('nation')==$l->id)   selected @endif value="{{$l->id}}">{{$l->country_name_vn}} - {{$l->country_name_en}}</option>
                                @endforeach                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Ngày bắt đầu</label>
                            <input type="text" class="form-control" name="date_start"  value="{{Session::get('date_start')}}" id="search_datepicker1" placeholder="Tên danh mục">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Ngày kết thúc</label>
                            <input type="text" class="form-control" name="date_end"value="{{Session::get('date_end')}}" id="search_datepicker2" placeholder="Tên danh mục">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary" name="category_product_name" id="exampleInputEmail1" value="Tìm kiếm">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <span  class="btn btn-default"style="float:right" id="delete_search">Xóa bộ lọc</span>
                        </div>
                    </div>
                </div></form>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <b style="margin:5px ;">{{Session::get('soluong')}} Khuyến mãi</b>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <a href="{{asset('admin/hotel/add')}}" style="float:right" class="btn btn-primary">Thêm mới</a>
                        </div>
                    </div>
                </div>
                <table class="table table-striped b-t b-light">
                    <thead>
                    <tr>
                        <th style="width:20px;">
                            <label class="i-checks m-b-none">
                                <input type="checkbox"><i></i>
                            </label>
                        </th>
                        <th>STT</th>
                        <th>Mã chương trình</th>
                        <th>Mã khuyến mãi</th>
                        <th>Chương trình khuyến mãi</th>
                        <th>Loại khuyến mãi</th>
                        <th>Ngày bắt đầu</th>
                        <th>Ngày kết thúc</th>
                        <th>Số lượng</th>
                        <th style="width:150px;">Hành động</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i=0;?>
                    @if($data_promo!=null)
                    @foreach($data_promo as $po)

                        <?php
                            $i++;
                            $program_code_promo=$po->program_code_promo;
                            $tach=explode(' ',$program_code_promo);
                        ?>
                    <tr>
                        <td><label class="i-checks m-b-none"><input type="checkbox" name="post[]"><i></i></label></td>
                        <td>{{$i}}</td>
                        <td>{{$tach[0]}}<br> {{$tach[1]}} </td>
                        <td>{{$po->code_promotion}}<br>@if($po->status==0) <span style="color: orange">Inactive</span> @elseif($po->status==1)<span style="color: green">Active</span> @elseif($po->status==2)<span style="color: red">Deactive</span> @else<span style="color: grey"> Expired </span>@endif
                        </td>
                        <td>
                            {{$po->title }}
                        </td>
                        <td>
                            @if($po->type_promo ==1)Promo Code @elseif($po->type_promo ==2) Unique Code @elseif($po->type_promo ==3)Partner Code @else Special Campaign @endif
                        </td>
                        <td>
                            {{$po->date_start}}<br>
                        </td>
                        <td>
                            {{$po->date_end}}<br>
                        </td>
                        <td>
                            {{$po->number_max}}
                        </td>

                        <td><a href="{{asset('admin/hotel/view/'.$po->id_promo)}}" class="active" ui-toggle-class=""><i class="fa fa-eye text-success text-active"></i></a>&nbsp;&nbsp;&nbsp;<a href="{{asset('admin/hotel/duplicate/'.$po->id_promo)}}" class="active" ui-toggle-class=""><i class="fa fa-clone text-primary"></i></a>&nbsp;&nbsp;&nbsp;<a href="{{asset('admin/hotel/edit/'.$po->id_promo)}}" class="active" ui-toggle-class=""><i class="fa fa-pencil-square-o text-warning text-active"></i></a>&nbsp;&nbsp;&nbsp; <a href="{{asset('admin/hotel/del/'.$po->id_promo)}}" onclick="confirm('Bạn có chắc xóa không?')"> <i class="fa fa-times text-danger text"></i></a>
                        </td>
                    </tr>
                    @endforeach
                    @else
                        <tr><td>No data !!!</td></tr>
                    @endif
                    </tbody>
                </table>
            </div>
            <footer class="panel-footer">
                <div class="row">
                    <div class="col-sm-7 text-right text-center-xs">
                    </div>
                </div>
            </footer>
        </div>
    </div>
@endsection
