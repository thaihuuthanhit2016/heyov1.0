@extends('admin.master');
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <form method="post"enctype="multipart/form-data">
                    <header class="panel-heading">
                        Thêm Khuyến mãi
                    </header>
                    <div class="panel-body">
                        <div class="position-center">
                            <div class="group-info-pro">
                                <header class="">
                                    Thông tin khuyến mãi
                                </header>
                                @csrf
                                <div class="form-group">
                                    @include('admin.errors.error')
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Phân hệ</label>
                                            <input type="text" readonly  name="subsystem" class="form-control" value="Hotel" placeholder="Hotel">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Loại khuyến mãi</label>
                                            <select class="form-control" id="type_promo" name="type_promo">
                                                <option value="1" @if(Session::get('type_promo')==1) selected @endif >Promo Code</option>
                                                <option value="2" @if(Session::get('type_promo')==2) selected @endif >Unique Code</option>
                                                <option value="3" @if(Session::get('type_promo')==3) selected @endif >Partner Code</option>
                                                <option value="4" @if(Session::get('type_promo')==4) selected @endif > Special Campaign</option>
                                                <?php
                                                Session::put('type_promo','')
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Tên chương trình khuyến mãi<span style="color:red;">*</span></label>
                                            <input type="text"  value="{{Session::get('title')}}" class="form-control" required onKeyUp="limitText(this.form.title,this.form.countdown,100);"onKeyDown="limitText(this.form.title,this.form.countdown,100);"  name="title" id="title" placeholder="Hotel">
                                            @if(Session::get('titleE'))                                        <br>
                                            <span class="alert alert-danger">{{Session::get('titleE')}}</span>
                                            <?php Session::put('titleE','')?>
                                            <?php
                                            Session::put('title','')
                                            ?>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Nôi dung khuyến mãi<span style="color:red;">*</span></label>
                                            <textarea rows="5" class="form-control" required name="content">{{Session::get('content')}}</textarea>
                                            <?php
                                            Session::put('content','')
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Mã khuyến mãi<span style="color:red;">*</span></label>
                                            <input type="text"  class="form-control"name="code_promotion" value="{{Session::get('code_promotion')}}" required id="code_pro"  placeholder="Hotel">
                                            @if(Session::get('code_promotionE'))                                        <br>
                                            <span class="alert alert-danger">{{Session::get('code_promotionE')}}</span>
                                            <?php Session::put('code_promotionE','')?>
                                            <?php
                                            Session::put('code_promotion','')
                                            ?>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Mã chương trình</label>
                                            <input type="text" readonly   class="form-control" name="program_code_promo" id="code_program" value="{{date('dmY')}} H-MAKHUYENMAI">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Trạng thái</label>
                                            <select class="form-control" name="status">
                                                <option @if(Session::get('status')==0) selected @endif value="0">Inactive</option>
                                                <option @if(Session::get('status')==1) selected @endif value="1">Active</option>
                                                <option @if(Session::get('status')==2) selected @endif value="2">Deactive</option>
                                                <option @if(Session::get('status')==3) selected @endif value="3">Expired</option>
                                                <?php
                                                Session::put('status','')
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Hình ảnh cover<span style="color:red;">*</span></label>
                                            <input type="file" name="images_cover"  class="form-control" required placeholder="Hotel">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Hình ảnh logo<span style="color:red;">*</span></label>
                                            <input type="file"  name="images_logo" class="form-control" required placeholder="Hotel">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="group-setting-pro">
                                <header class="">
                                    Thiết lập khuyến mãi
                                </header>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Kiểu hiển thị trên ứng dụng</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="display" value="0"  id="flexRadioDefault1" checked>
                                                    <label class="form-check-label" for="flexRadioDefault1">
                                                        Hiển thị trong danh sách
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="display" value="1"  readonly id="flexRadioDefault2" >
                                                    <label class="form-check-label" for="flexRadioDefault1">
                                                        Nhập mã để tìm
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Đường link hiển thị</label>
                                            <input type="text" name="link"  class="form-control" id="link"  placeholder="Hotel">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Ngày bắt đầu<span style="color:red;">*</span></label>
                                            <div class="form-group">
                                                <div class='input-group date' id='datetimepicker1'>
                                                    <input type='text' name="date_start" required class="form-control"value="{{Session::get('date_start')}}" />
                                                    <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            @if(Session::get('Errordate_start'))                                        <br>
                                            <span class="alert alert-danger">{{Session::get('Errordate_start')}}</span>
                                            <?php Session::put('Errordate_start','');
                                            Session::put('date_start','')?>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Ngày kết thúc<span style="color:red;">*</span></label>
                                            <div class="form-group">
                                                <div class='input-group date' id='datetimepicker2'>
                                                    <input type='text' name="date_end" required class="form-control"value="{{Session::get('date_end')}}" />
                                                    <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            @if(Session::get('Errordate_end'))
                                                <br>
                                                <span class="alert alert-danger">{{Session::get('Errordate_end')}}</span>
                                                <?php Session::put('Errordate_end','');
                                                Session::put('date_end','')?>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Số lượng phát hành<span style="color:red;">*</span></label>
                                            <input type="text"  class="form-control"name="number_max" id="uang" required  value="{{Session::get('number_max')}}"  placeholder="Hotel">
                                            <?php
                                            Session::put('number_max','')
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Giới hạn sử dụng trong ngày<span style="color:red;">*</span></label>
                                            <input type="text"  class="form-control"name="number_limit_day" id="uang2"  required value="{{Session::get('number_limit_day')}}"  placeholder="Hotel">
                                            <?php
                                            Session::put('number_limit_day','')
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Loại giảm giá</label>
                                            <div class="row">
                                                <div class="col-md-9">
                                                    <div class="col-md-6">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="id_type_discount" value="1" id="flexRadioDefault1_2"checked>
                                                            <label class="form-check-label" for="flexRadioDefault1_2">
                                                                Giảm giá cố định
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="id_type_discount" value="2" id="flexRadioDefault2_2" >
                                                            <label class="form-check-label" for="flexRadioDefault1_2">
                                                                Giảm giá theo %
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div id="giamgiacodinh">
                                                <div class="form-group" >
                                                    <label for="exampleInputEmail1">Mức tiền giảm</label>
                                                    <input type="text" name="reduced_amount"  class="form-control"id="uang4" value="{{Session::get('reduced_amount')}}"placeholder="Hotel">
                                                    <?php
                                                    Session::put('reduced_amount','')
                                                    ?>
                                                </div>
                                            </div>
                                            <div id="giamgiatheophantram" class="none">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Mức giảm theo %</label>
                                                        <input type="text" name="percentage_reduction" value="{{Session::get('percentage_reduction')}}"  class="form-control"  placeholder="Hotel">
                                                        <?php
                                                        Session::put('percentage_reduction','')
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Mức tiền giảm tối đa</label>
                                                        <input type="text"  name="reduced_amount1"  class="form-control" id="uang5" value="{{Session::get('reduced_amount1')}}"   placeholder="Hotel">
                                                        <?php
                                                        Session::put('reduced_amount1','')
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                    Session::put('code_promotion','');
                                ?>
                                <div class="clearfix"></div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group" id="class_service">

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label for="exampleInputEmail1">Nhà cung cấp dịch vụ</label>
                                                </div>
                                                <div class="col-md-6">
                                                    <p style="cursor: pointer;float:right;color:blue;" id="del_service" >Xóa hết</p>
                                                </div>
                                            </div>

                                            <select class='js-example-basic-single form-control'  required name="services[]" id="service" multiple='multiple'>
                                                <option>TVP</option>
                                                <option>EZC</option>
                                                <option>HPL</option>
                                                <option>VJ</option>
                                            </select>
                                            <?php
                                            Session::put('services','')
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group" id="class_service">

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label for="exampleInputEmail1">Đối tác thanh toán</label>
                                                </div>
                                                <div class="col-md-6">
                                                    <p style="cursor: pointer;float:right;color:blue;" id="del_payment_partner" >Xóa hết</p>
                                                </div>
                                            </div>

                                            <select class='js-example-basic-single form-control'  required name="payment_partner[]" id="payment_partner" multiple='multiple'>
                                                <?php
                                                $partner_payment=DB::table('tbl_partner_payment')->get();
                                                ?>
                                                @foreach($partner_payment as $p)
                                                <option>{{$p->name}}</option>
                                                    @endforeach
                                            </select>
                                            <?php
                                            Session::put('services','')
                                            ?>
                                        </div>
                                    </div>
                                </div>                                <div class="clearfix"></div>
                            </div>
                            <br>
                            <br>
                            <div class="group-con-pro">
                                <header class="">
                                    Điều kiện đơn hàng áp dụng
                                </header>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Quốc gia du lịch</label>
                                            <select class="form-control" name="id_nation"  id="nation">
                                                @foreach($locations as $l)
                                                    <option @if(Session::get('id_nation')==$l->id)  selected @endif value="{{$l->id}}">{{$l->country_name_vn}} - {{$l->country_name_en}}</option>
                                                @endforeach
                                                <?php
                                                Session::put('id_nation','')
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Tỉnh/Thành phố</label>
                                            <select class='js-example-basic-single form-control' required name="id_city[]"  id="result_city"multiple='multiple'>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Ngày nhận phòng từ<span style="color:red;">*</span></label>
                                            <div class="form-group">
                                                <div class='input-group date' id='datetimepicker3'>
                                                    <input type='text' name="checkinFrom" required class="form-control"value="{{Session::get('checkinFrom')}}" />
                                                    <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                                </div>
                                            </div>
                                            @if(Session::get('checkinFromE'))
                                                <br>
                                                <span class="alert alert-danger">{{Session::get('checkinFromE')}}</span>
                                                <?php Session::put('checkinFromE','')?>
                                                <?php
                                                Session::put('checkinFrom','')
                                                ?>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Ngày nhận phòng đến<span style="color:red;">*</span></label>
                                            <div class="form-group">
                                                <div class='input-group date' id='datetimepicker4'>
                                                    <input type='text' name="checkinTo" required  class="form-control"value="{{Session::get('checkinTo')}}" />
                                                    <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                                </div>
                                            </div>
                                            @if(Session::get('ErrorcheckinTo'))
                                                <br>
                                                <span class="alert alert-danger">{{Session::get('ErrorcheckinTo')}}</span>
                                                <?php Session::put('ErrorcheckinTo','')?>
                                                <?php
                                                Session::put('checkinTo','')
                                                ?>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label for="exampleInputEmail1">Giá trị đơn hàng tối thiểu phải đạt</label>
                                                </div>
                                                <div class="col-md-6" style="float:right">
                                                    <input type="checkbox" for="exampleInputEmail1" name="check_box_nolimit"   id="check_box_nolimit" > Không giới hạn
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <input type="text"   class="form-control" name="value_order_min" value="{{Session::get('value_order_min')}}" id="min_value" placeholder="Hotel">
                                                    </div>
                                                    <?php
                                                    Session::put('value_order_min','')
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Số lượng khách tối thiểu</label>
                                            <input type="text"   class="form-control"  name="number_user_min" required value="{{Session::get('number_user_min')}}" placeholder="Hotel">
                                            <?php
                                            Session::put('number_user_min','')
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="row">
                                            <div class="col-md-6">
                                                    <label for="exampleInputEmail1">Khách sạn áp dụng</label>
                                            </div>
                                                <div class="col-md-6">
                                                    <p style="cursor: pointer;float:right;color:blue;" id="del_hotel" >xóa hết</p>
                                                </div>
                                            </div>

                                            <select class='js-example-basic-single form-control' required name="hotel_apply[]" id="hotel_apply" multiple='multiple'>
                                                <?php
                                                    $hotel=DB::table('hotels')->get();
                                                ?>
                                                @foreach($hotel as $h)
                                                <option>{{$h->hotel_name}}</option>
                                                    @endforeach
                                            </select>
                                            <?php
                                            Session::put('hotel_apply','')
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="submit"   class="btn-primary btn" value="Lưu">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </section>
        </div>
    </div>
    <link rel='stylesheet' href='https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css'>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js'></script>
    <script src='https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js'></script>
    <script>
        $('#del_payment_partner').click(function () {
            $("#payment_partner").val(null).trigger("change");

        });
        $('#del_hotel').click(function () {
            $("#hotel_apply").val(null).trigger("change");

        });
        $('#del_service').click(function () {
            $("#service").val(null).trigger("change");

        });
        $('#datetimepicker1').datetimepicker({
            format: 'DD-MM-YYYY LT',minDate:new Date()
        });
        $('#datetimepicker2').datetimepicker({
            format: 'DD-MM-YYYY LT',minDate:new Date()
        });
        $('#datetimepicker3').datetimepicker({
            format: 'DD-MM-YYYY LT',minDate:new Date()

        });
        $('#datetimepicker4').datetimepicker({
            format: 'DD-MM-YYYY LT',
            minDate:new Date()

        });

        const query = document.querySelector.bind(document);

        const removeComma = string => string.slice(0, string.length - 1).trim();



        const isInvalid = stringInput => {
            const inputs = Array.from(query('.tags').children).map(input => input.firstElementChild.textContent);

            return !/^[A-Za-z0-9]{3,}/.test(stringInput) ||
                inputs.some(name => name === removeComma(stringInput)) ||
                query('.tags').children.length >= 10;
        };



        function modifyTags(e) {
            if (e.key === ',') {

                if (isInvalid(e.target.value)) {
                    e.target.value = '';
                    return;
                }

                addTag(e.target.value);
                e.target.value = '';

            }

            if (e.key === 'Backspace' && !e.target.value.length) {
                deleteTag(null, query('.tags').children.length - 1);
            }

            query('.tags-count').textContent = `${1 + query('.tags').children.length}`;
        }



        function addTag(textValue) {
            const tag = document.createElement('div'),
                tagName = document.createElement('label'),
                remove = document.createElement('span');

            tagName.setAttribute('class', 'tag-name');
            tagName.textContent = removeComma(textValue);

            remove.setAttribute('class', 'remove');
            remove.textContent = 'X';
            remove.addEventListener('click', deleteTag);

            tag.setAttribute('class', 'tag');
            tag.appendChild(tagName);
            tag.appendChild(remove);

            query('.tags').appendChild(tag);
        }



        function deleteTag(e, i = Array.from(query('.tags').children).indexOf(e.target.parentElement)) {
            const index = query('.tags').getElementsByClassName('tag')[i];

            query('.tags').removeChild(index);
            query('.tags-count').textContent = `${10 - query('.tags').children.length}`;
        }



        function focus() {
            query('#tag').focus();
        }



        query('.input').addEventListener('click', focus);
        query('#tag').addEventListener('keyup', modifyTags);
    </script>
    <script type="text/javascript">
        function limitText(limitField, limitCount, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            } else {
                limitCount.value = limitNum - limitField.value.length;
            }
        }

    </script>
@endsection
<style>
    .group-info-pro,.group-setting-pro,.group-con-pro{
        border: 1px solid;
        padding: 10px;
        border-radius: 10px;
    }
    .none{
        display: none;}
</style>
