@extends('admin.master');
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <form method="post"enctype="multipart/form-data">
                <header class="panel-heading">
                    Cập nhật Khuyến mãi
                </header>
                <div class="panel-body">
                    <div class="position-center">
                        <div class="group-info-pro">
                            <header class="">
                                Thông tin khuyến mãi
                            </header>
                            @csrf
                            <div class="form-group">
                                @include('admin.errors.error')
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Phân hệ</label>
                                        <input type="text" readonly  name="subsystem" class="form-control" value="Hotel" placeholder="Hotel">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Loại khuyến mãi</label>
                                        <select class="form-control" id="type_promo" name="type_promo">
                                            <option value="1" @if($promo->type_promo==1) selected @endif >Promo Code</option>
                                            <option value="2" @if($promo->type_promo==2) selected @endif >Unique Code</option>
                                            <option value="3" @if($promo->type_promo==3) selected @endif >Partner Code</option>
                                            <option value="4" @if($promo->type_promo==4) selected @endif > Special Campaign</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Tên chương trình khuyến mãi</label>
                                        <input type="text"  value="{{$promo->title}}" class="form-control" required name="title" id="title" placeholder="Hotel">
                                        @if(Session::get('titleE'))                                        <br>

                                        <span class="alert alert-danger">{{Session::get('titleE')}}</span>
                                        <?php Session::put('titleE','')?>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Nôi dung khuyến mãi</label>
                                        <textarea rows="5" class="form-control" required name="content">{{$promo->content}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Mã khuyến mãi</label>
                                        <input type="text"  class="form-control"name="code_promotion" value="{{$promo->code_promotion}}" readonly id="code_pro"  placeholder="Hotel">
                                        @if(Session::get('code_promotionE'))                                        <br>

                                        <span class="alert alert-danger">{{Session::get('code_promotionE')}}</span>
                                        <?php Session::put('code_promotionE','')?>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Mã chương trình</label>
                                        <input type="text" readonly   class="form-control" name="program_code_promo" id="code_program" value="{{$promo->program_code_promo}}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Trạng thái</label>
                                        <select class="form-control" name="status">
                                            <option @if($promo->status==0) selected @endif value="0">Inactive</option>
                                            <option @if($promo->status==1) selected @endif value="1">Active</option>
                                            <option @if($promo->status==2) selected @endif value="2">Deactive</option>
                                            <option @if($promo->status==3) selected @endif value="3">Expired</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Hình ảnh cover</label>
                                        <input type="file" name="images_cover"  class="form-control"  placeholder="Hotel">
                                    </div>
                                    <div class="form-group">
                                        <img src="{{asset('upload/'.$promo->images_cover)}}" width="250px">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Hình ảnh logo</label>
                                        <input type="file"  name="images_logo" class="form-control"  placeholder="Hotel">
                                    </div>

                                    <div class="form-group">
                                        <img src="{{asset('upload/'.$promo->images_logo)}}" width="100px">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="group-setting-pro">
                            <header class="">
                                Thiết lập khuyến mãi
                            </header>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Kiểu hiển thị trên ứng dụng</label>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="display" readonly value="0" id="flexRadioDefault1" @if($promo->display==0)checked @endif>
                                                <label class="form-check-label" for="flexRadioDefault1">
                                                    Hiển thị trong danh sách
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="display" readonly value="1" id="flexRadioDefault2" @if($promo->display==1)checked @endif>
                                                <label class="form-check-label" for="flexRadioDefault1">
                                                        Nhập mã để tìm

                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Đường link hiển thị</label>
                                        <input type="text" name="link" required class="form-control" id="link" value="{{$promo->link}}"  placeholder="Hotel">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Ngày bắt đầu</label>
                                        <div class="form-group">
                                            <div class='input-group date' id='datetimepicker1'>
                                                <input type='text' name="date_start" required class="form-control"value="{{$promo->date_start}}" />
                                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                            </div>
                                        </div>



                                        <div class="clearfix"></div>
                                        @if(Session::get('Errordate_start'))                                        <br>

                                        <span class="alert alert-danger">{{Session::get('Errordate_start')}}</span>
                                            <?php Session::put('Errordate_start','')?>
                                            @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Ngày kết thúc</label>
                                        <div class="form-group">
                                            <div class='input-group date' id='datetimepicker2'>
                                                <input type='text' name="date_end" required class="form-control"value="{{$promo->date_end}}" />
                                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>

                                    @if(Session::get('Errordate_end'))

                                        <br>

                                        <span class="alert alert-danger">{{Session::get('Errordate_end')}}</span>
                                            <?php Session::put('Errordate_end','')?>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Số lượng phát hành</label>
                                        <input type="text"  class="form-control"name="number_max"  id="uang" required  value="{{$promo->number_max}}"  placeholder="Hotel">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Giới hạn sử dụng trong ngày</label>
                                        <input type="text"  class="form-control"name="number_limit_day"id="uang2" value="{{$promo->number_limit_day}}" required placeholder="Hotel">
                                    </div>
                                </div>
                            </div>
                            <?php
                             $t=DB::table('tbl_type_discount')->where('id_type_discount',$promo->id_type_discount)->first();
                            ?>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Loại giảm giá</label>
                                        <div class="row">
                                            <div class="col-md-9">
                                                <div class="col-md-6">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="id_type_discount" value="1" id="flexRadioDefault1_2" @if($t->percentage_reduction==null) checked @endif>
                                                        <label class="form-check-label" for="flexRadioDefault1_2">
                                                            Giảm giá cố định
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="id_type_discount" value="2" id="flexRadioDefault2_2" @if($t->percentage_reduction!=null) checked @endif>
                                                        <label class="form-check-label" for="flexRadioDefault1_2">
                                                            Giảm giá theo %
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div id="giamgiacodinh" @if($t->percentage_reduction!=null) class="none" @endif>
                                            <div class="form-group" >
                                                <label for="exampleInputEmail1">Mức tiền giảm</label>
                                                <input type="text" name="reduced_amount"  class="form-control" id="uang4" value="{{$t->reduced_amount}}"placeholder="Hotel">
                                            </div>
                                        </div>
                                        <div id="giamgiatheophantram" @if($t->percentage_reduction==null) class="none" @endif>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Mức giảm theo %</label>
                                                    <input type="text" name="percentage_reduction" value="{{$t->percentage_reduction}}" class="form-control"  placeholder="Hotel">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Mức tiền giảm tối đa</label>
                                                    <input type="text"  name="reduced_amount1"  class="form-control" id="uang5" value="{{$t->reduced_amount}}"   placeholder="Hotel">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group" id="class_service">

                                        <div class="row">
                                            <div class="col-md-10">
                                                <label for="exampleInputEmail1">Nhà cung cấp dịch vụ</label> <span>{{$promo->services}}</span>
                                            </div>
                                            <div class="col-md-2">
                                                <p style="cursor: pointer;float:right;color:blue;" id="del_service" >Xóa hết</p>
                                            </div>
                                        </div>
                                        <select class='js-example-basic-single form-control' id="services" name="services[]" multiple='multiple'>
                                            <option>TVP</option>
                                            <option>EZC</option>
                                            <option>HPL</option>
                                            <option>VJ</option>
                                        </select>
                                        <?php
                                        Session::put('services_lll','')
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group" id="class_service">
                                        <div class="row">
                                            <div class="col-md-10">
                                                <label for="exampleInputEmail1">Đối tác thanh toán </label><span> {{$promo->payment_partner}}</span>
                                            </div>
                                            <div class="col-md-2">
                                                <p style="cursor: pointer;float:right;color:blue;" id="del_payment_partner" >Xóa hết</p>
                                            </div>
                                        </div>

                                        <select class='js-example-basic-single form-control'   name="payment_partner[]" id="payment_partner" multiple='multiple'>
                                            <?php
                                            $partner_payment=DB::table('tbl_partner_payment')->get();
                                            ?>
                                            @foreach($partner_payment as $p)
                                                <option>{{$p->name}}</option>
                                            @endforeach
                                        </select>
                                        <?php

                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="group-con-pro">
                            <header class="">
                                Điều kiện đơn hàng áp dụng
                            </header>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Quốc gia du lịch</label>
                                        <select class="form-control" name="id_nation"  id="nation">
                                            @foreach($locations as $l)
                                            <option @if($promo->id_nation==$l->id)  selected @endif value="{{$l->id}}">{{$l->country_name_vn}} - {{$l->country_name_en}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Tỉnh/Thành phố:</label>
                                        <span >{{$promo->id_city}}</span>
                                        <?php
                                        Session::put('city',$promo->id_city);
                                        ?>

                                        <select class='js-example-basic-single form-control'  name="id_city[]"   id="result_city"multiple='multiple'>
                                            @foreach($city as $c)
                                                <option >{{$c->city_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Ngày nhận phòng từ</label>
                                        <div class="form-group">
                                            <div class='input-group date' id='datetimepicker3'>
                                                <input type='text' name="checkinFrom" required class="form-control"value="{{$promo->checkinFrom}}" />
                                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                            </div>
                                        </div>

                                        @if(Session::get('checkinFromE'))

                                            <br>

                                            <span class="alert alert-danger">{{Session::get('checkinFromE')}}</span>
                                            <?php Session::put('checkinFromE','')?>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Ngày nhận phòng đến1</label>
                                        <div class="form-group">
                                            <div class='input-group date' id='datetimepicker4'>
                                                <input type='text' name="checkinTo" required class="form-control"value="{{$promo->checkinTo}}" />
                                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                            </div>
                                        </div>

                                        @if(Session::get('checkinToE'))

                                            <br>

                                            <span class="alert alert-danger">{{Session::get('checkinToE')}}</span>
                                            <?php Session::put('checkinToE','')?>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="exampleInputEmail1">Giá trị đơn hàng tối thiểu phải đạt</label>
                                            </div>
                                            <div class="col-md-6" style="float:right">
                                                <input type="checkbox" for="exampleInputEmail1" name="check_box_nolimit" @if($promo->value_order_min==null) checked @endif  id="check_box_nolimit" > Không giới hạn
                                            </div>
                                        </div>                            <div class="row">

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <input type="text"   class="form-control" name="value_order_min" id="uang3" value="{{$promo->value_order_min}}" id='min_value' placeholder="Hotel">
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Số lượng khách tối thiểu</label>
                                        <input type="text"   class="form-control" required name="number_user_min" value="{{$promo->number_user_min}}" placeholder="Hotel">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-10">
                                                <label for="exampleInputEmail1">Khách sạn áp dụng </label><span> {{$promo->hotel_apply}}</span>
                                            </div>
                                            <div class="col-md-2">
                                                <p style="cursor: pointer;float:right;color:blue;" id="del_hotel" >xóa hết</p>
                                            </div>
                                        </div>

                                        <select class='js-example-basic-single form-control'  id="hotel_apply" name="hotel_apply[]" multiple='multiple'>
                                            <?php
                                            $hotel=DB::table('hotels')->get();
                                            ?>
                                            @foreach($hotel as $h)
                                                <option>{{$h->hotel_name}}</option>
                                            @endforeach
                                        </select>
                                        <?php
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="submit"   class="btn-primary btn" value="Cập nhật">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div></form>
            </section>
        </div>
    </div>

    <link rel='stylesheet' href='https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css'>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js'></script>
    <script src='https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js'></script>
    <script>
        $('#datetimepicker1').datetimepicker({
            format: 'DD-MM-YYYY LT',
        });
        $('#datetimepicker2').datetimepicker({
            format: 'DD-MM-YYYY LT',
        });
        $('#datetimepicker3').datetimepicker({
            format: 'DD-MM-YYYY LT',
        });
        $('#datetimepicker4').datetimepicker({
            format: 'DD-MM-YYYY LT',
        });
        $('#del_payment_partner').click(function () {
            $("#payment_partner").val(null).trigger("change");

        });
        $('#del_hotel').click(function () {
            $("#hotel_apply").val(null).trigger("change");

        });
        $('#del_service').click(function () {
            $("#service").val(null).trigger("change");

        });
    </script>
@endsection
<style>
    .group-info-pro,.group-setting-pro,.group-con-pro{
        border: 1px solid;
        padding: 10px;
        border-radius: 10px;
    }
    .none{
        display: none;}
</style>
