<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\HomeController;
use App\Http\Controllers\AdminController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/admin', [AdminController::class, 'index']);
Route::post('/admin', [AdminController::class, 'postDashboard']);
Route::get('/logout', [AdminController::class, 'getLogout']);

Route::group(['prefix' => '/admin'], function () {
    Route::group(['prefix' => '/hotel'], function () {
        Route::get('/list', [AdminController::class, 'list']);
        Route::post('/list', [AdminController::class, 'postlist']);
        Route::get('/search', [AdminController::class, 'postlist']);
        Route::get('/add', [AdminController::class, 'add']);
        Route::get('/view/{id}', [AdminController::class, 'view']);
        Route::get('/del/{id}', [AdminController::class, 'del']);
        Route::get('/edit/{id}', [AdminController::class, 'edit']);
        Route::get('/duplicate/{id}', [AdminController::class, 'duplicate']);
        Route::post('/duplicate/{id}', [AdminController::class, 'postduplicate']);
        Route::post('/add', [AdminController::class, 'postadd']);
        Route::post('/edit/{id}', [AdminController::class, 'postedit']);
        Route::get('/customlink', [AdminController::class, 'customlink']);
        Route::get('/loaddata', [AdminController::class, 'loaddata']);
        Route::get('/loadcity', [AdminController::class, 'loadcity']);
        Route::get('/delservice', [AdminController::class, 'delservice']);

    });
});
Route::post('/api/promo', [AdminController::class, 'apipromo']);

