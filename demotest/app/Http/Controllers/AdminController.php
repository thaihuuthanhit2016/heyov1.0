<?php
namespace App\Http\Controllers;
date_default_timezone_set("Asia/Bangkok");
use App\Models\Card_user;
use App\Models\City;
use App\Models\Comment;
use App\Models\Customer;
use App\Models\Product;
use App\Models\Product_shop;
use App\Models\Promotion;
use App\Models\Promotion_unique;
use App\Models\Shipper;
use App\Models\Shop;
use App\Models\Statistical;
use App\Models\Team;
use App\Models\Config;
use App\Models\Type_discount;
use App\Models\Vistors;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;
use Mail;
use App\Mail\SendMail;
use Illuminate\Support\Facades\Redirect;
use Session;
class AdminController extends Controller
{
    public function index(Request $request)
    {
        return view('admin.login');

    }

    public function apicheckpromo(Request $request)
    {
        $check = DB::table('tbl_promotion')->where('code_promotion', $request->code_promotion)
            ->first();
        if ($check != null)
        {
            return json_encode($check);

        }
        else
        {
            $data = ['message' => 'faile'];
            return json_encode($data);

        }
    }
    public function postDashboard(Request $request)
    {

        $email = $request->Email;
        $password = $request->Password;

        $result = DB::table('tbl_admin')->where('admin_email', $email)->where('admin_password', md5($password))->first();

        if ($result != null)
        {
            Session::put('admin_name', $result->admin_name);
            Session::put('admin_id', $result->id_admin);
            return redirect('admin/hotel/list');
        }
        else
        {
            Session::put('message', "Lỗi tài khoản hoặc mật khẩu chưa đúng");
            return redirect('admin');
        }

    }

    public function list(Request $request)
    {
        if (!Session::get('admin_id'))
        {
            return redirect('admin');

        }
        Session::put('title', '');
        Session::put('content', '');
        Session::put('type_promo', '');
        Session::put('code_promotion', '');
        Session::put('status', '');
        Session::put('date_start', '');
        Session::put('date_end', '');
        Session::put('number_max', '');
        Session::put('number_limit_day', '');
        Session::put('reduced_amount', '');
        Session::put('reduced_amount1', '');
        Session::put('percentage_reduction', '');
        Session::put('services', '');
        Session::put('checkinFrom', '');
        Session::put('id_nation', '');
        Session::put('checkinTo', '');
        Session::put('checkinTo', '');
        Session::put('value_order_min', '');
        Session::put('number_user_min', '');
        Session::put('hotel_apply', '');
        if (Session::get('admin_id') == 1)
        {

            $promo = DB::table('tbl_promotion')->paginate(5);
            $promoall = DB::table('tbl_promotion')->get();
            $locations = DB::table('countries')->get();
        }
        else
        {

            $promo = DB::table('tbl_promotion')->where('ud_user', Session::get('admin_id'))
                ->paginate(10);
            $promoall = DB::table('tbl_promotion')->where('ud_user', Session::get('admin_id'))
                ->get();
            $locations = DB::table('countries')->get();
        }
        Session::put('soluong', count($promoall));
        foreach ($promo as $po)
        {
            if (strtotime($po->date_end) < strtotime(date('m/d/Y')))
            {
                $promo1 = Promotion::find($po->id_promo);
                $promo1->status = 3;
                $promo1->save();
            }
        }
        return view('admin.hotel.list', compact('promo', 'promoall', 'locations'));
    }

    public function customlink(Request $request)
    {
        $title = $request->title;
        $actual_link = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $tach = explode('=', $actual_link);
        $title = ($tach[1]);
        $link = slugify($title);
        return 'https://test.heyo.group/' . $link;
    }

    public function postlist(Request $request)
    {
        Session::put('program_code_promo', $request->program_code_promo);
        Session::put('code_promotion', $request->code_promotion);
        Session::put('date_start', $request->date_start);
        Session::put('date_end', $request->date_end);
        Session::put('nation', $request->nation);
        Session::put('status', $request->status);
        Session::put('type_promo', $request->type_promo);
        $locations = DB::table('countries')->get();
        $promoall = DB::table('tbl_promotion')->get();
        if (Session::get('admin_id') == 1)
        {

            $promo = DB::table('tbl_promotion')->paginate(10);
            $promoall = DB::table('tbl_promotion')->get();
            $locations = DB::table('countries')->get();
        }
        else
        {

            $promo = DB::table('tbl_promotion')->where('ud_user', Session::get('admin_id'))
                ->paginate(10);
            $promoall = DB::table('tbl_promotion')->where('ud_user', Session::get('admin_id'))
                ->get();
            $locations = DB::table('countries')->get();
        }
        foreach ($promo as $po)
        {
            if (strtotime($po->date_end) < strtotime(date('m/d/Y')))
            {
                $promo1 = Promotion::find($po->id_promo);
                $promo1->status = 3;
                $promo1->save();
            }
        }
        $data = '';
        //7
        if ($request->program_code_promo != null && $request->code_promotion != null && $request->type_promo != null && $request->status != null && $request->nation != null && $request->date_start != null && $request->date_end != null)
        {
            $data_promo = Promotion::where('program_code_promo', $request->program_code_promo)
                ->where('code_promotion', $request->code_promotion)
                ->where('date_start', $request->date_start)
                ->where('id_nation', $request->nation)
                ->here('date_end', $request->date_end)
                ->where('type_promo', $request->type_promo)
                ->where('status', $request->status)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        } //6
        if ($request->program_code_promo != null && $request->code_promotion != null && $request->type_promo != null && $request->status != null && $request->nation != null && $request->date_start != null)
        {
            $data_promo = Promotion::where('program_code_promo', $request->program_code_promo)
                ->where('code_promotion', $request->code_promotion)
                ->where('id_nation', $request->nation)
                ->where('date_start', $request->date_start)
                ->where('type_promo', $request->type_promo)
                ->where('status', $request->status)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->date_start != null && $request->code_promotion != null && $request->type_promo != null && $request->status != null && $request->nation != null && $request->date_end != null)
        {
            $data_promo = Promotion::where('date_start', $request->date_start)
                ->where('code_promotion', $request->code_promotion)
                ->where('id_nation', $request->nation)
                ->where('date_end', $request->date_end)
                ->where('type_promo', $request->type_promo)
                ->where('status', $request->status)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->program_code_promo != null && $request->code_promotion != null && $request->type_promo != null && $request->date_start != null && $request->nation != null && $request->date_end != null)
        {
            $data_promo = Promotion::where('program_code_promo', $request->program_code_promo)
                ->where('code_promotion', $request->code_promotion)
                ->where('id_nation', $request->nation)
                ->where('date_end', $request->date_end)
                ->where('type_promo', $request->type_promo)
                ->where('date_start', $request->date_start)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->program_code_promo != null && $request->code_promotion != null && $request->type_promo != null && $request->date_start != null && $request->status != null && $request->date_end != null)
        {
            $data_promo = Promotion::where('program_code_promo', $request->program_code_promo)
                ->where('code_promotion', $request->code_promotion)
                ->where('status', $request->status)
                ->where('date_end', $request->date_end)
                ->where('type_promo', $request->type_promo)
                ->where('date_start', $request->date_start)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->program_code_promo != null && $request->code_promotion != null && $request->status != null && $request->nation != null && $request->date_start != null && $request->date_end != null)
        {
            $data_promo = Promotion::where('program_code_promo', $request->program_code_promo)
                ->where('code_promotion', $request->code_promotion)
                ->where('status', $request->status)
                ->where('date_end', $request->date_end)
                ->where('id_nation', $request->nation)
                ->where('date_start', $request->date_start)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->program_code_promo != null && $request->type_promo != null && $request->nation != null && $request->date_start != null && $request->status != null && $request->date_end != null)
        {
            $data_promo = Promotion::where('program_code_promo', $request->program_code_promo)
                ->where('type_promo', $request->type_promo)
                ->where('status', $request->status)
                ->where('date_end', $request->date_end)
                ->where('id_nation', $request->nation)
                ->where('date_start', $request->date_start)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->code_promotion != null && $request->type_promo != null && $request->nation != null && $request->date_start != null && $request->status != null && $request->date_end != null)
        {
            $data_promo = Promotion::where('program_code_promo', $request->program_code_promo)
                ->where('type_promo', $request->type_promo)
                ->where('status', $request->status)
                ->where('date_end', $request->date_end)
                ->where('id_nation', $request->nation)
                ->where('date_start', $request->date_start)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        //5
        if ($request->program_code_promo != null && $request->code_promotion != null && $request->type_promo != null && $request->status != null && $request->nation != null)
        {
            $data_promo = Promotion::where('program_code_promo', $request->program_code_promo)
                ->where('code_promotion', $request->code_promotion)
                ->where('id_nation', $request->nation)
                ->wwhere('type_promo', $request->type_promo)
                ->where('status', $request->status)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->program_code_promo != null && $request->code_promotion != null && $request->type_promo != null && $request->status != null && $request->date_start != null)
        {
            $data_promo = Promotion::where('program_code_promo', $request->program_code_promo)
                ->where('code_promotion', $request->code_promotion)
                ->where('date_start', $request->date_start)
                ->where('type_promo', $request->type_promo)
                ->where('status', $request->status)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->program_code_promo != null && $request->code_promotion != null && $request->type_promo != null && $request->status != null && $request->date_end != null)
        {
            $data_promo = Promotion::where('program_code_promo', $request->program_code_promo)
                ->where('code_promotion', $request->code_promotion)
                ->where('date_end', $request->date_end)
                ->where('type_promo', $request->type_promo)
                ->where('status', $request->status)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->program_code_promo != null && $request->code_promotion != null && $request->type_promo != null && $request->nation != null && $request->date_end != null)
        {
            $data_promo = Promotion::where('program_code_promo', $request->program_code_promo)
                ->where('code_promotion', $request->code_promotion)
                ->where('date_end', $request->date_end)
                ->where('type_promo', $request->type_promo)
                ->where('id_nation', $request->nation)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->program_code_promo != null && $request->code_promotion != null && $request->type_promo != null && $request->nation != null && $request->date_start != null)
        {
            $data_promo = Promotion::where('program_code_promo', $request->program_code_promo)
                ->where('code_promotion', $request->code_promotion)
                ->where('date_start', $request->date_start)
                ->where('type_promo', $request->type_promo)
                ->where('id_nation', $request->nation)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->program_code_promo != null && $request->type_promo != null && $request->status != null && $request->nation != null && $request->date_start != null)
        {
            $data_promo = Promotion::where('program_code_promo', $request->program_code_promo)
                ->where('code_promotion', $request->code_promotion)
                ->where('date_start', $request->date_start)
                ->where('type_promo', $request->type_promo)
                ->where('id_nation', $request->nation)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->program_code_promo != null && $request->type_promo != null && $request->status != null && $request->nation != null && $request->date_end != null)
        {
            $data_promo = Promotion::where('program_code_promo', $request->program_code_promo)
                ->where('code_promotion', $request->code_promotion)
                ->where('date_end', $request->date_end)
                ->where('type_promo', $request->type_promo)
                ->where('id_nation', $request->nation)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->program_code_promo != null && $request->date_start != null && $request->status != null && $request->nation != null && $request->date_end != null)
        {
            $data_promo = Promotion::where('program_code_promo', $request->program_code_promo)
                ->where('code_promotion', $request->code_promotion)
                ->where('date_end', $request->date_end)
                ->where('date_start', $request->date_start)
                ->where('id_nation', $request->nation)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        //4
        if ($request->program_code_promo != null && $request->code_promotion != null && $request->type_promo != null && $request->status != null)
        {
            $data_promo = Promotion::where('program_code_promo', $request->program_code_promo)
                ->where('code_promotion', $request->code_promotion)
                ->where('type_promo', $request->type_promo)
                ->where('status', $request->status)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->program_code_promo != null && $request->code_promotion != null && $request->type_promo != null && $request->nation != null)
        {
            $data_promo = Promotion::where('program_code_promo', $request->program_code_promo)
                ->where('code_promotion', $request->code_promotion)
                ->where('type_promo', $request->type_promo)
                ->where('id_nation', $request->nation)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->program_code_promo != null && $request->code_promotion != null && $request->type_promo != null && $request->date_start != null)
        {
            $data_promo = Promotion::where('program_code_promo', $request->program_code_promo)
                ->where('code_promotion', $request->code_promotion)
                ->where('type_promo', $request->type_promo)
                ->where('date_start', $request->date_start)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->program_code_promo != null && $request->code_promotion != null && $request->type_promo != null && $request->date_end != null)
        {
            $data_promo = Promotion::where('program_code_promo', $request->program_code_promo)
                ->where('code_promotion', $request->code_promotion)
                ->where('type_promo', $request->type_promo)
                ->where('date_end', $request->date_end)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->program_code_promo != null && $request->status != null && $request->type_promo != null && $request->date_end != null)
        {
            $data_promo = Promotion::where('program_code_promo', $request->program_code_promo)
                ->where('status', $request->status)
                ->where('type_promo', $request->type_promo)
                ->where('date_end', $request->date_end)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->program_code_promo != null && $request->status != null && $request->type_promo != null && $request->date_start != null)
        {
            $data_promo = Promotion::where('program_code_promo', $request->program_code_promo)
                ->where('code_promotion', $request->code_promotion)
                ->where('type_promo', $request->type_promo)
                ->where('date_start', $request->date_start)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->program_code_promo != null && $request->status != null && $request->type_promo != null && $request->nation != null)
        {
            $data_promo = Promotion::where('program_code_promo', $request->program_code_promo)
                ->where('code_promotion', $request->code_promotion)
                ->where('type_promo', $request->type_promo)
                ->where('date_start', $request->nation)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->program_code_promo != null && $request->status != null && $request->date_start != null && $request->nation != null)
        {
            $data_promo = Promotion::where('program_code_promo', $request->program_code_promo)
                ->where('code_promotion', $request->code_promotion)
                ->where('date_start', $request->date_start)
                ->where('date_start', $request->nation)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->program_code_promo != null && $request->status != null && $request->date_end != null && $request->nation != null)
        {
            $data_promo = Promotion::where('program_code_promo', $request->program_code_promo)
                ->where('code_promotion', $request->code_promotion)
                ->where('date_start', $request->date_start)
                ->where('date_end', $request->date_end)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        //3
        if ($request->program_code_promo != null && $request->code_promotion != null && $request->type_promo != null)
        {
            $data_promo = Promotion::where('program_code_promo', $request->program_code_promo)
                ->where('code_promotion', $request->code_promotion)
                ->where('type_promo', $request->type_promo)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->program_code_promo != null && $request->code_promotion != null && $request->status != null)
        {
            $data_promo = Promotion::where('program_code_promo', $request->program_code_promo)
                ->where('code_promotion', $request->code_promotion)
                ->where('status', $request->status)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->program_code_promo != null && $request->code_promotion != null && $request->nation != null)
        {
            $data_promo = Promotion::where('program_code_promo', $request->program_code_promo)
                ->where('code_promotion', $request->code_promotion)
                ->where('id_nation', $request->nation)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->program_code_promo != null && $request->code_promotion != null && $request->date_start != null)
        {
            $data_promo = Promotion::where('program_code_promo', $request->program_code_promo)
                ->where('code_promotion', $request->code_promotion)
                ->where('date_start', $request->date_start)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->program_code_promo != null && $request->code_promotion != null && $request->date_end != null)
        {
            $data_promo = Promotion::where('program_code_promo', $request->program_code_promo)
                ->where('code_promotion', $request->code_promotion)
                ->where('date_end', $request->date_end)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->program_code_promo != null && $request->type_promo != null && $request->status != null)
        {
            $data_promo = Promotion::where('program_code_promo', $request->program_code_promo)
                ->where('type_promo', $request->type_promo)
                ->where('status', $request->status)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->program_code_promo != null && $request->type_promo != null && $request->nation != null)
        {
            $data_promo = Promotion::where('program_code_promo', $request->program_code_promo)
                ->where('type_promo', $request->type_promo)
                ->where('id_nation', $request->nation)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->program_code_promo != null && $request->type_promo != null && $request->date_start != null)
        {
            $data_promo = Promotion::where('program_code_promo', $request->program_code_promo)
                ->where('type_promo', $request->type_promo)
                ->where('date_start', $request->date_start)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->program_code_promo != null && $request->type_promo != null && $request->date_end != null)
        {
            $data_promo = Promotion::where('program_code_promo', $request->program_code_promo)
                ->where('type_promo', $request->type_promo)
                ->where('date_end', $request->date_end)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->program_code_promo != null && $request->status != null && $request->nation != null)
        {
            $data_promo = Promotion::where('program_code_promo', $request->program_code_promo)
                ->where('status', $request->status)
                ->where('id_nation', $request->nation)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->program_code_promo != null && $request->status != null && $request->date_start != null)
        {
            $data_promo = Promotion::where('program_code_promo', $request->program_code_promo)
                ->where('status', $request->status)
                ->where('date_start', $request->date_start)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->program_code_promo != null && $request->status != null && $request->date_end != null)
        {
            $data_promo = Promotion::where('program_code_promo', $request->program_code_promo)
                ->where('status', $request->status)
                ->where('date_end', $request->date_end)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->program_code_promo != null && $request->nation != null && $request->date_start != null)
        {
            $data_promo = Promotion::where('program_code_promo', $request->program_code_promo)
                ->where('id_nation', $request->nation)
                ->where('date_start', $request->date_start)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->program_code_promo != null && $request->nation != null && $request->date_end != null)
        {
            $data_promo = Promotion::where('program_code_promo', $request->program_code_promo)
                ->where('id_nation', $request->nation)
                ->where('date_start', $request->date_end)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        //2
        if ($request->program_code_promo != null && $request->code_promotion != null)
        {
            $data_promo = Promotion::where('program_code_promo', $request->program_code_promo)
                ->where('code_promotion', $request->code_promotion)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->program_code_promo != null && $request->type_promo != null)
        {
            $data_promo = Promotion::where('program_code_promo', $request->program_code_promo)
                ->where('type_promo', $request->type_promo)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->program_code_promo != null && $request->status != null)
        {

            $data_promo = Promotion::where('program_code_promo', $request->program_code_promo)
                ->where('status', $request->type_promo)
                ->get();
            dd($data_promo);
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->program_code_promo != null && $request->nation != null)
        {
            $data_promo = Promotion::where('program_code_promo', $request->program_code_promo)
                ->where('id_nation', $request->nation)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->program_code_promo != null && $request->date_start != null)
        {
            $data_promo = Promotion::where('program_code_promo', $request->program_code_promo)
                ->where('date_start', $request->date_start)
                ->get();
            Session::put('soluong', count($data_promo));
        }
        if ($request->program_code_promo != null && $request->date_end != null)
        {
            $data_promo = Promotion::where('program_code_promo', $request->program_code_promo)
                ->where('date_end', $request->date_end)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->code_promotion != null && $request->type_promo != null)
        {
            $data_promo = Promotion::where('code_promotion', $request->code_promotion)
                ->where('type_promo', $request->type_promo)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->code_promotion != null && $request->status != null)
        {
            $data_promo = Promotion::where('code_promotion', $request->code_promotion)
                ->where('status', $request->status)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->code_promotion != null && $request->nation != null)
        {
            $data_promo = Promotion::where('code_promotion', $request->code_promotion)
                ->where('id_nation', $request->nation)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->code_promotion != null && $request->date_start != null)
        {
            $data_promo = Promotion::where('code_promotion', $request->code_promotion)
                ->where('date_start', $request->date_start)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->code_promotion != null && $request->date_end != null)
        {
            $data_promo = Promotion::where('code_promotion', $request->code_promotion)
                ->where('date_start', $request->date_end)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->type_promo != null && $request->status != null)
        {
            $data_promo = Promotion::where('type_promo', $request->type_promo)
                ->where('status', $request->status)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->type_promo != null && $request->nation != null)
        {
            $data_promo = Promotion::where('type_promo', $request->type_promo)
                ->where('id_nation', $request->nation)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->type_promo != null && $request->date_start != null)
        {
            $data_promo = Promotion::where('type_promo', $request->type_promo)
                ->where('date_start', $request->date_start)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->type_promo != null && $request->date_end != null)
        {
            $data_promo = Promotion::where('type_promo', $request->type_promo)
                ->where('date_end', $request->date_end)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->status != null && $request->nation != null)
        {
            $data_promo = Promotion::where('status', $request->status)
                ->where('id_nation', $request->nation)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->status != null && $request->date_start != null)
        {
            $data_promo = Promotion::where('status', $request->status)
                ->where('date_start', $request->date_start)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->status != null && $request->date_end != null)
        {
            $data_promo = Promotion::where('status', $request->status)
                ->where('date_end', $request->date_end)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->nation != null && $request->date_end != null)
        {
            $data_promo = Promotion::where('id_nation', $request->nation)
                ->where('date_end', $request->date_end)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->nation != null && $request->date_start != null)
        {
            $data_promo = Promotion::where('id_nation', $request->nation)
                ->where('date_end', $request->date_start)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->date_end != null && $request->date_start != null)
        {
            $data_promo = Promotion::where('date_end', $request->date_end)
                ->where('date_end', $request->date_start)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        //1
        if ($request->program_code_promo != null)
        {
            $data_promo = Promotion::where('program_code_promo', $request->program_code_promo)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->code_promotion != null)
        {
            $data_promo = Promotion::where('code_promotion', $request->code_promotion)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->type_promo != null)
        {
            $data_promo = Promotion::where('type_promo', $request->type_promo)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->status != null)
        {
            $data_promo = Promotion::where('status', $request->status)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->nation != null)
        {
            $data_promo = Promotion::where('id_nation', $request->nation)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->date_start != null)
        {
            $data_promo = Promotion::where('date_start', '>=', $request->date_start)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        if ($request->date_end != null)
        {
            $data_promo = Promotion::where('date_end', '<=', $request->date_end)
                ->get();
            Session::put('soluong', count($data_promo));
            return view('admin.hotel.search', compact('data_promo', 'locations', 'promoall'));
        }
        //2

    }

    public function add(Request $request)
    {
        if (!Session::get('admin_id'))
        {
            return redirect('admin');

        }
        $locations = DB::table('countries')->get();
        return view('admin.hotel.add', compact('locations'));
    }

    public function edit($id)
    {
        if (!Session::get('admin_id'))
        {
            return redirect('admin');

        }
        $locations = DB::table('countries')->get();
        $promo = Promotion::find($id);
        $countries = DB::table('countries')->where('id', $promo->id_nation)
            ->first();
        $city = DB::table('cities')->where('country_id', $countries->id)
            ->get();

        Session::put('images_cover', $promo->images_cover);
        Session::put('images_logo', $promo->images_logo);
        Session::put('id_city', $promo->id_city);
        Session::put('services', $promo->services);

        Session::put('hotel_apply', $promo->hotel_apply);
        Session::put('payment_partner', $promo->payment_partner);


        return view('admin.hotel.edit', compact('locations', 'promo', 'city'));
    }

    public function duplicate($id)
    {
        if (!Session::get('admin_id'))
        {
            return redirect('admin');

        }
        $locations = DB::table('countries')->get();
        $promo = Promotion::find($id);
        $countries = DB::table('countries')->where('id', $promo->id_nation)
            ->first();
        $city = DB::table('cities')->where('country_id', $countries->id)
            ->get();
        Session::put('images_cover', $promo->images_cover);
        Session::put('images_logo', $promo->images_logo);
        Session::put('id_city', $promo->id_city);
        Session::put('services', $promo->services);
        Session::put('hotel_apply', $promo->hotel_apply);
        Session::put('payment_partner', $promo->payment_partner);

        return view('admin.hotel.duplicate', compact('locations', 'promo', 'city'));
    }

    public function view($id, Request $request)
    {
        if (!Session::get('admin_id'))
        {
            return redirect('admin');

        }
        $promo = Promotion::find($id);
        return view('admin.hotel.view', compact('promo'));
    }

    public function del($id, Request $request)
    {
        if (!Session::get('admin_id'))
        {
            return redirect('admin');

        }
        Promotion::destroy($id);
        return \redirect('admin/hotel/list');
    }

    public function getLogout()
    {

        Session::put('admin_name', '');
        Session::put('admin_id', '');
        Session::put('shipper', '');
        Session::put('username', '');
        Session::put('id_shop_user', '');
        return redirect('admin');

    }
    public function apipromo(Request $request)
    {
        $getHeaders = apache_request_headers();

        $_token = ($getHeaders['Key']);
        if ($_token == '4Fguozdx7eFzUKtXO1AsBzoe2re8jSGlH7DcOXG0')
        {
            $promoall = DB::table('tbl_promotion')->get();

            return json_encode($promoall);
        }
        else
        {
            return 'Access denied ';
        }

    }

    public function postadd(Request $request)
    {
        Session::put('title', $request->title);
        Session::put('content', $request->content);
        Session::put('type_promo', $request->type_promo);
        Session::put('code_promotion', $request->code_promotion);
        Session::put('status', $request->status);
        Session::put('date_start', $request->date_start);
        Session::put('date_end', $request->date_end);
        Session::put('number_max', $request->number_max);
        Session::put('number_limit_day', $request->number_limit_day);
        Session::put('reduced_amount', $request->reduced_amount);
        Session::put('reduced_amount1', $request->reduced_amount1);
        Session::put('percentage_reduction', $request->percentage_reduction);
        Session::put('services', $request->services);
        Session::put('checkinFrom', $request->checkinFrom);
        Session::put('id_nation', $request->id_nation);
        Session::put('checkinTo', $request->checkinTo);
        Session::put('checkinTo', $request->checkinTo);
        Session::put('value_order_min', $request->value_order_min);
        Session::put('number_user_min', $request->number_user_min);
        Session::put('hotel_apply', $request->hotel_apply);

        if (strtotime($request->date_start) < strtotime(date('d-m-Y h:i')))
        {

            return back()
                ->with('Errordate_start', 'Lỗi ngày bắt đầu nhỏ hơn ngày hiện tại');
        }

        if (strtotime($request->date_end) < strtotime(date('d-m-Y h:i')))
        {

            return back()
                ->with('Errordate_end', 'Lỗi ngày Kết thúc nhỏ hơn ngày hiện tại');
        }

        if (strtotime($request->checkinFrom) < strtotime(date('d-m-Y h:i')))
        {

            return back()
                ->with('checkinFromE', 'Lỗi ngày nhận phòng từ nhỏ hơn ngày bắt đầu');
        }

        if (strtotime($request->checkinTo) < strtotime(date('d-m-Y h:i')))
        {

            return back()
                ->with('ErrorcheckinTo', 'Lỗi ngày ngày nhận phòng đến nhỏ hơn ngày hiện tại');
        }

        if (strtotime($request->date_start) > strtotime($request->date_end))
        {
            return back()
                ->with('Errordate_end', 'Lỗi ngày kết thúc nhỏ hơn ngày bắt đầu');
        }

        if (strtotime($request->date_start) > strtotime($request->checkinFrom))
        {

            return back()
                ->with('ErrorcheckinTo', 'Lỗi ngày nhận phòng từ nhỏ hơn ngày bắt đầu khuyến mãi');
        }

        if (strtotime($request->date_end) < strtotime($request->checkinTo))
        {

            return back()
                ->with('ErrorcheckinTo', 'Lỗi ngày nhận phòng đến lớn hơn ngày kết thúc khuyến mãi');
        }
        if (strtotime($request->checkinFrom) > strtotime($request->checkinTo))
        {
            return back()
                ->with('checkinToE', 'Lỗi ngày nhận phòng từ lớn hơn ngày ngày nhận phòng đến ');
        }

        $data = Promotion::where('title', $request->title)
            ->first();
        if ($data != null)
        {
            return back()->with('titleE', 'Tên chương trình khuyến mãi đã tồn tại');
        }
        $data = Promotion::where('code_promotion', $request->code_promotion)
            ->first();
        if ($data != null)
        {
            return back()->with('code_promotionE', 'Mã khuyến mãi đã tồn tại');
        }
        $type_discount = new Type_discount();
        if ($request->reduced_amount == null)
        {
            $type_discount->reduced_amount = $request->reduced_amount1;
        }
        else
        {
            $type_discount->reduced_amount = $request->reduced_amount;
        }
        $type_discount->percentage_reduction = $request->percentage_reduction;
        $type_discount->save();

        $promo_hotel = new Promotion();
        $promo_hotel->title = $request->title;
        $promo_hotel->content = $request->content;
        $promo_hotel->code_promotion = $request->code_promotion;
        if (strtotime($request->date_start) == strtotime(date('d-m-Y h:i')))
        {
            $promo_hotel->status = 1;
        }
        $promo_hotel->status = $request->status;

        $promo_hotel->program_code_promo = $request->program_code_promo;

        if ($request->hasFile('images_cover'))
        {
            $file = $request
                ->images_cover
                ->getClientOriginalName();
            $day = date('Y-m-d');
            $file_custom = $day . '_' . $file;
            $promo_hotel->images_cover = $file_custom;
            $request
                ->images_cover
                ->move('upload/', $file_custom);
        }
        if ($request->hasFile('images_logo'))
        {
            $file = $request
                ->images_logo
                ->getClientOriginalName();
            $day = date('Y-m-d');
            $file_custom = $day . '_' . $file;
            $promo_hotel->images_logo = $file_custom;
            $request
                ->images_logo
                ->move('upload/', $file_custom);
        }
        if ($request->subsystem == "Hotel")
        {
            $promo_hotel->subsystem = 0;
        }
        else
        {
            $promo_hotel->subsystem = 1;
        }
        $promo_hotel->type_promo = $request->type_promo;
        if ($request->type_promo == 1)
        {
            $promo_hotel->display = 0;
        }
        else
        {
            $promo_hotel->display = 1;
        }

        $promo_hotel->link = $request->link;
        $promo_hotel->date_start = $request->date_start;
        $promo_hotel->date_end = $request->date_end;
        $promo_hotel->number_max = $request->number_max;
        $promo_hotel->number_limit_day = $request->number_limit_day;
        $promo_hotel->id_type_discount = $type_discount->id_type_discount;
        $promo_hotel->id_nation = $request->id_nation;
        $d_city = $request->id_city;
        if ($d_city != null)
        {
            $count = count($request->id_city);
            $data_city = '';
            for ($i = 0;$i < $count;$i++)
            {
                $data_city .= $d_city[$i] . ', ';
            }

            $promo_hotel->id_city = $data_city;
        }
        else
        {
            $promo_hotel->id_city = Session::get('id_city');

        }
        $services = $request->services;
        if ($services != null)
        {
            $count = count($request->services);
            $data_services = '';
            for ($i = 0;$i < $count;$i++)
            {
                $data_services .= $services[$i] . ', ';
            }

            $promo_hotel->services = $data_services;
        }
        else
        {
            $promo_hotel->services = Session::get('services');

        }
        $hotel_apply = $request->hotel_apply;
        if ($hotel_apply != null)
        {

            $count = count($request->hotel_apply);
            $data_hotel_apply = '';
            for ($i = 0;$i < $count;$i++)
            {
                $data_hotel_apply .= $hotel_apply[$i] . ', ';
            }
            $promo_hotel->hotel_apply = $data_hotel_apply;

        }
        else
        {
            $promo_hotel->hotel_apply = Session::get('hotel_apply');

        }
        $payment_partner = $request->payment_partner;
        if ($payment_partner != null)
        {

            $count = count($request->payment_partner);
            $data_payment_partner = '';
            for ($i = 0;$i < $count;$i++)
            {
                $data_payment_partner .= $payment_partner[$i] . ', ';
            }
            $promo_hotel->payment_partner = $data_payment_partner;

        }
        else
        {
            $promo_hotel->payment_partner = Session::get('payment_partner');

        }
        $promo_hotel->checkinFrom = $request->checkinFrom;
        $promo_hotel->checkinTo = $request->checkinTo;
        if ($request->value_order_min == null)
        {

            $promo_hotel->value_order_min = 0;
        }
        else
        {

            $promo_hotel->value_order_min = $request->value_order_min;
        }
        $promo_hotel->number_user_min = $request->number_user_min;
        $promo_hotel->ud_user = Session::get('admin_id');
        $promo_hotel->create_day = date('d-m-Y');

        $promo_hotel->save();
        if ($request->type_promo == 2)
        {
            for ($i = 0;$i < $request->number_max;$i++)
            {
                $promotion_unique = new Promotion_unique();
                $promotion_unique->id_promo = $promo_hotel->id_promo;
                $promotion_unique->code = $request->code_promotion . $i;
                $promotion_unique->save();
            }
        }
        return \redirect('admin/hotel/list');
    }

    public function postduplicate(Request $request)
    {

        Session::put('title', $request->title);
        Session::put('content', $request->content);
        Session::put('type_promo', $request->type_promo);
        Session::put('code_promotion', $request->code_promotion);
        Session::put('status', $request->status);
        Session::put('date_start', $request->date_start);
        Session::put('date_end', $request->date_end);
        Session::put('number_max', $request->number_max);
        Session::put('number_limit_day', $request->number_limit_day);
        Session::put('reduced_amount', $request->reduced_amount);
        Session::put('reduced_amount1', $request->reduced_amount1);
        Session::put('percentage_reduction', $request->percentage_reduction);
        Session::put('services', $request->services);
        Session::put('checkinFrom', $request->checkinFrom);
        Session::put('id_nation', $request->id_nation);
        Session::put('checkinTo', $request->checkinTo);
        Session::put('checkinTo', $request->checkinTo);
        Session::put('value_order_min', $request->value_order_min);
        Session::put('number_user_min', $request->number_user_min);
        Session::put('hotel_apply', $request->hotel_apply);

        if (strtotime($request->date_start) < strtotime(date('d-m-Y h:i')))
        {

            return back()
                ->with('Errordate_start', 'Lỗi ngày bắt đầu nhỏ hơn ngày hiện tại');
        }

        if (strtotime($request->date_end) < strtotime(date('d-m-Y h:i')))
        {

            return back()
                ->with('Errordate_end', 'Lỗi ngày Kết thúc nhỏ hơn ngày hiện tại');
        }

        if (strtotime($request->checkinFrom) < strtotime(date('d-m-Y h:i')))
        {

            return back()
                ->with('checkinFromE', 'Lỗi ngày nhận phòng từ nhỏ hơn ngày bắt đầu');
        }

        if (strtotime($request->checkinTo) < strtotime(date('d-m-Y h:i')))
        {

            return back()
                ->with('ErrorcheckinTo', 'Lỗi ngày ngày nhận phòng đến nhỏ hơn ngày hiện tại');
        }

        if (strtotime($request->date_start) > strtotime($request->date_end))
        {
            return back()
                ->with('Errordate_end', 'Lỗi ngày kết thúc nhỏ hơn ngày bắt đầu');
        }

        if (strtotime($request->date_start) > strtotime($request->checkinFrom))
        {

            return back()
                ->with('ErrorcheckinTo', 'Lỗi ngày nhận phòng từ nhỏ hơn ngày bắt đầu khuyến mãi');
        }

        if (strtotime($request->date_end) < strtotime($request->checkinTo))
        {

            return back()
                ->with('ErrorcheckinTo', 'Lỗi ngày nhận phòng đến lớn hơn ngày kết thúc khuyến mãi');
        }
        if (strtotime($request->checkinFrom) > strtotime($request->checkinTo))
        {
            return back()
                ->with('checkinToE', 'Lỗi ngày nhận phòng từ lớn hơn ngày ngày nhận phòng đến ');
        }

        $data = Promotion::where('title', $request->title)
            ->first();
        if ($data != null)
        {
            return back()->with('titleE', 'Tên chương trình khuyến mãi đã tồn tại');
        }
        $data = Promotion::where('code_promotion', $request->code_promotion)
            ->first();
        if ($data != null)
        {
            return back()->with('code_promotionE', 'Mã khuyến mãi đã tồn tại');
        }
        $type_discount = new Type_discount();
        if ($request->reduced_amount == null)
        {
            $type_discount->reduced_amount = $request->reduced_amount1;
        }
        else
        {
            $type_discount->reduced_amount = $request->reduced_amount;
        }
        $type_discount->percentage_reduction = $request->percentage_reduction;
        $type_discount->save();
        $promo_hotel = new Promotion();
        $promo_hotel->title = $request->title;
        $promo_hotel->content = $request->content;
        $promo_hotel->code_promotion = $request->code_promotion;
        if (strtotime($request->date_start) == strtotime(date('d-m-Y h:i')))
        {
            $promo_hotel->status = 1;
        }
        $promo_hotel->status = $request->status;

        $promo_hotel->program_code_promo = $request->program_code_promo;

        if ($request->hasFile('images_cover'))
        {
            $file = $request
                ->images_cover
                ->getClientOriginalName();
            $day = date('Y-m-d');
            $file_custom = $day . '_' . $file;
            $promo_hotel->images_cover = $file_custom;
            $request
                ->images_cover
                ->move('upload/', $file_custom);
        }
        else
        {
            $promo_hotel->images_cover = Session::get('images_cover');

        }
        if ($request->hasFile('images_logo'))
        {
            $file = $request
                ->images_logo
                ->getClientOriginalName();
            $day = date('Y-m-d');
            $file_custom = $day . '_' . $file;
            $promo_hotel->images_logo = $file_custom;
            $request
                ->images_logo
                ->move('upload/', $file_custom);
        }
        else
        {
            $promo_hotel->images_logo = Session::get('images_logo');

        }
        if ($request->subsystem == "Hotel")
        {
            $promo_hotel->subsystem = 0;
        }
        else
        {
            $promo_hotel->subsystem = 1;
        }
        $promo_hotel->type_promo = $request->type_promo;
        if ($request->type_promo == 1)
        {
            $promo_hotel->display = 0;
        }
        else
        {
            $promo_hotel->display = 1;
        }

        $promo_hotel->link = $request->link;
        $promo_hotel->date_start = $request->date_start;
        $promo_hotel->date_end = $request->date_end;
        $promo_hotel->number_max = $request->number_max;
        $promo_hotel->number_limit_day = $request->number_limit_day;
        $promo_hotel->id_type_discount = $type_discount->id_type_discount;
        $promo_hotel->id_nation = $request->id_nation;
        $d_city = $request->id_city;
        if ($d_city != null)
        {
            $count = count($request->id_city);
            $data_city = '';
            for ($i = 0;$i < $count;$i++)
            {
                $data_city .= $d_city[$i] . ', ';
            }

            $promo_hotel->id_city = $data_city;
        }
        else
        {
            $promo_hotel->id_city = Session::get('id_city');

        }
        $services = $request->services;
        if ($services != null)
        {
            $count = count($request->services);
            $data_services = '';
            for ($i = 0;$i < $count;$i++)
            {
                $data_services .= $services[$i] . ', ';
            }

            $promo_hotel->services = $data_services;
        }
        else
        {
            $promo_hotel->services = Session::get('services');

        }
        $hotel_apply = $request->hotel_apply;
        if ($hotel_apply != null)
        {

            $count = count($request->hotel_apply);
            $data_hotel_apply = '';
            for ($i = 0;$i < $count;$i++)
            {
                $data_hotel_apply .= $hotel_apply[$i] . ', ';
            }
            $promo_hotel->hotel_apply = $data_hotel_apply;

        }
        else
        {
            $promo_hotel->hotel_apply = Session::get('hotel_apply');

        }
        $payment_partner = $request->payment_partner;
        if ($payment_partner != null)
        {

            $count = count($request->payment_partner);
            $data_payment_partner = '';
            for ($i = 0;$i < $count;$i++)
            {
                $data_payment_partner .= $payment_partner[$i] . ', ';
            }
            $promo_hotel->payment_partner = $data_payment_partner;

        }
        else
        {
            $promo_hotel->payment_partner = Session::get('$payment_partner');

        }
        $promo_hotel->checkinFrom = $request->checkinFrom;
        $promo_hotel->checkinTo = $request->checkinTo;
        if ($request->value_order_min == null)
        {

            $promo_hotel->value_order_min = 0;
        }
        else
        {

            $promo_hotel->value_order_min = $request->value_order_min;
        }
        $promo_hotel->number_user_min = $request->number_user_min;
        $promo_hotel->ud_user = Session::get('admin_id');
        $promo_hotel->create_day = date('d-m-Y');
        $promo_hotel->save();
        return \redirect('admin/hotel/list');
    }

    public function delservice()
    {
        $html = '';
        $html = ' <select class=\'js-example-basic-single form-control\' required name="services[]" id="service" multiple=\'multiple\'>
                                                <option>TVP</option>
                                                <option>EZC</option>
                                                <option>HPL</option>
                                                <option>VJ</option>
                                            </select>';
        return $html;
    }
    public function postedit(Request $request, $id)
    {

        Session::put('title', $request->title);
        Session::put('content', $request->content);
        Session::put('type_promo', $request->type_promo);
        Session::put('code_promotion', $request->code_promotion);
        Session::put('status', $request->status);
        Session::put('date_start', $request->date_start);
        Session::put('date_end', $request->date_end);
        Session::put('number_max', $request->number_max);
        Session::put('number_limit_day', $request->number_limit_day);
        Session::put('reduced_amount', $request->reduced_amount);
        Session::put('reduced_amount1', $request->reduced_amount1);
        Session::put('percentage_reduction', $request->percentage_reduction);
        Session::put('services_lll', $request->services);
        Session::put('checkinFrom', $request->checkinFrom);
        Session::put('checkinTo', $request->checkinTo);
        Session::put('checkinTo', $request->checkinTo);
        Session::put('value_order_min', $request->value_order_min);
        Session::put('number_user_min', $request->number_user_min);
        Session::put('hotel_apply_ss', $request->hotel_apply);

        if (strtotime($request->date_start) < strtotime(date('d-m-Y h:i')))
        {

            return back()
                ->with('Errordate_start', 'Lỗi ngày bắt đầu nhỏ hơn ngày hiện tại');
        }

        if (strtotime($request->date_end) < strtotime(date('d-m-Y h:i')))
        {

            return back()
                ->with('Errordate_end', 'Lỗi ngày Kết thúc nhỏ hơn ngày hiện tại');
        }

        if (strtotime($request->checkinFrom) < strtotime(date('d-m-Y h:i')))
        {

            return back()
                ->with('checkinFromE', 'Lỗi ngày nhận phòng từ nhỏ hơn ngày bắt đầu');
        }

        if (strtotime($request->checkinTo) < strtotime(date('d-m-Y h:i')))
        {

            return back()
                ->with('ErrorcheckinTo', 'Lỗi ngày ngày nhận phòng đến nhỏ hơn ngày hiện tại');
        }

        if (strtotime($request->date_start) > strtotime($request->date_end))
        {
            return back()
                ->with('Errordate_end', 'Lỗi ngày kết thúc nhỏ hơn ngày bắt đầu');
        }

        if (strtotime($request->date_start) > strtotime($request->checkinFrom))
        {

            return back()
                ->with('ErrorcheckinTo', 'Lỗi ngày nhận phòng từ nhỏ hơn ngày bắt đầu khuyến mãi');
        }

        if (strtotime($request->date_end) < strtotime($request->checkinTo))
        {

            return back()
                ->with('ErrorcheckinTo', 'Lỗi ngày nhận phòng đến lớn hơn ngày kết thúc khuyến mãi');
        }
        if (strtotime($request->checkinFrom) > strtotime($request->checkinTo))
        {
            return back()
                ->with('checkinToE', 'Lỗi ngày nhận phòng từ lớn hơn ngày ngày nhận phòng đến ');
        }

        $promo_hotel = Promotion::find($id);
        $type_discount = Type_discount::find($promo_hotel->id_type_discount);

        if ($request->reduced_amount == null)
        {
            $type_discount->reduced_amount = $request->reduced_amount1;
        }
        else
        {
            $type_discount->reduced_amount = $request->reduced_amount;
        }
        $type_discount->percentage_reduction = $request->percentage_reduction;
        $type_discount->save();

        $promo_hotel->title = $request->title;
        $promo_hotel->content = $request->content;
        $promo_hotel->code_promotion = $request->code_promotion;
        if (strtotime($request->date_start) >= strtotime(date('d-m-Y h:i')))
        {
            $promo_hotel->status = 1;
        }
        $promo_hotel->status = $request->status;

        $promo_hotel->program_code_promo = $request->program_code_promo;

        if ($request->hasFile('images_cover'))
        {
            $file = $request
                ->images_cover
                ->getClientOriginalName();
            $day = date('Y-m-d');
            $file_custom = $day . '_' . $file;
            $promo_hotel->images_cover = $file_custom;
            $request
                ->images_cover
                ->move('upload/', $file_custom);
        }
        if ($request->hasFile('images_logo'))
        {
            $file = $request
                ->images_logo
                ->getClientOriginalName();
            $day = date('Y-m-d');
            $file_custom = $day . '_' . $file;
            $promo_hotel->images_logo = $file_custom;
            $request
                ->images_logo
                ->move('upload/', $file_custom);
        }
        if ($request->subsystem == "Hotel")
        {
            $promo_hotel->subsystem = 0;
        }
        else
        {
            $promo_hotel->subsystem = 1;
        }
        $promo_hotel->type_promo = $request->type_promo;
        if ($request->type_promo == 1)
        {
            $promo_hotel->display = 0;
        }
        else
        {
            $promo_hotel->display = 1;
        }

        $promo_hotel->link = $request->link;
        $promo_hotel->date_start = $request->date_start;
        $promo_hotel->date_end = $request->date_end;
        $promo_hotel->number_max = $request->number_max;
        $promo_hotel->number_limit_day = $request->number_limit_day;
        $promo_hotel->id_type_discount = $type_discount->id_type_discount;
        $promo_hotel->id_nation = $request->id_nation;
        $d_city = $request->id_city;
        if ($d_city != null)
        {
            $count = count($request->id_city);
            $data_city = '';
            for ($i = 0;$i < $count;$i++)
            {
                $data_city .= $d_city[$i] . ', ';
            }

            $promo_hotel->id_city = $data_city;
        }
        else
        {
            $promo_hotel->id_city = Session::get('id_city');

        }
        $services = $request->services;
        if ($services != null)
        {
            $count = count($request->services);
            $data_services = '';
            for ($i = 0;$i < $count;$i++)
            {
                $data_services .= $services[$i] . ', ';
            }

            $promo_hotel->services = $data_services;
        }
        else
        {
            $promo_hotel->services = Session::get('services');

        }
        $hotel_apply = $request->hotel_apply;
        if ($hotel_apply != null)
        {

            $count = count($request->hotel_apply);
            $data_hotel_apply = '';
            for ($i = 0;$i < $count;$i++)
            {
                $data_hotel_apply .= $hotel_apply[$i] . ', ';
            }
            $promo_hotel->hotel_apply = $data_hotel_apply;

        }
        else
        {
            $promo_hotel->hotel_apply = Session::get('hotel_apply');

        }
        $payment_partner = $request->payment_partner;
        if ($payment_partner != null)
        {

            $count = count($request->payment_partner);
            $data_payment_partner = '';
            for ($i = 0;$i < $count;$i++)
            {
                $data_payment_partner .= $payment_partner[$i] . ', ';
            }
            $promo_hotel->payment_partner = $data_payment_partner;
        }
        else
        {
            $promo_hotel->payment_partner = Session::get('payment_partner');

        }
        $promo_hotel->checkinFrom = $request->checkinFrom;
        $promo_hotel->checkinTo = $request->checkinTo;
        if ($request->value_order_min == null)
        {

            $promo_hotel->value_order_min = 0;
        }
        else
        {

            $promo_hotel->value_order_min = $request->value_order_min;
        }
        $promo_hotel->number_user_min = $request->number_user_min;
        $promo_hotel->ud_user = Session::get('admin_id');
        $promo_hotel->create_day = date('d-m-Y');

        $promo_hotel->save();
        return \redirect('admin/hotel/list');
    }

    public function loaddata()
    {
        $product = DB::table('tbl_tinhthanhpho')->get();
        return json_encode($product);
    }

    public function loadcity(Request $request)
    {

        $actual_link = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $tach = explode('=', $actual_link);
        $nation = ($tach[1]);
        $city = DB::table('cities')->where('country_id', $nation)->get();
        $html = '';
        $html .= "
<select class='js-example-basic-single form-control' multiple='multiple'>
   ";
        foreach ($city as $c)
        {
            $html .= "
   <option>" . $c->city_name . "</option>
   ";
        }
        $html .= "
</select>
";
        return $html;
    }

}

